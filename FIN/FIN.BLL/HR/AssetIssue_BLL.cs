﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class AssetIssue_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo
        public static void GetDepartmentName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetDesignationName(ref DropDownList ddlList, string DEPT_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetDesignationName(DEPT_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeName(ref DropDownList ddlList, string DEPT_ID, string DESIG_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetEmployeeName(DEPT_ID, DESIG_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_FIRST_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeName_asper_Applicant(ref DropDownList ddlList, string app_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetEmployeeName_asper_Applicant(app_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_FIRST_NAME, "INT_DTL_ID", dtDropDownData, true, false);
        }

        public static void GetAssetName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetAssetName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtDropDownData, true, false);
        }
        # endregion

        public static HR_ASSET_ISSUE_HDR getClassEntity(string str_Id)
        {
            HR_ASSET_ISSUE_HDR obj_HR_ASSET_ISSUE_HDR = new HR_ASSET_ISSUE_HDR();
            using (IRepository<HR_ASSET_ISSUE_HDR> userCtx = new DataRepository<HR_ASSET_ISSUE_HDR>())
            {
                obj_HR_ASSET_ISSUE_HDR = userCtx.Find(r =>
                    (r.ASSET_HDR_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_ASSET_ISSUE_HDR;
        }
    }
}
