﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class CareerPlanDtls_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo
        public static void GetDepartmentName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeProfile(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetEmployeeProfiles()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PROF_NAME, FINColumnConstants.PROF_ID, dtDropDownData, true, false);
        }
        public static void GetEmployeeProfile_Dtls(ref DropDownList ddlList, string emp_prof_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetEmployeeProfiles_Dtls(emp_prof_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.COM_LEVEL_DESC, FINColumnConstants.PROF_DTL_ID, dtDropDownData, true, false);
        }

        public static void GetCareerPath(ref DropDownList ddlList,string Dept_id,string Desig_id, string pathdtlId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetCareerPath(Dept_id, Desig_id, pathdtlId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PATH_NAME, FINColumnConstants.PATH_DTL_ID, dtDropDownData, true, false);
        }
        public static void GetCareerPlan(ref DropDownList ddlList, string pathdtlId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetCareerPlan(pathdtlId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PLAN_DESC", "PLAN_HDR_ID", dtDropDownData, true, false);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.CareerPlanDtls_DAL.GetCareerPlanDtls(Str_ID)).Tables[0];
            return dt_Det;
        }
    }
}
