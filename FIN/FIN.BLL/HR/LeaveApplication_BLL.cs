﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class LeaveApplication_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        # region FillCombo

        public static void GetLeaveApplication(ref DropDownList ddlList, string staff_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplication(staff_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_NAME, FINColumnConstants.LEAVE_REQ_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveReqID(ref DropDownList ddlList, string staff_id, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveReqID(staff_id, mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "TRANS_ID", FINColumnConstants.LEAVE_REQ_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveApplication4YearStafId(ref DropDownList ddlList, string staff_id, string str_finyear, string mode, string strid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplication4YearStaffId(staff_id, str_finyear, mode, strid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_NAME, FINColumnConstants.LEAVE_REQ_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveEncash(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveReturn_DAL.GetLeaveEncash()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LC_ID", "LC_ID", dtDropDownData, true, false);
        }

        public static void GetLeaveEncashLevReq(ref DropDownList ddlList, string str_emp_id, string MODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveReturn_DAL.GetLeaveReqdtls(str_emp_id, MODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LEAVE_REQ", "LEAVE_REQ_ID", dtDropDownData, true, false);
        }


        public static void GetDepartment(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }
        public static void GetStaff(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetLeave(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeave()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        # endregion


        public static HR_LEAVE_APPLICATIONS getClassEntity(string str_Id)
        {
            HR_LEAVE_APPLICATIONS obj_HR_LEAVE_APPLICATIONS = new HR_LEAVE_APPLICATIONS();
            using (IRepository<HR_LEAVE_APPLICATIONS> userCtx = new DataRepository<HR_LEAVE_APPLICATIONS>())
            {
                obj_HR_LEAVE_APPLICATIONS = userCtx.Find(r =>
                    (r.LEAVE_REQ_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_LEAVE_APPLICATIONS;
        }
        public static String ValidateEntity(HR_LEAVE_APPLICATIONS obj_HR_LEAVE_APPLICATIONS)
        {
            string str_Error = "";
            return str_Error;
        }

        public static String SaveEntity(HR_LEAVE_APPLICATIONS obj_HR_LEAVE_APPLICATIONS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            obj_HR_LEAVE_APPLICATIONS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_LEAVE_APPLICATIONS_SEQ);
                        }
                        DBMethod.SaveEntity<HR_LEAVE_APPLICATIONS>(obj_HR_LEAVE_APPLICATIONS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_LEAVE_APPLICATIONS>(obj_HR_LEAVE_APPLICATIONS, true);
                        break;
                    }
            }
            return str_PK;
        }
        public static String DeleteEntity(string str_ID)
        {
            string str_Message = "";
            HR_LEAVE_APPLICATIONS obj_HR_LEAVE_APPLICATIONS = new HR_LEAVE_APPLICATIONS();
            obj_HR_LEAVE_APPLICATIONS.LEAVE_REQ_ID = str_ID;
            DBMethod.DeleteEntity<HR_LEAVE_APPLICATIONS>(obj_HR_LEAVE_APPLICATIONS);
            return str_Message;
        }

        public static DataTable getListData(string modifyURL, string deleteURL)
        {
            return DBMethod.ExecuteQuery("select LEAVE_REQ_ID," + modifyURL + "," + deleteURL + " from HR_LEAVE_APPLICATIONS").Tables[0];
        }

        public static DataTable get_Leave_Emp_Assign_Substitue_chk(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.get_Leave_Emp_Assign_Substitue_chk(P_STAFF_ID, P_LEAVE_FROM, P_LEAVE_TO)).Tables[0];
        }

        public static DataTable get_Substitue_emp_Leave_chk(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.get_Substitue_emp_Leave_chk(P_STAFF_ID, P_LEAVE_FROM, P_LEAVE_TO)).Tables[0];
        }
       

    }
}
