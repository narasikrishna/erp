﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class TrainingSchedule_BLL
   {
       DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();
       public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

     
       # region FillCombo


       public static void fn_GetCourse(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetCourse()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
       }
       public static void fn_GetProgram(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetProgram()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PROG_DESC", "PROG_ID", dtDropDownData, true, false);
       }
       public static void fn_GetSubject(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetSubject()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
       }
       public static void GetScheduleData(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetScheduleData()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "TRN_DESC", "TRN_SCH_HDR_ID", dtDropDownData, true, false);
       }


       public static void fn_GetProgram4Shedule(ref DropDownList ddlList, string ScheduleID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetProgram4Shedule(ScheduleID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PROG_DESC", "PROG_ID", dtDropDownData, true, false);
       }

       public static void fn_GetCourse4SheduleProg(ref DropDownList ddlList,string ScheduleID, string ProgID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetCourse4SheduleProg(ScheduleID, ProgID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
       }

       public static void fn_GetCourse4Prog(ref DropDownList ddlList, string ProgID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetCourse4Prog( ProgID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
       }

       public static void fn_GetSubject4ScheduleprogCourse(ref DropDownList ddlList,string ScheduleID, string ProgID, string courseID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetSubject4ScheduleprogCourse(ScheduleID, ProgID,courseID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
       }

       public static void fn_GetSubject4progCourse(ref DropDownList ddlList, string ProgID, string courseID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetSubject4progCourse(ProgID, courseID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
       }

       public static void fn_GetTrngSchDtlID4ScheduleprogCourseSubjSession(ref DropDownList ddlList, string ScheduleID, string ProgID, string courseID, string subjID, string sessionID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetTrngSchDtlID4ScheduleprogCourseSubjSession(ScheduleID, ProgID, courseID, subjID, sessionID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
       }

       public static void fn_GetRequestType(ref DropDownList ddlList, string req_prog_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetRequestType(req_prog_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "REQ_HEADER", "REQ_HDR_ID", dtDropDownData, true, false);
       }

       public static void fn_GetRequestDtls(ref DropDownList ddlList, string REQ_HDR_ID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetRequestDtls(REQ_HDR_ID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "REQ_DTLS", "REQ_DTL_ID", dtDropDownData, true, false);
       }


       # endregion

       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingSchedule_DAL.GetTRSchdtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }

       public static HR_TRM_SCHEDULE_HDR getClassEntity(string str_Id)
       {
           HR_TRM_SCHEDULE_HDR obj_HR_TRM_SCHEDULE_HDR = new HR_TRM_SCHEDULE_HDR();
           using (IRepository<HR_TRM_SCHEDULE_HDR> userCtx = new DataRepository<HR_TRM_SCHEDULE_HDR>())
           {
               obj_HR_TRM_SCHEDULE_HDR = userCtx.Find(r =>
                   (r.TRN_SCH_HDR_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_HR_TRM_SCHEDULE_HDR;
       }
       public static String ValidateEntity(HR_TRM_SCHEDULE_HDR obj_HR_TRM_SCHEDULE_HDR)
       {
           string str_Error = "";
           return str_Error;
       }


       //public static String DeleteEntity(int int_PKID)
       //{
       //    string str_Message = "";
       //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
       //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
       //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
       //    return str_Message;
       //}

       public static System.Collections.SortedList ValidateDateRange(DataTable dtGridData, int rowIndex, DateTime? startdate, DateTime? enddate, DateTime? trndate, string trnDateName)
       {
           ErrorCollection.Remove("TRN_DATE");

           DateTime? prevstartDate = null;
           DateTime? prevEndDate = null;

           //if (dtGridData != null)
           //{
           //    if (dtGridData.Rows.Count > 0)
           //    {
           //        for (int i = 0; i < dtGridData.Rows.Count; i++)
           //        {
           //            if (rowIndex != i)
           //            {

           if (ErrorCollection.Count == 0)
           {
               if (trndate != null)
               {
                   if (!trndate.Equals(DateTime.MinValue))
                   {
                       if (trndate < startdate || trndate > enddate)
                       {
                           ErrorCollection.Add("TRN_DATE", "Training Date should be greater than from date and less than to date");

                       }
                   }
               }
           }

           //if (dtGridData.Rows[i][trnDateName].ToString() != string.Empty)
           //{
           //    prevEndDate = DateTime.Parse(dtGridData.Rows[i][trnDateName].ToString());
           //}

           //            }
           //        }
           //    }
           //}
           return ErrorCollection;
       }

   }
}
