﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class Trainer_BLL
   {
     
       
       public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();



       public static void GetTrainer(ref DropDownList ddlList, bool allFlag = false)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Trainer_DAL.GetTrainer()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "emp_name", "emp_id", dtDropDownData, !allFlag, allFlag);
       }

       public static void GetTrainer_frTrainingFeedback(ref DropDownList ddlList, string emp_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Trainer_DAL.GetTrainer_frTrainingFeedback(emp_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "emp_name", "emp_id", dtDropDownData, true, false);
       }

       public static void GetTrainer_ForTrainingEnrolment(ref DropDownList ddlList,string sub_id,string sees_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Trainer_DAL.GetTrainer_ForTrainingEnrolment(sub_id, sees_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "emp_name", "emp_id", dtDropDownData, true, false);
       }

   }
}
