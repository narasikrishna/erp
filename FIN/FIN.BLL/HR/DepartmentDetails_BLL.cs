﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class DepartmentDetails_BLL
    {

        DropDownList ddlList = new DropDownList();
       static  DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public void fn_GetDepartmentDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
        }

        public void fn_GetDepartmentDetails_All(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
        }


        public static void fn_GetDesigName(ref DropDownList ddlList,Boolean bol_Sel=true )
        {
            dtDropDownData = DBMethod.ExecuteQuery(DepartmentDetails_DAL.GetDesigName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DESIG_NAME", "PARENT_DEPT_DESIG_ID", dtDropDownData, bol_Sel, !bol_Sel);
        }

        public static void fn_getDesigName4DeptId(ref DropDownList ddlList, string str_Dept_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(DepartmentDetails_DAL.GetDesigName4DeptId(str_Dept_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DESIG_NAME", "PARENT_DEPT_DESIG_ID", dtDropDownData, true, false);
       
        }

        public static void fn_getSectionName4DeptId(ref DropDownList ddlList, string str_Dept_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(DepartmentDetails_DAL.GetSectionName4DeptId(str_Dept_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEC_NAME", "SEC_ID", dtDropDownData, true, false);

        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.DepartmentDetails_DAL.GetDepartmentDetails(Str_ID)).Tables[0];
            return dt_Det;
        }



    }
}
