﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class EmployeeVerification_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo

        public static void GetApplnId(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeVerification_DAL.GetApplicantDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.APP_NAME, FINColumnConstants.APP_ID, dtDropDownData, true, false);
        }

        public static void GetApplnId_Edit(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeVerification_DAL.GetApplicantDetails_Edit()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.APP_NAME, FINColumnConstants.APP_ID, dtDropDownData, true, false);
        }
        #endregion


        public static DataTable getApplnName(string App_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeVerification_DAL.GetApplicantName(App_id)).Tables[0];
            return dt_Det;
        }

        public static HR_EMP_VERIFICATION_HDR getClassEntity(string str_Id)
        {
            HR_EMP_VERIFICATION_HDR obj_HR_EMP_VERIFICATION_HDR = new HR_EMP_VERIFICATION_HDR();
            using (IRepository<HR_EMP_VERIFICATION_HDR> userCtx = new DataRepository<HR_EMP_VERIFICATION_HDR>())
            {
                obj_HR_EMP_VERIFICATION_HDR = userCtx.Find(r =>
                    (r.VERIFY_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_EMP_VERIFICATION_HDR;
        }
    }
}
