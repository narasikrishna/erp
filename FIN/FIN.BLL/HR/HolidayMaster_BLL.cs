﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;

using System.Data;


namespace FIN.BLL.HR
{
    public class HolidayMaster_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
       
        public static HR_HOLIDAYS_MASTER getClassEntity(string str_Id)
        {
            HR_HOLIDAYS_MASTER obj_HR_HOLIDAYS_MASTER = new HR_HOLIDAYS_MASTER();
            using (IRepository<HR_HOLIDAYS_MASTER> userCtx = new DataRepository<HR_HOLIDAYS_MASTER>())
            {
                obj_HR_HOLIDAYS_MASTER = userCtx.Find(r =>
                    (r.HOLIDAY_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_HOLIDAYS_MASTER;
        }
      


    }
}
