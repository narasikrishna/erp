﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class TimeAttendanceGroup_BLL
    {

       static DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_GetDepartmentDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
        }
        public static void GetTmAttndGrpdtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TimeAttendanceGroup_DAL.GetTmAttndGrpdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "tm_grp_code", "tm_grp_code", dtDropDownData, true, false);
        }
        public static void GetTmAttndGrpdtls_R(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TimeAttendanceGroup_DAL.GetTmAttndGrpdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "tm_grp_code", "tm_grp_code", dtDropDownData, false, true);
        } 
        # endregion

        public static DataTable getChildEntityDet(int Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TimeAttendanceGroup_DAL.GetTmAttndGrpdtls(Str_ID)).Tables[0];
            return dt_Det;
        }



    }
}
