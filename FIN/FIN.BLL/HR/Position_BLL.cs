﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class Position_BLL
    {

        DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

        # region FillCombo
       public static void fn_GetJobname(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "JOB_CODE", "JOB_ID", dtDropDownData, true, false);
        }

        public static void fn_GetPositionNameBasedonJob(ref DropDownList ddlList,string job_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Position_DAL.GetPositionNameBasedonJob(job_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "POSITION_DESC", "POSITION_ID", dtDropDownData, true, false);
        }
        public static void fn_GetPositionNameBasedonJob_R(ref DropDownList ddlList, string job_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Position_DAL.GetPositionNameBasedonJob(job_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "POSITION_DESC", "POSITION_ID", dtDropDownData, false, true);
        }
        public static void fn_GetPositionNam(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Position_DAL.GetPositionNam()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "POSITION_DESC", "POSITION_ID", dtDropDownData, false, true);
        }
        public static void fn_GetJobname_basedon_category(ref DropDownList ddlList, string CATEGORY_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname_basedon_category(CATEGORY_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "JOB_NAME", "JOB_ID", dtDropDownData, true, false);
        }

        public static void fn_GetPositionName(ref DropDownList ddlList, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Position_DAL.GetPositionName(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "POSITION_DESC", "POSITION_ID", dtDropDownData, true, false);
        }

        public void fn_GetChildRecord(ref DropDownList ddlList, string pos_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Position_DAL.CheckChildRecord(pos_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "JOB_CODE", "JOB_ID", dtDropDownData, true, false);
        }


        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Position_DAL.GetPositiondtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        //public static INV_TRANSFER_HDR getClassEntity(string str_Id)
        //{
        //    INV_TRANSFER_HDR obj_INV_TRANSFER_HDR = new INV_TRANSFER_HDR();
        //    using (IRepository<INV_TRANSFER_HDR> userCtx = new DataRepository<INV_TRANSFER_HDR>())
        //    {
        //        obj_INV_TRANSFER_HDR = userCtx.Find(r =>
        //            (r.INV_TRANS_ID == str_Id)
        //            ).SingleOrDefault();
        //    }


        //    return obj_INV_TRANSFER_HDR;
        //}
        //public static String ValidateEntity(INV_TRANSFER_HDR obj_INV_TRANSFER_HDR)
        //{
        //    string str_Error = "";
        //    return str_Error;
        //}


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
