﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;
namespace FIN.BLL.HR
{
   public class InitiateApprisal_BLL
    {
       public static HR_PER_APP_INIT getClassEntity(string str_Id)
        {
            HR_PER_APP_INIT obj_HR_PER_APP_INIT = new HR_PER_APP_INIT();
            using (IRepository<HR_PER_APP_INIT> userCtx = new DataRepository<HR_PER_APP_INIT>())
            {
                obj_HR_PER_APP_INIT = userCtx.Find(r =>
                    (r.PER_APP_INIT_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_PER_APP_INIT;
        }
    }
}
