﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class ProbationCompetencyBLL
    {
        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbationCompetencyDAL.GetProbationdtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static HR_PROBATION_COMP_DTL getClassEntity(string str_Id)
        {
            HR_PROBATION_COMP_DTL obj_HR_PROBATION_COMP_DTL = new HR_PROBATION_COMP_DTL();
            using (IRepository<HR_PROBATION_COMP_DTL> userCtx = new DataRepository<HR_PROBATION_COMP_DTL>())
            {
                obj_HR_PROBATION_COMP_DTL = userCtx.Find(r =>
                    (r.COM_LINE_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_PROBATION_COMP_DTL;
        }
    }
}
