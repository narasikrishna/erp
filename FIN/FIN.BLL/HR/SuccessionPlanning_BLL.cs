﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class SuccessionPlanning_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static void GetEmpProfileName(ref DropDownList ddlList, string profId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetEmpProfileName(profId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "prof_name", "prof_id", dtDropDownData, true, false);
        }
    }
}
