﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class GradeLevel_BLL
    {


        static DataTable dtDropDownData = new DataTable();
        static String Count = "";

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.GradeLevel_DAL.GetGradeLeveldtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static void fn_GetGradeNM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGradeNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GRADE_DESC, FINColumnConstants.GRADE_ID, dtDropDownData, true, false);
        }

        public static void fn_getElement(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

        }

        public static DataTable fn_GetGradeDtlsForDuplicate(String Grade_ID, String Position_ID, String Element_Id, String Job_ID, String Category_ID, string GRADE_LEVEL_ID)
        {
            DataTable dt_det = DBMethod.ExecuteQuery(FIN.DAL.HR.GradeLevel_DAL.GetGradeLeveldtls4Validation(Grade_ID, Position_ID, Element_Id, Job_ID, Category_ID, GRADE_LEVEL_ID)).Tables[0];
            return dt_det;
        }

    }
}
