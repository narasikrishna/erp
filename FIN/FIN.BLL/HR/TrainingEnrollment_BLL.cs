﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class TrainingEnrollment_BLL
   {
       DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();
       public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

       public static DataTable getTrngReqDeptEmpDet(string str_trngReqId)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingEnrollment_DAL.getTrngReqDeptEmpDet(str_trngReqId)).Tables[0];
           return dtDropDownData;
       }
       
   }
}
