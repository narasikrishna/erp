﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class ResignationInterview_BLL
    {
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public void fn_GetDepartmentDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
        }

        public void fn_GetDesignationName(ref DropDownList ddlList,string DEPT_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDesignationName(DEPT_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DESIG_NAME", "DESIGN_ID", dtDropDownData, true, false);
        }

        public void fn_GetEmployeeDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_NAME", "EMP_ID", dtDropDownData, true, false);
        }

        public void fn_GetResigRequestdtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ResignationInterview_DAL.GetResigRequestdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "RESIG_HDR_ID", "RESIG_HDR_ID", dtDropDownData, true, false);
        }


        # endregion




        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationInterview_DAL.getResigIntervDtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_EXIT_INTERVIEW_HDR getClassEntity(string str_Id)
        {
            HR_EXIT_INTERVIEW_HDR obj_HR_EXIT_INTERVIEW_HDR = new HR_EXIT_INTERVIEW_HDR();
            using (IRepository<HR_EXIT_INTERVIEW_HDR> userCtx = new DataRepository<HR_EXIT_INTERVIEW_HDR>())
            {
                obj_HR_EXIT_INTERVIEW_HDR = userCtx.Find(r =>
                    (r.EX_INT_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_EXIT_INTERVIEW_HDR;
        }
        public static String ValidateEntity(HR_EXIT_INTERVIEW_HDR obj_HR_EXIT_INTERVIEW_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
