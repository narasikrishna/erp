﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class TrainingRequest_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public static void fn_GetDeptNM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TrainingRequest_DAL.GetDeptNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "dept_id", dtDropDownData, true, false);
        }

        public static void fn_GetProgram(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TrainingRequest_DAL.GetProgram()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PROG_DESC", "PROG_ID", dtDropDownData, true, false);
        }


        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingRequest_DAL.GetTrainingReqdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_TRM_REQUEST_HDR getClassEntity(string str_Id)
        {
            HR_TRM_REQUEST_HDR obj_HR_TRM_REQUEST_HDR = new HR_TRM_REQUEST_HDR();
            using (IRepository<HR_TRM_REQUEST_HDR> userCtx = new DataRepository<HR_TRM_REQUEST_HDR>())
            {
                obj_HR_TRM_REQUEST_HDR = userCtx.Find(r =>
                    (r.REQ_HDR_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_TRM_REQUEST_HDR;
        }
        public static String ValidateEntity(HR_TRM_REQUEST_HDR obj_HR_TRM_REQUEST_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
