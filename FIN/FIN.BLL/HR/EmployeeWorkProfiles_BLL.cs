﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class EmployeeWorkProfiles_BLL
    {
            DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void fn_GetEmployeeName(ref DropDownList ddlList, string Dept_ID, string Desg_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeWorkProfile_DAL.GetEmployeeName(Dept_ID, Desg_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMPLOYEE_NAME", "EMP_ID", dtDropDownData, true, false);
        }

        public static void fn_GetEmployeeworkdtl(ref DropDownList ddlList, string profileId="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeWorkProfile_DAL.GetEmployeeprofdtl(profileId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_LEVEL_DESC", "PROF_DTL_ID", dtDropDownData, true, false);
        }

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeWorkProfile_DAL.GetEmpWorkProfdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_EMP_WORK_PROFILE_HDR getClassEntity(string str_Id)
        {
            HR_EMP_WORK_PROFILE_HDR obj_HR_EMP_WORK_PROFILE_HDR = new HR_EMP_WORK_PROFILE_HDR();
            using (IRepository<HR_EMP_WORK_PROFILE_HDR> userCtx = new DataRepository<HR_EMP_WORK_PROFILE_HDR>())
            {
                obj_HR_EMP_WORK_PROFILE_HDR = userCtx.Find(r =>
                    (r.EMP_WORK_PROF_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_EMP_WORK_PROFILE_HDR;
        }
        public static String ValidateEntity(HR_EMP_WORK_PROFILE_HDR obj_HR_EMP_WORK_PROFILE_HDR)
        {
            string str_Error = "";
            return str_Error;
        }
    }
}
