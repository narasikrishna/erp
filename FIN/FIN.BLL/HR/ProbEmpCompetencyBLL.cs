﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class ProbEmpCompetencyBLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public static void fn_GetCompetency(ref DropDownList ddlList)
        {

            dtDropDownData = DBMethod.ExecuteQuery(ProbEmpCompetencyDAL.GetCompetencyName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_DESC", "COM_HDR_ID", dtDropDownData, true, false);
        }

        public static void fn_GetCompetency_Level(ref DropDownList ddlList, string COM_HDR_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ProbEmpCompetencyDAL.GetCompetency_DescName(COM_HDR_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_LEVEL_DESC", "COM_LINE_ID", dtDropDownData, true, false);
        }

        public static void GetProbation_Competency_Name(ref DropDownList ddlList,string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ProbEmpCompetencyDAL.GetProbation_Competency_Name(emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_DESC", "PROB_DTL_ID", dtDropDownData, true, false);
        }

        public static void GetProbation_Emp_Name(ref DropDownList ddlList,string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ProbEmpCompetencyDAL.GetProbation_Emp_Name(emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PROB_COMPETENCY", "PROB_ID", dtDropDownData, true, false);
        }

        public static void fn_GetEmployee_Dtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "emp_name", "emp_id", dtDropDownData, true, false);
        }
        #endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpCompetencyDAL.GetProbationEmpdtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static HR_PROBATION_EMP_DTL getClassEntity(string str_Id)
        {
            HR_PROBATION_EMP_DTL obj_HR_PROBATION_EMP_DTL = new HR_PROBATION_EMP_DTL();
            using (IRepository<HR_PROBATION_EMP_DTL> userCtx = new DataRepository<HR_PROBATION_EMP_DTL>())
            {
                obj_HR_PROBATION_EMP_DTL = userCtx.Find(r =>
                    (r.PROB_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_PROBATION_EMP_DTL;
        }
    }
}
