﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class LeaveDepartment_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static HR_LEAVE_DEPT getClassEntity(string str_Id)
        {
            HR_LEAVE_DEPT obj_HR_LEAVE_DEPT = new HR_LEAVE_DEPT();
            using (IRepository<HR_LEAVE_DEPT> userCtx = new DataRepository<HR_LEAVE_DEPT>())
            {
                obj_HR_LEAVE_DEPT = userCtx.Find(r =>
                    (r.DEPT_LEAVE_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_LEAVE_DEPT;
        }
        public static void GetLeaveDepartmentBasedFisYear(ref DropDownList ddlList,string fiscalYr)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveDepartmentBasedFisYear(fiscalYr)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetDeptBasedFisYear(ref DropDownList ddlList, string fiscalYr)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetDeptBasedFisYear(fiscalYr)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetDepartment(ref DropDownList ddlList, string fiscalYr)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveDepartment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveBasedFiscalYrOrgID(ref DropDownList ddlList, string fiscalYr)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveBasedFiscalYrOrgID(fiscalYr)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }
    }
}
