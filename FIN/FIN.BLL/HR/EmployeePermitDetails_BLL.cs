﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
    public class EmployeePermitDetails_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo

        public static void fn_GetEmployee(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeePermitDetails_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_NO", "EMP_ID", dtDropDownData, true, false);
        }
        # endregion
    }
}
