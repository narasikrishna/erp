﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
   public class Permission_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo

        public static void GetEmployeeName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeNameBothIntExt(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetailsBothIntExt()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void fn_GetEmployeeDetails(ref DropDownList ddlList, string str_deptid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Permission_DAL.GetEmployeeDetails(str_deptid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Employee_Name", "emp_id", dtDropDownData, true, false);
        }

        //public static void GetgetCategory(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(Category_DAL.getCategory()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CATEGORY_CODE, FINColumnConstants.CATEGORY_ID, dtDropDownData, true, false);
        //}
        public static void getReason(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Reason_DAL.getReason()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.REASON_DESC, FINColumnConstants.REASON_ID, dtDropDownData, true, false);
        }

        public static void GetReasonBasedonCategory(ref DropDownList ddlList, string str_categoryid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Permission_DAL.GetReasonBasedonCategory(str_categoryid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Reason_Name", "Reason_id", dtDropDownData, true, false);
        }

        public static DataTable GetTimeScheduleforOrg()
        {
            dtDropDownData = DBMethod.ExecuteQuery(Permission_DAL.GetTimeScheduleofOrg()).Tables[0];

            return dtDropDownData;
        }

        # endregion
    }
}
