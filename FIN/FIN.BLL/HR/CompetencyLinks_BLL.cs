﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class CompetencyLinks_BLL
    {
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public void fn_getcompetencyname(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Competency_DAL.GetCompetencyName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_DESC", "COM_HDR_ID", dtDropDownData, true, false);
        }
        public void fn_GetDepartmentDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
        }
        public void fn_GetJobname(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "JOB_NAME", "JOB_ID", dtDropDownData, true, false);
        }
        public void fn_GetPositionName(ref DropDownList ddlList, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.Position_DAL.GetPositionName(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "POSITION_DESC", "POSITION_ID", dtDropDownData, true, false);
        }
        public void fn_GetLookUpValues(ref DropDownList ddlList, string typeKeyId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOOKUP_NAME", "LOOKUP_ID", dtDropDownData, true, false);
        }

        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.CompetencyLinks_DAL.GetCompetencyLinksdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_COMPETANCY_LINKS_HDR getClassEntity(string str_Id)
        {
            HR_COMPETANCY_LINKS_HDR obj_HR_COMPETANCY_LINKS_HDR = new HR_COMPETANCY_LINKS_HDR();
            using (IRepository<HR_COMPETANCY_LINKS_HDR> userCtx = new DataRepository<HR_COMPETANCY_LINKS_HDR>())
            {
                obj_HR_COMPETANCY_LINKS_HDR = userCtx.Find(r =>
                    (r.COM_LINK_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_COMPETANCY_LINKS_HDR;
        }
        public static String ValidateEntity(HR_COMPETANCY_LINKS_HDR obj_HR_COMPETANCY_LINKS_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
