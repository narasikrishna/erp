﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class Clearance_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Clearance_DAL.GetClearancedtls(Str_ID)).Tables[0];
            return dt_Det;
        }
       public static DataTable getChildEntityDet4Dept(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Clearance_DAL.GetClearancedtls4Dept(Str_ID)).Tables[0];
            return dt_Det;
        }
       
        public static HR_EMP_CLEARANCE_DTL getClassEntity(string str_Id)
        {
            HR_EMP_CLEARANCE_DTL obj_HR_EMP_CLEARANCE_DTL = new HR_EMP_CLEARANCE_DTL();
            using (IRepository<HR_EMP_CLEARANCE_DTL> userCtx = new DataRepository<HR_EMP_CLEARANCE_DTL>())
            {
                obj_HR_EMP_CLEARANCE_DTL = userCtx.Find(r =>
                    (r.EMP_CLEARANCE_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_EMP_CLEARANCE_DTL;
        }

        public static void fn_getParticulars(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Clearance_DAL.getParticulars()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PARTICULAR", "CLEARANCE_ID", dtDropDownData, true, false);
        }

        public static void fn_getParticularsAR(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Clearance_DAL.getParticulars()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PARTICULAR_OL", "CLEARANCE_ID", dtDropDownData, true, false);
        }

        public static DataTable getOvertimeCalDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Clearance_DAL.GetOvertimeCaldtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getLabEntrySlabDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Clearance_DAL.GetLabEntrySlabdtls(Str_ID)).Tables[0];
            return dt_Det;
        }



       

      

    }
}
