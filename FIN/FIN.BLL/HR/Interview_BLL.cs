﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class Interview_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public static void fn_GetVacancyNM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetVacancyNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VAC_NAME", "VAC_ID", dtDropDownData, true, false);
        }
        public static void fn_GetApplicantNM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetApplicantNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_FIRST_NAME", "APP_ID", dtDropDownData, true, false);
        }

        public static void fn_GetApplicantNM_basedon_vacancy(ref DropDownList ddlList,string vac_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetApplicantNM_basedon_vacancy(vac_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_FIRST_NAME", "APP_ID", dtDropDownData, true, false);
        }

        public static void fn_GetVacCriteria(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetVacCriteria()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VAC_CRIT_NAME", "VAC_CRIT_ID", dtDropDownData, true, false);
        }

        public static void fn_GetInterview(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetInterview()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "INT_DESC", "INT_ID", dtDropDownData, true, false);
        }

        public static void getLoc(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.Interview_DAL.getLocation()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOC_DESC", "LOC_ID", dtDropDownData, true, false);
        }

        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Interview_DAL.GetInterviewdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_INTERVIEWS_HDR getClassEntity(string str_Id)
        {
            HR_INTERVIEWS_HDR obj_HR_INTERVIEWS_HDR = new HR_INTERVIEWS_HDR();
            using (IRepository<HR_INTERVIEWS_HDR> userCtx = new DataRepository<HR_INTERVIEWS_HDR>())
            {
                obj_HR_INTERVIEWS_HDR = userCtx.Find(r =>
                    (r.INT_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_INTERVIEWS_HDR;
        }
        public static String ValidateEntity(HR_INTERVIEWS_HDR obj_HR_INTERVIEWS_HDR)
        {
            string str_Error = "";
            return str_Error;
        }

        public static DataTable getInterview4InterveiwId(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Interview_DAL.getInterview4InterveiwId(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getShortListApp4vacancy(string str_VacID,string str_intid)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Interview_DAL.getShortListApp4vacancy(str_VacID, str_intid)).Tables[0];
            return dt_Det;
        }

        public static void GetEmployeeName(ref DropDownList ddlList, string DEPT_ID, string DESIG_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Interview_DAL.GetEmployeeName(DEPT_ID, DESIG_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_FIRST_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
