﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class EmployeeProfile_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public static void fn_GetCompetency(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeProfile_DAL.GetCompetency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_DESC", "COM_HDR_ID", dtDropDownData, true, false);
        }
        public static void fn_GetCompetencyLevedesc(ref DropDownList ddlList, string COM_HDR_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeProfile_DAL.GetCompetencyLevedesc(COM_HDR_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COM_LEVEL_DESC", "COM_LINE_ID", dtDropDownData, true, false);
        }
        public static void fn_GetEmployeeProfile(ref DropDownList ddlList, string prof_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeProfile_DAL.GetEmployeeProfName(prof_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PROF_NAME", "PROF_ID", dtDropDownData, true, false);
        }
        
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeProfile_DAL.GetEmpProfdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_EMP_PROFILE_HDR getClassEntity(string str_Id)
        {
            HR_EMP_PROFILE_HDR obj_HR_EMP_PROFILE_HDR = new HR_EMP_PROFILE_HDR();
            using (IRepository<HR_EMP_PROFILE_HDR> userCtx = new DataRepository<HR_EMP_PROFILE_HDR>())
            {
                obj_HR_EMP_PROFILE_HDR = userCtx.Find(r =>
                    (r.PROF_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_EMP_PROFILE_HDR;
        }
        public static String ValidateEntity(HR_EMP_PROFILE_HDR obj_HR_EMP_PROFILE_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}



    }
}
