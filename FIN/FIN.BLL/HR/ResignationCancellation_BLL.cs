﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public  class ResignationCancellation_BLL
    {
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public void fn_GetResigReqNo(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ResignationCancellation_DAL.GetResigNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "RESIG_HDR_ID", "RESIG_HDR_ID", dtDropDownData, true, false);
        }

        public void fn_GetResigReqNoCncel(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ResignationCancellation_DAL.GetResigNofromCancel()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "RESIG_HDR_ID", "RESIG_HDR_ID", dtDropDownData, true, false);
        }

        # endregion
    }
}
