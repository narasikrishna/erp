﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
   public  class Department_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.GetDeptdtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getSectionEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.GetSecdtls(Str_ID)).Tables[0];
            return dt_Det;
        }

       public static void GetDepartmentName(ref DropDownList ddlList,Boolean bol_sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, bol_sel, !bol_sel);
        }

       public static void GetDepartmentNam(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, false, true);
       }
       public static void GetDepartmentName4Group(ref DropDownList ddlList,string str_group,bool bol_sel=true)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails4Group(str_group)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, bol_sel, !bol_sel);
       }

       public static void GetDesignationName(ref DropDownList ddlList, string DEPT_ID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDesignationName(DEPT_ID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, true, false);
       }
       public static void GetDesignationName_R(ref DropDownList ddlList, string DEPT_ID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDesignationName(DEPT_ID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, false, true);
       }
       public static void GetDesignationName_All(ref DropDownList ddlList, string DEPT_ID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDesignationName(DEPT_ID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, true, false);
       }

       public static void GetDesignation(ref DropDownList ddlList,Boolean bol_Sel=true)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDesignation()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, bol_Sel, !bol_Sel);
       }
       public static void fn_GetEmployeeDetails(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetEmployeeDetails()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
       }

       public static DataSet GetManpowerPlanReport()
       {
           return DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.get_ManpowerPlanReport());
       }

       public static DataSet GetLetterTemplateReport()
       {
           return DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.get_LetterTemplateReport());
       }
       public static void GetLetterName(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetLetterDetails()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "LETTER_DESC","LETTER_ID", dtDropDownData, true, false);
       }


        # endregion
    }
}
