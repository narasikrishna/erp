﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class EmployeeContractAssignment_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static HR_APPLICANTS getClassEntity(string str_Id)
        {
            HR_APPLICANTS obj_HR_APPLICANTS = new HR_APPLICANTS();
            using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
            {
                obj_HR_APPLICANTS = userCtx.Find(r =>
                    (r.APP_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_APPLICANTS;
        }

        
    }
}
