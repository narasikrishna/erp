﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class Grades_BLL
    {

        DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

        # region FillCombo
      

        public static void fn_getGrade(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGradeName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GRADE_DESC, FINColumnConstants.GRADE_ID, dtDropDownData, true, false);
        }
        public static void fn_getGrade_R(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGradeNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GRADE_DESC, FINColumnConstants.GRADE_ID, dtDropDownData, false, true);
        }
        public static void fn_getGradeName4Position(ref DropDownList ddlList, string str_Position)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGrade4Position(str_Position)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GRADE_NAME, FINColumnConstants.GRADE_ID, dtDropDownData, true, false);
        }
        public static void fn_getGradeName4Position_R(ref DropDownList ddlList, string str_Position)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGrade4Position(str_Position)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GRADE_NAME, FINColumnConstants.GRADE_ID, dtDropDownData, false, true);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Grades_DAL.GetGradedtls(Str_ID)).Tables[0];
            return dt_Det;
        }
     


    }
}
