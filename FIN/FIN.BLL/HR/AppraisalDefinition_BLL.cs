﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.HR
{
    public class AppraisalDefinition_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_GetAppraisalPeriodDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefinition_DAL.fn_GetAppraisalPeriodDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.APP_NAME, FINColumnConstants.APP_ID, dtDropDownData, true, false);
        }

        # endregion

    }
}
