﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;


namespace FIN.BLL.HR
{
    public class Vacation_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static HR_VACATIONS getClassEntity(string str_Id)
        {
            HR_VACATIONS obj_HR_VACATIONS = new HR_VACATIONS();
            using (IRepository<HR_VACATIONS> userCtx = new DataRepository<HR_VACATIONS>())
            {
                obj_HR_VACATIONS = userCtx.Find(r =>
                    (r.VACATION_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_VACATIONS;
        }

      
    }
}
