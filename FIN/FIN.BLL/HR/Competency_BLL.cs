﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class Competency_BLL
  {
      DropDownList ddlList = new DropDownList();
      DataTable dtDropDownData = new DataTable();

      # region FillCombo


      //public void fn_getLineNo(ref DropDownList ddlList)
      //{
      //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
      //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
      //}

    


      # endregion




      public static DataTable getChildEntityDet(string Str_ID)
      {
          DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Competency_DAL.GetCompetencydtls(Str_ID)).Tables[0];
          return dt_Det;
      }

      public static DataTable getpayrollDistributionDet(string Str_ID)
      {
          DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Competency_DAL.GetPayrollDistributiondtls(Str_ID)).Tables[0];
          return dt_Det;
      }

      public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
          where T : class
          where TC : class
      {
          try
          {
              DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
          }
          catch (Exception ex)
          {
              throw ex.InnerException;
          }

      }

      public static HR_COMPETENCY_HDR getClassEntity(string str_Id)
      {
          HR_COMPETENCY_HDR obj_HR_COMPETENCY_HDR = new HR_COMPETENCY_HDR();
          using (IRepository<HR_COMPETENCY_HDR> userCtx = new DataRepository<HR_COMPETENCY_HDR>())
          {
              obj_HR_COMPETENCY_HDR = userCtx.Find(r =>
                  (r.COM_HDR_ID == str_Id)
                  ).SingleOrDefault();
          }


          return obj_HR_COMPETENCY_HDR;
      }
      public static String ValidateEntity(HR_COMPETENCY_HDR obj_HR_COMPETENCY_HDR)
      {
          string str_Error = "";
          return str_Error;
      }


      //public static String DeleteEntity(int int_PKID)
      //{
      //    string str_Message = "";
      //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
      //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
      //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
      //    return str_Message;
      //}



  }
}
