﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
   public class TrainingCost_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
       public static void GetBudgetName(ref DropDownList ddlList,string PROG_ID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingCost_DAL.GetBudgetName(PROG_ID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.TRN_BUDGET_SHORT_NAME, FINColumnConstants.TRN_BUDGET_ID, dtDropDownData, true, false);
       }
    }
}
 