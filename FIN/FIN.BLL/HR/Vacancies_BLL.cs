﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class Vacancies_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void getVacancies(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Vacancies_DAL.getVacancies()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "vac_name", "vac_id", dtDropDownData, true, false);
        }


    }
}
