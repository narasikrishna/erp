﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
    public class ProbEmpEvalBLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static void fn_ProbAppraiser4Emp(ref DropDownList ddlList, string str_empid)
        {

            dtDropDownData = DBMethod.ExecuteQuery(ProbEmpEvalDAL.getProbAppraiser4Emp(str_empid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_NAME", "PROB_APPR_ID", dtDropDownData, true, false);
        }
    }
}
