﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class Course_BLL
    {
        DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        public static void fn_GetFacilitydtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetFacilitydtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "FACILITY_DTL", "FACILITY_ID", dtDropDownData, true, false);
        }
        public static void GetCourseBasedPgm(ref DropDownList ddlList, string pgmId)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetCourseBasedPgm(pgmId)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
       }
        public static void GetSubjectBasedCourse(ref DropDownList ddlList, string courseId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetSubjectBasedCourse(courseId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Course_DAL.GetSubjectdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_TRM_COURSE getClassEntity(string str_Id)
        {
            HR_TRM_COURSE obj_HR_TRM_COURSE = new HR_TRM_COURSE();
            using (IRepository<HR_TRM_COURSE> userCtx = new DataRepository<HR_TRM_COURSE>())
            {
                obj_HR_TRM_COURSE = userCtx.Find(r =>
                    (r.COURSE_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_TRM_COURSE;
        }
        public static String ValidateEntity(HR_COMPETENCY_HDR obj_HR_TRM_COURSE)
        {
            string str_Error = "";
            return str_Error;
        }


        //public static String DeleteEntity(int int_PKID)
        //{
        //    string str_Message = "";
        //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
        //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
        //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
        //    return str_Message;
        //}

        public static void fn_getCourse(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetCourse()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
        }
        public static void GetTriningCourse(ref DropDownList ddlList, string assignHdrId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetTriningCourse(assignHdrId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
        }

    }
}
