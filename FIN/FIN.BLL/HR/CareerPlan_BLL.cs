﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class CareerPlan_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo
        public static void GetDepartmentName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlan_DAL.GetDepartmentDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DEPT_NAME, FINColumnConstants.DEPT_ID, dtDropDownData, true, false);
        }

        public static void GetDesignationName(ref DropDownList ddlList,string Dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CareerPlan_DAL.GetDesignationName(Dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, true, false);
        }
        #endregion

        public static HR_CAREER_PATH_HDR getClassEntity(string str_Id)
        {
            HR_CAREER_PATH_HDR obj_HR_CAREER_PATH_HDR = new HR_CAREER_PATH_HDR();
            using (IRepository<HR_CAREER_PATH_HDR> userCtx = new DataRepository<HR_CAREER_PATH_HDR>())
            {
                obj_HR_CAREER_PATH_HDR = userCtx.Find(r =>
                    (r.PATH_HDR_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_CAREER_PATH_HDR;
        }
    }
}
