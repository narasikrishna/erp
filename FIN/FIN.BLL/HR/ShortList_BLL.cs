﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class ShortList_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        public static HR_SHORT_LIST_HDR getClassEntity(string str_Id)
        {
            HR_SHORT_LIST_HDR obj_HR_SHORT_LIST_HDR = new HR_SHORT_LIST_HDR();
            using (IRepository<HR_SHORT_LIST_HDR> userCtx = new DataRepository<HR_SHORT_LIST_HDR>())
            {
                obj_HR_SHORT_LIST_HDR = userCtx.Find(r =>
                    (r.SHORT_LIST_HDR_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_SHORT_LIST_HDR;
        }

        public static HR_SHORT_LIST_DTL getDtlClassEntity(string str_Id)
        {
            HR_SHORT_LIST_DTL obj_HR_SHORT_LIST_DTL = new HR_SHORT_LIST_DTL();
            using (IRepository<HR_SHORT_LIST_DTL> userCtx = new DataRepository<HR_SHORT_LIST_DTL>())
            {
                obj_HR_SHORT_LIST_DTL = userCtx.Find(r =>
                    (r.SHORT_LIST_DTL_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_SHORT_LIST_DTL;
        }


    }
}
