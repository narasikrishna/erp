﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
  public class Program_BLL
  {
      DropDownList ddlList = new DropDownList();
      static DataTable dtDropDownData = new DataTable();

      # region FillCombo


      public static void fn_GetCourse(ref DropDownList ddlList)
      {
          dtDropDownData = DBMethod.ExecuteQuery(Program_DAL.GetCourse()).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
      }


      # endregion

      public static DataTable getChildEntityDet(string Str_ID)
      {
          DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Program_DAL.GetProgramdtls(Str_ID)).Tables[0];
          return dt_Det;
      }
      public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
          where T : class
          where TC : class
      {
          try
          {
              DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
          }
          catch (Exception ex)
          {
              throw ex.InnerException;
          }

      }

      public static HR_TRM_PROG_HDR getClassEntity(string str_Id)
      {
          HR_TRM_PROG_HDR obj_HR_TRM_PROG_HDR = new HR_TRM_PROG_HDR();
          using (IRepository<HR_TRM_PROG_HDR> userCtx = new DataRepository<HR_TRM_PROG_HDR>())
          {
              obj_HR_TRM_PROG_HDR = userCtx.Find(r =>
                  (r.PROG_ID == str_Id)
                  ).SingleOrDefault();
          }


          return obj_HR_TRM_PROG_HDR;
      }
      public static String ValidateEntity(HR_TRM_PROG_HDR obj_HR_TRM_PROG_HDR)
      {
          string str_Error = "";
          return str_Error;
      }

      public static void fn_getProgrameName(ref DropDownList ddlProgramName)
      {
          ddlProgramName.AppendDataBoundItems = true;
          ddlProgramName.Items.Clear();
          if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
          {

              ddlProgramName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
          }
          else
          {
              ddlProgramName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
          }
          //ddlProgramName.Items.Add(new ListItem("---Select---", ""));
          ddlProgramName.DataTextField = "PROG_DESC";
          ddlProgramName.DataValueField = "PROG_ID";
          ddlProgramName.DataSource = DBMethod.ExecuteQuery(Program_DAL.GetProgramName()).Tables[0];
          ddlProgramName.DataBind();
      }
      //public static String DeleteEntity(int int_PKID)
      //{
      //    string str_Message = "";
      //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
      //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
      //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
      //    return str_Message;
      //}



  }
}
