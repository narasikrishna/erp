﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class Location_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo
        public static void GetLocationDetails(ref DropDownList ddlList, string countryId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Location_DAL.GetLocationDetails(countryId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOC_DESC, FINColumnConstants.LOC_ID, dtDropDownData, true, false);
        }

        public static void GetLocations(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Location_DAL.GetLocations()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOC_DESC, FINColumnConstants.LOC_ID, dtDropDownData, true, false);
        }

        public static void GetSchedule(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Location_DAL.GetSchedule()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SCHEDULE_TIME", "TM_SCH_CODE", dtDropDownData, true, false);
        }


        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Location_DAL.fn_GetLocationGridDtls(Str_ID)).Tables[0];
            return dt_Det;
        }

    
    }
}
