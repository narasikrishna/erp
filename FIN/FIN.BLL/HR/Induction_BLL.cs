﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.HR
{
   public class Induction_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_INDUCTION_DTL getClassEntity(string str_Id)
        {
            HR_INDUCTION_DTL obj_HR_INDUCTION_DTL = new HR_INDUCTION_DTL();
            using (IRepository<HR_INDUCTION_DTL> userCtx = new DataRepository<HR_INDUCTION_DTL>())
            {
                obj_HR_INDUCTION_DTL = userCtx.Find(r =>
                    (r.HR_IND_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_INDUCTION_DTL;
        }
        public static void fn_GetApplicantDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Induction_DAL.GetApplicantDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "HR_APPLICANT_NAME", FINColumnConstants.HR_APPLICANT_ID, dtDropDownData, true, false);
        }

        public void fn_getApplicantName(ref DropDownList ddlList, string APP_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Induction_DAL.getApplicantName(APP_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "HR_APPLICANT_NAME", "HR_APPLICANT_ID", dtDropDownData, true, false);
        }

        public static void fn_getActivityName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Induction_DAL.getActivityName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "HR_ACTIVITY_DESC", "HR_IND_ACT_ID", dtDropDownData, true, false);
        }

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Induction_DAL.GetInductionActivitydtls(Str_ID)).Tables[0];
            return dt_Det;
        }

    }
}
