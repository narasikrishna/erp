﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class LeaveIndemCalc_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();



        public static void fn_getIndemName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveIndemCalc_DAL.GetIndemName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "indem_name", "indem_name", dtDropDownData, true, false);
        }
    }
}
