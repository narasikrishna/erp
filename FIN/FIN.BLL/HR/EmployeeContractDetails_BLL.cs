﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
namespace FIN.BLL.HR
{
    public class EmployeeContractDetails_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeContractDetails_DAL.GetEmployeeContractDtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static void fn_ContractName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeContractDetails_DAL.GetContractName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CONTRACT_DESC", "CONTRACT_HDR_ID", dtDropDownData, true, false);
        }

        public static void fn_getContractName4EmpId(ref DropDownList ddlList,string strEmpId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(EmployeeContractDetails_DAL.getContractName4EmpId(strEmpId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CONTRACT_DESC", "CONTRACT_HDR_ID", dtDropDownData, true, false);
        }

    }
}
