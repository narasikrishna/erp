﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
    public class TrainingFeedback_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo

        public static void fn_GetSchedule(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TrainingFeedback_DAL.GetSchedule()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "TRN_DESC", "TRN_SCH_HDR_ID", dtDropDownData, true, false);
        }

        public static void fn_GetProgram(ref DropDownList ddlList, string ScheduleID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TrainingFeedback_DAL.GetProgram(ScheduleID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PROG_DESC", "PROG_ID", dtDropDownData, true, false);
        }

        public static void fn_GetCourse(ref DropDownList ddlList, string ProgID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Program_DAL.GetCourse(ProgID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
        }

        public static void fn_GetSubject(ref DropDownList ddlList, string courseID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetSubjectBasedCourse(courseID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
        }

        # endregion

        public static DataSet get_MonthlysalariesRevealedTrailReport()
        {
            DataSet dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_MonthlysalariesRevealedTrailReport());
            return dt_Det;
        }

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.fn_GetTrnFeedbackDtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static HR_TRM_FEEDBACK_HDR getClassEntity(string str_Id)
        {
            HR_TRM_FEEDBACK_HDR obj_HR_TRM_FEEDBACK_HDR = new HR_TRM_FEEDBACK_HDR();
            using (IRepository<HR_TRM_FEEDBACK_HDR> userCtx = new DataRepository<HR_TRM_FEEDBACK_HDR>())
            {
                obj_HR_TRM_FEEDBACK_HDR = userCtx.Find(r =>
                    (r.TRN_SCH_HDR_ID == str_Id)
                    ).SingleOrDefault();
            }

            return obj_HR_TRM_FEEDBACK_HDR;
        }
        public static String ValidateEntity(HR_TRM_FEEDBACK_HDR obj_HR_TRM_FEEDBACK_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


    }
}
