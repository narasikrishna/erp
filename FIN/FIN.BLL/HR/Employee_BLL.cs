﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.HR
{
    public class Employee_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static string getMaxEmpNo()
        {
            DataTable dt_tmp = DBMethod.ExecuteQuery(Employee_DAL.GetEmpMaxNumber()).Tables[0];
            return dt_tmp.Rows[0][0].ToString();
        }

        public static DataSet GetEmpDetailsWithBasic()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.get_EmpDetailsWithBasic());
        }

        public static DataSet GetEmpDetails_With_ReportingTo()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.get_EmpDetails_With_ReportingTo());
        }

        public static void GetEmployeeDetails_all(ref DropDownList ddlList, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails_all()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, !bol_ALL, bol_ALL);
        }

        public static String ValidateEntity(HR_EMPLOYEES obj_GL_ACCT_GROUPS)
        {
            string str_Error = "";
            return str_Error;
        }
        public static String SaveEntity(GL_ACCT_GROUPS obj_GL_ACCT_GROUPS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";
            if (GenPKID)
            {
                obj_GL_ACCT_GROUPS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_GROUPS_SEQ);
            }
            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        DBMethod.SaveEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS, true);
                        break;
                    }
            }
            return str_PK;
        }


        public static String ValidateEntity(HR_ADDRESS obj_HR_ADDRESS)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String SaveEntity(HR_ADDRESS obj_HR_ADDRESS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {

                        DBMethod.SaveEntity<HR_ADDRESS>(obj_HR_ADDRESS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_ADDRESS>(obj_HR_ADDRESS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static DataTable getEmployeeAddress(string str_empid)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeAddress(str_empid)).Tables[0];

        }

        public static DataTable getEmployeeContact(string str_empid)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeContact(str_empid)).Tables[0];

        }
        public static DataTable getEmployeeDependant(string str_empid)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeDependants(str_empid)).Tables[0];

        }
        public static String ValidateEntity(HR_EMP_CONTACT obj_HR_EMP_CONTACT)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String SaveEntity(HR_EMP_CONTACT obj_HR_EMP_CONTACT, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            //obj_HR_EMP_CONTACT.EMP_CONTACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_EMP_CONTACT>(obj_HR_EMP_CONTACT);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_CONTACT>(obj_HR_EMP_CONTACT, true);
                        break;
                    }
            }
            return str_PK;
        }


        public static String SaveEntity(HR_EMP_PASSWORD_DTLS obj_HR_EMP_PASSWORD_DTLS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            //obj_HR_EMP_CONTACT.EMP_CONTACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_EMP_PASSWORD_DTLS>(obj_HR_EMP_PASSWORD_DTLS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_PASSWORD_DTLS>(obj_HR_EMP_PASSWORD_DTLS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static String SaveEntity(HR_DEPENDANTS obj_HR_DEPENDANTS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            //obj_HR_EMP_CONTACT.EMP_CONTACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_DEPENDANTS>(obj_HR_DEPENDANTS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_DEPENDANTS>(obj_HR_DEPENDANTS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static String SaveEntity(HR_QUALIFICATIONS obj_HR_QUALIFICATIONS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {

                        DBMethod.SaveEntity<HR_QUALIFICATIONS>(obj_HR_QUALIFICATIONS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_QUALIFICATIONS>(obj_HR_QUALIFICATIONS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static String SaveEntity(HR_EMP_PASSPORT_DETAILS obj_HR_EMP_PASSPORT_DETAILS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {

                        DBMethod.SaveEntity<HR_EMP_PASSPORT_DETAILS>(obj_HR_EMP_PASSPORT_DETAILS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_PASSPORT_DETAILS>(obj_HR_EMP_PASSPORT_DETAILS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static String ValidateEntity(HR_EMP_WORK_DTLS obj_HR_EMP_WORK_DTLS)
        {
            string str_Error = "";
            return str_Error;
        }

        public static String ValidateEntity(HR_DEPENDANTS obj_HR_DEPENDANTS)
        {
            string str_Error = "";
            return str_Error;
        }
        public static String SaveEntity(HR_EMP_WORK_DTLS obj_HR_EMP_WORK_DTLS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            // obj_HR_EMP_WORK_DTLS.EMP_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_EMP_WORK_DTLS>(obj_HR_EMP_WORK_DTLS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_WORK_DTLS>(obj_HR_EMP_WORK_DTLS, true);
                        break;
                    }
            }
            return str_PK;
        }

        public static String ValidateEntity(HR_EMP_SKILL_DTLS obj_HR_EMP_SKILL_DTLS)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String SaveEntity(HR_EMP_SKILL_DTLS obj_HR_EMP_SKILL_DTLS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            //obj_HR_EMP_SKILL_DTLS.EMP_SKILL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_EMP_SKILL_DTLS>(obj_HR_EMP_SKILL_DTLS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_SKILL_DTLS>(obj_HR_EMP_SKILL_DTLS, true);
                        break;
                    }
            }
            return str_PK;
        }



        public static String ValidateEntity(HR_EMP_BANK_DTLS obj_HR_EMP_BANK_DTLS)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String SaveEntity(HR_EMP_BANK_DTLS obj_HR_EMP_BANK_DTLS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            // obj_HR_EMP_BANK_DTLS.EMP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                        }
                        DBMethod.SaveEntity<HR_EMP_BANK_DTLS>(obj_HR_EMP_BANK_DTLS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_EMP_BANK_DTLS>(obj_HR_EMP_BANK_DTLS, true);
                        break;
                    }
            }
            return str_PK;
        }
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getReportData());
        }
        public static DataSet GetDetailedMonthlyScaleReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getDetailedMonthlyScaleReport());
        }

        # region FillCombo

        public static void GetEmpName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetResignedEmpDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetResignedEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetEmp_whohaveinLevapp(ref DropDownList ddlList, string dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmp_whohaveinLevapp(dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeName(ref DropDownList ddlList,Boolean bol_Sel=true )
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetEmployeeName_t(ref DropDownList ddlList, Boolean bol_Sel = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails_D()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetEmployeeNam(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }

        public static void GetEmployeeTrainerName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeTrainerDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeNameWithCode(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetailswithcode()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }


        public static void GetEmployeeId(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetEmplName(ref DropDownList ddlList, string emp_dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNMasperdept(emp_dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeNMasperdept_whohavALonly(ref DropDownList ddlList, string emp_dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNMasperdept_whohavALonly(emp_dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }


        public static void GetEmplNameBothIntExt(ref DropDownList ddlList, string emp_dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNMasperdeptBothIntExt(emp_dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmployee_NotinAppliedemp(ref DropDownList ddlList, string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployee_NotinAppliedemp(emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmplDet(ref DropDownList ddlList, string emp_dept_id, string emp_desi_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNdeptNdes(emp_dept_id, emp_desi_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetEmplNameNDeptDesig(ref DropDownList ddlList, string emp_dept_id, string dept_desig_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNMasperdeptnDesig(emp_dept_id, dept_desig_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetEmplNameNDeptDesig_R(ref DropDownList ddlList, string emp_dept_id, string dept_desig_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNMasperdeptnDesig(emp_dept_id, dept_desig_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }
        public static DataTable getEmployeeCurrentDepartment(string str_Emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.getEmployeeCurrentdepartment(str_Emp_id)).Tables[0];
            return dtDropDownData;

        }

        public static DataTable getLeaveDetails(string str_finyear, string str_deptid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.getLeaveDetails(str_finyear, str_deptid)).Tables[0];
            return dtDropDownData;

        }

        public static void GetEmpNameFromPayroll(ref DropDownList ddlList, string periodId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmpNameFromPayroll(periodId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetEmpNameFromPayroll_R(ref DropDownList ddlList, string periodId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmpNameFromPayrollPeriod(periodId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }

        public static void GetTerminatedEmpDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetTerminatedEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetActiveEmpDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetActiveEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }

        public static void GetEmployeeNameBasedOnDept(ref DropDownList ddlList, string emp_dept_id,Boolean bol_Sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNameBasedOnDept(emp_dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetEmployeeNameBasedOnDept_R(ref DropDownList ddlList, string emp_dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNameBasedOnDept(emp_dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }
        public static void GetEmployeeNationality(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNationality()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "NATIONALITY", "NATIONALITY", dtDropDownData, !includeAll, includeAll);
        }

        # endregion
    }
}
