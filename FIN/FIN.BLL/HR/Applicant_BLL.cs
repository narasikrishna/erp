﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
    public  class Applicant_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        private HR_APPLICANTS_QUALIFICATION _HR_APPLICANT;
        public HR_APPLICANTS_QUALIFICATION HR_APPLICANT
        {
            get { return _HR_APPLICANT; }
            set { _HR_APPLICANT = value; }
        }

        //public void Validate()
        //{
        //    IsDBDuplicateOrganizationMaster();


        //}


        //private void IsDBDuplicateOrganizationMaster()
        //{
        //    ErrorCollection.Remove(FINColumnConstants.APP_QUALI_ID);


        //    dtData = DBMethod.ExecuteQuery(FINSQL.IsMultipleValueExists(FINTableConstant.HR_APPLICANTS_QUALIFICATION, FINColumnConstants.PK_ID, HR_APPLICANT.PK_ID, FINColumnConstants.APP_QUALI_ID, HR_APPLICANT.APP_QUALI_ID.ToString(), FINColumnConstants.APPLICANT_ID,HR_APPLICANT.APP_ID, "", "")).Tables[0];
        //    if (dtData != null)
        //    {
        //        if (dtData.Rows.Count > 0)
        //        {
        //            count = int.Parse(dtData.Rows[0]["count"].ToString());
        //        }
        //    }
        //    if (count > 0)  
        //    {
        //        ErrorCollection.Add(FINColumnConstants.APP_QUALI_ID, VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Record "));

        //    }
        //}

        public static HR_APPLICANTS getClassEntity(string str_Id)
        {
            HR_APPLICANTS obj_HR_APPLICANTS = new HR_APPLICANTS();
            using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
            {
                obj_HR_APPLICANTS = userCtx.Find(r =>
                    (r.APP_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_APPLICANTS;
        }

        public static void fn_GetApplicantNM(ref DropDownList ddlList,bool bol_sel=true )
        {
            dtDropDownData = DBMethod.ExecuteQuery(Applicant_DAL.GetApplicantNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_FIRST_NAME", "APP_ID", dtDropDownData, bol_sel, !bol_sel);
        }

        public static void fn_GetApplicantNMEmployeeEntry(ref DropDownList ddlList,string str_Status, string  str_mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Applicant_DAL.GetApplicantNM4EmployeeEntry(str_Status, str_mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_FIRST_NAME", "APP_ID", dtDropDownData, true, false);
        }
    }
}
