﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


namespace FIN.BLL.HR
{
    public class EmployeeInsurance_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static INSURANCE_PLAN_ENTRY_HDR getClassEntity(string str_Id)
        {
            INSURANCE_PLAN_ENTRY_HDR obj_INSURANCE_PLAN_ENTRY_HDR = new INSURANCE_PLAN_ENTRY_HDR();
            using (IRepository<INSURANCE_PLAN_ENTRY_HDR> userCtx = new DataRepository<INSURANCE_PLAN_ENTRY_HDR>())
            {
                obj_INSURANCE_PLAN_ENTRY_HDR = userCtx.Find(r =>
                    (r.PLAN_ENTRY_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_INSURANCE_PLAN_ENTRY_HDR;
        }

        public static INSURANCE_PLAN_ENTRY_DTL getDetailClassEntity(string str_Id)
        {
            INSURANCE_PLAN_ENTRY_DTL obj_INSURANCE_PLAN_ENTRY_DTL = new INSURANCE_PLAN_ENTRY_DTL();
            using (IRepository<INSURANCE_PLAN_ENTRY_DTL> userCtx = new DataRepository<INSURANCE_PLAN_ENTRY_DTL>())
            {
                obj_INSURANCE_PLAN_ENTRY_DTL = userCtx.Find(r =>
                    (r.PLAN_ENTRY_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INSURANCE_PLAN_ENTRY_DTL;
        }

    }
}
