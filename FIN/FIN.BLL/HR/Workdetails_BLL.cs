﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class Workdetails_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
              
        # endregion

        public static HR_EMP_WORK_DTLS getEmpWorkDetails(string empId)
        {
            HR_EMP_WORK_DTLS obj_HR_EMP_WORK_DTLS = new HR_EMP_WORK_DTLS();
            using (IRepository<HR_EMP_WORK_DTLS> userCtx = new DataRepository<HR_EMP_WORK_DTLS>())
            {
                obj_HR_EMP_WORK_DTLS = userCtx.Find(r =>
                    (r.EMP_ID == (empId.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_EMP_WORK_DTLS;
        }
    }
}
