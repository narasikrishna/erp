﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class Jobs_BLL
    {

        DropDownList ddlList = new DropDownList();
        static  DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getJob4Category(ref DropDownList ddlList, string str_Category)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobCategory(str_Category)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.JOB_NAME, FINColumnConstants.JOB_ID, dtDropDownData, true, false);
        }
        public void fn_GetGradeName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGradeName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GRADE_DESC", FINColumnConstants.GRADE_ID, dtDropDownData, true, false);
        }

        public void fn_GetGrade(ref DropDownList ddlList,string str_Category_Id,string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Grades_DAL.GetGrade(str_Category_Id,mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GRADE_DESC", FINColumnConstants.GRADE_ID, dtDropDownData, true, false);
        }
        public static void fn_getJobName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.JOB_NAME, FINColumnConstants.JOB_ID, dtDropDownData, true, false);
        }
        public static void fn_getJob4Category_R(ref DropDownList ddlList, string str_Category)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobCategory(str_Category)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.JOB_NAME, FINColumnConstants.JOB_ID, dtDropDownData, false, true);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Getjobdtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getChildEntityDet4Category(string Cat_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Getjobdtls4Category(Cat_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getSectionEntityDet4Dept(string Dept_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.GetSectiondtls4Dept(Dept_ID)).Tables[0];
            return dt_Det;
        }
        public static DataSet GetJobOfferedReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.get_JobOfferedReport());
        }
    }
}
