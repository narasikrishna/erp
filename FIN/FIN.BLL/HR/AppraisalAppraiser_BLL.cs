﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.HR
{
    public class AppraisalAppraiser_BLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_GetAppraiser4Appraisal(ref DropDownList ddlList,string str_Appsal_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalAppraiser_DAL.GetAppraiser4Appraisal(str_Appsal_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_FIRST_NAME", "RA_HDR_ID", dtDropDownData, true, false);
        }
        public static void fn_GetAppraiser(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalAppraiser_DAL.GetAppraiser()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_FIRST_NAME", "RA_HDR_ID", dtDropDownData, true, false);
        }
        public static void fn_GetAppraisee4Appraiser(ref DropDownList ddlList, string str_Appraiser_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalAppraiser_DAL.GetAppraisee4Appraiser(str_Appraiser_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_FIRST_NAME", "EMP_ID", dtDropDownData, true, false);
        }

        # endregion

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static HR_PER_APP_REVIEW_ASGNMNT_DTL getClassEntity(string str_Id)
        {
            HR_PER_APP_REVIEW_ASGNMNT_DTL obj_HR_PER_APP_REVIEW_ASGNMNT_DTL = new HR_PER_APP_REVIEW_ASGNMNT_DTL();
            using (IRepository<HR_PER_APP_REVIEW_ASGNMNT_DTL> userCtx = new DataRepository<HR_PER_APP_REVIEW_ASGNMNT_DTL>())
            {
                obj_HR_PER_APP_REVIEW_ASGNMNT_DTL = userCtx.Find(r =>
                    (r.RA_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_PER_APP_REVIEW_ASGNMNT_DTL;
        }


        public static HR_PER_APP_REVIEW_ASGNMNT_DTL getDetailClassEntity(string str_Id)
        {
            HR_PER_APP_REVIEW_ASGNMNT_DTL obj_HR_PER_APP_REVIEW_ASGNMNT_DTL = new HR_PER_APP_REVIEW_ASGNMNT_DTL();
            using (IRepository<HR_PER_APP_REVIEW_ASGNMNT_DTL> userCtx = new DataRepository<HR_PER_APP_REVIEW_ASGNMNT_DTL>())
            {
                obj_HR_PER_APP_REVIEW_ASGNMNT_DTL = userCtx.Find(r =>
                    (r.RA_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_PER_APP_REVIEW_ASGNMNT_DTL;
        }



        public static String ValidateEntity(HR_PER_APP_REVIEW_ASGNMNT_DTL obj_HR_PER_APP_REVIEW_ASGNMNT_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            HR_PER_APP_REVIEW_ASGNMNT_DTL obj_HR_PER_APP_REVIEW_ASGNMNT_DTL = new HR_PER_APP_REVIEW_ASGNMNT_DTL();
            obj_HR_PER_APP_REVIEW_ASGNMNT_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<HR_PER_APP_REVIEW_ASGNMNT_DTL>(obj_HR_PER_APP_REVIEW_ASGNMNT_DTL);
            return str_Message;
        }

    }
}
