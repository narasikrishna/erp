﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class StaffLeaveDetails_BLL
    {
        public static HR_LEAVE_STAFF_DEFINTIONS getClassEntity(string str_Id)
        {
            HR_LEAVE_STAFF_DEFINTIONS obj_HR_LEAVE_STAFF_DEFINTIONS = new HR_LEAVE_STAFF_DEFINTIONS();
            using (IRepository<HR_LEAVE_STAFF_DEFINTIONS> userCtx = new DataRepository<HR_LEAVE_STAFF_DEFINTIONS>())
            {
                obj_HR_LEAVE_STAFF_DEFINTIONS = userCtx.Find(r =>
                    (r.LSD_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_LEAVE_STAFF_DEFINTIONS;
        }
    }
}
