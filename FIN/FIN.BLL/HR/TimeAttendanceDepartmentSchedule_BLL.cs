﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class TimeAttendanceDepartmentSchedule_BLL
   {

       DropDownList ddlList = new DropDownList();
       DataTable dtDropDownData = new DataTable();

       # region FillCombo
       public void fn_GetDepartmentDetails(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "DEPT_NAME", "DEPT_ID", dtDropDownData, true, false);
       }

       public void fn_GetSchedulecode(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TimeAttendanceSchedule_DAL.GetSchedulecode()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "TM_SCH_DESC", "TM_SCH_CODE", dtDropDownData, true, false);
       }

       # endregion

       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TimeAttendanceDepartmentSchedule_DAL.GetTMATNDdeptschdtls(Str_ID)).Tables[0];
           //DataTable dt_Det = new DataTable();
           return dt_Det;
       }



   }
}
