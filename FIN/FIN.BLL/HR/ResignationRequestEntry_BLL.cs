﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{

    public class ResignationRequestEntry_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        # region FillCombo
        
        public static void fn_getDepartment(ref DropDownList ddlList, string Dept_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ResignationRequestEntry_DAL.getEmployee_dept(Dept_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_FIRST_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        # endregion

    }
}
