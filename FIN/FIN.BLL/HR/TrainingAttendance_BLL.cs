﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class TrainingAttendance_BLL
   {
       DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

       # region FillCombo

       public static void fn_GetSchedule(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingAttendance_DAL.GetSchedule()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "TRN_DESC", "TRN_SCH_HDR_ID", dtDropDownData, true, false);
       }

       public static void fn_GetProgram(ref DropDownList ddlList, string ScheduleID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(TrainingAttendance_DAL.GetProgram(ScheduleID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PROG_DESC", "PROG_ID", dtDropDownData, true, false);
       }

       public static void fn_GetCourse(ref DropDownList ddlList, string ProgID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Program_DAL.GetCourse(ProgID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "COURSE_DESC", "COURSE_ID", dtDropDownData, true, false);
       }

       public static void fn_GetSubject(ref DropDownList ddlList, string courseID)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Course_DAL.GetSubjectBasedCourse(courseID)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "SUBJECT_DESC", "SUBJECT_ID", dtDropDownData, true, false);
       }

       # endregion

       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingAttendance_DAL.fn_GetTrnAttendanceDtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }

       public static HR_TRM_ATTEND_HDR getClassEntity(string str_Id)
       {
           HR_TRM_ATTEND_HDR obj_HR_TRM_ATTEND_HDR = new HR_TRM_ATTEND_HDR();
           using (IRepository<HR_TRM_ATTEND_HDR> userCtx = new DataRepository<HR_TRM_ATTEND_HDR>())
           {
               obj_HR_TRM_ATTEND_HDR = userCtx.Find(r =>
                   (r.ATT_HDR_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_HR_TRM_ATTEND_HDR;
       }
       public static String ValidateEntity(HR_TRM_ATTEND_HDR obj_HR_TRM_ATTEND_HDR)
       {
           string str_Error = "";
           return str_Error;
       }


   }
}
