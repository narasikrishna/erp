﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class LeaveDefinition_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static HR_LEAVE_DEFINITIONS getClassEntity(string str_Id)
        {
            HR_LEAVE_DEFINITIONS obj_HR_LEAVE_DEFINITIONS = new HR_LEAVE_DEFINITIONS();
            using (IRepository<HR_LEAVE_DEFINITIONS> userCtx = new DataRepository<HR_LEAVE_DEFINITIONS>())
            {
                obj_HR_LEAVE_DEFINITIONS = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_LEAVE_DEFINITIONS;
        }
        public static void GetLeave(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeave()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveType(ref DropDownList ddlList,string dept_id,string staff_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeaveType(dept_id, staff_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_TYPE, FINColumnConstants.LEAVE_TYPE, dtDropDownData, true, false);
        }
        public static void GetLeaveBasedFiscalYear(ref DropDownList ddlList, string fisYear)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeaveBasedFiscalYear(fisYear)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveBasedFiscalYear_FRLD(ref DropDownList ddlList, string fisYear)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeaveBasedFiscalYear_frLD(fisYear)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveBasedFiscalYearDept(ref DropDownList ddlList, string fisYear,string deptId,string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveBasedFiscalYearDept(fisYear, deptId, emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }
        public static void GetLeaveBasedDept(ref DropDownList ddlList,string deptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveBasedDept(deptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }
        public static void GetLeaveBasedFiscalYearDept_frSLD(ref DropDownList ddlList, string fisYear, string deptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveBasedFiscalYearDept_frSLD(fisYear, deptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        public static void GetLeaveBasedDept_frSLD(ref DropDownList ddlList, string deptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveBasedDept_frSLD(deptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LEAVE_DESC, FINColumnConstants.LEAVE_ID, dtDropDownData, true, false);
        }

        public static DataSet get_LeaveProvisionReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.getLeaveProvisionReportData());
        }
        public static DataSet get_AnnualleaveLedger()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.getAnnualLeaveLedger());
        }

    }
}
