﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
   public class PassportHandover_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        //public static void fn_HandoverType(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(PassportHandover_DAL.GetHandoverType()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "HO_TYPE", "HO_TYPE_ID", dtDropDownData, true, false);
        //}
        public static void GetHandoverTo(ref DropDownList ddlList,string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PassportHandover_DAL.GetHandoverTo(emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetPassportName(ref DropDownList ddlList,string EMP_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PassportHandover_DAL.GetPassportName(EMP_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PASSPORT_NAME","PASS_TXN_ID", dtDropDownData, true, false);
        }
        public static void GetReturnBy(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PassportHandover_DAL.GetReturnBy()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
    }
}
