﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class LoanRequest_BLL
    {
        static DataTable dtDropDownData = new DataTable();


        public static LN_LOAN_REQUEST_HDR getClassEntity(string str_Id)
        {
            LN_LOAN_REQUEST_HDR obj_LN_LOAN_REQUEST_HDR = new LN_LOAN_REQUEST_HDR();
            using (IRepository<LN_LOAN_REQUEST_HDR> userCtx = new DataRepository<LN_LOAN_REQUEST_HDR>())
            {
                obj_LN_LOAN_REQUEST_HDR = userCtx.Find(r =>
                    (r.LN_REQUEST_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_LN_LOAN_REQUEST_HDR;
        }

        public static LN_LOAN_REQUEST_DTL getDetailClassEntity(string str_Id)
        {
            LN_LOAN_REQUEST_DTL obj_LN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();
            using (IRepository<LN_LOAN_REQUEST_DTL> userCtx = new DataRepository<LN_LOAN_REQUEST_DTL>())
            {
                obj_LN_LOAN_REQUEST_DTL = userCtx.Find(r =>
                    (r.LN_CONTRACT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_LN_LOAN_REQUEST_DTL;
        }

        public static LN_LOAN_REQUEST_TERMS getWHClassEntity(string str_Id)
        {
            LN_LOAN_REQUEST_TERMS obj_LN_LOAN_REQUEST_TERMS = new LN_LOAN_REQUEST_TERMS();
            using (IRepository<LN_LOAN_REQUEST_TERMS> userCtx = new DataRepository<LN_LOAN_REQUEST_TERMS>())
            {
                obj_LN_LOAN_REQUEST_TERMS = userCtx.Find(r =>
                    (r.TERM_ID == str_Id)
                    ).SingleOrDefault();
            }

            return obj_LN_LOAN_REQUEST_TERMS;
        }

        # region FillCombo

        public static void fn_getRequestNumber(ref DropDownList ddlList, string REQ_EMP_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.getRequestNumber(REQ_EMP_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.REQ_ID, FINColumnConstants.REQ_ID, dtDropDownData, true, false);
        }

        public static void GetEmployeeName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }
        public static void GetLoanDetails(ref DropDownList ddlList, string loanType, string empid, string loanReqId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoanDetails(loanType, empid, loanReqId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "req_name","req_id", dtDropDownData, true, false);
        }

        public static void GetLookUpValues(ref DropDownList ddlList, string typeKeyId,string LOAN)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLookUpValues_Loan(typeKeyId,LOAN)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }

        public static void GetLoanPropertyDtls(ref DropDownList ddlList, string propertyTypeID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetPropertyName(propertyTypeID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "ln_property_id", dtDropDownData, true, false);
        }


        public static void GetLoanRequestContractNumber(ref DropDownList ddlList, string LN_REQ_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoanRequestDetails(LN_REQ_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_contract_num", "ln_contract_id", dtDropDownData, true, false);
            
        }

        public static void fn_GetLoanDetails4PropertyId(ref DropDownList ddlList, string str_Propty_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoanDetails4PropertyId(str_Propty_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_contract_num", "ln_contract_id", dtDropDownData, true, false);
            
        }

        

        # endregion

    }
}
