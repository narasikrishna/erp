﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class RepayPlan_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static DataTable getRepayPlanDetailsDetails(string StrID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.RepayPlan_DAL.getRepayPlanDetailsDetails(StrID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getRepayPlanDetailsDetails_edit(string StrID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.RepayPlan_DAL.getRepayPlanDetailsDetails_Edit(StrID)).Tables[0];
            return dt_Det;
        }
        # region FillCombo


        public static void fn_LoanReqId(ref DropDownList ddlList, string emp_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(RepayPlan_DAL.GetLoanReqId(emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "REQ_ID", "REQ_ID", dtDropDownData, true, false);
        }
        #endregion

    }
}
