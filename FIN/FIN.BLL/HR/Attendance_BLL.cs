﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.HR
{
    public class Attendance_BLL
    {
        public static DataTable fngetEnrolledEmployee(string str_EnrolledId)
        {
            DataTable dt_data = DBMethod.ExecuteQuery(Attendance_DAL.fngetEnrolledEmployee(str_EnrolledId)).Tables[0];
            return dt_data;
        }
    }
}
