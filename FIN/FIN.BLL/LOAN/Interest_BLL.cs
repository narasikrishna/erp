﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.LOAN;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.LOAN
{
   public   class Interest_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getInterestDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Interest_DAL.getInterestDetails(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataSet GetFinancialDtlsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.get_FinancialDetailsReport());
        }
        public static DataSet GetAccruedExpenseReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.get_AccruedExpenseReport());
        }
        public static DataSet GetPaidInstallmentReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.get_PaidInstallmentReport());
        }
    }
}
