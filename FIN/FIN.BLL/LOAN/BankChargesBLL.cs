﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;


namespace FIN.BLL.LOAN
{
    public class BankChargesBLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetPropertyID(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.BankChargesDAL.getProperty()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "ln_property_id", dtDropDownData, true, false);
        }
        public static void GetPropertyName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.BankChargesDAL.getProperty()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "ln_property_id", dtDropDownData, false, true);
        }
        public static void GetContract(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.BankChargesDAL.getContractNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_contract_num", "ln_contract_id", dtDropDownData, true, false);
        }
        public static LN_BANK_CHARGES getClassEntity(string str_Id)
        {
            LN_BANK_CHARGES obj_LN_BANK_CHARGES = new LN_BANK_CHARGES();
            using (IRepository<LN_BANK_CHARGES> userCtx = new DataRepository<LN_BANK_CHARGES>())
            {
                obj_LN_BANK_CHARGES = userCtx.Find(r =>
                    (r.BK_CONTRACT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_LN_BANK_CHARGES;
        }
      
    }
}
