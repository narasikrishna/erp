﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.LOAN;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.LOAN
{
    public class Contract_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        DropDownList ddlList = new DropDownList();

        public static void fn_GetLoanRequest(ref DropDownList ddlList, string str_Property_id, string str_Facility_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumber4PropFacility(str_Property_id, str_Facility_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, true, false);
        }

        public static void fn_getContractNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, true, false);
        }
        public static void fn_getContractNumbers(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }
        public static void fn_getContractNumbersByFacility(ref DropDownList ddlList,string Facility)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumberByFacilty(Facility)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }
        public static void fn_getContractNumbersByProperty(ref DropDownList ddlList, string Property)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumberByProperty(Property)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }

        public static void fn_getContractNumbersByBank(ref DropDownList ddlList, string Bank)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumberByBank(Bank)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }
        public static void fn_getContractNumbersByBankFacility(ref DropDownList ddlList, string facility, string Bank)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumberByBankFacility(facility,Bank)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }
        public static void fn_getContractNumbersByAll(ref DropDownList ddlList,string Facility,string Property,string Bank)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractNumberByAll(Facility,Property, Bank)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_CONTRACT_NUM", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }

        public static void fn_getContractType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getContractType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CONTRACT_NUM_TYPE", "LN_CONTRACT_ID", dtDropDownData, false, true);
        }

        public static void fn_getDebitCreditCostCentre(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ContractDetails_DAL.getDebitCreditCostCentreDtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "segment_value", "segment_value_id", dtDropDownData, true, false);
        }


    }
}
