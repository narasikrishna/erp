﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.LOAN
{
    public class PropertyRevaluationBLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getRevalPropDetails(string Master_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.LOAN.PropertyRevalDAL.getRevalPropDetails(Master_id)).Tables[0];
            return dt_Det; 
        }

        public static void GetPropertyID(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.PropertyRevalDAL.getProperty()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList,"ln_property_name", "ln_property_id", dtDropDownData, true, false);
        }
        public static void GetRequestID(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.PropertyRevalDAL.getProperty()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "LN_REQUEST_ID", dtDropDownData, true, false);
        }
        public static LN_PROPERTY_REVALUATION_HDR getClassEntity(string str_Id)
        {
            LN_PROPERTY_REVALUATION_HDR obj_LN_PROPERTY_REVALUATION_HDR = new LN_PROPERTY_REVALUATION_HDR();
            using (IRepository<LN_PROPERTY_REVALUATION_HDR> userCtx = new DataRepository<LN_PROPERTY_REVALUATION_HDR>())
            {
                obj_LN_PROPERTY_REVALUATION_HDR = userCtx.Find(r =>
                    (r.REVAL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_LN_PROPERTY_REVALUATION_HDR;
        }
    }
}
