﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.LOAN
{
    public class PropertyPledgeBLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetPropertyPledge(ref DropDownList ddlList, string  Bank_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.PropertyPledgeDAL.getProperty(Bank_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "ln_property_id", dtDropDownData, true, false);
        }
    }
}
