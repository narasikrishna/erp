﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.LOAN;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.LOAN
{

    public class Facility_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static void fn_GetFacilityName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Facility_DAL.getFacilityName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_FACILITY_NAME", "LN_FACILITY_ID", dtDropDownData, true, false);
        }
        public static void fn_GetFacilityNames(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Facility_DAL.getFacilityName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_FACILITY_NAME", "LN_FACILITY_ID", dtDropDownData, false, true);
        }
        public static void fn_getPorpertyname4Facility(ref DropDownList ddlList,string str_facility_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Facility_DAL.getPorpertyname4Facility(str_facility_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_PROPERTY_NAME", "LN_PROPERTY_ID", dtDropDownData, true, false);
        }

        public static void fn_GetLoanProperty(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Facility_DAL.getLoanPropertyDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ln_property_name", "ln_property_id", dtDropDownData, true, false);
        }
    }
}
