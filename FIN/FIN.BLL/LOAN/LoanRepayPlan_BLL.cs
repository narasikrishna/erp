﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.LOAN;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.LOAN
{
    public class LoanRepayPlan_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public void fn_GetLoanDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRepayPlan_DAL.GetLoanDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_REQUEST_DESC", "LN_LOAN_ID", dtDropDownData, true, false);
        }
        public void fn_GetInstallmentDtls(ref DropDownList ddlList, string LOAN_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanRepayPlan_DAL.GetInstallmentDetails(LOAN_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_INSTALLMENT_NO", "LN_LOAN_DTL_ID", dtDropDownData, true, false);
        }

        #endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanRepayPlan_DAL.GetLoanRepayDetails(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static DataTable fn_GetLoanRepayDtls4Contract(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.InventoryPriceList_DAL.GetLoanRepayDtls4Contract(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getLoanRepayPlanDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.LOAN.PrincipalRepayPlan_DAL.GetLoanRepayDtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static LN_REPAY_DTL getClassEntity(string str_Id)
        {
            LN_REPAY_DTL obj_LN_REPAY_DTL = new LN_REPAY_DTL();
            using (IRepository<LN_REPAY_DTL> userCtx = new DataRepository<LN_REPAY_DTL>())
            {
                obj_LN_REPAY_DTL = userCtx.Find(r =>
                    (r.LN_PRN_REPAY_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_LN_REPAY_DTL;
        }
    }
}
