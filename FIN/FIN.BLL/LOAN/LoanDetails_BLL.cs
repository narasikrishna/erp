﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.LOAN;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.LOAN
{
   public class LoanDetails_BLL
    {
       static DataTable dtDropDownData = new DataTable();

        DropDownList ddlList = new DropDownList();
       

        # region FillCombo

        public static void fn_GetLoanRequest(ref DropDownList ddlList, string str_mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(LoanDetails_DAL.GetLoanRequest(str_mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LN_REQUEST_DESC", "LN_REQUEST_ID", dtDropDownData, true, false);
        }

        # endregion




        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanDetails_DAL.GetLoanDetails(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static LN_APPROVED_LOAN_HDR getClassEntity(string str_Id)
        {
            LN_APPROVED_LOAN_HDR obj_LN_APPROVED_LOAN_HDR = new LN_APPROVED_LOAN_HDR();
            using (IRepository<LN_APPROVED_LOAN_HDR> userCtx = new DataRepository<LN_APPROVED_LOAN_HDR>())
            {
                obj_LN_APPROVED_LOAN_HDR = userCtx.Find(r =>
                    (r.LN_LOAN_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_LN_APPROVED_LOAN_HDR;
        }
        public static String ValidateEntity(LN_APPROVED_LOAN_HDR obj_LN_APPROVED_LOAN_HDR)
        {
            string str_Error = "";
            return str_Error;
        }


      

    }
}
