﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL
{
    #region "Query String Parameters "
    public enum QueryStringTags : short
    {
        ProgramID = 1,
        ID = 2,
        Mode = 3,
        AddFlag = 4,
        ReportName = 5,
        UpdateFlag = 6,
        DeleteFlag = 7,
        QueryFlag=8,
        ReportName_OL = 9
    }
    #endregion

    #region "Application Title "
    public enum ApplicationTitle
    {
        FIN
    }
    #endregion

    #region "Global Parameters"
    public enum GlobalParams
    {
        GlobalFieldSeperator,
        GlobalTieldSeperator,
        GlobalSylSeperator
    }

    #endregion

    #region "ApplicationMessages "
    public enum ApplicationMessages
    {
        Saved,
        Updated,
        NotSaved,
        Deleted,
        NotDeleted,
        CannotBeDeleted,
        ErrorMessages,
        CannotBeModified
    }
    #endregion




    #region "Row Types "
    public enum RowType
    {
        None,
        ALL
    }
    #endregion

    #region "Program parameters "
    public enum ProgramParameters
    {
        ProgramID,
        ProgramName,
        AccessibleName,
        AddURL,
        ModifyURL,
        DeleteURL,
        ListURL,
        HelpPage,
        HeaderName,
        ReportName,
        AddFlag,
        ApproveURL,
        FormCode

    }
    #endregion

    #region "ControlTypes "
    public enum controlType
    {
        ASPxTextBox,
        ASPxButton,
        ASPxRadioButtonList,
        ASPxMemo,
        ASPxComboBox,
        ASPxCheckBox,
        ASPxGridView,
        ASPxDateEdit,
        ASPxLabel
    }
    #endregion

    #region "View State "
    public enum ViewStateParam
    {
        ProgramID,
        KeyName,
        ProgramName,
        gridData,
        customGriddata
    }
    #endregion



    #region "Menu buttons "
    public enum menuButtons
    {
        btnNew,
        btnPDF,
        btnExcel,
        btnCSV,
        btnTxt,
        btnCustom,
        btnHelp,
        btnFilter
    }
    #endregion

    #region "Program Modes "
    public enum ProgramMode
    {
        Add,
        Update,
        Delete,
        Read,
        WApprove

    }
    #endregion

    #region "Yes No "
    public enum ConditionalYesNo : short
    {
        Yes = 1,
        No = 0
    }
    #endregion



    #region "Customise Grid Menu Buttons "
    public enum menuButtonsCustomiseGrid
    {
        btnSelectAll,
        btnUnselectAll,
        btnApply
    }
    #endregion

    #region "Messages "
    public enum aMessages
    {
        Blank,
        //can't be blank
        Duplicate,
        //already defined
        notDefined,
        //not defined
        InvalidData,
        //Not exists
        Save,
        //Record saved successfuly
        SaveError,
        //Record not saved
        Delete,
        //Record deleted successfuly
        DeleteError,
        //Record can't inactivated since it is in use
        InputDataError,
        //Required information not supplied Correctly
        ZeroDetails,
        //Details Information must be Provided
        MessageSaved,
        //Message Saved Successfully
        AlreadyExists,
        //already exists
        LessThan,
        //should be less than current value
        SpaceNotAllowed,
        //Space Not Allowed
        DeleteQuestion,
        //Delete Question          
        RecordInactivated,
        //Record inactivated successfully
        RecordInuse
        //Record can't be inactivated since it is in use
    }
    #endregion

    #region "SelectUnSelectOptions"
    public enum SelectUnSelectOptions
    {
        SelectAll,
        UnselectAll
    }
    #endregion

    #region "Enum : Grid Columns "
    public enum GridColumns
    {
        COLUMN_ID,
        COLUMN_NM,
        COLUMN_VISIBLE,
        COLUMN_WIDTH,
        COLUMN_ALIGNMENT,
        COLUMN_DISPLAY_FORMAT,
        COLUMN_GRP_FLAG,
        LIST_COLUMN_NAME,
        LIST_DTL_ID,
        DISPLAY_COLUMN_NM

    }
    #endregion



    #region "Flags "

    public enum RecordStatus : short
    {
        Active = 1,
        Inactive = 0
    }

    public enum Applicability : short
    {
        Applicable = 1,
        NotApplicable = 0
    }

    public enum Availability
    {
        Available = 1,
        NotAvailable = 0
    }





    public enum Gender
    {
        Male,
        Female
    }


    public enum CommunicationWay
    {
        Email,
        Fax,
        Post
    }



    public enum Months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }


    public enum ChartFlag
    {
        ByColumn = 1,
        ByRow = 2
    }


    #endregion
}
