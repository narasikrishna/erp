﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


namespace FIN.BLL
{
    public class Lookup_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetLookUpValues(ref DropDownList ddlList, string typeKeyId,Boolean bol_ALL=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, !bol_ALL, bol_ALL);
        }
        public static void GetLookUpValue(ref DropDownList ddlList, string typeKeyId, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, bol_ALL, !bol_ALL);
        }

        public static void GetReimbLookUpValues(ref DropDownList ddlList, string typeKeyId, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetReimbLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, bol_ALL, !bol_ALL);
        }

        public static void GetGLSourceLookUpValues(ref DropDownList ddlList, string typeKeyId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetGLSourceLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }
        public static void GetGLJTLookUpValues(ref DropDownList ddlList, string typeKeyId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetGLJTLookUpValues(typeKeyId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }
        public static void GetModuleName(ref DropDownList ddlList,string str_lng)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FINSQL.getModuelName(str_lng)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MASTER_CODE, FINColumnConstants.MASTER_CODE, dtDropDownData, true, false);
        }
        public static void GetModuleNam(ref DropDownList ddlList, string str_lng)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FINSQL.getModuelNam(str_lng)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MASTER_CODE, FINColumnConstants.MASTER_CODE, dtDropDownData, true, false);
        }
        public static void GetModuleName_R(ref DropDownList ddlList, string str_lng)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FINSQL.getModuelNam(str_lng)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MASTER_CODE, FINColumnConstants.MASTER_CODE, dtDropDownData, false, true);
        }
        public static void GetMenuname(ref DropDownList ddlList,string str_moduleName,string str_lng)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FINSQL.getMenuName(str_moduleName, str_lng)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SCREEN_NAME, FINColumnConstants.SCREEN_CODE, dtDropDownData, true, false);
        }

    }
}
