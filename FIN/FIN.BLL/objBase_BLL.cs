using System;
using System.Data;
using VMVServices.Services.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL
{
    /// <summary>
    /// Summary description for objBase.
    /// </summary>
    public abstract class objBase_BLL : VMVServices.Services.Business.BusinessObjectBase
    {
        public objBase_BLL() { }

     
        protected override void PreAddAction(long auditUserID)
        {
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
            //this.SysDateLastUpdated = DateTime.Now;//.ToString(iThink.Settings.Instance.DefaultDateTimeFormatString);

            base.PreAddAction(auditUserID);
        }

        protected override void PreUpdateAction(long auditUserID)
        {
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
            //           this.SysDateLastUpdated = DateTime.Now;//.ToString(iThink.Settings.Instance.DefaultDateTimeFormatString);
            base.PreUpdateAction(auditUserID);
        }


        string _SysDateCreated = "";
        //[SqlField(IsAuditProperty = true, DbType = System.Data.DbType.DateTime)]
        public virtual string SysDateCreated { get { return _SysDateCreated; } set { _SysDateCreated = value; } }


        string _SysDateLastUpdated = "";
        //[SqlField(DbType = System.Data.DbType.DateTime)]
        public virtual string SysDateLastUpdated { get { return _SysDateLastUpdated; } set { _SysDateLastUpdated = value; } }

        //[SqlField()]
        public virtual long SysAuditUserID { get { return this.auditUserID; } set { this.auditUserID = value; } }


        //[SqlField(IsAuditProperty=true)]public virtual DateTime SysAuditDate { get{return this.auditDate;} set{this.auditDate = value;} }
        //[SqlField(IsAuditProperty=true)]public virtual string SysAuditAction { get{return this.auditAction;} set{this.auditAction = value;} }

        public void CreateData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getInsertCmd(obj, true));
        }
        public void ModifyData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getUpdateCmd(obj));
        }
        public void DeleteData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getDeleteCmd(obj));
        }
        public long GetNextIDValue(object obj,string columnName)
        {
            long nextID = 0;

            nextID = SelectNextIdValue(obj, columnName);
            return nextID;
        }
        public bool IsExistValue(string objName, string whereClause)
        {
            int idValue = 0;
            bool returnValue = false;

            DataTable dtValue = new DataTable();

            dtValue = SelectAllWithCondition(objName, whereClause);
            if (dtValue != null)
            {
                if (dtValue.Rows.Count > 0)
                {
                    idValue = int.Parse(dtValue.Rows[0][dtValue.Columns[0]].ToString());
                }
            }
            if (idValue > 0)
            {
                returnValue = true;
            }
            else
            {
                returnValue = false;
            }
            return returnValue;
        }
   
    }
}
