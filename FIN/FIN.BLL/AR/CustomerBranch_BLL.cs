﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
   public class Customer_Branch_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetCustomerBranch(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CustomerBranch_DAL.GetCustomerBranch()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_BRANCH_CODE", "VENDOR_LOC_ID", dtDropDownData, !includeAll, includeAll);
        }
        public static void GetCustomerBranch(ref DropDownList ddlList, string customerNo, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CustomerBranch_DAL.GetCustomerBranch(customerNo)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_BRANCH_CODE", "VENDOR_LOC_ID", dtDropDownData, !includeAll, includeAll);
        }

        public static void GetCustomerNameFromBranch(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(CustomerBranch_DAL.GetCustomerNameFromBranch()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VEND_NAME", "VENDOR_ID", dtDropDownData, !includeAll, includeAll);
        }

    }
}
