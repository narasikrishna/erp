﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
   public  class Customer_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

       public static void GetCustomerName(ref DropDownList ddlList, bool includeAll=true)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Customer_DAL.GetCustomerName()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
       }
       public static void GetCustomerrNumber(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Customer_DAL.GetCustomerName()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_CODE, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
       }
       public static DataSet GetVendorReportData()
       {
           return DBMethod.ExecuteQuery(Customer_DAL.getVendorReportData());
       }

    }
}
