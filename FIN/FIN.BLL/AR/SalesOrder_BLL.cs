﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.AR
{
    public static class SalesOrder_BLL
    {
        public static DataTable getSalesOrderDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getSalesOrderDetails(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static DataTable getItemUnitPriceDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getItemUnitPriceDetails(Str_ID)).Tables[0];
            return dt_Det;
        }


        # region FillCombo
        public static void fn_getAccountGroupName(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountGroupName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        public static void fn_getCurrency(ref DropDownList ddlList, string quote_hdr_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getCurrency(quote_hdr_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_DESC, FINColumnConstants.CURRENCY_ID, dt_Det, true, false);
        }

        public static void GetSaleOrderDetails(ref DropDownList ddlList, string orderId = "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.GetSaleOrderDetails(orderId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.OM_ORDER_NUM, FINColumnConstants.OM_ORDER_ID, dt_Det, true, false);
        }


        public static void fn_GetOrderNo(ref DropDownList ddlList, string orderId = "", bool includeAll = false)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.GetOrderNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.OM_ORDER_NUM, FINColumnConstants.OM_ORDER_ID, dt_Det, !includeAll, includeAll);
        }

        public static void fn_getOrderNo(ref DropDownList ddlList, string ven_loc_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getOrderNo(ven_loc_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.OM_ORDER_NUM, FINColumnConstants.OM_ORDER_ID, dt_Det, true, false);
        }

        public static void fn_GetSalesOrderNo(ref DropDownList ddlList, string orderId = "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.GetOrderNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.OM_ORDER_NUM, FINColumnConstants.OM_ORDER_ID, dt_Det, false, true);
        }




        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static OM_CUSTOMER_ORDER_HDR getClassEntity(string str_Id)
        {
            OM_CUSTOMER_ORDER_HDR obj_OM_CUSTOMER_ORDER_HDR = new OM_CUSTOMER_ORDER_HDR();
            using (IRepository<OM_CUSTOMER_ORDER_HDR> userCtx = new DataRepository<OM_CUSTOMER_ORDER_HDR>())
            {
                obj_OM_CUSTOMER_ORDER_HDR = userCtx.Find(r =>
                    (r.OM_ORDER_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_OM_CUSTOMER_ORDER_HDR;
        }


        public static OM_CUSTOMER_ORDER_DTL getDetailClassEntity(string str_Id)
        {
            OM_CUSTOMER_ORDER_DTL obj_OM_CUSTOMER_ORDER_DTL = new OM_CUSTOMER_ORDER_DTL();
            using (IRepository<OM_CUSTOMER_ORDER_DTL> userCtx = new DataRepository<OM_CUSTOMER_ORDER_DTL>())
            {
                obj_OM_CUSTOMER_ORDER_DTL = userCtx.Find(r =>
                    (r.OM_ORDER_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_CUSTOMER_ORDER_DTL;
        }



        public static String ValidateEntity(OM_CUSTOMER_ORDER_DTL obj_OM_CUSTOMER_ORDER_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            OM_CUSTOMER_ORDER_DTL obj_OM_CUSTOMER_ORDER_DTL = new OM_CUSTOMER_ORDER_DTL();
            obj_OM_CUSTOMER_ORDER_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<OM_CUSTOMER_ORDER_DTL>(obj_OM_CUSTOMER_ORDER_DTL);
            return str_Message;
        }
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getReportData());
        }

        public static void fn_GetReceiptNo(ref DropDownList ddlList, string orderId = "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.GetReceiptNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CUST_PAY_ID", "CUST_PAY_ID", dt_Det, false, true);
        }

        public static DataSet GetReceiptReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.getReceiptReportData());
        }
    }
}
