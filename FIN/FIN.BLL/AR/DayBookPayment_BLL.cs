﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
    public class DayBookPayment_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.DayBookPayment_DAL.getReportData());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.DayBookPayment_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_ID", dtDropDownData, false, true);
        }

    }
}
