﻿using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AR
{
   public class CustomeStatementofAccounts_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetCustomerStatementOfAccountReportData()
        {
          return DBMethod.ExecuteQuery(FIN.DAL.AR.CustomerStatementofAccount_DAL.getCustomerStatementofAccounts());
        }
        public static void GetVendorNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierStatementOfAccount_DAL.getVendorNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_ID", "VENDOR_ID", dtDropDownData, false, true);
        }
    }
}