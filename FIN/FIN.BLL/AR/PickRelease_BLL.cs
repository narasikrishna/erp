﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.AR
{
   public class PickRelease_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static OM_PICK_RELEASE_HDR getClassEntity(string str_Id)
        {
            OM_PICK_RELEASE_HDR obj_OM_PICK_RELEASE_HDR = new OM_PICK_RELEASE_HDR();
            using (IRepository<OM_PICK_RELEASE_HDR> userCtx = new DataRepository<OM_PICK_RELEASE_HDR>())
            {
                obj_OM_PICK_RELEASE_HDR = userCtx.Find(r =>
                    (r.OM_PR_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_PICK_RELEASE_HDR;
        }

        public static OM_PICK_RELEASE_DTL getDetailClassEntity(string str_Id)
        {
            OM_PICK_RELEASE_DTL obj_OM_PICK_RELEASE_DTL = new OM_PICK_RELEASE_DTL();
            using (IRepository<OM_PICK_RELEASE_DTL> userCtx = new DataRepository<OM_PICK_RELEASE_DTL>())
            {
                obj_OM_PICK_RELEASE_DTL = userCtx.Find(r =>
                    (r.OM_PR_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_PICK_RELEASE_DTL;
        }

        # region FillCombo
        public static void fn_getLotNo(ref DropDownList ddlList, string item_id)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.PickRelease_DAL.getLotNo(item_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", FINColumnConstants.LOT_ID, dt_Det, true, false);
        }

      
        # endregion


    }
}
