﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
    public class Invoice_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void getInvoice(ref DropDownList ddlList)
        {
            using (IRepository<OM_CUST_INVOICE_HDR> userCtx = new DataRepository<OM_CUST_INVOICE_HDR>())
            {
                CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.OM_INV_NUM, FINColumnConstants.OM_INV_ID, userCtx.Find(r =>
                    (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1")
                    ).OrderBy(r => r.OM_INV_ID), true, false);
            }
        }
        public static OM_CUST_INVOICE_HDR getClassEntity(string str_Id)
        {
            OM_CUST_INVOICE_HDR obj_OM_CUST_INVOICE_HDR = new OM_CUST_INVOICE_HDR();
            using (IRepository<OM_CUST_INVOICE_HDR> userCtx = new DataRepository<OM_CUST_INVOICE_HDR>())
            {
                obj_OM_CUST_INVOICE_HDR = userCtx.Find(r =>
                    (r.OM_INV_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_CUST_INVOICE_HDR;
        }
        public static OM_CUST_INVOICE_DTL getDetailClassEntity(string str_Id)
        {
            OM_CUST_INVOICE_DTL obj_OM_CUST_INVOICE_DTL = new OM_CUST_INVOICE_DTL();
            using (IRepository<OM_CUST_INVOICE_DTL> userCtx = new DataRepository<OM_CUST_INVOICE_DTL>())
            {
                obj_OM_CUST_INVOICE_DTL = userCtx.Find(r =>
                    (r.OM_CUST_INV_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_CUST_INVOICE_DTL;
        }

        public static void fn_getInvoice4Perpayment(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoice4Perpayment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, true, false);
        }
        public static void fn_getInvoiceNumber(ref DropDownList ddlList ,bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, !includeAll, includeAll);
        }


        public static void fn_getInvoiceNumber_forcustomer(ref DropDownList ddlList, string OM_VENDOR_ID="", bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumber_forCustomer(OM_VENDOR_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_NUM", "OM_INV_ID", dtDropDownData, !includeAll, includeAll);
        }
        
        public static void fn_getInvoiceNumber4CustomerPayemnt(ref DropDownList ddlList, string OM_VENDOR_ID, string str_InvCur, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumber4CustomerPayemnt(OM_VENDOR_ID,str_InvCur)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_NUM", "OM_INV_ID", dtDropDownData, !includeAll, includeAll);
        }
        public static void getInvoice4Payemntt(ref DropDownList ddlList, string OM_VENDOR_ID, string MODE, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoice4Payemnt(OM_VENDOR_ID, MODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_NUM", "OM_INV_ID", dtDropDownData, !includeAll, includeAll);
        }

        public static void getInvoiceForPayment(ref DropDownList ddlList, string OM_VENDOR_ID, string MODE, string currId, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceForPayment(OM_VENDOR_ID, MODE, currId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_NUM", "OM_INV_ID", dtDropDownData, !includeAll, includeAll);
        }

        public static DataSet GetInvoiceListReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getInvoiceListReportData());
        }


        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getReportData());
        }
        public static DataSet GetGrossProfitMarginReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getGrossProfitMarginReportData());
        }

        public static DataSet GetDayBookRemittanceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getDayBookRemittanceReportData());
        }

        public static DataSet GetReportDayBookInvoice()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getDayBookInvoiceReportData());
        }

        public static DataSet GetReportDayBookReceipt()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getDayBookReceiptReportData());
        }

        public static DataSet GetReportInvStoresLedger()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getInvStoresLedgerReportData());
        }

    }
}
