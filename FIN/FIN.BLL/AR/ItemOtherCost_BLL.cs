﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;
namespace FIN.BLL.AR
{
   public class ItemOtherCost_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static OM_DC_OTHER_COST_HDR getClassEntity(string str_Id)
        {
            OM_DC_OTHER_COST_HDR obj_OM_DC_OTHER_COST_HDR = new OM_DC_OTHER_COST_HDR();
            using (IRepository<OM_DC_OTHER_COST_HDR> userCtx = new DataRepository<OM_DC_OTHER_COST_HDR>())
            {
                obj_OM_DC_OTHER_COST_HDR = userCtx.Find(r =>
                    (r.OM_OTHER_COST_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_DC_OTHER_COST_HDR;
        }

        public static OM_DC_OTHER_COST_DTL getDetailClassEntity(string str_Id)
        {
            OM_DC_OTHER_COST_DTL obj_OM_DC_OTHER_COST_DTL = new OM_DC_OTHER_COST_DTL();
            using (IRepository<OM_DC_OTHER_COST_DTL> userCtx = new DataRepository<OM_DC_OTHER_COST_DTL>())
            {
                obj_OM_DC_OTHER_COST_DTL = userCtx.Find(r =>
                    (r.OM_OTHER_COST_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_DC_OTHER_COST_DTL;
        }

       


    }
}
