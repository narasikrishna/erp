﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
  public  class RPTItemWiseSalesOrderBLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ItemWiseSalesOrder_DAL.getReportData());
        }
        public static void GetItemName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.ItemWiseSalesOrder_DAL.getItemName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ITEM_NAME", "ITEM_ID", dtDropDownData, false, true);
        }
    }
}
