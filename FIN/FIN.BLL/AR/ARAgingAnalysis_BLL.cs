﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
    public class ARAgingAnalysis_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getReportData());
        }

        public static DataSet GetInvoiceListReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getInvoiceListReportData());
        }

        public static DataSet GetCheckListReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getCheckListReportData());
        }

        public static DataSet GetCollectedReceivablesBalanceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getCollectedReceivablesBalanceReportData());
        }

        public static DataSet GetReceivablesSystemReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getReceivablesSystemReportData());
        }

        public static DataSet GetChequesReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getChequesReportData());
        }

        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.ARAgingAnalysis_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_VALUE_ID", dtDropDownData, false, true);
        }

    }
}
