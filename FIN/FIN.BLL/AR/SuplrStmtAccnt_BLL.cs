﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
    public class SuplrStmtAccnt_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.SuplrStmtAccnt_DAL.getReportData());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.SuplrStmtAccnt_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_ID", dtDropDownData, false, true);
        }

        public static void GetVendorName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.SuplrStmtAccnt_DAL.getVendorDtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_ID", "VENDOR_ID", dtDropDownData, false, true);
        }

        public static void GetSupplierName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.SuplrStmtAccnt_DAL.getSupplierName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SUPPLIER_NAME", "SUPPLIER_NAME", dtDropDownData, false, true);
        }

    }
}
