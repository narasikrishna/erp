﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AR
{
   public class DeliveryChallan_BLL
    {
       
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo

        # endregion

        public static OM_DC_HDR getClassEntity(string str_Id)
        {
            OM_DC_HDR obj_OM_DC_HDR = new OM_DC_HDR();
            using (IRepository<OM_DC_HDR> userCtx = new DataRepository<OM_DC_HDR>())
            {
                obj_OM_DC_HDR = userCtx.Find(r =>
                    (r.OM_DC_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_DC_HDR;
        }

        public static OM_DC_DTL getDetailClassEntity(string str_Id)
        {
            OM_DC_DTL obj_OM_DC_DTL = new OM_DC_DTL();
            using (IRepository<OM_DC_DTL> userCtx = new DataRepository<OM_DC_DTL>())
            {
                obj_OM_DC_DTL = userCtx.Find(r =>
                    (r.OM_DC_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_DC_DTL;
        }



        # region FillCombo
        public static void GetOrderNo(ref DropDownList ddlList)
        {
            using (IRepository<OM_CUSTOMER_ORDER_HDR> userCtx = new DataRepository<OM_CUSTOMER_ORDER_HDR>())
            {
                CommonUtils.LoadDropDownList(ddlList, "OM_ORDER_NUM", FINColumnConstants.OM_ORDER_ID, userCtx.Find(x => x.ENABLED_FLAG == "1" && x.WORKFLOW_COMPLETION_STATUS == "1").OrderBy(x => x.OM_ORDER_NUM).ThenBy(x => x.OM_ORDER_NUM), true, false);
            }
        }
      
        public static void GetDCNo(ref DropDownList ddlList)
        {
            using (IRepository<OM_DC_HDR> userCtx = new DataRepository<OM_DC_HDR>())
            {
                CommonUtils.LoadDropDownList(ddlList, "OM_DC_ID", "OM_DC_ID", userCtx.Find(x => x.ENABLED_FLAG == "1" && x.WORKFLOW_COMPLETION_STATUS == "1").OrderBy(x => x.OM_DC_ID).ThenBy(x => x.OM_DC_ID), true, false);
            }
        }


        public static void fn_GetDCLineNo(ref DropDownList ddlList, string OM_DC_ID)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.DeliveryChallan_DAL.GetDCLineNo(OM_DC_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_DC_LINE_NUM", "OM_DC_LINE_NUM", dt_Det, true, false);
        }

        public static void GetDCLineNo_fr_PR(ref DropDownList ddlList, string OM_DC_ID)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.DeliveryChallan_DAL.GetDCLineNo_fr_PR(OM_DC_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_DC_LINE_NUM_DESC", "OM_DC_LINE_NUM", dt_Det, true, false);
        }


        # endregion

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.DeliveryChallan_DAL.getReportData());
        }

        public static DataSet GetDeliveryChellanReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.DeliveryChallan_DAL.getDeliveryChellanReportData());
        }
        public static DataSet GetItemwiseDailyCollectionReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.DeliveryChallan_DAL.getItemwiseDailyCollectionReportData());
        }
    
    }
}
