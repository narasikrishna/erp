﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;


namespace FIN.BLL.AR
{
    public class SalesPayment_BLL
    {

        public static OM_CUST_PAYMENT_HDR getClassEntity(string str_Id)
        {
            OM_CUST_PAYMENT_HDR obj_OM_CUST_PAYMENT_HDR = new OM_CUST_PAYMENT_HDR();
            using (IRepository<OM_CUST_PAYMENT_HDR> userCtx = new DataRepository<OM_CUST_PAYMENT_HDR>())
            {
                obj_OM_CUST_PAYMENT_HDR = userCtx.Find(r =>
                    (r.CUST_PAY_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_OM_CUST_PAYMENT_HDR;
        }
        public static OM_CUST_PAYMENT_DTL getDetailClassEntity(string str_Id)
        {
            OM_CUST_PAYMENT_DTL obj_OM_CUST_PAYMENT_DTL = new OM_CUST_PAYMENT_DTL();
            using (IRepository<OM_CUST_PAYMENT_DTL> userCtx = new DataRepository<OM_CUST_PAYMENT_DTL>())
            {
                obj_OM_CUST_PAYMENT_DTL = userCtx.Find(r =>
                    (r.CUST_PAY_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_CUST_PAYMENT_DTL;
        }
        public static String DeleteEntity(string payId)
        {
            string str_Message = "";
            OM_CUST_PAYMENT_DTL obj_OM_CUST_PAYMENT_DTL = new OM_CUST_PAYMENT_DTL();
            obj_OM_CUST_PAYMENT_DTL.CUST_PAY_ID = payId;
            DBMethod.DeleteEntity<OM_CUST_PAYMENT_DTL>(obj_OM_CUST_PAYMENT_DTL);
            return str_Message;
        }
        public static string CalculateAmount(DataTable dtGridData, decimal currentAmount)
        {
            decimal CalculateAmount = currentAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (dtGridData.Rows[i]["CUST_INV_AMT"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["CUST_INV_AMT"].ToString());
                        }
                    }
                }
                else
                {
                    CalculateAmount = currentAmount;
                }
            }
            return CalculateAmount.ToString();
        }
        static DataTable dtDropDownData = new DataTable();

        public static void getInvoiceDetails(ref DropDownList ddlList,string currId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(SalesPayment_DAL.getInvoiceDetails(currId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_NUM", "OM_INV_ID", dtDropDownData, true, false);
        }
        public static void getAccountCodeDetails(ref DropDownList ddlList, string currId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(SalesPayment_DAL.getInvoiceDetails(currId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "OM_INV_ACCT_CODE_ID", "OM_INV_ACCT_CODE_ID", dtDropDownData, true, false);
        }


    }
}
