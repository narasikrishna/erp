﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.DAL.AP;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;


namespace FIN.BLL.AR
{
    public class AdvancedRefund_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        public static ADVANCED_REFUND getClassEntity(string str_Id)
        {
            ADVANCED_REFUND obj_aDVANCED_REFUND = new ADVANCED_REFUND();
            using (IRepository<ADVANCED_REFUND> userCtx = new DataRepository<ADVANCED_REFUND>())
            {
                obj_aDVANCED_REFUND = userCtx.Find(r =>
                    (r.REFUND_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_aDVANCED_REFUND;
        }

        public static ADVANCED_REFUND getClassEntityData(string str_Id)
        {
            ADVANCED_REFUND obj_aDVANCED_REFUND = new ADVANCED_REFUND();
            using (IRepository<ADVANCED_REFUND> userCtx = new DataRepository<ADVANCED_REFUND>())
            {
                obj_aDVANCED_REFUND = userCtx.Find(r =>
                    (r.REFUND_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_aDVANCED_REFUND;
        }
        public static void getInvoiceDetails(ref DropDownList ddlList, string currId, string supplierId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Payment_DAL.getInvoiceDetails(currId, supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, dtDropDownData, true, false);
        }

        public static void getAccCodeDetails(ref DropDownList ddlList, string currId, string supplierId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Payment_DAL.getInvoiceDetails(currId, supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "inv_acct_code_id", "inv_acct_code_id", dtDropDownData, true, false);
        }
        public static DataSet GetPaymentRegisterReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getPaymentRegisterReportData());
        }
        public static DataSet GetSupplierwisePaymentReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getSupplierwisePaymentReportData());
        }
    }
}
