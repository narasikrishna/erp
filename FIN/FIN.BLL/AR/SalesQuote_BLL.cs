﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.AR
{
    public static class SalesQuote_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static OM_SALE_QUOTE_HDR getClassEntity(string str_Id)
        {
            OM_SALE_QUOTE_HDR obj_OM_SALE_QUOTE_HDR = new OM_SALE_QUOTE_HDR();
            using (IRepository<OM_SALE_QUOTE_HDR> userCtx = new DataRepository<OM_SALE_QUOTE_HDR>())
            {
                obj_OM_SALE_QUOTE_HDR = userCtx.Find(r =>
                    (r.QUOTE_HDR_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_SALE_QUOTE_HDR;
        }

        public static OM_SALE_QUOTE_DTL getDetailClassEntity(string str_Id)
        {
            OM_SALE_QUOTE_DTL obj_OM_SALE_QUOTE_DTL = new OM_SALE_QUOTE_DTL();
            using (IRepository<OM_SALE_QUOTE_DTL> userCtx = new DataRepository<OM_SALE_QUOTE_DTL>())
            {
                obj_OM_SALE_QUOTE_DTL = userCtx.Find(r =>
                    (r.QUOTE_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_OM_SALE_QUOTE_DTL;
        }

        # region FillCombo
        public static void fn_getSalesQuote(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesQuote_DAL.getSalesQuota()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.QUOTE_NAME, FINColumnConstants.QUOTE_HDR_ID, dt_Det, true, false);
        }

        public static void fn_getPayterm(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesQuote_DAL.getPayterm()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.TERM_NAME, FINColumnConstants.TERM_ID, dt_Det, true, false);
        }


        # endregion

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AR.SalesQuote_DAL.getReportData());
        }
        public static void GetQuoteNumber(ref DropDownList ddlList,Boolean bol_ALL=false )
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesOrder_DAL.GetQuoteNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "QUOTE_HDR_ID", "QUOTE_HDR_ID", dtDropDownData, !bol_ALL, bol_ALL);
        }
    }
}
