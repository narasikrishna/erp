﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL
{
    public class FINAppConstants
    {

        public const string intialRowValueField = "";
        public const string intialRowTextField = "---Select---";
        public const string intialRowTextField_OL = "---حدد---";
        public const string intialRowAllTextField = "All";
        public const string intialRowAllTextField_OL = "كل";
        public const int defaultPrecision = 3;
        public const int TMP_YEAR = 2000;
        public const int TMP_MONTH = 1;
        public const int TMP_DAY = 1;

        public const string WF = "W";
        public const string Add = "A";
        public const string Update = "U";
        public const string Delete = "D";
        public const string Search = "S";
        public const string List = "L";
        public const string EnabledFlag = "1";
        public const string DisabledFlag = "0";
        public const string TRUEFLAG = "TRUE";
        public const string FALSEFLAG = "FALSE";
        public const string Posted = "1";


        public const string Y = "1";
        public const string CANCELED = "5";
        public const string N = "0";
        public const string TEXT_BOX = "TextBox";
        public const string DROP_DOWN_LIST = "DropDownList";
        public const string DATE_TIME = "DateTime";
        public const string DATE_RANGE_VALIDATE = "DateRangeValidate";
        public const String DATAIMPORT = "Data Imported Successfully";
        public const String ERROR = "ERROR";
        public const String DATASAVED = "DATASAVED";
        public const String DATADELETED = "DATADELETED";
        public const String DATAPROCESSED = "DATAPROCESSED";
        public const String RECORDMSG = "RECORDMSG";
        public const String SAVED = "SAVED";
        public const String POSTED = "POSTED";


        public const string ACTCODE_M = "ACTCODE_M";
        public const string ACTCODE_D = "ACTCODE_D";
        public const string GL_0001 = "GL_0001";
        public const string GL_0002 = "GL_0002";
        public const string GL_0003 = "GL_0003";
        public const string GL_0003_D = "GL_0003_D";
        public const string GL_0004 = "GL_0004";
        public const string AP_0001 = "AP_0001";
        public const string AP_0002 = "AP_0002";
        public const string AP_0003 = "AP_0003";
        public const string AP_0004 = "AP_0004";
        public const string AP_0005 = "AP_0005";
        public const string AP_0006 = "AP_0006"; //UOM master
        public const string AP_027 = "AP_027";



        public const string AP_007_M = "AP_007_M";
        public const string AP_007_D = "AP_007_D";
        public const string AP_009 = "AP_009";
        public const string AP_010 = "AP_010";
        public const string AP_011_M = "AP_011_M";
        public const string AP_011_D = "AP_011_D";
        public const string AP_012_M = "AP_012_M";
        public const string AP_012_D = "AP_012_D";
        public const string AP_013_M = "AP_013_M";
        public const string AP_013_D = "AP_013_D";
        public const string AP_013_S = "AP_013_S";
        
        public const string AP_013_LN = "AP_013_LN";
        public const string AP_023_M = "AP_023_M";
        public const string AP_023_D = "AP_023_D";
        public const string AP_028_M = "AP_028_M";
        public const string AP_028_D = "AP_028_D";
        public const string AP_028_T = "AP_028_T";

        public const string SSM_0001 = "SSM_0001";
        public const string GL_ORGN_M = "GL_ORGN_M";
        public const string GL_ORGN_D = "GL_ORGN_D";
        public const string ACTCAL_M = "ACTCAL_M";
        public const string ACTCAL_D = "ACTCAL_D";
        public const string SEG_M = "SEG_M";
        public const string SEG_D = "SEG_D";
        public const string JE_M = "JE_M";
        public const string JE_D = "JE_D";
        public const string GL_ACT_GRP = "GL_ACT_GRP";
        public const string AP_TERM_M = "AP_TERM_M";
        public const string AP_TERM_D = "AP_TERM_D";
        public const string GL_005_M = "GL_005_M";
        public const string GL_005_D = "GL_005_D";
        public const string GL_006_M = "GL_006_M";
        public const string GL_006_D = "GL_006_D";
        public const string GL_020 = "GL_020";
        public const string AP_014_LN = "AP_014_LN";
        public const string AP_014_D = "AP_014_D";
        public const string AP_014_M = "AP_014_M";

        public const string AP_015_LN = "AP_015_LN";
        public const string AP_015_D = "AP_015_D";
        public const string AP_015_M = "AP_015_M";
        public const string AP_016_M = "AP_016_M";
        public const string AP_016_D = "AP_016_D";
        public const string AP_018_M = "AP_018_M";
        public const string AP_018_D = "AP_018_D";
        public const string ACT_GP_DSE = "ACT_GP_DSE";
        public const string AP_017 = "AP_017";
        public const string AP_021_D = "AP_021_D";
        public const string AP_021_M = "AP_021_M";
        public const string AP_021_T = "AP_021_T";
        public const string AP_021_LN = "AP_021_LN";
        public const string STR_SCRENCODE = "STR_SCRENCODE";


        public const string HR_001 = "HR_001";
        public const string HR_PERM_M = "HR_PERM_M";
        public const string HR_LR_M = "HR_LR_M";
        public const string HR_002 = "HR_002";
        public const string HR_003 = "HR_003";
        public const string HR_118 = "HR_118";
        public const string HR_128 = "HR_128";
        public const string HR_004 = "HR_004";
        public const string HR_005 = "HR_005";
        public const string HR_006 = "HR_006";
        public const string HR_007 = "HR_007";
        public const string HR_008 = "HR_008";
        public const string HR_009 = "HR_009";
        public const string HR_009_NO = "HR_009_NO";
        public const string HR_009_ADD = "HR_009_ADD";
        public const string HR_009_CT = "HR_009_CT";
        public const string HR_017 = "HR_017";
        public const string HR_024 = "HR_024";
        public const string HR_026 = "HR_026";
        public const string HR_027 = "HR_027";
        public const string HR_018 = "HR_018";
        public const string HR_023 = "HR_023";
        public const string HR_025 = "HR_025";
        public const string HR_020 = "HR_020";
        public const string HR_021 = "HR_021";
        public const string HR_029 = "HR_029";
        public const string HR_034 = "HR_034";
        public const string HR_035_D = "HR_035_D";
        public const string HR_037 = "HR_037";
        public const string HR_041 = "HR_041";
        public const string HR_042 = "HR_042";
        public const string HR_036 = "HR_036";
        public const string HR_040_M = "HR_040_M";
        public const string HR_040_D = "HR_040_D";
        public const string HR_046 = "HR_046";
        public const string HR_050_M = "HR_050_M";
        public const string HR_050_D = "HR_050_D";
        public const string HR_054_M = "HR_054_M";
        public const string HR_054_D = "HR_054_D";
        public const string HR_090_M = "HR_090_M";
        public const string HR_090_D = "HR_090_D";
        public const string HR_047 = "HR_047";
        public const string HR_089 = "HR_089";
        public const string HR_048 = "HR_048";
        public const string HR_048_D = "HR_048_D";
        public const string HR_071 = "HR_071";
        public const string HR_071_S = "HR_071_S";
        public const string HR_071_WS = "HR_071_WS";
        public const string HR_055 = "HR_055";
        public const string HR_075 = "HR_075";
        public const string HR_077 = "HR_077";
        public const string HR_078 = "HR_078";
        public const string HR_124_M = "HR_124_M";
        public const string HR_124_D = "HR_124_D";
        public const string HR_079 = "HR_079";
        public const string HR_081 = "HR_081";
        public const string HR_097 = "HR_097";
        public const string HR_097_D = "HR_097_D";
        public const string HR_087 = "HR_087";
        public const string HR_082 = "HR_082";
        public const string HR_039_M = "HR_039_M";
        public const string HR_039_D = "HR_039_D";
        public const string HR_022 = "HR_022";
        public const string HR_109 = "HR_109";
        public const string HR_062 = "HR_062";
        public const string HR_083 = "HR_083";
        public const string GL_0004_D = "GL_0004_D";

        public const string CA_001 = "CA_001";
        public const string CA_002 = "CA_002";
        public const string CA_003 = "CA_003";
        public const string CA_004_M = "CA_004_M";
        public const string CA_004_D = "CA_004_D";

        public const string PER_008 = "PER_008";
        public const string PER_007 = "PER_007";
        public const string PER_005 = "PER_005";

        public const string AP_022_M = "AP_022_M";
        public const string AP_022_D = "AP_022_D";
        public const string PER_009 = "PER_009";
        public const string GL_007_JN = "GL_007_JN";
        public const string GL_007_M = "GL_007_M";
        public const string GL_007_D = "GL_007_D";
        public const string AP_009_VC = "AP_009_VC";
        public const string PER_011 = "PER_011";
        public const string HR_100_S = "HR_100_S";

        public const string AR_001 = "AR_001";
        public const string AR_002 = "AR_002";
        public const string AR_005_M = "AR_005_M";
        public const string AR_005_D = "AR_005_D";
        public const string AR_005_ON = "AR_005_ON";
        
        public const string AR_006_M = "AR_006_M";
        public const string AR_006_D = "AR_006_D";
        public const string SSM_019 = "SSM_019";
        public const string SSM_020 = "SSM_020";
        public const string AR_021_M = "AR_021_M";
        public const string AR_021_LN = "AR_021_LN";
        public const string AR_021_D = "AR_021_D";
        public const string AR_021_IN = "AR_021_IN";
        


        public const string HR_080 = "HR_080";
        public const string CA_009_M = "CA_009_M";
        public const string CA_009_D = "CA_009_D";

        public const string HR_085_M = "HR_085_M";
        public const string HR_085_D = "HR_085_D";

        
        public const string LN_001_M = "LN_001_M";
        public const string LN_001_D = "LN_001_D";

        public const string LN_005 = "LN_005";
        public const string LN_010 = "LN_010";

        public const string LN_003_M = "LN_003_M";
        public const string LN_003_D = "LN_003_D";

        public const string HR_091_M = "HR_091_M";
        public const string HR_091_D = "HR_091_D";

        public const string HR_092_M = "HR_092_M";
        public const string HR_092_D = "HR_092_D";

        public const string HR_093_M = "HR_093_M";
        public const string HR_093_D = "HR_093_D";


        public const string AP_025_M = "AP_025_M";

        public const string AP_025_D = "AP_025_D";
        public const string AP_025_S = "AP_025_S";

        public const string AP_025_R = "AP_025_R";
        public const string AP_025_N = "AP_025_N";


        public const string AP_026_M = "AP_026_M";
        public const string AP_026_D = "AP_026_D";
        public const string AP_026_A = "AP_026_A";
        public const string AP_26_Q = "AP_26_Q";
        
        public const string HR_098 = "HR_098";
        public const string HR_100 = "HR_100";

        public const string HR_104 = "HR_104";

        public const string PR_020_PA = "PR_020_PA";
        public const string HR_102 = "HR_102";
        public const string HR_107 = "HR_107";
        public const string HR_111 = "HR_111";

        public const string HR_121 = "HR_121";
        public const string HR_121_D = "HR_121_D";
        public const string HR_109_M = "HR_109_M";

        public const string HR_122 = "HR_122";
        public const string HR_122_D = "HR_122_D";


        public const string HR_126_M = "HR_126_M";
        public const string HR_126_D = "HR_126_D";

        public const string HR_127 = "HR_127";
        public const string LN_004_M = "LN_004_M";
        public const string LN_004_D = "LN_004_D";
        public const string LN_001 = "LN_001";
        
    }
}

