﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL
{
    public  class FINListConstants_BLL
    {
        public const string GL = "GL";
        public const string AP = "AP";
        public const string HR = "HR";
        public const string CA = "CA";
        public const string SSM = "SSM";
        public const string PA = "PR";//Payroll
        public const string FA = "FA";//Fixed Assets
        public const string AR = "AR";//Fixed Assets
        public const string LN = "LN";//Loan


        public const string GL_D = "General Ledger";
        public const string AP_D = "Payable";
        public const string HR_D = "Human Resource";
        public const string CA_D = "Cash Account";
        public const string SSM_D = "System Setup";
        public const string PA_D = "Payroll";//Payroll
        public const string FA_D = "Fixed Assets";//Fixed Assets
        public const string AR_D = "Receivable";
        public const string LN_D = "Loan";
       
    }
}
