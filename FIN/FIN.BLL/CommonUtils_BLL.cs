﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FIN.BLL;
using FIN.DAL;
using FIN.DAL.SSM;
using System.Configuration;
using System.Drawing.Imaging;
using System.Net;
using System.Drawing;
using System.IO;
using VMVServices.Services.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace FIN.BLL
{
    public class CommonUtils
    {
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

       
        public static void LoadDropDownList(DropDownList ddl, string dataTextField, string dataValueField, object dataSource, bool includeBlankItem, bool includeAllItem)
        {
            ddl.AppendDataBoundItems = true;
            ddl.Items.Clear();

            if (includeBlankItem)
            {
                if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
                {

                    ddl.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
                }
                else
                {
                    ddl.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
                }
            }
            if (includeAllItem)
            {
                if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
                {

                    ddl.Items.Add(new ListItem(FINAppConstants.intialRowAllTextField, ""));
                }
                else
                {
                    ddl.Items.Add(new ListItem(FINAppConstants.intialRowAllTextField_OL, ""));
                }
               
            }

            ddl.DataTextField = dataTextField;
            ddl.DataValueField = dataValueField;
            ddl.DataSource = dataSource;
            ddl.DataBind();

        }
        public static int ConvertStringToInt(string strValue)
        {
            int intValue = 0;

            if (strValue != string.Empty)
            {
                intValue = int.Parse(strValue.ToString());
            }

            return intValue;
        }
        public static decimal ConvertStringToDecimal(string strValue)
        {
            decimal decimalValue = 0;

            if (strValue != string.Empty)
            {
                decimalValue = decimal.Parse(strValue.ToString());
            }

            return decimalValue;
        }
        public static double ConvertStringToDouble(string strValue)
        {
            double doubleValue = 0;

            if (strValue != string.Empty)
            {
                doubleValue = double.Parse(strValue.ToString());
            }

            return doubleValue;
        }
        public static long ConvertStringToLong(string strValue)
        {
            long longValue = 0;

            if (strValue != string.Empty)
            {
                longValue = long.Parse(strValue.ToString());
            }

            return longValue;
        }

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        public static void SaveMultipleEntity<T, TC, T2>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
            where T2 : class
        {
            try
            {
                DBMethod.SaveMultipleEntity<T, TC, T2>(entity, tmpChildEntity, ChildEntity, tmpSecondChildEntity, SecondChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        public static void SaveMultipleRowsEntity<TC, T2>(List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, bool modifyMode = false)

            where TC : class
            where T2 : class
        {
            try
            {
                DBMethod.SaveMultipleRowsEntity<TC, T2>(tmpChildEntity, ChildEntity, tmpSecondChildEntity, SecondChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        public static void SaveFourEntity<T, TC, T2, T3>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, List<Tuple<object, string>> tmpThirdChildEntity, T3 ThirdChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
            where T2 : class
            where T3 : class
        {
            try
            {
                DBMethod.SaveFourEntity<T, TC, T2, T3>(entity, tmpChildEntity, ChildEntity, tmpSecondChildEntity, SecondChildEntity, tmpThirdChildEntity, ThirdChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        public static void SaveSingleEntity<T>(List<Tuple<object, string>> entity, bool modifyMode = false)
            where T : class
        {
            try
            {
                DBMethod.SaveSingleEntity<T>(entity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        /// <summary>
        /// To check the empty validation of UI controls
        /// </summary>
        /// <param name="SLctrlname">Contains the controls's name</param>
        /// <param name="strctrltype">Contains the type of the controls</param>
        /// <param name="strMessage">Contains the message of corresponding controls</param>
        /// <returns>Error message</returns>
        public static System.Collections.SortedList IsValid(System.Collections.SortedList SLctrlname, string strctrltype, string strMessage)
        {

            ErrorCollection.Clear();

            string[] strTypes = strctrltype.Split('~');
            string[] strMessages = strMessage.Split('~');

            for (int iLoop = 0; iLoop < strTypes.Length; iLoop++)
            {
                string str_Ctrl_Type = strTypes[iLoop].ToString();

                if (str_Ctrl_Type == FINAppConstants.TEXT_BOX)
                {
                    System.Web.UI.WebControls.TextBox txt_tmp = (System.Web.UI.WebControls.TextBox)SLctrlname[iLoop];
                    if (txt_tmp.Text.ToString().Trim().Length == 0)
                    {
                        ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, strMessages[iLoop]));
                    }
                }
                if (str_Ctrl_Type == FINAppConstants.DROP_DOWN_LIST)
                {
                    System.Web.UI.WebControls.DropDownList ddl_tmp = (System.Web.UI.WebControls.DropDownList)SLctrlname[iLoop];
                    if (ddl_tmp.SelectedItem == null)
                    {
                        if (ddl_tmp.SelectedValue.ToString() == FINAppConstants.intialRowValueField)
                        {
                            ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, strMessages[iLoop]));
                        }
                    }
                    else
                    {
                        if (ddl_tmp.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddl_tmp.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
                        {
                            ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, strMessages[iLoop]));
                        }
                    }
                }
                if (str_Ctrl_Type == FINAppConstants.DATE_TIME)
                {
                    System.Web.UI.WebControls.TextBox txt_tmp = (System.Web.UI.WebControls.TextBox)SLctrlname[iLoop];
                    if (txt_tmp.Text.ToString().Trim().Length > 0)
                    {
                        try
                        {
                            DateTime tmp;
                            if (!DateTime.TryParse(DBMethod.ConvertStringToDate(txt_tmp.Text).ToString(), out tmp))
                            {
                                ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.INVALID_DATE_FORMAT, strMessages[iLoop]));
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.INVALID_DATE_FORMAT, strMessages[iLoop]));
                        }
                    }
                }
                if (str_Ctrl_Type == FINAppConstants.DATE_RANGE_VALIDATE)
                {
                    System.Web.UI.WebControls.TextBox txt_endDate = (System.Web.UI.WebControls.TextBox)SLctrlname[iLoop];
                    System.Web.UI.WebControls.TextBox txt_StartDate = (System.Web.UI.WebControls.TextBox)SLctrlname[iLoop - 1];
                    if (txt_endDate.Text.ToString().Trim().Length > 0)
                    {
                        try
                        {
                            DateTime tmp;
                            if (!DateTime.TryParse(DBMethod.ConvertStringToDate(txt_endDate.Text).ToString(), out tmp))
                            {
                                ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.INVALID_DATE_FORMAT, strMessages[iLoop]));
                            }
                            else
                            {
                                if (Convert.ToDateTime(DBMethod.ConvertStringToDate(txt_StartDate.Text)) >= Convert.ToDateTime(DBMethod.ConvertStringToDate(txt_endDate.Text)))
                                {
                                    ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage("End Date Must Be Greater Than Start Date ", strMessages[iLoop]));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorCollection.Add(strMessage[iLoop] + iLoop.ToString(), VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.INVALID_DATE_FORMAT, strMessages[iLoop]));
                        }
                    }

                }



            }
            return ErrorCollection;
        }

        /// <summary>
        /// Shows the error message for email id
        /// </summary>
        /// <returns>Error message</returns>
        public static System.Collections.SortedList IsValidEmailId()
        {
            ErrorCollection.Remove(FINColumnConstants.EMAIL);
            ErrorCollection.Add(FINColumnConstants.EMAIL, "Email Id should be valid [someone@somewhere.com]");

            return ErrorCollection;
        }
        public static System.Collections.SortedList IsAnySingleEntry(string Data1, string Data2, string POQuantity)
        {
            ErrorCollection.Remove("Single");

            if (decimal.Parse(POQuantity.ToString()) < decimal.Parse(Data1.ToString()))
            {
                ErrorCollection.Add("Singlea", "Received quantity should not be greater than the PO Quantity");
            }
            if (decimal.Parse(POQuantity.ToString()) < decimal.Parse(Data2.ToString()))
            {
                ErrorCollection.Add("Singles", "Rejected quantity should not be greater than the PO Quantity");
            }


            if (Data1 == string.Empty && Data2 == string.Empty)
            {
                ErrorCollection.Add("Singlec", "Enter Received quantity or Rejected quantity");
            }
            if (Data1 != string.Empty && Data2 != string.Empty)
            {
                if (decimal.Parse(Data1.ToString()) < decimal.Parse(Data2.ToString()))
                {
                    ErrorCollection.Add("Singled", "Rejected quantity should not be greater than the Received quantity");
                }
            }


            return ErrorCollection;
        }
        public static string AmountSeparation(string amtValue)
        {
            string formattedValue = "0";

            if (amtValue != null || amtValue.ToString().Trim() != string.Empty)
            {
                //var formatInfo = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
                //formatInfo.CurrencyGroupSeparator = string.Empty;

                //var stringValue = Convert.ToDecimal(strValue).ToString("C", formatInfo);
                //formattedValue = stringValue.Replace("$", "");// decimal.Parse(strValue.ToString(), CultureInfo.InvariantCulture).ToString("N", new CultureInfo("de-DE"));


                string format = "#,##0.00;-#,##0.00;0";
                formattedValue = decimal.Parse(amtValue.ToString()).ToString(format);

            }
            return formattedValue;
        }
        /// <summary>
        /// To check the empty validation of Gridview control
        /// </summary>
        /// <param name="dtGridData">Contains the datasource of gridview</param>
        /// <param name="detailName">Grid name, it varies each and every form</param>
        /// <returns>Error message</returns>
        public static System.Collections.SortedList IsEmptyGrid(DataTable dtGridData, string detailName)
        {
            ErrorCollection.Remove("GridView");
            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count == 0)
                {
                    ErrorCollection.Add("GridView", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, detailName + " details"));
                }
            }
            else
            {
                ErrorCollection.Add("GridView", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, detailName + " details"));
            }
            return ErrorCollection;
        }

        /// <summary>
        /// Avoid the future date
        /// </summary>
        /// <param name="dateValue">Future the date value, which date are you going to be validate</param>
        /// <param name="dateName">Future the date name what message want to show to the client(for ex:Exam Name)</param>
        /// <returns></returns>        
        public static System.Collections.SortedList IsFutureDate(string dateValue, string dateName)
        {
            ErrorCollection.Remove("FutureDate");
            if (DBMethod.ConvertStringToDate(dateValue.ToString()) >= DateTime.Today)
            {
                ErrorCollection.Add("FutureDate", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.FUTURE_DATE_FORMAT, dateName + " "));
            }
            return ErrorCollection;
        }

        /// <summary>
        /// Avoid the past date
        /// </summary>
        /// <param name="dateValue">Pass the date value, which date are you going to be validate</param>
        /// <param name="dateName">Pass the date name what message want to show to the client(for ex:Exam Name)</param>
        /// <returns></returns>
        public static System.Collections.SortedList IsPastDate(string dateValue, string dateName)
        {
            ErrorCollection.Remove("PastDate");
            if (DBMethod.ConvertStringToDate(dateValue.ToString()) <= DateTime.Today)
            {
                ErrorCollection.Add("PastDate", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.PAST_DATE_FORMAT, dateName + " "));
            }
            return ErrorCollection;
        }

        public static Bitmap GenerateBarcode(string barcodeNumber, Bitmap bitmap)
        {
            // Bitmap bitMap;
            VMVServices.Web.Utils.BarcodeBitmap = bitmap;

            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();

            using (Graphics graphics = Graphics.FromImage(VMVServices.Web.Utils.BarcodeBitmap))
            {
                Font oFont = new Font("IDAutomationHC39M", 16);
                PointF point = new PointF(2f, 2f);
                SolidBrush blackBrush = new SolidBrush(Color.Black);
                SolidBrush whiteBrush = new SolidBrush(Color.White);
                graphics.FillRectangle(whiteBrush, 0, 0, VMVServices.Web.Utils.BarcodeBitmap.Width, VMVServices.Web.Utils.BarcodeBitmap.Height);
                graphics.DrawString("*" + barcodeNumber + "*", oFont, blackBrush, point);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                VMVServices.Web.Utils.BarcodeBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] byteImage = ms.ToArray();

                Convert.ToBase64String(byteImage);
                imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
            }

            //}
            return VMVServices.Web.Utils.BarcodeBitmap;
        }

        /// <summary>
        /// Sum the Column
        /// </summary>
        /// <param name="datatable">Calculate the Sum of the Specified Column Name</param>
        /// <param name="Columnname">ColumnName need TO Sum
        /// 
        public static string CalculateTotalAmount(DataTable dtGridData, string ColumnName, string exteraCondition = "")
        {
            //dtGridData.Compute("sum(ColumnName)", "DELETED='N'").ToString()
            string sumVal = string.Empty;
            string str_Condition = "DELETED IN ('N','0')";
            if (exteraCondition.Length > 0)
            {
                str_Condition += " and " + exteraCondition;
            }

            // temporary column for converting string to decimal
            dtGridData.Columns.Add(ColumnName + "Temp", typeof(decimal), "Convert(" + ColumnName + ", 'System.Decimal')");
            // using temporary column to do aggregation

            object TotalCost = dtGridData.Compute("Sum(" + ColumnName + "Temp)", str_Condition);

            dtGridData.Columns.Remove(ColumnName + "Temp");

            sumVal = TotalCost.ToString();//. dtGridData.Compute("sum(" + ColumnName + ")", str_Condition).ToString();
            return sumVal;
        }
        public static string CalculateTotalAmounts(DataTable dtGridData, string ColumnName, string exteraCondition = "")
        {
            //dtGridData.Compute("sum(ColumnName)", "DELETED='N'").ToString()
            string sumVal = string.Empty;
            string str_Condition = exteraCondition;

            // temporary column for converting string to decimal
            dtGridData.Columns.Add(ColumnName + "Temp", typeof(decimal), "Convert(" + ColumnName + ", 'System.Decimal')");
            // using temporary column to do aggregation

            object TotalCost = dtGridData.Compute("Sum(" + ColumnName + "Temp)", str_Condition);

            dtGridData.Columns.Remove(ColumnName + "Temp");

            sumVal = TotalCost.ToString();//. dtGridData.Compute("sum(" + ColumnName + ")", str_Condition).ToString();
            return sumVal;
        }


        /// <summary>
        /// Sum the Column
        /// </summary>
        /// <param name="datatable">Calculate the Sum of the Specified Column Name</param>
        /// <param name="Columnname">ColumnName need TO Sum
        /// 


        public static System.Collections.SortedList ValidateGridDate(DataTable dtGridData, int rowIndex, DateTime? startdate, DateTime? enddate, string startDateName, string endDateName)
        {
            ErrorCollection.Remove("START_DATE");

            DateTime? prevstartDate = null;
            DateTime? prevEndDate = null;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (rowIndex != i)
                        {

                            if (ErrorCollection.Count == 0)
                            {
                                if (prevEndDate != null)
                                {
                                    if (!prevEndDate.Equals(DateTime.MinValue))
                                    {
                                        if (prevEndDate >= startdate || prevEndDate >= enddate)
                                        {
                                            ErrorCollection.Add("START_DATE" + i, VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Date range "));
                                            break;
                                        }
                                    }
                                }
                            }
                            if (dtGridData.Rows[i][startDateName].ToString() != string.Empty)
                            {
                                prevstartDate = DateTime.Parse(dtGridData.Rows[i][startDateName].ToString());
                            }
                            if (dtGridData.Rows[i][endDateName].ToString() != string.Empty)
                            {
                                prevEndDate = DateTime.Parse(dtGridData.Rows[i][endDateName].ToString());
                            }

                        }
                    }
                }
            }
            return ErrorCollection;
        }

        public static System.Collections.SortedList Validate2Amounts(decimal fromAmt, decimal toAmt)
        {
            ErrorCollection.Remove("AmtError");
            ErrorCollection.Remove("FromAmtError");
            ErrorCollection.Remove("TOAmtError");

            if (fromAmt == 0)
            {
                ErrorCollection.Add("FromAmtError", "From Amount should be greater than the Zero");
            }
            if (toAmt == 0)
            {

                ErrorCollection.Add("TOAmtError", "To Amount should be greater than the Zero");
            }
            if (fromAmt >= toAmt)
            {
                ErrorCollection.Add("AmtError", "From Amount should not be greater than the To Amount");
            }

            return ErrorCollection;
        }

        public static System.Collections.SortedList Validate2AmountsExp(string fromAmt, string toAmt)
        {
            ErrorCollection.Remove("InvalidFromAmt");
            ErrorCollection.Remove("InvalidToAmt");
            ErrorCollection.Remove("AmtError");
           
            decimal dec_frmamt=0;
            decimal dec_toamt = 0;

            if (fromAmt.Length > 0 && toAmt.Length > 0)
            {
                if (decimal.TryParse(fromAmt, out dec_frmamt))
                {
                }
                else
                {
                    ErrorCollection.Add("InvalidFromAmt", "Invalid From Amount");
                }

                if (decimal.TryParse(toAmt, out dec_toamt))
                {
                }
                else
                {
                    ErrorCollection.Add("InvalidToAmt", "Invalid To Amount");
                }


                if (dec_frmamt >= dec_toamt)
                {
                    ErrorCollection.Add("AmtError", "From Amount should not be greater than the To Amount");
                }
            }
            return ErrorCollection;
        }

        public static System.Collections.SortedList Validate2QuantityExp(string fromAmt, string toAmt)
        {
            ErrorCollection.Remove("InvalidFromQnty");
            ErrorCollection.Remove("InvalidToQnty");
            ErrorCollection.Remove("QntyError");

            decimal dec_frmamt = 0;
            decimal dec_toamt = 0;

            if (fromAmt.Length > 0 && toAmt.Length > 0)
            {
                if (decimal.TryParse(fromAmt, out dec_frmamt))
                {
                }
                else
                {
                    ErrorCollection.Add("InvalidFromQnty", "Invalid From Quantity");
                }

                if (decimal.TryParse(toAmt, out dec_toamt))
                {
                }
                else
                {
                    ErrorCollection.Add("InvalidToQnty", "Invalid To Quantity");
                }


                if (dec_frmamt >= dec_toamt)
                {
                    ErrorCollection.Add("QntyError", "From Quantity should not be greater than the To Quantity");
                }
            }
            return ErrorCollection;
        }

        public static System.Collections.SortedList ValidateDateRange(string str_fromDt, string str_ToDt)
        {
            ErrorCollection.Remove("DateRante");

            if (str_fromDt.Length > 0 && str_ToDt.Length > 0)
            {
                if (DBMethod.ConvertStringToDate(str_fromDt.ToString()) > DBMethod.ConvertStringToDate(str_ToDt.ToString()))
                {
                    ErrorCollection.Add("daterange", "From Date should not be greater than the To Date");                

                }
            }

            return ErrorCollection;
        }
        public static System.Collections.SortedList Validate2ChequeNumber(decimal fromAmt, decimal toAmt)
        {
            ErrorCollection.Remove("FromCheque");
            ErrorCollection.Remove("TOCheque");
            ErrorCollection.Remove("ChequeError");

            if (fromAmt == 0)
            {
                ErrorCollection.Add("FromCheque", "From Cheque Number should be greater than the Zero");
            }
            if (toAmt == 0)
            {

                ErrorCollection.Add("TOCheque", "To Cheque Number should be greater than the Zero");
            }
            if (fromAmt >= toAmt)
            {
                ErrorCollection.Add("ChequeError", "From Cheque Number should not be greater than the To Cheque Number");
            }

            return ErrorCollection;
        }



        public static System.Collections.SortedList Validate2InvoiceNumber(int fromAmt, int toAmt)
        {
            ErrorCollection.Remove("AmtError");

            if (fromAmt > toAmt)
            {
                ErrorCollection.Add("InvError", "Invoice To number should be reater than Invoice From Number");
            }

            return ErrorCollection;
        }


        public static System.Collections.SortedList ValidateGridValue(DataTable dtGridData, int rowIndex, int? MinVal, int? MaxVal, string MinValName, string MaxValName)
        {
            ErrorCollection.Remove("LC_LOW_VALUE");

            int? prevMinVal = null;
            int? prevMaxVal = null;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count == 1)
                {

                    if (dtGridData.Rows[0][MinValName].ToString() != string.Empty)
                    {
                        prevMinVal = int.Parse(dtGridData.Rows[0][MinValName].ToString());
                    }
                    if (dtGridData.Rows[0][MaxValName].ToString() != string.Empty)
                    {
                        prevMaxVal = int.Parse(dtGridData.Rows[0][MaxValName].ToString());
                    }

                    if (ErrorCollection.Count == 0)
                    {
                        if (prevMaxVal != null)
                        {

                            if (prevMaxVal >= MinVal || prevMaxVal >= MaxVal)
                            {
                                ErrorCollection.Add("LC_LOW_VALUE123", "Value range Already Exists..");

                            }

                        }
                    }
                }
                else if (dtGridData.Rows.Count > 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (rowIndex != i)
                        {

                            if (ErrorCollection.Count == 0)
                            {
                                if (prevMaxVal != null)
                                {

                                    if (prevMaxVal >= MinVal || prevMaxVal >= MaxVal)
                                    {
                                        ErrorCollection.Add("LC_LOW_VALUE", "Value range Already Exists..");
                                        break;
                                    }

                                }
                            }
                            if (dtGridData.Rows[i][MinValName].ToString() != string.Empty)
                            {
                                prevMinVal = int.Parse(dtGridData.Rows[i][MinValName].ToString());
                            }
                            if (dtGridData.Rows[i][MaxValName].ToString() != string.Empty)
                            {
                                prevMaxVal = int.Parse(dtGridData.Rows[i][MaxValName].ToString());
                            }

                        }
                    }
                }
            }
            return ErrorCollection;
        }


        public static void DeleteData(string ChildTableName, string ChildColumnName, string ChildRecId, string ParentTableName, string ParentColumnName, string ParentRecId)
        {
            string str_Query = "";
            if (ChildTableName.Length > 0)
            {
                str_Query = "Delete From " + ChildTableName + " where " + ChildColumnName + "='" + ChildRecId + "'";
                DBMethod.ExecuteNonQuery(str_Query);
            }
            if (ParentTableName.Length > 0)
            {
                str_Query = "Delete From " + ParentTableName + " where " + ParentColumnName + "='" + ParentRecId + "'";
                DBMethod.ExecuteNonQuery(str_Query);
            }
        }

        public static void GenerateEmailAttachment(string toAddress, string alertMsg, string displayName = "", string pdfFileName = "")
        {
            MailMessage oMsg = new MailMessage();
            try
            {
                ErrorCollection.Clear();

                displayName = "Welcome...";
                string emailParam = string.Empty;
                string email = string.Empty;



                emailParam = DBMethod.GetStringValue(SystemParameters_DAL.GetSysEmailParam());

                if (emailParam != string.Empty)
                {
                    if (emailParam.ToUpper() == "YES" || emailParam.ToUpper() == "Y")
                    {
                        DataTable dtData = new DataTable();
                        dtData = DBMethod.ExecuteQuery(Alerts_DAL.GetEmailAlert()).Tables[0];

                        if (dtData != null)
                        {
                            if (dtData.Rows.Count > 0)
                            {


                                oMsg.From = new MailAddress(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_NAME"].ToString());
                                oMsg.To.Add(new MailAddress(toAddress, displayName));
                                oMsg.Subject = dtData.Rows[0]["PARAM_NAME"].ToString();
                                oMsg.Body = alertMsg;
                                oMsg.Attachments.Add(new Attachment(pdfFileName));
                                oMsg.Attachments.Add(new System.Net.Mail.Attachment(pdfFileName, System.Net.Mime.MediaTypeNames.Application.Octet));
                                oMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                                oMsg.BodyEncoding = System.Text.Encoding.UTF8;
                                oMsg.IsBodyHtml = false;
                                oMsg.Priority = MailPriority.High;

                                SmtpClient oSmtp = new SmtpClient("smtp.gmail.com", 587);
                                oSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                                oSmtp.EnableSsl = true;

                                NetworkCredential oCredential = new NetworkCredential(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_VALUE"].ToString());

                                oSmtp.UseDefaultCredentials = true;
                                oSmtp.Credentials = oCredential;
                                oSmtp.Send(oMsg);
                                Alerts_DAL.InsertProcessLog("Success", toAddress, "1");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Clear();
                ErrorCollection.Add("emilaerror", ex.Message);
            }
            finally
            {
                oMsg.Dispose();
                if (ErrorCollection.Count > 0)
                {
                    System.Collections.IDictionaryEnumerator myEnumerator = ErrorCollection.GetEnumerator();
                    while (myEnumerator.MoveNext())
                    {
                        Alerts_DAL.InsertProcessLog(myEnumerator.Value.ToString(), toAddress, "0");
                    }
                }
            }
        }




        public static void GenerateEmailAttachment(string toAddress, string alertMsg, string displayName = "", string pdfFileName = "", string str_Host = "smtp.gmail.com", int int_Port = 487, string str_Subject = "")
        {
            MailMessage oMsg = new MailMessage();
            try
            {
                ErrorCollection.Clear();

                displayName = "Welcome...";
                string emailParam = string.Empty;
                string email = string.Empty;

                alertMsg = "<HTML><BODY>" + alertMsg + "</BODY></HTML>";

                string body = string.Empty;


                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(alertMsg);
                writer.Flush();
                stream.Position = 0;


                using (StreamReader reader = new StreamReader(stream))
                {
                    alertMsg = reader.ReadToEnd();
                }


                emailParam = DBMethod.GetStringValue(SystemParameters_DAL.GetSysEmailParam());

                if (emailParam != string.Empty)
                {
                    if (emailParam.ToUpper() == "YES" || emailParam.ToUpper() == "Y")
                    {
                        DataTable dtData = new DataTable();

                        dtData = DBMethod.ExecuteQuery(Alerts_DAL.GetEmailAlert()).Tables[0];

                        if (dtData != null)
                        {
                            if (dtData.Rows.Count > 0)
                            {

                                oMsg.From = new MailAddress(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_DESC"].ToString());
                                oMsg.To.Add(new MailAddress(toAddress, displayName));
                                if (str_Subject.Length == 0)
                                    str_Subject = dtData.Rows[0]["PARAM_NAME"].ToString();
                                oMsg.Subject = str_Subject;
                                oMsg.Body = alertMsg;
                                oMsg.Attachments.Add(new Attachment(pdfFileName));
                                // oMsg.Attachments.Add(new System.Net.Mail.Attachment(pdfFileName, System.Net.Mime.MediaTypeNames.Application.Octet));
                                oMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                                oMsg.BodyEncoding = System.Text.Encoding.UTF8;
                                oMsg.IsBodyHtml = true;
                                oMsg.Priority = MailPriority.High;

                                SmtpClient oSmtp = new SmtpClient(str_Host, int_Port);
                                oSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                                oSmtp.EnableSsl = true;

                                NetworkCredential oCredential = new NetworkCredential(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_VALUE"].ToString());

                                oSmtp.UseDefaultCredentials = true;
                                oSmtp.Credentials = oCredential;
                                oSmtp.Send(oMsg);
                                Alerts_DAL.InsertProcessLog("Success", toAddress, "1");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Clear();
                ErrorCollection.Add("emilaerror", ex.Message);
                FIN.DAL.SSM.Alerts_DAL.InsertProcessLog("err", ex.Message, "0");
                FIN.DAL.SSM.Alerts_DAL.InsertProcessLog("innerex", ex.InnerException.ToString().Substring(0, 65), "0");
            }
            finally
            {
                oMsg.Dispose();
                if (ErrorCollection.Count > 0)
                {
                    System.Collections.IDictionaryEnumerator myEnumerator = ErrorCollection.GetEnumerator();
                    while (myEnumerator.MoveNext())
                    {
                        Alerts_DAL.InsertProcessLog(myEnumerator.Value.ToString(), toAddress, "0");
                    }
                }
            }
        }
        


        public static void GenerateEmail(string toAddress, string alertMsg, string displayName = "")
        {
            try
            {
                displayName = "Welcome...";

                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(Alerts_DAL.GetEmailAlert()).Tables[0];
                string emailParam = string.Empty;
                string email = string.Empty;



                emailParam = DBMethod.GetStringValue(SystemParameters_DAL.GetSysEmailParam());

                if (emailParam != string.Empty)
                {
                    if (emailParam.ToUpper() == "YES" || emailParam.ToUpper() == "Y")
                    {
                        if (dtData != null)
                        {
                            if (dtData.Rows.Count > 0)
                            {
                                MailMessage oMsg = new MailMessage();

                                oMsg.From = new MailAddress(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_NAME"].ToString());
                                oMsg.To.Add(new MailAddress(toAddress, displayName));
                                oMsg.Subject = dtData.Rows[0]["PARAM_NAME"].ToString();
                                oMsg.Body = alertMsg;

                                oMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                                oMsg.BodyEncoding = System.Text.Encoding.UTF8;
                                oMsg.IsBodyHtml = false;
                                oMsg.Priority = MailPriority.High;

                                SmtpClient oSmtp = new SmtpClient("smtp.gmail.com", 587);
                                oSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                                oSmtp.EnableSsl = true;

                                NetworkCredential oCredential = new NetworkCredential(dtData.Rows[0]["PARAM_DESC"].ToString(), dtData.Rows[0]["PARAM_VALUE"].ToString());

                                oSmtp.UseDefaultCredentials = true;
                                oSmtp.Credentials = oCredential;
                                oSmtp.Send(oMsg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex; //  Response.Write("Could not send the e-mail - error: " + ex.Message);
            }
        }
        public void GenerateSMS()
        {
            string api = "http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp";
            string uid = "vmvsys";
            string password = "741833959";
            string sendername = "vmvsys";
            string msg = "Welcome to VMV Systems";
            string mobinumber = "919962880010";
            HttpWebRequest myReq =
                //  (HttpWebRequest)WebRequest.Create(api + "?username=" + uid + "&password=" + password + "&message=" + msg + "&mobileno=" + mobinumber + "&provider=mysmsmantra");

            (HttpWebRequest)WebRequest.Create(api + "?username=" + uid + "&password=" + password + "&sendername=" + sendername + "&message=" + msg + "&mobileno=" + mobinumber);
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
        }

        public static System.Collections.SortedList ValidateBetweenValues(string baseValue, string fromValue, string toValue, string errorMessage)
        {
            ErrorCollection.Remove("ValidateBetweenValues");

            if (CommonUtils.ConvertStringToDecimal(baseValue) >= 0 && CommonUtils.ConvertStringToDecimal(fromValue) >= 0 && CommonUtils.ConvertStringToDecimal(toValue) >= 0)
            {
                if ((CommonUtils.ConvertStringToDecimal(toValue) - CommonUtils.ConvertStringToDecimal(fromValue)) > CommonUtils.ConvertStringToDecimal(baseValue))
                {
                    ErrorCollection.Add("ValidateBetweenValues", errorMessage);
                }
            }

            return ErrorCollection;
        }
    }
}
