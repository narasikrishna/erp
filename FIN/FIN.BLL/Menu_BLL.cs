﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FIN.DAL;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
namespace FIN.BLL
{
    public static class Menu_BLL
    {
        static DataTable dtData = new DataTable();

        public static Hashtable GetMenuDetail(string menuCode)
        {
            Hashtable ht = new Hashtable();
            //Get Programs Information
            SSM_MENU_ITEMS sSM_MENU_ITEMS = new SSM_MENU_ITEMS();
            string tableName = string.Empty;

            using (IRepository<SSM_MENU_ITEMS> userCtx = new DataRepository<SSM_MENU_ITEMS>())
            {
                sSM_MENU_ITEMS = userCtx.Find(r =>
                    (r.MENU_CODE == menuCode)
                    ).SingleOrDefault();
            }

            dtData = DBMethod.ExecuteQuery(FINSQL.GetMenuDtl(long.Parse(sSM_MENU_ITEMS.PK_ID.ToString()))).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    tableName = dtData.Rows[0]["list_table_name"].ToString();
                }
            }

            string modifyURL = "";
            string deleteURL = "";
            string addURL = "";
            string listURL = "";


            string pageURL = sSM_MENU_ITEMS.ENTRY_PAGE_OPEN;
            string programIDTag = QueryStringTags.ProgramID.ToString() + "=" + sSM_MENU_ITEMS.PK_ID.ToString();
            string programModeTagUpdate = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Update.ToString().Substring(0, 1) + "'";
            string programModeTagDelete = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Delete.ToString().Substring(0, 1) + "'";
            string programKeyTag = "'" + QueryStringTags.ID.ToString() + "='||" + tableName + "." + sSM_MENU_ITEMS.PK_COLUMN_NAME;
            string HeaderName = sSM_MENU_ITEMS.FORM_LABEL;

            modifyURL = string.Format("'{0}'||{1}||{2}||{1}||{3} as ModifyURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagUpdate, programKeyTag);

            deleteURL = string.Format("'{0}'||{1}||{2}||{1}||{3} as DeleteURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagDelete, programKeyTag);

            addURL = string.Format(pageURL + "?" + QueryStringTags.ID.ToString() + "={0}" + "&" + QueryStringTags.Mode.ToString() + "={1}" + "&" + QueryStringTags.ProgramID.ToString() + "={2}", 0, ProgramMode.Add.ToString().Substring(0, 1), sSM_MENU_ITEMS.PK_ID);
            listURL = string.Format(sSM_MENU_ITEMS.MENU_URL + "?" + QueryStringTags.ProgramID.ToString() + "={0}", sSM_MENU_ITEMS.PK_ID);


            ht.Add(ProgramParameters.ProgramID.ToString(), sSM_MENU_ITEMS.PK_ID);
            ht.Add(ProgramParameters.ProgramName.ToString(), sSM_MENU_ITEMS.FORM_LABEL);
            ht.Add(ProgramParameters.AccessibleName.ToString(), sSM_MENU_ITEMS.PK_COLUMN_NAME.ToUpper());
            ht.Add(ProgramParameters.ReportName.ToString(), sSM_MENU_ITEMS.REPORT_NAME);

            ht.Add(ProgramParameters.ModifyURL.ToString(), modifyURL);
            ht.Add(ProgramParameters.DeleteURL.ToString(), deleteURL);
            ht.Add(ProgramParameters.AddURL.ToString(), addURL);
            ht.Add(ProgramParameters.ListURL.ToString(), listURL);
            ht.Add(ProgramParameters.HeaderName.ToString(), HeaderName);
            ht.Add(ProgramParameters.AddFlag.ToString(), sSM_MENU_ITEMS.ENABLED_FLAG.ToString());
            ht.Add(ProgramParameters.FormCode.ToString(), sSM_MENU_ITEMS.FORM_CODE.ToString());
            return ht;
        }
        public static Hashtable GetMenuDetail(int programID)
        {
            Hashtable ht = new Hashtable();
            //Get Programs Information
            SSM_MENU_ITEMS sSM_MENU_ITEMS = new SSM_MENU_ITEMS();
            string tableName = string.Empty;

            using (IRepository<SSM_MENU_ITEMS> userCtx = new DataRepository<SSM_MENU_ITEMS>())
            {
                sSM_MENU_ITEMS = userCtx.Find(r =>
                    (r.PK_ID == programID)
                    ).SingleOrDefault();
            }

            dtData = DBMethod.ExecuteQuery(FINSQL.GetMenuDtl(programID)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    tableName = dtData.Rows[0]["list_table_name"].ToString();
                }
            }

            string modifyURL = "";
            string deleteURL = "";
            string addURL = "";
            string listURL = "";


            string pageURL = sSM_MENU_ITEMS.ENTRY_PAGE_OPEN;
            string programIDTag = QueryStringTags.ProgramID.ToString() + "=" + programID.ToString();
            string programModeTagUpdate = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Update.ToString().Substring(0, 1) + "'";
            string programModeTagDelete = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Delete.ToString().Substring(0, 1) + "'";
            string str_ReportName = "'" + QueryStringTags.ReportName.ToString() + "=" + sSM_MENU_ITEMS.REPORT_NAME.ToString() + "'";
            string str_ReportName_OL = "";
            if (sSM_MENU_ITEMS.ATTRIBUTE3 != null)
            {
                str_ReportName_OL = "'" + QueryStringTags.ReportName_OL.ToString() + "=" + sSM_MENU_ITEMS.ATTRIBUTE3.ToString() + "'";
            }
            else
            {
                str_ReportName_OL = "'" + QueryStringTags.ReportName_OL.ToString() + "=" + "" + "'";
            }

            string programKeyTag = "'" + QueryStringTags.ID.ToString() + "='||" + tableName + "." + sSM_MENU_ITEMS.ATTRIBUTE4;
            string HeaderName = sSM_MENU_ITEMS.FORM_LABEL;

            modifyURL = string.Format("'{0}'||{1}||{2}||{1}||{3}||{1}||{4}||{1}||{5} as ModifyURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagUpdate, programKeyTag, str_ReportName, str_ReportName_OL);

            deleteURL = string.Format("'{0}'||{1}||{2}||{1}||{3}||{1}||{4}||{1}||{5} as DeleteURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagDelete, programKeyTag, str_ReportName, str_ReportName_OL);


            //addURL = pageURL + "?";
            //addURL += "ProgramID=" + sSM_MENU_ITEMS.PK_ID.ToString() + "&";
            //addURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
            //addURL += QueryStringTags.ID.ToString() + "=0" + "&";
            //addURL += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
            //addURL += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
            //addURL += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
            //addURL += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
            //addURL += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "'";

            addURL = string.Format(pageURL + "?" + QueryStringTags.ID.ToString() + "={0}" + "&" + QueryStringTags.Mode.ToString() + "={1}" + "&" + QueryStringTags.ProgramID.ToString() + "={2}", 0, ProgramMode.Add.ToString().Substring(0, 1), programID);
            listURL = string.Format(sSM_MENU_ITEMS.MENU_URL + "?" + QueryStringTags.ProgramID.ToString() + "={0}", programID);


            ht.Add(ProgramParameters.ProgramID.ToString(), programID);
            ht.Add(ProgramParameters.ProgramName.ToString(), sSM_MENU_ITEMS.FORM_LABEL);
            ht.Add(ProgramParameters.AccessibleName.ToString(), sSM_MENU_ITEMS.PK_COLUMN_NAME.ToUpper());
            ht.Add(ProgramParameters.ReportName.ToString(), sSM_MENU_ITEMS.REPORT_NAME);

            ht.Add(ProgramParameters.ModifyURL.ToString(), modifyURL);
            ht.Add(ProgramParameters.DeleteURL.ToString(), deleteURL);
            ht.Add(ProgramParameters.AddURL.ToString(), addURL);
            ht.Add(ProgramParameters.ListURL.ToString(), listURL);
            ht.Add(ProgramParameters.HeaderName.ToString(), HeaderName);
            ht.Add(ProgramParameters.AddFlag.ToString(), sSM_MENU_ITEMS.ENABLED_FLAG.ToString());
            ht.Add(ProgramParameters.FormCode.ToString(), sSM_MENU_ITEMS.FORM_CODE.ToString());
            return ht;
        }
        public static Hashtable GetMultiMenuDetail(int programID)
        {
            Hashtable ht = new Hashtable();
            //Get Programs Information
            //AON_MENU_MST aON_MENU_MST = new AON_MENU_MST();

            //using (IRepository<AON_MENU_MST> userCtx = new DataRepository<AON_MENU_MST>())
            //{
            //    aON_MENU_MST = userCtx.Find(r =>
            //        (r.MENU_KEY_ID == programID)
            //        ).SingleOrDefault();
            //}


            //string modifyURL = "";
            //string deleteURL = "";
            //string addURL = "";
            //string listURL = "";
            //string approveURL = "";

            //string pageURL = aON_MENU_MST.ENTRY_PAGE_OPEN;
            //string programIDTag = QueryStringTags.ProgramID.ToString() + "=" + programID.ToString();
            //string programModeTagUpdate = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Update.ToString().Substring(0, 1) + "'";
            //string programModeTagApprove = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "'";
            //string programModeTagDelete = "'" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Delete.ToString().Substring(0, 1) + "'";
            //string programKeyTag = "'" + QueryStringTags.ID.ToString() + "='||" + aON_MENU_MST.PK_COLUMN_NAME;
            //string HeaderName = aON_MENU_MST.MENU_DESCRIPTION;

            //modifyURL = string.Format("'{0}'||{1}||{2}||{1}||{3} as ModifyURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagUpdate, programKeyTag);

            //approveURL = string.Format("'{0}'||{1}||{2}||{1}||{3} as ApproveURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagApprove, programKeyTag);

            //deleteURL = string.Format("'{0}'||{1}||{2}||{1}||{3} as DeleteURL ", pageURL + "?" + programIDTag, "Chr(38)", programModeTagDelete, programKeyTag);



            //addURL = string.Format(pageURL + "?" + QueryStringTags.ID.ToString() + "={0}" + "&" + QueryStringTags.Mode.ToString() + "={1}" + "&" + QueryStringTags.ProgramID.ToString() + "={2}", 0, ProgramMode.Add.ToString().Substring(0, 1), programID);
            //listURL = string.Format(aON_MENU_MST.MENU_URL + "?" + QueryStringTags.ProgramID.ToString() + "={0}", programID);


            //ht.Add(ProgramParameters.ProgramID.ToString(), programID);
            //ht.Add(ProgramParameters.ProgramName.ToString(), aON_MENU_MST.MENU_DESCRIPTION);
            //ht.Add(ProgramParameters.AccessibleName.ToString(), aON_MENU_MST.PK_COLUMN_NAME.ToUpper());


            //ht.Add(ProgramParameters.ModifyURL.ToString(), modifyURL);
            //ht.Add(ProgramParameters.ApproveURL.ToString(), approveURL);
            //ht.Add(ProgramParameters.DeleteURL.ToString(), deleteURL);
            //ht.Add(ProgramParameters.AddURL.ToString(), addURL);
            //ht.Add(ProgramParameters.ListURL.ToString(), listURL);
            //ht.Add(ProgramParameters.HeaderName.ToString(), HeaderName);
            //ht.Add(ProgramParameters.AddFlag.ToString(), aON_MENU_MST.ADD_FLAG.ToString());
            return ht;
        }

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetMenuData(ref DropDownList ddlList)
        {
            using (IRepository<SSM_MENU_ITEMS> userCtx = new DataRepository<SSM_MENU_ITEMS>())
            {
                dtDropDownData = DataSetLinqOperators.CopyToDataTable<SSM_MENU_ITEMS>(userCtx.Find(r => (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1")).Distinct());
            }

            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PARENT_MENU_CODE, FINColumnConstants.PARENT_MENU_CODE, dtDropDownData, true, false);
        }


    }
}
