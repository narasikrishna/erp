﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL
{
    public class FINSessionConstants
    {
        public const string UserID = "UserID";
        public const string UserName = "UserName";
        public const string PassWord = "PassWord";
        public const   string UserGroupID = "UserGroupID";

        public const string GridData = "GridData";
        public const string GridDataUR = "GridDataUR";
        public const string MenuData = "MenuData";
        public const string LotGridData = "LotGridData";
        public const string ScopeGridData = "ScopeGridData";

        public const string ModuleName = "ModuleName";
        public const string ModuleDescription = "ModuleDescription";
        public const string ORGCurrency = "ORGCurrency";
        public const string ORGCurrencySymbol = "ORGCurrencySymbol";

        public const string Sel_Lng = "Sel_Lng";
        public const string LevelCode = "LevelCode";
        public const string EMPDept = "EMPDept";
        public const string Attachments = "Attachments";
        public const string WindowWidth = "WindowWidth";
        public const string WindowHeight = "WindowHeight";


    }
}
