﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FIN.DAL;
using System.Collections;

namespace FIN.BLL
{
   public  class UserUtility_BLL
    {

        //Eval("PERIOD_START_DATE","{0:dd/MM/yyyy}")

       public static SortedList DateRangeValidate(DateTime? startdate ,DateTime? enddate, string appMode)
       {
           SortedList errorCollection = new SortedList();



           if (appMode != FINAppConstants.Add)
           {
               if (startdate >= enddate)
               {

                   errorCollection.Add("period_validation", " start date cannot be greater than  end date");
               }
           }


           return errorCollection;
       }

       public static  string fn_convertdate(string datevalue)
       {
          
           string str_retdate = string.Empty;
           if (datevalue.Length > 0)
           {
               string[] str_temp = datevalue.Split(' ');
               str_temp = str_temp[0].Split('/');
               str_retdate = str_temp[1] + "/" + str_temp[0] + "/" + str_temp[2];
           }
           return  str_retdate ;
       }

       public static string fn_convertdatetime(string datevalue)
       {
           string str_retdate = string.Empty;
           if (datevalue.Length > 0)
           {
               string[] str_temp = datevalue.Split('/');
               str_retdate = str_temp[1] + "/" + str_temp[0] + "/" + str_temp[2];
           }
           return str_retdate;
       }
       public static SortedList DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
       {
           SortedList ErrorCollection = new SortedList();
           if (dtGridData.Select(strCondition).Length > 0)
           {
               ErrorCollection.Add(strMessage, strMessage);
           }
           return ErrorCollection;
       }
    }
}
