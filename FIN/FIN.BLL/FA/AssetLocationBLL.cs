﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class AssetLocationBLL
    {
        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        string prevCateName = string.Empty;

        private AST_LOCATION_MST _AST_LOCATION_MST;
        public AST_LOCATION_MST aST_LOCATION_MST
        {
            get { return _AST_LOCATION_MST; }
            set { _AST_LOCATION_MST = value; }
        }

        public void ValidateGrid(DataTable dtGridData, string mode)
        {
            if (mode == FINAppConstants.Add)
            {
                ValidateDuplicateLocationName(dtGridData);
            }
            IsDBDuplicateLocationName();
        }

        private void ValidateDuplicateLocationName(DataTable dtGridData)
        {
            ErrorCollection.Remove(FINColumnConstants.LOCATION_NAME + "Duplicate");

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        prevCateName = dtGridData.Rows[i][FINColumnConstants.LOCATION_NAME].ToString();
                        if (prevCateName == aST_LOCATION_MST.LOCATION_NAME)
                        {
                            ErrorCollection.Add(FINColumnConstants.LOCATION_NAME + "Duplicate", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Location Name "));
                        }
                    }
                }
            }
        }

        public void IsDBDuplicateLocationName()
        {
            ErrorCollection.Remove(FINColumnConstants.LOCATION_NAME + "DuplicateDB");
            
            dtData = DBMethod.ExecuteQuery(FA_SQL.IsValueExistsBasedOrganization(FINTableConstant.AST_LOCATION_MST, FINColumnConstants.ASSET_LOCATION_ID, aST_LOCATION_MST.ASSET_LOCATION_ID, FINColumnConstants.LOCATION_NAME, aST_LOCATION_MST.LOCATION_NAME)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["count"].ToString());
                }
            }
            if (count > 0)
            {
                ErrorCollection.Add(FINColumnConstants.DPRN_NAME + "DuplicateDB", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Location Name "));
            }
        }

        public static DataSet GetAssetTransferReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.FA.Asset_DAL.getAssetTransferReportData());
        }

    }
}
