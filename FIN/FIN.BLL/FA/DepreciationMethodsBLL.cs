﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class DepreciationMethodsBLL
    {
        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        string prevCateName = string.Empty;

        private AST_DEPRECIATION_METHOD_MST _AST_DEPRECIATION_METHOD_MST;
        public AST_DEPRECIATION_METHOD_MST aST_DEPRECIATION_METHOD_MST
        {
            get { return _AST_DEPRECIATION_METHOD_MST; }
            set { _AST_DEPRECIATION_METHOD_MST = value; }
        }

        public void ValidateGrid(DataTable dtGridData, string mode, int rowIndex)
        {
            if (mode == FINAppConstants.Add)
            {
                ValidateDuplicateDepreciationName(dtGridData, rowIndex);
            }
            IsDBDuplicateDepreciationName();
        }

        private void ValidateDuplicateDepreciationName(DataTable dtGridData, int rowIndex)
        {
            ErrorCollection.Remove(FINColumnConstants.MINOR_CATEGORY_NAME + "Duplicate");

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (rowIndex != i)
                        {
                            prevCateName = dtGridData.Rows[i][FINColumnConstants.DPRN_NAME].ToString();
                            if (prevCateName == aST_DEPRECIATION_METHOD_MST.DPRN_NAME)
                            {
                                ErrorCollection.Add(FINColumnConstants.DPRN_NAME + "Duplicate", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Depreciation Method Name "));
                            }
                        }
                    }
                }
            }
        }

        public void IsDBDuplicateDepreciationName()
        {
            ErrorCollection.Remove(FINColumnConstants.DPRN_NAME + "DuplicateDB");

            dtData = DBMethod.ExecuteQuery(FA_SQL.IsValueExistsBasedOrganization(FINTableConstant.AST_DEPRECIATION_METHOD_MST, FINColumnConstants.DPRN_METHOD_MST_ID, aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID, FINColumnConstants.DPRN_NAME, aST_DEPRECIATION_METHOD_MST.DPRN_NAME)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["count"].ToString());
                }
            }
            if (count > 0)
            {
                ErrorCollection.Add(FINColumnConstants.DPRN_NAME + "DuplicateDB", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Depreciation Method Name "));
            }
        }
    }
}
