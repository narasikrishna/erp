﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Linq;
using FIN.DAL.FA;
using FIN.DAL;
using VMVServices.Services.Data;
using VMVServices.Services.Business;


namespace FIN.BLL.FA
{
    public class AssetBLL
    {
        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        string prevCateName = string.Empty;

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        private AST_ASSET_MST _AST_ASSET_MST;
        public AST_ASSET_MST aST_ASSET_MST
        {
            get { return _AST_ASSET_MST; }
            set { _AST_ASSET_MST = value; }
        }
        public void Validate()
        {
            IsDBDuplicateAsset();
        }
        private void IsDBDuplicateAsset()
        {
            ErrorCollection.Remove(FINColumnConstants.ASSET_NAME + "DuplicateDB");

            dtData = DBMethod.ExecuteQuery(FA_SQL.IsValueExistsBasedOrganization(FINTableConstant.AST_ASSET_MST, FINColumnConstants.ASSET_MST_ID, aST_ASSET_MST.ASSET_MST_ID, FINColumnConstants.ASSET_NAME, aST_ASSET_MST.ASSET_NAME)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["count"].ToString());
                }
            }
            if (count > 0)
            {
                ErrorCollection.Add(FINColumnConstants.ASSET_NAME + "DuplicateDB", "Asset Name already exists");
            }
        }

        public static AST_ASSET_MST getClassEntity(string str_Id)
        {
            AST_ASSET_MST obj_AST_ASSET_MST = new AST_ASSET_MST();
            using (IRepository<AST_ASSET_MST> userCtx = new DataRepository<AST_ASSET_MST>())
            {
                obj_AST_ASSET_MST = userCtx.Find(r =>
                    (r.ASSET_MST_ID.ToString() == str_Id)
                    ).SingleOrDefault();
            }
            return obj_AST_ASSET_MST;
        }

        public static DataSet GetAssetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.FA.Asset_DAL.getAssetReportData());
        }

        public static void GetAssetNumberName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Asset_DAL.GetAssetNumberAssetName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "asset_name", "asset_mst_id", dtDropDownData, false, true);
        }
        public static void GetAssetLocation(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Asset_DAL.GetAssetLocation()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOCATION_NAME", "ASSET_LOCATION_ID", dtDropDownData, false, true);
        }
    }
}
