﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class AssetCategoryBLL
    {

        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        string prevCateName = string.Empty;
        string screenMode = string.Empty;


        private AST_MAJOR_CATEGORY_MST _AST_MAJOR_CATEGORY_MST;
        public AST_MAJOR_CATEGORY_MST AST_MAJOR_CATEGORY_MST
        {
            get { return _AST_MAJOR_CATEGORY_MST; }
            set { _AST_MAJOR_CATEGORY_MST = value; }

        }
        private AST_MINOR_CATEGORY_DTL _AST_MINOR_CATEGORY_DTL;
        public AST_MINOR_CATEGORY_DTL AST_MINOR_CATEGORY_DTL
        {
            get { return _AST_MINOR_CATEGORY_DTL; }
            set { _AST_MINOR_CATEGORY_DTL = value; }

        }

        public void Validate()
        {
            IsDBDuplicateMajorCategory();
        }
        public void ValidateGrid(DataTable dtGridData, string mode, int rowIndex)
        {
            if (mode == FINAppConstants.Add)
            {
                ValidateDuplicateMinorCategory(dtGridData, rowIndex);
            }
            IsDBDuplicateMinorCategory();
        }

        private void ValidateDuplicateMinorCategory(DataTable dtGridData, int rowIndex)
        {
            ErrorCollection.Remove(FINColumnConstants.MINOR_CATEGORY_NAME + "Duplicate");

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (rowIndex != i)
                        {
                            prevCateName = dtGridData.Rows[i][FINColumnConstants.MINOR_CATEGORY_NAME].ToString();
                            if (prevCateName == AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME)
                            {
                                ErrorCollection.Add(FINColumnConstants.MINOR_CATEGORY_NAME + "Duplicate", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Minor Category Name "));
                            }
                        }
                    }
                }
            }
        }
        private void IsDBDuplicateMinorCategory()
        {
            ErrorCollection.Remove(FINColumnConstants.MINOR_CATEGORY_NAME + "DuplicateDB");

            dtData = DBMethod.ExecuteQuery(FA_SQL.IsValueExists(FINTableConstant.AST_MINOR_CATEGORY_DTL, FINColumnConstants.MINOR_CATEGORY_ID, AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID, FINColumnConstants.MINOR_CATEGORY_NAME, AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["count"].ToString());
                }
            }
            if (count > 0)
            {
                ErrorCollection.Add(FINColumnConstants.MINOR_CATEGORY_NAME + "DuplicateDB", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Minor Category Name "));
            }
        }
        private void IsDBDuplicateMajorCategory()
        {
            ErrorCollection.Remove(FINColumnConstants.CATEGORY_NAME);

            dtData = DBMethod.ExecuteQuery(FA_SQL.IsValueExistsBasedOrganization(FINTableConstant.AST_MAJOR_CATEGORY_MST, FINColumnConstants.MAJOR_CATEGORY_ID, AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID, FINColumnConstants.CATEGORY_NAME, AST_MAJOR_CATEGORY_MST.CATEGORY_NAME)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["count"].ToString());
                }
            }
            if (count > 0)
            {
                ErrorCollection.Add(FINColumnConstants.CATEGORY_NAME, VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.ALREADY_EXISTS, "Major Category Name "));
            }
        }
        //private void ValidateMinorCategoryName(DataTable dtGridData)
        //{
        //    ErrorCollection.Remove(FINColumnConstants.MINOR_CATEGORY_NAME);

        //    if (VMVServices.Helpers.Validation.IsEmpty((string)AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME))
        //    {
        //        ErrorCollection.Add(FINColumnConstants.MINOR_CATEGORY_NAME, VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, "Minor Category Name "));
        //    }
        //    else
        //    {
        //        ValidateDuplicateMinorCategory(dtGridData);
        //    }

        //}
        //private void ValidateMajorCategoryName()
        //{
        //    ErrorCollection.Remove(FINColumnConstants.CATEGORY_NAME);

        //    if (VMVServices.Helpers.Validation.IsEmpty((string)AST_MAJOR_CATEGORY_MST.CATEGORY_NAME))
        //    {
        //        ErrorCollection.Add(FINColumnConstants.CATEGORY_NAME, VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.CANNOT_EMPTY, "Major Category Name "));
        //    }
        //    else
        //    {
        //        IsDuplicateData();
        //    }

        //}
    }
}
