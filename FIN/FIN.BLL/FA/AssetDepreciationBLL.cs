﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Linq;
using FIN.DAL.FA;
using FIN.DAL;
using VMVServices.Services.Data;
using VMVServices.Services.Business;


namespace FIN.BLL.FA
{
    public class AssetDepreciationBLL
    {
        DataTable dtData = new DataTable();
        int count = 0;
        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        string prevCateName = string.Empty;

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        private AST_ASSET_DEPRECIATION_DTL _AST_ASSET_DEPRECIATION_DTL;
        public AST_ASSET_DEPRECIATION_DTL aST_ASSET_DEPRECIATION_DTL
        {
            get { return _AST_ASSET_DEPRECIATION_DTL; }
            set { _AST_ASSET_DEPRECIATION_DTL = value; }
        }
       
        public static AST_ASSET_DEPRECIATION_DTL getClassEntity(string str_Id)
        {
            AST_ASSET_DEPRECIATION_DTL obj_AST_ASSET_DEPRECIATION_DTL = new AST_ASSET_DEPRECIATION_DTL();
            using (IRepository<AST_ASSET_DEPRECIATION_DTL> userCtx = new DataRepository<AST_ASSET_DEPRECIATION_DTL>())
            {
                obj_AST_ASSET_DEPRECIATION_DTL = userCtx.Find(r =>
                    (r.ASSET_DEPRECIATION_ID.ToString() == str_Id)
                    ).SingleOrDefault();
            }
            return obj_AST_ASSET_DEPRECIATION_DTL;
        }

        public static DataSet GetAssetDepreciationReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.FA.Asset_DAL.getAssetDepreciationReportData());
        }

    }
}
