﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Collections;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class BasecurrencyBLL
    {
        public static SortedList Validate(long currency_id, DateTime eff_date,long RecordID)
        {
            SortedList errorCollection = new SortedList();

            DataTable dt = DBMethod.ExecuteQuery(FA_SQL.GetBaseCurrencyEffectiveDate(currency_id, eff_date, RecordID)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString().Length > 0)
                {
                    
                        errorCollection.Add("period_validation", " Effective date must be gerater then Last Effective date");
                    
                }
            }


             dt = DBMethod.ExecuteQuery(FA_SQL.GetBaseCurrencyAssetcreateDate(eff_date)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString().Length > 0)
                {

                    errorCollection.Add("Asset_Date_Validation", " Effective Date Must Be Greater Then Asset Date");

                }
            }

            

            return errorCollection;
        }

    }
}
