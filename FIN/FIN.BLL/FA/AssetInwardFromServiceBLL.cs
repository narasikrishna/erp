﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Collections;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class AssetInwardFromServiceBLL
    {
        public static SortedList Validate(long outward_service_id, DateTime eff_date)
        {
            SortedList errorCollection = new SortedList();

            DataTable dt = DBMethod.ExecuteQuery(FA_SQL.GetAssetOutwardServiceDate(outward_service_id, eff_date)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString().Length > 0)
                {

                    errorCollection.Add("Date_Validation", " Inward Date must be greater than expected Delivery Date");

                }
            }
            return errorCollection;
        }
    }
}
