﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using FIN.DAL.FA;
using FIN.DAL;
using FIN.BLL;

using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace FIN.BLL.FA
{
    public class AssetoutwardtoserviceBLL
    {

        private AST_ASSET_OUTWARD_SERVICE_DTL _aST_ASSET_OUTWARD_SERVICE_DTL;
        public AST_ASSET_OUTWARD_SERVICE_DTL aST_ASSET_OUTWARD_SERVICE_DTL
        {
            get { return _aST_ASSET_OUTWARD_SERVICE_DTL; }
            set { _aST_ASSET_OUTWARD_SERVICE_DTL = value; }

        }
        int count = 0;
        public SortedList Validate(long asset_mst_id, long RecordID, string appMode)
        {
            SortedList errorCollection = new SortedList();

            errorCollection.Remove("period_validation");
            errorCollection.Remove("Asset_outwarddate_Validation");


            DataTable dt = DBMethod.ExecuteQuery(FA_SQL.Assetoutwardtoserviceoutwarddate(asset_mst_id, RecordID, aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE, appMode)).Tables[0];

            if (appMode != FINAppConstants.Add)
            {
                if (aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE <= aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE)
                {
                    errorCollection.Add("period_validation", VMVServices.Helpers.Validation.FormatMessage(ValidationStringCollection.EXPECTED_SHOULD_BE_GREATER, ""));
                    //errorCollection.Add("period_validation", " Outward date cannot be greater than  Expected Delivery date");
                }
            }



            dt = DBMethod.ExecuteQuery(FA_SQL.Assetoutwardtoserviceoutwarddate(asset_mst_id, RecordID, aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE, appMode)).Tables[0];


            if (dt.Rows.Count > 0)
            {
                count = int.Parse(dt.Rows[0]["count"].ToString());
                if (count > 0)
                {
                    errorCollection.Add("Asset_outwarddate_Validation", " Outward date must be greater than last inward date");
                }
                //if (dt.Rows[0][0].ToString().Length == 0)
                //{
                //    errorCollection.Add("Asset_outwarddate_Validation", " Outward date must be greater than last inward date");
                //}
            }



            return errorCollection;
        }

    }
}
