﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL
{
    public static class FINMessageConstatns
    {
        public const string ORA02292 = "Please delete the child records of this data";
        public const string ValidationFormPath = "../Validation.aspx";
        public const string ValidationReportPath = "../../Validation.aspx";
        public const string CrystalReportViewerPath = "../RPTCrystalReportViewer.aspx";// "../../RPTCrystalReportViewer.aspx";
        public const string CrystalReportViewerReportPath = "../RPTCrystalReportViewer.aspx";
        public const string AckofRegReportPath = "Reports//Admission//RPTAcknowledgemntforRegistration.rpt";
        public const string EntranceCallLetterReportPath = "Reports//Admission//RPTCallLetterforEntrance.rpt";
        public const string EntranceHallTicketReportPath = "Reports//Admission//RPTHallTicketforEntrance.rpt";
        public const string StudentIdCardReportPath = "Reports//Admission//RPTStudentIDCard.rpt";

        //Report Path
        public const string RPTCourseGroupPath = "Reports//Academic//RPTCourseGroup.rpt";

        public const string BarCodeImage = "BarCodeImage";
        public const string RecordAlreadyExists = "Record Already Exists";
        public const string ElementCodeAlreadyExists = "Element Code Already Exists";
        public const string SegmentNameAlreadyExists = "Segment Name Already Exists";
        public const string Grade = "Grade Already Exists";
        public const string DepartmentName = "Department Name Already Exists";
        public const string DesignationName = "Designation Name Already Exists";
        public const string DesignationShortName = "Designation Short Name Already Exists";
        public const string JobCode = "Job Code Already Exists";
        public const string Particulars = "Particulars Already Exists";
        public const string ResponsibilityType = " Combination of Responsibility Type and Name Already Exists";
        public const string TraingReq_DeptEmpGrp = " Combination of Department and Employee Group Already Exists";
        public const string ResponsibilityType1 = " This Job Code & Responsibility Type Combination Already in use.";
        public const string VacationName = "Vacation Name Already Exists";
        public const string Code = "Code Already Exists";
        public const string LevelDesc = "Level Description Already Exists";
        public const string NoRecordsFound = "No Records Found";
        public const string RevalType = "Revaluation Type Already Exists";

        public const string QualificationGrid = "Qualification";
        public const string CourseGrid = "Course";
        public const string Exam = "Exam";
        public const string ThirdPartyType = "Third Party Type";
        public const string CourseSubject = "Course Subject";
        public const string TermName = "TermName";

        public const string EventDet = "Event Details";
        public const string EmployeeMaster = "Employee Master";
        public const string StructureDetails = "Structure";
        public const string IssueDate = "Issue Date";
        public const string CalendarDetails = "Calendar";
        public const string Facilty = "Facilty";
        public const string Subject = "Subject";
        public const string SectionDetails = "Section Details";
        public const string CourseTerm = "Course Term";
        public const string GroupAppdtl = "Selected Application";
        //public const string Contracter = "Contracter";
        public const string Student_Mark = "Student Marks";
        public const string Seat_Plan = "Seat Plan";
        public const string PaperAllocationDetails = "Paper Allocation Details";
        public const string RequiredTechnologyDetails = "Required Technology Details";
        public const string ProposedDesigDetails = "Proposed Name & Designation";
        public const string VehicleDetails = "Vehicle Details";
        public const string LookUpDetails = "LookUp Details";
        public const string ItemDetails = "Item ";
        public const string AttendanceType_Present = "Present";
        public const string AttendanceType_Leave = "Leave";


        public const string Exchange_Type = "Exchange Type";
        public const string Discard_Type = "Discard Type";


        public const string AssetCategory = "Asset Category ";
        public const string DepreciationMethod = "Depreciation Method ";
        public const string AssetLocation = "Asset Location ";
        public const string AssetTransfer = "Asset Transfer ";
        public const string BaseCurrency = "Base Currency ";
        public const string ChangeAssetCategory = "Change Asset Category";
        public const string DepreciationAssetCategories = "Depreciation - Asset Categories ";
        public const string Assetoutwardtoservice = "Asset outward to service ";
        public const string Asset = "Asset ";
        public const string AssetDiscard = "Asset Discard ";
        public const string AssetInwardsFromService = "Asset Inwards From Service";
        public const string AssetDepreciationToGL = "Asset Depreciation To GL";
        public const string Currency = "Currency ";

        public const string PER_APP_EMP_ID = "APP Employee Number";
        public const string PO_Number = "PO Number is ";

        public const string CompDesc = "Competency Name & Level Description already Exist";
        public const string GroupDesc = "Group Description already Exists";
        public const string GradeDesc = "Grade Details Already Exists";


        public const string Leave_Approved_Alert = "ALRT_CDE-0000000018";
        public const string Document_Expired_Alert = "ALRT_CDE-0000000006";
        public const string Employee_Work_Alert = "ALRT_CDE-0000000007";
        public const string Employee_Permit_Alert = "ALRT_CDE-0000000012";

        public const string Leave_Acting_Emp_Alert = "ALRT_CDE-0000000013";
        public const string Annual_Leave_payment_Alert = "ALRT_CDE-0000000019";
        public const string Initiate_Appraisal_Alert = "ALRT_CDE-0000000020";
        public const string Appraiser_following_employees_Alert = "ALRT_CDE-0000000021";
        public const string Appraisal_Assesment_Appraiser_Alert = "ALRT_CDE-0000000022";
        public const string Appraisal_Assesment_Appraisee_Alert = "ALRT_CDE-0000000023";

        public const string RESIGNATION_REQ_ALERT = "ALRT_CDE-0000000024";
        public const string RESIGNATION_HANDOVER_HR_ALERT = "ALRT_CDE-0000000025";
        public const string RESIGNATION_HANDOVER_EMP_ALERT = "ALRT_CDE-0000000026";


        public const string INTERVIEW_ALERT = "ALRT_CDE-0000000027";
        public const string Interview_Schedule_Applicant_ALERT = "ALRT_CDE-0000000028";
        public const string APPLICANT_VERIFICATION_ALERT = "ALRT_CDE-0000000030";

        public const string PERMISSION_ALERT = "ALRT_CDE-0000000031";
        public const string DRAFT_RFQ_ALERT = "ALRT_CDE-0000000032";
        public const string INVOICE_ALERT = "ALRT_CDE-0000000033";

        public const string PAYMENT_ALERT = "ALRT_CDE-0000000034";
        public const string PERMISSION_EMP_ALERT = "ALRT_CDE-0000000036";
        public const string RFQ_ALERT = "ALRT_CDE-0000000037";
        public const string Emp_probation_ALERT = "ALRT_CDE-0000000040";

        public const string SectionName = "Section Name Already Exists";
    }
}

