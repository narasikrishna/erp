﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="VacancyChart.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.VacancyChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../Scripts/ORGChart/jquery.orgchart.css" />
    <script type="text/javascript" src="../Scripts/ORGChart/jquery.orgchart.js"></script>
    <script type="text/javascript">

        function fn_showGraph() {
            $("#organisation").orgChart({ container: $("#main") });
        }
        $(window).bind("load", function () {
            $("#organisation").orgChart({ container: $("#main") });
        });
    </script>
    <style type="text/css">
        .ChartFont
        {
            font-family: Verdana;
            font-size: 8px;
            width: 100%;
            height: 300px;
        }
        
        .imgSize
        {
            width: 150px;
            height: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divClear_10" style="width: 60%">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 150px" id="lblDepartmentName">
            Department Name
        </div>
        <div class="divtxtBox LNOrient" style="width: 450px">
            <asp:DropDownList ID="ddlDeptname" CssClass="validate[required]  RequiredField ddlStype"
                runat="server" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlDeptname_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div id="main" style="overflow: auto; height: 480px;">
    </div>
    <div id="left" runat="server" class="ChartFont" style="display: none;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>