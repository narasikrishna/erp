﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="OrganizationTree_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.OrganizationTree_DB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../Scripts/ORGChart/jquery.orgchart.css" />
    <script type="text/javascript" src="../Scripts/ORGChart/jquery.orgchart.js"></script>
    <script src="../Scripts/html2canvas.min.js" type="text/javascript"></script>
    <script src="../Scripts/html2canvas.svg.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function fn_showGraph() {
            $("#organisation").orgChart({ container: $("#main") });

        }
        $(window).bind("load", function () {
            $("#organisation").orgChart({ container: $("#main") });
        });
        function fn_PrintDiv(div_printDiv_id) {

            //Get the HTML of div

            var divElements = document.getElementById(div_printDiv_id).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;
            console.log(divElements);
            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
                          "<html><head><title></title></head><body>" +
                          divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;




        }

        function fn_img() {



            //  $('#main').toggleClass('rotated');

            html2canvas($('#main'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL("image/png")
                    window.open(img);
                }
            });



        }
       
    </script>
    <style type="text/css">
        .ChartFont
        {
            font-family: Verdana;
            font-size: 8px;
            width: 100%;
            height: 300px;
        }
        
        .imgSize
        {
            width: 150px;
            height: 150px;
        }
        
        @media print
        {
        
        
            div
            {
                page-break-inside: auto;
            }
            table
            {
                page-break-after: always;
                page-break-inside: auto;
            }
            tr
            {
                page-break-inside: auto;
                page-break-after: auto;
            }
            td
            {
                page-break-inside: auto;
                page-break-after: auto;
            }
            thead
            {
                display: table-header-group;
            }
            tfoot
            {
                display: table-footer-group;
            }
        
            #printSection *
            {
                visibility: visible;
            }
            #printSection
            {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                margin: 0 auto;
                width: 100%;
            }
        
        
        }
        
        .rotated
        {
            -webkit-transform: rotate(90deg); /* Chrome, Safari 3.1+ */
            -moz-transform: rotate(90deg); /* Firefox 3.5-15 */
            -ms-transform: rotate(90deg); /* IE 9 */
            -o-transform: rotate(90deg); /* Opera 10.50-12.00 */
            transform: rotate(90deg); /* Firefox 16+, IE 10+, Opera 12.10+ */
        }
    </style>
    <style>
        /*Now the CSS*/
        *
        {
            margin: 0;
            padding: 0;
        }
        
        .tree ul
        {
            padding-top: 20px;
            position: relative;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }
        
        .tree li
        {
            float: left;
            text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }
        
        /*We will use ::before and ::after to draw the connectors*/
        
        .tree li::before, .tree li::after
        {
            content: '';
            position: absolute;
            top: 0;
            right: 50%;
            border-top: 1px solid #ccc;
            width: 50%;
            height: 20px;
        }
        .tree li::after
        {
            right: auto;
            left: 50%;
            border-left: 1px solid #ccc;
        }
        
        /*We need to remove left-right connectors from elements without 
any siblings*/
        .tree li:only-child::after, .tree li:only-child::before
        {
            display: none;
        }
        
        /*Remove space from the top of single children*/
        .tree li:only-child
        {
            padding-top: 0;
        }
        
        /*Remove left connector from first child and 
right connector from last child*/
        .tree li:first-child::before, .tree li:last-child::after
        {
            border: 0 none;
        }
        /*Adding back the vertical connector to the last nodes*/
        .tree li:last-child::before
        {
            border-right: 1px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
        }
        .tree li:first-child::after
        {
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
        }
        
        /*Time to add downward connectors from parents*/
        .tree ul ul::before
        {
            content: '';
            position: absolute;
            top: 0;
            left: 50%;
            border-left: 1px solid #ccc;
            width: 0;
            height: 20px;
        }
        
        .tree li a
        {
            border: 1px solid #ccc;
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }
        
        /*Time for some hover effects*/
        /*We will apply the hover effect the the lineage of the element also*/
        .tree li a:hover, .tree li a:hover + ul li a
        {
            background: #c8e4f8;
            color: #000;
            border: 1px solid #94a0b4;
        }
        /*Connector styles on hover*/
        .tree li a:hover + ul li::after, .tree li a:hover + ul li::before, .tree li a:hover + ul::before, .tree li a:hover + ul ul::before
        {
            border-color: #94a0b4;
        }
        
        /*Thats all. I hope you enjoyed it.
Thanks :)*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="GridHeader LNOrient" style="width: 100%; height: 25px;">
        <div class=" LNOrient" style="padding-left: 10px; padding-top: 2px">
            Organisation Chart
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div class="divRowContainer">
        <div class="lblBox  LNOrient" style="width: 150px">
            Department Group
        </div>
        <div class="divtxtBox" style="float: left; width: 300px">
            <asp:DropDownList ID="ddlDeptGroup" CssClass=" ddlStype" runat="server" TabIndex="1"
                AutoPostBack="True" OnSelectedIndexChanged="ddlDeptGroup_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="colspace LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 150px">
            Department Name
        </div>
        <div class="divtxtBox LNOrient" style="width: 300px">
            <asp:DropDownList ID="ddlDeptname" CssClass=" ddlStype" runat="server" TabIndex="1">
            </asp:DropDownList>
        </div>
        <div class="lblBox LNOrient" style="width: 150px">
            <asp:Button ID="btnProcess" runat="server" Text="Process" OnClick="btnProcess_Click" />
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div class="DisplayFont">
        Please Click Show Button if Organisation Chart Is Not Showing
       <%-- <div class="colspace LNOrient" style="padding-left: 5px;">
        </div>--%>
        <asp:Button ID="btnShow" runat="server" Text="Show" OnClick="btnShow_Click" />
        &nbsp;
        <input id="btnImage" type="button" value="Image" onclick="fn_img()" />
        <input id="btnPrint" type="button" value="Print" onclick="fn_PrintDiv('main')" />
    </div>
    <div id="main" style="overflow: visible; width: 6000px; height: 1500px">
    </div>
    <div class="divClear_10">
        &nbsp;</div>
    <div id="left" runat="server" class="ChartFont tree" style="display: none">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
