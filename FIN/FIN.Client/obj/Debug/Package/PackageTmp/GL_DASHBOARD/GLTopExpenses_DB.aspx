﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLTopExpenses_DB.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLTopExpenses_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px;"  id="divHeader" runat="server">
        <div style="float: left; padding-left: 10px; padding-top: 2px">
            Top 5 Expenses
        </div>
         <div id="divPrintIcon" runat="server" style=" float: right; padding-right: 10px; padding-top: 2px">
            <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" onclick="btnGraphRep_Click"  />
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_E_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_E_Period_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_E_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_E_Year_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div id="dividExpenChrt" style="width: 98%;">
        <asp:Chart ID="chartExpenses" runat="server" Width="500px" Height="310px" 
            EnableViewState="True">
            <Series>
                <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="false"
                    XValueMember="group_name" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>

    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"  ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="group_name" HeaderText="Group Name"></asp:BoundField>
                <asp:BoundField DataField="BALANCE_AMT" HeaderText="Amount">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
