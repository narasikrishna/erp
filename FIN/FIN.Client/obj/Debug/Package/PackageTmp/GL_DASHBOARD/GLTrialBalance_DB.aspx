﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLTrialBalance_DB.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLTrialBalance_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="dividTB" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height:25px">
            <div style="float: left; padding-left:10px; padding-top:2px">
                Trail Balance
            </div>
            <div style="width: 120px; float: right; padding-right: 10px; padding-top:2px">
                <asp:DropDownList ID="ddl_TBFinPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddl_TBFinPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="width: 120px; float: right; padding-right: 10px; padding-top:2px">
                <asp:DropDownList ID="ddl_TBFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddl_TBFinYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           
        </div>
        <div class="divClear_10" style="width: 58%">
        </div>
        <div id="div8" style="height: 300px; width: 98%; overflow: auto">
            <asp:GridView ID="gvTrailBal" runat="server" CssClass="Grid" Width="100%" DataKeyNames="ACCT_CODE_ID"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="ACCT_CODE" HeaderText="Code"></asp:BoundField>
                    <asp:BoundField DataField="ACCT_CODE_DESC" HeaderText="Name"></asp:BoundField>
                    <asp:BoundField DataField="OP_BAL" HeaderText="Opening Balance">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TRANS_DR" HeaderText="Debit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TRANS_CR" HeaderText="Credit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CLOS_BAL" HeaderText="Closing Balance">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
