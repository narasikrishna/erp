﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="DeductionRequestEntry.aspx.cs" Inherits="FIN.Client.PER.DeductionRequestEntry" %>



<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtRequestNumber" Enabled="false" MaxLength="15" CssClass="txtBox" runat="server"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" TabIndex="2" ID="txtRequestDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtRequestDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 521px">
                <asp:DropDownList ID="ddlEmployeeName"  TabIndex="3" CssClass="validate[required]  RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="  width: 525px">
                <asp:TextBox ID="txtDept" Enabled="false"  CssClass=" RequiredField txtBox"
                    TabIndex="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div1">
                Designation
            </div>
            <div class="divtxtBox LNOrient" style="  width: 525px">
                <asp:TextBox ID="txtDesignation" Enabled="false"  TabIndex="5" CssClass=" RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divEligRemark" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblELigRemarks">
                Eligible Remarks
            </div>
            <div class="divtxtBox LNOrient" style="  width: 530px">
                <asp:TextBox ID="txtEligibleRemarks" Height="30px" TextMode="MultiLine" Enabled="true" CssClass="validate[required]  RequiredField txtBox"
                    TabIndex="6" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblStatus">
                Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlType" CssClass="validate[required]  ddlStype" runat="server"
                    TabIndex="7" AutoPostBack="True" 
                    onselectedindexchanged="ddlType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <%-- <div class="lblBox LNOrient" style="  width: 200px" id="lblStatus">
                Status
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlStatus" CssClass="validate[required]  ddlStype" runat="server" TabIndex="7">
                </asp:DropDownList>
            </div>--%>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 195px" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 149px">
                <asp:TextBox ID="txtAmount" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="8"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtAmount" />
            </div>
        </div>

         <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdRentded1">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                Actual Rent Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                   <asp:TextBox ID="txtActrentamt" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="9"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtActrentamt" />
            </div>
           
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 195px" id="Div3">
                Discount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDiscount" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtDiscount" />
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdRentded2">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div4">
              Recovery Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                   <asp:TextBox ID="txtRecovamt" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="11"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtRecovamt" />
            </div>
           
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 195px" id="Div5">
                Hold Recovery
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                 <asp:CheckBox ID="chkHold" runat="server" Checked="True" TabIndex="12" />
            </div>
        </div>

          <div class="divClear_10">
        </div>
        <div class="divRowContainer"  runat="server" id="IdRentded3">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div6">
                Reason for Holding
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtHoldreason" Height="30px" MaxLength = "500" CssClass=" txtBox" TextMode="MultiLine" runat="server"
                    TabIndex="13"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer"  runat="server" id="IdMobded1">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div7">
               Mobile Remarks
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtMobremarks" Height="30px" MaxLength= "500" CssClass=" txtBox" TextMode="MultiLine" runat="server"
                    TabIndex="14"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdMobded2">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div8">
               Mobile Bill Details
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtMobbilldtl" Height="20px" CssClass=" txtBox" MaxLength = "100" TextMode="MultiLine" runat="server"
                    TabIndex="15"></asp:TextBox>
            </div>
        </div>
         <%-- <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdAdminDed">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div9">
               No of Minutes
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:TextBox ID="txtNoofMin" CssClass=" txtBox"  runat="server"
                    TabIndex="8"></asp:TextBox>
                     <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtNoofMin" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdMedDed1">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div10">
              Medical Insurance Detail
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtMedinsdtl" Height="30px" MaxLength = "100" CssClass=" txtBox" TextMode="MultiLine" runat="server"
                    TabIndex="16"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="IdMedDed2">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div11">
              Medical Invoice Detail
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtMedInvdtl" Height="30px" MaxLength = "100" CssClass=" txtBox" TextMode="MultiLine" runat="server"
                    TabIndex="17"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox LNOrient" style="  width: 528px">
                <asp:TextBox ID="txtComments" Height="30px" CssClass=" txtBox" MaxLength = "500" TextMode="MultiLine" runat="server"
                    TabIndex="18"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblNoOfInstallments">
                No. Of Installments
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtNoOfInstallments" MaxLength="3" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" AutoPostBack="true" TabIndex="19" OnTextChanged="txtNoOfInstallments_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtNoOfInstallments" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblInstallmentAmount">
                Installment Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtInstallmentAmount" MaxLength="13" Enabled="true" CssClass="txtBox_N"
                    runat="server" TabIndex="20"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtInstallmentAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPaymentReleaseDate">
                Payment Release Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" TabIndex="21" ID="txtPaymentReleaseDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtPaymentReleaseDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPaymentReleaseDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblLoanStartDate">
                Loan Start Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox TabIndex="22" runat="server" ID="txtLoanStartDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtLoanStartDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLoanStartDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEnabledFlag">
                Enabled Flag
            </div>
            <div class="divtxtBox  LNOrient" style="  width: 150px">
                <asp:CheckBox ID="ChkEnabledFlag" runat="server" Checked="True" TabIndex="23" />
            </div>
        </div>
        <asp:HiddenField ID="hfdempdays_fromjoining" runat="server" />
        <asp:HiddenField ID="hfloaneligibleindays" runat="server" />
        <asp:HiddenField ID="hflastlnreqdt" runat="server" />
        <asp:HiddenField ID="hflnreeligibledt" runat="server" />
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDeleteas">
            <asp:HiddenField ID="hfallowloan" runat="server" />
            <cc2:ModalPopupExtender ID="mpAllowloan" runat="server" TargetControlID="hfallowloan"
                PopupControlID="pnlConfirm" CancelControlID="Button3" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" CssClass="lblBox LNOrient" runat="server" Text="Selected employee has not complete the eligible criteria for loan are you sure to allow for furthur loan"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" OnClick="btNo_Click" />
                                <asp:Button runat="server" CssClass="button" ID="Button3" Text="No" Width="60px"
                                    OnClick="btNo_Click" Style="display: none" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divEligibleCond">
            <asp:HiddenField ID="hfEligibleAllow" runat="server" />
            <cc2:ModalPopupExtender ID="mpeEligible" runat="server" TargetControlID="hfEligibleAllow"
                PopupControlID="pnlEligible" CancelControlID="Button2" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlEligible" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="Label1" CssClass="lblBox LNOrient" runat="server" Text="Selected employee Not Eligible for System condition. Do YouWant to Proceed"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="btn" ID="Button1" Text="Yes" Width="60px" OnClick="Button1_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="btn" ID="btnEligibleNO" Text="No" Width="60px"
                                    OnClick="btnEligibleNO_Click" />
                                <asp:Button runat="server" CssClass="btn" ID="Button2" Text="No" Style="display: none"
                                    Width="60px" OnClick="btnEligibleNO_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

       

    </script>
</asp:Content>
