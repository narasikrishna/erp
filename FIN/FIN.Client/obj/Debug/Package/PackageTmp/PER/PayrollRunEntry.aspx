﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollRunEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollRunEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                Payroll Period</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlPayPeriod" runat="server" TabIndex="3" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPayrollPeriod">
                Payroll ID</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlPayrollPeriod" runat="server" TabIndex="3" CssClass="EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayrollPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divtxtBox LNOrient" style="  width: 750px" align="right">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/Images/btnProcess.png"
                    OnClick="btnProcess_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnSave" runat="server" Text="Process" OnClick="btnProcess_Click" />--%>
                &nbsp;
                <asp:ImageButton ID="btnEmail" runat="server" ImageUrl="~/Images/btnSendEmail.png" Enabled="false"
                    OnClick="btnEmail_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnEmail" runat="server" Text="Send Email" OnClick="btnEmail_Click" Enabled="false" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="80%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_ID,PAY_DEPT_ID,WF" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" CssClass=" RequiredField  txtBox" Text='<%# Eval("dept_name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDepartmentName" Width="130px" runat="server" Text='<%# Eval("dept_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="13" runat="server" CssClass=" RequiredField  txtBox"
                                Enabled="false" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="13" runat="server" Visible="false"
                                CssClass="RequiredField   txtBox" Enabled="false"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("pay_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Deduction Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeductionAmount" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("pay_ded_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtDeductionAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDeductionAmount" Width="130px" MaxLength="13" runat="server"
                                Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtDeductionAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDeductionAmount" Width="130px" runat="server" Text='<%# Eval("pay_ded_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="13" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%# Eval("pay_net_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtNetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="13" runat="server" Visible="false"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtNetAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNetAmount" Width="130px" runat="server" Text='<%# Eval("pay_net_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click"
                                Visible="false" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvEmpDetails" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Visible="false" Width="80%" DataKeyNames="PAYROLL_DTL_ID,EMP_ID" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Employee Name">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpName" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Number">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNumber" Width="130px" runat="server" Text='<%# Eval("EMP_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("pay_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Deduction Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblDeductionAmount" Width="130px" runat="server" Text='<%# Eval("pay_ded_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="13" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%# Eval("pay_net_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtNetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="13" runat="server" Visible="false"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtNetAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNetAmount" Width="130px" runat="server" Text='<%# Eval("pay_net_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnEmpDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnEmpDetails_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="White" Width="90%" Height="90%"
                ScrollBars="Auto">
                <div class="divClear_10">
                </div>
                <div class="ConfirmForm">
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div3">
                            Employee
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 300px">
                            <asp:Label ID="lblEmp" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div12">
                            Department
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 200px">
                            <asp:Label ID="lbldept" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div4">
                            Designation
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 300px">
                            <asp:Label ID="lblDesig" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div4">
                            Date of Join
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 200px">
                            <asp:Label ID="lblDoj" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <table width="98%">
                            <tr>
                                <td>
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div8">
                                        Earnings
                                    </div>
                                </td>
                                <td>
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div9">
                                        Deduction
                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td style="width: 50%">
                                    <asp:GridView ID="gvEmployeePay" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                        OnRowDataBound="gvEmployeePay_RowDataBound" ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Visible="false" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Element">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" MaxLength="100" onkeyup="CalculateEar();"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" onkeyup="CalculateEar();" Visible="false"
                                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" onkeyup="CalculateEar();" MaxLength="100"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </td>
                                <td style="width: 50%">
                                    <asp:GridView ID="gvEmployeePay2" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        OnRowDataBound="gvEmployeePay2_RowDataBound" Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                        ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Element">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" MaxLength="100" onkeyup="CalculateDed();"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" onkeyup="CalculateDed();" Visible="false"
                                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" onkeyup="CalculateDed();" MaxLength="100"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px" id="Div16">
                                        Total Earnings
                                    </div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px; height: 50px" id="Div17">
                                        <asp:Label ID="lblTotalEar" runat="server"></asp:Label>
                                    </div>
                                </td>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px; height: 50px"
                                        id="Div14">
                                        Total Deduction
                                    </div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div15">
                                        <asp:Label ID="lblTotDed" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div11">
                                        Net Amount</div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div13">
                                        <asp:Label ID="lblNet" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                        Visible="false" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <%--<div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="White" Width="80%" Height="80%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <div class="divClear_10">
                        &nbsp;
                    </div>
                    <div align="center">
                        <table width="90%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvEmployeePay" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="95%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID"
                                        ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Employee">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Element">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElement" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElement" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElement" Width="130px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="90%" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox_N"
                                                        Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="90%" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="90%" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox_N"
                                                        Text='<%# Eval("pay_amount") %>' Enabled="false"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="120px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                        Visible="false" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>--%>
        <div>
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" HasDrillUpButton="False"
                AutoDataBind="True" Height="1039px" Width="901px" ToolPanelView="None" />
        </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });


        function CalculateEar() {
            var grid = document.getElementById('<%=gvEmployeePay.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount") > 1) {
                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();

        }

        function CalculateDed() {
            var grid = document.getElementById('<%=gvEmployeePay2.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount2") > 1) {

                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                            //alert(total);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotDed.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();


        }

        function calculate_time() {
            var hour_start = document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML
            // var hour_start = document.getElementById(parseInt('lblTotalEar'));                    
            var hour_end = document.getElementById('<%= lblTotDed.ClientID %>').innerHTML
            var hour_total = hour_start - hour_end;

            document.getElementById('<%=lblNet.ClientID %>').innerHTML = hour_total.toFixed(3);

        }


    </script>
</asp:Content>
