﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="~/PER/DashBoard_PER.aspx.cs" Inherits="FIN.Client.PER.DashBoard_PER" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowDeptEmp() {
            $("#divDeptEmpGrid").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="98%" border="0px">
            <tr>
                <td style="width: 40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Salary
                                    </div>
                                    <div style="width: 150px; float: right; padding-right: 10px">
                                        <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 150px; float: right; padding-right: 10px;">
                                        <asp:DropDownList ID="ddlDept" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div id="divcompEmpGrap" style="width: 98%">
                                    <asp:Chart ID="chrt_SalCount" runat="server" Width="500px" Height="230px">
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="s_Total_Emp" IsValueShownAsLabel="True"
                                                XValueMember="row_number" YValueMembers="TOTAL_EMP" Legend="Legend1" LegendText="Total Employee ( #TOTAL{N0} )">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_NEW_JOIN_EMP"
                                                XValueMember="row_number" YValueMembers="NEW_JOIN_EMP" Legend="Legend1" LegendText="New Joined  ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_CONFIRMED_EMP"
                                                XValueMember="row_number" YValueMembers="CONFIRMED_EMP" Legend="Legend1" LegendText="Conifrmed Employee  ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_TERMINATION_EMP"
                                                XValueMember="row_number" YValueMembers="TERMINATION_EMP" Legend="Legend1" LegendText="Termination Employee ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <Area3DStyle Enable3D="True" Inclination="25" Rotation="70"></Area3DStyle>
                                            </asp:ChartArea>
                                            <%-- <asp:ChartArea Name="ChartArea2" >
                                                 </asp:ChartArea>--%>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Mobile Expense
                                    </div>
                                    <div style="float: right; padding: 10px">
                                        <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowDeptEmp()" />
                                    </div>
                                    <div id="divDeptEmpGrid" style="width: 40%; background-color: ThreeDFace; position: fixed;
                                        z-index: 5000; top: 60px; left: 10px; display: none">
                                        <div style="width: 150px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddlDeptName" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlDeptName_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 150px; float: right; padding-right: 10px;">
                                            <asp:DropDownList ID="ddlEmpl" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlEmpl_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div id="div1" style="width: 98%">
                                    <asp:Chart ID="chrt_Dept" runat="server" Height="250px" Width="500px">
                                        <Series>
                                            <asp:Series Name="S_Dept_Count" ChartType="Pie" XValueMember="DEPT_NAME" YValueMembers="DEPT_COUNT"
                                                IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                                                Legend="Legend1" LegendText="#VALX ( #VAL )">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 60%" valign="top">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
