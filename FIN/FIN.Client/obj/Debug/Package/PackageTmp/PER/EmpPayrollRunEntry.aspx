﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpPayrollRunEntry.aspx.cs" Inherits="FIN.Client.PER.EmpPayrollRunEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 100px" id="lblPayrollPeriod">
                Payroll Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlPayrollPeriod" runat="server" TabIndex="1" CssClass="validate[required] EntryFont RequiredField ddlStype">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEmpName">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 350px">
                <asp:DropDownList ID="ddlEmpName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/Images/process.png" TabIndex = "3"
                    OnClick="btnSave_Click" Style="border: 0px;" />
            <%--<asp:Button ID="btnSave" runat="server" Text="Process" TabIndex = "3" OnClick="btnSave_Click" CssClass="btn" />--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divEmpSalDet" runat="server" visible="false">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div2">
                    Employee
                </div>
                <div class="divtxtBox LNOrient" style="  width: 300px">
                    <asp:Label ID="lblEmp" runat="server" Text=""></asp:Label>
                </div>
                <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div12">
                    Department
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:Label ID="lbldept" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div4">
                    Designation
                </div>
                <div class="divtxtBox LNOrient" style="  width: 300px">
                    <asp:Label ID="lblDesig" runat="server" Text=""></asp:Label>
                </div>
                <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div3">
                    Date of Join
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:Label ID="lblDoj" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <table width="100%">
                    <tr>
                        <td>
                            <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div8">
                                Earnings
                            </div>
                        </td>
                        <td>
                            <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div9">
                                Deduction
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="width: 50%">
                            <asp:GridView ID="gvEmployeePay" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                OnRowDataBound="gvEmployeePay_RowDataBound" ShowFooter="false">
                                <Columns>
                                    <asp:TemplateField Visible="false" HeaderText="Employee">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" Visible="false" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false" HeaderText="Type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Element">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount" Width="95%" MaxLength="100" onkeyup="CalculateEar();"
                                                runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount" Width="95%" onkeyup="CalculateEar();" Visible="false"
                                                MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" Width="95%" onkeyup="CalculateEar();" MaxLength="100"
                                                runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                        <td style="width: 50%">
                            <asp:GridView ID="gvEmployeePay2" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                OnRowDataBound="gvEmployeePay2_RowDataBound" Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                ShowFooter="false">
                                <Columns>
                                    <asp:TemplateField Visible="false" HeaderText="Employee">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false" HeaderText="Type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Element">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount2" Width="96%" MaxLength="100" onkeyup="CalculateDed();"
                                                runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount2" Width="96%" onkeyup="CalculateDed();" Visible="false"
                                                MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount2" Width="96%" onkeyup="CalculateDed();" MaxLength="100"
                                                runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px" id="Div16">
                                Total Earnings
                            </div>
                            <div class="lblBox LNOrient" style="float: right; width: 100px; height: 50px" id="Div17">
                                <asp:Label ID="lblTotalEar" runat="server"></asp:Label>
                            </div>
                        </td>
                        <td align="right">
                            <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px; height: 50px"
                                id="Div14">
                                Total Deduction
                            </div>
                            <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div15">
                                <asp:Label ID="lblTotDed" runat="server"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div11">
                                Net Amount</div>
                            <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div13">
                                <asp:Label ID="lblNet" runat="server" Text=""></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Update" Width="60px"
                                OnClick="btnOkay_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hf_dept_id" runat="server" Value="" />
            <asp:HiddenField ID="hf_Payroll_Id" runat="server" Value="" />
        </div>
        <div class="divRowContainer" runat="server" id="DivBank" visible="false">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblPONumber">
                    Bank Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px; height: 22px;">
                    <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="6" CssClass="validate[required]   ddlStype"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 145px" id="lblPOLineNumber">
                    Bank Branch
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:DropDownList ID="ddlBankBranch" runat="server" TabIndex="7" CssClass="validate[required]   ddlStype"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblItemDescription">
                    Account Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:DropDownList ID="ddlAccountnumber" runat="server" TabIndex="8" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlAccountnumber_SelectedIndexChanged" CssClass="validate[required]   ddlStype">
                    </asp:DropDownList>
                </div>
                <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 145px">
                    Cheque Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:DropDownList ID="ddlChequeNumber" runat="server" TabIndex="9" CssClass="validate[required]   ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divRowContainer" align="right">
            <asp:Button runat="server" CssClass="button" ID="btnPost" Text="Post" Width="60px"
                OnClick="btnPost_Click" Visible="False" />
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>