﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollTrailRunEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollTrailRunEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollPeriod">
                Payroll Period
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollPeriod" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollHoursDays">
                Payroll Hours/Days
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollHoursDays" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollStatus">
                Payroll Status
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollStatus" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollDate">
                Payroll Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtPayrollDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="1" OnTextChanged="txtPayrollDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtPayrollDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPayrollDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAllGroup">
                All Group
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkAllGroup" runat="server" Checked="True" TabIndex="5" />
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblGroup">
                Group
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlGroup" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="Div1">
            </div>
            <div class="lblBox" style="float: left; width: 300px" id="lblProcessPayrollTrailRun">
                <asp:Button ID="btnProcessPayrollTrailRun" runat="server" Text="Process Payroll Trail Run"
                    CssClass="btnProcess" OnClick="btnProcessPayrollTrailRun_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollTotal">
                Payroll Total
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblDeductionTotal">
                Deduction Total
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDeductionTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PayrollTrailRun.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
