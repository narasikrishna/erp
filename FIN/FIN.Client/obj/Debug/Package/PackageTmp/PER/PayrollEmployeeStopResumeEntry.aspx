﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollEmployeeStopResumeEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollEmployeeStopResumeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEmployeeNo">
                Employee
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlEmployeeNo" runat="server" AutoPostBack="True" CssClass="validate[Required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlEmployeeNo_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 60px" id="Div1">
                All
            </div>
            <div class="divtxtBox LNOrient" style="   width: 50px"  runat="server" 
                id="div2">
                <asp:CheckBox ID="chkall" runat="server" AutoPostBack = "true" Checked="false" TabIndex="5" Text=" " OnCheckedChanged="chkall_CheckedChanged" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblElement">
                Element
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlElement" runat="server" AutoPostBack="True" CssClass="validate[Required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlElement_SelectedIndexChanged" TabIndex="2">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 60px" id="lblAllElements" runat="server"
                visible="false">
                Stop
            </div>
            <div class="divtxtBox LNOrient" style="  width: 50px" runat="server" visible="false"
                id="divch">
                <asp:CheckBox ID="chkstopresume" runat="server" Checked="false" TabIndex="5" Text=" " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px;">
                <asp:TextBox ID="txtDescription" Enabled="false" TabIndex="3" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 90px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[Required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender13" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtFromDate">
                </cc2:CalendarExtender>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblStopReason">
                Stop Reason
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px;">
                <asp:DropDownList ID="ddlStopReason" runat="server" CssClass="validate[Required] RequiredField ddlStype"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="  width: 150px; display: none" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; display: none">
                <asp:TextBox ID="txtToDate" CssClass="validate[custom[ReqDateDDMMYYY],,dateRange[dg1]] txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender14" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtToDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px;">
                <asp:TextBox ID="txtRemarks" TabIndex="7" CssClass="validate[]  txtBox" runat="server"
                    Height="50px" TextMode="MultiLine" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div id="div_Resume" runat="server" visible="false">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblResumeDate">
                    Resume Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 100px">
                    <asp:TextBox ID="txtResumeDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[]] txtBox"
                        runat="server" TabIndex="8"></asp:TextBox>
                    <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtResumeDate">
                    </cc2:CalendarExtender>
                </div>
                <div class="lblBox LNOrient" style="  width: 150px" id="lblResume" runat="server"
                    visible="false">
                    Resume
                </div>
                <div class="divtxtBox LNOrient" style="  width: 90px" id="chkdivResume" runat="server"
                    visible="false">
                    <asp:CheckBox ID="chkResume" runat="server" TabIndex="9" Text=" " />
                </div>
                <div class="colspace  LNOrient" >
                &nbsp</div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblResumeRemarks">
                    Resume Remarks
                </div>
                <div class="divtxtBox LNOrient" style="  width: 520px;">
                    <asp:TextBox ID="txtResumeRemarks" TabIndex="10" CssClass="validate[]  txtBox" runat="server"
                        Height="50px" TextMode="MultiLine" MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
