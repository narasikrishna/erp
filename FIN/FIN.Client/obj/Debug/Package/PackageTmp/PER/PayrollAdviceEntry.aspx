﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollAdviceEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollAdviceEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 95%" id="div1">
        <div class="divRowContainer" id="divPayAdvNo" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div4">
                Pay Advice No</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:TextBox ID="txtPayAdvNo" runat="server" CssClass="txtBox RequiredField"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                Bank Name</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="1" AutoPostBack="true"
                    CssClass="validate[required] EntryFont RequiredField ddlStype" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div5">
                Payroll</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlPayroll" runat="server" TabIndex="1" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayroll_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPayrollPeriod">
                Payroll Period</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlPayrollPeriod" runat="server" TabIndex="2" CssClass=" EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayrollPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div3">
                Total Amount</div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:TextBox ID="txtTotalAmount" runat="server" CssClass=" txtBox" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none;">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPayrollDate">
                Payroll Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 100px">
                <asp:TextBox ID="txtPayrollDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtPayrollDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPayrollDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div6">
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px" align="right" >
               <%-- <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/print.png" ToolTip="Print"
                    OnClick="btnPrint_Click" Width="35px" Height="25px" Style="border: 0px" />--%>
                     <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/print.png" ToolTip="Print"
                    OnClick="btnPrint_Click" Style="border: 0px;" />
                &nbsp;&nbsp;&nbsp;
                 
                 <asp:ImageButton ID="btnProcess" runat="server" ImageUrl="~/Images/view.png"
                    OnClick="btnProcess_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnProcess" runat="server" Text="View" OnClick="btnProcess_Click" />--%>
            </div>
            <asp:HiddenField ID="hfPayAdviceNumber" runat="server" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="100%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_ID,PAY_DEPT_ID,WF,PAYROLL_PERIOD,emp_id,emp_bank_code"
                ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Employee Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEmpNo" Width="130px" MaxLength="100" runat="server" Enabled="false"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("emp_no") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtEmpNo" Width="130px" MaxLength="100" runat="server" Enabled="false"
                                Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNo"  runat="server" Text='<%# Eval("emp_no") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" CssClass=" RequiredField  txtBox" Text='<%# Eval("emp_name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDepartmentName" Style="word-wrap: break-word" runat="server"
                                Text='<%# Eval("emp_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bank">
                        <ItemTemplate>
                            <asp:Label ID="lblBankName" Width="130px" runat="server" Text='<%# Eval("bank_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account No">
                        <ItemTemplate>
                            <asp:Label ID="lblAccNo"  Style="word-wrap: break-word" runat="server"
                                Text='<%# Eval("emp_bank_acct_code") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount"  runat="server" Text='<%# Eval("pay_net_amt") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approve" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkApprove" runat="server" Checked='<%# Convert.ToBoolean(Eval("APPROVED")) %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAllApprove" runat="server" Text="Approved" AutoPostBack="true"
                                OnCheckedChanged="chkAllApprove_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });

        $("#FINContent_btnProcess").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
