﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTBankTransformation.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTBankTransformation" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblPeriodID">
                    Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlPeriod" runat="server" TabIndex="1" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
                <%--<div class="lblBox LNOrient" style="  width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="colspace" style=" ">
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace_40" style=" ">
                &nbsp</div>--%>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png" Width="35px" Height="25px"
                        OnClick="btnSave_Click" />
                </div>
            </div>
        </div></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>