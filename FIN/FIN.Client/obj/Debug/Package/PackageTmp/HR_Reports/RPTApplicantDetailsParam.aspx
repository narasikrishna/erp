﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTApplicantDetailsParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTApplicantDetailsParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblPeriodID">
                    Applicant Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlApplicantName" runat="server" TabIndex="1" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" id="div_Age" runat="server" visible="false">
                <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                    Age >
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtAgegreater" runat="server" TabIndex="2" CssClass="validate[required]  RequiredField txtBox_N"
                        MaxLength="13" Width="150px">
                    </asp:TextBox>
                </div>
                <div class="lblBox LNOrient" style="  width: 50px" id="Div3">
                    <
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtAgeLesser" runat="server" TabIndex="3" CssClass="validate[required]  RequiredField txtBox_N"
                        MaxLength="13" Width="150px">
                    </asp:TextBox>
                </div>
            </div>
        </div>
     <%--   <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer" id="div_DOB" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div4">
                DOB >
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDOBgreater" runat="server" TabIndex="4" CssClass="txtBox_N"
                    MaxLength="13" Width="150px">
                         
                </asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDOBgreater"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="  width: 50px" id="Div5">
                <
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDOBLesser" runat="server" TabIndex="5" CssClass="txtBox_N"
                    MaxLength="13" Width="150px">
                </asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDOBLesser"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div7">
                Gender
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlGender" runat="server" TabIndex="6" CssClass="ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div8">
                Nationality
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlNationality" runat="server" TabIndex="7" CssClass="ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="divFormcontainer" style="width: 500px" id="div6">
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <%-- <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                                    <img src="../Images/MainPage/settings-icon_B.png" alt="Show Report" width="15px"
                                        height="15px" title="Show Report" />--%>
                        <%-- </a>--%>
                        <%-- <asp:Button ID="btnSave1" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" Visible="false" />--%>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                            OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                    </td>
                    <%--<td>
                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                            </td>--%>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
