﻿// JScript File
var count = 0;
var hyphenCount = 0;
function ValidateNumber(fld, wdwev) {
    var i = 0;
    txtboxValue = fld.value;
    if (txtboxValue == "") {
        count = 0;
    }
    if (txtboxValue.length > 0) {
        for (i = 0; i < txtboxValue.length; i++) {
            if (txtboxValue.indexOf(".") == -1) {
                count = 0;
            }
        }
    }
    var boostatus = true;
    if (((wdwev.keyCode < 48) || (wdwev.keyCode >= 58))) {
        if (wdwev.keyCode != 46) {
            wdwev.keyCode = 0;
            boostatus = false;
            return boostatus;
        }
        else {
            count += 1;
            if (count > 1) {
                wdwev.keyCode = 0;
                boostatus = false;
                return boostatus;
            }
        }
    }
    return boostatus;
}

function fnValidDecimal(txtvalue) {
    if (txtvalue.value == ".") {
        txtvalue.value = "";
    }
}
function fnValidColon(txtvalue) {
    if (txtvalue.value == ":") {
        txtvalue.value = "";
    }
}
function ValidateTime(fld, wdwev) {
    var i = 0;
    txtboxValue = fld.value;
    if (txtboxValue == "") {
        count = 0;
    }
    if (txtboxValue.length > 0) {
        for (i = 0; i < txtboxValue.length; i++) {
            if (txtboxValue.indexOf(":") == -1) {
                count = 0;
            }
        }
    }
    var boostatus = true;
    if (((wdwev.keyCode < 48) || (wdwev.keyCode >= 58))) {
        if (wdwev.keyCode != 58) {
            wdwev.keyCode = 0;
            boostatus = false;
            return boostatus;
        }
        else {
            count += 1;
            if (count > 1) {
                wdwev.keyCode = 0;
                boostatus = false;
                return boostatus;
            }
        }
    }
    return boostatus;
}
function ValidNumber(fld, wdwev) {
    if (((wdwev.keyCode < 48) || (wdwev.keyCode >= 58))) {
        wdwev.keyCode = 0;
        return false;
    }
}
function fnValidateNumber(txtvalue) {
    ValidateNumber(txtvalue, window.event);
}
function fnValidateTime(txtvalue) {
    ValidateTime(txtvalue, window.event);
}
function fnValidNumber(txtvalue) {
    ValidNumber(txtvalue, window.event);
}

function fnValidDate(txtvalue) {
    ValidateDate(txtvalue, window.event);
}
function fnValidateNegativeNumber(txtvalue) {
    ValidateNegativeNumber(txtvalue, window.event);
}
function ValidateNegativeNumber(fld, wdwev) {
    var i = 0;

    txtboxValue = fld.value;
    if (txtboxValue == "") {
        count = 0;
        hyphenCount = 0;
    }
    if (txtboxValue.length > 0) {
        if (txtboxValue.indexOf(".") == -1) {
            count = 0;
        }
        if (txtboxValue.indexOf("-") == -1) {
            if ((txtboxValue.charAt(0)) == "-") {
                hyphenCount = 0;
            }
        }
    }
    var boostatus = true;
    if ((wdwev.keyCode < 48) || (wdwev.keyCode >= 58)) {
        if (wdwev.keyCode != 46) {
            if (wdwev.keyCode == 45) {
                //add code here	
                hyphenCount += 1;
                if (hyphenCount > 1) {
                    wdwev.keyCode = 0;
                    return false;
                }
            }
            else {
                wdwev.keyCode = 0;
                return false;
            }
        }
        else {
            count += 1;
            if (count > 1) {
                wdwev.keyCode = 0;
                return false;
            }
        }
    }
    return boostatus;
}

function ValidateDate(fld, wdwev) {
    if (((wdwev.keyCode < 48) || (wdwev.keyCode > 58))) {
        if (wdwev.keyCode != 47) {
            wdwev.keyCode = 0;
            return false;
        }
    }
    return true;
}
function fnFixedNumber(txtValue, count) {

}
function isDecimalValidation(txtValue) {
    isDecimalValidKey(txtValue, window.event);
}
//accept alpha numeric characters only
function alphaNumericOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 32))
        returnValue = true;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}

function StringOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 32))
        returnValue = true;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}

//accept alpha numeric characters only
function alphaNumericWithoutSpaceOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 32))
        returnValue = true;
    if (keyStroke == 32)
        returnValue = false;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}

//accept alpha characters only
function alphaOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    var returnValue = false;

    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || (keyStroke == 32))
        returnValue = true;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}
//accept alpha numeric and dot character only
function StringOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 46) || (keyStroke == 32)) {
        returnValue = true;
    }
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}
//accept alpha numeric and BACK SLASH character only
function alphaNumericDateOnly(eventRef) {

    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;

    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 47) || (keyStroke == 32)) {
        returnValue = true;
    }
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}
//Invalid Functions
function isDecimalValidKey(textboxIn, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 45 || charCode == 47 || charCode > 57)) {
        return false;
    }

    if (charCode == 46 && textboxIn.value.indexOf(".") != -1) {
        return false;
    }
    return true;
}

//Remove commas

function filterNum(str) {
    re = /^\$|,/g;
    // remove "$" and ","
    return str.replace(re, "");
}

//Currency Format Functon 

function monetary(amount) {
    var integer = Math.floor(amount);
    var floating = amount - integer;

    return commas(integer) + cents(floating);
}

function commas(integer) {
    integer = '' + Math.round(integer);

    if (integer.length > 3) {
        var mod = integer.length % 3;

        var output = (mod > 0 ? (integer.substring(0, mod)) : '');

        for (i = 0; i < Math.floor(integer.length / 3); i++) {
            if ((mod == 0) && (i == 0))
                output += integer.substring(mod + 3 * i, mod + 3 * i + 3);
            else
                output += ',' + integer.substring(mod + 3 * i, mod + 3 * i + 3);
        }
        return output;
    }
    return integer;
}

function cents(amount) {
    amount -= 0;

    amount = round(amount, 2);

    if (amount == 0)
        amount = '.00';
    else if (amount == Math.floor(amount))
        amount += '.00';
    else if (amount * 10 == Math.floor(amount * 10))
        amount += '0';

    return '' + amount;
}
function fncTrim(sStr) {
    if (typeof (sStr) != "string") return sStr;
    return sStr.replace(/(^\s*)|(\s*$)/g, "");
}
function CommaFormatted(amount) {
    var delimiter = ","; // replace comma if desired
    var a = amount.split('.', 2)
    var d = a[1];
    var i = parseInt(a[0]);
    if (isNaN(i)) { return ''; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while (n.length > 3) {
        var nn = n.substr(n.length - 3);
        a.unshift(nn);
        n = n.substr(0, n.length - 3);
    }
    if (n.length > 0) { a.unshift(n); }
    n = a.join(delimiter);
    if (d.length < 1) { amount = n; }
    else { amount = n + '.' + d; }
    amount = minus + amount;
    return amount;
}

function currencyFormat(fld, milSep, decSep, e, plen) {

    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;

    if (whichCode == 13) return true;  // Enter
    if (whichCode == 8) return true;  // Delete
    key = String.fromCharCode(whichCode);  // Get key value from key code
    if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
    len = fld.value.length;
    if (len < plen) {
        for (i = 0; i < len; i++)
            if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
        aux = '';
        for (; i < len; i++)
            if (strCheck.indexOf(fld.value.charAt(i)) != -1) aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        if (len == 0) fld.value = '';
        if (len == 1) fld.value = '0' + decSep + '0' + aux;
        if (len == 2) fld.value = '0' + decSep + aux;
        if (len > 2) {
            aux2 = '';
            for (j = 0, i = len - 3; i >= 0; i--) {
                if (j == 3) {
                    aux2 += milSep;
                    j = 0;
                }
                aux2 += aux.charAt(i);
                j++;
            }
            fld.value = '';
            len2 = aux2.length;
            for (i = len2 - 1; i >= 0; i--)
                fld.value += aux2.charAt(i);
            fld.value += decSep + aux.substr(len - 2, len);
            //fld.value =  fld.value.replace(/\,/g,'');

        }
    }
    return false;
}

function maskPaste(objEvent) {
    var strPasteData = window.clipboardData.getData("Text");
    var objInput = objEvent.srcElement;
    if (!isValid(strPasteData)) {
        objInput.focus();
        return false;
    }
}
function maskChange(objEvent) {
    var objInput;
    objInput = objEvent.srcElement;

    if (!isValid(objInput.value)) {
        objInput.value = objInput.validValue || "";
        objInput.focus();
        objInput.select();
    } else {
        objInput.validValue = objInput.value;
    }
}

function isValid(strValue) {
    return reValidString.test(strValue) ||
         strValue.length == 0;
}
var reValidString = /^\d*$/;


var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }
        if (i == 2) { this[i] = 29 }
    }
    return this
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        //alert("The date format should be : mm/dd/yyyy")
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert("Please enter a valid month")
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert("Please enter a valid day")
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert("Please enter a valid date")
        return false
    }
    return true
}

function ValidateForm() {
    if (isDate(fld.value) == false) {
        fld.focus()
        return false
    }
    return true
}

function isDateNew(source, arguments) {

    if (isDate(arguments.Value)) {
        arguments.IsValid = true;
    } else {
        arguments.IsValid = false;
    }
}