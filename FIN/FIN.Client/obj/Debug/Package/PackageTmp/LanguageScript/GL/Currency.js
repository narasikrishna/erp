﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/GL_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblCurrencyCode");
        x.innerHTML = result.CurrencyCode_P

        x = document.getElementById("lblCurrencyDescription");
        x.innerHTML = result.CurrencyDescription_P
        
        x = document.getElementById("lblCurrencySymbol");
        x.innerHTML = result.CurrencySymbol_P

        x = document.getElementById("lblInitialAmountSeparator");
        x.innerHTML = result.InitialAmountSeparator_P

        x = document.getElementById("lblSubsequentAmountSeparator");
        x.innerHTML = result.SubsequentAmountSeparator_P

        x = document.getElementById("lblDecimalPrecision");
        x.innerHTML = result.DecimalPrecision_P
        
        x = document.getElementById("lblDecimalSeperator");
        x.innerHTML = result.DecimalSeperator_P
        
        x = document.getElementById("lblActive");
        x.innerHTML = result.Active_P
        



    });
}
