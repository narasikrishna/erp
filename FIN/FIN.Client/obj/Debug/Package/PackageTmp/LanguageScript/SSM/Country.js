﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/SSM_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
       
        var x = document.getElementById("lblCountryCode");
        x.innerHTML = result.CountryCode_P

        x = document.getElementById("lblAlertDescription");
        x.innerHTML = result.AlertDescription_P


        x = document.getElementById("lblCountryName");
        x.innerHTML = result.CountryName_P


        x = document.getElementById("lblCountryShortName");
        x.innerHTML = result.CountryShortName_P


        x = document.getElementById("lblCurrency");
        x.innerHTML = result.Currency_p


        x = document.getElementById("lblLegacyCurrency");
        x.innerHTML = result.LanguageDescription_P


        x = document.getElementById("lblCurrencyFixRate");
        x.innerHTML = result.ScreenCode_P
        

    });
}