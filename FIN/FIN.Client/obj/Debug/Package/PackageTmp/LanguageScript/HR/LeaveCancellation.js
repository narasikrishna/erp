﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblStaffName");
        x.innerHTML = result.StaffName_P

        x = document.getElementById("lblCancelReason");
        x.innerHTML = result.CancelReason_P

        x = document.getElementById("lblLeaveApplication");
        x.innerHTML = result.LeaveApplication_P

        x = document.getElementById("lblCancelDate");
        x.innerHTML = result.CancelDate_P

        x = document.getElementById("lblLeaveFrom");
        x.innerHTML = result.LeaveFrom_P

        x = document.getElementById("lblLeaveTo");
        x.innerHTML = result.LeaveTo_P

        x = document.getElementById("lblNoOfDays");
        x.innerHTML = result.NoOfDays_P

        x = document.getElementById("lblHalfDayFrom");
        x.innerHTML = result.HalfDayFrom_P

        x = document.getElementById("lblHalfDayTo");
        x.innerHTML = result.HalfDayTo_P

    });
}