﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblAssessmentNumber");
        x.innerHTML = result.AssessmentNumber_p

        x = document.getElementById("lblAssessmentDate");
        x.innerHTML = result.AssessmentDate_p

        x = document.getElementById("lblEmployeeName");
        x.innerHTML = result.EmployeeName_p

        x = document.getElementById("lblKRANumber");
        x.innerHTML = result.KRANumber_p

        x = document.getElementById("lblAppraisalCode");
        x.innerHTML = result.AppraisalCode_p

        x = document.getElementById("lblDepartment");
        x.innerHTML = result.Department_p

        x = document.getElementById("lblJob");
        x.innerHTML = result.Job_p

        x = document.getElementById("lblReviewStatus");
        x.innerHTML = result.ReviewStatus_p

        x = document.getElementById("lblOverallAssessment");
        x.innerHTML = result.OverallAssessment_p

        x = document.getElementById("lblRecommendations");
        x.innerHTML = result.Recommendations_p

        x = document.getElementById("lblComments");
        x.innerHTML = result.Comments_p

    });
}