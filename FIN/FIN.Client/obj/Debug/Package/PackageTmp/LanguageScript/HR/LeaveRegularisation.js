﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblEmpName");
        x.innerHTML = result.EmpName_P

        x = document.getElementById("lblCategory");
        x.innerHTML = result.Category_P

        x = document.getElementById("lblType");
        x.innerHTML = result.lblType

        x = document.getElementById("lblFromDate");
        x.innerHTML = result.FromDate_P

        x = document.getElementById("lblToDate");
        x.innerHTML = result.ToDate_P

        x = document.getElementById("lblReason");
        x.innerHTML = result.Reason_P

        x = document.getElementById("lblStatus");
        x.innerHTML = result.Status_P

    });
}