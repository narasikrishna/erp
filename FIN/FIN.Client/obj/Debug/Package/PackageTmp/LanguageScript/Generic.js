﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/Generic_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.User_P);

        var x = document.getElementById("lblUserName");
        x.innerHTML = result.User_P;

        x = document.getElementById("divTime");
        x.innerHTML = result.Time_P;

        x = document.getElementById("lblOrganization");
        x.innerHTML = result.Org_P;

        var gridWF = document.getElementById("gvWorkFlow");
        if (gridWF != null) {
            gridWF.rows[0].cells[0].innerText = result.GV_WFDate_P;
            gridWF.rows[0].cells[1].innerText = result.GV_WFMsg_P;
            gridWF.rows[0].cells[2].innerText = result.GV_LevelCode_P;
            gridWF.rows[0].cells[3].innerText = result.GV_Comments_P;

            gridWF.rows[0].cells[0].textContent = result.GV_WFDate_P;
            gridWF.rows[0].cells[1].textContent = result.GV_WFMsg_P;
            gridWF.rows[0].cells[2].textContent = result.GV_LevelCode_P;
            gridWF.rows[0].cells[3].textContent = result.GV_Comments_P;
        }

        var gridAlert = document.getElementById("gvAlert");
        if (gridAlert != null) {
            gridAlert.rows[0].cells[0].innerText = result.GV_AlertDate_P;
            gridAlert.rows[0].cells[1].innerText = result.GV_AlertMsg_P;

            gridAlert.rows[0].cells[0].textContent = result.GV_AlertDate_P;
            gridAlert.rows[0].cells[1].textContent = result.GV_AlertMsg_P;
        }

    });
}