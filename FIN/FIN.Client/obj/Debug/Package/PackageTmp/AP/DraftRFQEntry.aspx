﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DraftRFQEntry.aspx.cs" Inherits="FIN.Client.AP.DraftRFQEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblNumber">
                Request for Quote Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px">
                <asp:TextBox ID="txtRFQNumber" runat="server" CssClass=" txtBox" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblDate">
                Request for Quote Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRFQDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtRFQDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierName ">
                Request for Quote Due Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRFQDueDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtRFQDueDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="width: 485px">
                <asp:TextBox ID="txtRemarks" runat="server" Width="485px" CssClass=" txtBox" Height="59px"
                    MaxLength="1000" TabIndex="4" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblPOType">
                Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px">
                <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div8">
                Status
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                <asp:DropDownList ID="ddlStatus" runat="server" Width="20px" TabIndex="6" CssClass=" validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblRequisitionNumber">
                Requisition Number</div>
            <div class="divtxtBox LNOrient" style=" overflow: auto; height: 120px; width: 520px;">
                <asp:CheckBoxList runat="server" ID="chkReqNumber" Width="500px" Height="110px" TabIndex="7"
                    BorderWidth="3">
                </asp:CheckBoxList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
               <%-- <asp:ImageButton ID="btnProcess" runat="server" ImageUrl="../Images/btnGet.png" OnClick="btnProcess_Click"
                    Style="border: 0px" />--%>
                 <asp:Button ID="btnProcess" runat="server" Width="120px" Text="Get" CssClass="btn"
                    TabIndex="8" OnClick="btnProcess_Click" />
            </div>
        </div>
        <div class="divRowContainer" align="right" style="display: none;">
            <div style="float: right;" id="Div5">
                &nbsp;
                <asp:Button ID="btnGenerate" runat="server" Text="Generate Quote" CssClass="btn"
                    Visible="false" /></div>
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="1000px"
                CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                DataKeyNames="TMP_RFQ_DTL_ID,tmp_rfq_item_id,tmp_rfq_item_uom,UOM_ID,LINE_NUM,tmp_rfq_item_cost,tmp_rfq_supplier_id,INV_ITEM_TYPE,VENDOR_ID,VENDOR_NAME,UOM_NAME">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("LINE_NUM") %>'
                                Width="20px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="20px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Name">
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemType" Enabled="false" runat="server" Text='<%# Eval("INV_ITEM_TYPE") %>'
                                Width="100px" TabIndex="14" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtItemType" Enabled="false" runat="server" Text='<%# Eval("INV_ITEM_TYPE") %>'
                                Width="100px" TabIndex="14" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit of Measurement">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="RequiredField ddlStype" Width="150px"
                                TabIndex="16">
                            </asp:DropDownList>
                            <asp:Label ID="lblUOM" Visible="false" runat="server" Text='<%# Eval("UOM_ID") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("TMP_RFQ_ITEM_QTY") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="50px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("TMP_RFQ_ITEM_QTY") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="50px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                    </asp:TemplateField>
                    <%-- <asp:TemplateField >
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" Visible="false" MaxLength="12" runat="server" Text='<%# Eval("tmp_rfq_item_cost") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtUnitPrice" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="12" Visible="false" runat="server" Text='<%# Eval("tmp_rfq_item_cost") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtUnitPrice" />
                        </ItemTemplate>
                        
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPORemarks" Text='<%# Eval("tmp_rfq_comments") %>' runat="server"
                                MaxLength="1000" CssClass="EntryFont  txtBox" Width="250px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtPORemarks" runat="server" CssClass="EntryFont  txtBox" MaxLength="1000"
                                Text='<%# Eval("tmp_rfq_comments") %>' Width="250px"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Supplier">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="RequiredField ddlStype"
                                Visible="false" Width="150px" TabIndex="16">
                            </asp:DropDownList>
                            <div class="divtxtBox" style="float: left; overflow: auto; height: 50px; width: 250px;">
                                <asp:CheckBoxList runat="server" ID="chkSupplier" Width="250px" TabIndex="16" BorderWidth="3"
                                    CssClass="RequiredField">
                                </asp:CheckBoxList>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="Div3">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
