﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SupplierBankDetails.aspx.cs" Inherits="FIN.Client.AP.SupplierBankDetails" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierNumber">
                Supplier Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlSupplierNumber" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged1"></asp:DropDownList>
                   
          
            </div>
            <div class="lblBox  LNOrient" style="width: 200px" id="lblSupplierName">
                Supplier Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtSupplierName" CssClass=" txtBox" runat="server" OnTextChanged="txtSupplierName_TextChanged"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer  LNOrient " >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PK_ID,BANK_ID,BANK_BRANCH_ID,LOOKUP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"  Width="1200px"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Bank Name" FooterStyle-Width="450px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBankname" runat="server" CssClass=" RequiredField ddlStype" Width="97%"
                            OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                            AutoPostBack="true"
                              TabIndex="3">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBankname" runat="server" CssClass=" RequiredField ddlStype" Width="97%"
                              OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                            AutoPostBack="true"
                               TabIndex="3">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBankname" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("BANK_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle Width="300px"></FooterStyle>
                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch Name" FooterStyle-Width="130px" >
                        <ItemTemplate>
                            <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("BANK_BRANCH_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBranchName" runat="server" CssClass=" RequiredField ddlStype"
                              Width="130px"  TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBranchName" runat="server" CssClass=" RequiredField ddlStype"
                             Width="130px"   TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <FooterStyle Width="250px"></FooterStyle>
                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IBAN" FooterStyle-Width="100px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtifsc" Width="130px" Enabled="false" runat="server" CssClass=" RequiredField  txtBox"
                                TabIndex="5" Text='<%# Eval("BANK_IFSC_CODE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtifsc" Width="130px" Enabled="false" runat="server" CssClass="RequiredField   txtBox"
                                TabIndex="5"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblifsc" Width="130px" runat="server" Text='<%# Eval("BANK_IFSC_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SWIFT Code" FooterStyle-Width="130px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtswift" Width="130px" runat="server" Enabled="false" CssClass=" RequiredField  txtBox"
                                TabIndex="6" Text='<%# Eval("BANK_SWIFT_CODE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtswift" Width="130px" runat="server" Enabled="false" CssClass="RequiredField   txtBox"
                                TabIndex="6"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblswift" Width="130px" runat="server" Text='<%# Eval("BANK_SWIFT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Type" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblAccountType" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountType" runat="server" CssClass=" RequiredField ddlStype"
                              Width="100%"  TabIndex="7">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountType" runat="server" CssClass="RequiredField ddlStype"
                             Width="100%"  TabIndex="7">
                            </asp:DropDownList>
                        </FooterTemplate>
                       <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Number" FooterStyle-Width="160px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAcctNo" Width="150px" runat="server" CssClass=" RequiredField  txtBox" MaxLength="20"
                                TabIndex="8" Text='<%# Eval("VENDOR_BANK_ACCOUNT_CODE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAcctNo" Width="160px" runat="server" CssClass="RequiredField   txtBox"  MaxLength="20"
                                TabIndex="8"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAcctNo" Width="160px" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;"  Text='<%# Eval("VENDOR_BANK_ACCOUNT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="9" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="9" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="10" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
