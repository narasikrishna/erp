﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TermsEntry.aspx.cs" Inherits="FIN.Client.AP.TermsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 950px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblTermName">
                Term Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 230px">
                <asp:TextBox ID="txtTermName" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="200" runat="server" OnTextChanged="txtTermName_TextChanged" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 150px" id="Div1">
                Term Name (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 230px">
                <asp:TextBox ID="txtTermNameOL" CssClass="txtBox_ol" runat="server" TabIndex="1" MaxLength="200"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblEffectiveFromDate">
                Effective From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 230px">
                <asp:TextBox ID="txtEffectiveFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" OnTextChanged="txtEffectiveFromDate_TextChanged" TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender13" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtEffectiveFromDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblEffectiveToDate">
                Effective To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 230px">
                <asp:TextBox ID="txtEffectiveToDate" CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]  txtBox" runat="server" OnTextChanged="txtEffectiveToDate_TextChanged"
                    TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender14" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtEffectiveToDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient " >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                DataKeyNames="TERM_DTL_ID,VALUE_KEY_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Term Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlTermType" Width="98%" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                TabIndex="4">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlTermType" Width="98%" TabIndex="4" runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTermType" Width="130px" runat="server"
                                Text='<%# Eval("VALUE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CR Days">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTermCRDays" Width="96%" runat="server" CssClass=" RequiredField txtBox_N"
                                TabIndex="5" MaxLength="10" Text='<%#  Eval("TERM_CR_DAYS") %>'></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers" TargetControlID="txtTermCRDays" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTermCRDays" Width="96%" runat="server" TabIndex="5" MaxLength="10"
                                CssClass="RequiredField txtBox_N"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12"
                                    runat="server" FilterType="Numbers" TargetControlID="txtTermCRDays" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTermCRDays" runat="server" Width="96%" Text='<%#  Eval("TERM_CR_DAYS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CR Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTermCRAmt" Width="97%" runat="server" CssClass="RequiredField txtBox_N"
                                MaxLength="13" TabIndex="6" Text='<%#  Eval("TERM_AMT") %>'></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtTermCRAmt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTermCRAmt" Width="97%" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="6" MaxLength="13"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtTermCRAmt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTermCRAmt" runat="server" 
                                Text='<%#  Eval("TERM_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Discount Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTermDiscType" Width="96%" runat="server" CssClass="RequiredField txtBox"
                                TabIndex="7" MaxLength="50" Text='<%#  Eval("TERM_DISCOUNT_TYPE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTermDiscType" Width="96%" runat="server" CssClass="RequiredField txtBox"
                                MaxLength="50" TabIndex="7"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtTermDiscType" runat="server"  Width="96%"
                                Text='<%#  Eval("TERM_DISCOUNT_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Days">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTermDiscDays" Width="96%" runat="server" CssClass="RequiredField txtBox_N"
                                MaxLength="10" TabIndex="8" Text='<%#  Eval("TERM_DISCOUNT_DAYS") %>'></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtenderd" runat="server" FilterType="Numbers" TargetControlID="txtTermDiscDays" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTermDiscDays" Width="96%" runat="server" CssClass="RequiredField txtBox_N"
                                MaxLength="10" TabIndex="8"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderd"
                                    runat="server" FilterType="Numbers" TargetControlID="txtTermDiscDays" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtTermDiscDays" runat="server" Width="96%"
                                Text='<%#  Eval("TERM_DISCOUNT_DAYS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTermDiscAmt" Width="96%" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="9" MaxLength="13" Text='<%#  Eval("TERM_DISCOUNT_AMT") %>'></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtendera" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtTermDiscAmt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTermDiscAmt" Width="96%" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="9" MaxLength="13"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtendera"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtTermDiscAmt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtTermDiscAmt" runat="server"  Width="96%"
                                Text='<%#  Eval("TERM_DISCOUNT_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="6" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete" class=" LNOrient">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
