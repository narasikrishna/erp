﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ItemOtherCostEntry.aspx.cs" Inherits="FIN.Client.AP.ItemOtherCostEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblGRNNumber">
                Goods Received Note Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 550px">
                <asp:DropDownList ID="ddlGRNNumber" runat="server" CssClass="validate[required] RequiredField ddlStype" Width="535px"
                    AutoPostBack="true" 
                    OnSelectedIndexChanged="ddlGRNNumber_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
           
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblLineNumber">
                Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 550px">
                <asp:DropDownList ID="ddlLineNumber" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype" Width="535px"
                    AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlLineNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div1">
                Goods Received Note Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                 <asp:TextBox ID="txtGRNDate" runat="server"  Enabled="false" TabIndex="3" 
                    CssClass=" RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtGRNDate" OnClientDateSelectionChanged="checkDate"/>
            </div>
            <div class="colspace LNOrient" >&nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Purchase Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 180px">
                <asp:TextBox ID="txtPONumber" Enabled="false" runat="server" TabIndex="4"  
                    CssClass="RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPOLineNumber">
                Purchase Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:TextBox ID="txtPOLineNumber" Enabled="false" runat="server" TabIndex="5" 
                    CssClass=" RequiredField txtBox_N"></asp:TextBox>
            </div>
            <div class="colspace LNOrient"  >&nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblItemDescription">
                Item Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 180px">
                <asp:TextBox ID="txtItemDescription" Enabled="false" runat="server" TabIndex="6"
                    CssClass=" RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer  LNOrient" >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="500px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="INV_ITEM_COST_ID,INV_RECEIPT_HDR_ID,INV_RECEIPT_LINE_ID,LOOKUP_ID,DELETED">
                <Columns>
                    <asp:TemplateField HeaderText="Cost Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCostType" TabIndex="7" runat="server" CssClass="RequiredField ddlStype" Width="350px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCostType" TabIndex="7" runat="server" CssClass="RequiredField ddlStype"  Width="350px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCostType" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'
                                 Width="350px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cost">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCost" TabIndex="8" MaxLength="13" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                runat="server" Text='<%# Eval("INV_CHARGE_AMT") %>' CssClass="EntryFont RequiredField txtBox_N"
                                Width="99%"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                    runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtCost" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCost" TabIndex="8" MaxLength="13" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                runat="server" CssClass="EntryFont RequiredField txtBox_N"  Width="99%"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                    TargetControlID="txtCost" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCost" runat="server" Text='<%# Eval("INV_CHARGE_AMT") %>'  Width="99%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" ToolTip="Edit"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" ToolTip="Delete"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" ToolTip="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" ToolTip="Cancel"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" ToolTip="Add"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" 
                            CssClass="btn"/>
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            onclick="btnDelete_Click"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                             />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                             />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
       

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
