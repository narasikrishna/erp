﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DashBoard_AP.aspx.cs" Inherits="FIN.Client.AP.DashBoard_AP" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowInvoicePaid() {
            $("#divInvoicePaid").fadeToggle(2000);
        }
        function fn_ShowGRN() {
            $("#divGRN").fadeToggle(2000);
        }
    </script>
    <style type="text/css">
        .zoom_img img
        {
            height: 80px;
            width: 100px;
            -moz-transition: -moz-transform 0.5s ease-in;
            -webkit-transition: -webkit-transform 0.5s ease-in;
            -o-transition: -o-transform 0.5s ease-in;
        }
        .zoom_img img:hover
        {
            margin: 15%;
            -moz-transform: scale(2);
            -webkit-transform: scale(2);
            -o-transform: scale(2);
        }
    </style>
    <script type="text/javascript">
        var Width, Height, OrgH, OrgW;
        function zoomToggle(imgWidth, imgHeight, ZoomWidth, ZoomHeight, Image) {
            OrgW = Image.style.width;
            OrgH = Image.style.height;
            if ((OrgW == ZoomWidth) || (OrgH == ZoomHeight)) {

                Width = imgWidth;
                Height = imgHeight;
            }
            else {

                Width = ZoomWidth;
                Height = ZoomHeight;
            }

            Image.style.width = Width;
            Image.style.height = Height;




        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="98%" border="0px">
            <tr>
                <td style="width: 40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divempGraph" style="float: left; width: 100%;">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left; padding-top: 5px">
                                            Top 5 Supplier
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div style="float: left; width: 100%; overflow: auto">
                                        <asp:Chart ID="chrtTop5Supply" runat="server" Height="200px" Width="500px">
                                            <Series>
                                                <asp:Series Name="S_supply_Count" XValueMember="vendor_name" YValueMembers="payment_amt"
                                                    IsValueShownAsLabel="true" LabelAngle="60" Legend="Legend1" LegendText="Total Amount ( #TOTAL{N0} )">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Invoiced/Paid
                                    </div>
                                    <div style="float: right; padding: 10px">
                                        <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowInvoicePaid()" />
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divcompEmpGrap" style="width: 98%">
                                    <asp:Chart ID="chrtInvoicedPaid" runat="server" Width="500px" Height="230px">
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="invoice_amt" IsValueShownAsLabel="True"
                                                XValueMember="vendor_name" YValueMembers="invoice_amt" Legend="Legend1" LegendText="Invoiced Amount( #TOTAL{N0} )">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="payment_amt"
                                                XValueMember="vendor_name" YValueMembers="payment_amt" Legend="Legend1" LegendText="Paid Amount( #TOTAL{N0} )\n">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divInvoicePaid" style="width: 40%; background-color: ThreeDFace; position: absolute;
                                    z-index: 5000; top: 400px; left: 10px; display: none">
                                    <div id="divEmpDept" runat="server" visible="true">
                                        <asp:GridView ID="gvInvoicePaid" runat="server" CssClass="Grid" Width="100%" DataKeyNames="vendor_id"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnk_Dept_Name" runat="server" Text='<%# Eval("vendor_name") %>'
                                                            OnClick="lnk_InvoicePaid_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="invoice_amt" HeaderText="Invoiced Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="payment_amt" HeaderText="Paid Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divPermintDet" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Goods Receipt Note
                                        </div>
                                        <div style="float: right; padding: 10px">
                                            <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowGRN()" />
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div id="div1" style="width: 98%" class="zoom_img">
                                        <asp:Chart ID="chartGRN" runat="server" Width="400px" Height="220px">
                                            <Series>
                                                <asp:Series ChartArea="ChartArea1" Name="qty_received" IsValueShownAsLabel="True"
                                                    XValueMember="item_name" YValueMembers="qty_received" Legend="Legend1" LegendText="Received Quantity ( #TOTAL{N0} )">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="qty_approved"
                                                    XValueMember="item_name" YValueMembers="qty_approved" Legend="Legend1" LegendText="Approved Quantity ( #TOTAL{N0} )\n">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="qty_rejected"
                                                    XValueMember="item_name" YValueMembers="qty_rejected" Legend="Legend1" LegendText="Rejected Quantity ( #TOTAL{N0} )\n">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1">
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                    <div id="divGRN" style="width: 40%; height: 100px; background-color: ThreeDFace;
                                        position: absolute; z-index: 5000; top: 45px; left: 530px; display: none">
                                        <div class="divRowContainer">
                                            <div class="lblBox" style="float: left; width: 80px" id="lblInvoiceCurrency">
                                                Supplier
                                            </div>
                                            <div style="width: 420px; float: right; padding-right: 10px">
                                                <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="divClear_10">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox" style="float: left; width: 80px" id="Div2">
                                                Item
                                            </div>
                                            <div style="width: 420px; float: right; padding-right: 10px">
                                                <asp:DropDownList ID="ddlItem" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlItem_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="divEmpAge" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Aging Analysis
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div id="divempAgeGraph">
                                        <asp:Chart ID="chrtEmpAge" runat="server" Height="200px" Width="370px">
                                            <Series>
                                                <asp:Series Name="Outstanding Amount" ChartType="Column" XValueMember="AgeDiff" YValueMembers="AMOUNT_PAID"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top" align="left">
                                <div id="divCompanyHoliday" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Item Utlization
                                        </div>
                                    </div>
                                    <div class="divClear_10">
                                    </div>
                                    <div id="div3">
                                        <asp:Chart ID="chartIssuedQuantity" runat="server" Height="240px" Width="350px">
                                            <Series>
                                                <asp:Series Name="IssuedQuantity" ChartType="Pie" XValueMember="item_name" YValueMembers="issued_quantity"
                                                    IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                                                    Legend="Legend1" LegendText="#VALX ( #VAL )">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <div id="divEmpExpir" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        Supplierwise PO Awaited
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divEmpExpirGraph">
                                        <asp:Chart ID="chrtPOAwait" runat="server" Width="300px" Height="200px">
                                            <Series>
                                                <asp:Series Name="s_POAwaitr" ChartType="Funnel" XValueMember="vendor_name" YValueMembers="counts"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1" Docking="Bottom">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
