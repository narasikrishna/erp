﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MaterialIssueEntry.aspx.cs" Inherits="FIN.Client.AP.MaterialIssueEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDept">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px">
                <asp:DropDownList ID="ddlDept" runat="server" TabIndex="1" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 80px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox LNOrient" style=" width: 115px">
                <asp:TextBox ID="txtDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployeeName ">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 395px">
                <asp:DropDownList ID="ddlEmployeeName" runat="server" TabIndex="3" CssClass="ddlStype RequiredField validate[required]">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblReceivedBy">
                Received By
            </div>
            <div class="divtxtBox LNOrient" style="width: 400px">
                <asp:TextBox ID="txtReceivedBy" runat="server" TabIndex="4" MaxLength="50" CssClass="validate[required] RequiredField txtBox"> </asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Account Code
            </div>
            <div class="divtxtBox LNOrient" style=" width: 395px">
                <asp:DropDownList ID="ddlAccCode" runat="server" TabIndex="5" CssClass="ddlStype RequiredField validate[required]">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblMIRemarks">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style=" width: 400px">
                <asp:TextBox ID="txtMIRemarks" runat="server" TextMode="MultiLine" TabIndex="6" MaxLength="1000"
                    Height="59px" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField runat="server" ID="hf_UnitPrice" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="800px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="INDENT_ID,INDENT_DTL_ID,ITEM_ID,indent_dept_id,issuing_emp_id">
                <Columns>
                    <asp:TemplateField HeaderText="Item Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Width="150px" TabIndex="7" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Width="150px" TabIndex="7" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="250px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UOM">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUOM" MaxLength="50" Enabled="false" runat="server" Text='<%# Eval("ITEM_UOM") %>'
                                TabIndex="8" CssClass="EntryFont RequiredField txtBox" Width="80px"></asp:TextBox>
                            <%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtUOM"/>--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUOM" MaxLength="50" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="8" Width="80px"></asp:TextBox><%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender66"
                                    runat="server" TargetControlID="txtUOM" />--%>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUOM" Style="word-wrap: break-word" runat="server" Text='<%# Eval("ITEM_UOM") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRequestedQuantity" MaxLength="10" runat="server" Text='<%# Eval("item_req_qty") %>'
                                TabIndex="9" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoexExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtRequestedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRequestedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="9" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxEextender23"
                                    FilterType="Numbers" runat="server" TargetControlID="txtRequestedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRequestedQuantity" runat="server" Text='<%# Eval("item_req_qty") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issued Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIssuedQuantity" MaxLength="10" runat="server" Text='<%# Eval("item_issue_qty") %>'
                                TabIndex="10" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xgtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtIssuedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIssuedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="10" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredText3BoxEfxtender23"
                                    FilterType="Numbers" runat="server" TargetControlID="txtIssuedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblIssuedQuantity" runat="server" Text='<%# Eval("item_issue_qty") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" Text='<%# Eval("REMARKS") %>'
                                TabIndex="11" CssClass="EntryFont txtBox" Width="200px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                TabIndex="11" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblREMARKS" runat="server" Text='<%# Eval("REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Lot Details" CommandName="btnPop"
                                TabIndex="12" Enabled="false" OnClick="btnLot_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Lot Details" CommandName="btnPop"
                                TabIndex="12" OnClick="btnLot_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Lot Details" CommandName="btnPop"
                                TabIndex="12" OnClick="btnLot_Click" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="13" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="14" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="15" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="16" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="17" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        <%--OnClick="btnDelete_Click" />--%>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div1" class=" LNOrient">
            <asp:HiddenField runat="server" ID="HFQtyfromMI" />
            <asp:HiddenField runat="server" ID="HFlotqty" />
            <asp:HiddenField runat="server" ID="hf_qty" />
            <asp:HiddenField runat="server" ID="hdLot" />
            <asp:HiddenField runat="server" ID="hdItemIndex" />
            <cc2:ModalPopupExtender ID="mpeReceipt" runat="server" TargetControlID="hdLot" PopupControlID="pnlpopup"
                BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlpopup" runat="server" Width="750px" Height="400px">
                <div class="ConfirmForm LNOrient" style="overflow: auto;">
                    <table>
                        <tr class="ConfirmLotHeading">
                            <td style="height: 300px;" valign="top">
                                <div class="divRowContainer" align="left">
                                    <asp:GridView ID="gvLot" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" DataKeyNames="LOT_ID,INV_WH_ID,ITEM_ID,DELETED" OnRowCancelingEdit="gvLot_RowCancelingEdit"
                                        OnRowCommand="gvLot_RowCommand" OnRowCreated="gvLot_RowCreated" OnRowDataBound="gvLot_RowDataBound"
                                        OnRowDeleting="gvLot_RowDeleting" OnRowEditing="gvLot_RowEditing" OnRowUpdating="gvLot_RowUpdating"
                                        ShowFooter="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Warehouse">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlWH" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                                        Width="150px" TabIndex="1" OnSelectedIndexChanged="ddlWH_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlWH" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                                        Width="150px" TabIndex="1" OnSelectedIndexChanged="ddlWH_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWH" Width="170px" runat="server" Text='<%# Eval("INV_WH_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lot">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlLot" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                                        Width="150px" TabIndex="2" OnSelectedIndexChanged="ddlLot_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlLot" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                                        Width="150px" TabIndex="2" OnSelectedIndexChanged="ddlLot_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLotID" Width="170px" runat="server" Text='<%# Eval("LOT_NUMBER") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Quantity">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtQty" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                                                        runat="server" TabIndex="3" Text='<%# Eval("LOT_QTY") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilterwqeddTextBoxExtender21" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtQty" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtQty" MaxLength="13" CssClass=" RequiredField txtBox_N"
                                                        runat="server" TabIndex="3"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FielteryedTefxtBoxExtender21" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtQty" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQty" Width="170px" runat="server" Text='<%# Eval("LOT_QTY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                        TabIndex="6" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                        TabIndex="7" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                        TabIndex="6" Height="20px" ImageUrl="~/Images/Update.png" />
                                                    <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                        TabIndex="7" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                        TabIndex="8" ImageUrl="~/Images/Add.jpg" />
                                                </FooterTemplate>
                                                <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLotError" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnLotNo" OnClick="btnLotNo_Click"
                                    Text="Close" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
