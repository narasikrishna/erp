﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PropertysaleEntry.aspx.cs" Inherits="FIN.Client.PMS.PropertysaleEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
           Sale Number
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
            <asp:TextBox ID="txtSaleNumber" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                MaxLength="10" runat="server"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
            Date
        </div>
        
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="txtDate" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                MaxLength="10" runat="server" TabIndex="1"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
         <div class="lblBox" style="float: left; width: 200px" id="Div5">
            Buyer Number
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
             <asp:DropDownList ID="ddlBuyerNumber" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
        </div>
        
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
           First Name
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
           <asp:TextBox ID="txtFirstName" CssClass="RequiredField txtBox" Enabled="false" MaxLength="10"
                runat="server" TabIndex="3"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
           Middle Name
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="txtPropertyName" CssClass="txtBox" Enabled="false" MaxLength="10"
                runat="server" TabIndex="3"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 200px" id="Div1">
           Last Name
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="txtLastName" CssClass="txtBox" Enabled="false" MaxLength="10"
                runat="server" TabIndex="3"></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
            Property Number
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
             <asp:DropDownList ID="ddlPropertyNumber" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
            Unit Number
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="txtNoOfInstallment" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                runat="server"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 200px" id="Div2">
            Proposed Price
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
           
            <asp:TextBox ID="txtProposedPrice" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
  
     <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 200px" id="Div3">
           Brokerage Percentage
        </div>
        <div class="divtxtBox" style="float: left; width: 155px">
             <asp:DropDownList ID="DropDownList1" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox" style="float: left; width: 195px" id="Div4">
            Brokerage Amount
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="TextBox1" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                runat="server"></asp:TextBox>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="divtxtBox" style="float: left; width: 300px" align="center">
            <asp:Button ID="btnInvoice" runat="server" Text="Generate Invoice" CssClass="btn" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
