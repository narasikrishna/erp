﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PropertyPaymentEntry.aspx.cs" Inherits="FIN.Client.PMS.PropertyPaymentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
      
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
                Payment Number
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtpaymentNumber" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
                Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDate" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
                Property Number
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlPropertyNumber" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
                Property Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPropertyName" CssClass="txtBox" Enabled="false" MaxLength="10"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
                Payment Amount
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtPayAmount" TabIndex="4" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
                Number Of Installaments
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtNoOfInstallment" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="RECEIPT_ID" Width="800px" ShowFooter="True">
                <Columns>
                 <asp:TemplateField HeaderText="Installment Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInstNumber" TabIndex="16" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInstNumber" TabIndex="6" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInstNumber" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bank Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" TabIndex="17" Width="150px"
                                AutoPostBack="True">
                                <asp:ListItem Value="----Select----"></asp:ListItem>
                                <asp:ListItem Value="01">TEN0001</asp:ListItem>
                                <asp:ListItem Value="02">TEN0002</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" CssClass="ddlStype"
                                Width="150px" TabIndex="7">
                                <asp:ListItem Value="----Select----"></asp:ListItem>
                                <asp:ListItem Value="01">TEN0001</asp:ListItem>
                                <asp:ListItem Value="02">TEN0002</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBankName" runat="server" Text='<%# Eval("GRN_NUM") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="ddlBranchName" TabIndex="6" Width="80px" MaxLength="18" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ddlBranchName" TabIndex="8" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBranchName" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="6" Width="80px" MaxLength="19" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="9" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Cheque Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtChequNumber" TabIndex="20" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtChequNumber" TabIndex="10" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblChequNumber" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Cheque Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpChequeDate" TabIndex="21" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="80px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpChequeDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpChequeDate" TabIndex="11" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="80px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpChequeDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblchequeDate" Width="80px" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="22" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="12" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="14" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="15" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" TabIndex="23" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="24" runat="server" AlternateText="Cancel" 
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="13" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
