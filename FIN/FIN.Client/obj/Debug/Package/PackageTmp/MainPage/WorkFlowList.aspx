﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WorkFlowList.aspx.cs" Inherits="FIN.Client.MainPage.WorkFlowList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_CallParentScript(str_FormName) {
            window.top.fn_ResizeEntryScreen(str_FormName, "inline", "inline");
            window.document.forms[0].target = 'centerfrm';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
        Style="width: 95%; height: 80%;" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
        CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
        OnRowDataBound="gvWorkFlow_RowDataBound">
        <Columns>
            <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
            <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkFormTarget" Style="text-decoration: none;" OnClientClick='<%# "fn_CallParentScript(\"" + Eval("module_description")+ " -- > " + Eval("SCREEN_NAME") +"\")" %>'
                        Text='<%#Eval("workflow_message") %>' runat="server" Width="190px"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="White">
                <ItemTemplate>
                    <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Target="centerfrm"
                        Text='<%#Eval("Level_Code") %>' Width="40px" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="White">
                <ItemTemplate>
                    <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Target="centerfrm"
                        Text='<%#Eval("remarks") %>' Width="100px" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
        <HeaderStyle CssClass="GridHeader" />
        <PagerStyle CssClass="pgr"></PagerStyle>
        <AlternatingRowStyle CssClass="GrdAltRow" />
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
