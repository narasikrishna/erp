﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeftFrame.aspx.cs" Inherits="FIN.Client.MainPage.LeftFrame" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/MenuStyle.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#firstpane p.menu_head").click(function () {
                $(this).css({ backgroundImage: "url(down.png)" }).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
                $(this).siblings().css({ backgroundImage: "url(left.png)" });
            });
            $("#secondpane p.menu_head").mouseover(function () {
                $(this).css({ backgroundImage: "url(down.png)" }).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
                $(this).siblings().css({ backgroundImage: "url(left.png)" });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <table width="100%">
        <tr>
            <td>
                <div id="div_Menu" runat="server" style=">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
