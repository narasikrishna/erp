﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModuleMasterFixed.aspx.cs"
    Inherits="FIN.Client.MainPage.ModuleMasterFixed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <link href="../Scripts/JQueryTab/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQueryTab/jquery-ui.js" type="text/javascript"></script>
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <%--<script>
        $(function () {
            $("#tabs").tabs();
        });
    </script>--%>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .myheader1
        {
            background-image: url("../Images/workflowhead.png");
            background-repeat: no-repeat;
            height: 23px;
            overflow: visible;
        }
        .myheader
        {
            height: 420px;
            overflow: auto;
        }
        .GridModule
        {
            color: #333;
            border-style: solid;
            border-width: 1px;
            border-color: Black;
        }
        .GridEmptyRowStyle
        {
            text-align: center;
            background-color: #1D4E76;
            color: #fff;
            font-weight: bold;
            height: 400px;
        }
        .GridMsg
        {
            color: #333;
            border-style: solid;
            border-width: 1px;
            border-color: Black;
        }
        .GridGrdAltRow
        {
            background-color: #DFDFDF;
            color: #333;
        }
        .slider1Cont
        {
            background-image: url("../Images/slideBG.jpg");
            background-repeat: no-repeat;
        }
    </style>
    <!-- it works the same with all jquery version from 1.x to 2.x -->
    <script type="text/javascript" src="../Scripts/Slide/jquery-1.9.1.min.js"></script>
    <!-- use jssor.slider.mini.js (39KB) or jssor.sliderc.mini.js (31KB, with caption, no slideshow) or jssor.sliders.mini.js (26KB, no caption, no slideshow) instead for release -->
    <!-- jssor.slider.mini.js = jssor.sliderc.mini.js = jssor.sliders.mini.js = (jssor.core.js + jssor.utils.js + jssor.slider.js) -->
    <script type="text/javascript" src="../Scripts/Slide/jssor.core.js"></script>
    <script type="text/javascript" src="../Scripts/Slide/jssor.utils.js"></script>
    <script type="text/javascript" src="../Scripts/Slide/jssor.slider.js"></script>
    <script>

        jQuery(document).ready(function ($) {
            //Reference http://www.jssor.com/development/slider-with-caption.html
            //Reference http://www.jssor.com/development/reference-ui-definition.html#captiondefinition
            //Reference http://www.jssor.com/development/tool-caption-transition-viewer.html

            var _SlideshowTransitions = [
            //Swing Outside in Stairs
            {$Duration: 1200, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $FlyDirection: 9, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: { $Left: $JssorEasing$.$EaseInWave, $Top: $JssorEasing$.$EaseInWave, $Clip: $JssorEasing$.$EaseOutQuad }, $ScaleHorizontal: 0.2, $ScaleVertical: 0.1, $Outside: true, $Round: { $Left: 1.3, $Top: 2.5} }

            //Dodge Dance Outside out Stairs
            , { $Duration: 1500, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: { $Left: [0.1, 0.9], $Top: [0.1, 0.9] }, $SlideOut: true, $FlyDirection: 9, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: { $Left: $JssorEasing$.$EaseInJump, $Top: $JssorEasing$.$EaseInJump, $Clip: $JssorEasing$.$EaseOutQuad }, $ScaleHorizontal: 0.3, $ScaleVertical: 0.3, $Outside: true, $Round: { $Left: 0.8, $Top: 2.5} }

            //Dodge Pet Outside in Stairs
            , { $Duration: 1500, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $FlyDirection: 9, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 260, $Easing: { $Left: $JssorEasing$.$EaseInWave, $Top: $JssorEasing$.$EaseInWave, $Clip: $JssorEasing$.$EaseOutQuad }, $ScaleHorizontal: 0.2, $ScaleVertical: 0.1, $Outside: true, $Round: { $Left: 0.8, $Top: 2.5} }

            //Dodge Dance Outside in Random
            , { $Duration: 1500, $Delay: 20, $Cols: 8, $Rows: 4, $Clip: 15, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $FlyDirection: 9, $Easing: { $Left: $JssorEasing$.$EaseInJump, $Top: $JssorEasing$.$EaseInJump, $Clip: $JssorEasing$.$EaseOutQuad }, $ScaleHorizontal: 0.3, $ScaleVertical: 0.3, $Outside: true, $Round: { $Left: 0.8, $Top: 2.5} }

            //Flutter out Wind
            , { $Duration: 1800, $Delay: 30, $Cols: 10, $Rows: 5, $Clip: 15, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $SlideOut: true, $FlyDirection: 5, $Reverse: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 2050, $Easing: { $Left: $JssorEasing$.$EaseInOutSine, $Top: $JssorEasing$.$EaseOutWave, $Clip: $JssorEasing$.$EaseInOutQuad }, $ScaleHorizontal: 1, $ScaleVertical: 0.2, $Outside: true, $Round: { $Top: 1.3} }

            //Collapse Stairs
            , { $Duration: 1200, $Delay: 30, $Cols: 8, $Rows: 4, $Clip: 15, $SlideOut: true, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Assembly: 2049, $Easing: $JssorEasing$.$EaseOutQuad }

            //Collapse Random
            , { $Duration: 1000, $Delay: 80, $Cols: 8, $Rows: 4, $Clip: 15, $SlideOut: true, $Easing: $JssorEasing$.$EaseOutQuad }

            //Vertical Chess Stripe
            , { $Duration: 1000, $Cols: 12, $FlyDirection: 8, $Formation: $JssorSlideshowFormations$.$FormationStraight, $ChessMode: { $Column: 12} }

            //Extrude out Stripe
            , { $Duration: 1000, $Delay: 40, $Cols: 12, $SlideOut: true, $FlyDirection: 2, $Formation: $JssorSlideshowFormations$.$FormationStraight, $Assembly: 260, $Easing: { $Left: $JssorEasing$.$EaseInOutExpo, $Opacity: $JssorEasing$.$EaseInOutQuad }, $ScaleHorizontal: 0.2, $Opacity: 2, $Outside: true, $Round: { $Top: 0.5} }

            //Dominoes Stripe
            , { $Duration: 2000, $Delay: 60, $Cols: 15, $SlideOut: true, $FlyDirection: 8, $Formation: $JssorSlideshowFormations$.$FormationStraight, $Easing: $JssorEasing$.$EaseOutJump, $Round: { $Top: 1.5} }
            ];

            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1

                },

                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(parentWidth, 1400));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }


            //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            //    $(window).bind("orientationchange", ScaleSlider);
            //}
            //responsive code end
        });
    </script>
</head>
<body style="margin: 0px 0px 0px 0px">
    <form id="form1" runat="server">
    <div style="width: 100%">
        <div style="height: 150px; width: 100%; background: url('/images/MHeaderRight.jpg')">
            <div style="background: url('/images/MHeader.jpg') no-repeat left; height: 150px">
                <table style="height: 150px">
                    <tr valign="middle" style="height: 150px">
                        <td style="padding-left: 10px">
                            <img src="../Images/Logo.png" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="height: 10px; width: 100%; background-color: rgb(243,10,10);" id="divMenuLink">
        </div>
        <div style="clear: both; height: 10px;">
        </div>
        <div align="center" style="width: 100%">
            <br />
            <div style="width: 65%; float: left;">
                <div style="height: 350px;">
                    <div style="width: 420px; height: 100%; float: left;">
                        <div class="myheadera" align="center">
                            <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,module_code"
                                Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
                                CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                                OnRowDataBound="gvWorkFlow_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                        ItemStyle-HorizontalAlign="Left" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Workflow Date" />
                                    <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperMsgID" Text='<%#Eval("workflow_message") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="GridEmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridGrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="width: 420px; height: 100%; float: left;">
                        <div class="myheadera" align="center">
                            <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,transaction_code"
                                Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
                                EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                        DataField="created_date" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-ForeColor="White"
                                        HeaderText="Alert Date" />
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                        ItemStyle-HorizontalAlign="Left" DataField="alert_message" HeaderText="Alert Message" />
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <%--  <div align="left" style="height: 60px;">
                    <div class="GridHeader" style="width: 395px; margin-left: 13px;">
                        Messaging
                    </div>
                    <div style="border: thin solid #1D4E76; height: 60px; width: 393px; margin-left: 13px;
                        float: left;">
                    </div>
                </div>  <br />  <br />--%>
            </div>
            <div style="width: 35%; height: 350px; float: left;">
                Dashboard
            </div>
        </div>
        <div style="clear: both; height: 10px;">
        </div>
        <div align="center" style="width: 100%">
            <div align="center">
                <table>
                    <tr>
                        <td>
                            <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                Organization
                            </div>
                        </td>
                        <td>
                            <div class="divtxtBox" style="float: left; width: 250px">
                                <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width: 100%; height: 100px;"  runat="server" id="divModuleSel">
            <br />
            <div style="width: 100%; height: 100%; float: left;">
                <!-- Jssor Slider Begin -->
                <!-- You can move inline styles (except 'top', 'left', 'width' and 'height') to css file or css block. -->
                <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 1355px;
                    height: 100px; overflow: hidden;" class="slider1Cont">
                    <!-- Loading Screen -->
                    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                            background-color: #000000; top: 0px; left: 0px; width: 100%; height: 100%;">
                        </div>
                        <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                            top: 0px; left: 0px; width: 100%; height: 100%;">
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div align="center">
                    </div>
                    <div class="divClear_10">
                    </div>
                    <!-- Slides Container -->
                    <div align="center" u="slides" style="cursor:pointer; position: absolute; left: 0px;
                        top: 0px; width: 1355px; height: 100%; overflow: hidden;">
                        <div align="center">
                            <asp:ImageButton ID="imgbtnGL" runat="server" ImageUrl="~/Images/gl.png" OnClick="imgbtnGL_Click"
                                Width="1355px" />
                        </div>
                        <div>
                            <asp:ImageButton ID="imgbtnAP" runat="server" ImageUrl="~/Images/ap.png" OnClick="imgbtnAP_Click"
                                Width="1355px" />
                        </div>
                        <div>
                            <asp:ImageButton ID="imgbtnHR" runat="server" ImageUrl="~/Images/hr.png" OnClick="imgbtnHR_Click"
                                Width="1355px" />
                        </div>
                        <div>
                            <asp:ImageButton ID="imgbtnCA" runat="server" ImageUrl="~/Images/ca.png" OnClick="imgbtnCA_Click"
                                Width="1355px" />
                        </div>
                        <div>
                            <asp:ImageButton ID="imgbtnPA" runat="server" ImageUrl="~/Images/pa.png" OnClick="imgbtnPA_Click"
                                Width="1355px" />
                        </div>
                        <div>
                            <asp:ImageButton ID="imgbtnFA" runat="server" ImageUrl="~/Images/fa.png" OnClick="imgbtnFA_Click"
                                Width="1355px" />
                        </div>
                    </div>
                    <!-- Bullet Navigator Skin Begin -->
                    <!-- jssor slider bullet navigator skin 01 -->
                    <style>
                        /*
            .jssorb01 div           (normal)
            .jssorb01 div:hover     (normal mouseover)
            .jssorb01 .av           (active)
            .jssorb01 .av:hover     (active mouseover)
            .jssorb01 .dn           (mousedown)
            */
                        .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av
                        {
                            background: url(../Images/slideBullet.png) no-repeat;
                            overflow: hidden;
                            cursor: pointer;
                            cursor: hand;
                        }
                        
                        .jssorb21 div
                        {
                            background-position: -5px -5px;
                        }
                        .jssorb21 div:hover, .jssorb21 .av:hover
                        {
                            background-position: -35px -5px;
                        }
                        .jssorb21 .av
                        {
                            background-position: -65px -5px;
                        }
                        .jssorb21 .dn, .jssorb21 .dn:hover
                        {
                            background-position: -95px -5px;
                        }
                    </style>
                    <!-- bullet navigator container -->
                    <div u="navigator" class="jssorb21" style="position: absolute; bottom: 7px; right: 30px;">
                        <!-- bullet navigator item prototype -->
                        <%-- <div u="prototype" style="position: absolute; width: 12px; height: 12px;">
                                </div>--%>
                        <div u="prototype" style="position: absolute; width: 19px; height: 19px; text-align: center;
                            line-height: 19px; color: White; font-size: 12px;">
                        </div>
                    </div>
                    <!-- Bullet Navigator Skin End -->
                    <!-- Arrow Navigator Skin Begin -->
                    <style>
                        /* jssor slider arrow navigator skin 02 css */
                        /*
            .jssora02l              (normal)
            .jssora02r              (normal)
            .jssora02l:hover        (normal mouseover)
            .jssora02r:hover        (normal mouseover)
            .jssora02ldn            (mousedown)
            .jssora02rdn            (mousedown)
            */
                        .jssora21l, .jssora21r, .jssora21ldn, .jssora21rdn
                        {
                            position: absolute;
                            cursor: pointer;
                            cursor: hand;
                            display: block;
                            background: url(~/Images/slideArrrow.png) center center no-repeat;
                            overflow: hidden;
                        }
                        .jssora21l
                        {
                            background-position: -3px -33px;
                        }
                        .jssora21r
                        {
                            background-position: -63px -33px;
                        }
                        .jssora21l:hover
                        {
                            background-position: -123px -33px;
                        }
                        .jssora21r:hover
                        {
                            background-position: -183px -33px;
                        }
                        .jssora21ldn
                        {
                            background-position: -243px -33px;
                        }
                        .jssora21rdn
                        {
                            background-position: -303px -33px;
                        }
                    </style>
                    <!-- Arrow Left -->
                    <span u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 123px;
                        left: 8px;"></span>
                    <!-- Arrow Right -->
                    <span u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 123px;
                        right: 8px"></span>
                    <!-- Arrow Navigator Skin End -->
                    <a style="display: none" href="http://www.jssor.com">Slider</a>
                </div>
                <!-- Jssor Slider End -->
            </div>
        </div>
    </div>
    </form>
</body>
</html>
