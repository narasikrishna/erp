﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="FIN.Client.MainPage.MainPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />    
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function setPageHeight() {
            var y = screen.availHeight;
            var x = y - 70;
            var a = document.getElementById('total').style.height = x + 'px';
        }
        document.oncontextmenu = function () { return false };

        function fn_getSaveBtn() {
            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                alert('Invalid Save Button Click');
            }

        }

       
    </script>
    <script type="text/javascript">

        

        $(function () {
            $("#slidedown").click(function (event) {
                $('#MenuConatiner').css("width", "30%");
                $('#formHolder').css("width", "70%");
                event.preventDefault();
                //                $("#divMenu").slideToggle();
                $("#divMenu").slideDown();
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");

            });

            $("#SlideUp").click(function (event) {
                $('#MenuConatiner').css("width", "5%");
                $('#formHolder').css("width", "95%");
                event.preventDefault();
                $("#divMenu").slideUp();
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");

            });

            

            $("#ibQuery").click(function (event) {
                //$("#hid_toggle").val('a');
                //alert($('#divSearch').width());

                var hid_val = $("#hid_toggle").val();
                alert(hid_val);
                if (hid_val == 'b') {
                    $("#hid_toggle").val('b');
                    alert('b');
                    $('#divSearch').css("width", "50%");
                    $('#divEntry').css("width", "50%");
                    document.getElementById('Searchfrm').contentDocument.location.reload(true);
                }
                else {
                    $("#hid_toggle").val('b');
                    //alert($("#hid_toggle").val);
                    $('#divSearch').css("width", "0%");
                    $('#divEntry').css("width", "100%");
                }
                event.preventDefault();
                $("#divSearch").slideToggle();
                //$("#divMenu").Edit();
                //                $("#SlideUp").css("display", "none");
                //                $("#slidedown").css("display", "inline");

            });
        });
    </script>
    <style type="text/css">
        #container
        {
            display: block;
            width: 190px;
            height: 20px;
            padding: 5px;
            background: #ddd url(/files/box.jpg) no-repeat;
        }
        
        #divMenu
        {
            width: 188px;
            display: none;
            padding: 5px;
            border: 1px solid #ddd;
            background-color: #eee; /*position: absolute;*/ /* used to over the  div tag(box) on header */
        }
    </style>
</head>
<body bgcolor="#707070">
    <form id="form1" runat="server">
    <div class="total" id="total" style="background-color: White">
        <div class="header">
            <div style="width: 100%">
                <img src="../Images/Logo.png" width="75px" height="75px" />
            </div>
        </div>
        <div id="MenuConatiner" style="width: 5%; float: left">
            <img src="../Images/SlideDown.png" width="40px" id="slidedown" />
            <img src="../Images/SlideUp.png" width="40px" id="SlideUp" style="display: none" />
            <div id="divMenu" style="width: 300px">
                <div class="menu" runat="server" id="div_Menu">
                    <ul>
                        <li><a href="#" title="Products">File</a>
                            <ul>
                                <li><a href="../Client/RegintrationEntry.aspx" title="Location" target="centerfrm"
                                    onclick="ifrmaster">GUI</a></li>
                                <li><a href="../Admin/CountryMasterEntry.aspx" title="Master Sub Groups" target="centerfrm"
                                    onclick="ifrmaster">Country</a></li>
                                <li><a href="../Admin/UserDetailsEntry.aspx" title="Ledgers" target="centerfrm" onclick="ifrmaster">
                                    New User</a> </li>
                                <li><a href="../Admin/CountrySettingsEntry.aspx" title="Ledgers" target="centerfrm"
                                    onclick="ifrmaster">Country Settings</a> </li>
                                <li><a href="../Admin/ProductEntry.aspx" title="Ledgers" target="centerfrm" onclick="ifrmaster">
                                    Product Database</a> </li>
                                <li><a href="../Admin/ProductSelectionEntry.aspx" title="Ledgers" target="centerfrm"
                                    onclick="ifrmaster">Product Selection Product Selection Product Selection Product
                                    Selection</a> </li>
                            </ul>
                        </li>
                        <li><a href="#" title="Vouchers">Menu Header</a>
                            <ul>
                                <li><a href="../Underconstruction.aspx" title="BL / Voucher-wise" target="centerfrm"
                                    onclick="ifrmaster">Menu 1</a> </li>
                                <li><a href="../Underconstruction.aspx" title="BL / Voucher-wise" target="centerfrm"
                                    onclick="ifrmaster">Menu 2</a> </li>
                                <li><a href="../Underconstruction.aspx" title="Statistics" target="centerfrm" onclick="ifrmaster">
                                    Menu 3</a> </li>
                                <li><a href="#" title="Vouchers">Menu Header</a>
                                    <ul>
                                        <li><a href="../Client/DelTest.aspx" title="Statistics" target="centerfrm" onclick="ifrmaster">
                                            Menu 1</a> </li>
                                        <li><a href="../Underconstruction.aspx" title="Ledger View" target="centerfrm" onclick="ifrmaster">
                                            Menu 2</a> </li>
                                        <li><a href="../Underconstruction.aspx" title="Customer Retention" target="centerfrm"
                                            onclick="ifrmaster">Menu 3</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="MenuMaster.aspx" title="Query" target="centerfrm" onclick="ifrmaster">Menu</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="width: 95%; float: left" id="formHolder">
            <div style="width: 100%">
                <%--<div style="width: 7%; float: left">--%>
                <img src="../Images/Edit_icon.png" width="25px" height="25px" id="Edit"  />
                &nbsp;
                <img src="../Images/Query_icon.png" width="25px" height="25px" id="ibQuery"  />
                
                <%--</div>--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--<div style="width: 7%; float: left">--%>
                <img src="../Images/Edit_icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Query_icon.png" width="25px" height="25px" />
                <%--</div>--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--<div style="width: 14%; float: left">--%>
                <img src="../Images/Backfast_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Back_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Next_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/NextFast_Icon.png" width="25px" height="25px" />
                <%--</div>--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--<div style="width: 14%; float: left">--%>
                <img src="../Images/Org_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Back_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/wf_icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/wfs_icon.png" width="25px" height="25px" />
                <%--</div>--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--<div style="width: 10%; float: left">--%>
                <img src="../Images/Org_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Lang_Icon.png" width="25px" height="25px" />
                &nbsp;
                <img src="../Images/Question_Icon.png" width="25px" height="25px" />
                <%--</div>--%>
            </div>
            <div id="divSearch" style="width: 0%; float: left">
                <iframe id="Searchfrm" name="Searchfrm"  class="frames" frameborder="0" style="border-style: solid; 
                    border-width: 1px" src="../QueryForm/QueryForm.aspx" ></iframe>
            </div>
            <div id="divEntry" style="width: 100%; height: 600px; float: left">
                <iframe id="ifrmaster" name="centerfrm" class="frames" frameborder="0" height="600px"
                    style="border-style: solid; border-width: 1px"></iframe>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div class="footer ">
            <div class="adminFormFieldHeading">
                <marquee behavior="alternate">
                    Welcome to FIN
           </marquee>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_toggle" runat="server" Value="a" />
    </form>
</body>
</html>
