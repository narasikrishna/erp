﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMFixedNew.aspx.cs" Inherits="FIN.Client.MainPage.MMFixedNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="../Scripts/Slide/jcarousellite_1.0.1.js" type="text/javascript"></script>
    <link href="../Scripts/Slide/Jcarousellite.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery.rotate.1-1.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

        });
        $(function () {
            $(".jCarouselLite").jCarouselLite({
                btnNext: ".prev",
                btnPrev: ".next",
                circular: true,
                visible: 5,
                speed: 1500,
                beforeStart: function (a) {
                    $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.gif');
                    $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.gif');
                    $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.gif');
                    $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.gif');
                    $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.gif');
                    $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.gif');
                    $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.gif');
                    $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.gif');
                },
                afterEnd: function (a) {
                    $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.png');
                    $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.png');
                    $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.png');
                    $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.png');
                    $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.png');
                    $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.png');
                    $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.png');
                    $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.png');
                }
            });
        });

        function fn_OpenModule(ModuleName) {
            //            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP_OPEN.gif');
        }
        function fn_RotateModuel() {
            /* 
            $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.gif');
            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.gif');
            $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.gif');
            $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.gif');          
            $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.gif');
            $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.gif');
            $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.gif');
            $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.gif');*/
        }
        function fn_NormalModuel() {
            $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.png');
            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.png');
            $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.png');
            $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.png');
            $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.png');
            $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.png');
            $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.png');
            $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.png');
        }

        function fn_CloseBrowser() {
            window.close();
        }
    </script>
    <script type="text/javascript">

        function updateClock() {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = (currentHours < 12) ? "AM" : "PM";

            // Convert the hours component to 12-hour format if needed
            currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = (currentHours == 0) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;


            $("#clock").html(currentTimeString);

        }

        $(document).ready(function () {
            setInterval('updateClock()', 1000);
        });
    </script>
    <script type="text/javascript">
        document.write("<script src='../Scripts/JQueryValidatioin/jquery.validationEngine_" + '<%= Session["Sel_Lng"] %>' + ".js'><\/script>");
    </script>
    <style type="text/css">
        .modal
        {
            /*  position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
            */
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: transparent;
            z-index: 999;
        }
    </style>
</head>
<body style="margin: 0px 0px 0px 0px">
    <form id="form1" runat="server">
    <div style="width: 100%">
        <div style="height: 150px; width: 100%; background: url('../images/MHeaderRight.jpg')">
            <div style="background: url('../images/MHeader.jpg') no-repeat left; height: 150px">
                <div style="float: left">
                    <table style="height: 150px">
                        <tr valign="middle" style="height: 150px">
                            <td style="padding-left: 10px">
                                <img src="../Images/Logo.png" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: right; padding: 10px 30px 0 0">
                    <table>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="lblUserName">
                                    User
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div3">
                                    <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="divTime">
                                    Time
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="clock">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div style="height: 25px; width: 100%; background-color: rgb(243,10,10);" id="divMenuLink">
            <div style="float: right; padding-right: 20px">
                <a onclick="fn_CloseBrowser()">
                    <img src="../Images/BrowserClose.gif" alt="Close" width="25px" height="25px" onclick="fn_CloseBrowser()" /></a>
            </div>
        </div>
        <div style="clear: both; height: 10px;">
        </div>
        <div align="center" style="width: 100%">
            <br />
            <div style="width: 65%; float: left;">
                <div style="height: 350px;">
                    <div style="width: 420px; height: 100%; float: left;">
                        <div class="myheadera" align="center">
                            <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
                                Style="width: 95%; height: 80%;" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
                                CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                                OnRowDataBound="gvWorkFlow_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                        ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                                        DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
                                    <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperMsgID" Text='<%#Eval("workflow_message") %>'
                                                Width="190px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Text='<%#Eval("Level_Code") %>'
                                                Width="40px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Text='<%#Eval("remarks") %>'
                                                Width="100px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="pgr"></PagerStyle>
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="width: 420px; height: 100%; float: left;">
                        <div class="myheadera" align="center">
                            <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc,ALERT_USER_LEVEL_CODE"
                                Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
                                PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderStyle-ForeColor="White" HeaderText="Alert Date">
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperAlertMsgID" Text='<%#Eval("alert_message") %>'
                                                Width="190px" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgCloseAlert" runat="server" ImageUrl="~/Images/close.png"
                                                Width="20px" Height="20px" OnClick="imgCloseAlert_Click" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="pgr"></PagerStyle>
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 35%; height: 350px; float: left;">
                <%-- Dashboard--%>
            </div>
        </div>
        <div style="clear: both; height: 10px;">
        </div>
        <div align="center" style="width: 100%">
            <div align="center">
                <table>
                    <tr>
                        <td>
                            <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                Organization
                            </div>
                        </td>
                        <td>
                            <div class="divtxtBox" style="float: left; width: 250px">
                                <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width: 100%; height: 100px;" align="center" runat="server" id="divModuleSel">
            <div class="carousel main" align="center" style="width: 1100px;">
                <a class="prev" href="#"></a>
                <div class="jCarouselLite" style="visibility: visible; overflow: hidden; position: relative;">
                    <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none;
                        z-index: 1;">
                        <li>
                            <asp:ImageButton ID="imgbtnGL" runat="server" ImageUrl="~/Images/Module/GL.png" OnClick="imgbtnGL_Click"
                                Height="100px" Width="200px" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnAP" runat="server" ImageUrl="~/Images/Module/AP.png" OnClick="imgbtnAP_Click"
                                Height="100px" Width="200px" onmouseover="fn_OpenModule('AP')" onmouseout="fn_NormalModuel()" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnHR" runat="server" ImageUrl="~/Images/Module/HR.png" OnClick="imgbtnHR_Click"
                                Height="100px" Width="200px" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnCA" runat="server" ImageUrl="~/Images/Module/CA.png" OnClick="imgbtnCA_Click"
                                Height="100px" Width="200px" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnPA" runat="server" ImageUrl="~/Images/Module/PA.png" OnClick="imgbtnPA_Click"
                                Height="100px" Width="200px" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnFA" runat="server" ImageUrl="~/Images/Module/FA.png" OnClick="imgbtnFA_Click"
                                Height="100px" Width="200px" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnAR" runat="server" ImageUrl="~/Images/Module/AR.png" Height="100px"
                                Width="200px" OnClick="imgbtnAR_Click" /></li>
                        <li>
                            <asp:ImageButton ID="imgbtnSSM" runat="server" ImageUrl="~/Images/Module/SSM.png"
                                Height="100px" Width="200px" OnClick="imgbtnSSM_Click1" /></li>
                    </ul>
                </div>
                <a class="next" href="#"></a>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <div class="loading" align="center">
        <img src="../Images/loading.gif" alt="" />
    </div>
    <script src="../LanguageScript/Generic.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        $("#ddlOrg").change(function () {
            ShowProgress();
        });
    </script>
    </form>
</body>
</html>
