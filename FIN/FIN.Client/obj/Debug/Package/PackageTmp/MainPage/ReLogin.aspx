﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReLogin.aspx.cs" Inherits="FIN.Client.MainPage.ReLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/menu_style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hf_S_OrganizationID" runat="server" Value="" />
    <asp:HiddenField ID="hf_S_Multilanguage" runat="server" Value="" />
    <asp:HiddenField ID="hf_S_ModuleName" runat="server" Value="" />
    <asp:HiddenField ID="hf_S_ModuleDesc" runat="server" Value="" />
    <asp:HiddenField ID="hf_pwdtryCount" runat="server" Value="0" />
    <div class="ConfirmForm" style="width: 900px; height: 300px; background-color: transparent">
        <table width="100%" style="height: 300px">
            <tr style="height: 20%">
                <td style="width: 40%">
                </td>
                <td style="width: 60%" align="center">
                    <asp:Label ID="lblError" runat="server" CssClass="lblBox" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr style="height: 108px">
                <td style="width: 40%;" rowspan="2" align="center">
                    <img src="../Images/clipart_B.png" />
                </td>
                <td style="background-image: url('../Images/User_B.png'); background-repeat: no-repeat;"
                    align="left" valign="top">
                    <div style="padding-left: 18%; padding-top: 6%">
                        <asp:TextBox CssClass="validate[required] txtBox" MaxLength="50" ID="txtUserID" runat="server"
                            Width="200px"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr style="height: 108px">
                <td style="width: 60%; background-image: url('../Images/Password_B.png'); background-repeat: no-repeat;"
                    align="left" valign="top">
                    <div style="padding-left: 18%; padding-top: 6%">
                        <asp:TextBox CssClass="validate[required] txtBox" TextMode="Password" MaxLength="50"
                            ID="txtPassword" runat="server" Width="200px"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr style="height: 10%">
                <td style="width: 40%">
                </td>
                <td style="width: 60%; padding-left: 100px">
                    <asp:Button runat="server" CssClass="Button" ID="btnYes" Text="Login" Width="80px"
                        Height="30px" OnClick="btnYes_Click" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="Button" ID="btnRLClose" Text="Cancel" Width="80px"
                        Style="display: none" Height="30px" />
                </td>
            </tr>
            <tr style="height: 10%">
                <td style="width: 40%">
                    &nbsp;
                </td>
                <td style="width: 60%">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
