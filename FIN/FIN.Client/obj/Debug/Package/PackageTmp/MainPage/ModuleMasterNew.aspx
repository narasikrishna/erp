﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModuleMasterNew.aspx.cs"
    Inherits="FIN.Client.MainPage.ModuleMasterNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="shortcut icon" href="../Images/logo.ico">
    <title>Meithree</title>
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <link href="../Scripts/JQueryTab/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQueryTab/jquery-ui.js" type="text/javascript"></script>
    <script>
        $(function () {
            $("#tabs").tabs();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <br />
        <br />
        <br />
        <br />
    <div align="center">
       
        <div id="tabs" style="width: 700px">
            <ul>
                <li><a href="#tabs-1"><img src="../Images/Module/GL.png" alt="GL" style="border:0px" /></a></li>
                <li><a href="#tabs-2"><img src="../Images/Module/AP.png" alt="GL" style="border:0px" /></a></li>
                <li><a href="#tabs-3"><img src="../Images/Module/HR.png" alt="GL" style="border:0px" /></a></li>
                <li><a href="#tabs-4"><img src="../Images/Module/CA.png" alt="CA" style="border:0px" /></a></li>
                <li><a href="#tabs-5"><a></li>
            </ul>
            <div id="tabs-1">
                <p>
                    <asp:ImageButton ID="imgbtnGL" runat="server" 
                        ImageUrl="~/Images/Module/GL-IMAGE.png" onclick="imgbtnGL_Click" />
                    
                </p>
            </div>
            <div id="tabs-2">
                <p>
                <asp:ImageButton ID="imgbtnAP" runat="server" 
                        ImageUrl="~/Images/Module/AP-IMAGE.png" onclick="imgbtnAP_Click" />
                    
                </p>
            </div>
            <div id="tabs-3">
                <p>
                 <asp:ImageButton ID="imgbtnHR" runat="server" 
                        ImageUrl="~/Images/Module/HR-IMAGE.png" onclick="imgbtnHR_Click" />                   
                   
                </p>
            </div>
            <div id="tabs-4">
                <p>
                    <asp:ImageButton ID="imgbtnCA" runat="server" 
                        ImageUrl="~/Images/Module/CA-IMAGE.jpg" onclick="imgbtnCA_Click"  />  
                </p>
            </div>
            <div id="tabs-5">
                <p>
                    
                    <asp:Button ID="btnSSM" runat="server" Text="SSM" OnClick="btnSSM_Click" />
                </p>
            </div>
        </div>
            
    </div>
    </form>
</body>
</html>
