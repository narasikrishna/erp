﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPageNew.aspx.cs" Inherits="FIN.Client.MainPage.MainPageNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico">
    <title>Meithree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/menu_style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        /***********************************************
        * IFrame SSI script II- © Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
        * Visit DynamicDrive.com for hundreds of original DHTML scripts
        * This notice must stay intact for legal use
        ***********************************************/

        //Input the IDs of the IFRAMES you wish to dynamically resize to match its content height:
        //Separate each ID with a comma. Examples: ["myframe1", "myframe2"] or ["myframe"] or [] for none:
        var iframeids = ["ifrmaster"]

        //Should script hide iframe from browsers that don't support this script (non IE5+/NS6+ browsers. Recommended):
        var iframehide = "yes"

        var getFFVersion = navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]
        var FFextraHeight = parseFloat(getFFVersion) >= 0.1 ? 16 : 0 //extra height in px to add to iframe in FireFox 1.0+ browsers

        function resizeCaller() {

            var dyniframe = new Array()
            for (i = 0; i < iframeids.length; i++) {
                if (document.getElementById)
                    resizeIframe(iframeids[i])
                //reveal iframe for lower end browsers? (see var above):
                if ((document.all || document.getElementById) && iframehide == "no") {
                    var tempobj = document.all ? document.all[iframeids[i]] : document.getElementById(iframeids[i])
                    tempobj.style.display = "block"
                }
            }
        }

        function resizeIframe(frameid) {
            var currentfr = document.getElementById(frameid)
            if (currentfr && !window.opera) {
                currentfr.style.display = "block"
                if (currentfr.contentDocument && currentfr.contentDocument.body.offsetHeight) //ns6 syntax
                {
                    currentfr.height = currentfr.contentDocument.body.offsetHeight + FFextraHeight + 50;

                    if (currentfr.height < 400)
                        currentfr.height = 400;

                }
                else if (currentfr.Document && currentfr.Document.body.scrollHeight) //ie5+ syntax
                {
                    currentfr.height = currentfr.Document.body.scrollHeight + 50;

                    if (currentfr.height < 400)
                        currentfr.height = 400;

                }
                document.getElementById('CenterLeft').style.height = currentfr.height + 'px';
                document.getElementById('Searchfrm').style.height = currentfr.height + 'px';
                document.getElementById('divEntry').style.height = currentfr.height + 'px';
                document.getElementById('divSearch').style.height = currentfr.height + 'px';
                document.getElementById('DivCenter').style.height = currentfr.height + 'px';
                document.getElementById('CenterContainer').style.height = currentfr.height + 'px';





                if (currentfr.addEventListener)
                    currentfr.addEventListener("load", readjustIframe, false)
                else if (currentfr.attachEvent) {
                    currentfr.detachEvent("onload", readjustIframe) // Bug fix line
                    currentfr.attachEvent("onload", readjustIframe)
                }
            }
        }

        function readjustIframe(loadevt) {
            var crossevt = (window.event) ? event : loadevt
            var iframeroot = (crossevt.currentTarget) ? crossevt.currentTarget : crossevt.srcElement
            if (iframeroot)
                resizeIframe(iframeroot.id);
        }

        function loadintoIframe(iframeid, url) {
            if (document.getElementById)
                document.getElementById(iframeid).src = url
        }

        if (window.addEventListener)
            window.addEventListener("load", resizeCaller, false)
        else if (window.attachEvent)
            window.attachEvent("onload", resizeCaller)
        else
            window.onload = resizeCaller
    </script>
    <script type="text/javascript" language="javascript">

        function fn_ResizeEntryScreen(screenName) {

            $("#hid_toggle").val('CLOSE');
            //alert($("#hid_toggle").val);
            $('#divSearch').css("width", "0%"); // fixed Size query and entry 0 changed to 30
            $('#divEntry').css("width", "98%");
            $("#MenuContainer").slideUp();
            $("#SlideUp").css("display", "none");
            $("#slidedown").css("display", "inline");

            $("#div_MenuTree").html($("#" + '<%= hf_TreeLink.ClientID %>').val() + " --> " + screenName);
            document.getElementById('Searchfrm').contentDocument.location.reload(true);
            $("#ifrmaster").contents().find("body").html('');

        }
        function fn_getSaveBtn() {
            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                alert('Invalid Save Button Click');
            }

        }

        function fn_QueryFormDisplay() {

            var hid_val = $("#hid_toggle").val();
            $("#Searchfrm").contents().find("body").html('');
            if (hid_val == 'CLOSE') {
                $("#hid_toggle").val('OPEN');
                $('#divSearch').css("width", "30%"); // fixed Size query and entry 50 changed to 30  AND 50 to 70
                $('#divEntry').css("width", "68%");
                //event.preventDefault();
                $("#divSearch").slideDown();
                var sfrm = document.getElementById('Searchfrm');
               // alert(sfrm);
                sfrm.src = sfrm.src;
                //sfrm.contentDocument.location.reload(true);
               // alert('close');
            }
            else {
                $("#hid_toggle").val('CLOSE');
                //alert($("#hid_toggle").val);
                $('#divSearch').css("width", "0%"); // fixed Size query and entry 0 changed to 30
                $('#divEntry').css("width", "98%");
               // event.preventDefault();
                //$("#divSearch").slideUP();
                alert('open');
            }
        }    
    </script>
    <script type="text/javascript">
        $(function () {
            $(document).ready(function () {
                fn_ResizeEntryScreen('');
            });
            $("#slidedown").click(function (event) {
                //alert('Down _ inside');
                event.preventDefault();
                $("#MenuContainer").slideDown();
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");

            });

            $("#SlideUp").click(function (event) {
                //alert('Up _ inside');
                event.preventDefault();
                $("#MenuContainer").slideUp();
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");

            });




        });
        
    </script>
    <style type="text/css">
        .divMenu
        {
            display: none;
            padding: 5px;
            border: 1px solid #ddd;
            background-color: #eee;
            position: absolute; /* used to over the  div tag(box) on header */
        }
    </style>
</head>
<body style="margin: 0px 0px 0px 0px">
    <form id="form1" runat="server">
    <div style="width: 100%">
        <div style="height: 150px; width: 100%; background: url('../images/MHeaderRight.jpg')">
            <div style="background: url('../images/MHeader.jpg') no-repeat left; height: 150px">
                <table style="height: 150px">
                    <tr valign="middle" style="height: 150px">
                        <td style="padding-left: 10px">
                            <img src="../Images/Logo.png" />
                        </td>
                    </tr>
                </table>
            </div>
             <div style="float: right; padding: 10px 30px 0 0">
                    <table>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="lblOrg">
                                    Organization
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div1">
                                    <asp:Label ID="lblOrgName" runat="server" Text="Organization"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="lblUserName">
                                    User
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div3">
                                    <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="divModuleName">
                                    Module Name
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div4">
                                    <asp:Label ID="lblModuleName" runat="server" Text="Module Name"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="divTime">
                                    Time
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="clock">
                                    
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
        </div>
        <div style="height: 25px; width: 100%; background-color: rgb(243,10,10);" id="divMenuLink">
            <div style="float: left">
                <img style="padding-left: 10px" src="../Images/SlideDown.png" width="25px" id="slidedown" />
                <img src="../Images/SlideUp.png" width="25px" id="SlideUp" style="padding-left: 10px;
                    display: none" />
            </div>
            <div id="div_MenuTree" style="float: left; padding-left: 20px; color: White; font-family: Verdana;
                font-size: 12px;">
            </div>
        </div>
        <div style="width: 100%" id="MenuContainer" class="divMenu">
            <div class="menu" runat="server" id="div_Menu">
            </div>
        </div>
        <div style="width: 100%" id="CenterContainer">
            <div style="float: left; width: 7%; height: 300px; background: url('../images/MFooterTop.png') no-repeat left bottom"
                id="CenterLeft">
            </div>
            <div style="float: left; width: 87%" id="DivCenter">
                <div id="divSearch" style="width: 0%; float: left; border-right: 2px solid gray">
                    <iframe id="Searchfrm" name="Searchfrm" class="frames" frameborder="0" style="border-style: solid;
                        border-width: 0px" src="../QueryForm/QueryForm.aspx"></iframe>
                </div>
                <div id="divEntry" style="width: 99%; float: left; border-left: 2px solid gray;">
                    <iframe runat="server" id="ifrmaster" name="centerfrm" class="frames" frameborder="0"
                        style="border-style: solid; border-width: 0px" align="middle" src="../DashBoard.aspx"> </iframe>
                </div>
            </div>
            <div style="float: right; width: 5%" id="divSIcon" align="right">
                <table width="100%" style="text-align: center" border="0px">
                    <tr>
                        <td style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Query_icon.png" width="25px" height="25px" id="ibQuery" alt="Query"
                                onclick="fn_QueryFormDisplay()" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Save_Icon.png" width="25px" height="25px" id="ibSave" onclick="fn_getSaveBtn()"
                                alt="Save" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Clear.png" width="25px" height="25px" id="ibClear" alt="Clear" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Org_Icon.png" width="25px" height="25px" id="ibOrg" alt="Org" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/S_Icon.png" width="25px" height="25px" id="ibS" alt="$" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/wf_icon.png" width="25px" height="25px" id="ibWF" alt="WF" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/wfs_icon.png" width="25px" height="25px" id="ibWFS" alt="WFS" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/ABR.png" width="25px" height="25px" id="ibABR" alt="ABR" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Lang_Icon.png" width="25px" height="25px" id="ibLang" alt="Lang" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../Images/Question_Icon.png" width="25px" height="25px" id="ibQuestion"
                                alt="?" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div style="width: 100%; height: 100px; background-image: url('../Images/MFooter.png');"
            id="divFooter">
        </div>
    </div>
    <asp:HiddenField ID="hid_toggle" runat="server" Value="CLOSE" />
    <asp:HiddenField ID="hf_TreeLink" runat="server" Value="Home" />

    <div style="clear: both">
        </div>
        <div id="divOrganization">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <cc2:ModalPopupExtender ID="mpeOrg" runat="server" TargetControlID="imgOrg" PopupControlID="pnlOrg"
                CancelControlID="btnClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlOrg" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                    Oranization
                                </div>
                            </td>
                            <td>
                                <div class="divtxtBox" style="float: left; width: 250px">
                                    <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
