﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master"
    AutoEventWireup="true" CodeBehind="RightFrame.aspx.cs" Inherits="FIN.Client.MainPage.RightFrame" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <table width="100%">
        <tr>
            <td>
                <marquee behavior="scroll" direction="up" scrollamount="2" onmouseover="this.setAttribute('scrollamount', 0, 0);"
                    onmouseout="this.setAttribute('scrollamount', 2, 0);">
                <div runat ="server" id="div_newsevent" >
                
                </div></marquee>
            </td>
        </tr>
        <tr>
            <td>
                <table  id ="websitehit_det" runat= "server">
                    <tr>
                        <td class ="adminFormFieldHeading">
                            <asp:Label ID="lbl_WebSiteHit" runat="server" Text="WebSite Hit : "></asp:Label>
                        </td>
                        <td class ="adminFormFieldHeading">
                            <asp:Label ID="lbl_CountofHit" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class ="adminFormFieldHeading">
                            <asp:Label ID="lbl_LoginHt" runat="server" Text="Login Hit : "></asp:Label>
                        </td>
                        <td class ="adminFormFieldHeading">
                            <asp:Label ID="lbl_countoflogin" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
            
            </td>
        </tr>
    </table>
</asp:Content>
