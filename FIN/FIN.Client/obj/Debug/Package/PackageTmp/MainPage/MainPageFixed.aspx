﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/MainPage/MainPageFixed.aspx.cs"
    Inherits="FIN.Client.MainPage.MainPageFixed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico">
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/menu_style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_ChangeBtnSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(35);
            $("#" + cntrlId).height(35);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('png', 'gif'));
        }
        function fn_ChangeBtnReSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(25);
            $("#" + cntrlId).height(25);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('gif', 'png'));
        }
        function ShowSubMenu(SubMenuDiv) {

            // alert(document.getElementById(SubMenuDiv));//.setAttribute("display", "inline");
            $("#" + SubMenuDiv).toggle('slow');
            //$("#" + SubMenuDiv).css("backgroundColor", "red");
            //$("#" + SubMenuDiv.replace('C_','')).css("backgroundColor", "red");

            var hid_val = $("#hf_PreviousSubMenu").val();
            if (hid_val == '') {
                $("#hf_PreviousSubMenu").val(SubMenuDiv)
            }
            else {
                //alert(hid_val);
                if (hid_val == SubMenuDiv) {
                }
                else {
                    $("#" + hid_val).css('display', 'none');
                    $("#hf_PreviousSubMenu").val(SubMenuDiv)
                }

            }
        }
        function fn_ResizeEntryScreen(screenName) {

            $("#hid_toggle").val('CLOSE');
            //alert($("#hid_toggle").val);
            $('#divSearch').css("width", "0px");
            $('#divEntry').css("width", "1245px");
            $("#MenuContainer").slideUp();
            $("#SlideUp").css("display", "none");
            $("#slidedown").css("display", "inline");
            // alert(screenName);
            $("#div_MenuTree").html($("#" + '<%= hf_TreeLink.ClientID %>').val() + " --> " + screenName);
            document.getElementById('Searchfrm').contentDocument.location.reload(true);
            $("#ifrmaster").contents().find("body").html('');
        }
        function fn_getSaveBtn() {

            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                alert('Invalid Save Button Click');
            }

        }

        $(function () {
            $(document).ready(function () {
                fn_ResizeEntryScreen('');
            });
            $("#slidedown").click(function (event) {
                //alert('Down _ inside');
                event.preventDefault();
                $("#MenuContainer").slideDown();
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");

            });

            $("#SlideUp").click(function (event) {
                //alert('Up _ inside');
                event.preventDefault();
                $("#MenuContainer").slideUp();
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");

            });
        });


        function fn_QueryFormDisplay() {

            var hid_val = $("#hid_toggle").val();
            $("#Searchfrm").contents().find("body").html('');
            if (hid_val == 'CLOSE') {
                $("#hid_toggle").val('OPEN');
                $('#divSearch').css("width", "450px");
                $('#divEntry').css("width", "795px");
                $("#divSearch").slideDown('slow');
                var sfrm = document.getElementById('Searchfrm');
                sfrm.src = sfrm.src;
                $('#divEntry').css("border-right-width", "2px");
            }
            else {
                $("#hid_toggle").val('CLOSE');
                $('#divSearch').css("width", "0px");
                $('#divEntry').css("width", "1245px");
                $('#divEntry').css("border-right-width", "0px");

            }
            return false;
        }    
    </script>
    <style type="text/css">
        .divMenu
        {
            display: none;
            padding: 5px;
            border: 1px solid #ddd;
            background-color: #eee;
            position: absolute; /* used to over the  div tag(box) on header */
            font-family: Verdana;
            font-size: 12px;
            text-decoration: none;
        }
        
        .menu A:link
        {
            color: blue;
            text-decoration: none;
            font-weight: normal;
        }
        .menu A:visited
        {
            color: blue;
            text-decoration: none;
            font-weight: normal;
        }
        .menu A:active
        {
            color: black;
            text-decoration: none;
        }
        .menu A:hover
        {
            color: black;
            text-decoration: none;
            font-weight: bold;
        }
        .divContainer
        {
            width: 1365px;
            height: 670px;
        }
        .divCenterContainer
        {
            height: 465px;
            /*background-image: url('../Images/MainPage/Center.png');*/
            background-repeat: no-repeat;
            background-color: White;
        }
    </style>
    <style type="text/css">
        .modal
        {
            /*  position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
            */
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: transparent;
            z-index: 999;
        }
    </style>
    <script type="text/javascript">
        function fn_CloseBrowser() {
            window.close();
        }
        function fn_ShowLang() {

            $("#divListLang").toggle('slow');
        }
        function fn_HideLang() {
            $("#divListLang").toggle('slow');
            //            document.getElementById('ifrmaster').contentWindow.fn_changeLng('<%= Session["Sel_Lng"] %>');
        }

    </script>
    <script type="text/javascript">

        function updateClock() {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = (currentHours < 12) ? "AM" : "PM";

            // Convert the hours component to 12-hour format if needed
            currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = (currentHours == 0) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;


            $("#clock").html(currentTimeString);

        }

        $(document).ready(function () {
            setInterval('updateClock()', 1000);
        });
    </script>
</head>
<body style="margin: 0px 0px 0px 0px;">
    <form id="form1" runat="server">
    <div class="divContainer">
        <div style="height: 150px; width: 100%; background: url('../images/MHeaderRight.jpg')">
            <div style="background: url('../images/MHeader.jpg') no-repeat left; height: 150px">
                <div style="float: left">
                    <table style="height: 150px">
                        <tr valign="middle" style="height: 150px">
                            <td style="padding-left: 10px">
                                <img src="../Images/Logo.png" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: right; padding: 10px 30px 0 0">
                    <table>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="lblOrg">
                                    Organization
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div1">
                                    <asp:Label ID="lblOrgName" runat="server" Text="Organization"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="lblUserName">
                                    User
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div3">
                                    <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="divModuleName">
                                    Module Name
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="Div4">
                                    <asp:Label ID="lblModuleName" runat="server" Text="Module Name"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 110px" id="divTime">
                                    Time
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left" id="clock">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div style="height: 25px; width: 100%; background-color: rgb(243,10,10);" id="divMenuLink">
            <div style="float: left">
                <img style="padding-left: 10px" src="../Images/SlideDown.png" width="25px" id="slidedown" />
                <img src="../Images/SlideUp.png" width="25px" id="SlideUp" style="padding-left: 10px;
                    display: none" />
            </div>
            <div id="div_MenuTree" style="float: left; padding-left: 20px; color: White; font-family: Verdana;
                font-size: 12px;">
            </div>
            <div style="float: right; padding-right: 20px">
                <a onclick="fn_CloseBrowser()">
                    <img src="../Images/BrowserClose.gif" alt="Close" width="25px" height="25px" onclick="fn_CloseBrowser()"
                        id="imgbtnClose1" /></a>
            </div>
        </div>
        <div style="width: 98%" id="MenuContainer" class="divMenu">
            <div class="menu" runat="server" id="div_Menu">
            </div>
        </div>
        <div style="width: 100%;" id="CenterContainer" class="divCenterContainer">
            <div style="float: left;" id="DivCenter">
                <div id="divEntry" style="width: 1245px; height: 465px; float: left; border-left: 0px solid gray;"
                    runat="server">
                    <iframe runat="server" id="ifrmaster" name="centerfrm" class="frames" frameborder="0"
                        allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;">
                    </iframe>
                </div>
                <div id="divSearch" style="width: 0px; height: 465px; float: left; border-left: 0px solid gray">
                    <iframe id="Searchfrm" name="Searchfrm" class="frames" frameborder="0" allowtransparency="true"
                        style="border-style: solid; border-width: 0px; background-color: transparent"
                        src="../QueryForm/QueryForm.aspx"></iframe>
                </div>
            </div>
            <div style="float: right; width: 115px; height: 465px; background-image: url('../Images/MainPage/curve-menu.png');
                background-repeat: no-repeat;" id="divSIcon">
                <table width="115px">
                    <tr>
                        <td rowspan="11" valign="middle">
                            <div style="float: right; position: static; display: none; font-family: Verdana;
                                font-size: 12px; color: White; padding: 20px" id="divListLang">
                                <table cellspacing="10px" style="color: White">
                                    <tr>
                                        <td align="right">
                                            <a id="lngCancel" onclick="fn_HideLang()">
                                                <img src="../Images/close.png" alt="Close" width="15px" height="15px" id="imgClose" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEN" runat="server" ToolTip="English" CommandName="Language"
                                                CommandArgument="EN" OnClick="ChangeLanguage">English</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnAr" runat="server" ToolTip="العربية" CommandName="لغة"
                                                CommandArgument="AR" OnClick="ChangeLanguage">العربية</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td style="height: 18px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 80px">
                                    </td>
                                    <td style="width: 25px">
                                        <%-- <img src="../Images/MainPage/search-icon.png" width="25px" height="25px" id="ibQuery" runat="server"
                                            title="Search" alt="Search" onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" />--%>
                                        <div id="div_wf_query" runat="server" visible="false">
                                            <img src="../Images/MainPage/search-icon.png" width="25px" height="25px" id="imgwfquery"
                                                alt="Search" /></div>
                                        <div id="div_Query" runat="server" visible="true">
                                            <img src="../Images/MainPage/search-icon.png" width="25px" height="25px" id="ibQuery"
                                                alt="Search" onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)"
                                                onmouseout="fn_ChangeBtnReSize(this)" />
                                        </div>
                                    </td>
                                    <td style="width: 10px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 50px">
                                    </td>
                                    <td style="width: 25px">
                                        <%--  <img src="../Images/MainPage/Save.png" width="25px" height="25px" id="Img1" alt="Save" runat="server"
                                            title="Save" onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                        --%>
                                        <div id="div_wf_Save" runat="server" visible="false">
                                            <img src="../Images/MainPage/Save.png" width="25px" height="25px" alt="Save" title="Save"
                                                id="imgWfSave" />
                                        </div>
                                        <div id="div_Save" runat="server" visible="true">
                                            <img src="../Images/MainPage/Save.png" width="25px" height="25px" alt="Save"
                                                id="imgWfSave1" onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)"
                                                onmouseout="fn_ChangeBtnReSize(this)" />
                                        </div>
                                    </td>
                                    <td style="width: 40px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 30px">
                                    </td>
                                    <td style="width: 25px">
                                        <%--<img src="../Images/MainPage/Clear.png" width="25px" height="25px" id="Img2" alt="Clear"  />--%>
                                        <asp:ImageButton ID="imgClear" ImageUrl="../Images/MainPage/Clear.png" Width="25px"
                                            Height="25px" runat="server" OnClick="imgClear_Click" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 60px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 20px">
                                    </td>
                                    <td style="width: 25px">
                                        <asp:ImageButton ID="imgOrg" ImageUrl="../Images/MainPage/org.png" Width="25px"
                                            Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 70px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 15px">
                                    </td>
                                    <td style="width: 25px">
                                        <%--<img src="../Images/MainPage/s-icon.png" width="25px" height="25px" id="Img4" alt="$" title="Currency" runat="server"
                                            onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />--%>
                                        <asp:ImageButton ID="ImgCurrency" ImageUrl="../Images/MainPage/s-icon.png" Width="25px"
                                            Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 75px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 17px">
                                    </td>
                                    <td style="width: 25px">
                                        <asp:ImageButton ID="imgWF" ImageUrl="../Images/MainPage/wf.png" Width="25px"
                                            Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                            OnClick="imgWF_Click" />
                                    </td>
                                    <td style="width: 73px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25px">
                                    </td>
                                    <td style="width: 25px">
                                        <img src="../Images/MainPage/wf.png" width="25px" height="25px" id="Img6" alt="WFS" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 65px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25px">
                                    </td>
                                    <td style="width: 25px">
                                        <asp:ImageButton ID="imgbtnABR" ImageUrl="../Images/MainPage/abr.png" Width="25px"
                                            Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" OnClick="imgbtnABR_Click" />
                                    </td>
                                    <td style="width: 65px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 40px">
                                    </td>
                                    <td style="width: 25px">
                                        <%--  <img src="../Images/MainPage/lang.png" width="25px" height="25px" onclick="fn_ShowLang()"  runat="server"
                                            id="Img8" alt="Language" title="Language" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                        --%>
                                        <div id="div_wf_lng" runat="server" visible="false">
                                            <img src="../Images/MainPage/lang.png" width="25px" height="25px" alt="Language"
                                                id="imgLang" />
                                        </div>
                                        <div id="div_lng" runat="server" visible="true">
                                            <img src="../Images/MainPage/lang.png" width="25px" height="25px" onclick="fn_ShowLang()"
                                                id="imgChngLang" alt="Language" onmouseover="fn_ChangeBtnSize(this)"
                                                onmouseout="fn_ChangeBtnReSize(this)" /></div>
                                    </td>
                                    <td style="width: 50px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 70px">
                                    </td>
                                    <td style="width: 25px">
                                        <%--  <img src="../Images/MainPage/help.png" width="25px" height="25px" id="Img9" alt="Help" title="Help"
                                            onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                        --%>
                                        <asp:ImageButton ID="ImgHelp" ImageUrl="../Images/MainPage/help.png" Width="25px" 
                                            Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 20px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div style="width: 100%; height: 15px; background-color: rgb(243,10,10)" id="divFooter">
            <div style="float: right; color: White; font-family: Verdana; font-size: 10px;">
                Powered by VMV Systems Pvt Ltd
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div id="divOrganization">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <cc2:ModalPopupExtender ID="mpeOrg" runat="server" TargetControlID="imgOrg" PopupControlID="pnlOrg"
                CancelControlID="btnClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlOrg" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                    Organization
                                </div>
                            </td>
                            <td>
                                <div class="divtxtBox" style="float: left; width: 250px">
                                    <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div style="clear: both">
        </div>
        <div id="divWorkFlow">
            <asp:HiddenField ID="hfWF" runat="server" />
            <cc2:ModalPopupExtender ID="mpeWF" runat="server" TargetControlID="hfWF" PopupControlID="pnlWF"
                CancelControlID="btnWFClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlWF" runat="server">
                <div class="ConfirmForm">
                    <table width="100%" id="tblWF" runat="server">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="btn" ID="btnWFClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:RadioButtonList ID="rblWF" runat="server" CssClass="lblBox">
                                    <asp:ListItem Value="WFST01" Selected="True">Approve</asp:ListItem>
                                    <asp:ListItem Value="WFST02">Reject</asp:ListItem>
                                    <asp:ListItem Value="WFST04">Need For Clarification</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="txtBox"
                                    Width="200px" Height="50px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblNeedRemarks" runat="server" Text="Please Type Remarks" CssClass="lblBox"
                                    Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div style="clear: both">
        </div>
        <div id="divABR">
            <asp:HiddenField ID="hfABR" runat="server" />
            <cc2:ModalPopupExtender ID="mpeABR" runat="server" TargetControlID="hfABR" PopupControlID="pnlABR"
                CancelControlID="btnABRClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlABR" runat="server">
                <div class="ConfirmForm">
                    <table width="100%" id="Table1" runat="server">
                        <tr>
                            <td align="right">
                                <asp:Button runat="server" CssClass="btn" ID="btnABRClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="div_ABTREC" runat="server" class="lblBox">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div style="clear: both">
    </div>
    <asp:HiddenField ID="hid_toggle" runat="server" Value="CLOSE" />
    <asp:HiddenField ID="hf_TreeLink" runat="server" Value="Home" />
    <asp:HiddenField ID="hf_PreviousSubMenu" runat="server" Value="" />
    <div class="loading" align="center">
        <img src="../Images/loading.gif" alt="" />
    </div>
    <script src="../LanguageScript/MainPage.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
    <script type="text/javascript">
        document.write("<script src='../Scripts/JQueryValidatioin/jquery.validationEngine_" + '<%= Session["Sel_Lng"] %>' + ".js'><\/script>");
    </script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        $("#btnSubmit").click(function (e) {
            ShowProgress();
        })
        $("#ddlOrg").change(function () {
            ShowProgress();
        });
    </script>
    </form>
</body>
</html>
