﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetDiscardEntry.aspx.cs" Inherits="FIN.Client.FA.AssetDiscardEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px;" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblTaxName">
                Asset Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlAssetName" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblTaxRate">
                Discard Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlDiscardType" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="2" Width="250px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                Discard Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpDiscardDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="dtpDiscardDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div4">
                Sell Price
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtSellPrice" CssClass="RequiredField   txtBox_N" MaxLength="13"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers,Custom"
                    ValidChars="." TargetControlID="txtSellPrice" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblEffectiveStartDate">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlCurrency" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged"
                    >
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="Div5">
                Exchange Rate Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpExchangeDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="dtpExchangeDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="didv">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div6">
                Exchange Rate
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtExchangeRate" MaxLength="50" CssClass="RequiredField   txtBox_N"
                    TabIndex="8" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="txtExchangeRate" ValidChars="." />
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div8">
                Customer Reference
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtCustomerReference" MaxLength="15" CssClass="RequiredField txtBox"
                    TabIndex="10" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divextype" visible="false">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                Exchange Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlExchangeType" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="Div7">
                Base Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlBaseCurrency" CssClass="RequiredField ddlStype" Width="255px"
                    TabIndex="9" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
         <div class="divRowContainer" align="right" >
            <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="btnPrint_Click" Visible="false" Style="border: 0px" />
            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Style="border: 0px" Visible="false"
                OnClick="imgBtnPost_Click" />
            <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png" Visible="false"
                Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" TabIndex="11"
                            CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
                $(document).ready(function () {
                    fn_changeLng('<%= Session["Sel_Lng"] %>');
                });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
