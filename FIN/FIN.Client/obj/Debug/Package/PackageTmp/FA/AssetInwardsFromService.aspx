﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetInwardsFromService.aspx.cs" Inherits="FIN.Client.FA.AssetInwardsFromService" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px;" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
          
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblTaxRate">
                Asset
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlAsset" CssClass="RequiredField ddlStype" runat="server" AutoPostBack="true"
                    TabIndex="2" 
                    onselectedindexchanged="ddlAsset_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
              <div class="lblBox  LNOrient" style=" width: 145px" id="lblTaxName">
                Third Party
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlThirdParty" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Inward Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpInwardDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtenader2"
                    TargetControlID="dtpInwardDate" OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblEffectiveStartDate">
                Service Cost
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="ntxtServiceCost" MaxLength="13" CssClass="RequiredField   txtBox_N"
                    TabIndex="4" runat="server" ></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtendaer1" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="ntxtServiceCost" ValidChars="." />
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlCurrency" CssClass="RequiredField ddlStype" 
                    AutoPostBack="True" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged"
                    TabIndex="5" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                Exchange Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpExchangeRateDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField  txtBox"
                    AutoPostBack="True" OnTextChanged="dtpExchangeDate_TextChanged" runat="server"
                    TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendaarExtender3"
                    TargetControlID="dtpExchangeRateDate" OnClientDateSelectionChanged="checkDate" />
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Exchange Rate
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="ntxtExchangeRate" MaxLength="13" CssClass="RequiredField   txtBox_N"
                    TabIndex="8" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxsExtender2" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="ntxtExchangeRate" ValidChars="." />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divexType" visible="false">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                Exchange Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlExchangeRateType" CssClass="RequiredField ddlStype" 
                    TabIndex="6" runat="server">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 145px" id="Div7">
                Base Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlBaseCurrency" CssClass="RequiredField ddlStype" 
                    TabIndex="9" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Payment Reference
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtPaymentReference" MaxLength="100" CssClass="RequiredField txtBox"
                    TabIndex="10" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
