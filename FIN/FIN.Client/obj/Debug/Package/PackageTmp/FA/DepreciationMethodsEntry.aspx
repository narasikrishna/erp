﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DepreciationMethodsEntry.aspx.cs" Inherits="FIN.Client.FA.DepreciationMethodsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="700px" DataKeyNames="DPRN_METHOD_MST_ID,DPRN_METHOD_ID,DPRN_TYPE_DURATION_ID"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="ASSET_LOCATION_ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDPRNMETHODMSTID" Text='<%# Eval("DPRN_METHOD_MST_ID") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblDPRNMETHODMSTID" Text='<%# Eval("DPRN_METHOD_MST_ID") %>' runat="server"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblDPRNMETHODMSTID" Text='' runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle CssClass="ColumnHide" />
                        <HeaderStyle CssClass="ColumnHide" />
                        <FooterStyle CssClass="ColumnHide" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" Text='<%# Eval("DPRN_NAME") %>' CssClass="Label_Style" runat="server"
                                Width="210px"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" MaxLength="100" CssClass="RequiredField   txtBox_en" Text='<%# Eval("DPRN_NAME") %>'
                                TabIndex="1" Width="210px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtName" MaxLength="100" CssClass="RequiredField   txtBox_en" Text='<%# Eval("DPRN_NAME") %>'
                                TabIndex="1" runat="server" Width="210px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Name(Arabic)">
                        <ItemTemplate>
                            <asp:Label ID="lblNameAr" Text='<%# Eval("DPRN_NAME_OL") %>' CssClass="Label_Style" runat="server"
                                Width="210px"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNameAr" MaxLength="100" CssClass="RequiredField   txtBox_ol" Text='<%# Eval("DPRN_NAME_OL") %>'
                                TabIndex="1" Width="210px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNameAr" MaxLength="100" CssClass="RequiredField   txtBox_ol" Text='<%# Eval("DPRN_NAME_OL") %>'
                                TabIndex="1" runat="server" Width="210px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" CssClass="Label_Style" Text='<%# Eval("DPRN_DESC") %>'
                                Width="100px" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" MaxLength="100" CssClass="RequiredField   txtBox"
                                TabIndex="2" Text='<%# Eval("DPRN_DESC") %>' runat="server" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" MaxLength="100" CssClass="RequiredField   txtBox"
                                Width="100px" TabIndex="2" Text='' runat="server"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Method">
                        <ItemTemplate>
                            <asp:Label ID="lblMethod" CssClass="Label_Style" Text='<%# Eval("DPRN_METHOD_ID") %>'
                                Width="95px" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlMethod" CssClass="RequiredField ddlStype" runat="server"
                                TabIndex="3" Width="95px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlMethod" CssClass="RequiredField ddlStype" runat="server"
                                TabIndex="3" Width="95px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rate">
                        <ItemTemplate>
                            <asp:Label ID="lblRate" CssClass="Label_Style" Text='<%# Eval("DPRN_RATE") %>' runat="server"
                                Width="70px"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRate" MaxLength="5" CssClass="RequiredField   txtBox_N" Text='<%# Eval("DPRN_RATE") %>'
                                TabIndex="4" runat="server" Width="70px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                TargetControlID="txtRate" ValidChars="." />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRate" MaxLength="5" CssClass="RequiredField   txtBox_N" Text='<%# Eval("DPRN_RATE") %>'
                                TabIndex="4" runat="server" Width="70px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom" TargetControlID="txtRate"
                                    ValidChars="." />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duration Type">
                        <ItemTemplate>
                            <asp:Label ID="lblDurationType" CssClass="Label_Style" Text='<%# Eval("DPRN_TYPE_DURATION_ID") %>'
                                Width="85px" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlDurationType" CssClass="RequiredField ddlStype" runat="server"
                                Width="85px" TabIndex="5">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlDurationType" CssClass="RequiredField ddlStype" runat="server"
                                Width="85px" TabIndex="5">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duration">
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" CssClass="Label_Style" Text='<%# Eval("DPRN_TYPE_DURATION") %>'
                                Width="60px" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="ntxtDuration" CssClass="RequiredField   txtBox_N" MaxLength="10"
                                TabIndex="6" Text='<%# Eval("DPRN_TYPE_DURATION") %>' runat="server" Width="60px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                                TargetControlID="ntxtDuration" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ntxtDuration" CssClass="RequiredField   txtBox_N" MaxLength="10"
                                TabIndex="6" runat="server" Width="60px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                                TargetControlID="ntxtDuration" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="7" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="8" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="9" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="10" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="11" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
