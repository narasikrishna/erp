﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ARCustomerSOA_DB.aspx.cs" Inherits="FIN.Client.AR_DASHBOARD.ARCustomerSOA_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
        runat="server">
        <div style="float: left; padding-left: 10px; padding-top: 2px">
            Customer - Sales Order Awaited
        </div>
        <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
            <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div id="divSOAwait">
        <asp:Chart ID="chrtSOAwait" runat="server" Width="500px" Height="310px" EnableViewState="true">
            <Series>
                <asp:Series Name="s_SOAwaitr" ChartType="Funnel" XValueMember="vendor_name" YValueMembers="counts"
                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                    <Area3DStyle Enable3D="True"></Area3DStyle>
                </asp:ChartArea>
            </ChartAreas>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom">
                </asp:Legend>
            </Legends>
        </asp:Chart>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="vendor_name" HeaderText="Customer"></asp:BoundField>
                <asp:BoundField DataField="counts" HeaderText="Records"></asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
