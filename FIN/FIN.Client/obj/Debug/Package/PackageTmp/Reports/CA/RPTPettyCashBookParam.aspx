﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTPettyCashBookParam.aspx.cs" Inherits="FIN.Client.Reports.CA.RPTPettyCashBookParam" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
<div class="divFormcontainer" style="width: 370px" id="div1">
 <div class="divRowContainer">
            
            <div class="lblBox" style="float: left; width: 200px" id="lblUser">
               User
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlUser" runat="server" TabIndex="1"  CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            </div>
            <div class="divClear_10">
        </div>
            <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDate">
               From Date
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                ></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            </div>
            <div class="divClear_10">
        </div>
            <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
               To Date
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                   ></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            </div>
    <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
      <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
</asp:Content>
