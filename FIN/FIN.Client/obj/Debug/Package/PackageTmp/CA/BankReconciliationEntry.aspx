﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankReconciliationEntry.aspx.cs" Inherits="FIN.Client.CA.BankReconciliationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 200px" id="lblBankShortName">
                Bank Short Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBankShortName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 200px" id="lblBranchShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBranchShortName" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlAccountNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="width: 100%;">
                <div>
                    <div style="width: 400px;">
                        <div class="myheadera" align="center">
                            <div class="lblBox  LNOrient" align="center">
                                System Transaction
                            </div>
                            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" DataKeyNames="CHECK_DTL_ID,DELETED"
                                CssClass="Grid" Style="width: 97%;" EmptyDataText="No Record Found" ShowHeaderWhenEmpty="True"
                                RowStyle-Wrap="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Trans Type" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTransType" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>'
                                                CssClass="EntryFont txtBox_N" Width="80px" Style="word-wrap: break-word"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtTransType" runat="server" CssClass="EntryFont txtBox_N" Width="80px"
                                                Style="word-wrap: break-word"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransType" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>'
                                                Width="80px" Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Number" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>' CssClass="EntryFont txtBox_N"
                                                Width="80px" Style="word-wrap: break-word"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNumber" runat="server" CssClass="EntryFont txtBox_N" Width="80px"
                                                Style="word-wrap: break-word"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>' Width="80px"
                                                Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trans Date" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="dtpTransDate" runat="server" CssClass="EntryFont  txtBox" Width="80px"
                                                Text='<%#  Eval("CHECK_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="dtpTransDate" runat="server" CssClass="EntryFont  txtBox" Width="80px"
                                                Style="word-wrap: break-word" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransDate" runat="server" Width="80px" Text='<%# Eval("CHECK_DT","{0:dd/MM/yyyy}") %>'
                                                Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cheque No" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                                                CssClass="EntryFont txtBox_N" Width="80px" Style="word-wrap: break-word"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtChequeNumber" runat="server" CssClass="EntryFont txtBox_N" Width="80px"
                                                Style="word-wrap: break-word"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                                                Width="80px" Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cheque Date" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="80px"
                                                Style="word-wrap: break-word" Text='<%#  Eval("CHECK_DT","{0:dd/MM/yyyy}") %>'
                                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="80px"
                                                Style="word-wrap: break-word" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChequeDate" runat="server" Width="80px" Text='<%# Eval("CHECK_DT","{0:dd/MM/yyyy}") %>'
                                                Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" ItemStyle-Wrap="true">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' CssClass="EntryFont txtBox_N"
                                                Width="150px" Style="word-wrap: break-word"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                                ValidChars=".," TargetControlID="txtAmount" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="EntryFont txtBox_N" Width="150px"
                                                Style="word-wrap: break-word"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                                ValidChars=".," TargetControlID="txtAmount" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' Width="150px"
                                                Style="word-wrap: break-word"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="width: 400px;">
                        <div class="myheadera" align="center">
                            <div class="lblBox LNOrient" align="center">
                                Bank Transaction
                            </div>
                            <asp:GridView ID="gvBankTrans" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,transaction_code"
                                Style="width: 99%;" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                                CssClass="Grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Statement No">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtStatementNo" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>'
                                                CssClass="EntryFont txtBox_N" Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtStatementNo" runat="server" CssClass="EntryFont txtBox_N" Width="150px"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatementNo" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cheque No">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                                                CssClass="EntryFont txtBox_N" Width="150px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtChequeNumber" runat="server" CssClass="EntryFont txtBox_N" Width="150px"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cheque Date">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                Text='<%#  Eval("CHECK_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                                            </cc2:CalendarExtender>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblChequeDate" runat="server" Width="100px" Text='<%# Eval("CHECK_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' CssClass="EntryFont txtBox_N"
                                                Width="100px"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                                ValidChars=".," TargetControlID="txtAmount" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="EntryFont txtBox_N" Width="100px"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                                ValidChars=".," TargetControlID="txtAmount" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' Width="150px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reconciled">
                                        <EditItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkReconciled" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox runat="server" ID="chkReconciled" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkReconciled" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_btnCreateCheques").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
