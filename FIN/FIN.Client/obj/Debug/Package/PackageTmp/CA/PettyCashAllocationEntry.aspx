﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PettyCashAllocationEntry.aspx.cs" Inherits="FIN.Client.CA.PettyCashAllocationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblBankShortName">
                Withdrawal Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:TextBox runat="server" ID="txtWithdrawalDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtWithdrawalDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtWithdrawalDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblBranchShortName">
                Withdrawal Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:TextBox runat="server" ID="txtWithdrawlTyp" CssClass="validate[]  txtBox" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 527px">
                <asp:DropDownList ID="ddlAccountNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" Width="520px" AutoPostBack="True" OnSelectedIndexChanged="ddlAccountNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                Cheque Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlChequeDtl" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" display: none; width: 195px" id="Div4">
                Cheque Detail
            </div>
            <div class="divtxtBox  LNOrient" style=" display: none; width: 157px">
                <asp:DropDownList ID="ddlChequeHeader" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="7" AutoPostBack="True" OnSelectedIndexChanged="ddlChequeDtl_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                Withdrawal Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtWithdrawalAmount" CssClass="validate[required] RequiredField EntryFont txtBox_N"
                    MaxLength="20" runat="server" TabIndex="8"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtWithdrawalAmount" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                Balance Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:TextBox ID="txtTotAmt" CssClass="EntryFont txtBox_N" MaxLength="20" runat="server"
                    Enabled="false" TabIndex="9"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtTotAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                Total Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBalAmount" CssClass="EntryFont txtBox_N" MaxLength="20" runat="server"
                    TabIndex="8"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtBalAmount" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div6">
                &nbsp;
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px" align="right">
            </div>
        </div>
        <div class="divRowContainer" align="right">
            <table>
                <tr>
                    <td>
                        <asp:ImageButton ID="btnPrintReport" runat="server" ImageUrl="~/Images/print.png"
                            OnClick="btnPrintReport_Click" Style="border: 0px" Visible="false" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnChequePrint" runat="server" ImageUrl="~/Images/printCheque.png"
                            OnClick="imgBtnChequePrint_Click" Style="border: 0px" />
                    </td>
                    <td>
                        <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Visible="false"
                            OnClick="imgBtnPost_Click" Style="border: 0px" />
                        <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                            Visible="false" Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer " align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="400px" DataKeyNames="GL_PCA_DTL_ID,USER_CODE,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Employee">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUserId" Width="100%" AutoPostBack="true" TabIndex="7" runat="server"
                                OnSelectedIndexChanged="ddlUserId_SelectedIndexChanged" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlUserId" Width="100%" AutoPostBack="true" TabIndex="7" runat="server"
                                OnSelectedIndexChanged="ddlUserId_SelectedIndexChanged" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUserId" Width="200px" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Allocation Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpAllocDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                Text='<%#  Eval("GL_PCA_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtenderd8" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpAllocDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpAllocDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtendedr9" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpAllocDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAllocDate" runat="server" Width="100px" Text='<%# Eval("GL_PCA_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Allocated Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAllocAmount" runat="server" Text='<%# Eval("GL_PCA_AMOUNT") %>'
                                OnTextChanged="txtAllocAmount_TextChanged" AutoPostBack="true" CssClass="EntryFont txtBox_N"
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAllocAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAllocAmount" runat="server" CssClass="EntryFont txtBox_N" OnTextChanged="txtAllocAmount_TextChanged"
                                AutoPostBack="true" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAllocAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAllocAmount" runat="server" Text='<%# Eval("GL_PCA_AMOUNT") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Previous Balance Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPreviousBalAmount" runat="server" Text='<%# Eval("previous_bal_amount") %>'
                                Enabled="false" CssClass="EntryFont txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTfextBoxExtender6" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtPreviousBalAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPreviousBalAmount" runat="server" CssClass="EntryFont txtBox_N"
                                Width="100px" Enabled="false"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextsdBoxExtender6" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtPreviousBalAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPreviousBalAmount" runat="server" Text='<%# Eval("previous_bal_amount") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Balance Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBalAmount" runat="server" Text='<%# Eval("GL_PCA_BALANCE_AMOUNT") %>'
                                Enabled="false" CssClass="EntryFont txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FiltereerdTextBoxExtender6" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtBalAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtBalAmount" runat="server" CssClass="EntryFont txtBox_N" Width="100px"
                                Enabled="false"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtBalAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBalAmount" runat="server" Text='<%# Eval("GL_PCA_BALANCE_AMOUNT") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("GL_PCA_REMARKS") %>'
                                CssClass="EntryFont txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="EntryFont txtBox_N" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("GL_PCA_REMARKS") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="9" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="9" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnPosting" runat="server" CssClass="btn" Text="Post" CommandName="btnPop"
                                TabIndex="9" Enabled='<%# Convert.ToBoolean(Eval("POSTED_FLAG")) %>' OnClick="btnPosting_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnPosting" OnClick="btnPosting_Click" runat="server" CssClass="btn"
                                Enabled='<%# Convert.ToBoolean(Eval("POSTED_FLAG")) %>' Text="Post" CommandName="btnPop"
                                TabIndex="9" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnPosting" OnClick="btnPosting_Click" runat="server" CssClass="btn"
                                Text="Post" CommandName="btnPop" TabIndex="9" Enabled="false" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="11" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="12" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_btnCreateCheques").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
