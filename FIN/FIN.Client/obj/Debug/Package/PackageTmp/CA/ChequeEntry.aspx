﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ChequeEntry.aspx.cs" Inherits="FIN.Client.CA.ChequeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankShortName">
                Bank Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBankShortName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                    <asp:ListItem>---Select---</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBranchShortName" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlAccountNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" Width="150px">
                    <asp:ListItem>---Select---</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblNumberofCheques">
                Number of Cheques
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtNumberofCheques" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
               From Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                  <asp:TextBox ID="txtChequeNumberFrom" CssClass="validate[required] txtBox_N"
                    MaxLength="10" runat="server" TabIndex="6"></asp:TextBox>
            </div>
            <div style=" width: 300px" id="lblCreateCheques">
             <asp:ImageButton ID="btnCreateCheques" runat="server" ImageUrl="~/Images/btnCreateCheques.png"
                    OnClick="btnCreateCheques_Click" CssClass="btnProcess" Style="border: 0px" TabIndex="7" />
                <%--<asp:Button ID="btnCreateCheques" runat="server" Text="Create Cheques" CssClass="btnProcess"
                    OnClick="btnCreateCheques_Click" TabIndex="7" />--%>
            </div>
            <div class="divClear_10">
            </div>
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="LOOKUP_ID,CHECK_DTL_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Line No.">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>' CssClass="EntryFont txtBox_N"
                                Width="30px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" CssClass="EntryFont txtBox_N" Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>' Width="95%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cheque Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                              Enabled="false"  CssClass="EntryFont txtBox_N" Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtChequeNumber" Enabled="false" runat="server" CssClass="EntryFont txtBox_N" Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblChequeNumber" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'
                                Width="30px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cheque Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="95%"
                                Text='<%#  Eval("CHECK_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ddtpChequeDate" runat="server" CssClass="EntryFont  txtBox" Width="95%"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="ddtpChequeDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblChequeDate" runat="server" Width="50px" Text='<%# Eval("CHECK_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pay To">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPayTo" runat="server" Text='<%# Eval("CHECK_PAY_TO") %>' CssClass="EntryFont txtBox"
                                Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPayTo" runat="server" CssClass="EntryFont txtBox" Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPayTo" runat="server" Text='<%# Eval("CHECK_PAY_TO") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' CssClass="EntryFont txtBox_N"
                                Width="100PX"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="EntryFont txtBox_N" Width="100PX"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("CHECK_AMT") %>' ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cheque Status">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlChequeStatus"  Width="95px" runat="server" CssClass="ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlChequeStatus"  Width="95px" runat="server" CssClass="ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblChequeStatus" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("CHECK_CANCEL_REMARKS") %>'
                                CssClass="EntryFont txtBox" Width="130px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="EntryFont txtBox" Width="130px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("CHECK_CANCEL_REMARKS") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Crossed Cheque">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkCrossedCheque" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("CHECK_CROSSED_YN")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkCrossedCheque" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCrossedCheque" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("CHECK_CROSSED_YN")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_btnCreateCheques").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
