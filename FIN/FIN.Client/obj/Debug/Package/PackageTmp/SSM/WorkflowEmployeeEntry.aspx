﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WorkflowEmployeeEntry.aspx.cs" Inherits="FIN.Client.SSM.WorkflowEmployeeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divmodule" runat="server">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Module Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlModuleCode" runat="server" TabIndex="1" CssClass="ddlStype"
                    Width="200px" OnSelectedIndexChanged="ddlModuleCode_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Module Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtModuleDescription" CssClass="validate[] txtBox" Enabled="false"
                    Width="200px" MaxLength="100" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblWorkflowCode">
                Workflow Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlWorkFlowCode" runat="server" TabIndex="1" CssClass="ddlStype"
                    Width="200px" OnSelectedIndexChanged="ddlWorkFlowCode_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblWorkflowDescription">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtWorkflowDescription" CssClass="validate[] txtBox" Enabled="false"
                    Width="200px" MaxLength="100" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
           <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="1" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    Width="200px"  AutoPostBack="true">
                </asp:DropDownList>
            </div>
           
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DELETED,PK_ID,ROLE_CODE,EXCEPTION_FLAG,emp_name,EMP_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Employee">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlRoleCode" runat="server" TabIndex="3" CssClass="ddlStype"
                              >
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlRoleCode" runat="server" TabIndex="3" CssClass="ddlStype"
                            >
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRoleCode" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("remarks") %>' CssClass="EntryFont RequiredField txtBox"
                                MaxLength="200" TabIndex="6" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="EntryFont RequiredField txtBox"
                                MaxLength="200" TabIndex="6" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("remarks") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective Start Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="7" MaxLength="10" Width="125px" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Text='<%#  Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="7" MaxLength="10" Width="125px" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Width="100px" Text='<%# Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="8" Width="100px" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="8" Width="100px" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Width="100px" Text='<%# Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Exception Flag">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkExFlag" TabIndex="9" runat="server" Checked='<%# Convert.ToBoolean(Eval("EXCEPTION_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkExFlag" TabIndex="9" runat="server"  />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkExFlag" runat="server" Checked='<%# Convert.ToBoolean(Eval("EXCEPTION_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="10" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="10" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="15" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
