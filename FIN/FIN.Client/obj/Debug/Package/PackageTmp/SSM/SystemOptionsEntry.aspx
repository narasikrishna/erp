﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SystemOptionsEntry.aspx.cs" Inherits="FIN.Client.SSM.SystemOptionsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Module
            </div>
            <div class="divtxtBox LNOrient " style=" width: 150px">
                <asp:DropDownList ID="ddlModule" runat="server" CssClass="validate[] RequiredField  ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 100%" runat="server" id="divAP">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblAlterCode">
                    AP Liability Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPliabilityAcct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="2">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblAlertDescription">
                    AP Advance Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPadvanceacct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="3">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblAlertMessage">
                    AP Retention Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPretentionacct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="4" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblAlertType">
                    AP Discount Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPdiscountacct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="5" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div29">
                    AP Default Cash Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPDefaultCash" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="6" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblLanguageCode">
                    AP Exchange Gain
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPexchangegain" runat="server" CssClass="validate[] RequiredField ddlStype"
                        TabIndex="7" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblScreenCode">
                    AP Exchange Loss
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlAPexchangeloss" runat="server" CssClass="validate[]  ddlStype"
                        TabIndex="8" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblAlertLevelCode">
                    AP Payment Terms
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtAPpaymentterms" CssClass="validate[] RequiredField txtBox_N" runat="server"
                        MaxLength="3" TabIndex="9"></asp:TextBox>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                    AP Start Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtAPstartdate" CssClass="validate[] RequiredField txtBox" runat="server"
                        TabIndex="10"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtAPstartdate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtAPstartdate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                    AP End Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtAPenddate" CssClass="validate[] RequiredField txtBox" runat="server"
                        TabIndex="11"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtAPenddate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtAPenddate" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive">
                    Active
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 180px">
                    <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="12" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 100%" runat="server" id="divAR">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                    AR Debitor Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARdebitoracct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="1" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                    AR Revenue Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARrevenueacct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="2" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                    AR Discount Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARdiscountacct" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="3" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div28">
                    AR Default Cash Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARDefaultCash" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="4" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div7">
                    AR Exchange Gain
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARexchangegain" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="5" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div8">
                    AR Exchange Loss
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARexchangeloss" runat="server" CssClass="validate[] RequiredField ddlStype"
                        TabIndex="6" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
              <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div36">
                    AR Clearance Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlARClearanceAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="4" Width="100%">
                    </asp:DropDownList>
                </div>
               
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                    AR Payment Terms
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtARpaymentterms" CssClass="validate[] RequiredField txtBox" runat="server"
                        MaxLength="3" TabIndex="7"></asp:TextBox>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div12">
                    AR Start Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtARstartdate" CssClass="validate[] RequiredField txtBox" runat="server"
                        TabIndex="8"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtARstartdate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtARstartdate" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div13">
                    AR End Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtARenddate" CssClass="validate[] RequiredField txtBox" runat="server"
                        TabIndex="9"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtARenddate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtARenddate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div11">
                    Active
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 180px">
                    <asp:CheckBox ID="ARchkact" runat="server" Checked="true" TabIndex="10" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 100%" runat="server" id="divHR">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div14">
                    SSDP Employee
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtssdpemplee" CssClass="validate[required] RequiredField txtBox_N" runat="server"
                        MaxLength="10" TabIndex="1"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=","
                        FilterType="Numbers,Custom" TargetControlID="txtssdpemplee" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div15">
                    SSDP Employer
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtssdpemployer" CssClass="validate[required] RequiredField txtBox_N" runat="server"
                        MaxLength="10" TabIndex="2"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=","
                        FilterType="Numbers,Custom" TargetControlID="txtssdpemployer" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div16">
                    Loan Eligible Year
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtlneligibleyr" CssClass="validate[required] RequiredField txtBox_N" runat="server"
                        MaxLength="10" TabIndex="3"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=","
                        FilterType="Numbers,Custom" TargetControlID="txtlneligibleyr" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div17">
                    Loan Re-Eligible Year
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtlnreeligibleyr" CssClass="validate[required] RequiredField txtBox_N"
                        MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars=","
                        FilterType="Numbers,Custom" TargetControlID="txtlnreeligibleyr" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div10">
                    Permission Hours
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtHRpermissionhours" CssClass="validate[required] RequiredField txtBox_N"
                        MaxLength="3" runat="server" TabIndex="5"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=""
                        FilterType="Numbers,Custom" TargetControlID="txtHRpermissionhours" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div18">
                    Loan Deduction Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHRElement" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="6" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div25">
                    Mobile Deduction Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHRMobile" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="7" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div24">
                    Mobile Invoice Deduction Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHRMobileInvoice" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="8" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div26">
                    Medical Deduction Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHRMedical" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="9" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div27">
                    Rent Deduction Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHRRent" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="10" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div21">
                    Basic Pay Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlBasicelementcode" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div22">
                    MGRP Employee Element
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlMGRPEmpElement" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="12" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div23">
                    MGRP Company Element Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlMGRPCompElement" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="13" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div30">
                    Annual Leave
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlHREarnLeave" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="14" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div19">
                    Effective Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtHREfffromdate" CssClass="validate[] RequiredField txtBox" runat="server"
                        TabIndex="15"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender5" TargetControlID="txtHREfffromdate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtHREfffromdate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div20">
                    End Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtHREnddaate" CssClass="validate[] RequiredField txtBox_N" runat="server"
                        TabIndex="16"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender6" TargetControlID="txtHREnddaate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtHREnddaate" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div31">
                    Leave Salary
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlLeavesal" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="17" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div32">
                    Leave Encashment
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlLeavencashment" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="18" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
             <div class="lblBox  LNOrient" style=" width: 150px" id="lbl">
                   Leave Adjustment Deduction
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlleaveadjustcode" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>

                 <div class="lblBox  LNOrient" style=" width: 150px" id="lbl">
                   Leave Adjustment Earnings
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlleaveadjustEarnings" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>

                 <div class="lblBox  LNOrient" style=" width: 150px" id="lbl">
                   Loan Payment
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlLoanPayment" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>

            </div>
            <div class="divClear_10">
            </div>

             <div class="divRowContainer">
             <div class="lblBox  LNOrient" style=" width: 150px" id="lsbl">
                   Leave Indemnity Suspense account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlleaveindmsuspeacc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>

                 <div class="lblBox  LNOrient" style=" width: 150px" id="lqbl">
                   Other Suspense account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlothersuspenseacc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
             <div class="lblBox  LNOrient" style=" width: 150px" id="Div34">
                   Indemnity Expense account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlIndemExpenseAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div>
                 <div class="lblBox  LNOrient" style=" width: 150px" id="Div35">
                   Leave Suspense account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlLeaveSuspenseAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="11" Width="100%">
                    </asp:DropDownList>
                </div></div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
             <div class="lblBox  LNOrient" style=" width: 150px" id="Div33">
                   Leave Plan(in Months)
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                     <asp:TextBox ID="txtLeavePlan" CssClass="validate[required] RequiredField txtBox_N"
                        MaxLength="2" runat="server" TabIndex="19"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" ValidChars=""
                        FilterType="Numbers,Custom" TargetControlID="txtHRpermissionhours" />
                </div>
            </div>
            <div class="divClear_10">
            </div>

            <div class="divClear_10">
                <asp:Label ID="lblssdp" runat="server" Style="color: red;" CssClass="DisplayFont lblBox"
                    Text="*SSDP - Social Security Deduction Percentage"></asp:Label>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 100%" runat="server" id="divFA">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                    FA Asset Cost Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlFAAssetCostAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="1" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                    FA Depreciation Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlFADepriciationAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="2" Width="100%">
                    </asp:DropDownList>
                </div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                    FA Accumulated Depreciation Account
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlFAAccumDepreciaAcc" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="3" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div28">
                    FA Depreciation Run
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlFADepreciationRun" runat="server" CssClass="validate[] RequiredField  ddlStype"
                        TabIndex="4" Width="100%">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
       
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                        TabIndex="21" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="22" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="23" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="24" />
                </td>
            </tr>
        </table>
    </div>
    <div class="divClear_10">
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
