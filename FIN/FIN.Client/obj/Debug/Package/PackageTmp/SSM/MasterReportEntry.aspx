﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MasterReportEntry.aspx.cs" Inherits="FIN.Client.SSM.MasterReportEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Report Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 455px;">
                <asp:TextBox ID="txtReportName" CssClass="validate[required] RequiredField  txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSegmentName">
                Screen Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 450px;">
                <asp:DropDownList ID="ddlscreencode" runat="server" TabIndex="2" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlscreencode_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblscreenname">
                Screen Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtscreenname" CssClass="validate[]  txtBox" runat="server" MaxLength="10"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Table Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 450px;">
                <asp:DropDownList ID="ddltablename" runat="server" TabIndex="1" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddltablename_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
             <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Order By 
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 450px;">
                <asp:DropDownList ID="ddlOBColName" runat="server" TabIndex="2" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive">
                Active
            </div>
            <div class="divChkbox" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="5" />
                <asp:HiddenField ID="hf_OrgId" runat="server" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="LIST_DTL_ID,DISPLAY_NAME,COLUMN_NAME,MASTER_DISP_COLUMN_NAME,WIDTH,COL_TYPE,MASTER_TABLE_NAME,HDR_DTL_LINK,FLAG_MASTER,LIST_DISPLAY,DELETED"
                ShowFooter="false" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="DISPLAY_NAME" HeaderText="Display Name" />
                    <asp:BoundField DataField="COLUMN_NAME" Visible="false" HeaderText="Display Name" />
                    <asp:BoundField DataField="MASTER_DISP_COLUMN_NAME" Visible="false" HeaderText="Display Name" />
                    <asp:BoundField DataField="WIDTH" Visible="false" HeaderText="Display Name" />
                    <asp:BoundField DataField="COL_TYPE" Visible="false" HeaderText="Display Name" />
                    <asp:BoundField DataField="MASTER_TABLE_NAME" Visible="false" HeaderText="Display Name" />
                    <asp:BoundField DataField="HDR_DTL_LINK" Visible="false" HeaderText="Display Name" />
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" Checked='<%# Convert.ToBoolean(Eval("LIST_DISPLAY")) %>'
                                runat="server" AutoPostBack="True" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button AutoPostBack="True" ID="btnSave" runat="server" Text="Save" CssClass="btn"
                            OnClick="btnSave_Click" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="8" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        //        $(document).ready(function () {
        //            fn_changeLng('<%= Session["Sel_Lng"] %>');
        //        });

        //        $(document).ready(function () {

        //        });

        //        $("#FINContent_btnSave").click(function (evt) {
        //            evt.preventDefault();
        //            if ($("#form1").validationEngine('validate') == false)
        //                return false;
        //        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
