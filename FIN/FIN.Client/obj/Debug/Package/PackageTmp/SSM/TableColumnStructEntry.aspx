﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TableColumnStructEntry.aspx.cs" Inherits="FIN.Client.SSM.TableColumnStructEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 130px" id="Div5">
                Screen Code
            </div>
            <div class="divtxtBox  LNOrient" style="width: 630px">
                <asp:DropDownList ID="ddlscreen" runat="server" CssClass="validate[required]  ddlStype" Width="100%"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlscreen_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 130px" id="lblAlertCode">
                Table Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddltable" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddltable_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 90px" id="Div6">
                Organization
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:DropDownList ID="ddlorg" runat="server" CssClass="validate[required]  ddlStype" Width="100%">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 130px" id="lblNoofAlerts">
                Column Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlcolumn" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="100%" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 90px" id="lblActive">
                Master
            </div>
            <div class="divtxtBox  LNOrient" style="width: 30px">
                <asp:CheckBox ID="chkmaster" runat="server" Checked="false" AutoPostBack="True" OnCheckedChanged="chkmaster_CheckedChanged"
                    Text=" " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div id="div_MasterDet" runat="server" visible="false">
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style="width: 130px" id="lblUserCode">
                    Master Table Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 630px">
                    <asp:DropDownList ID="ddlmastab" runat="server" CssClass="validate[required]  ddlStype" Width="100%"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlmastab_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" id="mascol" runat="server">
                <div class="lblBox  LNOrient" style="width: 130px" id="Div1">
                    Master Display column Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 630px">
                    <asp:DropDownList ID="ddlMasterdiscol" runat="server" CssClass="validate[required]  ddlStype"
                        Width="100%" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style="width: 130px" id="Div2">
                    Master Link column Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 630px">
                    <asp:DropDownList ID="ddlmaslinkcol" runat="server" CssClass="validate[required]  ddlStype"
                        Width="100%" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 130px" id="lblRemarks">
                Display Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtdisplayName" CssClass="validate[required] RequiredField txtBox_en"
                    runat="server"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style="width: 90px" id="Div3">
                Width
            </div>
            <div class="divtxtBox  LNOrient" style="width: 180px">
                <asp:TextBox ID="txtwidth" CssClass="validate[required] RequiredField txtBox" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtwidth" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 130px" id="lblDisplaynameol">
                Display Name(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtdisplayNameol" CssClass="txtBox_ol" runat="server"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style="width: 90px" id="Div4">
                Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 180px">
                <asp:DropDownList ID="ddlType" runat="server" 
                    CssClass="ddlStype validate[required]" AutoPostBack="True" 
                    onselectedindexchanged="ddlType_SelectedIndexChanged">
                    <asp:ListItem Value="TEXT">Text</asp:ListItem>
                    <asp:ListItem Value="DATE">Date</asp:ListItem>
                    <asp:ListItem Value="NUMBER">Number</asp:ListItem>
                    <asp:ListItem Value="FORMULA">Formula</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_formula" runat="server" visible="false">
            <div class="lblBox  LNOrient" style="width: 130px" id="Div7">
                Formula
            </div>
            <div class="divtxtBox  LNOrient" style="width: 640px">
                <asp:TextBox ID="txtFormula" CssClass="txtBox RequiredField validate[required]" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <table class="SaveTable" align="right">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnadd" runat="server" ImageUrl="~/Images/btnAdd.png" OnClick="btnadd_Click"
                            Style="border: 0px;" />
                        <%--<asp:Button ID="btnadd" runat="server" Text="Add" CssClass="btn" OnClick="btnadd_Click" />--%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="TAB_COL_STRUCT_ID" ShowFooter="false" EmptyDataText="No Record to Found"
                Width="100%" OnRowEditing="gvData_RowEditing">
                <Columns>
                    <asp:BoundField DataField="TABLE_NAME" HeaderText="Table Name" />
                    <asp:BoundField DataField="COLUMN_NAME" HeaderText="Column Name" />
                    <asp:BoundField DataField="MASTER_TABLE_NAME" HeaderText="Master Table" />
                    <asp:BoundField DataField="MASTER_DISP_COLUMN_NAME" HeaderText="Master Display Column Name" />
                    <asp:BoundField DataField="MASTER_LINK_COLUMN_NAME" HeaderText="Master Link Column Name" />
                    <asp:BoundField DataField="DISPLAY_NAME" HeaderText="Display Name" />
                    <asp:BoundField DataField="DISPLAY_NAME_OL" HeaderText="Display Name(Arabic)" />
                    <asp:BoundField DataField="WIDTH" HeaderText="Width" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="28" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnadd").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
