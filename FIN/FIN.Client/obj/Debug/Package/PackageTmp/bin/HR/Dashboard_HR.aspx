﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="Dashboard_HR.aspx.cs" Inherits="FIN.Client.HR.Dashboard_HR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowDept() {
            $("#divempGrid").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="98%" border="0px">
            <tr>
                <td style="width: 40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divempGraph" style="float: left; width: 100%;">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left; padding-top: 5px">
                                            Employee Details
                                        </div>
                                        <div style="float: right; padding: 10px">
                                            <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowDept()" />
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div style="float: left; width: 100%; overflow: auto">
                                        <asp:Chart ID="chrt_Dept" runat="server" Height="250px" Width="500px"  EnableViewState="true">
                                            <Series>
                                                <asp:Series Name="S_Dept_Count" ChartType="Pie" XValueMember="DEPT_NAME" YValueMembers="DEPT_COUNT"
                                                    IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                                                    Legend="Legend1" LegendText="#VALX ( #VAL )">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                    <div style="float: left; width: 48%; display: none">
                                        <asp:Chart ID="chrt_Desig" runat="server" Height="300px" Width="300px">
                                            <Series>
                                                <asp:Series Name="S_Desig_Count" ChartType="Pie" XValueMember="DESIG_COUNT" YValueMembers="DESIG_COUNT"
                                                    IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divempGrid" style="width: 40%; background-color: ThreeDFace; position: absolute;
                                    z-index: 5000; top: 60px; left: 10px; display: none">
                                    <div id="divEmpDept" runat="server" visible="true">
                                        <asp:GridView ID="gvDeptDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnk_Dept_Name" runat="server" Text='<%# Eval("DEPT_NAME") %>'
                                                            OnClick="lnk_Dept_Name_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DEPT_COUNT" HeaderText="Count">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                    <div id="divEmpDesig" runat="server" visible="false" style="display: none">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <asp:ImageButton ID="imgDesgBack" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                        Width="20px" Height="20px" OnClick="imgBack_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvDesigDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID,DEPT_DESIG_ID"
                                                        AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Designation">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnk_Desig_Name" runat="server" Text='<%# Eval("DESIG_NAME") %>'
                                                                        OnClick="lnk_Desig_Name_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DESIG_COUNT" HeaderText="Count">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divEmpDet" runat="server" visible="false" style="display: none">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                        Width="20px" Height="20px" OnClick="imgEmp_Click_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvEmpDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="EMP_ID"
                                                        AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="EMP_NO" HeaderText="Number"></asp:BoundField>
                                                            <asp:BoundField DataField="EMP_NAME" HeaderText="Name"></asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Employee Recuritment / Resigned
                                    </div>
                                    <div style="width: 150px; float: right; padding-right: 10px">
                                        <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divcompEmpGrap" style="width: 98%">
                                    <asp:Chart ID="chrt_empCount" runat="server" Width="500px" Height="230px" 
                                        EnableViewState="True">
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="s_Total_Emp" IsValueShownAsLabel="True"
                                                XValueMember="row_number" YValueMembers="TOTAL_EMP" Legend="Legend1" LegendText="Total Employee ( #TOTAL{N0} )">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_NEW_JOIN_EMP"
                                                XValueMember="row_number" YValueMembers="NEW_JOIN_EMP" Legend="Legend1" LegendText="New Joined  ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_CONFIRMED_EMP"
                                                XValueMember="row_number" YValueMembers="CONFIRMED_EMP" Legend="Legend1" LegendText="Conifrmed Employee  ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_TERMINATION_EMP"
                                                XValueMember="row_number" YValueMembers="TERMINATION_EMP" Legend="Legend1" LegendText="Termination Employee ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <Area3DStyle Enable3D="True" Inclination="25" Rotation="70"></Area3DStyle>
                                            </asp:ChartArea>
                                            <%-- <asp:ChartArea Name="ChartArea2" >
                                </asp:ChartArea>--%>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divPermintDet" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Permit Renewal Details
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddlPerFinPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlPerFinPeriod_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddlPerFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlPerFinYear_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div id="divpermitGrid" runat="server" visible="true">
                                        <asp:GridView ID="gvPermitGrid" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnk_Permit_Dept_Name" runat="server" Text='<%# Eval("DEPT_NAME") %>'
                                                            OnClick="lnk_Permit_Dept_Name_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="No_Of_Emp" HeaderText="Count">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                    <div id="divempPermitGrid" runat="server" visible="false">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <asp:ImageButton ID="img_emp_Permit_Det" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                        Width="20px" Height="20px" OnClick="img_emp_Permit_Det_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvEmpPermitDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID"
                                                        AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="EMP_NO" HeaderText="Number"></asp:BoundField>
                                                            <asp:BoundField DataField="EMP_NAME" HeaderText="Name"></asp:BoundField>
                                                            <asp:BoundField DataField="EXPIRY_DATE" HeaderText="Expiry Date" DataFormatString="{0:dd/MM/yyyy}">
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="divEmpAge" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Employee Age Bar
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div id="divempAgeGraph">
                                        <asp:Chart ID="chrtEmpAge" runat="server" Height="200px" Width="400px" 
                                            EnableViewState="True">
                                            <Series>
                                                <asp:Series Name="s_EmpAge" ChartType="Funnel" XValueMember="AgeDiff" YValueMembers="NoOfEmp"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Leave Details
                                    </div>
                                    <div style="width: 120px; float: right; padding-right: 10px">
                                        <asp:DropDownList ID="ddlLAPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlLAPeriod_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 120px; float: right; padding-right: 10px">
                                        <asp:DropDownList ID="ddlLAFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlLAFinYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div>
                                    <asp:Chart ID="chrt_LD" runat="server" Width="400px" Height="200px" 
                                        EnableViewState="True">
                                        <Series>
                                            <asp:Series Name="Series1" XValueMember="DEPT_NAME" YValueMembers="No_Of_Emp" IsValueShownAsLabel="true"
                                                ChartType="Bar" YValuesPerPoint="5">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <Area3DStyle Enable3D="false"></Area3DStyle>
                                                <AxisX>
                                                    <MajorGrid Enabled="false" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid Enabled="false" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divCompanyHoliday" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Holidays
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddlHolFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlHolFinYear_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divHolidayGrid">
                                        <asp:GridView ID="gvHolidayDet" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="HOLIDAY_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="HOLIDAY_DESC" HeaderText="Description"></asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="divEmpExpir" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        Employee Distribution in Year
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divEmpExpirGraph">
                                        <asp:Chart ID="chrt_emp_expire" runat="server" Width="400px" Height="380px" 
                                            EnableViewState="True">
                                            <Series>
                                                <asp:Series Name="s_Emp_Expir" ChartType="Doughnut" XValueMember="EMP_EXPI" YValueMembers="No_Of_Emp"
                                                    IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Bottom" Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
