﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTGrossProfitMarginReportParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTGrossProfitMarginReportParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="div1">
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 100px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequisitionType">
                Customer From
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlCustomerFrom" runat="server" TabIndex="2" CssClass="ddlStype"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                Customer To
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlCustomerTo" runat="server" TabIndex="3" CssClass="ddlStype"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                Transaction Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="ddlStype" AutoPostBack="true"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="5" CssClass="ddlStype"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                Invoice Number From
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlInvoiceNoFrom" runat="server" CssClass="ddlStype" AutoPostBack="true"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                Invoice Number To
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlInvoiceNoTo" runat="server" CssClass="ddlStype" AutoPostBack="true"
                    TabIndex="7">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                Transaction From Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtTransFromAmt" runat="server" TabIndex="8" CssClass="txtBox_N"
                    MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtTransFromAmt" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div11">
                Transaction To Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="9" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div12">
                Functional Currency From Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtFunFromAmt" runat="server" TabIndex="10" CssClass=" txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtFunFromAmt" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div8">
                Functional Currency To Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtFunToAmt" runat="server" TabIndex="11" CssClass=" txtBox_N"
                    MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtFunToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div9">
                % of GP From
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtGPFrom" runat="server" TabIndex="12" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtGPFrom" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div10">
                % of GP To
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtGPTo" runat="server" TabIndex="13" CssClass=" txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtGPTo" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="14" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="15" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td class=" LNOrient" style="width: 120px">
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
