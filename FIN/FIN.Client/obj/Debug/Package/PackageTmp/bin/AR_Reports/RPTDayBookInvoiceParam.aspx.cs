﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTDayBookInvoiceParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookInvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookInvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }

                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                if (ddlFromCustomerName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FROMCUSTOMERNAME", ddlFromCustomerName.SelectedItem.Text.ToString());
                    htFilterParameter.Add("FROM_CUSTOMER_ID", ddlFromCustomerName.SelectedValue.ToString());
                }
                if (ddlToCustomerName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TOCUSTOMERNAME", ddlToCustomerName.SelectedItem.Text.ToString());
                    htFilterParameter.Add("TO_CUSTOMER_ID", ddlToCustomerName.SelectedValue.ToString());
                }
                if (ddlFromInvoiceNumber.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromInvNumber", ddlFromInvoiceNumber.SelectedValue.ToString());
                    htFilterParameter.Add("From_InvNum", ddlFromInvoiceNumber.SelectedItem.Text.ToString());
                }

                if (ddlToInvoiceNumber.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToInvNumber", ddlToInvoiceNumber.SelectedValue.ToString());
                    htFilterParameter.Add("To_InvNum", ddlToInvoiceNumber.SelectedItem.Text.ToString());
                }
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount", txtFromAmount.Text);
                    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtToAmount.Text);
                        htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmount.Text));
                    }
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.Invoice_BLL.GetReportDayBookInvoice();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookInvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlFromCustomerName, true);
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlToCustomerName, true);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlFromInvoiceNumber, "", true);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlToInvoiceNumber, "", true);
        }

    }
}