﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using FIN.BLL.CA;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTDayBookRemittanceParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookRemittanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                fillBranchName();
               FillStartDate();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookRemittanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        //private void FillStartDate()
        //{
        //    string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
        //    DataTable dtDate = new DataTable();
        //    if (str_finyear != string.Empty)
        //    {
        //        dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
        //        txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
        //        txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
        //    }
        //}

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                if (ddlBankName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("BANK_ID", ddlBankName.SelectedValue);
                    htFilterParameter.Add("BANK_NAME", ddlBankName.SelectedItem.Text);
                }
                if (ddlBranchName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("BRANCH_ID", ddlBranchName.SelectedValue);
                    htFilterParameter.Add("BRANCH_NAME", ddlBranchName.SelectedItem.Text);
                }
                if (ddlTransacType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TRANC_ID", ddlTransacType.SelectedValue);
                    htFilterParameter.Add("TRANC_NAME", ddlTransacType.SelectedItem.Text);
                }
                if (txtFromAmount.Text != string.Empty)
                {

                    htFilterParameter.Add("FROM_AMOUNT", txtFromAmount.Text);
                }
                if (txtToAmount.Text != string.Empty)
                {

                    htFilterParameter.Add("TO_AMOUNT", txtToAmount.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }

                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.Invoice_BLL.GetDayBookRemittanceReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookRemittanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void FillComboBox()
        {
            Bank_BLL.fn_getBankNam(ref ddlBankName);
            Lookup_BLL.GetLookUpValues(ref ddlTransacType, "PAYMENT_MODE");
            
            //PettyCashAllocation_BLL.GetChequeNo_R(ref ddlChequeNoFrom);
            //PettyCashAllocation_BLL.GetChequeNo_R(ref ddlChequeNoTo);
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillBranchName();
        }
        private void fillBranchName()
        {
            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);
        }

    }
}