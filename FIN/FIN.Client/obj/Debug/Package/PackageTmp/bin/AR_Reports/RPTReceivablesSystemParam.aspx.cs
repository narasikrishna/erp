﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTReceivablesSystemParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                //txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbUNPosted.Items[0].Text = (Prop_File_Data["Posted_P"]);
                    rbUNPosted.Items[0].Selected = true;
                    rbUNPosted.Items[1].Text = (Prop_File_Data["Unposted_P"]);
                    rbUNPosted.Items[2].Text = (Prop_File_Data["Cancelled_P"]);
                    rbUNPosted.Items[3].Text = (Prop_File_Data["All_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtDebitFromAmt.Text, txtDebitToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtCreditFromAmt.Text, txtCreditToAmt.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("GlobalSegmentID", ddlGlobalSegment.SelectedValue);
                    htFilterParameter.Add("GlobalSegmentValue", ddlGlobalSegment.SelectedItem.Text);
                }
                if (ddlFromCustomer.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("From_Customer_Id", ddlFromCustomer.SelectedValue);
                    htFilterParameter.Add("From_Customer_Name", ddlFromCustomer.SelectedItem.Text);
                }
                if (ddlToCustomer.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("To_Customer_Id", ddlToCustomer.SelectedValue);
                    htFilterParameter.Add("To_Customer_Name", ddlToCustomer.SelectedItem.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtDebitFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_From_Amt", txtDebitFromAmt.Text);
                    htFilterParameter.Add("DebitFromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtDebitFromAmt.Text));
                }
                if (txtDebitToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_To_Amt", txtDebitToAmt.Text);
                    htFilterParameter.Add("DebitToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtDebitToAmt.Text));
                }
                if (txtCreditFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_From_Amt", txtCreditFromAmt.Text);
                    htFilterParameter.Add("CreditFromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditFromAmt.Text));
                }
                if (txtCreditToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_To_Amt", txtCreditToAmt.Text);
                    htFilterParameter.Add("CreditToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditToAmt.Text));
                }

                htFilterParameter.Add("POSTEDTYPE", rbUNPosted.SelectedValue.ToString());
                htFilterParameter.Add("POSTEDSTATUS", rbUNPosted.SelectedItem.Text);

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.ARAgingAnalysis_BLL.GetReceivablesSystemReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlFromCustomer);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlToCustomer);

            //FIN.BLL.AR.SuplrStmtAccnt_BLL.GetSupplierName(ref ddlSupplierName);

        }


    }
}
