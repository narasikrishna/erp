﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR_Reports
{
    public partial class RPTApplicantDetailsParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ApplicantDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ApplicantDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtDOBgreater.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtDOBLesser.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlApplicantName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("APP_ID", ddlApplicantName.SelectedItem.Value);
                }
                if (ddlGender.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("APP_Gender", ddlGender.SelectedItem.Value);
                }
                if (ddlNationality.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("Nationality", ddlNationality.SelectedItem.Text);
                }
                if (txtAgeLesser.Text != string.Empty)
                {
                    htFilterParameter.Add("Age_Lesser", txtAgeLesser.Text);
                }

                if (txtAgegreater.Text != string.Empty)
                {
                    htFilterParameter.Add("Age_greater", txtAgegreater.Text);
                }
                if (txtDOBgreater.Text != string.Empty)
                {
                    htFilterParameter.Add("DOB_greater", txtDOBgreater.Text);
                }
                if (txtDOBLesser.Text != string.Empty)
                {
                    htFilterParameter.Add("DOB_Lesser", txtDOBLesser.Text);
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.HR.Payslip_BLL.GetApplicantDetailsReport();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ApplicantDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            FIN.BLL.HR.Applicant_BLL.fn_GetApplicantNM(ref ddlApplicantName,false);
            //ddlApplicantName.Items.RemoveAt(0);
            //ddlApplicantName.Items.Insert(0, new ListItem("All", ""));
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlGender,"APP_GENDER");
            //ddlGender.Items.RemoveAt(0);
            //ddlGender.Items.Insert(0, new ListItem("All", ""));
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlNationality, "NATIONALITY");
            //ddlNationality.Items.RemoveAt(0);
            //ddlNationality.Items.Insert(0, new ListItem("All", ""));

        }
    }
}