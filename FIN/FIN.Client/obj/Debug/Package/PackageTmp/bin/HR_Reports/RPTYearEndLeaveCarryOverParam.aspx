﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTYearEndLeaveCarryOverParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTYearEndLeaveCarryOverParam" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
<div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblFinancialYear">
                    Financial Year
                </div>
                <div class="divtxtBox LNOrient" style="  width: 155px">
                    <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required] RequiredField ddlStype"
                        TabIndex="1">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            
           
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                </div>
                <div class="divtxtBox LNOrient" style="  width: 155px" align="right">
                    <div class="divtxtBox LNOrient" style="  width: 150px">
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>