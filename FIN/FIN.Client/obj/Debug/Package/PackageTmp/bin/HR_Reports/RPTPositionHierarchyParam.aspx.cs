﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR_Reports
{
    public partial class RPTPositionHierarchyParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PositionHierarchyReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
               
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PositionHierarchyReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                if (ddlCategory.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("category_id", ddlCategory.SelectedItem.Value);
                    htFilterParameter.Add("CATEGORY_NAME", ddlCategory.SelectedItem.Text);
                }
                if (ddlJob.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("job_id", ddlJob.SelectedItem.Value);
                    htFilterParameter.Add("JOB_NAME", ddlJob.SelectedItem.Text);
                }
                if (ddlPosition.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("position_id", ddlPosition.SelectedItem.Value);
                    htFilterParameter.Add("POSITION_DESC", ddlPosition.SelectedItem.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                //ReportData = FIN.BLL.HR.Payslip_BLL.GetReportData();
                //dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetAbsentLeaveDetails(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
                ReportData = FIN.BLL.HR.Payslip_BLL.GetPositionHierarchyReport();
                //SubReportData = FIN.BLL.HR.Payslip_BLL.GetEarninReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PositionHierarchyReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            Categories_BLL.fn_getCategory_R(ref ddlCategory, "A");
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Jobs_BLL.fn_getJob4Category_R(ref ddlJob, ddlCategory.SelectedValue);
        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            Position_BLL.fn_GetPositionNameBasedonJob_R(ref ddlPosition, ddlJob.SelectedValue);
        }




    }
}