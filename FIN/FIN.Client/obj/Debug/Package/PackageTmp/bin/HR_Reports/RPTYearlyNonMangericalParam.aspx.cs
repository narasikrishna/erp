﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.HR;

namespace FIN.Client.HR_Reports
{
    public partial class RPTYearlyNonMangericalParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("YearlyNonMangericalParam", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_YearlyNonMangericalParam", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDept);
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployee();
        }

        private void FillEmployee()
        {
            if (ddlDept.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmployee, ddlDept.SelectedValue.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddlDept.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("DEPT_ID", ddlDept.SelectedItem.Value);
                }

                if (ddlEmployee.Text != string.Empty)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmployee.SelectedItem.Value);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalDefinition_DAL.getEmpYrlyNonMgrlDtls());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("YearlyNonMangericalParamReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        //protected void btnSave_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        doc_tablecount = 0;

        //        string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
        //        string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

        //        string HRReportEN = HRReportPath.ToString() + "LeaveBalance_EN.docx";
        //        string HRReportAR = HRReportPath.ToString() + "LeaveBalance_AR.docx";

        //        string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
        //        string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + "HR_LeaveBalanceReport" + formatted + ".docx";

        //        switch (Session[FINSessionConstants.Sel_Lng].ToString())
        //        {
        //            case "AR":
        //                System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //            default:
        //                System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //        }

        //        using (DocX document = DocX.Load(saveHRTemplateAs))
        //        {
        //            document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));

        //            Novacode.Table tbl_LP = document.Tables[doc_tablecount];
        //            DataSet dsData = new DataSet();
        //            dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeaveLedgerData(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
        //            for (int iLoop = 0; iLoop < dsData.Tables[0].Rows.Count; iLoop++)
        //            {
        //                tbl_LP.InsertRow();
        //                if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //                {

        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                }
        //                else if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //                {
        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());                          
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                }
        //            }

        //            document.Tables[doc_tablecount] = tbl_LP;

        //            // Save all changes made to this document.
        //            document.Save();

        //            Session["FileName"] = Path.GetFileName(saveHRTemplateAs);
        //            Session["OnlyFileName"] = Path.GetFileNameWithoutExtension(saveHRTemplateAs);

        //            string linkToDownload = "../TmpWordDocs/" + Session["FileName"];

        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("SaveHREmpListEOS", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }
        //}
    }
}