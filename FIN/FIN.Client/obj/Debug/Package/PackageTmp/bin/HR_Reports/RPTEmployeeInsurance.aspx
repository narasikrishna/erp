﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTEmployeeInsurance.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTEmployeeInsurance" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
<div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="lblDate">
                    Financial Calendar
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlfinyear" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype" 
                      >
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="Div2">
                    Department
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddldept" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
             <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <asp:ImageButton ID="ImageBtn1" runat="server" ImageUrl="../Images/show-report-icon.png"
                        OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>