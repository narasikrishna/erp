﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTTerminationReport.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTTerminationReport" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="colspace" style="float: left;">
                    &nbsp</div>
                <div class="lblBox" style="float: left; width: 120px" id="Div3">
                    To Date
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="colspace_40" style="float: left;">
                    &nbsp</div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                        OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
