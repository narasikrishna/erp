﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.HR;

namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpIncrement : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpIncrement", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_EmpIncrement", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {
            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployeeName,false);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment, false);
           // FIN.BLL.HR.Department_BLL.GetDesignation(ref ddlDesig,false);
            //string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            //DataTable dtDate = new DataTable();
            //if (str_finyear != string.Empty)
            //{
            //    dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
            //    txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
            //    txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            //}


        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Date", txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}
                if (ddlEmployeeName.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmployeeName.SelectedValue);
                }
                if (ddlDepartment.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("DEPT_ID", ddlDepartment.SelectedValue);
                }
                if (ddlDesig.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("DESIGN_ID", ddlDesig.SelectedValue);
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmpIncrement());

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpIncrementReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Department_BLL.GetDesignationName_R(ref ddlDesig, ddlDepartment.SelectedValue);
            FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept_R(ref ddlEmployeeName, ddlDepartment.SelectedValue);
        }

        protected void ddlDesig_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Employee_BLL.GetEmplNameNDeptDesig_R(ref ddlEmployeeName, ddlDepartment.SelectedValue, ddlDesig.SelectedValue);
        }
    }
}