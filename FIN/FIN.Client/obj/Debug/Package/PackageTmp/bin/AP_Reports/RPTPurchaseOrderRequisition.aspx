﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTPurchaseOrderRequisition.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTPurchaseOrderRequisition" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 630px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequisitionNumber">
                Requisition Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlRequisitionNumber" runat="server" TabIndex="1" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Department Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlDepartName" runat="server" TabIndex="2" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Item
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlItem" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                        <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        fn_changeLng('<%= Session["Sel_Lng"] %>');
    });

    $(document).ready(function () {
        $("#form1").validationEngine();
        return fn_SaveValidation();
    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        return fn_SaveValidation();
    });

    function fn_SaveValidation() {
        $("#FINContent_btnSave").click(function (e) {
            //e.preventDefault();
            return $("#form1").validationEngine('validate')
        })
    }

    </script>
</asp:Content>