﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CashFlowTemplateViewParam.aspx.cs" Inherits="FIN.Client.GL_Reports.CashFlowTemplateViewParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_showGraph() {
            fn_JqueryTreeView();


        }
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }



        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#<%=hf_tmpl_id.ClientID %>").val(str_tmpl_id);
            $("#<%=btnTmpl.ClientID %>").click();



        }
        
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
                Template Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlTemplateName" runat="server" TabIndex="1" CssClass="validate[required] RequiredField  ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                Year
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlYear" runat="server" TabIndex="1" CssClass="validate[required] RequiredField  ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 570px">
                <div style="float: right">
                    <asp:ImageButton ID="imgBtnExport" runat="server" ImageUrl="~/Images/print.png" ToolTip="Export Result as PDF"
                        CommandName="PDF" AlternateText="PDF" Visible="false" OnClick="imgBtnExport_Click" />
                    <asp:HiddenField ID="hf_PrintData" runat="server" Value="" />
                </div>
                <div style="float: right">
                    <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" ToolTip="View"
                        AlternateText="View" OnClick="btnView_Click" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="  width: 30%; overflow: scroll;">
                <div id="div_Template" runat="server" class="tree" style="width: 800px">
                </div>
                <asp:HiddenField ID="hf_tmpl_id" runat="server" Value="" />
                <asp:Button ID="btnTmpl" runat="server" Text="Button" OnClick="btnTmpl_Click" Style="display: none" />
            </div>
            <div style="  width: 70%">
                <div class="divRowContainer" align="center" id="divExpandLevel" runat="server" visible="false">
                    <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                        Expand Level
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 50px">
                        <asp:TextBox ID="txtExpandLevel" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"
                            Text="1" MaxLength="1" AutoPostBack="True" OnTextChanged="txtExpandLevel_TextChanged"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtExpandLevel" />
                    </div>
                    <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                        <asp:Button ID="btnExpandAll" runat="server" Text="Expand All" OnClick="btnExpandAll_Click" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="LNOrient" id="div_gvData">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                        CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                        DataKeyNames="B_TYPE,ACCT_GRP_ID,PARENT_GROUP_ID,GROUP_LEVEL,ACCT_GRP_NAME,NEW_ACCT_GRP_NAME,GROUP_NUMBER">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px" Visible="true">
                                <ItemTemplate>
                                    <asp:ImageButton ID="img_Det" ImageUrl="../Images/expand_blue.png" runat="server"
                                        CommandName="PLUS" AlternateText="Details" OnClick="img_Det_Click" />
                                </ItemTemplate>
                                <ItemStyle Width="40px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAcct_grp_Name" runat="server" Text='<%# Eval("NEW_ACCT_GRP_NAME") %>'></asp:Label>
                                    <asp:HiddenField ID="hf_Parent_group_id" runat="server" Value='<%# Eval("PARENT_GROUP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_acct_grp_id" runat="server" Value='<%# Eval("ACCT_GRP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_Group_Level" runat="server" Value='<%# Eval("GROUP_LEVEL") %>'
                                        ClientIDMode="Static" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" ">
                                <ItemTemplate>
                                    Actual
                                    <br />
                                    <br />
                                    Budget
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 1">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP1" runat="server" Text='<%# Eval("PERIOD1_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP1" runat="server" Text='<%# Eval("PERIOD1_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 2">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP2" runat="server" Text=' <%# Eval("PERIOD2_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP2" runat="server" Text='<%# Eval("PERIOD2_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 3">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP3" runat="server" Text='<%# Eval("PERIOD3_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP3" runat="server" Text='<%# Eval("PERIOD3_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 4">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP4" runat="server" Text='<%# Eval("PERIOD4_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP4" runat="server" Text='<%# Eval("PERIOD4_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 5">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP5" runat="server" Text='<%# Eval("PERIOD5_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP5" runat="server" Text='<%# Eval("PERIOD5_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 6">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP6" runat="server" Text='<%# Eval("PERIOD6_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP6" runat="server" Text='<%# Eval("PERIOD6_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 7">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP7" runat="server" Text='<%# Eval("PERIOD7_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP7" runat="server" Text='<%# Eval("PERIOD7_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 8">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP8" runat="server" Text='<%# Eval("PERIOD8_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP8" runat="server" Text='<%# Eval("PERIOD8_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 9">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP9" runat="server" Text='<%# Eval("PERIOD9_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP9" runat="server" Text='<%# Eval("PERIOD9_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 10">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP10" runat="server" Text='<%# Eval("PERIOD10_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP10" runat="server" Text='<%# Eval("PERIOD10_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 11">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP11" runat="server" Text='<%# Eval("PERIOD11_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP11" runat="server" Text='<%# Eval("PERIOD11_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period 12">
                                <ItemTemplate>
                                    <asp:Label ID="lblBP12" runat="server" Text='<%# Eval("PERIOD12_BUDGET_BALANCE") %>'></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAP12" runat="server" Text='<%# Eval("PERIOD12_ACTUAL_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
