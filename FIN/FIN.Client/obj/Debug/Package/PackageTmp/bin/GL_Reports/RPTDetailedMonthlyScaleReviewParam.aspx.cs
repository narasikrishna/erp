﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTDetailedMonthlyScaleReviewParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();
                //FillStartDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                // txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                //txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();


            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmt.Text, txtToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtCreditFromAmt.Text, txtCreditToAmt.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                /*  if (txtFromAccount.Text.ToString().Length > 0)
                  {
                      htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                  }

                  if (txtToAccount.Text.ToString().Length > 0)
                  {
                      htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                  }*/

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Amt", txtFromAmt.Text);
                    htFilterParameter.Add("FromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmt.Text));
                }
                if (txtToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Amt", txtToAmt.Text);
                    htFilterParameter.Add("ToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmt.Text));
                }



                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }


                if (txtCreditFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_From_Amt", txtCreditFromAmt.Text);
                    htFilterParameter.Add("CreditFromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditFromAmt.Text));
                }
                if (txtCreditToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_To_Amt", txtCreditToAmt.Text);
                    htFilterParameter.Add("CreditToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditToAmt.Text));
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                    htFilterParameter.Add("global_segment_id", ddlGlobalSegment.SelectedItem.Value);
                }

                if (ddlAccCode.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("ACCOUNT_CODE", ddlAccCode.SelectedItem.Value);
                }

                if (ddlperiod.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("PERIOD_ID", ddlperiod.SelectedItem.Value);
                    htFilterParameter.Add("PERIOD_NAME", ddlperiod.SelectedItem.Text);
                }


                string str_acctcode = "";
                string str_unpost = "0";
                if (chkUnPosted.Checked)
                {
                    str_unpost = "1";
                }
                if (ddlAccCode.SelectedValue.ToString().Trim().Length == 0)
                {
                    str_acctcode = "ALL";
                }
                else
                {
                    str_acctcode = ddlAccCode.SelectedValue;
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportData = FIN.DAL.GL.AccountMonthlySummary_DAL.GetSP_AccountMonthlySummary(ddlGlobalSegment.SelectedValue.ToString(), ddlperiod.SelectedValue.ToString(), str_acctcode, str_unpost, FIN.BLL.GL.AccountMonthlySummary_BLL.GetReportData());


                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            // Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            ddlAccCode.Items.RemoveAt(0);
            ddlAccCode.Items.Insert(0, new ListItem("All", "All"));
            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);

            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

            ddlGlobalSegment.SelectedIndex = 1;

        }


    }
}