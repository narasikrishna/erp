﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class DayBookDetailsParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
                FillJournalNumber();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbUNPosted.Items[0].Text = (Prop_File_Data["Posted_P"]);
                    rbUNPosted.Items[0].Selected = true;
                    rbUNPosted.Items[1].Text = (Prop_File_Data["Unposted_P"]);
                    rbUNPosted.Items[2].Text = (Prop_File_Data["Both_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlAccountGroup, false);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            ddlAccCode.Items.RemoveAt(0);
            ddlAccCode.Items.Insert(0, new ListItem("All", ""));
            Lookup_BLL.GetLookUpValues(ref ddlJournalType, "JT", true);

            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

            FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddlSegment, "SEG_ID-0000000101", false);
            FIN.BLL.GL.JournalEntry_BLL.getDistinctJERefernce(ref ddlJournalRef, txtFromDate.Text, txtToDate.Text, false);
            Lookup_BLL.GetLookUpValues(ref ddlJournalSource, "SOURCE_FIELD", true);

        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            FillJournalNumber();
        }

        private void FillJournalNumber()
        {
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlFromJournal, txtFromDate.Text, txtToDate.Text);
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlToJournal, txtFromDate.Text, txtToDate.Text);
        }
        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }


        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (ddlAccountGroup.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ACCOUNT_GROUP", ddlAccountGroup.SelectedValue.ToString());
                    htFilterParameter.Add("ACCOUNTGROUP", ddlAccountGroup.SelectedItem.Text.ToString());
                }

                if (ddlAccCode.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ACCOUNT_CODE", ddlAccCode.SelectedValue.ToString());
                }

                if (ddlJournalType.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("JOURNAL_TYPE", ddlJournalType.SelectedValue.ToString());
                    htFilterParameter.Add("JOURNALTYPE", ddlJournalType.SelectedItem.Text.ToString());
                }

                if (ddlJournalRef.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("JOURNAL_REF", ddlJournalRef.SelectedValue.ToString());
                    htFilterParameter.Add("JOURNALREF", ddlJournalRef.SelectedItem.Text.ToString());
                }

                if (ddlSegment.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("SEGMENT", ddlSegment.SelectedValue.ToString());
                    htFilterParameter.Add("SEGMENT_VALUE", ddlSegment.SelectedItem.Text.ToString());
                }
                if (ddlFromJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromJournal", ddlFromJournal.SelectedValue.ToString());

                }

                if (ddlToJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToJournal", ddlToJournal.SelectedValue.ToString());
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }


                string str_Unpost = "";

                if (rbUNPosted.SelectedValue.ToString() == "P")
                {
                    str_Unpost = "1";
                }
                else if (rbUNPosted.SelectedValue.ToString() == "U")
                {
                    str_Unpost = "0";
                }
                else if (rbUNPosted.SelectedValue.ToString() == "B")
                {
                    str_Unpost = "1,0";
                }

                htFilterParameter.Add("UNPOST", str_Unpost);
                htFilterParameter.Add("JournalPosted", rbUNPosted.SelectedItem.Text);
                //if (chkWithZero.Checked)
                //{
                //    htFilterParameter.Add("WITHZERO", "TRUE");
                //}
                //else
                //{
                //    htFilterParameter.Add("WITHZERO", "FALSE");
                //}

                if (txtFromAmount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FROMAMOUNT", txtFromAmount.Text);
                }

                if (txtToAmount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("TOAMOUNT", txtToAmount.Text);
                }
                if (ddlJournalSource.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("Journal_Source", ddlJournalSource.SelectedValue);
                    htFilterParameter.Add("JournalSource", ddlJournalSource.SelectedItem.Text);
                }

                /* if (txtFromAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                 }

                 if (txtToAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                 }*/

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getDayBookDetails());

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        protected void ddlAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountCodes_BLL.getAccountBasedGroup(ref ddlAccCode, ddlAccountGroup.SelectedValue);
        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            getJEReference();
            FillJournalNumber();
        }
        private void getJEReference()
        {
            if (txtFromDate.Text.ToString().Length > 0)
            {
                if (txtToDate.Text.ToString().Length > 0)
                {
                }
            }


        }




    }
}