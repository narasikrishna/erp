﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTRevenueReport.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTRevenueReport" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
                    Cost Centre
                </div>
                <div class="divtxtBox LNOrient" style="  width: 535px">
                    <asp:DropDownList ID="ddlSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Journal Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:DropDownList ID="ddlFromJournal" runat="server" TabIndex="2" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
                  <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px">
                    To Journal Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 185px">
                    <asp:DropDownList ID="ddlToJournal" runat="server" TabIndex="3" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" runat="server" visible="false">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="4" CssClass="txtBox"></asp:TextBox>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px">
                    To Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtToAccount" runat="server" TabIndex="5" CssClass="txtBox"></asp:TextBox>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 535px">
                    <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass="  ddlStype"
                        TabIndex="4">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    To Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 535px">
                    <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass="  ddlStype"
                        TabIndex="5">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divRowContainer" style="display: none">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div7">
                    Account Description
                </div>
                <div class="divtxtBox LNOrient" style="  width: 350px">
                    <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype validate[required] RequiredField "
                        TabIndex="6" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="7" CssClass="txtBox" Width="150px"
                        AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="8" CssClass="txtBox"  Width="150px"
                        AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div8">
                    Debit From Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtDebitFromAmt" runat="server" TabIndex="9" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                        ValidChars="." TargetControlID="txtDebitFromAmt" />
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px" id="Div9">
                    Debit To Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtDebitToAmt" runat="server" TabIndex="10" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                        ValidChars="." TargetControlID="txtDebitToAmt" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div10">
                    Credit From Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtCreditFromAmt" runat="server" TabIndex="11" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                        ValidChars="." TargetControlID="txtCreditFromAmt" />
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px" id="Div11">
                    Credit To Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtCreditToAmt" runat="server" TabIndex="12" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                        ValidChars="." TargetControlID="txtCreditToAmt" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="display: none">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                    Include Zeros
                </div>
                <div class="lblBox LNOrient" style="  width: 100px" id="Div4">
                    <asp:CheckBox ID="chkWithZero" runat="server" Checked="false" Text=" " TabIndex="13" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
                    Include Unposted
                </div>
                <div class="lblBox LNOrient" style="  width: 100px" id="Div6">
                    <asp:CheckBox ID="chkUnPosted" runat="server" Checked="false" Text=" " TabIndex="14" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <%--<div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="lblPeriod">
                    Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField "
                        TabIndex="3">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="Div3">
                    Un Posted
                </div>
                <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
                    <asp:CheckBox ID="chkUnPosted" runat="server" Checked="false" Text=" " />
                </div>
            </div>--%>
            <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="15" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
