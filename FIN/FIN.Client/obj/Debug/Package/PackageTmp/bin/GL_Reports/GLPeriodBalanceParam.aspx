﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLPeriodBalanceParam.aspx.cs" Inherits="FIN.Client.GL_Reports.GLPeriodBalanceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function fn_PrintDiv(div_printDiv_id) {

            //Get the HTML of div

            var divElements = document.getElementById(div_printDiv_id).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;
            console.log(divElements);
            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
                          "<html><head><title></title></head><body>" +
                          divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;




        }

       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="div1">
        <div class="divRowContainer" style="display:none;">
            <div class="lblBox LNOrient" style="  width: 150px;display:none" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
               Include Unposted
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkUnPost" runat="server" Text="" Checked="false" TabIndex="4" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />--%>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png" TabIndex="5"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" Visible="false"
                                onclick="btnPrint_Click"   />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient" id="div_Grid_data">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                DataKeyNames="B_TYPE,ACCT_GRP_ID,PARENT_GROUP_ID,GROUP_LEVEL,ACCT_GRP_NAME,NEW_ACCT_GRP_NAME">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="40px">
                        <ItemTemplate>
                            <asp:ImageButton ID="img_Det" ImageUrl="../Images/expand_blue.png" runat="server" CommandName="PLUS"
                                AlternateText="Details" OnClick="img_Det_Click" />
                        </ItemTemplate>
                        <ItemStyle Width="40px"></ItemStyle>
                    </asp:TemplateField>
                 
                    <asp:TemplateField HeaderText="Name" >
                        <ItemTemplate>
                            <asp:Label ID="lblAcct_grp_Name" runat="server" Text='<%# Eval("NEW_ACCT_GRP_NAME") %>' ></asp:Label>
                        </ItemTemplate>                       
                    </asp:TemplateField>
                     
                    <asp:BoundField DataField="opening_balance" HeaderText="Opening Balance" >
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="transaction_debit" HeaderText="Transaction Debit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="transaction_credit" HeaderText="Transaction Credit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="closing_balance_debit" HeaderText="Closing Balance Debit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="closing_balance_credit" HeaderText="Closing Balance Credit">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="closing_balance" HeaderText="Closing Balance">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">

 <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
