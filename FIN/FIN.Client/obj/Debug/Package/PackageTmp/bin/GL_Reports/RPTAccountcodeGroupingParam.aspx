﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAccountcodeGroupingParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTAccountcodeGroupingParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 870px; border: 4; border-color: Blue;"
        id="divMainContainer">
        <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 28px;
            background-color: rgb(216,216,216);">
            <div align="center" class="lblBox LNOrient" style="  width: 780px; text-align: center;"
                id="lblGlobalSegment">
                Accounting Groups/Accounting Codes
            </div>
            <div class="lblBox LNOrient" align="left" style="  width: 70px" id="Div1">
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" OnClick="btnPrint_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="left">
            <div class="divtxtBox LNOrient" style="  width: 850px">
                <asp:Panel ID="pnlTreeview" runat="server" Width="850px" Height="400" ScrollBars="Auto">
                    <asp:TreeView ID="tvModules" Width="850" CssClass="MenuView" BackColor="Transparent"
                        BorderColor="Transparent" ExpandDepth="0" ShowExpandCollapse="true" EnableClientScript="false"
                        PopulateNodesFromClient="true" runat="server">
                        <ParentNodeStyle Font-Bold="false" BackColor="transparent" Height="20px" ForeColor="#333333" />
                        <SelectedNodeStyle Font-Underline="false" ForeColor="#993300" Font-Bold="True" HorizontalPadding="0px"
                            VerticalPadding="0px" Width="212px" />
                        <NodeStyle ForeColor="#333333" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px"
                            Width="212px" />
                        <RootNodeStyle CssClass="ModuleBackground" BorderWidth="0px" ChildNodesPadding="10px" />
                    </asp:TreeView>
                </asp:Panel>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>