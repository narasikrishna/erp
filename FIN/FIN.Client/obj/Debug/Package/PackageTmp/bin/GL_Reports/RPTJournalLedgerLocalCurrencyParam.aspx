﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTJournalLedgerLocalCurrencyParam.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTJournalLedgerLocalCurrencyParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer" style="display:none">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblAdjPeriod">
                    Global Segment
                </div>
                <div class="divtxtBox LNOrient" style="  width: 480px">
                    <asp:DropDownList ID="ddlSegment" runat="server" CssClass="ddlStype validate[required] RequiredField "
                        TabIndex="1">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" runat="server" visible="false">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="2" CssClass="txtBox"></asp:TextBox>
                </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px">
                    To Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:TextBox ID="txtToAccount" runat="server" TabIndex="3" CssClass="txtBox"></asp:TextBox>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 480px">
                    <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                        TabIndex="2">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    To Account
                </div>
                <div class="divtxtBox LNOrient" style="  width: 480px">
                    <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                        TabIndex="3">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px">
                    From Journal Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlFromJournal" runat="server" TabIndex="6" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px">
                    To Journal Number
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:DropDownList ID="ddlToJournal" runat="server" TabIndex="7" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblAccountingYear">
                    From Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromAmount" CssClass="txtBox_N" runat="server"
                        TabIndex="8"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                        ValidChars=".,-" TargetControlID="txtFromAmount" />
                </div>
                <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 130px" id="Div7">
                    To Amount
                </div>
                <div class="divtxtBox LNOrient" style="  width: 180px">
                    <asp:TextBox ID="txtToAmount" CssClass=" txtBox_N" runat="server" TabIndex="9"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,custom"
                        ValidChars=".,-" TargetControlID="txtToAmount" />
                </div>
                <%-- <div class="lblBox LNOrient" style="  width: 80px;display:none" id="Div5">
                    With Zero
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px;display:none">
                    <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" />
                </div>--%>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
                    Include Unposted
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:CheckBox ID="chkUnPost" runat="server" Checked="false" TabIndex="10" Text=" " />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 590px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="11" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
