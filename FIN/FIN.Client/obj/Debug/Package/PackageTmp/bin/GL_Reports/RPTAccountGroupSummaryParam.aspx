﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAccountGroupSummaryParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTAccountGroupSummaryParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="div1">
        <div class="divRowContainer" style="display:none;">
            <div class="lblBox LNOrient" style="  width: 150px;display:none;" id="lblGlobalSegment">
                 Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype  validate[required] RequiredField ">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
               From Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlFromAccGroup" runat="server" TabIndex="2" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
               To Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlToAccGroup" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="date" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="6" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
               Include Unposted
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkUnPost" runat="server" Text="" Checked="false" TabIndex="7" />
            </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox LNOrient" style="  width: 150px" id="Div6">
               Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 500px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />--%>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png" TabIndex="9"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                        </td>
                        <td>
                            <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn"  />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
