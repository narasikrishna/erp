﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.GL
{
    public partial class RPTBankBalanceSummaryParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenue", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                FillStartDate();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            //FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);

            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);
            // FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvalues(ref ddlsegment,false);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlsegment, false);

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.FROM_DATE, txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.TO_DATE, txtToDate.Text);
                }
                //if (ddlperiod.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                //}
                //if (chkUnPosted.Checked)
                //{
                //    htFilterParameter.Add("UnPost", "1");
                //}
                //else
                //{
                //    htFilterParameter.Add("UnPost", "0");
                //}

                if (ddlperiod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                }
                if (ddlsegment.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.SEGMENT_VALUE_ID, ddlsegment.SelectedValue.ToString());
                }
                if (chkUnPost.Checked)
                {
                    htFilterParameter.Add("UNPOST", "1");
                }
                else
                {
                    htFilterParameter.Add("UNPOST", "0");
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

              //  ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.GetBankBalanceRpt());
                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getBankBalance());

                ReportFormulaParameter = htHeadingParameters;
               
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}