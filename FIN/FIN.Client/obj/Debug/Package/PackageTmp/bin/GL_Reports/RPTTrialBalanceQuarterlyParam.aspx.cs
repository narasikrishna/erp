﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTTrialBalanceQuarterlyParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        string fromDate = string.Empty;
        string toDate = string.Empty;
        string firstHalfYearlyToDate = string.Empty;
        string secondHalfYearlyFromDate = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedValue);
                }
                if (ddlFinancialYear.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CAL_DTL_ID", ddlFinancialYear.SelectedValue);
                    htFilterParameter.Add("CAL_ACCT_YEAR", ddlFinancialYear.SelectedItem.Text);
                }
                if (ddlGroupName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("GROUP_ID", ddlGroupName.SelectedValue);
                }


                string str_Unpost = "0";
                if (chkUnPost.Checked)
                {
                    str_Unpost = "1";
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                htFilterParameter.Add("Quaterly", rbSelectMonths.SelectedItem.Text);
                htFilterParameter.Add("Quaterly_value", rbSelectMonths.SelectedItem.Value);

              
                   /* if (txtFromAccount.Text.ToString().Length > 0)
                    {
                        htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                    }

                    if (txtToAccount.Text.ToString().Length > 0)
                    {
                        htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                    }*/
                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }
               
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                if (rbSelectMonths.SelectedValue == "1")
                {

                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getTrialBalanceQtrly(rbSelectMonths.SelectedItem.Text.ToString(), str_Unpost));
                }
                else if (rbSelectMonths.SelectedValue == "2")
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getTrialBalanceQtrly(rbSelectMonths.SelectedItem.Text.ToString(), str_Unpost));
                }
                else if (rbSelectMonths.SelectedValue == "3")
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getTrialBalanceQtrly(rbSelectMonths.SelectedItem.Text.ToString(), str_Unpost));
                }
                else if (rbSelectMonths.SelectedValue == "4")
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getTrialBalanceQtrly(rbSelectMonths.SelectedItem.Text.ToString(), str_Unpost));
                }

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            ddlGlobalSegment.SelectedIndex = 1;
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlAccountGroup, false);
           // AccountingGroupLinks_BLL.GetGroupName4QuaterlyReport(ref ddlGroupName);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlGroupName,false);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

        }

       
    }
}