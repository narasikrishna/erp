﻿using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;
using FIN.BLL.GL;

namespace FIN.Client.GL_Reports
{
    public partial class RPTAccountDetailsParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenue", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();

                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            //FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);

            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlAccountGroup, false);
            FIN.BLL.GL.AccountCodes_BLL.fn_geAccountStartWith(ref ddlAcctStartWith, false);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);

        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbAccType.Items[0].Text = Prop_File_Data["ALL_P"];
                    rbAccType.Items[1].Text = Prop_File_Data["BalanceSheet_P"];
                    rbAccType.Items[2].Text = Prop_File_Data["Profit_/Loss_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();


            ErrorCollection = CommonUtils.ValidateDateRange(txtFromCreatedDate.Text, txtToCreatedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromModifiedDate.Text, txtToModifiedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (ddlAccountGroup.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ACCOUNT_GROUP", ddlAccountGroup.SelectedValue.ToString());
                }
                if (rbAccType.SelectedValue.ToString() != "A")
                {
                    htFilterParameter.Add("ACCOUNT_TYPE", rbAccType.SelectedValue.ToString());
                }
                if (ddlAcctStartWith.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ACCOUNT_START", ddlAcctStartWith.SelectedValue.ToString());
                }
                /*if (txtFromAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("From_Account", txtFromAccount.Text.ToString());
                }
                if (txtToAccount .Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("To_Account", txtToAccount.Text.ToString());
                }*/

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("From_Account", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("To_Account", ddlToAccNumber.SelectedValue);
                }

                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtFromCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtFromCreatedDate.Text);
                }
                if (txtToCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtToCreatedDate.Text);
                }
                if (txtFromModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtFromModifiedDate.Text);
                }
                if (txtToModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtToModifiedDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getglAcctDet());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}