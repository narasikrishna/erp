﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard_PR.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.DashBoard_PR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .frmStyle
        {
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: 0px solid red;
            top: 0;
            left: 0;
        }
        .frmdiv
        {
            border: 1px solid aqua;
            float:left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divDashboard">
        <div id="divSalDetails" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_SalDetails" class="frmStyle" src="DeptSalary_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divEmpDeductions" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_EmpDeductions" class="frmStyle" src="EmpDeductions_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divALP" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_ALP" class="frmStyle" src="AnnualLeavePaid_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divEmpDetails" class="frmdiv" style="width: 1000px; height: 350px;">
            <iframe id="frmEmpDetails" class="frmStyle" src="EmpDetails_DB.aspx" scrolling="no">
            </iframe>
        </div>
    </div>
    </form>
</body>
</html>
