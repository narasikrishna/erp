﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpDetails_DB.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.EmpDetails_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
        runat="server">
        <div style="float: left; padding-left: 10px; padding-top: 2px">
            Employee Details
        </div>
        <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
            <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
        </div>
        <div style="width: 400px; float: right; padding-right: 10px;">
            <asp:DropDownList ID="ddlEmplName" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddlEmplName_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div id="divEmpDetails">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 80px" id="lblGroupName">
                Number
            </div>
            <div class="lblBox" style="float: left; width: 100px; font-weight: bold">
                <asp:Label ID="lblEmpNo" runat="server" Text=""></asp:Label>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 80px" id="Div2">
                Name
            </div>
            <div class="lblBox" style="float: left; width: 350px; font-weight: bold">
                <asp:Label ID="lblNmae" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="divClear_10" style="width: 50%">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 80px" id="Div3">
                Department
            </div>
            <div class="lblBox" style="float: left; width: 250px; font-weight: bold">
                <asp:Label ID="lblDepartmet" runat="server" Text=""></asp:Label>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 80px" id="Div5">
                Designation
            </div>
            <div class="lblBox" style="float: left; width: 250px; font-weight: bold">
                <asp:Label ID="lblDesignation" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="divClear_10" style="width: 50%">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 80px" id="Div6">
                DOJ
            </div>
            <div class="lblBox" style="float: left; width: 80px; font-weight: bold">
                <asp:Label ID="lblDOJ" runat="server" Text=""></asp:Label>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 80px" id="Div7">
                DOC
            </div>
            <div class="lblBox" style="float: left; width: 80px; font-weight: bold">
                <asp:Label ID="lblDOC" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="divClear_10" style="width: 50%">
        </div>
        <div style="width: 150px; float: right; padding-right: 270px;">
            <asp:DropDownList ID="ddlFinYr" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddlFinYr_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        
        <div class="divClear_10" style="width: 50%">
            <asp:HiddenField ID="hf_deptid" runat="server" />
        </div>
        
        <asp:Chart ID="chrtEmpDetails" runat="server" Width="950px" Height="210px" EnableViewState="true">
            <Series>
                <asp:Series Name="sEmpDetails" XValueMember="pay_period_desc" YValueMembers="net_sal"
                    IsValueShownAsLabel="True">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                    <Area3DStyle Enable3D="false" Inclination="10" Rotation="20"></Area3DStyle>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
     <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="pay_period_desc" HeaderText="Payroll Period"></asp:BoundField>
                <asp:BoundField DataField="net_sal" HeaderText="Net Salary">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
