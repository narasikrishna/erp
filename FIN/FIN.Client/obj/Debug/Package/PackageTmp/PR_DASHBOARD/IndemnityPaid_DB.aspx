﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="IndemnityPaid_DB.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.IndemnityPaid_DB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="GridHeader" style="float: left; width: 100%; height: 25px">
        <div style="float: left; padding-left: 10px; padding-top: 2px">
            Indemnity Paid
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_IP_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_IP_Period_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_IP_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_IP_Year_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divIndemLeaveGraph">
        <asp:Chart ID="chrtIndemnityLeave" runat="server" Width="500px" Height="300px" EnableViewState="true" >
            <Series>
                <asp:Series Name="sIndemnityLeave" ChartType="Doughnut" XValueMember="DEPT_NAME" YValueMembers="TOT_AMT"
                    IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL" CustomProperties="PieLabelStyle=Outside">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
            <Legends>
                <asp:Legend Docking="Bottom" Name="Legend1">
                </asp:Legend>
            </Legends>
        </asp:Chart>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
