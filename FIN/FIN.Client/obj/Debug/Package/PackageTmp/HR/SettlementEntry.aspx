﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SettlementEntry.aspx.cs" Inherits="FIN.Client.HR.SettlementEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div10">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 740px">
                <asp:DropDownList ID="ddlDepartment" TabIndex="1" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                    AutoPostBack="true" CssClass=" validate[required]  RequiredField EntryFont ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="Div11">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 740px">
                <asp:DropDownList ID="ddlEmployee" TabIndex="2" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFinancialYear">
                Settlement Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" TabIndex="3" ID="txtSettlementDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtSettlementDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtSettlementDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 130px" id="Div1">
                Request Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSettlementType" TabIndex="4" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlSettlementType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblRequest">
                Request
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSettlementId" TabIndex="5" runat="server" CssClass="validate[required] RequiredField EntryFont ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlSettlementId_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div12">
                Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAmount" TabIndex="6" MaxLength="13" runat="server" Enabled="false"
                    CssClass="RequiredField txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtAmount" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 130px" id="Div5">
                Balance Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBalanceAmount" TabIndex="6" Width="150px" MaxLength="13" runat="server"
                    Enabled="false" CssClass="RequiredField txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtBalanceAmount" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblSettlementType">
                Settlement Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlType" TabIndex="7" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Settlement Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtSettlementAmount" TabIndex="8" MaxLength="13" runat="server"
                    CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtSettlementAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 750px">
                <asp:TextBox ID="txtRemarks" TabIndex="9"  MaxLength="50" runat="server"
                    Height="60px" TextMode="MultiLine" CssClass=" txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                Additional Detail1
            </div>
            <div class="divtxtBox  LNOrient" style="width: 750px">
                <asp:TextBox ID="txtAdditionalDetails1" TabIndex="10" MaxLength="500" runat="server"
                    CssClass=" txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                Additional Detail2
            </div>
            <div class="divtxtBox  LNOrient" style="width: 750px">
                <asp:TextBox ID="txtAdditionalDetails2" TabIndex="11" MaxLength="500" runat="server"
                    CssClass=" txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div8">
                Additional Detail3
            </div>
            <div class="divtxtBox  LNOrient" style="width: 750px">
                <asp:TextBox ID="txtAdditionalDetails3" TabIndex="12" MaxLength="500" runat="server"
                    CssClass=" txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
