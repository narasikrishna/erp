﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GradeLevelsEntry.aspx.cs" Inherits="FIN.Client.HR.GradeLevelsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">

            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="800px" DataKeyNames="GRADE_LEVEL_ID,CATEGORY_ID,JOB_ID,POSITION_ID,GRADE_ID,PAY_ELEMENT_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                <asp:TemplateField HeaderText="Category">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="ddlStype"   TabIndex="1"
                                Width="150px" AutoPostBack="True" 
                                onselectedindexchanged="ddlCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlCategory_SelectedIndexChanged" CssClass="ddlStype" Width="150px"
                                TabIndex="1">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("CATEGORY_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Job">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlJob" runat="server" CssClass="ddlStype" Width="150px"   TabIndex="2"
                                AutoPostBack="True" onselectedindexchanged="ddlJob_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlJob" runat="server"  AutoPostBack="True" onselectedindexchanged="ddlJob_SelectedIndexChanged" CssClass="ddlStype" Width="150px"
                                TabIndex="2">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblJob" runat="server" Text='<%# Eval("JOB_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Position">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlPosition" runat="server" CssClass="ddlStype"    TabIndex="3"
                                Width="150px" AutoPostBack="True" 
                                onselectedindexchanged="ddlPosition_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlPosition" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlPosition_SelectedIndexChanged" CssClass="ddlStype" Width="150px"
                                TabIndex="3">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPosition" runat="server" Text='<%# Eval("POSITION_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGrade" runat="server" CssClass="ddlStype" Width="150px"   TabIndex="4">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGrade" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="4">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGrade" runat="server" Text='<%# Eval("GRADE_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlElement" runat="server" CssClass="ddlStype" Width="150px"  TabIndex="5">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlElement" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="5">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElement" runat="server" Text='<%# Eval("PAY_ELEMENT") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Low Limit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtlowlimit" TabIndex="6" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("LOW_LIMIT") %>' AutoPostBack="false" ontextchanged="txtHighlimit_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtlowlimit" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtlowlimit" TabIndex="6" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("LOW_LIMIT") %>' AutoPostBack="false" ontextchanged="txtHighlimit_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtlowlimit" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbllowlimit" Width="130px" runat="server" Text='<%# Eval("LOW_LIMIT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="High Limit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtHighlimit" TabIndex="7" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("HIGH_LIMIT") %>' AutoPostBack="false" ontextchanged="txtHighlimit_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtHighlimit" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtHighlimit" TabIndex="7" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("HIGH_LIMIT") %>' 
                                AutoPostBack="false" ontextchanged="txtHighlimit_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtHighlimit" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblHighlimit" Width="130px" runat="server" Text='<%# Eval("HIGH_LIMIT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Increment Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtincrementamt" TabIndex="8" Width="130px" MaxLength="13" runat="server" 
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("INCREMENT_AMT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtincrementamt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtincrementamt" TabIndex="8" Width="130px" MaxLength="13" runat="server" 
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("INCREMENT_AMT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtincrementamt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblincrementamt" Width="130px" runat="server" Text='<%# Eval("INCREMENT_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Average Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAvgamt" TabIndex="9" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("AVERAGE_AMT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAvgamt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAvgamt" TabIndex="9" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox_N" Text='<%# Eval("AVERAGE_AMT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAvgamt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAvgamt" Width="130px" runat="server" Text='<%# Eval("AVERAGE_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="10" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="130px" Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="10" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                               Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" Width="130px" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="11" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Width="130px" Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="11" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                               Width="130px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Width="130px" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="12" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="12" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="13" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="14" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="15" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="16" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
       <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
