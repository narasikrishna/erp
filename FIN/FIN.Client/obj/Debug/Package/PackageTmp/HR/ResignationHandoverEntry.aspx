﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ResignationHandoverEntry.aspx.cs" Inherits="FIN.Client.HR.ResignationHandoverEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlRequestNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlRequestNumber_SelectedIndexChanged1"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRequestDate" CssClass="txtBox" runat="server" TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtRequestDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartmentName">
                Department Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 527px">
                <asp:TextBox ID="txtDepartmentName" CssClass="txtBox" runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 527px">
                <asp:TextBox ID="txtEmployeeName" CssClass="txtBox" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
                Reason
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtReason" CssClass="txtBox" runat="server" TabIndex="5" Height="50px"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRemarks" CssClass="txtBox" runat="server" TabIndex="6" Height="50px"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblHandoverDate">
                Handover Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtHandoverDate" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtHandoverDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtHandoverDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblHandoverStatus">
                Handover Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlHOStatus" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 196px" id="Div1">
                Handover To</div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlEmployeeName" runat="server" CssClass="validate[required] RequiredField  ddlStype"
                    TabIndex="9">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <asp:HiddenField runat="server" ID="HfEmpid" />
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="HO_DTL_ID,ASSET_DTL_ID,EMP_ID,HO_TYPE,HO_REMARKS,DELETED" OnRowCommand="gvData_RowCommand"
                OnRowDataBound="gvData_RowDataBound" Width="450px">
                <Columns>
                    <%-- <asp:TemplateField HeaderText="Sl No." ControlStyle-Width= "50px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtlineno" TabIndex="9" MaxLength="10" runat="server" CssClass="txtBox_N"
                                Text='<%# Eval("HO_LINE_NUM") %>' Width="50px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtlineno" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtlineno" TabIndex="9" MaxLength="10" runat="server" CssClass="txtBox_N"
                                Width="50px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtlineno" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;" Text='<%# Eval("HO_LINE_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:BoundField DataField="HO_TYPE" HeaderText="Type" />
                    <asp:BoundField DataField="ASSET" HeaderText="Asset" />
                    <asp:TemplateField HeaderText="Handover To">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlHandoverto" Width="100%" runat="server" CssClass="RequiredField ddlStype">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:TextBox ID="txtremarks" TabIndex="3" Width="300px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("HO_REMARKS") %>'></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkAct" TabIndex="8" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Type" ControlStyle-Width = "100px" ControlStyle-Height = "50px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txttype" TabIndex="10" MaxLength="50" runat="server" CssClass="txtBox" 
                                Text='<%# Eval("HO_TYPE") %>' Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txttype" TabIndex="10" MaxLength="50" runat="server" CssClass="txtBox" TextMode = "MultiLine"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" Enabled="false" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;" Text='<%# Eval("HO_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <%-- <asp:TemplateField HeaderText="Remarks" ControlStyle-Width= "300px" ControlStyle-Height = "50px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtremarks" TabIndex="11" MaxLength="50" runat="server" CssClass="txtBox"
                                Text='<%# Eval("HO_REMARKS") %>' Width="300px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtremarks" TabIndex="11" MaxLength="50" runat="server" CssClass="txtBox" TextMode = "MultiLine"
                                Width="300px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblremarks" Enabled="false" Style="word-wrap: break-word; white-space: pre-wrap;" runat="server" Text='<%# Eval("HO_REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <%-- <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="13" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="14" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="13" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="14" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="12" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
