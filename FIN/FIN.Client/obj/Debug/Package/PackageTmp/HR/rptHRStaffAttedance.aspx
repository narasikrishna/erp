﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="rptHRStaffAttedance.aspx.cs" Inherits="FIN.Client.HR.rptHRStaffAttedance" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%" id="div1">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox" style="float: left; width: 605px">
                <asp:DropDownList ID="ddlDepartment" runat="server" TabIndex="1" 
                    CssClass=" ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left">
                
            </div>
            <div class="lblBox" style="float: left; width: 130px; display:none" id="lblLeaveType">
                LeaveType
            </div>
            <div class="divtxtBox" style="float: left; width: 230px;display:none">
                <asp:DropDownList ID="ddlLeaveType" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <%--<div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="lblMonth">
                Month
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:DropDownList ID="ddlMonth" runat="server" TabIndex="1" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left">
            </div>
            <div class="lblBox" style="float: left; width: 120px" id="lblYear">
                Year
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:DropDownList ID="ddlYear" runat="server" TabIndex="1" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace" style="float: left; ">
            </div>
            <div class="lblBox" style="float: left; width: 130px" id="Div4">
                TO Date
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="lblEmpName">
                Employee Name
            </div>
            <div class="divtxtBox" style="float: left; width: 605px">
                <asp:DropDownList ID="ddlEmployeeName" runat="server" TabIndex="1" CssClass="ddlStype">
                    <asp:ListItem>---Select---</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox" style="float: left; width: 120px" id="lblFirstName">
                First Name
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:TextBox ID="txtFirstname" runat="server" TabIndex="2" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left">
            </div>
            <div class="lblBox" style="float: left; width: 130px" id="lblLastName">
                Last Name
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:TextBox ID="txtLastName" runat="server" TabIndex="2" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="lblShowOff">
                Show only who is off
            </div>
            <div class="divtxtBox" style="float: left; width: 230px">
                <asp:CheckBox ID="chkShowOff" runat="server" />
            </div>
            <div class="lblBox" style="float: left; width: 120px" id="Div2">
                
            </div>
            <div class="divtxtBox" style="float: left; width: 230px" align="right">                
                 <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png" OnClick="btnSave_Click"  Width="35px" Height="25px" style="border:0px" />
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hf_WeekOffDays" runat="server" />
        </div>
        <div class="divRowContainer" id="div_AttCol" runat="server" style="width:100%">
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
