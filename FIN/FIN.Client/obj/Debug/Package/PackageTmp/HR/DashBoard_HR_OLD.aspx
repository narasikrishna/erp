﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DashBoard_HR_OLD.aspx.cs" Inherits="FIN.Client.HR.DashBoard_HR" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div style="width: 1100px">
        <div style="width: 550px; float:left">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="lblDeptName">
                    Department Name
                </div>
                <div class="divtxtBox" style="float: left; width: 250px">
                    <asp:DropDownList ID="ddlDeptName" runat="server" TabIndex="1" Width="150px" CssClass=" ddlStype"
                        DataTextField="DEPT_NAME" DataValueField="DEPT_ID">
                    </asp:DropDownList>
                </div>
            
                <div class="lblBox" style="float: left; width: 200px" id="lblDesig">
                    Designation
                </div>
                <div class="divtxtBox" style="float: left; width: 250px">
                    <asp:DropDownList ID="ddlDesignation" runat="server" TabIndex="1" Width="150px" CssClass=" ddlStype"
                        DataTextField="desig_name" DataValueField="PARENT_DEPT_DESIG_ID">
                    </asp:DropDownList>
                </div>
            
                <div class="lblBox" style="float: left; width: 200px" id="lblBloodGroup">
                    Blood Group
                </div>
                <div class="divtxtBox" style="float: left; width: 250px">
                    <asp:DropDownList ID="ddlBloodGroup" runat="server" TabIndex="1" Width="150px" CssClass=" ddlStype"
                        DataTextField="LOOKUP_NAME" DataValueField="LOOKUP_ID">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divRowContainer" style="float: right; width: 190px">
                <asp:Button ID="btnGenData" runat="server" Text="Generate Data" OnClick="btnGenerateData_Click"
                    CssClass="btn" TabIndex="3" />
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <asp:GridView ID="GridViewHR" runat="server" AutoGenerateColumns="False" AllowPaging="True" Width="70%" PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="dept_name" HeaderText="Dept Name" ReadOnly="True" SortExpression="dept_name" />
                        <asp:BoundField DataField="desig_name" HeaderText="Desig Name" SortExpression="desig_name" />
                        <asp:BoundField DataField="emp_name" HeaderText="Emp Name" SortExpression="emp_name" />
                        <asp:BoundField DataField="emp_blood_group" HeaderText="Emp Blood Group" SortExpression="emp_blood_group" />
                    </Columns>
                </asp:GridView>
            </div>

            </div>

            <div style="width: 550px; float:left">
            <div class="divClear_10">
            </div>
             <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="lblFromDate">
                    From Date
                </div>
                <div class="divtxtBox" style="float: left; width: 250px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        Width="150px"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="lblToDate">
                    To Date
                </div>
                <div class="divtxtBox" style="float: left; width: 250px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        Width="150px"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="float: right; width: 290px">
                <asp:Button ID="btnGenChart" runat="server" Text="Generate Chart" OnClick="btnGenerateChart_Click"
                    CssClass="btn" TabIndex="3" />
            </div>
            <div class="divClear_10">
            </div>
            <asp:Chart ID="ChartHR" runat="server" Width="500" Height="450">
                <Series>
                    <asp:Series Name="HRInterviewAttendedSeries">
                    </asp:Series>
                    <asp:Series Name="HRInterviewSelectedSeries">
                    </asp:Series>
                    <asp:Series Name="HRResignationSeries">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend Name="Default">
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaHR">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>

            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
