﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeContractAssignmentEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeContractAssignmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblEmployee">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 495px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="1" AutoPostBack="True"
                    Width="487px" CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                    <%--onselectedindexchanged="ddlEmployee_SelectedIndexChanged"--%>
                </asp:DropDownList>
            </div>
            <%--  <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 160px" id="lblName">
                Name
            </div>
             <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtName" MaxLength="4" 
                    runat="server"  TabIndex="2" CssClass="txtBox"></asp:TextBox>
               </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer" >
            <div class="lblBox" style="float: left; width: 160px" id="Div1">
                Contract Type
            </div>

            <div class="divtxtBox" style="float: left; width: 495px">
                <asp:DropDownList ID="ddlcontracttyp" CssClass="validate[] RequiredField ddlStype"
                    runat="server" TabIndex="3" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblContractName">
                Contract Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 495px">
                <asp:DropDownList ID="ddlContractName" Width="487px" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 160px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtToDate" CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]    txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 487px">
                <asp:TextBox ID="txtRemarks" MaxLength="250" Height="50px" Width="487px" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:CheckBox ID="chKActive" runat="server" Checked="True" TabIndex="7" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="8" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
