﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingAgreementEntry.aspx.cs" Inherits="FIN.Client.HR.TrainingAgreementEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="AGR_HDR_ID,AGR_LOW_LIMIT,AGR_HIGH_LIMIT,AGR_EFFECTIVE_FROM_DT,AGR_EFFECTIVE_TO_DT"
                ShowFooter="true" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" EmptyDataText="No Record to Found"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Low Limit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLowlimit" MaxLength="10" runat="server" Text='<%# Eval("AGR_LOW_LIMIT") %>'
                                TabIndex="10" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoexExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtLowlimit" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLowlimit" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="1" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxEextender23"
                                    FilterType="Numbers" runat="server" TargetControlID="txtLowlimit" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLowlimit" runat="server" Text='<%# Eval("AGR_LOW_LIMIT") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="High Limit">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtHighlimit" MaxLength="10" runat="server" Text='<%# Eval("AGR_HIGH_LIMIT") %>'
                                TabIndex="11" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoexExtender28" runat="server" FilterType="Numbers"
                                TargetControlID="txtHighlimit" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtHighlimit" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="2" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxEextender29"
                                    FilterType="Numbers" runat="server" TargetControlID="txtHighlimit" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblHighlimit" runat="server" Text='<%# Eval("AGR_HIGH_LIMIT") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpFromdt" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox" TabIndex="12"
                                Width="130px" Text='<%#  Eval("AGR_EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromdt">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpFromdt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpFromdt" TabIndex="3" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromdt">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpFromdt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("AGR_EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpToDt" MaxLength="10" runat="server" CssClass="EntryFont  txtBox" TabIndex="13"
                                Width="130px" Text='<%#  Eval("AGR_EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpToDt">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpToDt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpToDt" TabIndex="4" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender11" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpToDt">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpToDt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("AGR_EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' TabIndex="14" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="5" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" CommandName="btnPop"  Enabled="false"
                                OnClick="btnDetails_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" CommandName="btnPop" TabIndex="15"
                                OnClick="btnDetails_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" CommandName="btnPop"
                                TabIndex="6" OnClick="btnDetails_Click" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="8" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="9" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="16" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="16" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="7" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
        <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField ID="btnDetails" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="btnDetails"
                PopupControlID="Panel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="300px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="LNOrient">
                            <td colspan="4">
                                <asp:GridView ID="gvAGRDtl" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                    DataKeyNames="AGR_DTL_ID" OnRowCancelingEdit="gvAGRDtl_RowCancelingEdit" OnRowCommand="gvAGRDtl_RowCommand"
                                    OnRowCreated="gvAGRDtl_RowCreated" OnRowDataBound="gvAGRDtl_RowDataBound" OnRowDeleting="gvAGRDtl_RowDeleting"
                                    OnRowEditing="gvAGRDtl_RowEditing" OnRowUpdating="gvAGRDtl_RowUpdating" ShowFooter="true"
                                    EmptyDataText="No Record to Found" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Clause">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtClause" MaxLength="250" runat="server" Text='<%# Eval("AGR_CLAUSE") %>'
                                                    TabIndex="11" CssClass="EntryFont txtBox" Width="100px"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtClause" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                                    TabIndex="11" Width="200px"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblClause" runat="server" Text='<%# Eval("AGR_CLAUSE") %>' Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkact2" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkact2" TabIndex="6" runat="server" Checked="true" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkact2" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                     ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                     ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                     ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                     ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                  ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Close"
                                    Width="60px" OnClick="btnCLosePopUp_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
