﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="Vacancies.aspx.cs" Inherits="FIN.Client.HR.Vacancies" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblVacancyName">
                Vacancy Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtVacancyName" MaxLength="100" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDepartment" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDesignation" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3" AutoPostBack="True">
                    <asp:ListItem Text="Select Department" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblNoOfPeoples">
                No Of People
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtNoOfPeoples" MaxLength="4" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers"
                    TargetControlID="txtNoOfPeoples" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblExperienceYears">
                Experience Years
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtExperienceYears" MaxLength="2" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers,custom"
                    ValidChars="" TargetControlID="txtExperienceYears" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblMinExp">
                Vacancy Minimum Experience
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtVacMinExp" MaxLength="2" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,custom"
                    ValidChars="" TargetControlID="txtVacMinExp" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblVacancyMaxExp">
                Vacancy Maximum Experience
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtVacancyMaxExp" MaxLength="2" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="7"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                    ValidChars="" TargetControlID="txtVacancyMaxExp" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRelocate">
                Relocate
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkRelocate" runat="server" Checked="True" TabIndex="8" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblType">
                Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlType" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="9" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblCommunicationlevel">
                Communication Level
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtCommunicationlevel" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="10"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblInternal/External">
                Internal/External
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlInternalExternal" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="11" AutoPostBack="True" OnSelectedIndexChanged="ddlInternalExternal_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="Agency1">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAgencyName">
                Agency Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtAgencyName" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="12"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="Agency2">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAgencyContact">
                Agency Contact
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAgencyContact" MaxLength="50" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="13"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblAgencyPerson">
                Agency Person
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAgencyPerson" MaxLength="100" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="14"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequiredtechnology">
                Required Technology
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:TextBox ID="txtRequiredTechnology" MaxLength="100" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="15"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px; display: none" id="lblBudget">
                Budget
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px; display: none">
                <asp:TextBox ID="txtBudget" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="16"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Period
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlPeriod" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="16">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div4">
                Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlStatus" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="17">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Budget From
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBudgetFrom" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="16"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                    ValidChars=",." TargetControlID="txtBudgetFrom" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Budget To
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBudgetTo" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="16"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                    ValidChars=",." TargetControlID="txtBudgetTo" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEnabledFlag">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="ChkEnabledFlag" runat="server" Checked="True" TabIndex="18" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div5">
                Certificate Req
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="ChkCertificateReq" runat="server" Checked="True" TabIndex="17" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="19"
                        OnClick="btnSave_Click" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="20" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="21"
                        OnClick="btnCancel_Click" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="22" />
                </td>
            </tr>
        </table>
    </div>
    <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
