﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RequestAssignmentEntry.aspx.cs" Inherits="FIN.Client.HR.RequestAssignmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequest">
                Request
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:DropDownList ID="ddlRequest" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="7" AutoPostBack="True" OnSelectedIndexChanged="ddlRequest_SelectedIndexChanged">
                   <%-- <asp:ListItem Text="---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequestDescription">
                Request Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtRequestDescription" MaxLength="250" Height="50px" CssClass="txtBox"
                    runat="server" TabIndex="8" ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequestedEmployee">
                Requested Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtRequestedEmployee" MaxLength="250" CssClass="txtBox" runat="server"
                    TabIndex="9" ReadOnly="True"></asp:TextBox><asp:HiddenField ID="hf_ReqEmpId" runat="server"
                        Value="" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:DropDownList ID="ddlDepartment" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="10" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:DropDownList ID="ddlDesignation" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="11" AutoPostBack="True" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged">
                    <%--<asp:ListItem Text=" ---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:DropDownList ID="ddlEmployeeName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="12" AutoPostBack="True">
                    <%--<asp:ListItem Text="---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAssignmentDate">
                Assignment Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtAssignedDate" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="13"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender1"
                        runat="server" Format="dd/MM/yyyy" TargetControlID="txtAssignedDate">
                    </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtAssignedDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblCompletionDate">
                Completion Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtCompletionDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField txtBox "
                    runat="server" TabIndex="14"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender2"
                        runat="server" Format="dd/MM/yyyy" TargetControlID="txtCompletionDate">
                    </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtCompletionDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblHRRemarks">
                HR Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtHRRemarks" MaxLength="250" CssClass="txtBox" runat="server" TabIndex="15"
                    Height="50px" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:CheckBox ID="ChkActive" runat="server" Checked="True" TabIndex="16" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div id="btnReqView" runat="server" class="lblBox" style="float: left; width: 312px">
                <asp:Button ID="btnReqV" runat="server" Text="View Request" 
                    onclick="btnReqV_Click" Visible = "false" />
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="17"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="19"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="20" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
