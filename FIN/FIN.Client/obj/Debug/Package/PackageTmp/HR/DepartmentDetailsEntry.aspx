﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DepartmentDetailsEntry.aspx.cs" Inherits="FIN.Client.HR.DepartmentDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartmentName">
                Department Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlDeptname" CssClass="validate[required]  RequiredField ddlStype"
                    runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlDeptname_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px; display: none" id="lblDepartmentType">
                Department Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px; display: none">
                <asp:TextBox ID="txtDepartmentType" CssClass="validate[required] txtBox" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <%--<div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblFromDate">
                Start Date
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]    txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DEPT_DESIG_ID,DELETED,PARENT_DEPT_DESIG_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Designation Short Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtshortnm" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass=" RequiredField  txtBox_en" Text='<%# Eval("DESIG_SHORT_NAME") %>' Onkeypress="return NoSpaceAllowed(event);"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtshortnm" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox_en" Onkeypress="return NoSpaceAllowed(event);"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblshortnm" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Width="160px" runat="server" Text='<%# Eval("DESIG_SHORT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesigName" TabIndex="4" Width="250px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox_en" Text='<%# Eval("DESIG_NAME") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDesigName" TabIndex="4" Width="250px" MaxLength="100" runat="server"
                                CssClass="RequiredField   txtBox_en"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDesigName" Width="250px" runat="server" Text='<%# Eval("DESIG_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation Short Name(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtshortnmAR" TabIndex="5" Width="160px" MaxLength="10" runat="server"
                                CssClass="txtBox_ol" Onkeypress="return NoSpaceAllowed(event);" Text='<%# Eval("DESIG_SHORT_NAME_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtshortnmAR" TabIndex="5" Width="160px" MaxLength="10" runat="server"
                                CssClass="txtBox_ol" Onkeypress="return NoSpaceAllowed(event);"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblshortnmAR" Width="160px" runat="server" Text='<%# Eval("DESIG_SHORT_NAME_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation Name(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesigNameAR" TabIndex="5" Width="250px" MaxLength="100" runat="server"
                                CssClass=" txtBox_ol" Text='<%# Eval("DESIG_NAME_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDesigNameAR" TabIndex="5" Width="250px" MaxLength="100" runat="server"
                                CssClass="  txtBox_ol"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDesigNameAR" Width="250px" runat="server" Text='<%# Eval("DESIG_NAME_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parent Designation">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlParentname" Width="200px" TabIndex="8" runat="server" CssClass=" EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlParentname" Width="200px" TabIndex="8" runat="server" CssClass=" EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblParentName" Width="200px" runat="server" Text='<%# Eval("PARENT_DESIG_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="No Of Employee">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoofEmp" TabIndex="12" Width="70px" MaxLength="2" runat="server" Text='<%# Eval("NO_OF_EMP") %>'
                                CssClass=" txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE5" runat="server" FilterType="Numbers"
                                TargetControlID="txtNoofEmp" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoofEmp" TabIndex="12" Width="70px" MaxLength="2" runat="server"
                                CssClass="txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE6" runat="server" FilterType="Numbers"
                                TargetControlID="txtNoofEmp" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoOfEmp" Width="70px" runat="server" Text='<%# Eval("NO_OF_EMP") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="6" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="8" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="9" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="8" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="9" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="7" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="5" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
