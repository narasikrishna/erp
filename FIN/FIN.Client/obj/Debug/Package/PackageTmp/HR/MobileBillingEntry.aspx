﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MobileBillingEntry.aspx.cs" Inherits="FIN.Client.HR.MobileBillingEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 500px">
                <asp:DropDownList ID="ddlPeriod" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Divs1">
                Employee
            </div>
            <div class="divtxtBox LNOrient" style="  width: 500px">
                <asp:DropDownList ID="ddlEmployeeName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDesignation">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="  width:505px">
                <asp:TextBox ID="txtDepartment" Enabled="false" CssClass="validate[] txtBox" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
       
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAmosunt">
                Mobile Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 160px">
                <asp:TextBox ID="txtMobile" CssClass="validate[] txtBox_N" runat="server" Enabled="false"
                    TabIndex="3" MaxLength="50"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="+ -,0"
                    FilterType="Numbers,Custom" TargetControlID="txtMobile" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAmoswunt">
                Limit
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtLimit" CssClass="validate[] txtBox_N" runat="server" Enabled="false"
                    TabIndex="4" MaxLength="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,0"
                    FilterType="Numbers,Custom" TargetControlID="txtLimit" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px;" id="Div12">
                Total Facility Limit
            </div>
            <div class="divtxtBox LNOrient" style="  width: 160px">
                <asp:TextBox ID="txttotFacillimit" runat="server" TabIndex="5" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                    Enabled="false" Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txttotFacillimit" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px;" id="Div12">
                Total Remaining Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txttotRemainingAmt" runat="server" TabIndex="6" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                    Enabled="false" Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txttotFacillimit" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="MOB_LIMIT_ENTRY_DTL_ID,CODE,DELETED" Width="700px" OnRowDataBound="gvData_RowDataBound"
                ShowFooter="false">
                <Columns>
                    <asp:BoundField DataField="DESCRIPTION" HeaderText="Facility" ItemStyle-Width="100px"
                        ItemStyle-Wrap="True" Visible="true">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText=" Used">
                        <ItemTemplate>
                            <asp:TextBox ID="txtFacilityLimit" MaxLength="15" TabIndex="7" runat="server" CssClass="txtBox_N"
                                OnTextChanged="txtFacilityLimit_TextChanged" AutoPostBack="True" Width="96%"
                                Text='<%# Eval("VALUE_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txtFacilityLimit" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eligibility">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxSelect" TabIndex="8" runat="server" Width="100%" Checked='<%# Convert.ToBoolean(Eval("ELIGIBILITY")) %>'>
                            </asp:CheckBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Eligibility Value">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRequiredLimit" MaxLength="15" TabIndex="9" runat="server" CssClass="txtBox_N"
                                OnTextChanged="txtRequiredLimit_TextChanged" Width="96%" Text='<%# Eval("ELIGIBILITY_AMT") %>'
                                AutoPostBack="True"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtRequiredLimit" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remaining Value">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRemainingvalue" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                OnTextChanged="txtRemainingvalue_TextChanged" Enabled="false" Width="96%" Text='<%# Eval("REMAINING_AMT") %>'
                                AutoPostBack="True"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtRequiredLimit" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
         <asp:HiddenField runat="server" ID="hd_dept_id" />
        <asp:HiddenField runat="server" ID="hd_mob_no" />
        <asp:HiddenField runat="server" ID="hd_mob_limit" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="26" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="28" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
