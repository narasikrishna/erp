﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveApplicationEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveApplicationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer" runat="server">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFinancialYear">
                Financial Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" AutoPostBack="true"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblApplicationDate">
                Application Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtApplicationDate" CssClass="validate[] RequiredField txtBox" runat="server"
                    OnTextChanged="txtApplicationDate_TextChanged" TabIndex="2" MaxLength="10"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtApplicationDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtApplicationDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:DropDownList ID="ddlStaffName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" TabIndex="3" Enabled="False">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveType">
                Leave Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlLeaveType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Available Leave
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtAvailLeave" Enabled="false" CssClass="validate[]  txtBox" runat="server"
                    TabIndex="6" MaxLength="10"></asp:TextBox>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveFrom">
                    Leave From
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtLeaveFrom" CssClass="validate[required] RequiredField txtBox"
                        AutoPostBack="True" OnTextChanged="txtLeaveFrom_TextChanged" runat="server" TabIndex="7"
                        MaxLength="10"></asp:TextBox>
                    <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtLeaveFrom">
                    </cc2:CalendarExtender>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtLeaveFrom" />
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveTo">
                    Leave To
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtLeaveTo" CssClass="EntryFont validate[required] RequiredField txtBox"
                        runat="server" AutoPostBack="True" OnTextChanged="txtLeaveTo_TextChanged" TabIndex="8"
                        MaxLength="10"></asp:TextBox>
                    <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtLeaveTo">
                    </cc2:CalendarExtender>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtLeaveTo" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 145px" id="Div7">
                    As on
                </div>
                <div class="LNOrient">
                    <asp:Label runat="server" ID="lblAsOnDate" class="lblBox"></asp:Label>
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtAsOnDateLeaveBalance" CssClass="EntryFont validate[required] RequiredField txtBox"
                        Enabled="false" runat="server" TabIndex="8" MaxLength="10"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBosxExtender6" runat="server" ValidChars="."
                        FilterType="Numbers,Custom" TargetControlID="txtAsOnDateLeaveBalance" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblNoOfDays">
                    No. Of Days
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtNoOfDays" CssClass="validate[] txtBox_N " runat="server" TabIndex="9"
                        Enabled="false" OnTextChanged="txtNoOfDays_TextChanged"></asp:TextBox>
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                    Relation
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlLevel" runat="server" CssClass="validate[] ddlStype" TabIndex="15">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblReason">
                    Reason
                </div>
                <div class="divtxtBox  LNOrient" style="width: 482px">
                    <asp:TextBox ID="txtReason" MaxLength="500" CssClass="validate[] txtBox" runat="server"
                        TabIndex="11"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" runat="server" id="divremarks" visible="false">
                <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                    Remarks
                </div>
                <div class="divtxtBox  LNOrient" style="width: 482px; height: 50px;">
                    <asp:TextBox ID="txtRemarks" MaxLength="240" Height="50px" CssClass="validate[] txtBox"
                        TextMode="MultiLine" runat="server" TabIndex="12"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveAddress">
                    Leave Address
                </div>
                <div class="divtxtBox  LNOrient" style="width: 480px">
                    <asp:TextBox ID="txtLeaveAddress" MaxLength="500" CssClass="validate[] txtBox" runat="server"
                        TabIndex="13"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px; height: 14px;" id="lblContactNumber">
                    Contact Number
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtContactNumber" MaxLength="50" CssClass="validate[] txtBox" runat="server"
                        TabIndex="14"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="+-"
                        FilterType="Numbers,Custom" TargetControlID="txtContactNumber" />
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 150px" id="lblApplicationStatus" runat="server"
                    visible="false">
                    Application Status
                </div>
                <div class="divtxtBox  LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlApplicationStatus" runat="server" CssClass="validate[required] RequiredField ddlStype"
                        Visible="false" TabIndex="10" OnSelectedIndexChanged="ddlApplicationStatus_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblHalfDayFrom">
                    Half Day From
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:CheckBox ID="HalfDayFrom" runat="server" TabIndex="15" />
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 150px" id="lblHalfDayTo">
                    Half Day To
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:CheckBox ID="HalfDayTo" runat="server" TabIndex="16" />
                </div>
                <%-- <div class="lblBox" style="float: left; width: 200px" id="Div16">
            </div>
            <div class="divtxtBox" style="float: left; width: 100px">
            </div>--%>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                    Planned/Unplanned Leave
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:CheckBox ID="chkPlanUnplan" runat="server" TabIndex="17" />
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                    Handover
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:CheckBox ID="chkHandOver" runat="server" TabIndex="18" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" runat="server" visible="false" id="divActingEmp">
                <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                    Acting Employee
                </div>
                <div class="divtxtBox  LNOrient" style="width: 480px">
                    <asp:DropDownList ID="ddlActingEmp" runat="server" CssClass="ddlStype" TabIndex="19">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                                TabIndex="20" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="21" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="22" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="23" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
