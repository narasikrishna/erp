﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalDefinitionEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalDefinitionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAppraisalCode">
                Appraisal Code
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAppraisalCode" MaxLength="10" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPeriod" TabIndex="2" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:TextBox ID="txtDescription" TabIndex="3" TextMode="MultiLine" MaxLength="500" CssClass="validate[required] RequiredField txtBox"
                 Height = "50px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Description(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:TextBox ID="txtDescriptionol" TabIndex="4" TextMode="MultiLine" MaxLength="500" CssClass="txtBox_ol"
                 Height = "50px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtToDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField  txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="7" />
            </div>
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDesignation" CssClass="validate[] txtBox" runat="server"
                    TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAmount" CssClass="validate[] txtBox" runat="server" 
                    TabIndex="6"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
