﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalFinalAssessmentEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalFinalAssessmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAppraisalCode">
                Employee Number
            </div>
            <div class="divtxtBox" style="float: left; width: 480px">
                <asp:DropDownList ID="ddlEmp" runat="server" 
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1" 
                    AutoPostBack="True" onselectedindexchanged="ddlEmp_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDescription">
                Department
            </div>
            <div class="divtxtBox" style="float: left; width: 480px">
                <asp:TextBox ID="txtDept" TabIndex="2" Enabled="false" CssClass="validate[] txtBox" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Designation
            </div>
            <div class="divtxtBox" style="float: left; width: 480px">
                <asp:TextBox ID="txtDesig" TabIndex="3" Enabled="false" CssClass="txtBox" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblFromDate">
                Rating
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" MaxLength="3" ID="txtRat" CssClass="validate[]  txtBox" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtRat" />
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="lblToDate">
                HR/CEO Rating
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtHrRat" MaxLength="3" CssClass="validate[required] RequiredField  txtBox"
                    TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtHrRat" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
       <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div1">
                Chairman/Vice Chairman Rating
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtchairmanrat" MaxLength="3" 
                    CssClass="validate[required] RequiredField  txtBox" TabIndex="6" 
                    AutoPostBack="True" ontextchanged="txtchairmanrat_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtchairmanrat" />
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="Div3">
              Total Rating
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtTotRat" MaxLength="3" CssClass="validate[]   txtBox"
                    TabIndex="7"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtTotRat" />
            </div>
        </div>
        <asp:HiddenField ID="hfRating" runat="server" />
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <%--<script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
