﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainerEntry.aspx.cs" Inherits="FIN.Client.HR.TrainerEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblType">
                Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlType" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" runat="server" TabIndex="1" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:DropDownList ID="ddlEmployeeName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="divClear_10">
            </div>
        </div>
        <%-- <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer" id="divName" runat="server" visible="false">
            <div class="lblBox LNOrient" style="float: left; width: 200px" id="lblName">
                Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 270px">
                <asp:TextBox ID="txtName" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblProfile">
                Profile
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:TextBox ID="txtProfile" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblExperience">
                Experience
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtExperience" MaxLength="2" CssClass="txtBox_N" runat="server" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="0"
                    FilterType="Numbers,Custom" TargetControlID="txtExperience" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" visible="false" runat="server"
                id="lblConsultancyName">
                Consultancy Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtConsultancyName" Visible="false" MaxLength="250" CssClass="txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="ConsultancyContact" runat="server" visible="false">
            <div class="lblBox LNOrient" style="width: 200px" id="lblConsultancyContactPerson">
                Consultancy Contact Person
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtConsultancyContactPerson" MaxLength="250" CssClass="txtBox" runat="server"
                    TabIndex="6"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblConsultancyContactNumber">
                Consultancy Contact Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtContactNo" MaxLength="250" CssClass="txtBox" runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <%--<div class="divFormcontainer" 
        style="width: 300px; z-index: 1; left: 8px; top: 115px; position: absolute; height: 34px;" 
        id="div1">--%>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="9" OnClick="btnSave_Click" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="11" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                </td>
            </tr>
        </table>
    </div>
    <%-- </div>--%>
    <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server"  CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
