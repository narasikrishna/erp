﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ResumeEntry.aspx.cs" Inherits="FIN.Client.HR.ResumeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblName">
                Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtName" CssClass="validate[required] RequiredField txtBox" runat="server" TabIndex="1"></asp:TextBox>
            </div>
             <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblTitle">
                Title
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtTitle" CssClass="validate[required] RequiredField txtBox" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblFileFormat">
                File Format
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlFileFormat" runat="server" CssClass="ddlStype" Width="100%"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
             <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblUploadFile">
                Upload File
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <div class="txtboxCenter">
                    <asp:FileUpload ID="fpUploadFile" runat="server" TabIndex="4"  />
                </div>
            </div>
        </div>    
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPrimarySkills">
                Primary Skills
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPrimarySkills" CssClass=" txtBox" runat="server" TabIndex="5"></asp:TextBox>
            </div>
             <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblSecSkills">
                Secondary Skills
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtSecSkills" CssClass=" txtBox" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblContactDate">
                Last Contact Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtLastContactDate" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]   txtBox"
                    runat="server" TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtenderToDate" runat="server" Format="dd/MM/yyyy" TargetControlID="txtLastContactDate">
                </cc2:CalendarExtender>
            </div>
             <div class="colspace" style="float: left;" >&nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblComment">
                Comments
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtComment" TextMode="MultiLine" CssClass=" txtBox" runat="server" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="9" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                            TabIndex="10" onclick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
      
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
