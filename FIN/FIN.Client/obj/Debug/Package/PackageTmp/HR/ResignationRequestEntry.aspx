﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ResignationRequestEntry.aspx.cs" Inherits="FIN.Client.HR.ResignationRequestEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRequestNumber" MaxLength="50" CssClass="txtBox" Enabled="false"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 156px">
                <asp:TextBox TabIndex="2" runat="server" ID="txtRequestDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtRequestDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblDepartmentName">
            Department Name
        </div>
        <div class="divtxtBox  LNOrient" style="width: 530px">
            <asp:DropDownList ID="ddlDepartmentName" runat="server" CssClass="validate[required] RequiredField  ddlStype"
                TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartmentName_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lbldesignation">
            Designation
        </div>
        <div class="divtxtBox  LNOrient" style="width: 530px">
            <asp:DropDownList ID="ddldesignation" runat="server" CssClass="validate[required] RequiredField  ddlStype"
                TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddldesignation_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblEmployeeName">
            Employee Name</div>
        <div class="divtxtBox  LNOrient" style="width: 530px">
            <asp:DropDownList ID="ddlEmployeeName" runat="server" CssClass="validate[required] RequiredField  ddlStype"
                TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblResignationType">
            Resignation Type
        </div>
        <div class="divtxtBox  LNOrient" style="width: 150px">
            <asp:DropDownList ID="ddlResignationType" runat="server" CssClass="validate[required] RequiredField  ddlStype"
                TabIndex="6">
            </asp:DropDownList>
        </div>
        <div class="colspace  LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 200px" id="lblNoticePeriod">
            Notice Period
        </div>
        <div class="divtxtBox  LNOrient" style="width: 156px">
            <asp:TextBox ID="txtNoticePeriod" CssClass="validate[required] RequiredField txtBox_N"
                runat="server" MaxLength="3" TabIndex="7" Enabled="true"></asp:TextBox>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=""
                FilterType="Numbers,Custom" TargetControlID="txtNoticePeriod" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
            From Date
        </div>
        <div class="divtxtBox  LNOrient" style="width: 150px">
            <asp:TextBox TabIndex="8" runat="server" ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtFromDate"
                OnClientDateSelectionChanged="checkDate" />
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
        </div>
        <div class="colspace  LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 200px" id="lblRequestStatus">
            Request Status
        </div>
        <div class="divtxtBox  LNOrient" style="width: 158px">
            <asp:DropDownList ID="ddlRequestStatus" runat="server" CssClass="validate[required] RequiredField ddlStype"
                TabIndex="9">
            </asp:DropDownList>
        </div>
        <%-- <div class="lblBox" style="float: left; width: 200px" id="lblToDate">
            To Date
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox TabIndex="8" runat="server" ID="txtToDate" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtToDate"
                OnClientDateSelectionChanged="checkDate" />
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                FilterType="Numbers,Custom" TargetControlID="txtToDate" />
        </div>--%>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <%-- <div class="lblBox" style="float: left; width: 200px" id="lblFileName">
            File Name
        </div>
        <div class="divtxtBox" style="float: left; width: 150px">
            <asp:TextBox ID="txtFileName" CssClass="txtBox" runat="server" MaxLength="100" TabIndex="9"></asp:TextBox>
        </div>--%>
        <%--           <div class="colspace" style="float: left;" >&nbsp</div>--%>
        <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
            Reason
        </div>
        <div class="divtxtBox  LNOrient" style="width: 528px">
            <asp:DropDownList ID="ddlReason" runat="server" CssClass="validate[required] RequiredField ddlStype"
                TabIndex="10">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <%--  <div class="colspace" style="float: left;" >&nbsp</div>--%>
        <div class="lblBox LNOrient" style="width: 200px" id="lblRemarks">
            Remarks
        </div>
        <div class="divtxtBox  LNOrient" style="width: 532px">
            <asp:TextBox ID="txtRemarks" CssClass="validate[] txtBox" runat="server" MaxLength="500"
                TextMode="MultiLine" Height="50px" TabIndex="11"></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
            Active
        </div>
        <div class="divtxtBox  LNOrient" style="width: 150px">
            <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="12" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="12" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
