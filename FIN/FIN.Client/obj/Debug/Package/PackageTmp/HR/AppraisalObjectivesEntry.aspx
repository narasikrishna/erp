﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalObjectivesEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalObjectivesEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PER_OBJ_ID,PER_TYPE,DELETED,PER_CATEGORY" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcode" TabIndex="1" Width="130px" MaxLength="10" runat="server"
                                CssClass=" RequiredField  txtBox_en" Text='<%# Eval("PER_CODE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="LowercaseLetters,UppercaseLetters,Numbers"
                                TargetControlID="txtcode" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtcode" TabIndex="9" Width="130px" MaxLength="10" runat="server"
                                Wrap="true" CssClass="RequiredField   txtBox_en"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="LowercaseLetters,UppercaseLetters,Numbers"
                                TargetControlID="txtcode" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcode" Width="130px" runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap;" Text='<%# Eval("PER_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdesc" TabIndex="2" Width="130px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox_en" Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdesc" TabIndex="10" Width="130px" MaxLength="100" runat="server"
                                Wrap="true" CssClass="RequiredField   txtBox_en"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldesc" Width="130px" runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap;" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcodeAR" TabIndex="3" Width="130px" MaxLength="10" runat="server"
                                CssClass=" txtBox_ol" Text='<%# Eval("PER_CODE_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtcodeAR" TabIndex="11" Width="130px" MaxLength="10" runat="server"
                                CssClass="txtBox_ol"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcodeAR" Width="130px" runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap;" Text='<%# Eval("PER_CODE_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="GridOLAlign"  />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdescAR" TabIndex="4" Width="130px" MaxLength="100" runat="server"
                                Wrap="true" CssClass=" txtBox_ol" Text='<%# Eval("PER_DESC_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdescAR" TabIndex="12" Width="130px" MaxLength="100" runat="server"
                                CssClass="txtBox_ol"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldescAR" Width="130px" runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap;" Text='<%# Eval("PER_DESC_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="GridOLAlign"  />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="RequiredField ddlStype"
                                Width="97%" TabIndex="5">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCategory" TabIndex="13" runat="server" CssClass="RequiredField ddlStype"
                                Wrap="true" Width="97%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" CssClass="adminFormFieldHeading" Width="97%" runat="server"
                                Text='<%# Eval("PER_CATEGORY") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlType" TabIndex="6" runat="server" CssClass="RequiredField ddlStype"
                                Width="97%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlType" TabIndex="14" runat="server" CssClass="RequiredField ddlStype"
                                Width="97%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" CssClass="adminFormFieldHeading" Width="97%" runat="server"
                                Text='<%# Eval("PER_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minimum Rating">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMin" TabIndex="7" Width="130px" MaxLength="10" runat="server"
                                CssClass=" RequiredField txtBox_N" Text='<%# Eval("PER_MIN_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtMin" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMin" TabIndex="15" Width="130px" MaxLength="10" runat="server"
                                CssClass="RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtMin" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmin" Width="130px" runat="server" Text='<%# Eval("PER_MIN_RATING") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Maximum Rating">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtmax" TabIndex="8" Width="130px" MaxLength="10" runat="server"
                                CssClass=" RequiredField txtBox_N" Text='<%# Eval("PER_MAX_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtmax" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtmax" TabIndex="16" Width="130px" MaxLength="10" runat="server"
                                CssClass="RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtmax" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmax" Width="130px" runat="server" Text='<%# Eval("PER_MAX_RATING") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="17" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="18" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="19" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="20" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="21" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
