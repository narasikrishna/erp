﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingScheduleEntry.aspx.cs" Inherits="FIN.Client.HR.TrainingScheduleEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1260px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDesc">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 526px">
                <asp:TextBox ID="txtdesc" CssClass="validate[required] RequiredField txtBox" runat="server"
                    MaxLength="200" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox TabIndex="2" runat="server" ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtFromDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox TabIndex="3" runat="server" ID="txtToDate" 
                    CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox" 
                    AutoPostBack="True" ontextchanged="txtToDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px;" id="lblTrainType">
                Training Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlTrainingType" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Number of Days
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtNoOfDays" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" MaxLength="4" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtsdrender4" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtNoOfDays" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="TRN_SCH_DTL_ID,REQ_HDR_ID,REQ_DTL_ID,EMP_ID,PROG_ID,COURSE_ID,SUBJECT_ID,LOOKUP_ID,DELETED"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Training Date">
                        <EditItemTemplate>
                            <asp:TextBox Width="150px" ID="dtpStartDate" MaxLength="10" runat="server" CssClass="RequiredField txtBox"
                                TabIndex="4" Text='<%#  Eval("TRN_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox Width="150px" ID="dtpStartDate" TabIndex="4" MaxLength="10" runat="server"
                                CssClass="RequiredField EntryFont  txtBox" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="150px" ID="lblStartDate" runat="server" Text='<%# Eval("TRN_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Program">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProg" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlProg_SelectedIndexChanged" Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlProg" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlProg_SelectedIndexChanged" Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProg" Style="word-wrap: break-word white-space: pre-wrap; height: 100%"
                                Width="130px" runat="server" Text='<%# Eval("PROG_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlReqType" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                OnSelectedIndexChanged="ddlReqType_SelectedIndexChanged" AutoPostBack="true"
                                Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlReqType" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                OnSelectedIndexChanged="ddlReqType_SelectedIndexChanged" AutoPostBack="true"
                                Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReqType" Style="word-wrap: break-word white-space: pre-wrap; height: 100%"
                                Width="130px" runat="server" Text='<%# Eval("REQ_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlReqDtl" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="true" Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlReqDtl" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="true" Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReqNo" Style="word-wrap: break-word white-space: pre-wrap; height: 100%"
                                Width="130px" runat="server" Text='<%# Eval("REQ_DTLS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trainer">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlTrainer" TabIndex="7" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlTrainer" TabIndex="7" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTrainer" Width="130px" runat="server" Text='<%# Eval("TRAINER") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Course">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCourse" Width="150px" TabIndex="8" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged"
                                AutoPostBack="true" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCourse" Width="150px" AutoPostBack="true" TabIndex="8" runat="server"
                                OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged" CssClass="RequiredField EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCourseName" Width="130px" runat="server" Text='<%# Eval("COURSE_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Subject">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlSubject" Width="150px" AutoPostBack="false" TabIndex="9"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlSubject" Width="150px" AutoPostBack="true" TabIndex="9"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                                <asp:ListItem Value="">---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSubjectName" Width="130px" runat="server" Text='<%# Eval("SUBJECT_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Session">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlSession" Width="150px" AutoPostBack="false" TabIndex="10"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlSession" Width="150px" AutoPostBack="true" TabIndex="10"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSession" Width="130px" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="150px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="15" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
