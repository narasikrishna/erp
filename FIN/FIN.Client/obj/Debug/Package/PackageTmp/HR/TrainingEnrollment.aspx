﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingEnrollment.aspx.cs" Inherits="FIN.Client.HR.TrainingEnrollment" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblJobCode">
                Enrollment Id
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:TextBox ID="txtCriteria" TabIndex="1" MaxLength="50" runat="server" Enabled="false"
                    CssClass=" txtBox"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblJobName">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:TextBox ID="dtpTrnDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpTrnDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblScheduleName">
                Schedule
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlScheduleName" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlSchedule_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblProgram">
                Program
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlProgram" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlProgram_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCourse">
                Course
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlCourse" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblSubject">
                Subject
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlSubject" runat="server" TabIndex="6" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlSubject_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblSession">
                Session
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlSession" runat="server" TabIndex="7" 
                    CssClass="validate[required] RequiredField ddlStype" 
                    onselectedindexchanged="ddlSession_SelectedIndexChanged" 
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Trainer
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlTrainer" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:HiddenField ID="hf_Trreqdtlid" runat="server" />
        </div>
         <div class="divClear_10">
        </div>
        <div align="left">
            <div class="LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="800px" DataKeyNames="ENRL_DTL_ID,EMP_ID"
                    ShowFooter="false" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:BoundField DataField="EMP_NAME" HeaderText="Employee Name" />
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("SEL_EMP")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="2" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="5" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
