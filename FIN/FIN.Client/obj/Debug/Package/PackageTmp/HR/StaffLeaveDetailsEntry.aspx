﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="StaffLeaveDetailsEntry.aspx.cs" Inherits="FIN.Client.HR.StaffLeaveDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="left">
            <div class="lblBox LNOrient" style="width: 120px" id="lblFinancialYear">
                Financial Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 340px">
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 120px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    Width="100%" runat="server" Enabled="false" TabIndex="2" AutoPostBack="True"
                    OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 115px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txtToDate" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]    txtBox"
                    Width="100%" runat="server" TabIndex="3" Enabled="false" 
                    ontextchanged="txtToDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtenderEndDate" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtToDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
       
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblStaffName">
                Staff Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 340px">
                <asp:DropDownList ID="ddlStaffName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="340px" TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
       
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 340px">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="340px" TabIndex="5" Enabled="false" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged1">
                </asp:DropDownList>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="LSD_ID,LEAVE_ID,ACCURED_LEAVE_BALANCE" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Leave Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLeave" Width="95%" TabIndex="6" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlLeave_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLeave" Width="95%" TabIndex="6" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlLeave_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLeaveName" Width="150px" runat="server" Text='<%# Eval("LEAVE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Days">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoOfDays" Width="95%" MaxLength="3" TabIndex = "7" runat="server" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("NO_OF_DAYS") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfDays" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoOfDays" TabIndex="7" Width="95%" MaxLength="3" runat="server"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfDays" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoOfDays" Width="130px" runat="server" Text='<%# Eval("NO_OF_DAYS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Leave Availed">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLeaveAvailed" TabIndex="8" Width="95%" MaxLength="3" runat="server"
                                Enabled="false" CssClass="RequiredField txtBox_N" Text='<%# Eval("LEAVE_AVAILED") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtLeaveAvailed" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLeaveAvailed" TabIndex="8" Width="95%" MaxLength="3" runat="server"
                                Enabled="false" CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtLeaveAvailed" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLeaveAvailed" Width="100px" runat="server" Text='<%# Eval("LEAVE_AVAILED") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Leave Balance" Visible="false" >
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLeaveBalance1" TabIndex="9" Width="95%" MaxLength="6" runat="server"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("LEAVE_BALANCE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtLeaveBalance" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLeaveBalance1" TabIndex="9" Width="95%" MaxLength="6" runat="server"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtLeaveBalance" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLeaveBalance" Width="100px" runat="server" Text='<%# Eval("LEAVE_BALANCE") %>'></asp:Label>
                        </ItemTemplate>
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Accured Leave Balance">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAccuredLeaveBalance" TabIndex="10" Width="95%" MaxLength="6" runat="server" Enabled="false"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("ACCURED_LEAVE_BALANCE") %>'></asp:TextBox>
                                 <asp:TextBox ID="txtLeaveBalance" TabIndex="10" Width="95%" MaxLength="6" runat="server" Visible="false"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("LEAVE_BALANCE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FiltersdedTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtAccuredLeaveBalance" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAccuredLeaveBalance" TabIndex="8" Width="95%" MaxLength="6" runat="server" Enabled="false" 
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                                 <asp:TextBox ID="txtLeaveBalance" TabIndex="11" Width="95%" MaxLength="6" runat="server" Visible="false"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("LEAVE_BALANCE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTsdfextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtAccuredLeaveBalance" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccuredLeaveBalance" Width="100px" runat="server" Text='<%# Eval("ACCURED_LEAVE_BALANCE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
