﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginNew_old.aspx.cs" Inherits="FIN.Client.LoginNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../Scripts/JQueryValidatioin/jquery.validationEngine_EN.js" type="text/javascript"></script>
    <script src="../Scripts/JQueryValidatioin/jquery.validationEngine.js" type="text/javascript"></script>
    <link href="../Scripts/JQueryValidatioin/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <script>
        jQuery(document).ready(function () { jQuery("#form1").validationEngine(); });
    </script>
    <title>Finance</title>
    <link href="Styles/ListMenuStyle.css" rel="stylesheet" type="text/css" />
    <link href="Styles/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 600px;
        }
    </style>
</head>
<body style="margin: 0px">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div align="center">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;
            background-color: gray">
            <tr align="center" style="background-color: White">
                <td align="left" valign="top" style="height: 120px; background-image: url(Images/logo.png);
                    background-repeat: no-repeat" class="style1">
                    <br />
                    &nbsp;
                </td>
            </tr>
            <tr align="center">
                <td style="padding-left: 300px; height: 120px;" align="center">
                    <table class="detailsTable">
                        <tr>
                            <td style="padding-left: 20px">
                                <table class="adminFormCaptureTable" id="Table1" width="100%">
                                    <tr>
                                        <td class="adminFormFieldHeading" width="630px" height="300px" style="background-image: url('Images/LOG-IN-box.jpg');
                                            background-repeat: no-repeat">
                                            <table class="adminFormCaptureTable" id="Table4" width="100%">
                                                <tr>
                                                    <td height="30px">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="adminFormFieldData" align="left">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <asp:TextBox CssClass="validate[required] txtBox" MaxLength="50" Width="150px" Height="25px"
                                                                        ID="txtUserID" runat="server"></asp:TextBox>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                                                </td>
                                                                <td style="color: White; font-family: Verdana Tahoma; font-size: 25px">
                                                                    User Name
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="adminFormFieldData" align="left">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <asp:TextBox CssClass="validate[required] txtBox" Width="150px" Height="25px" ID="txtPassword"
                                                                        runat="server" TextMode="Password"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <td style="color: White; font-family: Verdana Tahoma; font-size: 25px">
                                                                    Password
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr align="left">
                                                    <td class="adminFormButtons" style="height: 80px; padding: 0px;">
                                                        <table id="Table3" cellspacing="2" cellpadding="2" border="0">
                                                            <tr>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <td width="45px">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="110px">
                                                                    <asp:Button CssClass="button" Style="background-color: transparent; border: 0 none;
                                                                        font-family: Verdana Tahoma; font-size: 25px" ID="btnlogin" Height="60px" Width="95px"
                                                                        Text="Login" runat="server" OnClick="btnlogin_Click1"></asp:Button>
                                                                </td>
                                                                &nbsp;&nbsp;
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <%-- <input class="button" type="reset" value="Cancel" />--%>
                                                                    <asp:Button CssClass="button" ID="btnCancel" Style="background-color: transparent;
                                                                        border: 0 none; font-family: Verdana Tahoma; font-size: 25px" Height="60px" Width="95px"
                                                                        Text="Cancel" runat="server"></asp:Button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                 
                                                <tr  runat="server" id="tr_chngpw">
                                                    <td>
                                                      <asp:HiddenField ID="hfChngPW" runat="server" />
                                                        <cc2:ModalPopupExtender ID="ModalPopupExtenderCP" runat="server" TargetControlID="hfChngPW"
                                                            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                                                        </cc2:ModalPopupExtender>
                                                        <asp:Panel ID="pnlConfirm" runat="server">
                                                            <div class="ConfirmForm">
                                                                <table width="100%" style="background-color: White; width: 400px; height: 150px">
                                                                    <tr class="ConfirmHeading" style="width: 100%; background-color: White">
                                                                        <td>
                                                                            <asp:Label ID="lblConfirm" runat="server" Text="Change Password"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            New Password
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox  ID="txtnewpw" runat="server" TextMode="Password"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                           Confirm New Password
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox  ID="txtconfirmpw" runat="server" TextMode="Password"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Save" 
                                                                                Width="60px" onclick="btnYes_Click" />
                                                                            &nbsp;
                                                                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="Cancel" Width="60px" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
