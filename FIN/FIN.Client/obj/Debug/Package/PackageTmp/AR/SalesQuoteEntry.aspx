﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SalesQuoteEntry.aspx.cs" Inherits="FIN.Client.AR.SalesQuoteEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblQuoteNumber">
                Quote Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:TextBox ID="txtQuoteNumber" CssClass="validate[] txtBox" runat="server" TabIndex="1"
                    Enabled="false"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblQuoteDate">
                Quote Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtQuoteDate" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2" MaxLength="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtQuoteDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtQuoteDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblQuoteType">
                Quote Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlQuoteType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="3" AutoPostBack="true" OnSelectedIndexChanged="ddlQuoteType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblCustomerName">
                Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:DropDownList ID="ddlCustomerName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged" AutoPostBack="true"
                    Width="500px" TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblSupplierSite">
                Customer Site
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="5" CssClass="ddlStype"
                    Width="500px">
                </asp:DropDownList>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblDescription">
                    Description
                </div>
                <div class="divtxtBox LNOrient" style="width: 300px">
                    <asp:TextBox ID="txtDescription" CssClass="validate[required] RequiredField  txtBox"
                        TextMode="MultiLine" Width="505px" MaxLength="100" runat="server" TabIndex="6"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblRequestDate">
                    Request Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtRequestDate" CssClass=" txtBox" runat="server" TabIndex="6" MaxLength="10"
                        OnTextChanged="txtRequestDate_TextChanged"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtRequestDate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtQuoteDate" />
                </div>
                <div class="lblBox LNOrient" style="width: 190px" id="lblExpireDate">
                    Expire Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtExpireDate" CssClass=" txtBox" runat="server" TabIndex="7" MaxLength="10"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtExpireDate"
                        OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtExpireDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblCurrency">
                    Currency
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlStype" Width="150px"
                        TabIndex="8">
                    </asp:DropDownList>
                </div>
                <div class="lblBox LNOrient" style="width: 190px" id="lblPaymentTerm">
                    Payment Term
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlPaymentTerm" runat="server" CssClass="ddlStype" Width="150px"
                        TabIndex="9">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblShippingInstruction">
                    Shipping Instruction
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlShippingInstruction" runat="server" CssClass=" ddlStype"
                        Width="150px" TabIndex="10">
                    </asp:DropDownList>
                </div>
                <div class="lblBox LNOrient" style="width: 190px" id="lblCarrierService">
                    Carrier Service
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlCarrierService" runat="server" CssClass="ddlStype" Width="150px"
                        TabIndex="11">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <%-- <div class="lblBox" style="float: left; width: 190px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox" style="float: left; width: 500px">
                <asp:TextBox ID="txtDescription" CssClass="validate[required] txtBox" runat="server"
                    TabIndex="4" MaxLength="100"></asp:TextBox>
            </div>--%>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblNote">
                    Note
                </div>
                <div class="divtxtBox LNOrient" style="width: 515px">
                    <asp:TextBox ID="txtNote" CssClass=" txtBox" runat="server" MaxLength="200" TabIndex="12"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblModeofTransport">
                    Mode of Transport
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlModeofTransport" runat="server" CssClass="ddlStype" TabIndex="13">
                    </asp:DropDownList>
                </div>
                <div class="lblBox LNOrient" style="width: 190px" id="Div2">
                    Expected Delivery Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtDeleiveryDate" CssClass=" txtBox" runat="server" TabIndex="14"
                        MaxLength="10"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender32"
                        TargetControlID="txtDeleiveryDate" OnClientDateSelectionChanged="checkDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenders1" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtDeleiveryDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 190px" id="lblTotal">
                    Total Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:TextBox ID="txtTotal" CssClass="validate[required] RequiredField txtBox_N" Enabled="false"
                        runat="server" TabIndex="15" MaxLength="13"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                        FilterType="Numbers,Custom" TargetControlID="txtTotal" />
                </div>
                <div class="lblBox LNOrient" style="width: 190px" id="Div1">
                    Status
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="validate[required] RequiredField ddlStype"
                        TabIndex="16">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="divtxtBox LNOrient" style="width: 545px">
                    <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/print-icon.png"
                        OnClick="btnPrint_Click" Width="35px" Height="25px" Style="border: 0px" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divClear_10">
            </div>
            <div class=" LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="800px" DataKeyNames="QUOTE_DTL_ID,ITEM_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                    OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Line No" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" Text='<%# Eval("QUOTE_LINE_NUM") %>' Enabled="false"
                                    CssClass="EntryFont RequiredField txtBox_N" Width="70px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" Width="70px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLineNumn" runat="server" Text='<%# Eval("QUOTE_LINE_NUM") %>' Visible="false"
                                    Width="70px"></asp:Label>
                                <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                    Width="70px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Name">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype" Width="350px"
                                    TabIndex="25" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged1">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype" Width="300px"
                                    TabIndex="18" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged1">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemName" Width="300px" CssClass="adminFormFieldHeading" runat="server"
                                    Text='<%# Eval("ITEM_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <FooterStyle Width="200px" />
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Eval("COUNTRY_CODE") %>'
                                CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("COUNTRY_CODE") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Quantity">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("QUOTE_ITEM_QTY") %>'
                                    TabIndex="26" CssClass="EntryFont RequiredField txtBox_N" Width="96%" AutoPostBack="True"
                                    OnTextChanged="txtQuantity_TextChanged1"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                    TargetControlID="txtQuantity" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    TabIndex="19" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged1" Width="96%"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                    TargetControlID="txtQuantity" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUOTE_ITEM_QTY") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="UOM">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="150px" TabIndex="18">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="150px" TabIndex="18">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUOM" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("UOM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="UOM">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUOM" MaxLength="10" Enabled="false" runat="server" Text='<%# Eval("QUOTE_ITEM_UOM") %>'
                                    TabIndex="27" CssClass="EntryFont RequiredField txtBox" Width="96%"></asp:TextBox>
                                <%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtUOM"/>--%>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtUOM" MaxLength="10" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox"
                                    TabIndex="20" Width="96%"></asp:TextBox><%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender66"
                                    runat="server" TargetControlID="txtUOM" />--%>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUOM" Style="word-wrap: break-word" runat="server" Text='<%# Eval("QUOTE_ITEM_UOM") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit Price">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUnitPrice" runat="server" Text='<%# Eval("QUOTE_ITEM_UNIT_PRICE") %>'
                                    Enabled="false" TabIndex="28" CssClass="EntryFont RequiredField txtBox_N" Width="96%"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtUnitPrice" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" TabIndex="21" Width="96%"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtUnitPrice" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("QUOTE_ITEM_UNIT_PRICE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmount" Enabled="false" Text='<%# Eval("QUOTE_LINE_AMT") %>'
                                    runat="server" CssClass="EntryFont RequiredField txtBox_N" TabIndex="29" Width="96%"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmount" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    TabIndex="22" Width="96%"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("QUOTE_LINE_AMT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add/Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                    TabIndex="30" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                    TabIndex="31" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                    TabIndex="23" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                    TabIndex="24" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                    TabIndex="23" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                                TabIndex="11" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
