﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InvoiceEntry.aspx.cs" Inherits="FIN.Client.AR.InvoiceEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1070px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <table width="100%">
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceNumber">
                                Invoice Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px" id="lblInvoiceNoValue">
                                <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="txtBox"
                                    Enabled="false" MaxLength="50" TabIndex="1" OnTextChanged="txtInvoiceNumber_TextChanged"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceDate">
                                Invoice Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtInvoiceDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                                    AutoPostBack="True" OnTextChanged="txtInvoiceDate_TextChanged"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtInvoiceDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 130px" id="lblInvoiceDueDate">
                                Invoice Due Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 140px">
                                <asp:TextBox ID="txtInvDueDate" runat="server" CssClass=" txtBox" TabIndex="3"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="ceInvDueDate" TargetControlID="txtInvDueDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceType">
                                Invoice Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlInvoiceType" runat="server" TabIndex="4" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlInvoiceType_SelectedIndexChanged" CssClass=" ddlStype">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceCurrency">
                                Invoice Currency
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 440px">
                                <asp:DropDownList ID="ddlInvoiceCurrency" runat="server" TabIndex="5" CssClass="  ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlInvoiceCurrency_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierNumber">
                                Customer Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlSupplierNumber" runat="server" TabIndex="6" CssClass="validate[required] RequiredField ddlStype"
                                    OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierName">
                                Customer Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 440px">
                                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                                    Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierSite">
                                Customer Site
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="8" CssClass="ddlStype">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                                Payment Currency
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 440px">
                                <asp:DropDownList ID="ddlPaymentCurrency" runat="server" TabIndex="9" CssClass=" ddlStype">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPaymentType">
                                Payment Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlPaymentType" runat="server" TabIndex="11" CssClass=" ddlStype">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                                Exchange Rate Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlExchangeRateType" runat="server" TabIndex="12" CssClass="validate[required] RequiredField ddlStype">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 130px" id="Div2">
                                Exchange Rate
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 140px">
                                <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="12" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtExchangeRate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer LNOrient" style="display: none;">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblTerms">
                                Terms
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlTerm" runat="server" TabIndex="13" CssClass="validate[required] RequiredField ddlStype">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegment">
                                Global Segment
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="14">
                                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblRetentionAmount">
                                Retention Amount
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px;">
                                <asp:TextBox ID="txtRetAmount" runat="server" TabIndex="15" CssClass="txtBox_N" OnTextChanged="txtRetAmount_TextChanged"
                                    MaxLength="13"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtRetAmount" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 130px" id="lblPrePaymentBalance">
                                Prepayment Balance
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 140px;">
                                <asp:TextBox ID="txtPrePaymentBal" runat="server" TabIndex="16" CssClass=" txtBox_N"
                                    MaxLength="13"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtPrePaymentBal" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px;" id="lblInvoiceAmount">
                                Invoice Amount
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:TextBox ID="txtInvoiceAmount" runat="server" Enabled="false" TabIndex="17" MaxLength="13"
                                    CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                            <div class="lblBox LNOrient" style="width: 150px;" id="lblTax">
                                Tax
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtTax" runat="server" TabIndex="18" MaxLength="13" CssClass=" txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtTax" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp;</div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer" align="center">
                            <div class="lblBox LNOrient" style="width: 450px;" id="Div4">
                                <asp:Label ID="lblWaitApprove" runat="server" Text="WAITING FOR APPROVAL" Visible="false"
                                    CssClass="lblBox" Font-Size="18px" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 200px; display: none" id="lblBatch">
                                Batch
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px; display: none">
                                <asp:DropDownList ID="ddlBatch" runat="server" TabIndex="19" Width="150px" CssClass="validate[required] RequiredField ddlStype">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
                <td style="vertical-align:top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/Print.png" OnClick="btnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgbtninvoicecancel" runat="server" ImageUrl="~/Images/CancelJV.png"
                                    Style="border: 0px" OnClick="imgbtninvoicecancel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" OnClientClick="fn_PostVisible('FINContent_imgBtnPost')" />
                                <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnCancel" runat="server" ImageUrl="~/Images/CancelJV.png"
                                    Style="border: 0px" OnClick="imgBtnCancel_Click" />
                                <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnCancelJV" runat="server" ImageUrl="~/Images/CancelJVPrint.png"
                                    Style="border: 0px" OnClick="imgBtnCancelJV_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <asp:HiddenField ID="hfSalorderqty" runat="server" />
        <asp:HiddenField ID="Qtyadd" runat="server" />
        <asp:HiddenField ID="HFQTYFROMINV" runat="server" />
        <div class="divRowContainer LNOrient">
            <asp:Panel runat="server" ID="pnlgridview">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                    OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                    OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                    DataKeyNames="OM_CUST_INV_DTL_ID,OM_INV_LINE_TYPE,OM_DC_ID,ITEM_ID,CODE_ID,OM_INV_SEGMENT_ID1,OM_INV_SEGMENT_ID2,OM_INV_SEGMENT_ID3,OM_INV_SEGMENT_ID4,OM_INV_SEGMENT_ID5,OM_INV_SEGMENT_ID6">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="43" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="44" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="41" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="42" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="30" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Line No" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLineNo" TabIndex="31" runat="server" Enabled="true" Text='<%# Eval("OM_INV_LINE_NUM") %>'
                                    Width="24px" CssClass="EntryFont RequiredField txtBox_N" MaxLength="10"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLineNo" TabIndex="20" runat="server" MaxLength="10" Enabled="true"
                                    Text='<%# Container.DataItemIndex + 1 %>' CssClass="EntryFont RequiredField txtBox_N"
                                    Width="24px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("OM_INV_LINE_NUM") %>'
                                    Width="24px"></asp:Label>
                                <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                    Width="24px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Line Type">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlLineType" TabIndex="20" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlLineType_SelectedIndexChanged"
                                    Width="120px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlLineType" TabIndex="21" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlLineType_SelectedIndexChanged"
                                    Width="120px">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLineType" runat="server" Text='<%# Eval("OM_INV_LINE_TYPE_DESC") %>'
                                    Width="120px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlReceiptNo" TabIndex="33" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlReceiptNo_SelectedIndexChanged"
                                    Width="200px">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtMisNo" MaxLength="240" runat="server" Text='<%# Eval("MIS_NO") %>'
                                    Visible="false" CssClass="EntryFont RequiredField txtBox" Width="200px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlReceiptNo" TabIndex="22" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlReceiptNo_SelectedIndexChanged"
                                    Width="200px">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtMisNo" MaxLength="240" runat="server" Text='<%# Eval("MIS_NO") %>'
                                    Visible="false" CssClass="EntryFont RequiredField txtBox" Width="200px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblReceiptNo" runat="server" Text='<%# Eval("GRN_NUM") %>' Width="200px"></asp:Label>
                                <asp:Label ID="lblmisno" runat="server" Text='<%# Eval("MIS_NO") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Number">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlItemNo" TabIndex="34" runat="server" CssClass="ddlStype RequiredField"
                                    Width="170px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemNo_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                                <asp:TextBox ID="txtItemName" TabIndex="23" runat="server" Text='<%# Eval("ITEM_NAME") %>'
                                    CssClass="EntryFont txtBox" Enabled="false" Width="170px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlItemNo" runat="server" CssClass="ddlStype RequiredField"
                                    Width="170px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemNo_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                                <asp:TextBox ID="txtItemName" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                    Enabled="false" Width="170px" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemNo" Width="170px" runat="server" Text='<%# Eval("item_id") %>'></asp:Label>
                                <br />
                                <asp:Label ID="lblItemName" Width="170px" MaxLength="250" runat="server" Text='<%# Eval("ITEM_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="170px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Quantity">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQuantity" TabIndex="35" MaxLength="13" runat="server" Text='<%# Eval("OM_ITEM_QTY") %>'
                                    AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="80px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtQuantity" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtQuantity" TabIndex="24" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                        TargetControlID="txtQuantity" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" Style="word-wrap: break-word" runat="server" Text='<%# Eval("OM_ITEM_QTY") %>'
                                    Width="80px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit Price">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUnitPrice" TabIndex="36" MaxLength="13" runat="server" Text='<%# Eval("OM_ITEM_PRICE") %>'
                                    AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="150px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtUnitPrice" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtUnitPrice" TabIndex="25" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers,custom" ValidChars=".,"
                                        TargetControlID="txtUnitPrice" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUnitPrice" Style="word-wrap: break-word" runat="server" Text='<%# Eval("OM_ITEM_PRICE") %>'
                                    Width="80px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmount" TabIndex="37" MaxLength="13" runat="server" Text='<%# Eval("OM_ITEM_COST") %>'
                                    Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmount" TabIndex="26" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender64"
                                        runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAMount" Style="word-wrap: break-word" runat="server" Text='<%# Eval("OM_ITEM_COST") %>'
                                    Width="80px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account Code">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAccCode" TabIndex="38" runat="server" CssClass="ddlStype RequiredField"
                                    OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged" AutoPostBack="true"
                                    Width="230px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAccCode" TabIndex="27" runat="server" CssClass="ddlStype RequiredField"
                                    OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged" AutoPostBack="true"
                                    Width="230px">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAccCode" Style="word-wrap: break-word" runat="server" Text='<%# Eval("CODE_NAME") %>'
                                    Width="230px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 1">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment1" Width="180px" runat="server" Text='<%# Eval("SEGMENT_1_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 2">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment2" Width="180px" runat="server" Text='<%# Eval("SEGMENT_2_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 3">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment3" Width="180px" runat="server" Text='<%# Eval("SEGMENT_3_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 4">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment4" Width="180px" runat="server" Text='<%# Eval("SEGMENT_4_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 5">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment5" Width="180px" runat="server" Text='<%# Eval("SEGMENT_5_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 6">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                    TabIndex="8">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGSegment6" Width="180px" runat="server" Text='<%# Eval("SEGMENT_6_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle VerticalAlign="Top" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segments" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                            Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="btnSegments" runat="server" CssClass="btn" Text="Segments" OnClick="btnSegments_Click" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnSegments" TabIndex="39" runat="server" CssClass="btn" Text="Segments"
                                    OnClick="btnSegments_Click" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnSegments" TabIndex="28" runat="server" CssClass="btn" Text="Segments"
                                    OnClick="btnSegments_Click" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tax" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                            Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="btnTax" runat="server" CssClass="btn" Text="Tax" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnTax" TabIndex="40" runat="server" CssClass="btn" Text="Tax" OnClick="btnTax_Click" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnTax" TabIndex="29" runat="server" CssClass="btn" Text="Tax" OnClick="btnTax_Click" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:Panel runat="server" ID="pnlReason">
                <div class="lblBox LNOrient" style="width: 150px" id="lblReason">
                    Reason
                </div>
                <div class="divtxtBox LNOrient" style="width: 500px">
                    <asp:TextBox ID="txtReason" MaxLength="500" runat="server" TextMode="MultiLine" Height="50px"
                        CssClass=" txtBox" Width="200px"></asp:TextBox>
                </div>
            </asp:Panel>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:Panel runat="server" ID="pnlRejReason">
                <div class="lblBox LNOrient" style="width: 150px" id="lblRejectedReason">
                    Rejected Reason
                </div>
                <div class="divtxtBox LNOrient" style="width: 500px">
                    <asp:TextBox ID="txtRejectedReason" MaxLength="200" runat="server" Height="50px"
                        TextMode="MultiLine" CssClass=" txtBox" Width="200px"></asp:TextBox>
                </div>
                <div class="colspace LNOrient">
                    &nbsp</div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:ImageButton ID="btnPrint1" runat="server" ImageUrl="../Images/print-icon.png"
                        Visible="false" OnClick="btnPrint_Click" Width="35px" Height="25px" Style="border: 0px" />
                </div>
            </asp:Panel>
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm LNOrient">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment1_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" CssClass="ddlStype" Enabled="false"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment2_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" CssClass="ddlStype" Enabled="false"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment3_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" CssClass="ddlStype" Enabled="false"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment4_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" CssClass="ddlStype" Enabled="false"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment5_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" CssClass="ddlStype" Enabled="false"
                                    AutoPostBack="True">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divTaxPopup">
            <asp:HiddenField ID="hfTaxdet" runat="server" />
            <cc2:ModalPopupExtender ID="mptTaxDet" runat="server" TargetControlID="hfTaxdet"
                PopupControlID="pnlTaxDet" CancelControlID="btnTaxClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlTaxDet" runat="server" BackColor="Violet" Width="50%" ScrollBars="Auto">
                <div class="ConfirmForm LNOrient" style="height: 200px; overflow: scroll">
                    <table width="100%">
                        <tr>
                            <td class=" LNOrient">
                                <asp:HiddenField ID="hf_ItemNo" runat="server" Value="" />
                                <asp:HiddenField ID="hf_ReceitpNo" runat="server" Value="" />
                                <asp:GridView ID="gvTaxDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    DataKeyNames="INV_TAX_ID,TAX_ID,EntityNo,ItemNo" OnRowDataBound="gvTaxDet_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="TAX_NAME" HeaderText="Tax Name" />
                                        <asp:TemplateField HeaderText="Percentage">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTaxPercentage" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                    MaxLength="13" Text='<%# Eval("INV_TAX_PER") %>' Width="80px"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="fteTaxPercentage" runat="server" FilterType="Numbers,Custom"
                                                    TargetControlID="txtTaxPercentage" ValidChars="." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTaxAmount" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                    MaxLength="13" Text='<%# Eval("INV_TAX_AMT") %>' Width="80px"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="fteTaxAmount" runat="server" FilterType="Numbers,Custom"
                                                    TargetControlID="txtTaxAmount" ValidChars="." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:HiddenField ID="hf_TotTaxAmt" runat="server" Value="" />
                                <asp:Button ID="btnTaxOk" runat="server" Text="OK" OnClick="btnTaxOk_Click" />
                                &nbsp; &nbsp;
                                <asp:Button ID="btnTaxClose" runat="server" Text="Close" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="40" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="41" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="42" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="43" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
