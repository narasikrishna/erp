﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ReceiptEntry.aspx.cs" Inherits="FIN.Client.AP.ReceiptEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <table width="100%">
            <tr>
                <td class=" LNOrient">
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblWarehouseNumber">
                                Receipt Number
                            </div>
                            <div class="divtxtBox LNOrient" style="; width: 150px">
                                <asp:TextBox ID="txtPaymentNumber" runat="server" TabIndex="1" MaxLength="50" Enabled="false"
                                    CssClass="validate[required] RequiredField   txtBox"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblWarehouseName">
                                Receipt Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                    Width="100%" TabIndex="2" OnTextChanged="txtPaymentDate_TextChanged"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtPaymentDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblLineNumber">
                                Receipt Mode
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlPaymentMode" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                                    OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged" AutoPostBack="true"
                                    Width="100%">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGRNNumber">
                                Customer Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 310px">
                                <asp:DropDownList ID="ddlSupplierNumber" runat="server" TabIndex="3" CssClass="validate[required] RequiredField  ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGRNDate">
                                Customer Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 310px">
                                <asp:TextBox ID="txtSupplierName" Enabled="false" runat="server" MaxLength="50" TabIndex="4"
                                    CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPONumber">
                                Bank Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px; height: 22px;">
                                <asp:TextBox runat="server" ID="txtBank" CssClass="validate[required] RequiredField txtBox"
                                    TabIndex="6" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPOLineNumber">
                                Bank Branch
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox runat="server" ID="txtBankBranch" CssClass="validate[required] RequiredField txtBox"
                                    TabIndex="7" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                                Cheque Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox runat="server" ID="txtChequeNumber" CssClass="validate[required] RequiredField txtBox"
                                    TabIndex="8" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                                Value Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                                    Enabled="true" TabIndex="10"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtChequeDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                                Cheque Received By
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeReceivedBy" runat="server" CssClass="txtBox" TabIndex="9"
                                    Width="100%" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPaymentCurrency">
                                Receipt Currency
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlPaymentCurrency" runat="server" TabIndex="11" AutoPostBack="true"
                                    Width="100%" CssClass=" ddlStype" OnSelectedIndexChanged="ddlPaymentCurrency_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblExchangeRateType">
                                Exchange Rate Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlExchangeRateType" runat="server" TabIndex="12" CssClass="ddlStype"
                                    OnSelectedIndexChanged="ddlExchangeRateType_TextChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblExchangeRate">
                                Exchange Rate
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="13" CssClass="txtBox_N"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                                Receipt Amount
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtTotalPaymentAmt" runat="server" CssClass="validate[] txtBox_N"
                                    Enabled="false" Width="100%" TabIndex="14" MaxLength="13"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                    FilterType="Numbers,Custom" TargetControlID="txtTotalPaymentAmt" />
                            </div>
                            <div class="lblBox LNOrient" style="width: 350px;" id="Div15">
                                <asp:Label ID="lblWaitApprove" runat="server" Text="WAITING FOR APPROVAL" Visible="false"
                                    CssClass="lblBox" Font-Size="18px" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer LNOrient" runat="server" id="divgloacccode" visible="false">
                            <div class="lblBox" style="width: 150px" id="lblGlobalSegment">
                                Global Segment
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    Width="150px" TabIndex="16">
                                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                                Account Code
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlAccountCode" runat="server" TabIndex="17" Width="100%" CssClass=" ddlStype"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
                <td class=" LNOrient">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="imgBtnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnCancelReceipt" runat="server" ImageUrl="~/Images/stopPay.png"
                                    Style="border: 0px" onclick="imgBtnCancelReceipt_Click" />
                             
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" OnClientClick="fn_PostVisible('FINContent_imgBtnPost')" />
                                <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                            </td>
                        </tr>
                           
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnStopPayment" runat="server" ImageUrl="~/Images/stopPay.png"
                                    OnClick="imgStopPayment_Click" Style="border: 0px" />
                                <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnStopPayJV" runat="server" ImageUrl="~/Images/stopPayJV.png"
                                    Style="border: 0px" OnClick="imgBtnStopPayJV_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="divClear_10">
    </div>
    <asp:HiddenField runat="server" ID="hfSumreceipt_receivedamt" />
    <asp:HiddenField runat="server" ID="HFCustomerid" />
    <div class="LNOrient">
        <asp:Panel runat="server" ID="pnlgridview">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="line_no,CUST_PAY_DTL_ID,CUST_PAY_ID,CUST_INV_AMT,OM_INV_DATE,OM_INV_AMT,CUST_INV_ID,OM_INV_ID,sales_order_type"
                OnSelectedIndexChanged="gvData_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField HeaderText="Line No" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("line_no") %>'
                                TabIndex="27" Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                Width="50px" TabIndex="18"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("line_no") %>'
                                Width="50px"></asp:Label>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlSOType" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSOType_SelectedIndexChanged" TabIndex="28"
                                Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlSOType" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlSOType_SelectedIndexChanged" TabIndex="19"
                                Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSOTYpe" runat="server" Text='<%# Eval("SOT_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlInvoiceNumber" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceNumber_SelectedIndexChanged"
                                TabIndex="28" Width="150px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtOthers" runat="server" Visible="false" CssClass="txtBox" Text='<%# Eval("OM_INV_NUM") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlInvoiceNumber" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceNumber_SelectedIndexChanged"
                                TabIndex="19" Width="150px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtOthers" runat="server" Visible="false" CssClass="txtBox" Text='<%# Eval("OM_INV_NUM") %>'></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInvoiceNumber" runat="server" Text='<%# Eval("OM_INV_NUM") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInvoiceDate" MaxLength="20" runat="server" Text='<%# Eval("OM_INV_DATE","{0:dd/MM/yyyy}") %>'
                                TabIndex="29" Enabled="false" CssClass="EntryFont txtBox" Width="150px"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarssdExtender1"
                                TargetControlID="txtInvoiceDate" OnClientDateSelectionChanged="checkDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInvoiceDate" MaxLength="20" runat="server" CssClass="EntryFont txtBox"
                                TabIndex="20" Enabled="false" Width="150px"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="Calendas" TargetControlID="txtInvoiceDate"
                                OnClientDateSelectionChanged="checkDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Eval("OM_INV_DATE","{0:dd/MM/yyyy}") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInvoiceAmount" MaxLength="13" runat="server" Text='<%# Eval("OM_INV_AMT") %>'
                                Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="100px" TabIndex="30"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInvoiceAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Enabled="false" Width="100px" TabIndex="21"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInvoiceAmount" runat="server" Text='<%# Eval("OM_INV_AMT") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Retention Amount" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRetentionAmount" MaxLength="13" runat="server" Text='<%# Eval("OM_INV_RETENTION_AMT") %>'
                                Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="110px" TabIndex="31"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRetentionAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Enabled="false" Width="110px" TabIndex="22"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblrententionAmount" runat="server" Text='<%# Eval("OM_INV_RETENTION_AMT") %>'
                                Width="110px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount Received">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmountPaid" MaxLength="13" runat="server" Text='<%# Eval("CUST_INV_PAID_AMT") %>'
                                Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="110px" TabIndex="32"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtAmountPaid" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmountPaid" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Enabled="false" Width="100px" TabIndex="23"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtAmountPaid" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmountPaid" runat="server" Text='<%# Eval("CUST_INV_PAID_AMT") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Receipt Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmountToPay" Text='<%# Eval("CUST_INV_AMT") %>' runat="server"
                                MaxLength="13" CssClass="EntryFont RequiredField txtBox_N" Width="110px" TabIndex="33"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtensdsdder21" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtAmountToPay" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmountToPay" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                MaxLength="13" Width="100px" TabIndex="24"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="wwe" runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtAmountToPay" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmountToPay" runat="server" Text='<%# Eval("CUST_INV_AMT") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px" Visible="false" ControlStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click"
                                TabIndex="34" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click"
                                TabIndex="25" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="37" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="38" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="35" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="36" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="26" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </asp:Panel>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="30" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <div id="div6">
        <asp:HiddenField runat="server" ID="btnView1" />
        <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnView1"
            PopupControlID="Panel1" CancelControlID="Button2" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="Button2" Text="Close" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
