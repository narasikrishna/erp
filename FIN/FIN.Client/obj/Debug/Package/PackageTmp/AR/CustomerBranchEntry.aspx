﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CustomerBranchEntry.aspx.cs" Inherits="FIN.Client.AR.CustomerBranchEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierNumber">
                Customer Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlSupplierNumber" runat="server" AutoPostBack="true" TabIndex="1"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblSupplierName">
                Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 487px">
                <asp:TextBox ID="txtSupplierName" CssClass="validate[required] txtBox" runat="server"
                    Enabled="false" TabIndex="2"></asp:TextBox>
            </div>
           <%-- <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="Div1">
                Ship To
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtShipto" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Customer Status
            </div>
            <div class="divtxtBox LNOrient" style="width: 514px">
                <asp:DropDownList ID="ddlSupplierStatus" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 165px" id="Div4">
                Ship To
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipto" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblBranchShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBranchShortName" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 187px" id="lblBranchShortNameAR">
                Branch Short Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:TextBox ID="txtBranchShortNameAR" MaxLength="50" CssClass=" txtBox_ol" runat="server"
                    TabIndex="5"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 166px" id="Div2">
                Bill To
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBillto" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <%--<div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblSupplierStatus">
                Customer Status
            </div>
            <div class="divtxtBox" style="float: left; width: 527px">
                <asp:DropDownList ID="ddlSupplierStatus" runat="server" TabIndex="7" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress1" MaxLength="100" CssClass="validate[required] RequiredField txtBox" TextMode="MultiLine" Height="50px"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 188px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    runat="server" TabIndex="9" CssClass="validate[required]   RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 166px" id="lblPhone">
                Phone
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPhone" CssClass="txtBox" runat="server" TabIndex="10" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtPhone" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress2" MaxLength="100" CssClass=" txtBox" runat="server" TabIndex="11" TextMode="MultiLine" Height="50px"
                    OnTextChanged="txtAddress2_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 188px" id="lblState">
                State
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    TabIndex="12" CssClass="ddlStype" MaxLength="200">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 166px" id="lblEmail">
                Email
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px; height: 18px;">
                <asp:TextBox ID="txtEmail" MaxLength="20" CssClass="validate[custom[email]] txtBox"
                    runat="server" TabIndex="13"></asp:TextBox>
                <asp:RegularExpressionValidator ID="reqEmail" runat="server" ValidationGroup="btnSave"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* 
                                                    Email Id is Invalid Format</asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress3" MaxLength="100" CssClass=" txtBox" TextMode="MultiLine" Height="50px" runat="server" TabIndex="14"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 188px" id="lblCity">
                City
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="15" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 166px" id="lblFax">
                Fax
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFax" CssClass=" txtBox" runat="server" TabIndex="16" MaxLength="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtFax" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblURL">
                URL
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtURL" CssClass="txtBox" runat="server" TabIndex="17" MaxLength="50"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 188px" id="lblPostalCode">
                Postal Code
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPostalCode" MaxLength="13" CssClass="txtBox_N" runat="server"
                    TabIndex="17"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 166px" id="lblActive">
                Active
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="18" />
            </div>
        </div>
        <div class="divClear_10">
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                                ToolTip="save" TabIndex="19" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="20"
                                ToolTip="Delete" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="21"
                                ToolTip="Cancel" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="22"
                                ToolTip="Back" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table width="100%">
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
