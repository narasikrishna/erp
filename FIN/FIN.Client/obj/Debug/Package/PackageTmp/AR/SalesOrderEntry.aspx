﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SalesOrderEntry.aspx.cs" Inherits="FIN.Client.AR.SalesOrderEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
            <br />
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderNumber">
                Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 242px">
                <asp:TextBox ID="txtOrderNumber" MaxLength="50" CssClass="validate[required] RequiredField txtBox" Enabled="false"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 183px" id="lblOrderDate">
                Order Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtOrderDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtOrderDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtOrderDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblQuoteNumber">
                Quote Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 242px">
                <asp:DropDownList ID="ddlQuoteNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="3" OnSelectedIndexChanged="ddlQuoteNumber_SelectedIndexChanged" AutoPostBack="true"
                    Width="155px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 183px" id="lblOrderType">
                Order Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlOrderType" runat="server" CssClass="validate[]  ddlStype"
                    Width="155px" TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderCurrency">
                Order Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 242px">
                <asp:DropDownList ID="ddlOrderCurrency" runat="server" CssClass="validate[]   ddlStype"
                    Width="155px" TabIndex="5">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 183px" id="lblOrderStatus">
                Order Status
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="validate[] ddlStype"
                    Width="155px" TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblCustomerName">
                Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 242px">
                <asp:DropDownList ID="ddlCustomerName" runat="server" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged"
                    AutoPostBack="true" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                    Width="155px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 183px" id="Div2">
                Customer Site
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCustomerSite" runat="server" CssClass="validate[required] RequiredField ddlStype" Width="155px"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentDate">
                Shipment Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 244px">
                <asp:TextBox runat="server" ID="txtShipmentDate" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                    TabIndex="9" Width="150px"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtShipmentDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtShipmentDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 181px" id="Div1">
                Delivery Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtDeliveryDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="10" Width="150px"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender21"
                    TargetControlID="txtDeliveryDate" OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDeliveryDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div4">
                Exchange Rate
            </div>
            <div class="divtxtBox LNOrient" style="width: 240px">
                <asp:TextBox ID="txtExchangeRate" MaxLength="13" CssClass="txtBox_N" Text="1" runat="server"
                    Width="150px" TabIndex="11"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 181px" id="lblOrderAmount">
                Order Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtOrderAmount" TabIndex="12" MaxLength="13" CssClass="validate[] txtBox_N"
                    Enabled="false" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtOrderAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentMethod">
                Shipment Method
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentMethod" MaxLength="50" TabIndex="13" CssClass="validate[] txtBox"
                    Width="150px" runat="server" OnTextChanged="txtShipmentMethod_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <%--  <div class="lblBox" style="float: left; width: 190px" id="Div3">
                Lot Number
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlLotNumber" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="17"
                    Width="155px">
                </asp:DropDownList>
            </div>--%>
            <div class="lblBox LNOrient" style="width: 190px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:TextBox ID="txtComments" MaxLength="500" TabIndex="14" CssClass="validate[] txtBox"
                    TextMode="MultiLine" Height="25px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblBillingAddress">
                Billing Address
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:TextBox ID="txtBillingAddress" MaxLength="200" TabIndex="15" CssClass="validate[] txtBox"
                    TextMode="MultiLine" Height="25px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblShippingAddress">
                Shipping Address
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:TextBox ID="txtShippingAddress" MaxLength="200" TabIndex="16" CssClass="validate[] txtBox"
                    TextMode="MultiLine" Height="25px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="width: 510px">
                <asp:TextBox ID="txtRemarks" MaxLength="500" CssClass=" txtBox" TextMode="MultiLine"
                    Height="25px" runat="server" TabIndex="17"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            DataKeyNames="ITEM_ID,UOM_CODE,DELETED,OM_ORDER_ID,OM_ORDER_DTL_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
            OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
            OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
            ShowFooter="True">
            <Columns>
                <asp:TemplateField HeaderText="Line No" Visible="false">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtLineNo" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>'
                            Enabled="true" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtLineNo" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                            Enabled="false" Width="100px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblLineNumn" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>'
                            Visible="false" Width="130px"></asp:Label>
                        <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                            Width="130px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlItemName" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            AutoPostBack="True" Width="140px" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged"
                            TabIndex="27">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlItemName" TabIndex="19" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            AutoPostBack="True" Width="140px" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblItem" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="140px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDescription" TabIndex="28" runat="server" Text='<%# Eval("OM_ORDER_DESC") %>'
                            MaxLength="200" CssClass="EntryFont RequiredField txtBox" Width="140px"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="EntryFont RequiredField txtBox"
                            MaxLength="200" TabIndex="20" Width="140px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("OM_ORDER_DESC") %>'
                            Width="140px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("OM_ORDER_QTY") %>'
                            CssClass="EntryFont RequiredField txtBox_N" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged"
                            TabIndex="29" Width="100px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtQuantity" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtQuantity" TabIndex="21" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                            AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged" Width="100px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtQuantity" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("OM_ORDER_QTY") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Unit of Measurement">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtuom" MaxLength="13" Enabled="false" runat="server" Text='<%# Eval("UOM_NAME") %>'
                            CssClass="EntryFont RequiredField txtBox" 
                            TabIndex="30" Width="100px"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtuom" TabIndex="22" Enabled="false" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox"
                             Width="100px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM_NAME") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
               <%-- <asp:TemplateField HeaderText="UOM">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlUOM" TabIndex="30" Width="140px" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlUOM" TabIndex="22" Width="140px" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM_NAME") %>' Width="140px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Unit Price">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUnitPrice" TabIndex="31" runat="server" Text='<%# Eval("OM_ORDER_PRICE") %>'
                            MaxLength="13" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                            Width="130px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtUnitPrice" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                            MaxLength="13" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged" Enabled="true"
                            TabIndex="23" Width="130px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtUnitPrice" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("OM_ORDER_PRICE") %>'
                            Width="130px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Amount">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtAmount" runat="server" Enabled="false" Text='<%# Eval("AMOUNT") %>'
                            MaxLength="32" CssClass="EntryFont RequiredField txtBox_N" TabIndex="24" Width="130px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtAmount" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtAmount" TabIndex="24" runat="server" Enabled="false" CssClass="EntryFont RequiredField txtBox_N"
                            MaxLength="13" Width="130px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtAmount" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("AMOUNT") %>' Width="130px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkAct" TabIndex="33" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:CheckBox ID="chkAct" TabIndex="25" Checked="true" runat="server" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkAct" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                            Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Add / Edit">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" TabIndex="34" runat="server" AlternateText="Edit"
                            CausesValidation="false" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" TabIndex="35" runat="server" AlternateText="Delete"
                            CausesValidation="false" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" TabIndex="36" runat="server" AlternateText="Update"
                            CommandName="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" TabIndex="37" runat="server" AlternateText="Cancel"
                            CausesValidation="false" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" TabIndex="26" runat="server" AlternateText="Add"
                            CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="11" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm" align="center">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
