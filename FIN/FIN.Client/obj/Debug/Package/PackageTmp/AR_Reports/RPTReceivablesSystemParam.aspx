﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTReceivablesSystemParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTReceivablesSystemParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                    Global Segment
                </div>
                <div class="divtxtBox LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblCustomerName">
                    From Customer
                </div>
                <div class="divtxtBox LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlFromCustomer" runat="server" TabIndex="2" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
                <div class="colspace LNOrient" style="width: 20px;">
                </div>
                <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceType">
                    To Customer
                </div>
                <div class="divtxtBox LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlToCustomer" runat="server" TabIndex="3" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="Div8">
                    From Debit Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtDebitFromAmt" runat="server" TabIndex="4" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtDebitFromAmt" />
                </div>
                <div class="colspace LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 130px" id="Div9">
                    To Debit Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtDebitToAmt" runat="server" TabIndex="5" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtDebitToAmt" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="Div10">
                    From Credit Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtCreditFromAmt" runat="server" TabIndex="6" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtCreditFromAmt" />
                </div>
                <div class="colspace LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 130px" id="Div11">
                    To Credit Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtCreditToAmt" runat="server" TabIndex="7" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtCreditToAmt" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblDate">
                    As on Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="8" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="Div14">
                    Invoice Status
                </div>
                <div class="divtxtBox LNOrient" style="width: 550px" align="center">
                    <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                        <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="C">Cancelled &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="IU">All &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td>
                                <%-- <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                                    <img src="../Images/MainPage/settings-icon_B.png" alt="Show Report" width="15px"
                                        height="15px" title="Show Report" />--%>
                                <%-- </a>--%>
                                <%-- <asp:Button ID="btnSave1" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" Visible="false" />--%>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                            <%--<td>
                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>