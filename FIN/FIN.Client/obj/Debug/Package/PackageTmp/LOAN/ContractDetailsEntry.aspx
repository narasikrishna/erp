﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ContractDetailsEntry.aspx.cs" Inherits="FIN.Client.LOAN.ContractDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyType">
                Facility ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlPropertyType" runat="server" TabIndex="1" CssClass="ddlStype">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Complex</asp:ListItem>
                    <asp:ListItem Value="2">Residential Values</asp:ListItem>
                    <asp:ListItem Value="3">Independent Houses</asp:ListItem>
                    <asp:ListItem Value="4">Independent Villas</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyID">
                Property Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlPropertyID" runat="server" TabIndex="2" CssClass="ddlStype RequiredField validate[required]">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblLoanPurpose">
                Purpose of Loan
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 400px">
                <asp:TextBox ID="txtLoanPurpose" runat="server" TextMode="MultiLine" TabIndex="3"
                    MaxLength="1000" Height="59px" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:HiddenField runat="server" ID="hdBankId" /> <asp:HiddenField runat="server" ID="hdPayId" />
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField runat="server" ID="hf_UnitPrice" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="800px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="LN_REQUEST_ID,LN_CONTRACT_ID,LN_PARTY_ID,LN_CURRENCY_ID,LN_CONTRACT_STATUS">
                <Columns>
                    <asp:TemplateField HeaderText="Contract Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCN" MaxLength="50" Enabled="true" runat="server" Text='<%# Eval("LN_CONTRACT_NUM") %>'
                                TabIndex="19" CssClass="EntryFont RequiredField txtBox" Width="80px"></asp:TextBox>
                            <%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtUOM"/>--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCN" MaxLength="50" Enabled="true" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="4" Width="80px"></asp:TextBox><%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender66"
                                    runat="server" TargetControlID="txtUOM" />--%>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCN" Style="word-wrap: break-word" runat="server" Text='<%# Eval("LN_CONTRACT_NUM") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contract Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtContractDesc" MaxLength="500" runat="server" Text='<%# Eval("LN_CONTRACT_DESC") %>'
                                TabIndex="20" CssClass="EntryFont RequiredField txtBox" Width="150px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtContractDesc" MaxLength="500" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="5" Width="150px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblContractDesc" Style="word-wrap: break-word" runat="server" Text='<%# Eval("LN_CONTRACT_DESC") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bank Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="21">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="6">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBankName" runat="server" Text='<%# Eval("BANK_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contract Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpContractDate" TabIndex="22" MaxLength="10" runat="server" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                Text='<%#  Eval("LN_CONTRACT_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpContractDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpContractDate" TabIndex="7" MaxLength="10" runat="server" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender19" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpContractDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblContractDate" runat="server" Text='<%# Eval("LN_CONTRACT_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contract Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCT" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="21">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCT" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="6">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCT" runat="server" Text='<%# Eval("LN_CONTRACT_STATUS_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contract Status">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlContractStatus" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="23">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlContractStatus" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblContractStatus" runat="server" Text='<%# Eval("LN_CONTRACT_STATUS_DESC") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Currency">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="24">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="9">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("CURRENCY_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Principal Amount" HeaderStyle-HorizontalAlign="Right">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPrincipalAmt" MaxLength="10" runat="server" Text='<%# Eval("LN_PRINCIPAL_AMOUNT") %>'
                                TabIndex="25" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xgtender277" runat="server" FilterType="Numbers,custom"
                                TargetControlID="txtPrincipalAmt" ValidChars=".," />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPrincipalAmt" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="10" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredText3BoxEfxtender24"
                                    FilterType="Numbers,custom" runat="server" ValidChars=".," TargetControlID="txtPrincipalAmt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPrincipalAmt" runat="server" Text='<%# Eval("LN_PRINCIPAL_AMOUNT") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profit %" HeaderStyle-HorizontalAlign="Right">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProfit" MaxLength="10" runat="server" Text='<%# Eval("LN_ROI") %>'
                                TabIndex="26" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xgtender217" runat="server" FilterType="Numbers,custom"
                                TargetControlID="txtProfit" ValidChars=".," />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtProfit" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="11" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender213"
                                    FilterType="Numbers,custom" ValidChars=".," runat="server" TargetControlID="txtProfit" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProfit" runat="server" Text='<%# Eval("LN_ROI") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Principal Amount Repayment Term" HeaderStyle-HorizontalAlign="Right">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPrincipalAmtRT" MaxLength="10" runat="server" Text='<%# Eval("LN_PRINCIPAL_AMOUNT") %>'
                                TabIndex="25" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xgtender775" runat="server" FilterType="Numbers,custom"
                                TargetControlID="txtPrincipalAmtRT" ValidChars=".," />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPrincipalAmtRT" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="10" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredText3BoxEfxtender245"
                                    FilterType="Numbers,custom" runat="server" ValidChars=".," TargetControlID="txtPrincipalAmtRT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPrincipalAmtRT" runat="server" Text='<%# Eval("LN_PRINCIPAL_AMOUNT") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profit % Repayment Term" HeaderStyle-HorizontalAlign="Right">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProfitRT" MaxLength="10" runat="server" Text='<%# Eval("LN_ROI") %>'
                                TabIndex="26" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xgtender271" runat="server" FilterType="Numbers,custom"
                                TargetControlID="txtProfitRT" ValidChars=".," />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtProfitRT" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="11" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender231"
                                    FilterType="Numbers,custom" ValidChars=".," runat="server" TargetControlID="txtProfitRT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProfitRT" runat="server" Text='<%# Eval("LN_ROI") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Effective From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="27" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Text='<%#  Eval("LN_START_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender19" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="12" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender119" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("LN_START_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="28" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Text='<%#  Eval("LN_END_DATE","{0:dd/MM/yyyy}") %>' Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender110" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="13" MaxLength="10" runat="server" CssClass="RequiredField EntryFont   txtBox"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender210" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("LN_END_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="29" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="14" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Post">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Style="border: 0px"
                                OnClick="imgBtnPost_Click" />
                            <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox  LNOrient"
                                Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                        </ItemTemplate>
                    </asp:TemplateField> <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Term Details" CommandName="btnPop"
                                TabIndex="30" Visible="false" Enabled="false" OnClick="btnTerm_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Term Details" CommandName="btnPop"
                                TabIndex="15" Visible="false" OnClick="btnTerm_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Term Details" CommandName="btnPop"
                                TabIndex="12" Visible="false" OnClick="btnTerm_Click" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="17" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="18" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="31" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="32" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="16" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        <%--OnClick="btnDelete_Click" />--%>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div1">
            <asp:HiddenField runat="server" ID="HFQtyfromMI" />
            <asp:HiddenField runat="server" ID="HFlotqty" />
            <asp:HiddenField runat="server" ID="hf_qty" />
            <asp:HiddenField runat="server" ID="hdLot" />
            <asp:HiddenField runat="server" ID="hdItemIndex" />
            <cc2:ModalPopupExtender ID="mpeReceipt" runat="server" TargetControlID="hdLot" PopupControlID="pnlpopup"
                BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlpopup" runat="server" Width="750px" Height="400px">
                <div class="ConfirmForm" style="overflow: auto;">
                    <table>
                        <tr class="ConfirmLotHeading">
                            <td style="height: 300px;" valign="top">
                                <div class="divRowContainer" align="left">
                                    <asp:GridView ID="gvLot" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" DataKeyNames="LOT_ID,INV_WH_ID,ITEM_ID,DELETED" OnRowCancelingEdit="gvLot_RowCancelingEdit"
                                        OnRowCommand="gvLot_RowCommand" OnRowCreated="gvLot_RowCreated" OnRowDataBound="gvLot_RowDataBound"
                                        OnRowDeleting="gvLot_RowDeleting" OnRowEditing="gvLot_RowEditing" OnRowUpdating="gvLot_RowUpdating"
                                        ShowFooter="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Warehouse">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlWH" runat="server" CssClass="ddlStype" Width="150px" TabIndex="1">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlWH" runat="server" CssClass="ddlStype" Width="150px" TabIndex="1">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWH" Width="170px" runat="server" Text='<%# Eval("INV_WH_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lot">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlLot" runat="server" CssClass="ddlStype" Width="150px" TabIndex="2">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlLot" runat="server" CssClass="ddlStype" Width="150px" TabIndex="2">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLotID" Width="170px" runat="server" Text='<%# Eval("LOT_NUMBER") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Quantity">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtQty" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                                                        runat="server" TabIndex="3" Text='<%# Eval("LOT_QTY") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilterwqeddTextBoxExtender21" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtQty" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtQty" MaxLength="13" CssClass=" RequiredField txtBox_N" runat="server"
                                                        TabIndex="3"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FielteryedTefxtBoxExtender21" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtQty" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQty" Width="170px" runat="server" Text='<%# Eval("LOT_QTY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                        TabIndex="6" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                        TabIndex="7" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                        TabIndex="6" Height="20px" ImageUrl="~/Images/Update.png" />
                                                    <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                        TabIndex="7" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                        TabIndex="8" ImageUrl="~/Images/Add.jpg" />
                                                </FooterTemplate>
                                                <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLotError" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnLotNo" OnClick="btnTermNo_Click"
                                    Text="Close" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
