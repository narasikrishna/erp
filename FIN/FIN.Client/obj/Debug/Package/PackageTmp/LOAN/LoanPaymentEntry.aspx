﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanPaymentEntry.aspx.cs" Inherits="FIN.Client.LOAN.LoanPaymentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 100px" id="Div1">
                Bank
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBank" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" style="float: left">
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 100px;" id="lblRequestDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 75px" id="Div2">
                <asp:Button ID="btnProcess" runat="server" Text="Process" CssClass="btn" TabIndex="3"
                    OnClick="btnProcess_Click" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 75px" id="Div3">
                <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="btn" TabIndex="4" Visible="false" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
         <div class="divRowContainer" align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="LN_INSTALLMENT_NO,LN_INSTALLMENT_DT,LN_AMOUNT,LN_REVISED_PS_AMT,LN_PAID_AMT,LN_PRN_REPAY_DTL_ID"
                Width="700px" ShowFooter="false">
                <Columns>
                 <asp:BoundField DataField="LN_INSTALLMENT_NO" HeaderText="Contract Number" ItemStyle-Width="300px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                     <asp:BoundField DataField="LN_INSTALLMENT_NO" HeaderText="Contract Name" ItemStyle-Width="300px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_INSTALLMENT_NO" HeaderText="Installment Number" ItemStyle-Width="300px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_INSTALLMENT_DT" HeaderText="Installment Date" ItemStyle-Width="200px" DataFormatString="{0:dd/MM/yyyy}"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_AMOUNT" HeaderText="Installment Amount" ItemStyle-Width="200px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_REVISED_PS_AMT" HeaderText="Profit Share Amount" ItemStyle-Width="200px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Pay Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPayAmt" TabIndex="5" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("LN_PAID_AMT") %>' Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtPayAmt" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pay Profit Share Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPayProfitShareAmt" TabIndex="6" MaxLength="13" runat="server"
                                CssClass="EntryFont RequiredField txtBox_N" Text='<%# Eval("LS_PS_PAID_AMT") %>'
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FiltereddTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtPayProfitShareAmt" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pay Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPayDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                TabIndex="7" Text='<%#  Eval("LN_PAID_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendardExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtPayDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTexsdtBoxExtender5" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtPayDate" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="26"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="28" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
