﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PropertyRevaluationEntry.aspx.cs" Inherits="FIN.Client.LOAN.PropertyRevaluationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
         <table>
            <tr>
                <td>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblGRNNumber">
                Valuation ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 180px">
                <asp:TextBox ID="txtvaluation" Enabled="false" runat="server" TabIndex="1" CssClass=" RequiredField txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyId">
                Property ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:DropDownList ID="ddlPropertyID" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    Width="300px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Revaluation Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:DropDownList ID="ddlRevalOrgType" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    Width="300px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Person/Organization
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtPerOrg" runat="server" Enabled="true" TabIndex="3" 
                    CssClass=" RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblReqDescription">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox runat="server" ID="chkActive" Checked="true" TabIndex="4" />
            </div>
        </div>
        </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer " align="left">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="700px"
                    CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                    OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                    OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                    DataKeyNames="REVAL_DTL_ID,REVAL_TYPE,DELETED">
                    <Columns>
                        <asp:TemplateField HeaderText="Revaluation Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRevalDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox" TabIndex="13"
                                    Text='<%#  Eval("REVAL_DT","{0:dd/MM/yyyy}") %>' Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtRevalDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtRevalDate" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRevalDate" TabIndex="5" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                    Onkeypress="return DateKeyCheck(event,this);" Width="130px"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtRevalDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtRevalDate" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRevalDate" runat="server" Text='<%# Eval("REVAL_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revaluation Type">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlRevalType" TabIndex="14" runat="server" CssClass="RequiredField ddlStype"
                                    Width="280px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlRevalType" TabIndex="6" runat="server" CssClass="RequiredField ddlStype"
                                    Width="280px">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRevalType" runat="server" Text='<%# Eval("REVAL_TYPE") %>' Width="350px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revaluation Charge">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRevalCharge" TabIndex="15" MaxLength="13" runat="server" Text='<%# Eval("REVAL_CHARGE") %>'
                                    CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                        TargetControlID="txtRevalCharge" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRevalCharge" TabIndex="7" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="150"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                        runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtRevalCharge" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRevalCharge" runat="server" Text='<%# Eval("REVAL_CHARGE") %>'
                                    Width="99%"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRemarks" TabIndex="16" MaxLength="200" runat="server" CssClass=" txtBox"
                                    Text='<%# Eval("REVAL_REMARKS") %>' Width="350px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRemarks" MaxLength="200" TabIndex="8" runat="server" CssClass="txtBox"
                                    Width="350px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("REVAL_REMARKS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkact" TabIndex="17" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="chkact" TabIndex="9" runat="server" Checked="true" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" TabIndex="11"
                                    ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="12"
                                    ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" TabIndex="18"
                                    ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="19"
                                    ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" TabIndex="10"
                                    ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
