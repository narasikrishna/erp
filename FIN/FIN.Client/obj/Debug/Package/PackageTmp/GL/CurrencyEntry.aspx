﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CurrencyEntry.aspx.cs" Inherits="FIN.Client.GL.CurrencyEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblCurrencyCode">
                Currency Code
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCurrencyCode"  CssClass="validate[required] RequiredField txtBox"
                    MaxLength="5" runat="server" TabIndex="1" ></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblCurrencyDescription">
                Currency Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCurrencyDescription" CssClass="txtBox_en" MaxLength="100" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblCurrencyDescriptionol">
                Currency Description(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCurrencyDescriptionol" CssClass="txtBox_ol" MaxLength="100" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblCurrencySymbol">
                Currency Symbol
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCurrencySymbol" CssClass="txtBox" MaxLength="10" runat="server"
                    OnTextChanged="txtCurrencySymbol_TextChanged" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblInitialAmountSeparator">
                Initial Amount Separator
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlInitialAmountSeparator" MaxLength="50" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4" Width="155px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblSubsequentAmountSeparator">
                Subsequent Amount Separator
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlSubsequentAmountSeparator" MaxLength="50" runat="server"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="5" Width="155px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDecimalPrecision">
                Decimal Precision
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <%-- <asp:TextBox ID="txtDecimalPrecision" MaxLength="10" CssClass=" txtBox_N" 
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtDecimalPrecision" />--%>
                <asp:DropDownList ID="ddldecpre" MaxLength="50" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="6" Width="155px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDecimalSeperator">
                Decimal Seperator
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
               <%-- <asp:TextBox ID="txtDecimalSeperator" CssClass=" txtBox_N" MaxLength="10" runat="server"
                    TabIndex="6"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtDecimalSeperator" />--%>
                     <asp:DropDownList ID="ddldecsep" MaxLength="50" runat="server"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="7" Width="155px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox" style="  width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
     <script type="text/javascript">
          $(document).ready(function () {
              fn_changeLng('<%= Session["Sel_Lng"] %>');
          });

          $(document).ready(function () {
              $("#form1").validationEngine();
              return fn_SaveValidation();
          });

          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_endRequest(function () {
              return fn_SaveValidation();
          });

          function fn_SaveValidation() {
              $("#FINContent_btnSave").click(function (e) {
                  //e.preventDefault();
                  return $("#form1").validationEngine('validate')
              })
          }

    </script>
</asp:Content>
