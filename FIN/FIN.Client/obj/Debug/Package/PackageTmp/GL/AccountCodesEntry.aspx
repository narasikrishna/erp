﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AccountCodesEntry.aspx.cs" Inherits="FIN.Client.GL.AccountCodes" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 780px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblStructureName">
                Structure Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 230px">
                <asp:DropDownList ID="ddlStructureName" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    OnSelectedIndexChanged="ddlStructureName_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
            
            
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblGroupName">
                Group Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 600px">
                <asp:DropDownList ID="ddlGroupName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlGroupName_SelectedIndexChanged" TabIndex="2" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
         <div class="divRowContainer">
         <div class="lblBox LNOrient" style="  width: 130px" id="lblAccountCode">
                Account Code
            </div>
            <div class="divtxtBox LNOrient" style="  width: 230px">
                <asp:TextBox ID="txtAccountCode" CssClass="validate[required] RequiredField txtBox"
                    runat="server" OnTextChanged="txtAccountCode_TextChanged" TabIndex="3" MaxLength="50"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 170px" id="Div1">
                Old Account Code
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtoldAcctcde" CssClass="validate[] txtBox" runat="server" TabIndex="4"
                    MaxLength="240"></asp:TextBox>
            </div>
         </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblAccountCodeDescription">
                Account Code Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 605px">
                <asp:TextBox ID="txtAccountCodeDescription" CssClass="validate[required] RequiredField txtBox"
                    runat="server" MaxLength="250" TabIndex="5" OnTextChanged="txtAccountCodeDescription_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblAccountCodeDescriptionAR">
                Account Code Description(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 605px">
                <asp:TextBox ID="txtAccountCodeDescriptionAR" CssClass="txtBox_ol" MaxLength="240"
                    runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblAccountType">
                Account Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 230px">
                <asp:RadioButtonList ID="rbAccType" runat="server" CssClass="lblBox LNOrient" RepeatDirection="Horizontal"
                    TabIndex="7">
                    <asp:ListItem Value="B" Selected="True">BalanceSheet</asp:ListItem>
                    <asp:ListItem Value="P">Profit /Loss</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblControlAccount">
                Control Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 230px">
                <asp:RadioButtonList ID="rbControlAcc" runat="server" CssClass="lblBox LNOrient" RepeatDirection="Horizontal"
                    OnSelectedIndexChanged="rbControlAcc_SelectedIndexChanged" TabIndex="8" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                    <asp:ListItem Selected="False" Value="N">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 170px" id="lblControlAccountCategory">
                Control Account Category
            </div>
            <div class="divtxtBox LNOrient" style="  width: 176px">
                <asp:DropDownList ID="ddlControlAccountCategory" runat="server" CssClass="NRddlStype"
                    TabIndex="9">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:TextBox ID="txtEffectiveDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="10"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender13" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtEffectiveDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffectiveDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 110px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    runat="server" TabIndex="11"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender14" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtEndDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
              <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox LNOrient" style="  width: 110px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="12" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
           
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
            OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
            OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
            OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
            DataKeyNames="SEGMENT_ID,ACCT_CODE_SEG_ID" OnSelectedIndexChanged="gvData_SelectedIndexChanged">
            <Columns>
                <asp:TemplateField HeaderText="Segment Name" FooterStyle-Width="400px">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlSegment" TabIndex="13" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlSegment" TabIndex="13" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSegmentName" runat="server" Text='<%# Eval("SEGMENT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Date">
                    <EditItemTemplate>
                        <asp:TextBox ID="dtpFromDate" TabIndex="14" runat="server" CssClass="EntryFont RequiredField txtBox"
                            Text='<%#  Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                        </cc2:CalendarExtender>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                            FilterType="Numbers,Custom" TargetControlID="dtpFromDate" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dtpFromDate" TabIndex="14" runat="server" CssClass="EntryFont RequiredField txtBox"
                            Width="80px"></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                        </cc2:CalendarExtender>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                            FilterType="Numbers,Custom" TargetControlID="dtpFromDate" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="80px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <EditItemTemplate>
                        <asp:TextBox ID="dtpToDate" TabIndex="15" runat="server" CssClass="EntryFont  txtBox"
                            Text='<%#  Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtender11" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="dtpToDate">
                        </cc2:CalendarExtender>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                            FilterType="Numbers,Custom" TargetControlID="dtpToDate" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dtpToDate" TabIndex="15" runat="server" CssClass="EntryFont txtBox"
                            Width="80px"></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="dtpToDate">
                        </cc2:CalendarExtender>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                            FilterType="Numbers,Custom" TargetControlID="dtpToDate" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="80px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkact" TabIndex="16" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:CheckBox ID="chkact" Checked="true" TabIndex="16" runat="server" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"  TabIndex="18"
                            CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"  TabIndex="19"
                            CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"  TabIndex="20"
                            Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"  TabIndex="21"
                            CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" TabIndex="17"
                            ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                        TabIndex="15" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="16" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="17" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="18" />
                </td>
            </tr>
        </table>
    </div>
    <div class="divClear_10">
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();

                
                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
