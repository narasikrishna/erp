﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="FinancialTemplate_Segment.aspx.cs" Inherits="FIN.Client.GL.FinancialTemplate_Segment" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#FINContent_hf_BS_TMPL_GROUP_ID").val(str_tmpl_id);
            $("#FINContent_PARENT_GROUP_NAME").val(str_Group_name);
            $("#FINContent_lblSelectedGroup").text("Selected Group : " + str_Group_name);
            $("#btnSelectData").click();

        }

        function fn_showGraph() {
            $("#BSTemp").orgChart({ container: $("#main") });
            // addEvents();
            fn_JqueryTreeView();
        }

        $(window).bind("load", function () {
            $("#<%=btnGet.ClientID %>").click();
        });
    </script>
    <link rel="stylesheet" href="../Scripts/ORGChart/jquery.orgchart.css" />
    <script type="text/javascript" src="../Scripts/ORGChart/jquery.orgchart.js"></script>
    <style type="text/css">
        ul.LinkedList
        {
            display: block;
        }
        /* ul.LinkedList ul { display: none; } */
        .HandCursorStyle
        {
            cursor: pointer;
            cursor: hand;
        }
        /* For IE */
    </style>
    <script type="text/JavaScript">
        // Add this to the onload event of the BODY element
        function addEvents() {
            activateTree(document.getElementById("BSTemp"));
        }

        // This function traverses the list and add links 
        // to nested list items
        function activateTree(oList) {
            // Collapse the tree
            for (var i = 0; i < oList.getElementsByTagName("ul").length; i++) {
                oList.getElementsByTagName("ul")[i].style.display = "none";
            }
            // Add the click-event handler to the list items
            if (oList.addEventListener) {
                oList.addEventListener("click", toggleBranch, false);
            } else if (oList.attachEvent) { // For IE
                oList.attachEvent("onclick", toggleBranch);
            }
            // Make the nested items look like links
            addLinksToBranches(oList);
        }

        // This is the click-event handler
        function toggleBranch(event) {
            var oBranch, cSubBranches;
            if (event.target) {
                oBranch = event.target;
            } else if (event.srcElement) { // For IE
                oBranch = event.srcElement;
            }
            cSubBranches = oBranch.getElementsByTagName("ul");
            if (cSubBranches.length > 0) {
                if (cSubBranches[0].style.display == "block") {
                    cSubBranches[0].style.display = "none";
                } else {
                    cSubBranches[0].style.display = "block";
                }
            }
        }

        // This function makes nested list items look like links
        function addLinksToBranches(oList) {
            var cBranches = oList.getElementsByTagName("li");
            var i, n, cSubBranches;
            if (cBranches.length > 0) {
                for (i = 0, n = cBranches.length; i < n; i++) {
                    cSubBranches = cBranches[i].getElementsByTagName("ul");
                    if (cSubBranches.length > 0) {
                        addLinksToBranches(cSubBranches[0]);
                        cBranches[i].className = "HandCursorStyle";
                        cBranches[i].style.color = "blue";
                        cSubBranches[0].style.color = "black";
                        cSubBranches[0].style.cursor = "auto";
                    }
                }
            }
        }
    </script>
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 130px" id="lblStructureName">
                Template Name
            </div>
            <div class="divtxtBox" style="float: left; width: 500px">
                <asp:TextBox ID="txtTempName" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1" MaxLength="500" AutoPostBack="True" OnTextChanged="txtTempName_TextChanged"></asp:TextBox>
                <asp:Button ID="btnGet" runat="server" Text="Get" OnClick="btnGet_Click" Style="display: none" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <table width="100%" border="1">
                <tr>
                    <td style="width: 50%;" valign="top">
                        <div id="div_Template" runat="server" class="tree">
                        </div>
                    </td>
                    <td style="width: 50%" valign="top">
                        <div class="divRowContainer">
                            <asp:Label ID="lblSelectedGroup" runat="server" Text="" CssClass="lblBox"></asp:Label>
                            &nbsp;
                            <asp:ImageButton ID="imgBtnEdit" runat="server" ImageUrl="~/Images/edit.png" AlternateText="Edit"
                                ToolTip="Edit" OnClick="imgBtnEdit_Click" />
                            <div style="display: none">
                                <asp:Button ID="btnSelectData" runat="server" Text="Select Data" OnClick="btnSelectData_Click"
                                    ClientIDMode="Static" />
                            </div>
                        </div>
                        <div class="divClear_10" style="width: 30%">
                        </div>
                        <div class="divRowContainer" align="center">
                            <asp:RadioButtonList ID="rbl_Type" runat="server" CssClass="lblBox" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rbl_Type_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="GROUP">Group</asp:ListItem>
                                <asp:ListItem Value="ACCOUNT">Account</asp:ListItem>
                                <asp:ListItem Value="SEGMENT">Segment</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div id="div_Group" runat="server" visible="true">
                            <div class="divRowContainer">
                                <div class="lblBox" style="float: left; width: 100px" id="Div1">
                                    Group name
                                </div>
                                <div class="divtxtBox" style="float: left; width: 330px">
                                    <asp:TextBox ID="txtGroupName" CssClass="validate[required] RequiredField txtBox"
                                        runat="server" TabIndex="2" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10" style="width: 30%">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox" style="float: left; width: 100px" id="Div2">
                                    Group Number
                                </div>
                                <div class="divtxtBox" style="float: left; width: 50px">
                                    <asp:TextBox ID="txtGroupNumber" CssClass="validate[required] RequiredField txtBox"
                                        runat="server" TabIndex="3" MaxLength="5"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="txtGroupNumber" />
                                </div>
                                <div class="lblBox" style="float: left; width: 50px" id="Div9">
                                    Notes
                                </div>
                                <div class="divtxtBox" style="float: left; width: 50px">
                                    <asp:TextBox ID="txtNotes" CssClass="txtBox" runat="server" TabIndex="3" MaxLength="3"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                        TargetControlID="txtNotes" />
                                </div>
                                <div class="lblBox" style="float: left; width: 100px" id="Div3">
                                    Total Required
                                </div>
                                <div class="divtxtBox" style="float: left; width: 20px">
                                    <asp:CheckBox ID="chkTotReq" runat="server" Text=" " AutoPostBack="True" OnCheckedChanged="chkTotReq_CheckedChanged" />
                                </div>
                                <div class="lblBox" style="float: left; width: 70px" id="Div5">
                                    Formula
                                </div>
                                <div class="divtxtBox" style="float: left; width: 20px">
                                    <asp:CheckBox ID="chkFormulaReq" runat="server" Text=" " AutoPostBack="True" OnCheckedChanged="chkFormulaReq_CheckedChanged" />
                                </div>
                            </div>
                            <div class="divClear_10" style="width: 30%">
                            </div>
                            <div class="divRowContainer" id="div_Formula" runat="server" visible="false">
                                <div class="divRowContainer">
                                    <div class="lblBox" style="float: left; width: 100px" id="Div6">
                                        Group NUmber
                                    </div>
                                    <div class="divtxtBox" style="float: left; width: 80px">
                                        <asp:DropDownList ID="ddlGroupNuber" runat="server" CssClass="ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox" style="float: left; width: 80px" id="Div7">
                                        Operator
                                    </div>
                                    <div class="divtxtBox" style="float: left; width: 80px">
                                        <asp:DropDownList ID="ddlOperator" runat="server" CssClass=" ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox" style="float: left; width: 100px" id="Div8">
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 30%">
                                </div>
                                <div class="divRowContainer">
                                    <asp:TextBox ID="txtFormula" CssClass="txtBox" runat="server" Text=""></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="divClear_10" style="width: 30%">
                        </div>
                        <div id="div_acc" runat="server" visible="false">
                            <div class="divRowContainer">
                                <div style="float: left; width: 45%; height:500px; overflow:auto">
                                    <div style="float: left; width: 100%">
                                        <asp:TextBox ID="txtSearchAcct" runat="server" Text="" CssClass="txtBox" OnTextChanged="txtSearchAcct_TextChanged"></asp:TextBox>
                                    </div>
                                    <div class="divClear_10" style="width: 10%">
                                    </div>
                                    <div style="float: left; width: 100%">
                                        <asp:GridView ID="gv_All_Acct" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                            DataKeyNames="CODE_ID,SELECTED_REC,CODE_NAME" OnRowDataBound="gv_All_Acct_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Accoount Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("CODE_NAME")%></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkAll_Acct" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div style="float: left; width: 10%">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_AllAdd" runat="server" ImageUrl="~/Images/NextFast_Icon.png"  style="display:none"
                                                    AlternateText="ALL" ToolTip="Move All the Record" OnClick="img_AllAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_OneAdd" runat="server" ImageUrl="~/Images/Next_Icon.png"
                                                    AlternateText="One" ToolTip="Move Selected Record" OnClick="img_OneAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_oneRemove" runat="server" ImageUrl="~/Images/Back_Icon.png"
                                                    AlternateText="ALL" ToolTip="Remove Selected Record" OnClick="img_oneRemove_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_Allremove" runat="server" ImageUrl="~/Images/BackFast_Icon.png"  style="display:none"
                                                    AlternateText="ALL" ToolTip="Remove All Selected Record" OnClick="img_Allremove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="float: left; width: 45%; height:500px; overflow:auto">
                                    <asp:GridView ID="gv_Sel_Acct" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        DataKeyNames="CODE_ID,SELECTED_REC,CODE_NAME" OnRowDataBound="gv_Sel_Acct_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Accoount Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("CODE_NAME")%></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSel_Acct" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="divRowContainer">
                                <asp:HiddenField ID="hf_All_Acct" runat="server" />
                                <asp:HiddenField ID="hf_Sel_Acct" runat="server" />
                            </div>
                        </div>
                        <div class="divClear_10" style="width: 30%">
                        </div>
                        <div id="div_Segment" runat="server" visible="false">
                            <div class="divRowContainer">
                                <div style="float: left; width: 45%;height:500px; overflow:auto">
                                    <asp:GridView ID="gv_All_Segment" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        DataKeyNames="SEGMENT_ID,SELECTED_REC,SEGMENT_NAME" OnRowDataBound="gv_All_Segment_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Segment Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbl_AllSegName" runat="server" ><%# Eval("SEGMENT_NAME")%></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAll_Segment" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                                <div style="float: left; width: 10%">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_Seg_AllAdd" runat="server" ImageUrl="~/Images/NextFast_Icon.png" style="display:none"
                                                    AlternateText="ALL" ToolTip="Move All the Record" OnClick="img_Seg_AllAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_Seg_OneAdd" runat="server" ImageUrl="~/Images/Next_Icon.png"
                                                    AlternateText="One" ToolTip="Move Selected Record" OnClick="img_Seg_OneAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_Seg_OneRemove" runat="server" ImageUrl="~/Images/Back_Icon.png"
                                                    AlternateText="ALL" ToolTip="Remove Selected Record" OnClick="img_Seg_OneRemove_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="img_Seg_AllRemove" runat="server" ImageUrl="~/Images/BackFast_Icon.png"  style="display:none"
                                                    AlternateText="ALL" ToolTip="Remove All Selected Record" OnClick="img_Seg_AllRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="float: left; width: 45%;height:500px; overflow:auto">
                                    <asp:GridView ID="gv_Sel_Segment" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        DataKeyNames="SEGMENT_ID,SELECTED_REC,SEGMENT_NAME" OnRowDataBound="gv_Sel_Segment_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Segment Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbl_Sel_Segment" runat="server"><%# Eval("SEGMENT_NAME")%></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSel_Segment" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="divRowContainer">
                                <asp:HiddenField ID="hf_Seg_AllSegmentId" runat="server" />
                                <asp:HiddenField ID="hf_Seg_SelSegmentId" runat="server" />
                            </div>
                        </div>
                        <div class="divClear_10" style="width: 30%">
                        </div>
                        <div class="divRowContainer" align="right">
                            <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Images/Add.png" AlternateText="Add"
                                ToolTip="Add" OnClick="imgbtnAdd_Click" />
                            &nbsp;
                            <asp:ImageButton ID="imgBtnUpdate" runat="server" ImageUrl="~/Images/Update.png"
                                Visible="false" ToolTip="Update" AlternateText="Update" OnClick="imgBtnUpdate_Click" />
                            &nbsp;
                            <asp:ImageButton ID="imgBtnDelete" runat="server" ImageUrl="~/Images/Delete.png"
                                Visible="false" ToolTip="Delete" AlternateText="Delete" OnClick="imgBtnDelete_Click" />
                        </div>
                    </td>
                </tr>
                <tr style="display: none">
                    <td colspan="2">
                        <div id="main" style="display: none">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hf_BS_TMPL_ID" runat="server" />
        <asp:HiddenField ID="hf_BS_TMPL_GROUP_ID" runat="server" />
        <asp:HiddenField ID="PARENT_GROUP_NAME" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_imgbtnAdd").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_imgBtnUpdate").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
