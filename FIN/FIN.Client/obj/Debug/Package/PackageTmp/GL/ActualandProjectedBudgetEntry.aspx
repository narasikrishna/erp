﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ActualandProjectedBudgetEntry.aspx.cs" Inherits="FIN.Client.GL.ActualandProjectedBudget" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCompanyName">
                Company Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlCompanyName" CssClass="validate[required] RequiredField ddlStype"
                    MaxLength="50" runat="server" TabIndex="1">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblBudgetName">
                Budget Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtBudgetName" MaxLength="500" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
            Global Segment
        </div>
        <div class="divtxtBox LNOrient" style="  width: 300px">
            <asp:DropDownList ID="ddlGlobalSegment" CssClass="validate[required] RequiredField ddlStype"
                runat="server" TabIndex="3">
            </asp:DropDownList>
        </div>
       <div class="colspace  LNOrient" >
                &nbsp</div>
        <div class="lblBox LNOrient" style="  width: 150px" id="lblBudgetType">
            Budget Type
        </div>
        <div class="divtxtBox LNOrient" style="  width: 155px">
            <asp:DropDownList ID="ddlBudgetType" CssClass="validate[required] RequiredField ddlStype"
                MaxLength="50" runat="server" TabIndex="4">
            </asp:DropDownList>
        </div>
        <div class="divRowContainer" style="visibility: hidden">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblActualFromDate">
                Actual From Date
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblFromDate">
                Actual From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="5" Width="150px"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblToDate">
                Actual To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAnnualBudgetAmount">
                Annual Budget Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtAnnualBudgetAmount" CssClass=" EntryFont RequiredField txtBox_N"
                    runat="server" TabIndex="7" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="anamount" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtAnnualBudgetAmount">
                </cc2:FilteredTextBoxExtender>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 300px" id="Div2">
                <asp:ImageButton ID="btnDistributetoperiods" runat="server" ImageUrl="~/Images/btnDistributeToPeriods.png"
                    OnClick="btnDistributetoperiods_Click" Style="border: 0px" TabIndex="8" />
                <%-- <asp:Button ID="btnDistributetoperiods" runat="server" Text="Distribute to Periods"
                    CssClass="btnProcess" OnClick="btnDistributetoperiods_Click" TabIndex="8" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div  class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="BUDGET_DTL_ID,DELETED,GROUP_ID,CODE_ID,ACCT_CODE_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="false" OnSelectedIndexChanged="gvData_SelectedIndexChanged1">
                <Columns>
                    <asp:TemplateField HeaderText="Account Group" FooterStyle-Width="350px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountGroup" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlAccountGroup_SelectedIndexChanged"
                                TabIndex="9">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountGroup" AutoPostBack="True" OnSelectedIndexChanged="ddlAccountGroup_SelectedIndexChanged"
                                runat="server" CssClass="ddlStype" Width="180px" TabIndex="9">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountGroup" CssClass="adminFormFieldHeading" Width="180px" runat="server"
                                Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlAccount_SelectedIndexChanged"
                                TabIndex="10">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlAccount_SelectedIndexChanged"
                                TabIndex="10">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccount" CssClass="adminFormFieldHeading" Width="180px" runat="server"
                                Text='<%# Eval("CODE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Previous Year">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtActualPreviousAmt" Enabled="false" runat="server" Text='<%# Eval("ACTUAL_PREVIOUS_AMOUNT") %>'
                                TabIndex="11" CssClass="EntryFont txtBox_N" Width="120px" FilterType="Numbers,Custom"
                                ValidChars="."></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderr" runat="server" FilterType="Numbers"
                                TargetControlID="txtAnnualBudgetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtActualPreviousAmt" Enabled="false" runat="server" TabIndex="11"
                                CssClass="EntryFont txtBox_N" Width="120px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPreviousyr" CssClass="EntryFont RequiredField txtBox_N" Width="130px"
                                runat="server" Text='<%# Eval("ACTUAL_PREVIOUS_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Increment/Decrement(%)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPercentage" runat="server" Text='<%# Eval("PERCENTAGE") %>' TabIndex="12"
                                CssClass="EntryFont txtBox_N" Width="100px" MaxLength="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtPercentage">
                            </cc2:FilteredTextBoxExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPercentage" runat="server" TabIndex="12" CssClass="EntryFont txtBox_N"
                                Width="100px" MaxLength="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderre" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtPercentage">
                            </cc2:FilteredTextBoxExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPercentage" CssClass="EntryFont txtBox_N" Width="100px" runat="server"
                                Text='<%# Eval("PERCENTAGE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 1">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod01" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                CssClass="EntryFont RequiredField txtBox_N" Width="100px" TabIndex="13"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod01" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="13" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod01" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_01") %>' Width="130px"></asp:Label>
                            <br />
                            Budget:
                            <asp:Label ID="lblPeriod01" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_01") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 2">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod02" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                TabIndex="14" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod02" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="16"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod02" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_02") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod02" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_02") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 3">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod03" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod03" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="17"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod03" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_03") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod03" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_03") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 4">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod04" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                TabIndex="16" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod04" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="18"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod04" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_04") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod04" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_04") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 5">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod05" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                TabIndex="17" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod05" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="19"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod05" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_05") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod05" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_05") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 6">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod06" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                TabIndex="18" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod06" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="20"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod06" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_06") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod06" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_06") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 7">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod07" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                TabIndex="19" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod07" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="21"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod07" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_07") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod07" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_07") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 8">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod08" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                TabIndex="20" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod08" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="22"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod08" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_08") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod08" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_08") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 9">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod09" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                TabIndex="21" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod09" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="21" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod09" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_09") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod09" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_09") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 10">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod10" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                TabIndex="20" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod10" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="24"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_10") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_10") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 11">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod11" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod11" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="25"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod11" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_11") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod11" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_11") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 12">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod12" Enabled="false" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod12" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px" TabIndex="26"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            Actual :
                            <asp:Label ID="lblActualPeriod12" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("ACTUAL_BUDGET_AMT_12") %>' Width="130px"></asp:Label>
                            <br />
                            Budget :
                            <asp:Label ID="lblPeriod12" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("BUDGET_MONTH_AMT_12") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                        Visible="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnClick_Click"
                                Style="border: 0px" />
                            <%-- <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click" />--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnClick_Click"
                                Style="border: 0px" />
                            <%--<asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click" />--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnClick_Click"
                                Style="border: 0px" />
                            <%--<asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click"
                                TabIndex="23" />--%>
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="27" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="28" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="27" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="28" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="28" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" CssClass="ddlStype" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="10" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        //        $(document).ready(function () {
        //            $("#form1").validationEngine();
        //            return fn_SaveValidation();
        //        });

        //        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //        prm.add_endRequest(function () {
        //            return fn_SaveValidation();
        //        });

        //        function fn_SaveValidation() {
        //            $("#FINContent_btnSave").click(function (e) {
        //                //e.preventDefault();
        //                return $("#form1").validationEngine('validate')
        //            })
        //        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
