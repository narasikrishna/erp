﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AccountingGroupsEntry.aspx.cs" Inherits="FIN.Client.GL.AccountingGroupsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblGroupName">
                Group Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 480px">
                <asp:TextBox ID="txtGroupName" MaxLength="240" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblGroupNameAR">
                Group Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 480px">
                <asp:TextBox ID="txtGroupNameAR" MaxLength="240" CssClass="txtBox_ol"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblAlternateName">
                Alternate Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 480px">
                <asp:TextBox ID="txtAlternateName" MaxLength="240" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblFinancialGroup">
                Financial Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:RadioButtonList ID="rbLFGYN" runat="server" RepeatDirection="Horizontal" CssClass="lblBox LNOrient"
                    OnSelectedIndexChanged="rbLFGYN_SelectedIndexChanged" TabIndex="4">
                    <asp:ListItem Value="Y" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblFundTransfer">
                Fund Transfer
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:RadioButtonList ID="rbFundTransfer" runat="server" RepeatDirection="Horizontal"
                    CssClass="lblBox LNOrient" TabIndex="5">
                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                    <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:TextBox runat="server" ID="txtEffStartdate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEffStartdate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffStartdate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 180px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:TextBox runat="server" ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 180px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="  width: 120px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" TabIndex="7" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" TabIndex="8" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                        TabIndex="9" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                </td>
            </tr>
        </table>
    </div>
    <div class="divClear_10">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
