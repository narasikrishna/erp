﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AccountingCalendarEntry.aspx.cs" Inherits="FIN.Client.GL.AccountingCalendar" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalendarName">
                Calendar Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCalendarName" CssClass="validate[required] RequiredField txtBox"
                    runat="server" MaxLength="240" TabIndex="1" OnTextChanged="txtCalendarName_TextChanged"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalendarNameAR">
                Calendar Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCalendarNameAR" CssClass="txtBox_ol"
                    runat="server" MaxLength="240" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    TabIndex="3" OnTextChanged="txtEffectiveDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEffectiveDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffectiveDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAdjPeriod">
                Adjusting Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtAdjPeriod" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" MaxLength="2" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredQbeBeginDate" runat="server" FilterType="Numbers"
                    TargetControlID="txtAdjPeriod" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" ID="txtEndDate" CssClass=" validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        <div class="lblBox LNOrient" style="  width: 150px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="6" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="  width: 150px" id="Div3">
            </div>
            <div class="divChkbox" style="  width: 150px">
            </div>
            <div style="  width: 150px" id="Div1">
                <asp:HiddenField ID="hf_AcId" runat="server" />
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px" align="right">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Visible="false" CssClass="btn"
                    OnClick="btnAdd_Click" TabIndex="6" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="CAL_DTL_ID,DELETED" OnRowDataBound="gvData_RowDataBound" OnRowCommand="gvData_RowCommand"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCreated="gvData_RowCreated"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="true" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Accounting Year">
                        <EditItemTemplate>
                            <asp:TextBox ID="ntxtAcctYear" CssClass=" RequiredField txtBox_N" Text='<%# Eval("CAL_ACCT_YEAR") %>'
                                runat="server" MaxLength="4" TabIndex="7" Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredQbeBegisnDate" runat="server" FilterType="Numbers"
                                TargetControlID="ntxtAcctYear" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ntxtAcctYear" CssClass=" RequiredField txtBox" Text='' runat="server"
                                MaxLength="4" TabIndex="7" Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredQbeBeginDatse" runat="server" FilterType="Numbers"
                                TargetControlID="ntxtAcctYear" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblAcctYear" runat="server" Text='<%# Eval("CAL_ACCT_YEAR") %>' CssClass="EntryFont lblBox LNOrient"
                                Width="150px"></asp:Label>--%>
                            <asp:TextBox ID="ntxtAcctYear" Enabled="false" CssClass=" RequiredField txtBox" Text='<%# Eval("CAL_ACCT_YEAR") %>'
                                runat="server" MaxLength="4" TabIndex="7" Width="95%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredQssbeBeginDatse" runat="server" FilterType="Numbers"
                                TargetControlID="ntxtAcctYear" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Begin Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpBeginDate" TabIndex="8" runat="server" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                Width="95%" Text='<%# Eval("CAL_EFF_START_DT","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtBeginDate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpBeginDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredQbeBeginDate" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpBeginDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpBeginDate" TabIndex="8" runat="server" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                Width="95%" Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtenderBeginDate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpBeginDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredBeginDate" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpBeginDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblBeginDate" runat="server" Text='<%# Eval("CAL_EFF_START_DT","{0:dd/MM/yyyy}") %>'
                                Width="100px"></asp:Label>--%>
                            <asp:TextBox ID="dtpBeginDate" Enabled="false" TabIndex="8" runat="server" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                Width="95%" Text='<%# Eval("CAL_EFF_START_DT","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CALBegdt" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpBeginDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredBeginDate1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpBeginDate" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEnddate" TabIndex="9" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Width="95%" Text='<%# Eval("CALL_EFF_END_DT","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtEnddate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEnddate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredQbeEnddate" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEnddate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEnddate" TabIndex="9" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Width="95%" Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtenderEnddate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEnddate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredEnddate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dtpEnddate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblQbeExamDate" runat="server" Text='<%# Eval("CALL_EFF_END_DT","{0:dd/MM/yyyy}") %>'
                                Width="100px"></asp:Label>--%>
                            <asp:TextBox ID="dtpEnddate" TabIndex="9" Enabled="false" runat="server" Text='<%# Eval("CALL_EFF_END_DT","{0:dd/MM/yyyy}") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="95%" MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalExtenderEnddate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEnddate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilEnddate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dtpEnddate" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Generate Period">
                        <ItemTemplate>
                            <asp:Button ID="btnGeneratePeriod" runat="server" CssClass="button" Text="Generate Period"
                                TabIndex="10" CommandName="Select" OnClick="btnGeneratePeriod_Click" Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnGeneratePeriod" TabIndex="10" runat="server" CssClass="button"
                                Text="GeneratePeriod" CommandName="Select" OnClick="btnGeneratePeriod_Click"
                                Enabled="false" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnGeneratePeriod" TabIndex="10" runat="server" CssClass="button"
                                Text="GeneratePeriod" CommandName="Select" OnClick="btnGeneratePeriod_Click" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View">
                        <ItemTemplate>
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" TabIndex="11"
                                CommandName="Select" OnClick="btnView_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnView" TabIndex="11" runat="server" CssClass="button" Text="View"
                                CommandName="Select" OnClick="btnView_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnView" TabIndex="11" runat="server" CssClass="button" Text="View"
                                Enabled="false" CommandName="Select" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" TabIndex="11" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" TabIndex="12" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" TabIndex="13" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" TabIndex="14" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" ToolTip="Add"
                                CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" TabIndex="15" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:HiddenField ID="btnGeneratePeriod" runat="server" />
            <asp:HiddenField ID="hfPeriodDetId" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="btnGeneratePeriod"
                PopupControlID="Panel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="450px" Width="90%" BackColor="White"
                ScrollBars="Vertical">
                <div class="ConfirmForm" style="overflow: auto">
                    <table width="100%">
                        <%-- <tr>
                            <td class="adminFormFieldHeading">
                                <asp:Label ID="lblcal" runat="server" Text="Calendar Name"> </asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtcal" runat="server"></asp:TextBox>
                            </td>
                            <td class="">
                                <asp:Label ID="lblEffdate" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="Effective Date"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="dtpEffdate" CssClass="" />
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="dtpEffdate" />
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEffdate" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAdjperiod" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="Adjusting Period"></asp:Label>
                            </td>
                            <td>
                                <cc1:NumericTextBox ID="ntxtAdjperiod" runat="server"></cc1:NumericTextBox>
                            </td>
                            <td class="">
                                <asp:Label ID="lblenddate" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="End Date"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="dtpEndDate" CssClass="" />
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="dtpEndDate" />
                                <cc2:FilteredTextBoxExtender ID="ftePrepareDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                    TargetControlID="dtpEndDate" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAcctYr" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="Accounting Year"></asp:Label>
                            </td>
                            <td>
                                <cc1:NumericTextBox ID="ntxtAcctyr" runat="server"></cc1:NumericTextBox>
                            </td>
                            <td class="">
                                <asp:Label ID="lblbegindte" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="Begin Date"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtbegindte" CssClass="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="">
                                <asp:Label ID="lblActenddte" runat="server" CssClass="DisplayFont lblBox LNOrient" Text="End Date"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtActenddte" CssClass="" />
                            </td>
                        </tr>--%>
                        <tr class="LNOrient">
                            <td colspan="4">
                                <asp:GridView ID="gvperiods" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                    DataKeyNames="PERIOD_ID,LOOKUP_ID" OnRowDataBound="gvperiod_RowDataBound" OnRowCommand="gvperiodData_RowCommand"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Period Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("PERIOD_NAME") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Type">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodtype" runat="server" Text='<%# Eval("PERIOD_TYPE") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Fillfromdte" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dtfromdate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtenderfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredfromdate" runat="server" ValidChars="/"
                                                    FilterType="Numbers,Custom" TargetControlID="dtfromdate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQbeExamDate" runat="server" Text='<%# Eval("PERIOD_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtendertodate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredtodate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExttodate" runat="server" Format="dd/MM/yyyy" TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="FilteredtoDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbltoDate" runat="server" Text='<%# Eval("PERIOD_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Status" Visible="False">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" CssClass="DisplayFont button" Visible="false" ID="Button1"
                                    Text="Ok" Width="60px" OnClick="Button1_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Close"
                                    Width="60px" />
                                <asp:HiddenField ID="HiddenField4" runat="server" />
                                <asp:HiddenField ID="HiddenField5" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
