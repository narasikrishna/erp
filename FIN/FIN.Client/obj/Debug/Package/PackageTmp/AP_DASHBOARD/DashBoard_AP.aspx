﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard_AP.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.DashBoard_AP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .frmStyle
        {
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: 0px solid red;
            top: 0;
            left: 0;
        }
        .frmdiv
        {
            border: 1px solid aqua;
            float:left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divDashboard">
        <div id="divTopSupplier" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_TopSupplier" class="frmStyle" src="APTopSuppliers_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divInvoicePaid" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_InvReceiv" class="frmStyle" src="APInvoicePaid_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divAA" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_AA" class="frmStyle" src="APAgingAnalysis_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divSupPOA" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmSupPOA" class="frmStyle" src="APSupplierPOA_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGRN" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmGRN" class="frmStyle" src="APGRN_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divItemUtil" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmItemUtil" class="frmStyle" src="APItemUtilization_DB.aspx" scrolling="no">
            </iframe>
        </div>
    </div>
    </form>
</body>
</html>
