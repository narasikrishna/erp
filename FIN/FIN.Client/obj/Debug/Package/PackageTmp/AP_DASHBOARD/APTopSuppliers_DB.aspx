﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="APTopSuppliers_DB.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.APTopSuppliers_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Top 5 Suppliers
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divChrtTopSupp" style="width: 100%">
            <asp:Chart ID="chrtTop5Supply" runat="server" Height="310px" Width="500px" EnableViewState="true">
                <Series>
                    <asp:Series Name="S_supply_Count" XValueMember="vendor_name" YValueMembers="payment_amt"
                        IsValueShownAsLabel="true" LabelAngle="60" Legend="Legend1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                        <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="vendor_name" HeaderText="Supplier"></asp:BoundField>
                    <asp:BoundField DataField="payment_amt" HeaderText="Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
