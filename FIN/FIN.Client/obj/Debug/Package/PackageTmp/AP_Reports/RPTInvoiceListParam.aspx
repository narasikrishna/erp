﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTInvoiceListParam.aspx.cs" Inherits="FIN.Client.AP_Reports.RPTInvoiceListParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <%--     <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblInvoiceNumber">
                Invoice From Number
            </div>
            <div class="divtxtBox" style="float: left; width: 270px" id="lblInvoiceNoValue">
                <asp:TextBox ID="txtInvoiceFromNumber" runat="server" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
                Invoice To Number
            </div>
            <div class="divtxtBox" style="float: left; width: 270px" id="Div4">
                <asp:TextBox ID="txtInvoiceToNumber" runat="server" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceNumber">
                From Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlFromInvoiceNumber" runat="server" TabIndex="1" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px; ">
            </div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div4">
                To Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToInvoiceNumber" runat="server" TabIndex="2" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblSupplierName">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px; ">
            </div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblInvoiceType">
                Invoice Type
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlInvoiceType" runat="server" TabIndex="4" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div5">
                From Invoice Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtInvoiceFromAmount" runat="server" TabIndex="5" MaxLength="13"
                    CssClass="txtBox_N" Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtInvoiceFromAmount" />
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div6">
                To Invoice Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtInvoiceToAmount" runat="server" TabIndex="6" MaxLength="13" CssClass="txtBox_N"
                    Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtInvoiceToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="7" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="colspace LNOrient" style="width: 20px; ">
                </div>
                <div class="lblBox LNOrient" style=" width: 150px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="8" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 120px" id="Div3">
                    Invoice Status
                </div>
                <div class="divtxtBox  LNOrient" style="width: 550px" align="center">
                    <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                        <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="C">Cancelled &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="IU">All &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" id="divMainContainer">
                <div class="divRowContainer  LNOrient divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td style="width: 280px">
                                <%-- <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                                    <img src="../Images/MainPage/settings-icon_B.png" alt="Show Report" width="15px"
                                        height="15px" title="Show Report" />--%>
                                <%-- </a>--%>
                                <%-- <asp:Button ID="btnSave1" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" Visible="false" />--%>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                            <%--<td>
                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <%--<asp:TemplateField HeaderText="" ItemStyle-Width="50px" ControlStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" /><br />
                        </FooterTemplate>
                         <FooterStyle  VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
