﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WarehouseFacilityParam.aspx.cs" Inherits="FIN.Client.AP_Reports.WarehouseFacilityParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequisitionType">
                Warehouse Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlWarehouseName" runat="server" TabIndex="1" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblUOMClass">
                Facility Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlFacilityName" runat="server" CssClass="ddlStype" TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Created By
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlCreatedBy" runat="server" CssClass="ddlStype" TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div8">
                Modified By
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlModifiedBy" runat="server" TabIndex="5" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblFromDate">
                Created From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCreatedFromDate" runat="server" TabIndex="6" CssClass=" txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtCreatedFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblToDate">
                Created To Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCreatedToDate" runat="server" TabIndex="7" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtCreatedToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Modified From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtModifiedFromDate" runat="server" TabIndex="8" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtModifiedFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Modified To Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtModToDate" runat="server" TabIndex="9" CssClass=" txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtModToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
