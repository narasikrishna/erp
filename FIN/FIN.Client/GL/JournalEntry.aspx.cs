﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;
using VMVServices.Services.Data;


namespace FIN.Client.GL
{
    public partial class JournalEntry : PageBase
    {
        GL_JOURNAL_HDR gL_JOURNAL_HDR = new GL_JOURNAL_HDR();
        GL_JOURNAL_DTL gL_JOURNAL_DTL = new GL_JOURNAL_DTL();
        DataTable dtGridData = new DataTable();
        string ProReturn = null;
        Boolean savedBool;
        Boolean bol_rowVisiable;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
                else
                {
                    SetFocusControls(sender);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        private Control GetControlThatCausedPostBack(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button || c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }
        private void SetFocusControls(object sender)
        {
            WebControl wcICausedPostBack = (WebControl)GetControlThatCausedPostBack(sender as Page);
            int indx = wcICausedPostBack.TabIndex;
            var ctrl = from control in wcICausedPostBack.Parent.Controls.OfType<WebControl>()
                       where control.TabIndex > indx
                       select control;
            ctrl.DefaultIfEmpty(wcICausedPostBack).First().Focus();
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();
                //   txtExchangeRate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Session[FINSessionConstants.GridData] = null;
                EntityData = null;

                Session["Reversal"] = null;
                Session["cramt"] = null;
                Session["dramt"] = null;
                Session["JENUM"] = null;
                Session["ReversalText"] = null;

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtDate_TextChanged1(txtDate, new EventArgs());
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getJEDetails(Master.StrRecordId)).Tables[0];

                if (Master.Mode == FINAppConstants.Update && Master.StrRecordId != "0")
                {
                    txtExchangeRate.Enabled = false;
                }
                hf_WFStatus.Value = "0";
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<GL_JOURNAL_HDR> userCtx = new DataRepository<GL_JOURNAL_HDR>())
                    {
                        gL_JOURNAL_HDR = userCtx.Find(r =>
                            (r.JE_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_JOURNAL_HDR;
                    divJNo.Visible = true;
                    txtJVNo.Text = gL_JOURNAL_HDR.JE_NUMBER;
                    btnPrintReport.Visible = true;
                    btnReversal.Visible = true;


                    //if (gL_JOURNAL_HDR.JOURNAL_REVERSED != null)
                    //{
                    //    if (gL_JOURNAL_HDR.JOURNAL_REVERSED.ToString().ToUpper() == "Y")
                    //    {
                    //        btnReversal.Visible = false;
                    //    }
                    //}
                    hf_WFStatus.Value = gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS;
                    if (gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        ddlOrganisation.Enabled = false;
                        txtDate.Enabled = false;
                        ddlGlobalSegment.Enabled = false;
                        ddlJournalType.Enabled = false;
                        ddlCurrency.Enabled = false;
                        //      ddlExchangeRate.Enabled = false;
                        txtExchangeRate.Enabled = false;
                        txtJournalNumber.Enabled = false;
                        txtNarration.Enabled = false;
                        txtTotDrAmt.Enabled = false;
                        txtTotCrAmt.Enabled = false;
                        ddlPeriods.Enabled = false;
                        ddlSource.Enabled = false;

                    }

                    ddlOrganisation.SelectedValue = gL_JOURNAL_HDR.JE_COMP_ID;

                    Currency_BLL.getCurrencyDetails(ref ddlCurrency);
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlOrganisation.SelectedValue.ToString());

                    if (gL_JOURNAL_HDR.JE_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(gL_JOURNAL_HDR.JE_DATE.ToString());
                        //    ExchangeRate_BLL.GetExchangeRate(ref ddlExchangeRate, (txtDate.Text));
                        AccountingCalendar_BLL.GetCompPeriodBasedOrgJDate(ref ddlPeriods, (txtDate.Text));
                    }
                    if (gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID.ToString();
                    }
                    if (gL_JOURNAL_HDR.JE_TYPE != null)
                    {
                        ddlJournalType.SelectedValue = gL_JOURNAL_HDR.JE_TYPE;
                    }

                    if (gL_JOURNAL_HDR.JE_SOURCE != null)
                    {
                        ddlSource.SelectedValue = gL_JOURNAL_HDR.JE_SOURCE;
                    }
                    if (ddlSource.SelectedValue == "AP" || ddlSource.SelectedValue == "AR" || ddlSource.SelectedValue == "GL" || ddlSource.SelectedValue == "HR")
                    {
                        btnReversal.Visible = false;
                    }
                    else
                    {
                        btnReversal.Visible = true;
                    }

                    if (gL_JOURNAL_HDR.JE_CURRENCY_ID != null)
                    {
                        ddlCurrency.SelectedValue = gL_JOURNAL_HDR.JE_CURRENCY_ID.ToString();
                    }

                    if (gL_JOURNAL_HDR.JE_CURRENCY_RATE_ID != string.Empty && gL_JOURNAL_HDR.JE_CURRENCY_RATE_ID != null)
                    {
                        ddlExchangeRate.SelectedValue = gL_JOURNAL_HDR.JE_CURRENCY_RATE_ID.ToString();
                    }

                    if (gL_JOURNAL_HDR.JE_PERIOD_ID != null)
                    {
                        ddlPeriods.SelectedValue = gL_JOURNAL_HDR.JE_PERIOD_ID.ToString();
                    }

                    if (gL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE != null)
                    {
                        txtExchangeRate.Text = gL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE.ToString();
                    }

                    if (gL_JOURNAL_HDR.JE_REFERENCE != null)
                    {
                        txtJournalNumber.Text = gL_JOURNAL_HDR.JE_REFERENCE;
                    }

                    if (gL_JOURNAL_HDR.JE_DESC != null)
                    {
                        txtNarration.Text = gL_JOURNAL_HDR.JE_DESC;
                    }

                    ddlVouchertype.SelectedValue = gL_JOURNAL_HDR.VOUCHER_TYPE;

                    txtTotDrAmt.Text = gL_JOURNAL_HDR.JE_TOT_DR_AMOUNT;
                    txtTotCrAmt.Text = gL_JOURNAL_HDR.JE_TOT_CR_AMOUNT;

                    Session["cramt"] = gL_JOURNAL_HDR.JE_TOT_CR_AMOUNT;
                    Session["dramt"] = gL_JOURNAL_HDR.JE_TOT_DR_AMOUNT;
                    Session["JENUM"] = gL_JOURNAL_HDR.JE_NUMBER;

                    if (gL_JOURNAL_HDR.JE_RECURRING_END_DATE != null)
                    {
                        txtRecurringDate.Text = DBMethod.ConvertDateToString(gL_JOURNAL_HDR.JE_RECURRING_END_DATE.ToString());
                    }

                    if (gL_JOURNAL_HDR.JE_RECURRING_FREQUENCY != null)
                    {
                        ddlFrequency.SelectedValue = gL_JOURNAL_HDR.JE_RECURRING_FREQUENCY;
                    }

                    SourceChanged();

                    if (ddlSource.SelectedItem.Text == "Manual")
                    {
                        btnReversal.Visible = true;
                    }
                    Session["ReversalText"] = txtJournalNumber.Text;

                    if (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutomaticWorkFlow"].ToString()))
                    {
                        btnSendApprove.Visible = true;
                        if (gL_JOURNAL_HDR.ATTRIBUTE1 == null)
                        {
                            btnSendApprove.Enabled = true;
                            lblPosted.Visible = false;
                        }
                        else
                        {
                            btnSendApprove.Visible = false;
                            lblPosted.Visible = true;
                        }
                    }

                    if (FINSP.Is_workflow_defined("GL_015"))
                    {
                        if (gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS == "0")
                        {
                            lblWaitApprove.Visible = true;
                        }
                        else
                        {
                            lblWaitApprove.Visible = false;
                        }
                    }
                    else
                    {
                        lblWaitApprove.Visible = false;
                    }

                }
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    txtTotDrAmt.Text = gL_JOURNAL_HDR.JE_TOT_DR_AMOUNT;
                //    txtTotCrAmt.Text = gL_JOURNAL_HDR.JE_TOT_CR_AMOUNT;

                //}


                if (Master.Mode == FINAppConstants.Add)
                {
                    LoadTempJournal();
                }
                if (Request.QueryString.ToString().Contains("QueryMode"))
                {
                    gvData.FooterRow.Visible = false;
                    gvData.Enabled = false;
                    btnPrintReport.Enabled = false;
                    btnReversal.Enabled = false;
                    btnSendApprove.Enabled = false;
                    ddlGlobalSegment.Enabled = false;
                    txtJournalNumber.Enabled = false;
                    ddlJournalType.Enabled = false;
                    ddlSource.Enabled = false;
                    txtRecurringDate.Enabled = false;
                    ddlFrequency.Enabled = false;
                    ddlPeriods.Enabled = false;
                    ddlCurrency.Enabled = false;
                    txtNarration.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void LoadTempJournal()
        {
            DataTable dt_TempJou = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getTempJournal(this.LoggedUserName)).Tables[0];
            if (dt_TempJou.Rows.Count > 0)
            {
                divTmpJournal.Visible = true;
                gvTempJournal.DataSource = dt_TempJou;
                gvTempJournal.DataBind();
                mpeTempJournal.Show();
               
            }
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlJournalType;
                slControls[1] = ddlVouchertype;
                slControls[2] = ddlSource;
                slControls[3] = ddlPeriods;
                slControls[4] = ddlCurrency;
              
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = "Journal Type~Voucher type~Source~Periods~Currency";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    
                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                }
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    gL_JOURNAL_HDR = (GL_JOURNAL_HDR)EntityData;
                }

                gL_JOURNAL_HDR.JE_COMP_ID = ddlOrganisation.SelectedItem.Value;

                if (txtDate.Text != string.Empty)
                {
                    gL_JOURNAL_HDR.JE_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue.ToString();

                //GL_SEGMENT_VALUES GL_SEGMENT_VALUES = new DAL.GL_SEGMENT_VALUES();
                //using (IRepository<GL_SEGMENT_VALUES> sv = new DataRepository<GL_SEGMENT_VALUES>())
                //{
                //    GL_SEGMENT_VALUES = sv.Find(x => (x.SEGMENT_VALUE_ID == ddlGlobalSegment.SelectedValue.ToString())).SingleOrDefault();
                //    if (GL_SEGMENT_VALUES != null)
                //    {
                //        gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID = GL_SEGMENT_VALUES.SEGMENT_ID;
                //    }
                //}

                gL_JOURNAL_HDR.JE_SOURCE = ddlSource.SelectedValue.ToString();

                if (txtRecurringDate.Text != string.Empty)
                {
                    gL_JOURNAL_HDR.JE_RECURRING_END_DATE = DBMethod.ConvertStringToDate(txtRecurringDate.Text.ToString());
                }
                gL_JOURNAL_HDR.JE_RECURRING_FREQUENCY = ddlFrequency.SelectedValue;

                gL_JOURNAL_HDR.JE_TYPE = ddlJournalType.SelectedValue.ToString();
                gL_JOURNAL_HDR.JE_CURRENCY_ID = ddlCurrency.SelectedValue.ToString();
                //     gL_JOURNAL_HDR.JE_CURRENCY_RATE_ID = ddlExchangeRate.SelectedValue.ToString();

                gL_JOURNAL_HDR.JE_PERIOD_ID = ddlPeriods.SelectedValue.ToString();



                gL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE = txtExchangeRate.Text;//exchange rate value
                gL_JOURNAL_HDR.JE_TOT_DR_AMOUNT = txtTotDrAmt.Text;//Dr total amount
                gL_JOURNAL_HDR.JE_TOT_CR_AMOUNT = txtTotCrAmt.Text;//cr total amount
                gL_JOURNAL_HDR.JE_DESC = txtNarration.Text;

                gL_JOURNAL_HDR.VOUCHER_TYPE = ddlVouchertype.SelectedValue.ToString();
                if (Session["Reversal"] != null)
                {
                    gL_JOURNAL_HDR.JOURNAL_REVERSED = "Y";
                    gL_JOURNAL_HDR.ACTUAL_JOURNAL_ID = gL_JOURNAL_HDR.JE_HDR_ID;
                    gL_JOURNAL_HDR.JE_REFERENCE = txtJournalNumber.Text;


                    gL_JOURNAL_HDR.JE_NUMBER = FINSP.GetFN_Sequence_GL(FINAppConstants.GL_007_JN.ToString(), ddlPeriods.SelectedValue.ToString(), "GL");

                    gL_JOURNAL_HDR.JE_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_M.ToString());
                    gL_JOURNAL_HDR.CREATED_BY = this.LoggedUserName;
                    gL_JOURNAL_HDR.CREATED_DATE = DateTime.Today;
                    gL_JOURNAL_HDR.ENABLED_FLAG = "1";

                }
                else
                {
                    gL_JOURNAL_HDR.JE_REFERENCE = txtJournalNumber.Text;//journal reference number

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        gL_JOURNAL_HDR.MODIFIED_BY = this.LoggedUserName;
                        gL_JOURNAL_HDR.MODIFIED_DATE = DateTime.Today;
                    }
                    else
                    {
                        gL_JOURNAL_HDR.JE_NUMBER = FINSP.GetFN_Sequence_GL(FINAppConstants.GL_007_JN.ToString(), ddlPeriods.SelectedValue.ToString(), "GL");

                        gL_JOURNAL_HDR.JE_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_M.ToString());
                        gL_JOURNAL_HDR.CREATED_BY = this.LoggedUserName;
                        gL_JOURNAL_HDR.CREATED_DATE = DateTime.Today;
                        gL_JOURNAL_HDR.ENABLED_FLAG = "1";
                    }
                }

                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutomaticWorkFlow"].ToString()))
                {
                    gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_HDR.JE_HDR_ID.ToString());
                }
                else
                {
                    gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS = "0";
                    VMVServices.Web.Utils.SavedRecordId = gL_JOURNAL_HDR.JE_HDR_ID.ToString();
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_JOURNAL_DTL = new GL_JOURNAL_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString() != string.Empty)
                    {
                        using (IRepository<GL_JOURNAL_DTL> sv = new DataRepository<GL_JOURNAL_DTL>())
                        {
                            gL_JOURNAL_DTL = sv.Find(x => (x.JE_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString())).SingleOrDefault();
                        }
                    }


                    gL_JOURNAL_DTL.JE_ACCT_CODE_ID = dtGridData.Rows[iLoop]["ACCT_CODE_ID"].ToString();
                    gL_JOURNAL_DTL.JE_CREDIT_DEBIT = dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"].ToString().Trim();// == "Dr" ? "2" : "1";

                    if (dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"].ToString().Trim() == "Dr")
                    {
                        gL_JOURNAL_DTL.JE_AMOUNT_DR = decimal.Parse(dtGridData.Rows[iLoop]["JE_AMOUNT_dr"].ToString());
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_DR = decimal.Parse(dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_DR"].ToString());
                        gL_JOURNAL_DTL.JE_AMOUNT_CR = null;
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_CR = null;
                    }
                    else
                    {
                        gL_JOURNAL_DTL.JE_AMOUNT_CR = decimal.Parse(dtGridData.Rows[iLoop]["JE_AMOUNT_cr"].ToString());
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_CR = decimal.Parse(dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_CR"].ToString());
                        gL_JOURNAL_DTL.JE_AMOUNT_DR = null;
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_DR = null;
                    }

                    gL_JOURNAL_DTL.JE_SEGMENT_ID_1 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_1"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_2 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_2"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_3 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_3"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_4 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_4"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_5 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_5"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_6 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_6"].ToString());

                    gL_JOURNAL_DTL.JE_PARTICULARS = dtGridData.Rows[iLoop]["JE_PARTICULARS"].ToString();
                    gL_JOURNAL_DTL.ENABLED_FLAG = FINAppConstants.Y;


                    gL_JOURNAL_DTL.JE_HDR_ID = gL_JOURNAL_HDR.JE_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        if ((dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != "0" && (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != string.Empty)
                        {

                            gL_JOURNAL_DTL.JE_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString());
                            tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Delete));
                        }
                    }
                    else
                    {

                        if (Session["Reversal"] != null)
                        {
                            gL_JOURNAL_DTL.JE_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_D.ToString());
                            gL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";//FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                            gL_JOURNAL_DTL.CREATED_BY = this.LoggedUserName;
                            gL_JOURNAL_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Add));

                        }
                        else
                        {
                            if ((dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != "0" && (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != string.Empty)
                            {
                                gL_JOURNAL_DTL.JE_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString());
                                gL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";// FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                                gL_JOURNAL_DTL.MODIFIED_BY = this.LoggedUserName;
                                gL_JOURNAL_DTL.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Update));
                            }
                            else
                            {
                                gL_JOURNAL_DTL.JE_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_D.ToString());
                                gL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";//FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                                gL_JOURNAL_DTL.CREATED_BY = this.LoggedUserName;
                                gL_JOURNAL_DTL.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Add));
                            }
                        }
                    }
                }


                if (Session["Reversal"] != null)
                {
                    DBMethod.ExecuteQuery(JournalEntry_DAL.UpdateJournal(gL_JOURNAL_HDR.ACTUAL_JOURNAL_ID));
                    CommonUtils.SavePCEntity<GL_JOURNAL_HDR, GL_JOURNAL_DTL>(gL_JOURNAL_HDR, tmpChildEntity, gL_JOURNAL_DTL);
                    savedBool = true;
                }
                else
                {
                    switch (Master.Mode)
                    {
                        case FINAppConstants.Add:
                            {
                                CommonUtils.SavePCEntity<GL_JOURNAL_HDR, GL_JOURNAL_DTL>(gL_JOURNAL_HDR, tmpChildEntity, gL_JOURNAL_DTL);
                                savedBool = true;
                                break;
                            }
                        case FINAppConstants.Update:
                            {
                                CommonUtils.SavePCEntity<GL_JOURNAL_HDR, GL_JOURNAL_DTL>(gL_JOURNAL_HDR, tmpChildEntity, gL_JOURNAL_DTL, true);
                                savedBool = true;
                                break;
                            }
                    }
                }

                if (hf_Temp_RefNo.Value.ToString().Length > 0)
                {
                    DBMethod.ExecuteNonQuery("DELETE TMP_GL_JOURNAL_DTL WHERE JE_HDR_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                    DBMethod.ExecuteNonQuery("DELETE TMP_GL_JOURNAL_HDR WHERE JE_HDR_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                }

                hf_Temp_RefNo.Value = "";
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void AssignToRevesalBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_JOURNAL_HDR = (GL_JOURNAL_HDR)EntityData;
                }

                gL_JOURNAL_HDR.JE_COMP_ID = ddlOrganisation.SelectedItem.Value;
                if (txtDate.Text != string.Empty)
                {
                    gL_JOURNAL_HDR.JE_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue.ToString();

                gL_JOURNAL_HDR.JE_SOURCE = ddlSource.SelectedValue.ToString();

                if (txtRecurringDate.Text != string.Empty)
                {
                    gL_JOURNAL_HDR.JE_RECURRING_END_DATE = DBMethod.ConvertStringToDate(txtRecurringDate.Text.ToString());
                }
                gL_JOURNAL_HDR.JE_RECURRING_FREQUENCY = ddlFrequency.SelectedValue;

                gL_JOURNAL_HDR.JE_TYPE = ddlJournalType.SelectedValue.ToString();
                gL_JOURNAL_HDR.JE_CURRENCY_ID = ddlCurrency.SelectedValue.ToString();
                gL_JOURNAL_HDR.JE_PERIOD_ID = ddlPeriods.SelectedValue.ToString();


                gL_JOURNAL_HDR.JE_REFERENCE = "Reversal - " + txtJournalNumber.Text;//journal reference number
                gL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE = txtExchangeRate.Text;//exchange rate value
                gL_JOURNAL_HDR.JE_TOT_DR_AMOUNT = txtTotDrAmt.Text;//Dr total amount
                gL_JOURNAL_HDR.JE_TOT_CR_AMOUNT = txtTotCrAmt.Text;//cr total amount
                gL_JOURNAL_HDR.JE_DESC = txtNarration.Text;

                gL_JOURNAL_HDR.ACTUAL_JOURNAL_ID = gL_JOURNAL_HDR.JE_HDR_ID;
                gL_JOURNAL_HDR.JE_REFERENCE = "Reversal - " + txtJournalNumber.Text;
                gL_JOURNAL_HDR.JE_NUMBER = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_JN.ToString());

                gL_JOURNAL_HDR.JE_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_M.ToString());
                gL_JOURNAL_HDR.CREATED_BY = this.LoggedUserName;
                gL_JOURNAL_HDR.CREATED_DATE = DateTime.Today;
                gL_JOURNAL_HDR.ENABLED_FLAG = "1";

                gL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_HDR.JE_HDR_ID.ToString());


                var tmpChildEntity = new List<Tuple<object, string>>();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_JOURNAL_DTL = new GL_JOURNAL_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString() != string.Empty)
                    {
                        using (IRepository<GL_JOURNAL_DTL> sv = new DataRepository<GL_JOURNAL_DTL>())
                        {
                            gL_JOURNAL_DTL = sv.Find(x => (x.JE_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString())).SingleOrDefault();
                        }
                    }


                    gL_JOURNAL_DTL.JE_ACCT_CODE_ID = dtGridData.Rows[iLoop]["ACCT_CODE_ID"].ToString();
                    gL_JOURNAL_DTL.JE_CREDIT_DEBIT = dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"].ToString().Trim();// == "Dr" ? "2" : "1";

                    if (dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"].ToString().Trim() == "Dr")
                    {
                        gL_JOURNAL_DTL.JE_AMOUNT_DR = decimal.Parse(dtGridData.Rows[iLoop]["JE_AMOUNT_dr"].ToString());
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_DR = decimal.Parse(dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_DR"].ToString());
                    }
                    else
                    {
                        gL_JOURNAL_DTL.JE_AMOUNT_CR = decimal.Parse(dtGridData.Rows[iLoop]["JE_AMOUNT_cr"].ToString());
                        gL_JOURNAL_DTL.JE_ACCOUNTED_AMT_CR = decimal.Parse(dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_CR"].ToString());
                    }

                    gL_JOURNAL_DTL.JE_SEGMENT_ID_1 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_1"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_2 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_2"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_3 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_3"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_4 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_4"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_5 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_5"].ToString());
                    gL_JOURNAL_DTL.JE_SEGMENT_ID_6 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_6"].ToString());


                    gL_JOURNAL_DTL.ENABLED_FLAG = FINAppConstants.Y;


                    gL_JOURNAL_DTL.JE_HDR_ID = gL_JOURNAL_HDR.JE_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        gL_JOURNAL_DTL.JE_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString());
                        tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        //if ((dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != "0" && (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString()) != string.Empty)
                        //{
                        //    gL_JOURNAL_DTL.JE_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.JE_DTL_ID].ToString());
                        //    gL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";// FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                        //    gL_JOURNAL_DTL.MODIFIED_BY = this.LoggedUserName;
                        //    gL_JOURNAL_DTL.MODIFIED_DATE = DateTime.Today;
                        //    tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Update));
                        //}
                        //else
                        //{
                        gL_JOURNAL_DTL.JE_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_D.ToString());
                        gL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";//FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                        gL_JOURNAL_DTL.CREATED_BY = this.LoggedUserName;
                        gL_JOURNAL_DTL.CREATED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(gL_JOURNAL_DTL, FINAppConstants.Add));
                        // }
                    }
                }

                CommonUtils.SavePCEntity<GL_JOURNAL_HDR, GL_JOURNAL_DTL>(gL_JOURNAL_HDR, tmpChildEntity, gL_JOURNAL_DTL);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    savedBool = true;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillComboBox()
        {
            ComboFilling.fn_getOrganisationDetails(ref ddlOrganisation);

            ddlOrganisation.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlOrganisation.Enabled = false;

            Currency_BLL.getCurrencyDetails(ref ddlCurrency);

            if (ddlOrganisation.SelectedValue != string.Empty)
            {
                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlOrganisation.SelectedValue.ToString());
                // ddlGlobalSegment.SelectedValue = "SEG_V_ID-0000000251";
            }

            if (Master.Mode == FINAppConstants.Add)
            {
                Lookup_BLL.GetGLSourceLookUpValues(ref ddlSource, "SOURCE_FIELD");
                Lookup_BLL.GetGLJTLookUpValues(ref ddlJournalType, "JT");
            }
            else
            {
                Lookup_BLL.GetLookUpValues(ref ddlSource, "SOURCE_FIELD");
                Lookup_BLL.GetLookUpValues(ref ddlJournalType, "JT");
            }
            Lookup_BLL.GetLookUpValues(ref ddlVouchertype, "VOUCHER_TYPE");
            ddlVouchertype.SelectedIndex = 1;
            ddlJournalType.SelectedValue = "General";

            //ddlJournalType.SelectedIndex = 1;
            //ddlJournalType_SelectedIndexChanged(ddlJournalType, new EventArgs());
            ddlSource.SelectedIndex = 1;
            SourceChanged();
            if (Session[FINSessionConstants.ORGCurrency] != null)
            {
                ddlCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
                ddlCurrency_SelectedIndexChanged(ddlCurrency, new EventArgs());
            }

            Lookup_BLL.GetLookUpValues(ref ddlFrequency, "FREQ");

        }

        private void FillComboJEDetails()
        {
            ComboFilling.fn_getSegment1Details(ref ddlSegment1);
            ComboFilling.fn_getSegment2Details(ref ddlSegment2);
            ComboFilling.fn_getSegment3Details(ref ddlSegment3);
            ComboFilling.fn_getSegment4Details(ref ddlSegment4);
            ComboFilling.fn_getSegment5Details(ref ddlSegment5);
            ComboFilling.fn_getSegment6Details(ref ddlSegment6);
        }
        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Journal Entry ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtTotCrAmt.Text.ToString().Length == 0)
                    txtTotCrAmt.Text = "0";
                if (txtTotDrAmt.Text.ToString().Length == 0)
                    txtTotDrAmt.Text = "0";
                if (Convert.ToDouble(txtTotDrAmt.Text) != Convert.ToDouble(txtTotCrAmt.Text))
                {
                    ErrorCollection.Add("totdrcrvalue", Prop_File_Data["Credit_and_Debit_equal_P"] + ", But Data Saved Temporary");

                    return;
                }

                string globalOptional = DBMethod.GetStringValue(Organisation_DAL.getCompGlobalSegment());

                if (globalOptional != "0")
                {
                    if (ddlGlobalSegment.SelectedValue == string.Empty)
                    {
                        ErrorCollection.Add("isGlobalOptional", Prop_File_Data["Please_select_global_P"] + ", But Data Saved Temporary");

                        return;
                    }
                }



                AssignToBE();

                //if (Session["Reversal"] != null)
                //{
                //    AssignToRevesalBE();
                //}
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, gL_JOURNAL_HDR.JE_HDR_ID, gL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("GLOBALSEGMENT", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }


                //DisplaySaveCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnReversal_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Session["Reversal"] = null;

                Session["Reversal"] = "Reversed";

                // DataRow drList
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (dtGridData != null)
                {
                    if (dtGridData.Rows.Count > 0)
                    {
                        //drList=dtGridData.Copy();
                        txtJournalNumber.Text = string.Empty;

                        txtJournalNumber.Text = "Reversal - " + Session["ReversalText"];

                        for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                        {
                            if (dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"].ToString().Trim() == "Dr")
                            {
                                //drList["JE_CREDIT_DEBIT"] = "Cr";
                                dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"] = "Cr";

                                // dtGridData.Rows[iLoop]["JE_AMOUNT_DR_CR"] = dtGridData.Rows[iLoop]["JE_AMOUNT_DR_CR"].ToString();
                                dtGridData.Rows[iLoop]["JE_AMOUNT_CR"] = dtGridData.Rows[iLoop]["JE_AMOUNT_DR"];
                                dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_CR"] = dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_DR"];
                                dtGridData.Rows[iLoop]["JE_AMOUNT_DR"] = null;
                                dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_DR"] = null;
                            }
                            else
                            {
                                dtGridData.Rows[iLoop]["JE_CREDIT_DEBIT"] = "Dr";

                                // dtGridData.Rows[iLoop]["JE_AMOUNT_DR_CR"] = dtGridData.Rows[iLoop]["JE_AMOUNT_DR_CR"].ToString();
                                dtGridData.Rows[iLoop]["JE_AMOUNT_DR"] = dtGridData.Rows[iLoop]["JE_AMOUNT_CR"];
                                dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_DR"] = dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_CR"];
                                dtGridData.Rows[iLoop]["JE_AMOUNT_CR"] = null;
                                dtGridData.Rows[iLoop]["JE_ACCOUNTED_AMT_CR"] = null;
                            }
                            dtGridData.AcceptChanges();
                        }

                        BindGrid(dtGridData);

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        # region Grid Events

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;


            DataTable dt_tmp = dtData.Copy();

            if (dt_tmp.Rows.Count > 0)
            {
                txtTotCrAmt.Text = "0";
                txtTotDrAmt.Text = "0";

                txtTotCrAmt.Text = FIN.BLL.CommonUtils.CalculateTotalAmount(dt_tmp, "JE_AMOUNT_CR", "JE_CREDIT_DEBIT='Cr'");
                txtTotDrAmt.Text = FIN.BLL.CommonUtils.CalculateTotalAmount(dt_tmp, "JE_AMOUNT_DR", "JE_CREDIT_DEBIT='Dr'");

                txtTotCrAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotCrAmt.Text);
                txtTotDrAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotDrAmt.Text);

                dt_tmp.AsEnumerable().ToList().ForEach(p => p.SetField<String>("JE_AMOUNT_CR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("JE_AMOUNT_CR"))));
                dt_tmp.AsEnumerable().ToList().ForEach(p => p.SetField<String>("JE_AMOUNT_DR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("JE_AMOUNT_DR"))));
                dt_tmp.AsEnumerable().ToList().ForEach(p => p.SetField<String>("JE_ACCOUNTED_AMT_DR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("JE_ACCOUNTED_AMT_DR"))));
                dt_tmp.AsEnumerable().ToList().ForEach(p => p.SetField<String>("JE_ACCOUNTED_AMT_CR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("JE_ACCOUNTED_AMT_CR"))));
                dt_tmp.AcceptChanges();
            }
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                // dr["ENABLED_FLAG"] = "FALSE";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();

            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);




            //if (Session["cramt"] != null)
            //{
            //    txtTotCrAmt.Text = Session["cramt"].ToString();
            //}
            //if (Session["dramt"] != null)
            //{
            //    txtTotDrAmt.Text = Session["dramt"].ToString();
            //}

            if (hf_WFStatus.Value == "1")
            {
                gvData.FooterRow.Visible = false;
                gvData.Columns[0].Visible = false;
            }
        }

        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Cancel_Edit", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
            try
            {
                ErrorCollection.Clear();
              
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);

                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        ddlJournalType.Focus();
                        return;
                    }
                    else
                    {
                        TempJournalSave(drList, "A");
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                    DropDownList ddlAccountCodes = gvr.FindControl("ddlAccountCodes") as DropDownList;
                    ddlAccountCodes.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void TempJournalSave(DataRow dr_List, string str_Mode)
        {

            if (Master.StrRecordId.ToString() == "0")
            {
                TMP_GL_JOURNAL_HDR tMP_GL_JOURNAL_HDR = new TMP_GL_JOURNAL_HDR();
                if (hf_Temp_RefNo.Value.ToString().Length > 0)
                {
                    using (IRepository<TMP_GL_JOURNAL_HDR> userCtx = new DataRepository<TMP_GL_JOURNAL_HDR>())
                    {
                        tMP_GL_JOURNAL_HDR = userCtx.Find(r =>
                            (r.JE_HDR_ID == hf_Temp_RefNo.Value.ToString())
                            ).SingleOrDefault();
                    }
                }

                tMP_GL_JOURNAL_HDR.JE_COMP_ID = ddlOrganisation.SelectedItem.Value;
                if (txtDate.Text != string.Empty)
                {
                    tMP_GL_JOURNAL_HDR.JE_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                tMP_GL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue.ToString();
                tMP_GL_JOURNAL_HDR.JE_SOURCE = ddlSource.SelectedValue.ToString();
                if (txtRecurringDate.Text != string.Empty)
                {
                    tMP_GL_JOURNAL_HDR.JE_RECURRING_END_DATE = DBMethod.ConvertStringToDate(txtRecurringDate.Text.ToString());
                }
                tMP_GL_JOURNAL_HDR.JE_RECURRING_FREQUENCY = ddlFrequency.SelectedValue;
                tMP_GL_JOURNAL_HDR.JE_TYPE = ddlJournalType.SelectedValue.ToString();
                tMP_GL_JOURNAL_HDR.JE_CURRENCY_ID = ddlCurrency.SelectedValue.ToString();
                tMP_GL_JOURNAL_HDR.JE_PERIOD_ID = ddlPeriods.SelectedValue.ToString();
                tMP_GL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE = txtExchangeRate.Text;//exchange rate value
                tMP_GL_JOURNAL_HDR.JE_TOT_DR_AMOUNT = txtTotDrAmt.Text;//Dr total amount
                tMP_GL_JOURNAL_HDR.JE_TOT_CR_AMOUNT = txtTotCrAmt.Text;//cr total amount
                tMP_GL_JOURNAL_HDR.JE_DESC = txtNarration.Text;
                tMP_GL_JOURNAL_HDR.WORKFLOW_COMPLETION_STATUS = "0";
                if (Session["Reversal"] != null)
                {
                    tMP_GL_JOURNAL_HDR.JOURNAL_REVERSED = "Y";
                    tMP_GL_JOURNAL_HDR.ACTUAL_JOURNAL_ID = gL_JOURNAL_HDR.JE_HDR_ID;
                    tMP_GL_JOURNAL_HDR.JE_REFERENCE = "Reversal - " + txtJournalNumber.Text;
                    tMP_GL_JOURNAL_HDR.JE_NUMBER = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_JN.ToString());
                    tMP_GL_JOURNAL_HDR.JE_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_M.ToString());
                    tMP_GL_JOURNAL_HDR.CREATED_BY = this.LoggedUserName;
                    tMP_GL_JOURNAL_HDR.CREATED_DATE = DateTime.Today;
                    tMP_GL_JOURNAL_HDR.ENABLED_FLAG = "1";

                }
                else
                {
                    tMP_GL_JOURNAL_HDR.JE_REFERENCE = txtJournalNumber.Text;//journal reference number
                    if (hf_Temp_RefNo.Value.ToString().Length > 0)
                    {
                        tMP_GL_JOURNAL_HDR.MODIFIED_BY = this.LoggedUserName;
                        tMP_GL_JOURNAL_HDR.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<TMP_GL_JOURNAL_HDR>(tMP_GL_JOURNAL_HDR, true);
                    }
                    else
                    {
                        tMP_GL_JOURNAL_HDR.JE_NUMBER = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_JN.ToString());
                        tMP_GL_JOURNAL_HDR.JE_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_M.ToString());
                        hf_Temp_RefNo.Value = tMP_GL_JOURNAL_HDR.JE_HDR_ID.ToString();
                        tMP_GL_JOURNAL_HDR.CREATED_BY = this.LoggedUserName;
                        tMP_GL_JOURNAL_HDR.CREATED_DATE = DateTime.Today;
                        tMP_GL_JOURNAL_HDR.ENABLED_FLAG = "1";
                        DBMethod.SaveEntity<TMP_GL_JOURNAL_HDR>(tMP_GL_JOURNAL_HDR);
                    }
                }

                hf_Temp_RefNo.Value = tMP_GL_JOURNAL_HDR.JE_HDR_ID.ToString();



                TMP_GL_JOURNAL_DTL tMP_GL_JOURNAL_DTL = new TMP_GL_JOURNAL_DTL();
                if (str_Mode == "U")
                {
                    using (IRepository<TMP_GL_JOURNAL_DTL> userCtx = new DataRepository<TMP_GL_JOURNAL_DTL>())
                    {
                        tMP_GL_JOURNAL_DTL = userCtx.Find(r =>
                            (r.JE_HDR_ID == hf_Temp_RefNo.Value.ToString() && r.JE_ACCT_CODE_ID == dr_List["ACCT_CODE_ID"].ToString())
                            ).SingleOrDefault();
                    }
                }


                tMP_GL_JOURNAL_DTL.JE_ACCT_CODE_ID = dr_List["ACCT_CODE_ID"].ToString();
                tMP_GL_JOURNAL_DTL.JE_CREDIT_DEBIT = dr_List["JE_CREDIT_DEBIT"].ToString().Trim();// == "Dr" ? "2" : "1";

                if (dr_List["JE_CREDIT_DEBIT"].ToString().Trim() == "Dr")
                {
                    tMP_GL_JOURNAL_DTL.JE_AMOUNT_DR = decimal.Parse(dr_List["JE_AMOUNT_dr"].ToString());
                    tMP_GL_JOURNAL_DTL.JE_ACCOUNTED_AMT_DR = decimal.Parse(dr_List["JE_ACCOUNTED_AMT_DR"].ToString());
                    tMP_GL_JOURNAL_DTL.JE_AMOUNT_CR = null;
                    tMP_GL_JOURNAL_DTL.JE_ACCOUNTED_AMT_CR = null;
                }
                else
                {
                    tMP_GL_JOURNAL_DTL.JE_AMOUNT_CR = decimal.Parse(dr_List["JE_AMOUNT_cr"].ToString());
                    tMP_GL_JOURNAL_DTL.JE_ACCOUNTED_AMT_CR = decimal.Parse(dr_List["JE_ACCOUNTED_AMT_CR"].ToString());
                    tMP_GL_JOURNAL_DTL.JE_AMOUNT_DR = null;
                    tMP_GL_JOURNAL_DTL.JE_ACCOUNTED_AMT_DR = null;
                }

                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_1 = (dr_List["JE_SEGMENT_ID_1"].ToString());
                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_2 = (dr_List["JE_SEGMENT_ID_2"].ToString());
                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_3 = (dr_List["JE_SEGMENT_ID_3"].ToString());
                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_4 = (dr_List["JE_SEGMENT_ID_4"].ToString());
                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_5 = (dr_List["JE_SEGMENT_ID_5"].ToString());
                tMP_GL_JOURNAL_DTL.JE_SEGMENT_ID_6 = (dr_List["JE_SEGMENT_ID_6"].ToString());

                tMP_GL_JOURNAL_DTL.JE_PARTICULARS = (dr_List["JE_PARTICULARS"].ToString());
                tMP_GL_JOURNAL_DTL.ENABLED_FLAG = FINAppConstants.Y;


                tMP_GL_JOURNAL_DTL.JE_HDR_ID = tMP_GL_JOURNAL_HDR.JE_HDR_ID;

                if (dr_List[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {

                }
                else
                {

                    if (Session["Reversal"] != null)
                    {
                        tMP_GL_JOURNAL_DTL.JE_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_D.ToString());
                        tMP_GL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";//FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_JOURNAL_DTL.JE_DTL_ID.ToString());

                        tMP_GL_JOURNAL_DTL.CREATED_BY = this.LoggedUserName;
                        tMP_GL_JOURNAL_DTL.CREATED_DATE = DateTime.Today;

                        DBMethod.SaveEntity<TMP_GL_JOURNAL_DTL>(tMP_GL_JOURNAL_DTL);

                    }
                    else
                    {
                        if (str_Mode == "U")
                        {

                            tMP_GL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                            tMP_GL_JOURNAL_DTL.MODIFIED_BY = this.LoggedUserName;
                            tMP_GL_JOURNAL_DTL.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<TMP_GL_JOURNAL_DTL>(tMP_GL_JOURNAL_DTL, true);
                        }
                        else
                        {
                            tMP_GL_JOURNAL_DTL.JE_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_007_D.ToString());
                            tMP_GL_JOURNAL_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                            tMP_GL_JOURNAL_DTL.CREATED_BY = this.LoggedUserName;
                            gL_JOURNAL_DTL.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<TMP_GL_JOURNAL_DTL>(tMP_GL_JOURNAL_DTL);
                        }
                    }
                }


            }
        }


        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls1 = new System.Collections.SortedList();

            slControls1[0] = ddlJournalType;
            slControls1[1] = ddlVouchertype;
            slControls1[2] = ddlSource;
            slControls1[3] = ddlPeriods;
            slControls1[4] = ddlCurrency;

            string strCtrlTypes1 = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage1 = "Journal Type~Voucher type~Source~Periods~Currency";
            EmptyErrorCollection = CommonUtils.IsValid(slControls1, strCtrlTypes1, strMessage1);

           
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            RadioButtonList chkDrCr = gvr.FindControl("chkDrCr") as RadioButtonList;
            DropDownList ddlAccountCodes = gvr.FindControl("ddlAccountCodes") as DropDownList;
            Label lblCreditDebit = gvr.FindControl("lblCreditDebit") as Label;
            TextBox txtTransCurDr = gvr.FindControl("txtTransCurDr") as TextBox;
            TextBox txtTransCurCr = gvr.FindControl("txtTransCurCr") as TextBox;
            TextBox txtBaseCurDr = gvr.FindControl("txtBaseCurDr") as TextBox;
            TextBox txtBaseCurCr = gvr.FindControl("txtBaseCurCr") as TextBox;
            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtNarration = gvr.FindControl("txtNarration") as TextBox;

            Label lblSegmentValueId1 = gvr.FindControl("lblSegmentValueId1") as Label;
            Label lblSegmentValueId2 = gvr.FindControl("lblSegmentValueId2") as Label;
            Label lblSegmentValueId3 = gvr.FindControl("lblSegmentValueId3") as Label;
            Label lblSegmentValueId4 = gvr.FindControl("lblSegmentValueId4") as Label;
            Label lblSegmentValueId5 = gvr.FindControl("lblSegmentValueId5") as Label;
            Label lblSegmentValueId6 = gvr.FindControl("lblSegmentValueId6") as Label;



            DropDownList ddl_GSegment1 = gvr.FindControl("ddlGSegment1") as DropDownList;
            DropDownList ddl_GSegment2 = gvr.FindControl("ddlGSegment2") as DropDownList;
            DropDownList ddl_GSegment3 = gvr.FindControl("ddlGSegment3") as DropDownList;
            DropDownList ddl_GSegment4 = gvr.FindControl("ddlGSegment4") as DropDownList;
            DropDownList ddl_GSegment5 = gvr.FindControl("ddlGSegment5") as DropDownList;
            DropDownList ddl_GSegment6 = gvr.FindControl("ddlGSegment6") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.JE_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }




            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                if (ErrorCollection.Count > 0)
                {
                    ddlJournalType.Focus();
                    return drList;
                }
            }
            ErrorCollection.Clear();


            //slControls[0] = chkDrCr;
            slControls[0] = ddlAccountCodes;
            slControls[1] = ddl_GSegment1;
            // slControls[2] = ddl_GSegment2;
            //slControls[2] = lblDescription;
            //slControls[3] = lblBalance;
            //slControls[3] = txtTransCurDr;
            //slControls[4] = txtTransCurCr;
            //slControls[5] = txtBaseCurDr;
            //slControls[6] = txtBaseCurCr;
            //slControls[7] = btnDetails;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            //string strCtrlTypes = "RadioButtonList~DropDownList~Label~Label~TextBox~TextBox~TextBox~TextBox~Button";
            //string strMessage = "chkDrCr ~ ddlAccountCodes ~ lblDescription ~ lblBalance ~ txtTransCurDr ~ txtTransCurCr ~ txtBaseCurDr ~ txtBaseCurCr ~ btnDetails";
            string strCtrlTypes = "DropDownList" + "~" + FINAppConstants.DROP_DOWN_LIST;// +"~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Account_Code_P"] + "~" + " Segment1";// +"~" + " Segment2";
            //string strMessage = "Account Codes";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            ErrorCollection.Remove("drcr1");
            ErrorCollection.Remove("drcr2");

            if ((txtTransCurDr.Text.ToString().Trim().Length == 0) && (txtTransCurCr.Text.ToString().Trim().Length == 0))
            {
                ErrorCollection.Add("Invalid Amount", "Please Enter Amount");
                return drList;
            }

            if (txtTransCurDr.Text.ToString().Trim().Length > 0)
            {
                if (decimal.Parse(txtTransCurDr.Text) == 0)
                {
                    txtTransCurDr.Text = "";
                }
            }

            if (txtTransCurCr.Text.ToString().Trim().Length > 0)
            {
                if (decimal.Parse(txtTransCurCr.Text) == 0)
                {
                    txtTransCurCr.Text = "";
                }
            }

            if (txtTransCurDr.Text.ToString().Length > 0)
            {
                chkDrCr.SelectedValue = "2";
            }
            else
            {
                chkDrCr.SelectedValue = "1";
            }


            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //command by krishna by Natesh REFERENCE
            //string strCondition = "ACCT_CODE_ID='" + ddlAccountCodes.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}


            ErrorCollection.Remove("ExchangeRates");

            if (Master.Mode == FINAppConstants.Add)
            {
                if (Session["ExchangeRates"] == null)
                {
                    if (Session["ExchangeRates"].ToString() == "0")
                    {
                        if (ddlExchangeRate.SelectedValue == string.Empty)
                        {
                            ErrorCollection.Add("ExchangeRates", Prop_File_Data["Select_Exchange_rates_P"]);

                        }
                    }
                }
            }

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            txtLineNo.Text = (rowindex + 1).ToString();


            if (ddlAccountCodes.SelectedItem != null)
            {
                drList["ACCT_CODE_ID"] = ddlAccountCodes.SelectedItem.Value;
                drList["ACCT_CODE"] = ddlAccountCodes.SelectedItem.Text;
            }

            drList["ACCT_CODE_DESC"] = ddlAccountCodes.SelectedItem.Text;

            int precision = 0;
            if (Session["precision"] != null)
            {
                if (CommonUtils.ConvertStringToDecimal(Session["precision"].ToString()) > 0)
                {
                    precision = int.Parse(Session["precision"].ToString());
                }
            }
            else
            {
                precision = FINAppConstants.defaultPrecision;
            }

            if (chkDrCr.SelectedItem.Value == "2")//dr
            {
                drList["JE_CREDIT_DEBIT"] = "Dr";


                drList["JE_AMOUNT_DR"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtTransCurDr.Text), precision).ToString();
                drList["JE_ACCOUNTED_AMT_DR"] = Math.Round((CommonUtils.ConvertStringToDecimal(txtTransCurDr.Text) * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text)), precision).ToString();

                drList["JE_AMOUNT_DR_CR"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtTransCurDr.Text), precision).ToString();
                drList["JE_ACCOUNTED_AMT_CR_DR"] = Math.Round((CommonUtils.ConvertStringToDecimal(txtTransCurDr.Text) * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text)), precision).ToString();
                drList["JE_AMOUNT_CR"] = null;
                drList["JE_ACCOUNTED_AMT_CR"] = null;
                drList["JE_AMOUNT_DR_CR"] = null;
                drList["JE_ACCOUNTED_AMT_CR_DR"] = null;

            }
            else //cr
            {
                drList["JE_CREDIT_DEBIT"] = "Cr";

                drList["JE_AMOUNT_CR"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtTransCurCr.Text), precision).ToString();
                drList["JE_ACCOUNTED_AMT_CR"] = Math.Round((CommonUtils.ConvertStringToDecimal(txtTransCurCr.Text) * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text)), precision).ToString();

                drList["JE_AMOUNT_DR_CR"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtTransCurCr.Text), precision).ToString();
                drList["JE_ACCOUNTED_AMT_CR_DR"] = Math.Round((CommonUtils.ConvertStringToDecimal(txtTransCurCr.Text) * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text)), precision).ToString();
                drList["JE_AMOUNT_DR"] = null;
                drList["JE_ACCOUNTED_AMT_DR"] = null;
                drList["JE_AMOUNT_DR_CR"] = null;
                drList["JE_ACCOUNTED_AMT_CR_DR"] = null;

            }


            //drList["JE_SEGMENT_ID_1"] = lblSegmentValueId1.Text;
            //drList["JE_SEGMENT_ID_2"] = lblSegmentValueId2.Text;
            //drList["JE_SEGMENT_ID_3"] = lblSegmentValueId3.Text;
            //drList["JE_SEGMENT_ID_4"] = lblSegmentValueId4.Text;
            //drList["JE_SEGMENT_ID_5"] = lblSegmentValueId5.Text;
            //drList["JE_SEGMENT_ID_6"] = lblSegmentValueId6.Text;
            drList["JE_PARTICULARS"] = txtNarration.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;





            if (ddl_GSegment1.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_1"] = ddl_GSegment1.SelectedValue.ToString();
                drList["SEGMENT_1_TEXT"] = ddl_GSegment1.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_1"] = null;
                drList["SEGMENT_1_TEXT"] = null;
            }

            if (ddl_GSegment2.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_2"] = ddl_GSegment2.SelectedValue.ToString();
                drList["SEGMENT_2_TEXT"] = ddl_GSegment2.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_2"] = null;
                drList["SEGMENT_2_TEXT"] = null;
            }
            if (ddl_GSegment3.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_3"] = ddl_GSegment3.SelectedValue.ToString();
                drList["SEGMENT_3_TEXT"] = ddl_GSegment3.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_3"] = null;
                drList["SEGMENT_3_TEXT"] = null;
            }
            if (ddl_GSegment4.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_4"] = ddl_GSegment4.SelectedValue.ToString();
                drList["SEGMENT_4_TEXT"] = ddl_GSegment4.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_4"] = null;
                drList["SEGMENT_4_TEXT"] = null;
            }
            if (ddl_GSegment5.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_5"] = ddl_GSegment5.SelectedValue.ToString();
                drList["SEGMENT_5_TEXT"] = ddl_GSegment5.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_5"] = null;
                drList["SEGMENT_5_TEXT"] = null;
            }
            if (ddl_GSegment6.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_6"] = ddl_GSegment6.SelectedValue.ToString();
                drList["SEGMENT_6_TEXT"] = ddl_GSegment6.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_6"] = null;
                drList["SEGMENT_6_TEXT"] = null;
            }

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);

                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                else
                {
                    TempJournalSave(drList, "U");
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);

                    DropDownList ddlAccountCodes = gvr.FindControl("ddlAccountCodes") as DropDownList;
                    ddlAccountCodes.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Update", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Delete", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Journal_Entry_Row_Create", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAccCodes = tmpgvr.FindControl("ddlAccountCodes") as DropDownList;
                RadioButtonList chkDrCr = tmpgvr.FindControl("chkDrCr") as RadioButtonList;
                TextBox txtTransCurDr = tmpgvr.FindControl("txtTransCurDr") as TextBox;



                ImageButton ibtnEdit = tmpgvr.FindControl("ibtnEdit") as ImageButton;
                ImageButton ibtnDelete = tmpgvr.FindControl("ibtnDelete") as ImageButton;
                ImageButton ibtnUpdate = tmpgvr.FindControl("ibtnUpdate") as ImageButton;
                ImageButton ibtnInsert = tmpgvr.FindControl("ibtnInsert") as ImageButton;


                //if (Master.Mode != FINAppConstants.Add)
                //{
                //    if (ddlAccCodes != null && chkDrCr != null && txtTransCurDr != null)
                //    {
                //        ddlAccCodes.Enabled = false;
                //        chkDrCr.Enabled = false;
                //        txtTransCurDr.Enabled = false;
                //        gvData.FooterRow.Visible = false;
                //    }
                //}




                if (ddlJournalType.SelectedValue != string.Empty && ddlJournalType.SelectedItem.Text.ToLower() == "fund transfer")
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                    }
                    else
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", true);
                    }
                }
                else
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                    }
                    else
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", false);
                    }
                }



                //if (Master.Mode != FINAppConstants.Add)
                //{
                //    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit");
                //}
                //else
                //{
                //    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add");
                //}
                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccCodes.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ACCT_CODE_ID"].ToString();
                    RadioButtonList rb = (RadioButtonList)tmpgvr.FindControl("chkDrCr");
                    //rb.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_CREDIT_DEBIT"].ToString();
                    //rb.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["JE_CREDIT_DEBIT"].ToString();
                    rb.SelectedIndex = rb.Items.IndexOf(rb.Items.FindByText(gvData.DataKeys[gvData.EditIndex].Values["JE_CREDIT_DEBIT"].ToString()));


                    BindGSegment(tmpgvr);
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment1 = tmpgvr.FindControl("ddlGSegment1") as DropDownList;
                        ddl_GSegment1.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString();
                        LoadGSegmentValues(ddl_GSegment1);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment2 = tmpgvr.FindControl("ddlGSegment2") as DropDownList;
                        ddl_GSegment2.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString();
                        LoadGSegmentValues(ddl_GSegment2);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment3 = tmpgvr.FindControl("ddlGSegment3") as DropDownList;
                        ddl_GSegment3.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString();
                        LoadGSegmentValues(ddl_GSegment3);
                    }


                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment4 = tmpgvr.FindControl("ddlGSegment4") as DropDownList;
                        ddl_GSegment4.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString();
                        LoadGSegmentValues(ddl_GSegment4);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment5 = tmpgvr.FindControl("ddlGSegment5") as DropDownList;
                        ddl_GSegment5.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString();
                        LoadGSegmentValues(ddl_GSegment5);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment6 = tmpgvr.FindControl("ddlGSegment6") as DropDownList;
                        ddl_GSegment6.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString();
                        LoadGSegmentValues(ddl_GSegment6);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JournalEntryFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }


                //GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                //TableHeaderCell cell = new TableHeaderCell();
                //cell.Text = "Transaction Currency";
                //cell.ColumnSpan = 2;
                //row.Controls.Add(cell);

                //gvData.HeaderRow.Parent.Controls.AddAt(4, row);

            }
        }
        protected void btnBase_Click(object sender, EventArgs e)
        {
            mpeBaseCurrency.Show();
        }

        private void BindSegmentValues(int rowIndexValue)
        {
            if (Master.Mode != FINAppConstants.Add)
            {
                if (gvData.Rows.Count > 0)
                {
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment1);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment2);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment3);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment5);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment6);

                    //for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    //{
                    int iLoop = 0;
                    iLoop = rowIndexValue;

                    Session["Accountcode"] = gvData.DataKeys[iLoop].Values["ACCT_CODE_ID"].ToString();

                    if (Master.Mode != FINAppConstants.Add)
                    {
                        BindSegment();

                        string seg1 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_1].ToString();
                        string seg2 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_2].ToString();
                        string seg3 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_3].ToString();
                        string seg4 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_4].ToString();
                        string seg5 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_5].ToString();
                        string seg6 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_6].ToString();

                        ddlSegment1.SelectedValue = seg1;
                        ddlSegment2.SelectedValue = seg2;
                        ddlSegment3.SelectedValue = seg3;
                        ddlSegment4.SelectedValue = seg4;
                        ddlSegment5.SelectedValue = seg5;
                        ddlSegment6.SelectedValue = seg6;
                    }
                    //}
                }
            }
        }
        private void BindSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccountCodes");

            if (ddl_AccountCodes.SelectedValue != null && ddlGlobalSegment.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty && ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {
                        Label lblSegment = (Label)Master.FindControl("FINContent").FindControl("lblSegment" + (iLoop + 1));
                        lblSegment.Text = dtdataAc.Rows[iLoop][FINColumnConstants.SEGMENT_NAME].ToString();
                        if (iLoop == 0)
                        {
                            DropDownList ddlSegment = (DropDownList)Master.FindControl("FINContent").FindControl("ddlSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlSegment = (DropDownList)Master.FindControl("FINContent").FindControl("ddlSegment" + (iLoop + 1));
                            ddlSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["JE_SEGMENT_ID_" + (iLoop + 1)].ToString();
                            LoadSegmentValues(ddlSegment);
                        }
                    }
                }
            }
        }


        private void BindGSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccountCodes");

            if (ddl_AccountCodes.SelectedValue != null && ddlGlobalSegment.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty && ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {
                        //Label lblSegment = (Label)gvr.FindControl("lblSegment" + (iLoop + 1));
                        //lblSegment.Text = dtdataAc.Rows[iLoop][FINColumnConstants.SEGMENT_NAME].ToString();
                        if (iLoop == 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlGSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            ddlGSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["JE_SEGMENT_ID_" + (iLoop + 1)].ToString();

                        }
                    }
                }
            }
        }

        protected void ddlGSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGSegmentValues(sender);


        }
        private void LoadGSegmentValues(object sender)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Seg = (DropDownList)sender;
            int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlGSegment", ""));
            DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (int_ddlNo + 1));
            if (ddlGSegment != null && int_ddlNo < dtdataAc.Rows.Count)
            {
                if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                }
                else
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                }

            }
            ddlGSegment.Focus();
        }


        protected void ddlSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSegmentValues(sender);
            ModalPopupExtender2.Show();

        }
        private void LoadSegmentValues(object sender)
        {
            try
            {
                DropDownList ddl_Seg = (DropDownList)sender;
                int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlSegment", ""));
                DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
                DropDownList ddlSegment = (DropDownList)Master.FindControl("FINContent").FindControl("ddlSegment" + (int_ddlNo + 1));
                if (ddlSegment != null && int_ddlNo < dtdataAc.Rows.Count)
                {
                    if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                    {
                        Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                    }
                    else
                    {
                        Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                    }
                }
                // ddlSegment.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JournalEntryFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnClick_Click(object sender, EventArgs e)
        {
            Session["AccCode_Seg"] = null;
            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;

            BindSegment(gridViewRow);
            //int index = gridViewRow.RowIndex;
            // BindSegmentValues(index);


            // FillComboJEDetails();
            //DataTable dtperiod = new DataTable();
            //dtperiod = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getSegmentDetails(Master.StrRecordId)).Tables[0];

            //ddlSegment1.SelectedItem.Value = dtperiod.Rows[0]["segment_value"].ToString();
            //ddlSegment2.SelectedItem.Value = dtperiod.Rows[1]["segment_value"].ToString();
            //ddlSegment3.SelectedItem.Value = dtperiod.Rows[2]["segment_value"].ToString();
            //ddlSegment4.SelectedItem.Value = dtperiod.Rows[3]["segment_value"].ToString();
            //ddlSegment5.SelectedItem.Value = dtperiod.Rows[4]["segment_value"].ToString();
            //ddlSegment6.SelectedItem.Value = dtperiod.Rows[5]["segment_value"].ToString();


            //Label lblSegmentValueId1 = new Label();
            //Label lblSegmentValueId2 = new Label();
            //Label lblSegmentValueId3 = new Label();
            //Label lblSegmentValueId4 = new Label();
            //Label lblSegmentValueId5 = new Label();
            //Label lblSegmentValueId6 = new Label();

            //lblSegmentValueId1.ID = "lblSegmentValueId1";
            //lblSegmentValueId2.ID = "lblSegmentValueId2";
            //lblSegmentValueId3.ID = "lblSegmentValueId3";
            //lblSegmentValueId4.ID = "lblSegmentValueId4";
            //lblSegmentValueId5.ID = "lblSegmentValueId5";
            //lblSegmentValueId6.ID = "lblSegmentValueId6";

            //if (gvData.FooterRow != null)
            //{
            //    if (gvData.EditIndex < 0)
            //    {
            //        lblSegmentValueId1 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId1");
            //        lblSegmentValueId2 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId2");
            //        lblSegmentValueId3 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId3");
            //        lblSegmentValueId4 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId4");
            //        lblSegmentValueId5 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId5");
            //        lblSegmentValueId6 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId6");
            //    }
            //    else
            //    {
            //        lblSegmentValueId1 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId1");
            //        lblSegmentValueId2 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId2");
            //        lblSegmentValueId3 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId3");
            //        lblSegmentValueId4 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId4");
            //        lblSegmentValueId5 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId5");
            //        lblSegmentValueId6 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId6");
            //    }
            //}
            //else
            //{
            //    lblSegmentValueId1 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId1");
            //    lblSegmentValueId2 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId2");
            //    lblSegmentValueId3 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId3");
            //    lblSegmentValueId4 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId4");
            //    lblSegmentValueId5 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId5");
            //    lblSegmentValueId6 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId6");
            //}

            //if (lblSegmentValueId1 != null && lblSegmentValueId2 != null && lblSegmentValueId3 != null && lblSegmentValueId4 != null && lblSegmentValueId5 != null && lblSegmentValueId6 != null)
            //{
            //    ddlSegment1.SelectedItem.Value = lblSegmentValueId1.Text;
            //    ddlSegment2.SelectedItem.Value = lblSegmentValueId2.Text;
            //    ddlSegment3.SelectedItem.Value = lblSegmentValueId3.Text;
            //    ddlSegment4.SelectedItem.Value = lblSegmentValueId4.Text;
            //    ddlSegment5.SelectedItem.Value = lblSegmentValueId5.Text;
            //    ddlSegment6.SelectedItem.Value = lblSegmentValueId6.Text;

            //}




            ModalPopupExtender2.Show();
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {
            Label lblSegmentValueId1 = new Label();
            Label lblSegmentValueId2 = new Label();
            Label lblSegmentValueId3 = new Label();
            Label lblSegmentValueId4 = new Label();
            Label lblSegmentValueId5 = new Label();
            Label lblSegmentValueId6 = new Label();

            lblSegmentValueId1.ID = "lblSegmentValueId1";
            lblSegmentValueId2.ID = "lblSegmentValueId2";
            lblSegmentValueId3.ID = "lblSegmentValueId3";
            lblSegmentValueId4.ID = "lblSegmentValueId4";
            lblSegmentValueId5.ID = "lblSegmentValueId5";
            lblSegmentValueId6.ID = "lblSegmentValueId6";

            if (gvData.FooterRow != null)
            {
                if (gvData.EditIndex < 0)
                {
                    lblSegmentValueId1 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId1");
                    lblSegmentValueId2 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId2");
                    lblSegmentValueId3 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId3");
                    lblSegmentValueId4 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId4");
                    lblSegmentValueId5 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId5");
                    lblSegmentValueId6 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId6");
                }
                else
                {
                    lblSegmentValueId1 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId1");
                    lblSegmentValueId2 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId2");
                    lblSegmentValueId3 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId3");
                    lblSegmentValueId4 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId4");
                    lblSegmentValueId5 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId5");
                    lblSegmentValueId6 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId6");
                }
            }
            else
            {
                lblSegmentValueId1 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId1");
                lblSegmentValueId2 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId2");
                lblSegmentValueId3 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId3");
                lblSegmentValueId4 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId4");
                lblSegmentValueId5 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId5");
                lblSegmentValueId6 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId6");
            }

            if (lblSegmentValueId1 != null && lblSegmentValueId2 != null && lblSegmentValueId3 != null && lblSegmentValueId4 != null && lblSegmentValueId5 != null && lblSegmentValueId6 != null)
            {
                lblSegmentValueId1.Text = ddlSegment1.SelectedValue;
                lblSegmentValueId2.Text = ddlSegment2.SelectedValue;
                lblSegmentValueId3.Text = ddlSegment3.SelectedValue;
                lblSegmentValueId4.Text = ddlSegment4.SelectedValue;
                lblSegmentValueId5.Text = ddlSegment5.SelectedValue;
                lblSegmentValueId6.Text = ddlSegment6.SelectedValue;
            }

            ClearSegment();
            ModalPopupExtender2.Hide();
        }
        private void ClearSegment()
        {
            lblSegment1.Text = "Segment 1";
            lblSegment2.Text = "Segment 2";
            lblSegment3.Text = "Segment 3";
            lblSegment4.Text = "Segment 4";
            lblSegment5.Text = "Segment 5";
            lblSegment6.Text = "Segment 6";
            
            ddlSegment1.Items.Clear();
            ddlSegment2.Items.Clear();
            ddlSegment3.Items.Clear();
            ddlSegment4.Items.Clear();
            ddlSegment5.Items.Clear();
            ddlSegment6.Items.Clear();
        }


        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    //dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    gL_JOURNAL_HDR.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                //    DBMethod.DeleteEntity<GL_JOURNAL_HDR>(gL_JOURNAL_HDR);
                //}
                gL_JOURNAL_HDR.PK_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<GL_JOURNAL_HDR>(gL_JOURNAL_HDR);
                DisplaySaveCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void chkDrCr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = gvData.FooterRow;

                RadioButtonList chkDrCr = gvr.FindControl("chkDrCr") as RadioButtonList;

                TextBox txtTransCurDr = gvr.FindControl("txtTransCurDr") as TextBox;
                //   TextBox txtTransCurCr = gvr.FindControl("txtTransCurCr") as TextBox;

                TextBox txtBaseCurDr = gvr.FindControl("txtBaseCurDr") as TextBox;
                TextBox txtBaseCurCr = gvr.FindControl("txtBaseCurCr") as TextBox;

                if (chkDrCr.SelectedValue == "1")
                {
                    // txtTransCurCr.Enabled = false;
                    txtBaseCurCr.Enabled = false;

                    txtTransCurDr.Enabled = true;
                    txtBaseCurDr.Enabled = false;
                }
                else if (chkDrCr.SelectedValue == "2")
                {
                    //   txtTransCurCr.Enabled = false;
                    txtBaseCurCr.Enabled = false;

                    txtTransCurDr.Enabled = true;
                    txtBaseCurDr.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtDate_TextChanged(object sender, EventArgs e)
        {

        }
        private void ClearGrid()
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                gvData.DataSource = dtGridData;
                gvData.DataBind();

                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
        }
        protected void ddlJournalType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.FooterRow;
                DropDownList ddlAccCodes = gvr.FindControl("ddlAccountCodes") as DropDownList;

                if (ddlJournalType.SelectedItem.Text.ToLower() == "fund transfer")
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                    }
                    else
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", true);
                    }
                }
                else
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                    }
                    else
                    {
                        AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", false);
                    }
                }
                // ddlJournalType.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlGlobalSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dtData = new DataTable();
            //dtData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvalues(ddlOrganisation.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue.ToString())).Tables[0];
            //   ddlGlobalSegment.Focus();
            GridViewRow gvr = gvData.FooterRow;
            DropDownList ddlAccCodes = gvr.FindControl("ddlAccountCodes") as DropDownList;

            if (ddlJournalType.SelectedItem.Text.ToLower() == "fund transfer")
            {
                if (Master.Mode != FINAppConstants.Add)
                {
                    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                }
                else
                {
                    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", true);
                }
            }
            else
            {
                if (Master.Mode != FINAppConstants.Add)
                {
                    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "edit", false);
                }
                else
                {
                    AccountCodes_BLL.getNonCtrlAccCodeBasedOrg(ref ddlAccCodes, ddlOrganisation.SelectedValue.ToString(), "add", false);
                }
            }
        }
        private void BindSegment()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                DropDownList ddlAccountCodes = new DropDownList();
                ddlAccountCodes.ID = "ddlAccountCodes";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {

                        ddlAccountCodes = (DropDownList)gvData.FooterRow.FindControl("ddlAccountCodes");
                    }
                    else
                    {
                        ddlAccountCodes = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountCodes");
                    }
                }
                else
                {
                    ddlAccountCodes = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountCodes");
                }


                if (ddlAccountCodes != null)
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        if (Session["Accountcode"] != null)
                        {
                            ddlAccountCodes.SelectedValue = Session["Accountcode"].ToString();
                        }
                    }
                    if (ddlAccountCodes.SelectedValue != null)
                    {
                        dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddlAccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                        if (dtdataAc != null)
                        {
                            if (dtdataAc.Rows.Count > 0)
                            {
                                int j = dtdataAc.Rows.Count;

                                for (int i = 0; i < dtdataAc.Rows.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        //segment1
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }

                                    }
                                    if (i == 1)
                                    {
                                        //segment2
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 2)
                                    {
                                        //Segments3
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 3)
                                    {
                                        //segment4
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 4)
                                    {
                                        //segment5
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 5)
                                    {
                                        //segment6
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                }
                                if (j == 1)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 2)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 3)
                                {
                                    // Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, VMVServices.Web.Utils.OrganizationID);

                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 4)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 5)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
                //throw ex;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtDat = new DataTable();


                DropDownList ddlAccountCodes = new DropDownList();
                Label lblDescription = new Label();
                Label lblBalance = new Label();

                ddlAccountCodes.ID = "ddlAccountCodes";
                lblDescription.ID = "lblDescription";
                lblBalance.ID = "lblBalance";


                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        lblBalance = (Label)gvData.FooterRow.FindControl("lblBalance");
                        lblDescription = (Label)gvData.FooterRow.FindControl("lblDescription");
                        ddlAccountCodes = (DropDownList)gvData.FooterRow.FindControl("ddlAccountCodes");
                    }
                    else
                    {
                        lblBalance = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblBalance");
                        lblDescription = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblDescription");
                        ddlAccountCodes = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountCodes");
                    }
                }
                else
                {
                    lblBalance = (Label)gvData.Controls[0].Controls[1].FindControl("lblBalance");
                    lblDescription = (Label)gvData.Controls[0].Controls[1].FindControl("lblDescription");
                    ddlAccountCodes = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountCodes");
                }

                if (ddlAccountCodes != null && lblDescription != null)
                {
                    dtDat = DBMethod.ExecuteQuery(AccountCodes_DAL.getAccCodeBasedOrg(ddlOrganisation.SelectedValue.ToString(), ddlAccountCodes.SelectedValue.ToString())).Tables[0];

                    if (dtDat != null)
                    {
                        if (dtDat.Rows.Count > 0)
                        {
                            lblDescription.Text = dtDat.Rows[0][FINColumnConstants.acct_code_desc].ToString();
                            //if (dtDat.Rows[0][FINColumnConstants.ACCT_TYPE].ToString() == "B")
                            //{
                            //    gvData.Enabled = true;
                            //}
                            //else
                            //{
                            //    gvData.Enabled = false;
                            //}

                        }
                    }
                    //BindSegment();
                }
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                BindGSegment(gvr);
                //ddlAccountCodes.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlExchangeRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                DataTable dtDat = new DataTable();
                dtDat = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtDate.Text, ddlExchangeRate.SelectedValue.ToString())).Tables[0];

                if (dtDat != null)
                {
                    if (dtDat.Rows.Count > 0)
                    {
                        txtExchangeRate.Text = dtDat.Rows[0][FINColumnConstants.CURRENCY_STD_RATE].ToString();
                    }
                }
                ddlExchangeRate.Focus();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlOrganisation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Currency_BLL.getCurrencyDetails(ref ddlCurrency);
                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlOrganisation.SelectedValue.ToString());
                GridViewRow tmpgvr = gvData.FooterRow;
                FillFooterGridCombo(tmpgvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtDate_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //   ExchangeRate_BLL.GetExchangeRate(ref ddlExchangeRate, (txtDate.Text));

                AccountingCalendar_BLL.GetCompPeriodBasedOrgJDate(ref ddlPeriods, (txtDate.Text));
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                SSM_CURRENCIES SSM_CURRENCIES = new SSM_CURRENCIES();

                using (IRepository<SSM_CURRENCIES> ssm = new DataRepository<SSM_CURRENCIES>())
                {
                    SSM_CURRENCIES = ssm.Find(x => (x.CURRENCY_ID == ddlCurrency.SelectedValue.ToString())).SingleOrDefault();
                    if (SSM_CURRENCIES != null)
                    {
                        Session["precision"] = SSM_CURRENCIES.STANDARD_PRECISION;
                    }
                }

                DataTable dtDat = new DataTable();
                dtDat = DBMethod.ExecuteQuery(Currency_DAL.getCurrencyBasedOrg(ddlOrganisation.SelectedValue.ToString(), txtDate.Text)).Tables[0];

                if (dtDat != null)
                {
                    if (dtDat.Rows.Count > 0)
                    {
                        if (ddlCurrency.SelectedValue == dtDat.Rows[0][FINColumnConstants.CURRENCY_ID].ToString())
                        {
                            // ddlExchangeRate.Enabled = false;
                            txtExchangeRate.Enabled = false;
                            Session["ExchangeRates"] = "1";
                            txtExchangeRate.Text = "1";
                            // ExchangeRate_BLL.GetExchangeRate(ref ddlExchangeRate, (txtDate.Text));
                        }
                        else
                        {
                            // ddlExchangeRate.Enabled = true;
                            txtExchangeRate.Enabled = true;
                            Session["ExchangeRates"] = "0";
                            //txtExchangeRate.Text = "1";
                            //ExchangeRate_BLL.GetExchangeRate(ref ddlExchangeRate, (txtDate.Text));

                            DataTable dtDat1 = new DataTable();
                            dtDat1 = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtDate.Text, ddlCurrency.SelectedValue.ToString())).Tables[0];
                            // dtDat1 = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtDate.Text)).Tables[0];
                            // dtDat1 = ExchangeRate_BLL.getStandardRate(ddlCurrency.SelectedValue, (txtDate.Text));
                            if (dtDat1 != null)
                            {
                                if (dtDat1.Rows.Count > 0)
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_STD_RATE"].ToString();
                                }
                                else
                                {
                                    txtExchangeRate.Text = "1";
                                }
                            }
                            else
                            {
                                txtExchangeRate.Text = "1";
                            }
                        }
                    }
                }
                ddlCurrency.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnPrintReport_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";

                if (Session["JENUM"] != null)
                {
                    htFilterParameter.Add("JE_NUMBER", Session["JENUM"].ToString());
                    ReportData = DBMethod.ExecuteQuery(JournalEntry_DAL.GetJournalVoucherDetailsRPT(Session["JENUM"].ToString()));
                }

                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCrpt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void SourceChanged()
        {
            try
            {
                ErrorCollection.Clear();

                if (ddlSource.SelectedValue != string.Empty)
                {
                    if (ddlSource.SelectedItem.Text == "Recurring")
                    {
                        ddlFrequency.Enabled = true;
                        txtRecurringDate.Enabled = true;
                    }
                    else
                    {
                        ddlFrequency.Enabled = false;
                        txtRecurringDate.Enabled = false;
                    }
                }
                ddlSource.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCrpt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            SourceChanged();
        }
        #endregion

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            ClearSegment();
        }

        protected void btnSendApprove_Click(object sender, EventArgs e)
        {
            try
            {
                string str_workflowstatus = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, Master.StrRecordId);
                DBMethod.ExecuteNonQuery("Update GL_JOURNAL_HDR set Workflow_Completion_Status=" + str_workflowstatus + ",ATTRIBUTE1='SEND',POSTED_FLAG='" + FINAppConstants.Y +"',POSTED_DATE='" +DateTime.Today+"' where JE_HDR_ID='" + Master.StrRecordId + "'");
                btnSendApprove.Enabled = false;
            }
            catch (Exception EX)
            {
            }

        }
        protected void btnTmpJDelete_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Temp_RefNo.Value = gvTempJournal.DataKeys[gvr.RowIndex].Values["JE_HDR_ID"].ToString();
            if (hf_Temp_RefNo.Value.ToString().Length > 0)
            {
                DBMethod.ExecuteNonQuery("DELETE TMP_GL_JOURNAL_DTL WHERE JE_HDR_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                DBMethod.ExecuteNonQuery("DELETE TMP_GL_JOURNAL_HDR WHERE JE_HDR_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
            }
            hf_Temp_RefNo.Value = "";
            LoadTempJournal();
            mpeTempJournal.Show();
        }

        protected void btnTmpJselect_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Temp_RefNo.Value = gvTempJournal.DataKeys[gvr.RowIndex].Values["JE_HDR_ID"].ToString();
            TMP_GL_JOURNAL_HDR tMP_GL_JOURNAL_HDR = new TMP_GL_JOURNAL_HDR();
            using (IRepository<TMP_GL_JOURNAL_HDR> userCtx = new DataRepository<TMP_GL_JOURNAL_HDR>())
            {
                tMP_GL_JOURNAL_HDR = userCtx.Find(r =>
                    (r.JE_HDR_ID == hf_Temp_RefNo.Value)
                    ).SingleOrDefault();
            }


            ddlOrganisation.SelectedValue = tMP_GL_JOURNAL_HDR.JE_COMP_ID;

            Currency_BLL.getCurrencyDetails(ref ddlCurrency);
            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlOrganisation.SelectedValue.ToString());

            if (tMP_GL_JOURNAL_HDR.JE_DATE != null)
            {
                txtDate.Text = DBMethod.ConvertDateToString(tMP_GL_JOURNAL_HDR.JE_DATE.ToString());
                AccountingCalendar_BLL.GetCompPeriodBasedOrgJDate(ref ddlPeriods, (txtDate.Text));
            }
            if (tMP_GL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID != null)
            {
                ddlGlobalSegment.SelectedValue = tMP_GL_JOURNAL_HDR.JE_GLOBAL_SEGMENT_ID.ToString();
            }
            if (tMP_GL_JOURNAL_HDR.JE_TYPE != null)
            {
                ddlJournalType.SelectedValue = tMP_GL_JOURNAL_HDR.JE_TYPE;
            }

            if (tMP_GL_JOURNAL_HDR.JE_SOURCE != null)
            {
                ddlSource.SelectedValue = tMP_GL_JOURNAL_HDR.JE_SOURCE;
            }

            if (tMP_GL_JOURNAL_HDR.JE_CURRENCY_ID != null)
            {
                ddlCurrency.SelectedValue = tMP_GL_JOURNAL_HDR.JE_CURRENCY_ID.ToString();
                ddlCurrency_SelectedIndexChanged(sender, e);
            }

            if (tMP_GL_JOURNAL_HDR.JE_CURRENCY_RATE_ID != string.Empty && tMP_GL_JOURNAL_HDR.JE_CURRENCY_RATE_ID != null)
            {
                ddlExchangeRate.SelectedValue = tMP_GL_JOURNAL_HDR.JE_CURRENCY_RATE_ID.ToString();
            }

            if (tMP_GL_JOURNAL_HDR.JE_PERIOD_ID != null)
            {
                ddlPeriods.SelectedValue = tMP_GL_JOURNAL_HDR.JE_PERIOD_ID.ToString();
            }

            if (tMP_GL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE != null)
            {
                txtExchangeRate.Text = tMP_GL_JOURNAL_HDR.JE_EXCHANGE_RATE_VALUE.ToString();
            }

            if (tMP_GL_JOURNAL_HDR.JE_REFERENCE != null)
            {
                txtJournalNumber.Text = tMP_GL_JOURNAL_HDR.JE_REFERENCE;
            }

            if (tMP_GL_JOURNAL_HDR.JE_DESC != null)
            {
                txtNarration.Text = tMP_GL_JOURNAL_HDR.JE_DESC;
            }

            txtTotDrAmt.Text = tMP_GL_JOURNAL_HDR.JE_TOT_DR_AMOUNT;
            txtTotCrAmt.Text = tMP_GL_JOURNAL_HDR.JE_TOT_CR_AMOUNT;

            Session["cramt"] = tMP_GL_JOURNAL_HDR.JE_TOT_CR_AMOUNT;
            Session["dramt"] = tMP_GL_JOURNAL_HDR.JE_TOT_DR_AMOUNT;
            Session["JENUM"] = tMP_GL_JOURNAL_HDR.JE_NUMBER;

            if (tMP_GL_JOURNAL_HDR.JE_RECURRING_END_DATE != null)
            {
                txtRecurringDate.Text = DBMethod.ConvertDateToString(tMP_GL_JOURNAL_HDR.JE_RECURRING_END_DATE.ToString());
            }

            if (tMP_GL_JOURNAL_HDR.JE_RECURRING_FREQUENCY != null)
            {
                ddlFrequency.SelectedValue = tMP_GL_JOURNAL_HDR.JE_RECURRING_FREQUENCY;
            }
            SourceChanged();
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.get_TEMP_JEDetails(hf_Temp_RefNo.Value)).Tables[0];
            BindGrid(dtGridData);
        }

     

       
    }
}