﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class BudgetEntry : PageBase
    {
        GL_BUDGET_HDR gL_BUDGET_HDR = new GL_BUDGET_HDR();
        GL_BUDGET_DTL gL_BUDGET_DTL = new GL_BUDGET_DTL();
        DataTable dtGridData = new DataTable();
        DataTable dtGS = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                    dtGS = DBMethod.ExecuteQuery(Budget_DAL.getGlobalSegment()).Tables[0];
                    hfgs.Value = dtGS.Rows[0]["GLOBAL_SEGMENT_BALANCED"].ToString();

                    if (hfgs.Value != "0")
                    {
                        ddlGlobalSegment.Enabled = true;
                    }
                    else
                    {
                        ddlGlobalSegment.Enabled = false;
                    }


                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        protected void ddlOrganisation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlCompanyName.SelectedValue.ToString());

                //dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg(ddlCompanyName.SelectedValue.ToString());
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

               
                EntityData = null;

                dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg("0", "0");

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    

                    divFilter.Visible = false;

                    rblAccCodeGroup.Enabled = false;
                    ddlCompanyName.Enabled = false;
                    ddlGlobalSegment.Enabled = false;


                    dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId);
                    using (IRepository<GL_BUDGET_HDR> userCtx = new DataRepository<GL_BUDGET_HDR>())
                    {
                        gL_BUDGET_HDR = userCtx.Find(r =>
                            (r.BUDGET_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //AccountingGroupLinks_BLL.getClassEntity(Master.StrRecordId);

                    if (gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        gvData.Enabled = false;
                        btnSave.Visible = false;
                    }


                    EntityData = gL_BUDGET_HDR;

                    txtBudgetName.Text = gL_BUDGET_HDR.BUDGET_NAME;
                    ddlCompanyName.SelectedValue = gL_BUDGET_HDR.BUDGET_COMP;

                    ddlBudgetType.SelectedValue = gL_BUDGET_HDR.BUDGET_TYPE;
                    ddlCalyr.SelectedValue = gL_BUDGET_HDR.CAL_DTL_ID;
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlCompanyName.SelectedValue.ToString());

                    ddlGlobalSegment.SelectedValue = gL_BUDGET_HDR.ATTRIBUTE1;

                    if (gL_BUDGET_HDR.ATTRIBUTE2 == "1")
                    {
                        rblAccCodeGroup.SelectedValue = "1";
                    }
                    else
                    {
                        rblAccCodeGroup.SelectedValue = "2";
                    }

                    if (gL_BUDGET_HDR.BUDGET_PERIOD_START_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_START_DT.ToString());
                    }
                    if (gL_BUDGET_HDR.BUDGET_PERIOD_END_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_END_DT.ToString());
                    }

                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                DataTable dtPeriods = new DataTable();
                DataTable dtSegments = new DataTable();

                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_BUDGET_HDR = (GL_BUDGET_HDR)EntityData;
                }



                gL_BUDGET_HDR.BUDGET_NAME = txtBudgetName.Text;
                gL_BUDGET_HDR.BUDGET_COMP = ddlCompanyName.SelectedValue;
                gL_BUDGET_HDR.BUDGET_TYPE = ddlBudgetType.SelectedValue;
                gL_BUDGET_HDR.CAL_DTL_ID = ddlCalyr.SelectedValue;
                gL_BUDGET_HDR.ATTRIBUTE1 = ddlGlobalSegment.SelectedValue;
                gL_BUDGET_HDR.ATTRIBUTE2 = rblAccCodeGroup.SelectedValue;

                if (txtFromDate.Text.Trim() != string.Empty)
                {
                    gL_BUDGET_HDR.BUDGET_PERIOD_START_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text.Trim() != string.Empty)
                {
                    gL_BUDGET_HDR.BUDGET_PERIOD_END_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                gL_BUDGET_HDR.ENABLED_FLAG = "1";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_BUDGET_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    gL_BUDGET_HDR.BUDGET_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003.ToString(), false, true);

                    gL_BUDGET_HDR.CREATED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.CREATED_DATE = DateTime.Today;
                }
                gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_BUDGET_HDR.BUDGET_CODE.ToString());


                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Budget Entry");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        gL_BUDGET_DTL = new GL_BUDGET_DTL();

                        if (gvData.DataKeys[iLoop].Values["BUDGET_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["BUDGET_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<GL_BUDGET_DTL> userCtx = new DataRepository<GL_BUDGET_DTL>())
                            {
                                gL_BUDGET_DTL = userCtx.Find(r =>
                                    (r.BUDGET_DTL_ID == gvData.DataKeys[iLoop].Values["BUDGET_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }



                        gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = gvData.DataKeys[iLoop].Values[FINColumnConstants.GROUP_ID].ToString();
                        gL_BUDGET_DTL.BUDGET_ACCT_CODE_ID = gvData.DataKeys[iLoop].Values[FINColumnConstants.CODE_ID].ToString();
                        TextBox txt_Percentage = (TextBox)gvData.Rows[iLoop].FindControl("txtPercentage");
                        if (txt_Percentage.Text.ToString().Length > 0)
                        {
                            gL_BUDGET_DTL.PERCENTAGE = decimal.Parse(txt_Percentage.Text);
                        }
                        else
                        {
                            gL_BUDGET_DTL.PERCENTAGE = null;
                        }
                        TextBox txtActualPreviousAmt = (TextBox)gvData.Rows[iLoop].FindControl("txtActualPreviousAmt");
                        if (txtActualPreviousAmt.Text.ToString().Length > 0)
                        {
                            gL_BUDGET_DTL.ACTUAL_PREVIOUS_AMOUNT = decimal.Parse(gvData.DataKeys[iLoop].Values["ACTUAL_PREVIOUS_AMOUNT"].ToString());
                        }
                        else
                        {
                            gL_BUDGET_DTL.ACTUAL_PREVIOUS_AMOUNT = null;
                        }

                        TextBox txtAnnualBudgetAmount = (TextBox)gvData.Rows[iLoop].FindControl("txtAnnualBudgetAmount");


                        if (txtAnnualBudgetAmount.Text.Trim() != string.Empty)
                        {
                            if (Session[iLoop + "_Periods"] != null)
                            {
                                dtPeriods = (DataTable)Session[iLoop + "_Periods"];
                            }
                            if (Session[iLoop + "_Segments"] != null)
                            {
                                dtSegments = (DataTable)Session[iLoop + "_Segments"];
                            }

                            if (dtPeriods != null)
                            {
                                if (dtPeriods.Rows.Count > 0)
                                {
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_01 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_01"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_02 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_02"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_03 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_03"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_04 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_04"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_05 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_05"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_06 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_06"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_07 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_07"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_08 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_08"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_09 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_09"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_10 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_10"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_11 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_11"].ToString());
                                    gL_BUDGET_DTL.BUDGET_MONTH_AMT_12 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_12"].ToString());
                                }
                            }

                            if (dtSegments != null)
                            {
                                if (dtSegments.Rows.Count > 0)
                                {
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_1 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString();
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_2 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString();
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_3 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString();
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_4 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString();
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_5 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString();
                                    gL_BUDGET_DTL.BUDGET_SEGMENT_ID_6 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString();
                                }
                            }


                            gL_BUDGET_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                            gL_BUDGET_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            gL_BUDGET_DTL.BUDGET_CODE = gL_BUDGET_HDR.BUDGET_CODE;

                            if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                            {
                                tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Delete));
                            }
                            else
                            {
                                if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString() != string.Empty)
                                {
                                    gL_BUDGET_DTL.MODIFIED_DATE = DateTime.Today;
                                    tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Update));
                                }
                                else
                                {
                                    gL_BUDGET_DTL.BUDGET_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003_D.ToString(), false, true);
                                    gL_BUDGET_DTL.CREATED_BY = this.LoggedUserName;
                                    gL_BUDGET_DTL.CREATED_DATE = DateTime.Today;
                                    tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Add));
                                }
                            }
                        }
                    }
                }
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, gL_BUDGET_HDR.BUDGET_CODE, gL_BUDGET_HDR.BUDGET_COMP, gL_BUDGET_HDR.BUDGET_NAME, gL_BUDGET_HDR.BUDGET_TYPE);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("BUDGET", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillComboBox()
        {
            // ComboFilling.fn_getGlobalSegment(ref ddlGlobalSegment);
            ComboFilling.fn_getOrganisationDetails(ref ddlCompanyName);

            //ComboFilling.fn_getBudgetType(ref ddlBudgetType);

            Lookup_BLL.GetLookUpValues(ref ddlBudgetType, "BT");
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalYear(ref ddlCalyr);
            FIN.BLL.GL.AccountingGroups_BLL.getGroupDetails(ref ddlAccountGroupFilter);
            ddlCompanyName.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlCompanyName.Enabled = false;


            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);

        }
        private void FillComboJEDetails()
        {
            ComboFilling.fn_getSegment1Details(ref ddlSegment1);
            ComboFilling.fn_getSegment2Details(ref ddlSegment2);
            ComboFilling.fn_getSegment3Details(ref ddlSegment3);
            ComboFilling.fn_getSegment4Details(ref ddlSegment4);
            ComboFilling.fn_getSegment5Details(ref ddlSegment5);
            ComboFilling.fn_getSegment6Details(ref ddlSegment6);
        }
        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtDat = new DataTable();

                DropDownList ddlAccountGroup = new DropDownList();
                DropDownList ddlAccount = new DropDownList();

                ddlAccount.ID = "ddlAccount";
                ddlAccountGroup.ID = "ddlAccountGroup";


                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        ddlAccount = (DropDownList)gvData.FooterRow.FindControl("ddlAccount");
                        ddlAccountGroup = (DropDownList)gvData.FooterRow.FindControl("ddlAccountGroup");
                    }
                    else
                    {
                        ddlAccount = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccount");
                        ddlAccountGroup = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountGroup");
                    }
                }
                else
                {
                    ddlAccount = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccount");
                    ddlAccountGroup = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountGroup");
                }

                if (ddlAccount != null && ddlAccountGroup != null)
                {
                    FIN.BLL.GL.AccountCodes_BLL.getAccountBasedGroup(ref ddlAccount, ddlAccountGroup.SelectedValue.ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYacCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                // FillComboJEDetails();

                DropDownList ddlAccountGroup = tmpgvr.FindControl("ddlAccountGroup") as DropDownList;
                DropDownList ddlAccount = tmpgvr.FindControl("ddlAccount") as DropDownList;


                FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlAccountGroup);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAccount);


                if (gvData.EditIndex >= 0)
                {
                    ddlAccountGroup.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.GROUP_ID].ToString();
                    ddlAccount.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlAccountGroup = gvr.FindControl("ddlAccountGroup") as DropDownList;
            DropDownList ddlAccount = gvr.FindControl("ddlAccount") as DropDownList;
            TextBox txtPeriod01 = gvr.FindControl("txtPeriod01") as TextBox;
            TextBox txtPeriod02 = gvr.FindControl("txtPeriod02") as TextBox;
            TextBox txtPeriod03 = gvr.FindControl("txtPeriod03") as TextBox;
            TextBox txtPeriod04 = gvr.FindControl("txtPeriod04") as TextBox;
            TextBox txtPeriod05 = gvr.FindControl("txtPeriod05") as TextBox;
            TextBox txtPeriod06 = gvr.FindControl("txtPeriod06") as TextBox;
            TextBox txtPeriod07 = gvr.FindControl("txtPeriod07") as TextBox;
            TextBox txtPeriod08 = gvr.FindControl("txtPeriod08") as TextBox;
            TextBox txtPeriod09 = gvr.FindControl("txtPeriod09") as TextBox;
            TextBox txtPeriod10 = gvr.FindControl("txtPeriod10") as TextBox;
            TextBox txtPeriod11 = gvr.FindControl("txtPeriod11") as TextBox;
            TextBox txtPeriod12 = gvr.FindControl("txtPeriod12") as TextBox;
            DropDownList ddlSegment1 = gvr.FindControl("ddlSegment1") as DropDownList;
            DropDownList ddlSegment2 = gvr.FindControl("ddlSegment2") as DropDownList;
            DropDownList ddlSegment3 = gvr.FindControl("ddlSegment3") as DropDownList;
            DropDownList ddlSegment4 = gvr.FindControl("ddlSegment4") as DropDownList;
            DropDownList ddlSegment5 = gvr.FindControl("ddlSegment5") as DropDownList;
            DropDownList ddlSegment6 = gvr.FindControl("ddlSegment6") as DropDownList;

            Label lblSegmentValueId1 = gvr.FindControl("lblSegmentValueId1") as Label;
            Label lblSegmentValueId2 = gvr.FindControl("lblSegmentValueId2") as Label;
            Label lblSegmentValueId3 = gvr.FindControl("lblSegmentValueId3") as Label;
            Label lblSegmentValueId4 = gvr.FindControl("lblSegmentValueId4") as Label;
            Label lblSegmentValueId5 = gvr.FindControl("lblSegmentValueId5") as Label;
            Label lblSegmentValueId6 = gvr.FindControl("lblSegmentValueId6") as Label;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.BUDGET_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            //if (Session["singlePeriod"] != null && Session["singlePeriod"] != string.Empty)
            //{
            //    txtPeriod01.Text = Session["singlePeriod"].ToString();
            //    txtPeriod02.Text = Session["singlePeriod"].ToString();
            //    txtPeriod03.Text = Session["singlePeriod"].ToString();
            //    txtPeriod04.Text = Session["singlePeriod"].ToString();
            //    txtPeriod05.Text = Session["singlePeriod"].ToString();
            //    txtPeriod06.Text = Session["singlePeriod"].ToString();
            //    txtPeriod07.Text = Session["singlePeriod"].ToString();
            //    txtPeriod08.Text = Session["singlePeriod"].ToString();
            //    txtPeriod09.Text = Session["singlePeriod"].ToString();
            //    txtPeriod10.Text = Session["singlePeriod"].ToString();
            //    txtPeriod11.Text = Session["singlePeriod"].ToString();
            //    txtPeriod12.Text = Session["singlePeriod"].ToString();

            //}


            slControls[0] = ddlAccountGroup;
            // slControls[1] = ddlAccount;
            //slControls[1] = txtPeriod01;
            //slControls[2] = txtPeriod02;
            //slControls[4] = txtPeriod03;
            //slControls[5] = txtPeriod04;
            //slControls[6] = txtPeriod05;
            //slControls[7] = txtPeriod06;
            //slControls[8] = txtPeriod07;
            //slControls[9] = txtPeriod08;
            //slControls[10] = txtPeriod09;
            //slControls[11] = txtPeriod10;
            //slControls[12] = txtPeriod11;
            //slControls[13] = txtPeriod12;
            //slControls[14] = ddlSegment1;
            //slControls[15] = ddlSegment2;
            //slControls[16] = ddlSegment3;
            //slControls[17] = ddlSegment4;
            //slControls[18] = ddlSegment5;
            //slControls[19] = ddlSegment6;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Account_Group_P"] +  "";
           // string strMessage = "Account Group";

            //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

            //string strMessage = "Account Group ~ Account ~ Period 1 ~ Period 2 ~ Period 3 ~ Period 4 ~ Period 5 ~ Period 6 ~ Period 7 ~ Period 8 ~ Period 9 ~ Period 10 ~ Period 11 ~ Period 12 ";

            //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

            //string strMessage = "Account Group ~ Account ~ Period 1 ~ Period 2 ~ Period 3 ~ Period 4 ~ Period 5 ~ Period 6 ~ Period 7 ~ Period 8 ~ Period 9 ~ Period 10 ~ Period 11 ~ Period 12 ~ Segment 1 ~ Segment 2 ~ Segment 3 ~ Segment 4 ~ Segment 5 ~ Segment 6 ";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "BUDGET_CODE='" + ddlAccountGroup.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.GROUP_ID] = ddlAccountGroup.SelectedValue.ToString();
            drList[FINColumnConstants.GROUP_NAME] = ddlAccountGroup.SelectedItem.Text.ToString();
            drList[FINColumnConstants.CODE_ID] = ddlAccount.SelectedValue.ToString();
            drList[FINColumnConstants.CODE_NAME] = ddlAccount.SelectedItem.Text.ToString();


            drList[FINColumnConstants.BUDGET_MONTH_AMT_01] = txtPeriod01.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_02] = txtPeriod02.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_03] = txtPeriod03.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_04] = txtPeriod04.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_05] = txtPeriod05.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_06] = txtPeriod06.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_07] = txtPeriod07.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_08] = txtPeriod08.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_09] = txtPeriod09.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_10] = txtPeriod10.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_11] = txtPeriod11.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_12] = txtPeriod12.Text;

            drList[FINColumnConstants.BUDGET_SEGMENT_ID_1] = lblSegment1.Text;
            drList[FINColumnConstants.BUDGET_SEGMENT_ID_2] = lblSegment2.Text;
            drList[FINColumnConstants.BUDGET_SEGMENT_ID_3] = lblSegment3.Text;
            drList[FINColumnConstants.BUDGET_SEGMENT_ID_4] = lblSegment4.Text;
            drList[FINColumnConstants.BUDGET_SEGMENT_ID_5] = lblSegment5.Text;
            drList[FINColumnConstants.BUDGET_SEGMENT_ID_6] = lblSegment6.Text;

            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                    //if (rblAccCodeGroup.SelectedValue == "1")
                    //{
                    //for (int i = gvData.Rows.Count - 1; i > 0; i--)
                    //{
                    //    GridViewRow row = gvData.Rows[i];
                    //    GridViewRow previousRow = gvData.Rows[i - 1];
                    //    for (int j = 0; j < 15; j++)
                    //    {
                    //        if (row.Cells[j].Text == previousRow.Cells[j].Text)
                    //        {
                    //            if (previousRow.Cells[j].RowSpan == 0)
                    //            {
                    //                if (row.Cells[j].RowSpan == 0)
                    //                {
                    //                    previousRow.Cells[j].RowSpan += 2;
                    //                }
                    //                else
                    //                {
                    //                    previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
                    //                }
                    //                row.Cells[j].Visible = false;
                    //            }
                    //        }
                    //    }
                    //}
                    // }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void rblAccCodeGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList ddlAccount = new DropDownList();
                Label lblAccountCodeName = new Label();

                ddlAccount.ID = "ddlAccount";
                lblAccountCodeName.ID = "lblAccountCodeName";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        lblAccountCodeName = (Label)gvData.FooterRow.FindControl("lblAccountCodeName");
                        ddlAccount = (DropDownList)gvData.FooterRow.FindControl("ddlAccount");
                    }
                    else
                    {
                        lblAccountCodeName = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAccountCodeName");
                        ddlAccount = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccount");
                    }
                }
                else
                {
                    lblAccountCodeName = (Label)gvData.Controls[0].Controls[1].FindControl("lblAccountCodeName");
                    ddlAccount = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccount");
                }



                if (rblAccCodeGroup.SelectedValue == "1")
                {
                    ddlAccount.Visible = false;
                    lblAccountCodeName.Visible = false;

                    dtGridData = FIN.BLL.GL.Budget_BLL.getGroupDetails();
                    BindGrid(dtGridData);
                    divFilter.Visible = false;
                } 
                else if (rblAccCodeGroup.SelectedValue == "2")
                {
                    ddlAccount.Visible = true;
                    lblAccountCodeName.Visible = true;

                    dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetails();
                    BindGrid(dtGridData);
                    divFilter.Visible = true;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetExntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnClick_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hdRowIndex.Value = gvr.RowIndex.ToString();


            //if (Master.Mode == FINAppConstants.Update)
            //{
            //    DataTable dtperiod = new DataTable();
            //    dtperiod = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getSegmentDetails(Master.StrRecordId)).Tables[0];

            //    if (dtperiod != null)
            //    {
            //        if (dtperiod.Rows.Count > 0)
            //        {
            //            ddlSegment1.SelectedItem.Value = dtperiod.Rows[0]["segment_value"].ToString();
            //            ddlSegment2.SelectedItem.Value = dtperiod.Rows[1]["segment_value"].ToString();
            //            ddlSegment3.SelectedItem.Value = dtperiod.Rows[2]["segment_value"].ToString();
            //            ddlSegment4.SelectedItem.Value = dtperiod.Rows[3]["segment_value"].ToString();
            //            ddlSegment5.SelectedItem.Value = dtperiod.Rows[4]["segment_value"].ToString();
            //            ddlSegment6.SelectedItem.Value = dtperiod.Rows[5]["segment_value"].ToString();
            //        }
            //    }
            //}
            if (gvData.Rows.Count > 0)
            {
                GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
                int index = gridViewRow.RowIndex;
                int iLoop = index;
                //for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                //{
                if (rblAccCodeGroup.SelectedValue != null)
                {
                    if (rblAccCodeGroup.SelectedValue == "1")
                    {
                        BindSegmentBasedGroup(gvData.DataKeys[iLoop].Values[FINColumnConstants.GROUP_ID].ToString());
                    }
                    else
                    {
                        BindSegmentCode(gvData.DataKeys[iLoop].Values[FINColumnConstants.CODE_ID].ToString());
                    }
                }
                if (Master.Mode == FINAppConstants.Update)
                {
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment1, ddlCompanyName.SelectedValue.ToString());
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment2, ddlCompanyName.SelectedValue.ToString());
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment3, ddlCompanyName.SelectedValue.ToString());
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, ddlCompanyName.SelectedValue.ToString());
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment5, ddlCompanyName.SelectedValue.ToString());
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment6, ddlCompanyName.SelectedValue.ToString());

                    string seg1 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_1].ToString();
                    string seg2 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_2].ToString();
                    string seg3 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_3].ToString();
                    string seg4 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_4].ToString();
                    string seg5 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_5].ToString();
                    string seg6 = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_SEGMENT_ID_6].ToString();

                    ddlSegment1.SelectedValue = seg1;
                    ddlSegment2.SelectedValue = seg2;
                    ddlSegment3.SelectedValue = seg3;
                    ddlSegment4.SelectedValue = seg4;
                    ddlSegment5.SelectedValue = seg5;
                    ddlSegment6.SelectedValue = seg6;
                }
                //}
            }

            // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            //ImageButton btndetails = sender as ImageButton;
            //GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            //lblID.Text = gvData.DataKeys[gvrow.RowIndex].Value.ToString();


            ModalPopupExtender2.Show();
        }

        private void BindSegmentBasedGroup(string codeId)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();

                if (codeId != string.Empty)
                {
                    dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccGroup(codeId)).Tables[0];
                    if (dtdataAc != null)
                    {
                        if (dtdataAc.Rows.Count > 0)
                        {
                            int j = dtdataAc.Rows.Count;

                            for (int i = 0; i < dtdataAc.Rows.Count; i++)
                            {
                                if (i == 0)
                                {
                                    //segment1
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment1, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment1, codeId, "0");
                                    }

                                }
                                if (i == 1)
                                {
                                    //segment2
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, "0");
                                    }
                                }
                                if (i == 2)
                                {
                                    //Segments3
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                    }
                                }
                                if (i == 3)
                                {
                                    //segment4
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                    }
                                }
                                if (i == 4)
                                {
                                    //segment5
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                    }
                                }
                                if (i == 5)
                                {
                                    //segment6
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                                    }
                                }
                            }
                            if (j == 1)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 2)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 3)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 4)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 5)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindSegmentCode(string codeId)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                //DropDownList ddlAccountCodes = new DropDownList();
                //ddlAccountCodes.ID = "ddlAccountCodes";

                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {

                //        ddlAccountCodes = (DropDownList)gvData.FooterRow.FindControl("ddlAccountCodes");
                //    }
                //    else
                //    {
                //        ddlAccountCodes = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountCodes");
                //    }
                //}
                //else
                //{
                //    ddlAccountCodes = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountCodes");
                //}

                if (codeId != string.Empty)
                {
                    //ddlAccountCodes.SelectedValueCODE_ID
                    //if (ddlAccountCodes.SelectedValue != null)
                    //{
                    dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(codeId)).Tables[0];
                    if (dtdataAc != null)
                    {
                        if (dtdataAc.Rows.Count > 0)
                        {
                            int j = dtdataAc.Rows.Count;

                            for (int i = 0; i < dtdataAc.Rows.Count; i++)
                            {
                                if (i == 0)
                                {
                                    //segment1
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, codeId, "0");
                                    }

                                }
                                if (i == 1)
                                {
                                    //segment2
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, "0");
                                    }
                                }
                                if (i == 2)
                                {
                                    //Segments3
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                    }
                                }
                                if (i == 3)
                                {
                                    //segment4
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                    }
                                }
                                if (i == 4)
                                {
                                    //segment5
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                    }
                                }
                                if (i == 5)
                                {
                                    //segment6
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                                    }
                                }
                            }
                            if (j == 1)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 2)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 3)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 4)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 5)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnPeriodOk_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //  gvData.DataKeys[gvData.].Value.ToString();
                //Button btnPeriodOk = new Button();
                //btnPeriodOk = (Button)sender;
                //using (GridViewRow row = (GridViewRow)(btnPeriodOk.Parent.Parent))
                //{

                Label txtPeriod1 = new Label();
                Label txtPeriod2 = new Label();
                Label txtPeriod3 = new Label();
                Label txtPeriod4 = new Label();
                Label txtPeriod5 = new Label();
                Label txtPeriod6 = new Label();
                Label txtPeriod7 = new Label();
                Label txtPeriod8 = new Label();
                Label txtPeriod9 = new Label();
                Label txtPeriod10 = new Label();
                Label txtPeriod11 = new Label();
                Label txtPeriod12 = new Label();

                txtPeriod1.ID = "txtPeriod1";
                txtPeriod2.ID = "txtPeriod2";
                txtPeriod3.ID = "txtPeriod3";
                txtPeriod4.ID = "txtPeriod4";
                txtPeriod5.ID = "txtPeriod5";
                txtPeriod6.ID = "txtPeriod6";
                txtPeriod7.ID = "txtPeriod7";
                txtPeriod8.ID = "txtPeriod8";
                txtPeriod9.ID = "txtPeriod9";
                txtPeriod10.ID = "txtPeriod10";
                txtPeriod11.ID = "txtPeriod11";
                txtPeriod12.ID = "txtPeriod12";

                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {
                //        txtPeriod1 = (Label)gvData.FooterRow.FindControl("lblPeriod01");
                //        txtPeriod2 = (Label)gvData.FooterRow.FindControl("lblPeriod02");
                //        txtPeriod3 = (Label)gvData.FooterRow.FindControl("lblPeriod03");
                //        txtPeriod4 = (Label)gvData.FooterRow.FindControl("lblPeriod04");
                //        txtPeriod5 = (Label)gvData.FooterRow.FindControl("lblPeriod05");
                //        txtPeriod6 = (Label)gvData.FooterRow.FindControl("lblPeriod06");
                //        txtPeriod7 = (Label)gvData.FooterRow.FindControl("lblPeriod07");
                //        txtPeriod8 = (Label)gvData.FooterRow.FindControl("lblPeriod08");
                //        txtPeriod9 = (Label)gvData.FooterRow.FindControl("lblPeriod09");
                //        txtPeriod10 = (Label)gvData.FooterRow.FindControl("lblPeriod10");
                //        txtPeriod11 = (Label)gvData.FooterRow.FindControl("lblPeriod11");
                //        txtPeriod12 = (Label)gvData.FooterRow.FindControl("lblPeriod12");
                //    }
                //    else
                //    {
                //        txtPeriod1 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod01");
                //        txtPeriod2 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod02");
                //        txtPeriod3 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod03");
                //        txtPeriod4 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod04");
                //        txtPeriod5 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod05");
                //        txtPeriod6 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod06");
                //        txtPeriod7 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod07");
                //        txtPeriod8 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod08");
                //        txtPeriod9 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod09");
                //        txtPeriod10 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod10");
                //        txtPeriod11 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod11");
                //        txtPeriod12 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblPeriod12");
                //    }
                //}
                //else
                //{
                //    txtPeriod1 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod01");
                //    txtPeriod2 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod02");
                //    txtPeriod3 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod03");
                //    txtPeriod4 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod04");
                //    txtPeriod5 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod05");
                //    txtPeriod6 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod06");
                //    txtPeriod7 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod07");
                //    txtPeriod8 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod08");
                //    txtPeriod9 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod09");
                //    txtPeriod10 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod10");
                //    txtPeriod11 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod11");
                //    txtPeriod12 = (Label)gvData.Controls[0].Controls[1].FindControl("lblPeriod12");
                //}

                //if (txtPeriod1 != null && txtPeriod2 != null && txtPeriod3 != null && txtPeriod4 != null && txtPeriod5 != null && txtPeriod6 != null && txtPeriod7 != null && txtPeriod8 != null && txtPeriod9 != null && txtPeriod10 != null && txtPeriod11 != null && txtPeriod12 != null)
                //{
                //if (gvData.Rows.Count > 0)
                //{
                //    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                //    {
                //= txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;

                DataTable dtDataPeriods = new DataTable();

                DataRow dtRow;

                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_01");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_02");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_03");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_04");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_05");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_06");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_07");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_08");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_09");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_10");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_11");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_12");

                dtRow = dtDataPeriods.NewRow();
                dtRow["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                dtRow["BUDGET_MONTH_AMT_02"] = txtPeriodPop2.Text;
                dtRow["BUDGET_MONTH_AMT_03"] = txtPeriodPop3.Text;
                dtRow["BUDGET_MONTH_AMT_04"] = txtPeriodPop4.Text;
                dtRow["BUDGET_MONTH_AMT_05"] = txtPeriodPop5.Text;
                dtRow["BUDGET_MONTH_AMT_06"] = txtPeriodPop6.Text;
                dtRow["BUDGET_MONTH_AMT_07"] = txtPeriodPop7.Text;
                dtRow["BUDGET_MONTH_AMT_08"] = txtPeriodPop8.Text;
                dtRow["BUDGET_MONTH_AMT_09"] = txtPeriodPop9.Text;
                dtRow["BUDGET_MONTH_AMT_10"] = txtPeriodPop10.Text;
                dtRow["BUDGET_MONTH_AMT_11"] = txtPeriodPop11.Text;
                dtRow["BUDGET_MONTH_AMT_12"] = txtPeriodPop12.Text;

                dtDataPeriods.Rows.Add(dtRow);

                //if (hdRowIndex.Value == Session[hdRowIndex.Value])
                //{
                Session[hdRowIndex.Value + "_Periods"] = dtDataPeriods;
                // }



                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_02"] = txtPeriodPop2.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_03"] = txtPeriodPop3.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_04"] = txtPeriodPop4.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_05"] = txtPeriodPop5.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_06"] = txtPeriodPop6.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_07"] = txtPeriodPop7.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_08"] = txtPeriodPop8.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_09"] = txtPeriodPop9.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_10"] = txtPeriodPop10.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_11"] = txtPeriodPop11.Text;
                //gvData.DataKeys[iLoop].Values["BUDGET_MONTH_AMT_12"] = txtPeriodPop12.Text;

                //txtPeriod1.Text = txtPeriodPop1.Text;
                //txtPeriod2.Text = txtPeriodPop2.Text;
                //txtPeriod3.Text = txtPeriodPop3.Text;
                //txtPeriod4.Text = txtPeriodPop4.Text;
                //txtPeriod5.Text = txtPeriodPop5.Text;
                //txtPeriod6.Text = txtPeriodPop6.Text;
                //txtPeriod7.Text = txtPeriodPop7.Text;
                //txtPeriod8.Text = txtPeriodPop8.Text;
                //txtPeriod9.Text = txtPeriodPop9.Text;
                //txtPeriod10.Text = txtPeriodPop10.Text;
                //txtPeriod11.Text = txtPeriodPop11.Text;
                //txtPeriod12.Text = txtPeriodPop11.Text;

                //    }
                //}
                mpePeriods.Hide();
                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnPeriods_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                fillperiods(gvr);
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fillperiods(GridViewRow gvr)
        {
            
            hdRowIndex.Value = gvr.RowIndex.ToString();

            //if (!(bool)Session["periodchanged"])
            //{
            if (Master.Mode == FINAppConstants.Update)
            {
                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {

                        txtPeriodPop1.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_01].ToString();
                        txtPeriodPop2.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_02].ToString();
                        txtPeriodPop3.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_03].ToString();
                        txtPeriodPop4.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_04].ToString();
                        txtPeriodPop5.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_05].ToString();
                        txtPeriodPop6.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_06].ToString();

                        txtPeriodPop7.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_07].ToString();
                        txtPeriodPop8.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_08].ToString();
                        txtPeriodPop9.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_09].ToString();
                        txtPeriodPop10.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_10].ToString();
                        txtPeriodPop11.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_11].ToString();
                        txtPeriodPop12.Text = gvData.DataKeys[iLoop].Values[FINColumnConstants.BUDGET_MONTH_AMT_12].ToString();
                    }
                }
            }
            //}

            TextBox txtAnnualBudgetAmount = gvr.FindControl("txtAnnualBudgetAmount") as TextBox;

            if (txtAnnualBudgetAmount.Text.Trim() != string.Empty)
            {
                if (Session["singlePeriod"] != null)
                {
                    txtPeriodPop1.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop2.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop3.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop4.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop5.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop6.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop7.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop8.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop9.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop10.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop11.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop12.Text = Session["singlePeriod"].ToString();

                }
                //else
                //{
                //    txtPeriodPop1.Text = string.Empty;
                //    txtPeriodPop2.Text = string.Empty;
                //    txtPeriodPop3.Text = string.Empty;
                //    txtPeriodPop4.Text = string.Empty;
                //    txtPeriodPop5.Text = string.Empty;
                //    txtPeriodPop6.Text = string.Empty;
                //    txtPeriodPop7.Text = string.Empty;
                //    txtPeriodPop8.Text = string.Empty;
                //    txtPeriodPop9.Text = string.Empty;
                //    txtPeriodPop10.Text = string.Empty;
                //    txtPeriodPop11.Text = string.Empty;
                //    txtPeriodPop12.Text = string.Empty;
                //}
            }
            else
            {
                txtPeriodPop1.Text = string.Empty;
                txtPeriodPop2.Text = string.Empty;
                txtPeriodPop3.Text = string.Empty;
                txtPeriodPop4.Text = string.Empty;
                txtPeriodPop5.Text = string.Empty;
                txtPeriodPop6.Text = string.Empty;
                txtPeriodPop7.Text = string.Empty;
                txtPeriodPop8.Text = string.Empty;
                txtPeriodPop9.Text = string.Empty;
                txtPeriodPop10.Text = string.Empty;
                txtPeriodPop11.Text = string.Empty;
                txtPeriodPop12.Text = string.Empty;
                Session["singlePeriod"] = null;
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Budget Entry Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR, true);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnOkay_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlSegment1;
                //slControls[1] = ddlSegment2;
                //slControls[2] = ddlSegment3;
                //slControls[3] = ddlSegment4;
                //slControls[4] = ddlSegment5;
                //slControls[5] = ddlSegment6;

                //ErrorCollection.Clear();
                //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

                //string strMessage = "Segment1 ~ Segment2 ~ Segment3~ Segment4 ~ Segment5 ~ Segment6";

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);


                //Label lblSegmentValueId1 = new Label();
                //Label lblSegmentValueId2 = new Label();
                //Label lblSegmentValueId3 = new Label();
                //Label lblSegmentValueId4 = new Label();
                //Label lblSegmentValueId5 = new Label();
                //Label lblSegmentValueId6 = new Label();

                //lblSegmentValueId1.ID = "lblSegmentValueId1";
                //lblSegmentValueId2.ID = "lblSegmentValueId2";
                //lblSegmentValueId3.ID = "lblSegmentValueId3";
                //lblSegmentValueId4.ID = "lblSegmentValueId4";
                //lblSegmentValueId5.ID = "lblSegmentValueId5";
                //lblSegmentValueId6.ID = "lblSegmentValueId6";

                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {
                //        lblSegmentValueId1 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId1");
                //        lblSegmentValueId2 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId2");
                //        lblSegmentValueId3 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId3");
                //        lblSegmentValueId4 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId4");
                //        lblSegmentValueId5 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId5");
                //        lblSegmentValueId6 = (Label)gvData.FooterRow.FindControl("lblSegmentValueId6");
                //    }
                //    else
                //    {
                //        lblSegmentValueId1 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId1");
                //        lblSegmentValueId2 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId2");
                //        lblSegmentValueId3 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId3");
                //        lblSegmentValueId4 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId4");
                //        lblSegmentValueId5 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId5");
                //        lblSegmentValueId6 = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblSegmentValueId6");
                //    }
                //}
                //else
                //{
                //    lblSegmentValueId1 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId1");
                //    lblSegmentValueId2 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId2");
                //    lblSegmentValueId3 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId3");
                //    lblSegmentValueId4 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId4");
                //    lblSegmentValueId5 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId5");
                //    lblSegmentValueId6 = (Label)gvData.Controls[0].Controls[1].FindControl("lblSegmentValueId6");
                //}

                //if (lblSegmentValueId1 != null && lblSegmentValueId2 != null && lblSegmentValueId3 != null && lblSegmentValueId4 != null && lblSegmentValueId5 != null && lblSegmentValueId6 != null)
                //{
                //    lblSegmentValueId1.Text = ddlSegment1.SelectedValue;
                //    lblSegmentValueId2.Text = ddlSegment2.SelectedValue;
                //    lblSegmentValueId3.Text = ddlSegment3.SelectedValue;
                //    lblSegmentValueId4.Text = ddlSegment4.SelectedValue;
                //    lblSegmentValueId5.Text = ddlSegment5.SelectedValue;
                //    lblSegmentValueId6.Text = ddlSegment6.SelectedValue;
                //}

                DataTable dtDataPeriods = new DataTable();

                DataRow dtRow;

                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_1");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_2");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_3");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_4");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_5");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_6");


                dtRow = dtDataPeriods.NewRow();

                dtRow["BUDGET_SEGMENT_ID_1"] = ddlSegment1.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_2"] = ddlSegment2.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_3"] = ddlSegment3.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_4"] = ddlSegment4.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_5"] = ddlSegment5.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_6"] = ddlSegment6.SelectedValue;

                dtDataPeriods.Rows.Add(dtRow);

                Session[hdRowIndex.Value + "_Segments"] = dtDataPeriods;

                ModalPopupExtender2.Hide();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void txtAnnualBudgetAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtAnnualBudgetAmount = gvr.FindControl("txtAnnualBudgetAmount") as TextBox;
                Button btnPeriods = gvr.FindControl("btnPeriods") as Button;

                if (txtAnnualBudgetAmount.Text.Trim() != string.Empty)
                {
                    Session["singlePeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtAnnualBudgetAmount.Text) / 12, 2);
                    btnPeriods.Enabled = true;
                    fillperiods(gvr);
                }
                else
                {
                    Session["singlePeriod"] = null;
                    btnPeriods.Enabled = false;
                }

                //if (Master.Mode == FINAppConstants.Update)
                //{
                //    hdRowIndex.Value = gvr.RowIndex.ToString();
                //    Session[hdRowIndex.Value + "_annualAmt"] = true;
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void ClearGrid()
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                gvData.DataSource = dtGridData;
                gvData.DataBind();

                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);

            }
        }
        protected void ddlAccountGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg(VMVServices.Web.Utils.OrganizationID, ddlAccountGroupFilter.SelectedValue.ToString());
                if (dtGridData != null)
                {
                    if (dtGridData.Rows.Count > 0)
                    {
                        BindGrid(dtGridData);
                    }
                    else
                    {
                        ClearGrid();
                    }
                }
                else
                {
                    ClearGrid();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}