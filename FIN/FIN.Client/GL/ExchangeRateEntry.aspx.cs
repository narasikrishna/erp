﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class ExchangeRateEntry : PageBase
    {
        SSM_CURRENCY_EXCHANGE_RATES sSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }



        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.GL.ExchangeRate_BLL.getExchangeRateDetails(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<SSM_CURRENCY_EXCHANGE_RATES> userCtx = new DataRepository<SSM_CURRENCY_EXCHANGE_RATES>())
                    {
                        sSM_CURRENCY_EXCHANGE_RATES = userCtx.Find(r =>
                            (r.CURRENCY_RATE_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    EntityData = sSM_CURRENCY_EXCHANGE_RATES;

                    ddlOraganisationName.SelectedValue = sSM_CURRENCY_EXCHANGE_RATES.COMP_ID;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_CURRENCY_EXCHANGE_RATES = (SSM_CURRENCY_EXCHANGE_RATES)EntityData;
                }


                // sSM_CURRENCY_EXCHANGE_RATES.COMP_ID = ddlOraganisationName.SelectedValue.ToString();


                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Exchange Rate");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    sSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString() != "0")
                    {
                        using (IRepository<SSM_CURRENCY_EXCHANGE_RATES> userCtx = new DataRepository<SSM_CURRENCY_EXCHANGE_RATES>())
                        {
                            sSM_CURRENCY_EXCHANGE_RATES = userCtx.Find(r =>
                                (r.CURRENCY_RATE_ID == dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    sSM_CURRENCY_EXCHANGE_RATES.COMP_ID = ddlOraganisationName.SelectedValue;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString() != "0")
                    {
                        ExchangeRate_BLL.getExchangeRateDetails(dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_DT] != DBNull.Value)
                    {
                        sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_RATE_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_DT].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_ID] != DBNull.Value)
                    {
                        sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_ID = dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_ID].ToString();
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_STD_RATE] != DBNull.Value)
                    {
                        sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_STD_RATE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_STD_RATE].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_SEL_RATE] != DBNull.Value)
                    {
                        sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_SEL_RATE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_SEL_RATE].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_BUY_RATE] != DBNull.Value)
                    {
                        sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_BUY_RATE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_BUY_RATE].ToString());
                    }
                    sSM_CURRENCY_EXCHANGE_RATES.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    //sSM_CURRENCY_EXCHANGE_RATES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(sSM_CURRENCY_EXCHANGE_RATES, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString() != string.Empty)
                        {
                            sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_RATE_ID = dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_RATE_ID].ToString();
                            sSM_CURRENCY_EXCHANGE_RATES.MODIFIED_BY = this.LoggedUserName;
                            sSM_CURRENCY_EXCHANGE_RATES.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(sSM_CURRENCY_EXCHANGE_RATES, FINAppConstants.Update));
                        }
                        else
                        {

                            sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_RATE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0002.ToString(), false, true);
                            sSM_CURRENCY_EXCHANGE_RATES.CREATED_BY = this.LoggedUserName;
                            sSM_CURRENCY_EXCHANGE_RATES.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(sSM_CURRENCY_EXCHANGE_RATES, FINAppConstants.Add));
                        }

                        sSM_CURRENCY_EXCHANGE_RATES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_RATE_ID);
                    }

                    switch (Master.Mode)
                    {
                        case FINAppConstants.Add:
                            {
                                DBMethod.SaveEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES);
                                savedBool = true;
                                break;
                            }
                        case FINAppConstants.Update:
                            {

                                DBMethod.SaveEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES, true);
                                savedBool = true;
                                break;

                            }
                    }
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            CommonUtils.SavePCEntity<SSM_CURRENCY_EXCHANGE_RATES, SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES, tmpChildEntity, sSM_CURRENCY_EXCHANGE_RATES);
                //            savedBool = true;
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {
                //            CommonUtils.SavePCEntity<SSM_CURRENCY_EXCHANGE_RATES, SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES, tmpChildEntity, sSM_CURRENCY_EXCHANGE_RATES, true);
                //            savedBool = true;
                //            break;
                //        }
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            ComboFilling.fn_getOrganisationDetails(ref ddlOraganisationName);
            ddlOraganisationName.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlOraganisationName.Enabled = false;
            DataTable dtbasecurrency = new DataTable();
            dtbasecurrency = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.getBasecurrency(VMVServices.Web.Utils.OrganizationID).ToString()).Tables[0];
            txtBasecurrency.Text = dtbasecurrency.Rows[0]["CURRENCY_DESC"].ToString();


        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCurrency = tmpgvr.FindControl("ddlCurrency") as DropDownList;

                Currency_BLL.getCurrencyDetails(ref ddlCurrency);


                if (gvData.EditIndex >= 0)
                {
                    ddlCurrency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CURRENCY_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                   // dr["CURRENCY_RATE_DT"] = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpDate = gvr.FindControl("dtpDate") as TextBox;
            DropDownList ddl_Currency = gvr.FindControl("ddlCurrency") as DropDownList;
            TextBox txtStandardRate = gvr.FindControl("txtStandardRate") as TextBox;
            TextBox txtSellingRate = gvr.FindControl("txtSellingRate") as TextBox;
            TextBox txtBuyingRate = gvr.FindControl("txtBuyingRate") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.CURRENCY_RATE_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = dtpDate;
            slControls[1] = dtpDate;
            slControls[2] = ddl_Currency;
            slControls[3] = txtStandardRate;
            slControls[4] = txtSellingRate;
            slControls[5] = txtBuyingRate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Currency_P"] + " ~ " + Prop_File_Data["Standard_Rate_P"] + " ~ " + Prop_File_Data["Selling_Rate_P"] + " ~ " + Prop_File_Data["Buying_Rate_P"] + "";
            //string strMessage = "Date ~ Date ~ Currency ~ Standard Rate ~ Selling Rate ~ Buying Rate";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "CURRENCY_ID='" + ddl_Currency.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            //if (Session["precision"] != null)
            //{
            //    if (CommonUtils.ConvertStringToDecimal(Session["precision"].ToString()) > 0)
            //    {
            //        int precision = int.Parse(Session["precision"].ToString());
            //        txtStandardRate.Text = Math.Round(decimal.Parse(txtStandardRate.Text.ToString()), precision).ToString();
            //        txtSellingRate.Text = Math.Round(CommonUtils.ConvertStringToDecimal(txtSellingRate.Text.ToString()), precision).ToString();
            //        txtBuyingRate.Text = Math.Round(CommonUtils.ConvertStringToDecimal(txtBuyingRate.Text.ToString()), precision).ToString();
            //    }
            //}

            drList[FINColumnConstants.CURRENCY_RATE_DT] = dtpDate.Text;
            if (ddl_Currency.SelectedItem != null)
            {
                drList[FINColumnConstants.CURRENCY_ID] = ddl_Currency.SelectedItem.Value;
                drList[FINColumnConstants.CURRENCY_NAME] = ddl_Currency.SelectedItem.Text;
            }
            //   drList[FINColumnConstants.CURRENCY_ID] = ddl_Currency.SelectedValue;
            drList[FINColumnConstants.CURRENCY_STD_RATE] = CommonUtils.ConvertStringToDecimal(txtStandardRate.Text);
            drList[FINColumnConstants.CURRENCY_SEL_RATE] = CommonUtils.ConvertStringToDecimal(txtSellingRate.Text);
            drList[FINColumnConstants.CURRENCY_BUY_RATE] = CommonUtils.ConvertStringToDecimal(txtBuyingRate.Text);
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        {
            try
            {
                // SortedList ErrorCollection = new SortedList();
                if (dtGridData.Select(strCondition).Length > 0)
                {
                    ErrorCollection.Add(strMessage, strMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
            //return ErrorCollection;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlOraganisationName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
             
                GridViewRow gvr = gvData.FooterRow;

                DropDownList ddlCurrency = (DropDownList)gvr.FindControl("ddlCurrency");
                TextBox dtpDate = (TextBox)gvr.FindControl("dtpDate");
                TextBox txtStandardRate = (TextBox)gvr.FindControl("txtStandardRate");
                

                SSM_CURRENCIES SSM_CURRENCIES = new SSM_CURRENCIES();

                using (IRepository<SSM_CURRENCIES> ssm = new DataRepository<SSM_CURRENCIES>())
                {
                    SSM_CURRENCIES = ssm.Find(x => (x.CURRENCY_ID == ddlCurrency.SelectedValue.ToString())).SingleOrDefault();
                    if (SSM_CURRENCIES != null)
                    {
                        Session["precision"] = SSM_CURRENCIES.STANDARD_PRECISION;
                    }
                }
                DataTable dt_std_rate = new DataTable();
                dt_std_rate = FIN.BLL.GL.ExchangeRate_BLL.getStandardRate(ddlCurrency.SelectedValue,(dtpDate.Text));
                if (dt_std_rate != null)
                {
                    if (dt_std_rate.Rows.Count > 0)
                    {
                        txtStandardRate.Text = dt_std_rate.Rows[0]["currency_std_rate"].ToString();
                    }
                    else
                    {
                        txtStandardRate.Text = null;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

    }
}