﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ExchangeRateEntry.aspx.cs" Inherits="FIN.Client.GL.ExchangeRateEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblOraganisationName">
                Organization
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlOraganisationName" runat="server" CssClass="validate[required customer[required]]  RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Base Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:Label ID="txtBasecurrency" runat="server"></asp:Label>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="CURRENCY_RATE_ID,CURRENCY_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True" OnSelectedIndexChanged="gvData_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField HeaderText="Currency" FooterStyle-Width="200px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="2" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="2" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("CURRENCY_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="3" Text='<%#  Eval("CURRENCY_RATE_DT","{0:dd/MM/yyyy}") %>' Width="80px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="3" Width="80px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CURRENCY_RATE_DT","{0:dd/MM/yyyy}") %>'
                                Width="80px">
                            </asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Standard Rate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStandardRate" MaxLength="13" runat="server" Text='<%# Eval("CURRENCY_STD_RATE") %>'
                                TabIndex="4" CssClass="EntryFont RequiredField txtBox_N" Width="100px">
                            </asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtStandardRate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtStandardRate" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="4" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtStandardRate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStandardRate" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("CURRENCY_STD_RATE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Selling Rate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSellingRate" MaxLength="13" runat="server" Text='<%# Eval("CURRENCY_SEL_RATE") %>'
                                TabIndex="5" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtSellingRate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSellingRate" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="5" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtSellingRate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSellingRate" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("CURRENCY_SEL_RATE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Buying Rate">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBuyingRate" MaxLength="13" runat="server" Text='<%# Eval("CURRENCY_BUY_RATE") %>'
                                TabIndex="6" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtBuyingRate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtBuyingRate" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="6" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtBuyingRate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBuyingRate" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("CURRENCY_BUY_RATE") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="8" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="9" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="10" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="11" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="7" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
       
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style="width: 720px" id="Div2">
            Note: Enter the exchange rates on the basis of 1 unit of Foreign currency is equivalent to so much of base currency
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="4" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
