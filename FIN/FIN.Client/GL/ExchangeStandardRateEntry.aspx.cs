﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class ExchangeStandardRateEntry : PageBase
    {








        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS
        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS
        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS
        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS
        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS
        //   SCREEN NOT COMPLETED........   ITS A PROCESS SCREEN WITH PARAMETERS








        SSM_CURRENCY_EXCHANGE_RATES sSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
                Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
                Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<SSM_CURRENCY_EXCHANGE_RATES> userCtx = new DataRepository<SSM_CURRENCY_EXCHANGE_RATES>())
                    {
                        sSM_CURRENCY_EXCHANGE_RATES = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_CURRENCY_EXCHANGE_RATES;

                    //txtCurrencyCode.Text = sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_CODE;
                    //txtCurrencyDescription.Text = sSM_CURRENCIES.CURRENCY_DESC;
                    //txtCurrencySymbol.Text = sSM_CURRENCIES.CURRENCY_SYMBOL;
                    //ddlInitialAmountSeparator.SelectedValue = sSM_CURRENCIES.CURRENCY_AMT_SEPARATOR;
                    //ddlSubsequentAmountSeparator.SelectedValue = sSM_CURRENCIES.CURRENCY_SUB_SEQ_SEPRATOR;
                    //txtDecimalPrecision.Text = sSM_CURRENCIES.STANDARD_PRECISION.ToString();
                    //txtDecimalSeperator.Text = sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR.ToString();
                    
                    
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_CURRENCY_EXCHANGE_RATES = (SSM_CURRENCY_EXCHANGE_RATES)EntityData;
                }

                               
                sSM_CURRENCY_EXCHANGE_RATES.COMP_ID = ddlOraganisationName.SelectedValue.ToString();
                sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_ID = ddlCurrency.SelectedValue.ToString();
                
                sSM_CURRENCY_EXCHANGE_RATES.ENABLED_FLAG = FINAppConstants.Y;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    sSM_CURRENCY_EXCHANGE_RATES.MODIFIED_BY = this.LoggedUserName;
                    sSM_CURRENCY_EXCHANGE_RATES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0002.ToString(), false, true);
                    
                    sSM_CURRENCY_EXCHANGE_RATES.CREATED_BY = this.LoggedUserName;
                    sSM_CURRENCY_EXCHANGE_RATES.CREATED_DATE = DateTime.Today;


                }

                sSM_CURRENCY_EXCHANGE_RATES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_CURRENCY_EXCHANGE_RATES.CURRENCY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            ComboFilling.fn_getOrganisationDetails(ref ddlOraganisationName);
            FIN.BLL.GL.Currency_BLL.getCurrencyDetails(ref ddlCurrency);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    ErrorCollection.Clear();

            //    AssignToBE();


            //    switch (Master.Mode)
            //    {
            //        case FINAppConstants.Add:
            //            {
            //                DBMethod.SaveEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES);
            //                DisplaySaveCompleteMessage(Master.ListPageToOpen);
            //                break;
            //            }
            //        case FINAppConstants.Update:
            //            {

            //                DBMethod.SaveEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES, true);
            //                DisplaySaveCompleteMessage(Master.ListPageToOpen);
            //                break;

            //            }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("Save", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_CURRENCY_EXCHANGE_RATES>(sSM_CURRENCY_EXCHANGE_RATES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

        }

        protected void btnPopulate_Click(object sender, EventArgs e)
        {

            FINSP.GetSPFOR_EXCHG_STD_RATE(ddlCurrency.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text.ToString()), DBMethod.ConvertStringToDate(txtToDate.Text.ToString()),Decimal.Parse(txtStandardRate.Text.ToString()),ddlOraganisationName.SelectedValue.ToString());
        }


    }
}