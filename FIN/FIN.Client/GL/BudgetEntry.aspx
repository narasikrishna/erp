﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BudgetEntry.aspx.cs" Inherits="FIN.Client.GL.BudgetEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblCompanyName">
                Organization
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:DropDownList ID="ddlCompanyName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="1" OnSelectedIndexChanged="ddlOrganisation_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="lblBudgetName">
                Budget Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtBudgetName" MaxLength="500" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="lblBudgetType">
                Budget Type
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlBudgetType" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:DropDownList ID="ddlGlobalSegment" CssClass="validate[]  ddlStype" runat="server"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtToDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divtxtBox" style="float: left; width: 450px">
            <asp:RadioButtonList ID="rblAccCodeGroup" runat="server" TabIndex="7" RepeatDirection="Horizontal"
                OnSelectedIndexChanged="rblAccCodeGroup_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="1">Account Group</asp:ListItem>
                <asp:ListItem Value="2" Selected="True">Account Code</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divFilter">
            <div class="lblBox" style="float: left; width: 150px" id="lblFilterByAccountGroup">
                Filter By Account Group
            </div>
            <div class="divtxtBox" style="float: left; width: 500px">
                <asp:DropDownList ID="ddlAccountGroupFilter" CssClass="ddlStype" runat="server" TabIndex="8"
                    OnSelectedIndexChanged="ddlAccountGroupFilter_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 150px" id="Div1">
                Accounting Year
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlCalyr" CssClass="ddlStype" runat="server" TabIndex="8">
                </asp:DropDownList>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divRowContainer " align="left">
            <asp:HiddenField runat="server" ID="hfgs" />
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                EmptyDataText="No Record Found" DataKeyNames="BUDGET_DTL_ID,DELETED,GROUP_ID,PERCENTAGE,ACTUAL_PREVIOUS_AMOUNT,CODE_ID,BUDGET_SEGMENT_ID_1,BUDGET_SEGMENT_ID_2,BUDGET_SEGMENT_ID_3,BUDGET_SEGMENT_ID_4,BUDGET_SEGMENT_ID_5,BUDGET_SEGMENT_ID_6,BUDGET_MONTH_AMT_01,BUDGET_MONTH_AMT_02,BUDGET_MONTH_AMT_03,BUDGET_MONTH_AMT_04,BUDGET_MONTH_AMT_05,BUDGET_MONTH_AMT_06,BUDGET_MONTH_AMT_07,BUDGET_MONTH_AMT_08,BUDGET_MONTH_AMT_09,BUDGET_MONTH_AMT_10,BUDGET_MONTH_AMT_11,BUDGET_MONTH_AMT_12"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Account Group">
                        <ItemTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_6") %>'></asp:Label>
                            <asp:Label ID="lblAccountGroup" CssClass="adminFormFieldHeading" Width="98" runat="server"
                                Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_6") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("BUDGET_SEGMENT_ID_6") %>'></asp:Label></FooterTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Group" Visible="false">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountGroup" runat="server" CssClass="ddlStype" Width="130px"
                                OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged" AutoPostBack="true"
                                TabIndex="9">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountGroup" runat="server" CssClass="ddlStype" Width="130px"
                                OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged" AutoPostBack="true"
                                TabIndex="9">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountGcroup" CssClass="adminFormFieldHeading" Width="98" runat="server"
                                Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="ddlStype" Width="230px"
                                TabIndex="10">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="ddlStype" Width="230px"
                                Visible="false" TabIndex="10">
                            </asp:DropDownList>
                            <asp:Label ID="lblAccountCodeName" Visible="false" CssClass="adminFormFieldHeading"
                                Width="230px" runat="server" Text='<%# Eval("CODE_NAME") %>'></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountCodeName" CssClass="adminFormFieldHeading" Width="230px"
                                runat="server" Text='<%# Eval("CODE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Previous Year Budget Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtActualPreviousAmt" Enabled="false" runat="server" Text='<%# Eval("ACTUAL_PREVIOUS_AMOUNT") %>'
                                TabIndex="11" CssClass="EntryFont txtBox_N" Width="150px" FilterType="Numbers,Custom"
                                ValidChars="."></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderr" runat="server" FilterType="Numbers"
                                TargetControlID="txtAnnualBudgetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtActualPreviousAmt" Enabled="false" runat="server" TabIndex="11"
                                CssClass="EntryFont txtBox" Width="150px" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtActualPreviousAmt" Enabled="false" runat="server" Text='<%# Eval("ACTUAL_PREVIOUS_AMOUNT") %>'
                                TabIndex="11" CssClass="EntryFont txtBox_N" Width="150px" FilterType="Numbers,Custom"
                                ValidChars="."></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers"
                                TargetControlID="txtAnnualBudgetAmount" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Increment/Decrement(%)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPercentage" runat="server" Text='<%# Eval("PERCENTAGE") %>' TabIndex="12"
                                CssClass="EntryFont txtBox_N" Width="100px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                            <%-- <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers"
                                TargetControlID="txtAnnualBudgetAmount" />--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPercentage" runat="server" TabIndex="12" CssClass="EntryFont txtBox"
                                Width="100px" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtPercentage" runat="server" Text='<%# Eval("PERCENTAGE") %>' TabIndex="12"
                                CssClass="EntryFont txtBox_N" Width="100px" FilterType="Numbers,Custom" ValidChars="."></asp:TextBox>
                            <%-- <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers"
                                TargetControlID="txtAnnualBudgetAmount" />--%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Annual Budget Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAnnualBudgetAmount" runat="server" Text='<%# Eval("ANL_BUD_AMT") %>'
                                TabIndex="13" CssClass="EntryFont txtBox_N" Width="150px" AutoPostBack="True"
                                FilterType="Numbers,Custom" ValidChars="." OnTextChanged="txtAnnualBudgetAmount_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtAnnualBudgetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAnnualBudgetAmount" runat="server" AutoPostBack="True" OnTextChanged="txtAnnualBudgetAmount_TextChanged"
                                TabIndex="13" CssClass="EntryFont txtBox" Width="150px" Visible="false"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtendec" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtAnnualBudgetAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <%--   <asp:Label ID="lblAnnualBudgetAmountc" Text='<%# Eval("ANL_BUD_AMT") %>' runat="server"
                                Visible="false"></asp:Label>--%>
                            <asp:TextBox ID="txtAnnualBudgetAmount" runat="server" Text='<%# Eval("ANL_BUD_AMT") %>'
                                TabIndex="13" CssClass="EntryFont txtBox_N" Width="150px" AutoPostBack="True"
                                OnTextChanged="txtAnnualBudgetAmount_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtAnnualBudgetAmount" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Period 1" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod01" Enabled="false" runat="server" TabIndex="10" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod01" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod01" Enabled="false" runat="server" TabIndex="10" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod01" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod01" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 2" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod02" Enabled="false" runat="server" TabIndex="11" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod02" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod02" Enabled="false" runat="server" TabIndex="11" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod02" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod02" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 3" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod03" Enabled="false" runat="server" TabIndex="12" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod03" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod03" Enabled="false" runat="server" TabIndex="12" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod03" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod03" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 4" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod04" Enabled="false" runat="server" TabIndex="13" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod04" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod04" Enabled="false" runat="server" TabIndex="13" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod04" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod04" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 5" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod05" Enabled="false" runat="server" TabIndex="14" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod05" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod05" Enabled="false" runat="server" TabIndex="14" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod05" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod05" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 6" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod06" Enabled="false" runat="server" TabIndex="15" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod06" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod06" Enabled="false" runat="server" TabIndex="15" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod06" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod06" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 7" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod07" Enabled="false" runat="server" TabIndex="16" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod07" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod07" Enabled="false" runat="server" TabIndex="16" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod07" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod07" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 8" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod08" Enabled="false" runat="server" TabIndex="17" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod08" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod08" Enabled="false" runat="server" TabIndex="17" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod08" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod08" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 9" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod09" Enabled="false" runat="server" TabIndex="18" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod09" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod09" Enabled="false" runat="server" TabIndex="18" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod09" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod09" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 10" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod10" Enabled="false" runat="server" TabIndex="19" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox><asp:Label
                                    ID="lblPeriod10" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>' Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod10" Enabled="false" runat="server" TabIndex="19" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox><asp:Label ID="lblPeriod10" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                    Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod10" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 11" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod11" Enabled="false" runat="server" TabIndex="20" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod11" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod11" Enabled="false" runat="server" TabIndex="20" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod11" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod11" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period 12" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriod12" Enabled="false" runat="server" TabIndex="21" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod12" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                Width="130px"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriod12" Enabled="false" runat="server" TabIndex="21" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <asp:Label ID="lblPeriod12" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                Width="130px"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriod12" runat="server" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnPeriods" runat="server" CssClass="btn" TabIndex="14" Text="Periods"
                                OnClick="btnPeriods_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnPeriods" runat="server" CssClass="btn" TabIndex="14" Text="Periods"
                                OnClick="btnPeriods_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnPeriods" runat="server" CssClass="btn" TabIndex="14" Text="Periods"
                                OnClick="btnPeriods_Click" Visible="false" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" TabIndex="15" Text="Details"
                                OnClick="btnClick_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" TabIndex="15" Text="Details"
                                OnClick="btnClick_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" TabIndex="15" Text="Details"
                                OnClick="btnClick_Click" Visible="false" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit" Visible="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" Visible="false" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="dvPeriods">
            <asp:HiddenField ID="btnPeriods1" runat="server" />
            <cc2:ModalPopupExtender ID="mpePeriods" runat="server" TargetControlID="btnPeriods1"
                PopupControlID="pnlPeriods" CancelControlID="btnPeriodCancel" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlPeriods" runat="server" BackColor="Violet" Width="50%" Height="70%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Period 1" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop1" runat="server" TabIndex="25" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop1" />
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Period 2" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop2" runat="server" TabIndex="26" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop2" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Period 3" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop3" runat="server" TabIndex="27" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop3" />
                            </td>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="Period 4" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop4" runat="server" TabIndex="28" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop4" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="Period 5" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop5" runat="server" TabIndex="27" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop5" />
                            </td>
                            <td>
                                <asp:Label ID="Label9" runat="server" Text="Period 6" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop6" runat="server" TabIndex="28" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop6" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Period 7" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop7" runat="server" TabIndex="27" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender67" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop7" />
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Period 8" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop8" runat="server" TabIndex="28" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender77" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop8" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="Period 9" Width="75px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop9" runat="server" TabIndex="29" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender87" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop9" />
                            </td>
                            <td>
                                <asp:Label ID="Label10" runat="server" Text="Period 10" Width="60px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop10" runat="server" TabIndex="30" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender97" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop10" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label11" runat="server" Text="Period 11" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop11" runat="server" TabIndex="31" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop11" />
                            </td>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="Period 12" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodPop12" runat="server" TabIndex="32" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                    CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtPeriodPop12" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button runat="server" CssClass="button" ID="btnPeriodOk" Text="Ok" Width="60px"
                                    OnClick="btnPeriodOk_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnPeriodCancel" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="9" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
