﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.GL
{
    public partial class BudgetVarianceEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtGridData = new DataTable();
        DataTable dtAccBaldtl = new DataTable();
        DataTable dtAccBal_currwise = new DataTable();
        DataTable dtAccBal_Daywise = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;

        # region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;

            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;

            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion

        # region Save
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Session[FINSessionConstants.GridData] = null;

                Startup();

                FillComboBox();

                EntityData = null;
                btnSave.Visible = false;

                dtGridData = DBMethod.ExecuteQuery(Budget_DAL.GetBudgetVarianceDetails(ddlBudgerYr.SelectedValue, ddlAccGroup.SelectedValue)).Tables[0];
                BindGrid(dtGridData);



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {

            Budget_BLL.GetBudgetCalYear(ref ddlBudgerYr);
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    Label15.Text = Prop_File_Data["Current_Year_P"];
                    lblPeriod1.Text = Prop_File_Data["Period_1_P"];
                    lblPeriod2.Text = Prop_File_Data["Period_2_P"];
                    lblPeriod3.Text = Prop_File_Data["Period_3_P"];
                    lblPeriod4.Text = Prop_File_Data["Period_4_P"];
                    lblPeriod5.Text = Prop_File_Data["Period_5_P"];
                    lblPeriod6.Text = Prop_File_Data["Period_6_P"];
                    lblPeriod7.Text = Prop_File_Data["Period_7_P"];
                    lblPeriod8.Text = Prop_File_Data["Period_8_P"];
                    lblPeriod9.Text = Prop_File_Data["Period_9_P"];
                    lblPeriod10.Text = Prop_File_Data["Period_10_P"];
                    lblPeriod11.Text = Prop_File_Data["Period_11_P"];
                    lblPeriod12.Text = Prop_File_Data["Period_12_P"];
                   

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Budget_Amount", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative(p.Field<String>("Budget_Amount"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Previous_Amount", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative(p.Field<String>("Previous_Amount"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Difference", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative(p.Field<String>("Difference"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnDetails_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                DataTable dtData = new DataTable();

                txtPeriodPop1.Text = string.Empty;
                txtPeriodPop2.Text = string.Empty;
                txtPeriodPop3.Text = string.Empty;
                txtPeriodPop4.Text = string.Empty;
                txtPeriodPop5.Text = string.Empty;
                txtPeriodPop6.Text = string.Empty;
                txtPeriodPop7.Text = string.Empty;
                txtPeriodPop8.Text = string.Empty;
                txtPeriodPop9.Text = string.Empty;
                txtPeriodPop10.Text = string.Empty;
                txtPeriodPop11.Text = string.Empty;
                txtPeriodPop12.Text = string.Empty;

                dtData = DBMethod.ExecuteQuery(Budget_DAL.getMonthlyPeriodAmt(gvData.DataKeys[gvr.RowIndex].Values["BUDGET_DTL_ID"].ToString())).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtPeriodPop1.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_01"].ToString();
                        txtPeriodPop2.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_02"].ToString();
                        txtPeriodPop3.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_03"].ToString();
                        txtPeriodPop4.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_04"].ToString();
                        txtPeriodPop5.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_05"].ToString();
                        txtPeriodPop6.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_06"].ToString();
                        txtPeriodPop7.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_07"].ToString();
                        txtPeriodPop8.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_08"].ToString();
                        txtPeriodPop9.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_09"].ToString();
                        txtPeriodPop10.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_10"].ToString();
                        txtPeriodPop11.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_11"].ToString();
                        txtPeriodPop12.Text = dtData.Rows[0]["BUDGET_MONTH_AMT_12"].ToString();

                        mpePeriods.Show();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlBudgerYr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (ddlBudgerYr.SelectedValue != null)
                {
                    Budget_BLL.getGroupDetails(ref ddlAccGroup, ddlBudgerYr.SelectedValue);
                    dtGridData = DBMethod.ExecuteQuery(Budget_DAL.GetBudgetVarianceDetails(ddlBudgerYr.SelectedValue)).Tables[0];
                    BindGrid(dtGridData);
                    Session["Report"] = dtGridData;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDataBound(object o, GridViewRowEventArgs e)
        {
            //Assumes the Price column is at index 4
            if (e.Row.RowType == DataControlRowType.DataRow)
                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                 e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                 e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
        }
        protected void ddlAccGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (ddlBudgerYr.SelectedValue != null && ddlAccGroup.SelectedValue != null)
                {
                    dtGridData = DBMethod.ExecuteQuery(Budget_DAL.GetBudgetVarianceDetails(ddlBudgerYr.SelectedValue, ddlAccGroup.SelectedValue)).Tables[0];
                    BindGrid(dtGridData);
                    Session["Report"] = dtGridData;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                //if (ddlperiod.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_NAME, ddlperiod.SelectedItem.Text.ToString());
                //}
                //if (ddlAccountCodes.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.CODE_ID, ddlAccountCodes.SelectedValue.ToString());

                //}
                //if (txtFromAccNo.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_AccNo", txtFromAccNo.Text);
                //}
                //if (txtToAccNo.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_AccNo", txtToAccNo.Text);
                //}
                //if (txtFromValue.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Value", txtFromValue.Text);
                //    htFilterParameter.Add("FromValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromValue.Text));
                //}
                //if (txtToValue.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Value", txtToValue.Text);
                //    htFilterParameter.Add("ToValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtToValue.Text));
                //}
                //if (chkWithZero.Checked)
                //{
                //    htFilterParameter.Add("WITHZERO", "TRUE");
                //}
                //else
                //{
                //    htFilterParameter.Add("WITHZERO", "FALSE");
                //}
                //if (ddlGlobalSegment.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SEGMENT_VALUE", ddlGlobalSegment.SelectedItem.Text);
                //    htFilterParameter.Add("SEGMENT_VALUE_ID", ddlGlobalSegment.SelectedItem.Value);
                //}
                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.FROM_DATE txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.TO_DATE, txtToDate.Text);
                //}
                if (ddlAccGroup.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("Acc_Group", ddlAccGroup.SelectedItem.Text);
                }
                if (ddlBudgerYr.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("Budget_yr", ddlBudgerYr.SelectedItem.Text);
                }

                
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["Report"];

                DataSet ds5 = new DataSet();
                dtGrid = dtGrid.Copy();
                ds5.Tables.Add(dtGrid);
                ds5.AcceptChanges();
                ReportData = ds5;
                // ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodes());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedSysAccBalReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnExport_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                    dtGridData.Columns.Remove("BUDGET_DTL_ID");
                    dtGridData.AcceptChanges();

                    Session[FINSessionConstants.GridData] = dtGridData;

                    ImageButton img_btn_tmp = (ImageButton)sender;
                    Response.Redirect("../ExportGridData.aspx?ExportId=" + img_btn_tmp.CommandName, true);
                }
                else
                {
                    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
    }
}