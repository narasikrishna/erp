﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SegmentsEntry.aspx.cs" Inherits="FIN.Client.GL.SegmentsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblSegmentName">
                Segment Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px;">
                <asp:TextBox ID="txtSegmentName" TabIndex="1" CssClass="validate[required] RequiredField txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblSegmentNameAR">
                Segment Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px;">
                <asp:TextBox ID="txtSegmentNameAR" TabIndex="2" CssClass=" txtBox_ol" runat="server"
                    MaxLength="240"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtEffectiveDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" MaxLength="10" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEffectiveDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffectiveDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDescription" CssClass="validate[required] RequiredField  txtBox"
                    runat="server" TextMode="MultiLine" MaxLength="250" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    runat="server" MaxLength="10" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDependent">
                Dependent
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkdependent" runat="server" Checked="false" TabIndex="6" AutoPostBack="True"
                    OnCheckedChanged="chkdependent_CheckedChanged" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblParentSegment">
                Parent Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlParentSegment" runat="server" Enabled="false" CssClass="RequiredField validate[required] ddlStype"
                    AutoPostBack="True" TabIndex="7" OnSelectedIndexChanged="ddlParentSegment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="SEGMENT_VALUE_ID,PK_ID,EFFECTIVE_END_DT,DELETED,PARENT_SEGMENT_VALUE_ID"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Parent Segment value">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlParentSegmentValue" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="9">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlParentSegmentValue" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="9">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblParentSegmentValue" runat="server" Width="98" Text='<%# Eval("PARENT_SEGMENT_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSegval" TabIndex="10" runat="server" CssClass="EntryFont RequiredField txtBox"
                                MaxLength="100" Width="98" Text='<%# Eval("SEGMENT_VALUE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSegval" TabIndex="10" runat="server" CssClass="EntryFont RequiredField txtBox"
                                MaxLength="100" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSegval" Style="word-wrap: break-word; white-space: pre-wrap;" runat="server"
                                Width="98" Text='<%# Eval("SEGMENT_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment Value(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSegvalAR" TabIndex="11" runat="server" CssClass="EntryFont txtBox_ol"
                                MaxLength="240" Width="98" Text='<%# Eval("SEGMENT_VALUE_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSegvalAR" TabIndex="11" runat="server" CssClass="EntryFont txtBox_ol"
                                MaxLength="240" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSegvalAR" Style="word-wrap: break-word; white-space: pre-wrap;"
                                runat="server" Width="98" Text='<%# Eval("SEGMENT_VALUE_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" runat="server" TabIndex="12" CssClass="EntryFont RequiredField  txtBox"
                                Width="98" Text='<%#  Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" runat="server" TabIndex="12" CssClass="EntryFont RequiredField txtBox"
                                Width="98"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Width="98" Text='<%# Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" runat="server" TabIndex="13" CssClass="EntryFont  txtBox"
                                Width="98" Text='<%#  Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="13" runat="server" CssClass="EntryFont  txtBox"
                                Width="98"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Width="98" Text='<%# Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="14" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="14" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" TabIndex="16" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip=" Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" TabIndex="17" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" TabIndex="18" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" TabIndex="19" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="15" runat="server" AlternateText="Add"
                                ToolTip="Add" CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button AutoPostBack="True" ID="btnSave" runat="server" Text="Save" CssClass="btn"
                            OnClick="btnSave_Click" TabIndex="20" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="21" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="22" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="23" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        //        $(document).ready(function () {
        //            fn_changeLng('<%= Session["Sel_Lng"] %>');
        //        });

        //        $(document).ready(function () {

        //        });

        //        $("#FINContent_btnSave").click(function (evt) {
        //            evt.preventDefault();
        //            if ($("#form1").validationEngine('validate') == false)
        //                return false;
        //        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
