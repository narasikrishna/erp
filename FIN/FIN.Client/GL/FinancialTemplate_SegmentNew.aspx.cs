﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.GL
{
    public partial class FinancialTemplate_SegmentNew : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                AssingtoControl();
                Session["SubGroupData"] = null;
                Session["SubAccountData"] = null;
                Session["SubSegmentData"] = null;
            }
            else
            {
                Reload_BS_Structure();
            }
           
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            UserRightsChecking();



        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    imgbtnAdd.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    imgbtnAdd.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // imgBtnDelete.Visible = false;
                }
            }
        }


        private void AssingtoControl()
        {
            Startup();
            FillComboBox();
            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
            {
                txtTempName.Text = Master.StrRecordId;
                Reload_BS_Structure();
                txtTempName.Enabled = false;

            }
        }
        private void FillComboBox()
        {
            ddlOperator.Items.Add(new ListItem("---Select---", ""));
            ddlOperator.Items.Add(new ListItem("+", "+"));
            ddlOperator.Items.Add(new ListItem("-", "-"));
            ddlOperator.Items.Add(new ListItem("*", "*"));
            ddlOperator.Items.Add(new ListItem("/", "/"));
            ddlOperator.Items.Add(new ListItem("%", "%"));


            ddlGFOperation.Items.Add(new ListItem("---Select---", ""));
            ddlGFOperation.Items.Add(new ListItem("+", "+"));
            ddlGFOperation.Items.Add(new ListItem("-", "-"));
            ddlGFOperation.Items.Add(new ListItem("*", "*"));
            ddlGFOperation.Items.Add(new ListItem("/", "/"));
            ddlGFOperation.Items.Add(new ListItem("%", "%"));

            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);


            FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddlFromSegment, "SEG_ID-0000000101");
            FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddlToSegment, "SEG_ID-0000000101");
        }


        private void Reload_BS_Structure()
        {
            try
            {
                DataTable dt_BS_TMPL_DATA = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TemplateStructure(txtTempName.Text)).Tables[0];
                string str_ORG_STR = "";


                // ------------------------MODEL 2 -------------------
                if (dt_BS_TMPL_DATA.Rows.Count > 0)
                {
                    DataTable dt_EmpProcessList = new DataTable();
                    dt_EmpProcessList.Columns.Add("GROUP_NAME");
                    dt_EmpProcessList.Columns.Add("BS_TMPL_ID");
                    dt_EmpProcessList.Columns.Add("BS_DISPLAY_NAME");

                    dt_EmpProcessList.Columns.Add("STATUS");

                    var var_HeadEmpList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_GROUP_NAME"].ToString().Contains("HEAD"));
                    if (var_HeadEmpList.Any())
                    {
                        DataTable dt_data = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);



                        string str_Replace_str = "";
                        string str_new_replace_str = "";
                        string str_tmp;
                        str_ORG_STR += "  <ul  id='BSTemp'  > ";
                        str_ORG_STR += "  <li><a onclick=fn_GroupAccClick('','" + txtTempName.Text.Replace("-", "").Replace(" ", "") + "')> " + txtTempName.Text + "</a>";
                        str_ORG_STR += " <ul>";
                        for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                        {
                            DataRow dr = dt_EmpProcessList.NewRow();
                            dr["GROUP_NAME"] = dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                            dr["BS_TMPL_ID"] = dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            dr["BS_DISPLAY_NAME"] = dt_data.Rows[iLoop]["BS_DISPLAY_NAME"].ToString();
                            dr["STATUS"] = "N";
                            dt_EmpProcessList.Rows.Add(dr);

                            str_ORG_STR += "<li>";
                            str_ORG_STR += dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            str_ORG_STR += "</li>";
                            DataRow[] dr_col;
                            do
                            {
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                                if (dr_col.Length > 0)
                                {
                                    str_Replace_str = "<li>";
                                    str_Replace_str += dr_col[0]["BS_TMPL_ID"].ToString();
                                    str_Replace_str += "</li>";
                                    str_new_replace_str = "<li> <a onclick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["BS_DISPLAY_NAME"].ToString().Replace(" ", "_") + "')> " + dr_col[0]["BS_DISPLAY_NAME"].ToString() + "</a>";
                                    str_new_replace_str += "<UL>";

                                    var var_empList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_ID"].ToString().Contains(dr_col[0]["BS_TMPL_ID"].ToString()));
                                    str_tmp = "";
                                    if (var_empList.Any())
                                    {
                                        DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                                        for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                                        {
                                            str_tmp += "<li>";
                                            str_tmp += dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            str_tmp += "</li>";

                                            dr = dt_EmpProcessList.NewRow();
                                            dr["GROUP_NAME"] = dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                                            dr["BS_TMPL_ID"] = dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            dr["BS_DISPLAY_NAME"] = dt_EMP_List.Rows[jLoop]["BS_DISPLAY_NAME"].ToString();
                                            dr["STATUS"] = "N";
                                            dt_EmpProcessList.Rows.Add(dr);

                                        }
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                                    }
                                    else
                                    {
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                                    }

                                    dr_col[0]["STATUS"] = "Y";
                                    dt_EmpProcessList.AcceptChanges();
                                }
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                            } while (dr_col.Length > 0);

                        }
                        str_ORG_STR += " </ul>";
                        str_ORG_STR += "  </li> ";
                        str_ORG_STR += "  </ul> ";

                    }
                }
                else
                {
                    str_ORG_STR += "  <ul  id='BSTemp'> ";
                    str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + txtTempName.Text + "</div>";
                    str_ORG_STR += "  </li> ";
                    str_ORG_STR += "  </ul> ";
                }

                div_Template.InnerHtml = str_ORG_STR;
                ScriptManager.RegisterStartupScript(imgbtnAdd, typeof(Button), "LoadGraph", "fn_showGraph()", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }


        protected void txtTempName_TextChanged(object sender, EventArgs e)
        {
            Reload_BS_Structure();
        }





        protected void imgbtnAdd_Click(object sender, ImageClickEventArgs e)
        {
            if (div_Group.Visible)
            {
                AddTopGroup();

                if (rbl_subType.SelectedValue == "GROUP")
                {
                    AddSubGroup();
                }
                else if (rbl_subType.SelectedValue == "ACCOUNT")
                {
                    AddSubAccount();
                }
            }
            else
            {
                AddSubSegment();
            }
            LoadSelectedDataValue();
            Reload_BS_Structure();
        }

        private void AddTopGroup()
        {
            if (txtTempName.Text.ToString().Length == 0)
            {
                ErrorCollection.Add("InvlaidTempName", "Please Enter Template Name");
                return;
            }

            if (txtGroupName.Text.ToString().Length == 0)
            {
                ErrorCollection.Add("Please Enter the Group Name", " Please Enter Group Name ");
                return;
            }
            if (txtGroupNumber.Text.ToString().Length == 0)
            {
                ErrorCollection.Add("Please Enter the Group Number", " Please Enter Group Number ");
                return;
            }

            DataTable dt_dubData = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_Template4Group(txtTempName.Text, txtGroupNumber.Text)).Tables[0];
            if (dt_dubData.Rows.Count > 0)
            {
                if (hf_BS_TMPL_GROUP_ID.Value.ToString().Length > 0)
                {
                    if (dt_dubData.Rows.Count > 1)
                    {
                        ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                        return;
                    }
                    else
                    {
                        if (dt_dubData.Rows[0]["BS_TMPL_ID"].ToString() != hf_BS_TMPL_GROUP_ID.Value.ToString())
                        {
                            ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                            return;
                        }
                    }
                }
                else
                {
                    ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                    return;
                }
            }


            GL_COSTCENTER_TEMPLATE gL_COSTCENTER_TEMPLATE = new GL_COSTCENTER_TEMPLATE();
            if (hf_BS_TMPL_GROUP_ID.Value.ToString().Length > 0)
            {

                using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                {
                    gL_COSTCENTER_TEMPLATE = userCtx.Find(r =>
                        (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                        ).SingleOrDefault();
                }

            }

            gL_COSTCENTER_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
            gL_COSTCENTER_TEMPLATE.BS_TYPE = "GROUP";
            gL_COSTCENTER_TEMPLATE.BS_TOT_REQ = chkTotReq.Checked ? FINAppConstants.Y : FINAppConstants.N;
            gL_COSTCENTER_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
            gL_COSTCENTER_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
            gL_COSTCENTER_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;

            gL_COSTCENTER_TEMPLATE.BS_GROUP_NAME = txtGroupName.Text;
            if (txtGroupNumber.Text.ToString().Trim().Length > 0)
            {
                gL_COSTCENTER_TEMPLATE.BS_GROUP_NO = int.Parse(txtGroupNumber.Text);
            }
            else
            {
                gL_COSTCENTER_TEMPLATE.BS_GROUP_NO = null;
            }

            if (txtNotes.Text.ToString().Trim().Length > 0)
            {
                gL_COSTCENTER_TEMPLATE.NOTES = int.Parse(txtNotes.Text);
            }
            else
            {
                gL_COSTCENTER_TEMPLATE.NOTES = null;
            }


            if (chkFormulaReq.Checked)
            {
                gL_COSTCENTER_TEMPLATE.BS_FORMULA_GROUP = FINAppConstants.Y;
                gL_COSTCENTER_TEMPLATE.BS_FORMULA = txtFormula.Text;
            }
            else
            {
                gL_COSTCENTER_TEMPLATE.BS_FORMULA_GROUP = FINAppConstants.N;
                gL_COSTCENTER_TEMPLATE.BS_FORMULA = null;
            }

            if (hf_BS_TMPL_GROUP_ID.Value.ToString().Length > 0)
            {
                // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                gL_COSTCENTER_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                gL_COSTCENTER_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE, true);
            }
            else
            {
                gL_COSTCENTER_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                gL_COSTCENTER_TEMPLATE.CREATED_BY = this.LoggedUserName;
                gL_COSTCENTER_TEMPLATE.CREATED_DATE = DateTime.Today;
                DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
            }

            hf_BS_TMPL_GROUP_ID.Value = gL_COSTCENTER_TEMPLATE.BS_TMPL_ID;

        }


        private void AddSubGroup()
        {
            if (Session["SubGroupData"] == null)
                return;

            DataTable dt_SubGroup = (DataTable)Session["SubGroupData"];

            if (dt_SubGroup.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dt_SubGroup.Rows.Count; iLoop++)
                {

                    GL_COSTCENTER_TEMPLATE gL_COSTCENTER_TEMPLATE = new GL_COSTCENTER_TEMPLATE();
                    if (dt_SubGroup.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gL_COSTCENTER_TEMPLATE = userCtx.Find(r =>
                                (r.BS_TMPL_ID == dt_SubGroup.Rows[iLoop]["BS_TMPL_ID"].ToString())
                                ).SingleOrDefault();
                        }

                    }

                    gL_COSTCENTER_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                    gL_COSTCENTER_TEMPLATE.BS_TYPE = "GROUP";
                    gL_COSTCENTER_TEMPLATE.BS_TOT_REQ = dt_SubGroup.Rows[iLoop]["BS_TOT_REQ"].ToString();
                    gL_COSTCENTER_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    gL_COSTCENTER_TEMPLATE.BS_GROUP_NAME = dt_SubGroup.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                    if (dt_SubGroup.Rows[iLoop]["BS_GROUP_NO"].ToString().Trim().Length > 0)
                    {
                        gL_COSTCENTER_TEMPLATE.BS_GROUP_NO = decimal.Parse(dt_SubGroup.Rows[iLoop]["BS_GROUP_NO"].ToString());
                    }
                    else
                    {
                        gL_COSTCENTER_TEMPLATE.BS_GROUP_NO = null;
                    }

                    if (dt_SubGroup.Rows[iLoop]["NOTES"].ToString().Trim().Length > 0)
                    {
                        gL_COSTCENTER_TEMPLATE.NOTES = int.Parse(dt_SubGroup.Rows[iLoop]["NOTES"].ToString());
                    }
                    else
                    {
                        gL_COSTCENTER_TEMPLATE.NOTES = null;
                    }

                    gL_COSTCENTER_TEMPLATE.BS_FORMULA_GROUP = dt_SubGroup.Rows[iLoop]["BS_FORMULA_GROUP"].ToString();
                    gL_COSTCENTER_TEMPLATE.BS_FORMULA = dt_SubGroup.Rows[iLoop]["BS_FORMULA"].ToString();

                    if (hf_BS_TMPL_GROUP_ID.Value.ToString().Trim().Length > 0)
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();

                        GL_COSTCENTER_TEMPLATE gl_Parent_BS = new GL_COSTCENTER_TEMPLATE();
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can't add Group Inside the account");

                                return;
                            }
                            gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                    }




                    if (dt_SubGroup.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        //gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        gL_COSTCENTER_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.MODIFIED_DATE = DateTime.Today;

                        if (dt_SubGroup.Rows[iLoop]["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            DBMethod.DeleteEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                        else
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE, true);
                        }
                    }
                    else
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                        gL_COSTCENTER_TEMPLATE.CREATED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.CREATED_DATE = DateTime.Today;
                        if (dt_SubGroup.Rows[iLoop]["DELETED"].ToString() != FINAppConstants.Y)
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                    }
                }
            }
        }
        private void AddSubAccount()
        {
            if (Session["SubAccountData"] == null)
                return;

            DataTable dt_SubAccount = (DataTable)Session["SubAccountData"];

            if (dt_SubAccount.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dt_SubAccount.Rows.Count; iLoop++)
                {

                    GL_COSTCENTER_TEMPLATE gL_COSTCENTER_TEMPLATE = new GL_COSTCENTER_TEMPLATE();
                    if (dt_SubAccount.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gL_COSTCENTER_TEMPLATE = userCtx.Find(r =>
                                (r.BS_TMPL_ID == dt_SubAccount.Rows[iLoop]["BS_TMPL_ID"].ToString())
                                ).SingleOrDefault();
                        }

                    }
                    gL_COSTCENTER_TEMPLATE.BS_ACC_ID = dt_SubAccount.Rows[iLoop]["BS_ACC_ID"].ToString();
                    gL_COSTCENTER_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                    gL_COSTCENTER_TEMPLATE.BS_TYPE = "ACCOUNT";
                    gL_COSTCENTER_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    gL_COSTCENTER_TEMPLATE.BS_GROUP_NAME = dt_SubAccount.Rows[iLoop]["BS_GROUP_NAME"].ToString();

                    if (hf_BS_TMPL_GROUP_ID.Value.ToString().Trim().Length > 0)
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();

                        GL_COSTCENTER_TEMPLATE gl_Parent_BS = new GL_COSTCENTER_TEMPLATE();
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can't add Group Inside the account");

                                return;
                            }
                            gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                    }
                    if (dt_SubAccount.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        gL_COSTCENTER_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                        if (dt_SubAccount.Rows[iLoop]["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            DBMethod.DeleteEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                        else
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE, true);
                        }
                    }
                    else
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                        gL_COSTCENTER_TEMPLATE.CREATED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.CREATED_DATE = DateTime.Today;
                        if (dt_SubAccount.Rows[iLoop]["DELETED"].ToString() != FINAppConstants.Y)
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                    }
                }
            }
        }


        private void AddSubSegment()
        {
            if (Session["SubSegmentData"] == null)
                return;

            DataTable dt_SubSegment = (DataTable)Session["SubSegmentData"];

            if (dt_SubSegment.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dt_SubSegment.Rows.Count; iLoop++)
                {

                    GL_COSTCENTER_TEMPLATE gL_COSTCENTER_TEMPLATE = new GL_COSTCENTER_TEMPLATE();
                    if (dt_SubSegment.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gL_COSTCENTER_TEMPLATE = userCtx.Find(r =>
                                (r.BS_TMPL_ID == dt_SubSegment.Rows[iLoop]["BS_TMPL_ID"].ToString())
                                ).SingleOrDefault();
                        }

                    }

                    gL_COSTCENTER_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                    gL_COSTCENTER_TEMPLATE.BS_TYPE = "SEGMENT";
                    gL_COSTCENTER_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                    gL_COSTCENTER_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    gL_COSTCENTER_TEMPLATE.BS_ACC_NAME = dt_SubSegment.Rows[iLoop]["BS_ACC_NAME"].ToString();
                    gL_COSTCENTER_TEMPLATE.BS_ACC_ID = dt_SubSegment.Rows[iLoop]["BS_ACC_ID"].ToString();

                    if (hf_BS_TMPL_GROUP_ID.Value.ToString().Trim().Length > 0)
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();

                        GL_COSTCENTER_TEMPLATE gl_Parent_BS = new GL_COSTCENTER_TEMPLATE();
                        using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can't add Group Inside the account");

                                return;
                            }
                            gL_COSTCENTER_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                    }
                    if (dt_SubSegment.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                    {
                        // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        gL_COSTCENTER_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                        if (dt_SubSegment.Rows[iLoop]["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            DBMethod.DeleteEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                        else
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE, true);
                        }
                    }
                    else
                    {
                        gL_COSTCENTER_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                        gL_COSTCENTER_TEMPLATE.CREATED_BY = this.LoggedUserName;
                        gL_COSTCENTER_TEMPLATE.CREATED_DATE = DateTime.Today;
                        if (dt_SubSegment.Rows[iLoop]["DELETED"].ToString() != FINAppConstants.Y)
                        {
                            DBMethod.SaveEntity<GL_COSTCENTER_TEMPLATE>(gL_COSTCENTER_TEMPLATE);
                        }
                    }
                }
            }
        }
        protected void chkFormulaReq_CheckedChanged(object sender, EventArgs e)
        {
            ShowFormulaColumn();
        }
        private void ShowFormulaColumn()
        {
            if (chkFormulaReq.Checked)
            {
                div_Formula.Visible = true;
                txtFormula.Visible = true;
                FillGroupNumber();
                chkTotReq.Checked = false;
            }
            else
            {
                div_Formula.Visible = false;
                txtFormula.Visible = false;
            }
        }

        private void FillGroupNumber()
        {
            FIN.BLL.GL.FinancialTemplate_Segment_BLL.fn_getTemplateGroupNumber(ref ddlGroupNuber, txtTempName.Text);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFormula.Text.ToString().Contains("#" + ddlGroupNuber.SelectedValue.ToString() + "#"))
                {
                    ErrorCollection.Add("InvalidGroup", "Selected Group already available in the formula");
                    return;
                }

                if (ddlGroupNuber.SelectedValue.ToString().Length > 0)
                {
                    txtFormula.Text = txtFormula.Text + "#" + ddlGroupNuber.SelectedValue.ToString() + "#";
                }
                if (ddlOperator.SelectedValue.ToString().Length > 0)
                {
                    txtFormula.Text = txtFormula.Text + ddlOperator.SelectedValue.ToString();
                }
                ddlOperator.SelectedIndex = 0;
                ddlGroupNuber.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {
                Reload_BS_Structure();
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void chkTotReq_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTotReq.Checked)
            {
                chkFormulaReq.Checked = false;
            }
            ShowFormulaColumn();
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            Reload_BS_Structure();
        }

        protected void btnSelectData_Click(object sender, EventArgs e)
        {
            LoadSelectedDataValue();
        }

        private void LoadSelectedDataValue()
        {
            imgbtnAdd.Visible = true;
            div_Group.Visible = false;
            div_SubGroup.Visible = false;
            div_subAccount.Visible = false;
            div_SubSegment.Visible = false;
            div_selSubtype.Visible = false;
            rbl_subType.Enabled = true;

            Session["SubGroupData"] = null;
            Session["SubAccountData"] = null;
            Session["SubSegmentData"] = null;

            GL_COSTCENTER_TEMPLATE gL_COSTCENTER_TEMPLATE = new GL_COSTCENTER_TEMPLATE();
            using (IRepository<GL_COSTCENTER_TEMPLATE> userCtx = new DataRepository<GL_COSTCENTER_TEMPLATE>())
            {
                gL_COSTCENTER_TEMPLATE = userCtx.Find(r =>
                    (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                    ).SingleOrDefault();
            }
            if (gL_COSTCENTER_TEMPLATE != null)
            {
                if (gL_COSTCENTER_TEMPLATE.BS_TYPE.ToString() == "GROUP")
                {
                    div_Group.Visible = true;
                    div_selSubtype.Visible = true;
                    txtGroupName.Text = gL_COSTCENTER_TEMPLATE.BS_GROUP_NAME;
                    txtGroupNumber.Text = gL_COSTCENTER_TEMPLATE.BS_GROUP_NO.ToString();
                    if (gL_COSTCENTER_TEMPLATE.NOTES != null)
                    {
                        txtNotes.Text = gL_COSTCENTER_TEMPLATE.NOTES.ToString();
                    }
                    chkTotReq.Checked = false;
                    if (gL_COSTCENTER_TEMPLATE.BS_TOT_REQ == FINAppConstants.Y)
                    {
                        chkTotReq.Checked = true;
                    }
                    chkFormulaReq.Checked = false;
                    txtFormula.Text = gL_COSTCENTER_TEMPLATE.BS_FORMULA;
                    if (gL_COSTCENTER_TEMPLATE.BS_FORMULA_GROUP == FINAppConstants.Y)
                    {
                        chkFormulaReq.Checked = true;
                        div_selSubtype.Visible = false;
                        ShowFormulaColumn();
                        return;
                    }

                    DataTable dt_subGroup = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.getSubGroupDet(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                    Session["SubGroupData"] = dt_subGroup;
                    DataTable dt_subAccount = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.getSubAccountDet(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                    Session["SubAccountData"] = dt_subAccount;
                    if (dt_subGroup.Rows.Count > 0)
                    {
                        rbl_subType.Enabled = false;
                        if (dt_subGroup.Rows[0]["BS_TYPE"].ToString() == "GROUP")
                        {
                            rbl_subType.SelectedValue = "GROUP";
                            BindGrid(dt_subGroup);
                        }
                        else
                        {
                            rbl_subType.SelectedValue = "ACCOUNT";
                            BindAccountGrid(dt_subAccount);
                        }

                    }

                    showSubGroupAcct();


                }
                else if (gL_COSTCENTER_TEMPLATE.BS_TYPE.ToString() == "ACCOUNT")
                {
                    div_SubSegment.Visible = true;
                    txtSelectedAccount.Text = gL_COSTCENTER_TEMPLATE.BS_GROUP_NAME;
                    DataTable dt_subSegment = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.getSubSegmentDet(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                    Session["SubSegmentData"] = dt_subSegment;
                    BindSegmentGrid(dt_subSegment);
                }
                else if (gL_COSTCENTER_TEMPLATE.BS_TYPE.ToString() == "SEGMENT")
                {

                }

            }
            else
            {
                hf_BS_TMPL_GROUP_ID.Value = "";
                div_selSubtype.Visible = false;
                div_Group.Visible = true;
                txtGroupName.Text = "";
                txtGroupNumber.Text = "";
                txtNotes.Text = "";
                hf_BS_TMPL_ID.Value = "";
            }
        }


        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txt_GGroupname = gvr.FindControl("txtGGroupname") as TextBox;
            TextBox txt_GGroupNumber = gvr.FindControl("txtGGroupNumber") as TextBox;
            TextBox txt_GGroupNotes = gvr.FindControl("txtGGroupNotes") as TextBox;
            CheckBox chk_GtotReq = gvr.FindControl("chkGtotReq") as CheckBox;
            CheckBox chk_GFormula = gvr.FindControl("chkGFormula") as CheckBox;
            TextBox txt_GFormula = gvr.FindControl("txtGFormula") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["BS_TMPL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txt_GGroupname;
            slControls[1] = txt_GGroupNumber;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = "Group Name" + " ~ " + "Group Number";
            //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            string strCondition = "BS_GROUP_NAME='" + txt_GGroupname.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            drList["BS_GROUP_NAME"] = txt_GGroupname.Text;
            drList["BS_GROUP_NO"] = txt_GGroupNumber.Text;
            if (txt_GGroupNotes.Text.ToString().Length > 0)
            {
                drList["NOTES"] = txt_GGroupNotes.Text;
            }
            else
            {
                drList["NOTES"] = DBNull.Value;
            }
            if (chk_GtotReq.Checked)
            {
                drList["BS_TOT_REQ"] = FINAppConstants.Y;
            }
            else
            {
                drList["BS_TOT_REQ"] = FINAppConstants.N;
            }
            if (chk_GFormula.Checked)
            {
                drList["BS_FORMULA_GROUP"] = FINAppConstants.Y;
            }
            else
            {
                drList["BS_FORMULA_GROUP"] = FINAppConstants.N;
            }
            drList["BS_FORMULA"] = txt_GFormula.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        protected void gvSubGroup_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvSubGroup.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["SubGroupData"] != null)
                {
                    dtGridData = (DataTable)Session["SubGroupData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {

                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvSubGroup.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvSubGroup_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubGroupData"] != null)
                {
                    dtGridData = (DataTable)Session["SubGroupData"];
                }

                DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TemplateData4TmplId(dtGridData.Rows[e.RowIndex]["BS_TMPL_ID"].ToString())).Tables[0];
                if (dt_data.Rows.Count > 0)
                {
                    ErrorCollection.Add("Child RecordFound", "Child Record Found");
                    return;
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvSubGroup_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubGroupData"] != null)
                {
                    dtGridData = (DataTable)Session["SubGroupData"];
                }
                gvSubGroup.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                //GridViewRow gvr = gvSubGroup.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvSubGroup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvSubGroup_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["SubGroupData"] != null)
                {
                    dtGridData = (DataTable)Session["SubGroupData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvSubGroup.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvSubGroup_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubGroupData"] != null)
                {
                    dtGridData = (DataTable)Session["SubGroupData"];
                }
                gvSubGroup.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvSubGroup_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvSubGroup.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["SubGroupData"] = dtData;


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvSubGroup.DataSource = dt_tmp;
                gvSubGroup.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }






        private DataRow AssignToAccountGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_AccName = gvr.FindControl("ddlAccName") as DropDownList;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["BS_TMPL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_AccName;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            string strMessage = "Account Name";
            //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            string strCondition = "BS_ACC_ID='" + ddl_AccName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            drList["BS_GROUP_NAME"] = ddl_AccName.SelectedItem.Text;
            drList["BS_ACC_ID"] = ddl_AccName.SelectedValue.ToString();
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        protected void gvsubAccount_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvsubAccount.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["SubAccountData"] != null)
                {
                    dtGridData = (DataTable)Session["SubAccountData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToAccountGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {

                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvsubAccount.EditIndex = -1;
                    BindAccountGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvsubAccount_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubAccountData"] != null)
                {
                    dtGridData = (DataTable)Session["SubAccountData"];
                }
                DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TemplateData4TmplId(dtGridData.Rows[e.RowIndex]["BS_TMPL_ID"].ToString())).Tables[0];
                if (dt_data.Rows.Count > 0)
                {
                    ErrorCollection.Add("Child RecordFound", "Child Record Found");
                    return;
                }

                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindAccountGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvsubAccount_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubAccountData"] != null)
                {
                    dtGridData = (DataTable)Session["SubAccountData"];
                }
                gvsubAccount.EditIndex = e.NewEditIndex;
                BindAccountGrid(dtGridData);
                //GridViewRow gvr = gvsubAccount.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvsubAccount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvsubAccount_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["SubAccountData"] != null)
                {
                    dtGridData = (DataTable)Session["SubAccountData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvsubAccount.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToAccountGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindAccountGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvsubAccount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubAccountData"] != null)
                {
                    dtGridData = (DataTable)Session["SubAccountData"];
                }
                gvsubAccount.EditIndex = -1;

                BindAccountGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvsubAccount_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvsubAccount.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindAccountGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["SubAccountData"] = dtData;


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvsubAccount.DataSource = dt_tmp;
                gvsubAccount.DataBind();

                GridViewRow gvr = gvsubAccount.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlAccCode = tmpgvr.FindControl("ddlAccName") as DropDownList;
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAccCode);
                if (gvsubAccount.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccCode.SelectedValue = gvsubAccount.DataKeys[gvsubAccount.EditIndex].Values["BA_ACC_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }




        private DataRow AssignToSegmentGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_GSegment = gvr.FindControl("ddlGSegment") as DropDownList;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["BS_TMPL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_GSegment;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            string strMessage = "Segment Name";
            //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            string strCondition = "BS_ACC_ID='" + ddl_GSegment.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            drList["BS_ACC_NAME"] = ddl_GSegment.SelectedItem.Text;
            drList["BS_ACC_ID"] = ddl_GSegment.SelectedValue.ToString();
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        protected void gvSubSegment_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvSubSegment.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["SubSegmentData"] != null)
                {
                    dtGridData = (DataTable)Session["SubSegmentData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToSegmentGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {

                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvSubSegment.EditIndex = -1;
                    BindSegmentGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvSubSegment_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubSegmentData"] != null)
                {
                    dtGridData = (DataTable)Session["SubSegmentData"];
                }

                DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TemplateData4TmplId(dtGridData.Rows[e.RowIndex]["BS_TMPL_ID"].ToString())).Tables[0];
                if (dt_data.Rows.Count > 0)
                {
                    ErrorCollection.Add("Child RecordFound", "Child Record Found");
                    return;
                }

                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindSegmentGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvSubSegment_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubSegmentData"] != null)
                {
                    dtGridData = (DataTable)Session["SubSegmentData"];
                }
                gvSubSegment.EditIndex = e.NewEditIndex;
                BindSegmentGrid(dtGridData);
                //GridViewRow gvr = gvSubSegment.Rows[e.NewEditIndex];
                //FillFooterSegmentGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvSubSegment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvSubSegment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["SubSegmentData"] != null)
                {
                    dtGridData = (DataTable)Session["SubSegmentData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvSubSegment.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToSegmentGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindSegmentGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvSubSegment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SubSegmentData"] != null)
                {
                    dtGridData = (DataTable)Session["SubSegmentData"];
                }
                gvSubSegment.EditIndex = -1;

                BindSegmentGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvSubSegment_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvSubSegment.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindSegmentGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["SubSegmentData"] = dtData;


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvSubSegment.DataSource = dt_tmp;
                gvSubSegment.DataBind();

                GridViewRow gvr = gvSubSegment.FooterRow;
                FillFooterSegmentGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterSegmentGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddl_GSegment = tmpgvr.FindControl("ddlGSegment") as DropDownList;
                FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddl_GSegment, "SEG_ID-0000000101");
                if (gvSubSegment.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_GSegment.SelectedValue = gvSubSegment.DataKeys[gvSubSegment.EditIndex].Values["BA_ACC_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void rbl_subType_SelectedIndexChanged(object sender, EventArgs e)
        {
            showSubGroupAcct();

        }
        private void showSubGroupAcct()
        {
            div_SubGroup.Visible = false;
            div_subAccount.Visible = false;
            if (rbl_subType.SelectedValue.ToString() == "GROUP")
            {
                div_SubGroup.Visible = true;
                BindGrid((DataTable)Session["SubGroupData"]);
            }
            else if (rbl_subType.SelectedValue.ToString() == "ACCOUNT")
            {
                div_subAccount.Visible = true;
                BindAccountGrid((DataTable)Session["SubAccountData"]);
            }
        }

        protected void btnGFAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtGpopFormula.Text.ToString().Contains("#" + ddlGFgroupName.SelectedValue.ToString() + "#"))
                {
                    ErrorCollection.Add("InvalidGroup", "Selected Group already available in the formula");
                    return;
                }

                if (ddlGFgroupName.SelectedValue.ToString().Length > 0)
                {
                    txtGpopFormula.Text = txtGpopFormula.Text + "#" + ddlGFgroupName.SelectedValue.ToString() + "#";
                }
                if (ddlGFOperation.SelectedValue.ToString().Length > 0)
                {
                    txtGpopFormula.Text = txtGpopFormula.Text + ddlGFOperation.SelectedValue.ToString();
                }
                ddlGFOperation.SelectedIndex = 0;
                ddlGFgroupName.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {
                mpeFormula.Show();
                Reload_BS_Structure();
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void chkGFormula_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                if (gvr.RowType == DataControlRowType.Footer)
                {
                    hf_Temp_RefNo.Value = DataControlRowType.Footer.ToString();
                }
                else
                {
                    hf_Temp_RefNo.Value = gvr.RowIndex.ToString();
                }
                CheckBox chk_tmp = (CheckBox)sender;
                if (chk_tmp.Checked)
                {
                    FIN.BLL.GL.FinancialTemplate_Segment_BLL.fn_getTemplateGroupNumber(ref ddlGFgroupName, txtTempName.Text);

                    if (gvr.RowType != DataControlRowType.Footer)
                    {
                        DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TemplateData4TmplId(gvSubGroup.DataKeys[gvr.RowIndex].Values["BS_TMPL_ID"].ToString())).Tables[0];
                        if (dt_data.Rows.Count > 0)
                        {
                            chk_tmp.Checked = false;
                            ErrorCollection.Add("Selected Group Contain Value", "Invalid Formula Field Selected");
                            return;
                        }

                    }

                    TextBox txt_tmp = (TextBox)gvr.FindControl("txtGFormula");
                    txtGpopFormula.Text = txt_tmp.Text;
                    div_FormulaPOPUP.Visible = true;
                    
                    mpeFormula.Show();
                }
                else
                {
                    TextBox txt_tmp = (TextBox)gvr.FindControl("txtGFormula");
                    txt_tmp.Text = "";
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {

                Reload_BS_Structure();
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }


        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtGpopFormula.Text.ToString().Length > 0)
                {
                    string str_Fromula = txtGpopFormula.Text.ToString().Replace("#", "");
                    DataTable dt = DBMethod.ExecuteQuery("SELECT " + str_Fromula + " FROM DUAL ").Tables[0];

                }

                if (txtGpopFormula.Text.ToString().Length > 0)
                {
                    if (hf_Temp_RefNo.Value == DataControlRowType.Footer.ToString())
                    {
                        TextBox txt_tmp = (TextBox)gvSubGroup.FooterRow.FindControl("txtGFormula");
                        txt_tmp.Text = txtGpopFormula.Text;
                        txtGpopFormula.Text = "";
                    }
                    else
                    {
                        TextBox txt_tmp = (TextBox)gvSubGroup.Rows[int.Parse(hf_Temp_RefNo.Value)].FindControl("txtGFormula");
                        txt_tmp.Text = txtGpopFormula.Text;
                        txtGpopFormula.Text = "";
                    }
                }
                div_FormulaPOPUP.Visible = false;
            }
            catch (Exception ex_F)
            {
                mpeFormula.Show();
                ErrorCollection.Add("iNVALID fORMAULA", "Given Formula is invalid");
                return;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtFormula.Text = "";

        }

        protected void imgEditGFormula_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            CheckBox chk_tmp = (CheckBox)gvr.FindControl("chkGFormula");
            chkGFormula_CheckedChanged(chk_tmp, new EventArgs());

        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            Session["ALL_ACC"] = null;
            Session["SELECTED_ACC"] = null;
            DataTable dt_Data = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount("  ", "   ")).Tables[0];
            dt_Data.Rows.Clear();
            LoadSelectedAccount(dt_Data);
            div_AcctPop.Visible = true;
            mpeAcctCode.Show();
        }

        private void LoadAccount(DataTable dt)
        {
            Session["ALL_ACC"] = dt;
            gv_All_Acct.DataSource = dt;
            gv_All_Acct.DataBind();
        }

        private void LoadSelectedAccount(DataTable dt_sel)
        {
            Session["SELECTED_ACC"] = dt_sel;
            gv_Sel_Acct.DataSource = dt_sel;
            gv_Sel_Acct.DataBind();
        }
        protected void btnAcctSearch_Click(object sender, EventArgs e)
        {
            DataTable dt_Data = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount(ddlFromAccNumber.SelectedValue, ddlToAccNumber.SelectedValue)).Tables[0];

            LoadAccount(dt_Data);
            mpeAcctCode.Show();

        }


        protected void gv_All_Acct_RowDataBound(object sender, GridViewRowEventArgs e)
        {/*
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (gv_All_Acct.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "Y")
                // {
                //     e.Row.Visible = false;
                // }
                if (txtSearchAcct.Text.ToString().Length > 0)
                 {
                     if (!gv_All_Acct.DataKeys[e.Row.RowIndex].Values["CODE_NAME"].ToString().ToUpper().Contains(txtSearchAcct.Text.ToString().ToUpper()))
                     {
                         e.Row.Visible = false;
                     }
                 }
            }*/
        }

        protected void gv_Sel_Acct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //  if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    if (gv_Sel_Acct.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "N")
            //   {
            //      e.Row.Visible = false;
            //   }
            // }
        }

        protected void img_OneAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ALL_ACC"];
            DataTable dt_sel = (DataTable)Session["SELECTED_ACC"];
            for (int rloop = 0; rloop < gv_All_Acct.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Acct.Rows[rloop].FindControl("chkAll_Acct");
                if (chk_tmp.Checked)
                {
                    DataRow[] dr = dt_Acct.Select("CODE_ID='" + gv_All_Acct.DataKeys[rloop]["CODE_ID"].ToString() + "'");
                    DataRow dr_sel = dt_sel.NewRow();
                    dr_sel["CODE_ID"] = dr[0]["CODE_ID"];
                    dr_sel["ACCT_CODE"] = dr[0]["ACCT_CODE"];
                    dr_sel["CODE_NAME"] = dr[0]["CODE_NAME"];
                    dt_sel.Rows.Add(dr_sel);

                }

            }
            dt_Acct.Rows.Clear();
            LoadAccount(dt_Acct);
            LoadSelectedAccount(dt_sel);
            mpeAcctCode.Show();
        }

        protected void img_oneRemove_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];

            for (int rloop = 0; rloop < gv_Sel_Acct.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_Sel_Acct.Rows[rloop].FindControl("chkSel_Acct");
                if (chk_tmp.Checked)
                {
                    gv_Sel_Acct.Rows[rloop].Visible = false;
                    //hf_Sel_Acct.Value = gv_Sel_Acct.DataKeys[rloop]["CODE_ID"].ToString();
                    //DataRow[] dr = dt_Acct.Select("CODE_ID='" + hf_Sel_Acct.Value + "'");
                    //dr[0]["SELECTED_REC"] = "N";
                }
            }
            // LoadAccount(dt_Acct);
            mpeAcctCode.Show();
        }

        protected void btnAcctOK_Click(object sender, EventArgs e)
        {
            dtGridData = (DataTable)Session["SubAccountData"];
            for (int iLoop = 0; iLoop < gv_Sel_Acct.Rows.Count; iLoop++)
            {

                if (gv_Sel_Acct.Rows[iLoop].Visible)
                {
                    DataRow drList;

                    string strCondition = "BS_ACC_ID='" + gv_Sel_Acct.DataKeys[iLoop].Values["CODE_ID"].ToString() + "'";

                    ErrorCollection = UserUtility_BLL.DataDuplication(dtGridData, strCondition, "");
                    if (ErrorCollection.Count == 0)
                    {
                        drList = dtGridData.NewRow();
                        drList["BS_TMPL_ID"] = "0";
                        drList["BS_GROUP_NAME"] = gv_Sel_Acct.DataKeys[iLoop].Values["CODE_NAME"].ToString();
                        drList["BS_ACC_ID"] = gv_Sel_Acct.DataKeys[iLoop].Values["CODE_ID"].ToString();
                        drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                        dtGridData.Rows.Add(drList);

                    }
                }

            }
            BindAccountGrid(dtGridData);
            div_AcctPop.Visible = false;
        }

        protected void btnSegmentSel_Click(object sender, EventArgs e)
        {
            Session["ALL_SEG"] = null;
            Session["SELECTED_SEG"] = null;
            DataTable dt_Data = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.getSegmentvalue4Segment("SEG_ID-0000000101", "  ", "   ")).Tables[0];
            dt_Data.Rows.Clear();
            LoadSelectedSegment(dt_Data);
            div_Segment_POPUP.Visible = true;
            mpeSegment.Show();
        }

        private void LoadSegment(DataTable dt)
        {
            Session["ALL_SEG"] = dt;
            gv_All_Segment.DataSource = dt;
            gv_All_Segment.DataBind();
        }

        private void LoadSelectedSegment(DataTable dt_sel)
        {
            Session["SELECTED_SEG"] = dt_sel;
            gv_Sel_Segment.DataSource = dt_sel;
            gv_Sel_Segment.DataBind();
        }

        protected void btnSegmentSearch_Click(object sender, EventArgs e)
        {
            DataTable dt_Data = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.getSegmentvalue4Segment("SEG_ID-0000000101", ddlFromSegment.SelectedItem.Text, ddlToSegment.SelectedItem.Text)).Tables[0];

            LoadSegment(dt_Data);
            mpeSegment.Show();
        }


        protected void img_segment_oneAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ALL_SEG"];
            DataTable dt_sel = (DataTable)Session["SELECTED_SEG"];
            for (int rloop = 0; rloop < gv_All_Segment.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Segment.Rows[rloop].FindControl("chkAll_Acct");
                if (chk_tmp.Checked)
                {
                    DataRow[] dr = dt_Acct.Select("SEGMENT_VALUE_ID='" + gv_All_Segment.DataKeys[rloop]["SEGMENT_VALUE_ID"].ToString() + "'");
                    DataRow dr_sel = dt_sel.NewRow();
                    dr_sel["SEGMENT_VALUE_ID"] = dr[0]["SEGMENT_VALUE_ID"];
                    dr_sel["SEGMENT_VALUE"] = dr[0]["SEGMENT_VALUE"];
                    dr_sel["SEGMENT_VALUE_OL"] = dr[0]["SEGMENT_VALUE_OL"];
                    dt_sel.Rows.Add(dr_sel);

                }

            }
            dt_Acct.Rows.Clear();
            LoadSegment(dt_Acct);
            LoadSelectedSegment(dt_sel);
            mpeSegment.Show();
        }

        protected void img_Segment_Remove_Click(object sender, ImageClickEventArgs e)
        {


            for (int rloop = 0; rloop < gv_Sel_Segment.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_Sel_Segment.Rows[rloop].FindControl("chkSel_Acct");
                if (chk_tmp.Checked)
                {
                    gv_Sel_Segment.Rows[rloop].Visible = false;

                }
            }

            mpeSegment.Show();
        }


        protected void btnSegmentOk_Click(object sender, EventArgs e)
        {
            dtGridData = (DataTable)Session["SubSegmentData"];
            for (int iLoop = 0; iLoop < gv_Sel_Segment.Rows.Count; iLoop++)
            {

                if (gv_Sel_Segment.Rows[iLoop].Visible)
                {
                    DataRow drList;

                    string strCondition = "BS_ACC_ID='" + gv_Sel_Segment.DataKeys[iLoop].Values["SEGMENT_VALUE_ID"].ToString() + "'";

                    ErrorCollection = UserUtility_BLL.DataDuplication(dtGridData, strCondition, "");
                    if (ErrorCollection.Count == 0)
                    {
                        drList = dtGridData.NewRow();
                        drList["BS_TMPL_ID"] = "0";
                        drList["BS_ACC_NAME"] = gv_Sel_Segment.DataKeys[iLoop].Values["SEGMENT_VALUE"].ToString();
                        drList["BS_ACC_ID"] = gv_Sel_Segment.DataKeys[iLoop].Values["SEGMENT_VALUE_ID"].ToString();
                        drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                        dtGridData.Rows.Add(drList);

                    }
                }

            }
            BindSegmentGrid(dtGridData);
            div_Segment_POPUP.Visible = false;

        }

        protected void chk_Acct_Sel_ALL_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gv_All_Acct.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Acct.Rows[rloop].FindControl("chkAll_Acct");
                chk_tmp.Checked = chk_SAA.Checked;
            }
            mpeAcctCode.Show();
        }

        protected void chk_Seg_Sel_ALL_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gv_All_Segment.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Segment.Rows[rloop].FindControl("chkAll_Acct");
                chk_tmp.Checked = chk_SAA.Checked;
            }
            mpeSegment.Show();
        }

    }
}