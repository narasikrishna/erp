﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.IO;

namespace FIN.Client.GL
{
    public partial class MaisonetteImportToGL : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        //private static readonly char[] SpecialChars = "'!^".ToCharArray();

        string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
        string connectionStringOracle = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringOracle"].ToString();
        bool postedMsg = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                Session["dtData"] = null;
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                //if (dtData.Rows.Count > 0)
                //{
                //    if (dtData.Rows[0]["Debit"].ToString().Length > 0)
                //    {
                //        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Debit", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Debit"))));
                //    }
                //    if (dtData.Rows[0]["Credit"].ToString().Length > 0)
                //    {
                //        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Credit", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Credit"))));
                //    }
                //    dtData.AcceptChanges();
                //}

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["Migrated"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindFilteredGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;


                //if (dtData.Rows.Count > 0)
                //{
                //    if (dtData.Rows[0]["Debit"].ToString().Length > 0)
                //    {
                //        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Debit", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Debit"))));
                //    }
                //    if (dtData.Rows[0]["Credit"].ToString().Length > 0)
                //    {
                //        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Credit", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Credit"))));
                //    }
                //    dtData.AcceptChanges();
                //}

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["Migrated"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLBFG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtTransactionID = gvr.FindControl("txtTransactionID") as TextBox;
                TextBox txtTransactionDate = gvr.FindControl("txtTransactionDate") as TextBox;
                TextBox txtReferenceNumber = gvr.FindControl("txtReferenceNumber") as TextBox;
                TextBox txtPersonNumber = gvr.FindControl("txtPersonNumber") as TextBox;
                TextBox txtPersonName = gvr.FindControl("txtPersonName") as TextBox;

                if (txtTransactionID.Text.Length > 0)
                {
                    txtTransactionDate.Text = string.Empty;
                    txtReferenceNumber.Text = string.Empty;
                    txtPersonNumber.Text = string.Empty;
                    txtPersonName.Text = string.Empty;
                }
                else if (txtTransactionDate.Text.Length > 0)
                {
                    txtTransactionID.Text = string.Empty;
                    txtReferenceNumber.Text = string.Empty;
                    txtPersonNumber.Text = string.Empty;
                    txtPersonName.Text = string.Empty;
                }
                else if (txtReferenceNumber.Text.Length > 0)
                {
                    txtTransactionID.Text = string.Empty;
                    txtTransactionDate.Text = string.Empty;
                    txtPersonNumber.Text = string.Empty;
                    txtPersonName.Text = string.Empty;
                }
                else if (txtPersonNumber.Text.Length > 0)
                {
                    txtTransactionID.Text = string.Empty;
                    txtTransactionDate.Text = string.Empty;
                    txtReferenceNumber.Text = string.Empty;
                    txtPersonName.Text = string.Empty;
                }
                else if (txtPersonName.Text.Length > 0)
                {
                    txtTransactionID.Text = string.Empty;
                    txtTransactionDate.Text = string.Empty;
                    txtReferenceNumber.Text = string.Empty;
                    txtPersonNumber.Text = string.Empty;
                }
                DataTable dt = new DataTable();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt = (DataTable)Session[FINSessionConstants.GridData];
                    if (dt.Rows.Count > 0)
                    {
                        var var_List = dt.AsEnumerable().Where(r => r["TransactionID"].ToString() != string.Empty);

                        if (txtTransactionID.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["TransactionID"].ToString().ToUpper().StartsWith(txtTransactionID.Text.ToUpper()));
                        }
                        else if (txtTransactionDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["TransactionDate"].ToString().ToUpper().StartsWith(txtTransactionDate.Text.ToUpper()));
                        }
                        else if (txtReferenceNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["ReferenceNumber"].ToString().ToUpper().StartsWith(txtReferenceNumber.Text.ToUpper()));
                        }
                        else if (txtPersonNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["PersonNumber"].ToString().ToUpper().StartsWith(txtPersonNumber.Text.ToUpper()));
                        }
                        else if (txtPersonName.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["PersonName"].ToString().StartsWith(txtPersonName.Text));
                        }
                        if (var_List.Any())
                        {
                            dt = System.Data.DataTableExtensions.CopyToDataTable(var_List);
                            BindFilteredGrid(dt);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLPOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion



        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        FINSP.GetSP_GL_Posting(gvData.DataKeys[iLoop].Values["TransactionID"].ToString(), "SSM_037");
                        postedMsg = true;
                    }
                }

                if (postedMsg)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

                DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.PaymentGL_DAL.getTransactionData(txtFromDate.Text.ToString())).Tables[0];
                BindGrid(dtData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLPOST", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {


            string maxIdGL = "";
            try
            {

                if (txtFromDate.Text.ToString().Trim().Length == 0)
                {
                    ErrorCollection.Add("InvalidDate", "Please Select the Date");
                    return;
                }
                //AccountReceivable
                //Get the MAX Id
                using (OracleConnection connMax = new OracleConnection())
                {
                    connMax.ConnectionString = connectionStringOracle;
                    connMax.Open();
                    OracleCommand cmdMax = connMax.CreateCommand();
                    string sql = "";

                    sql = "Select max(g.TransactionID) maxValueGL from vwTransactions g where g.TransactionDate <=to_date('" + txtFromDate.Text + "','dd/MM/yyyy')";
                    DataTable dtDataMax = new DataTable();
                    OracleDataAdapter oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[0]["maxValueGL"].ToString().Length > 0)
                        {
                            maxIdGL = dtDataMax.Rows[0]["maxValueGL"].ToString();
                        }
                    }

                    connMax.Close();
                }

                //Create Connection - AccountReceivable  -- SQL
                SqlConnection con = new SqlConnection(connectionStringSQL);

                string sqlQuery = " select VT.TransactionID,convert(nvarchar,VT.TransactionDate,103) as TransactionDate, VT.BatchNumber,VT.ReferenceNumber, VT.Description_En,";
                sqlQuery += " VT.AccountId, VT.AccountNum,VT.AccountNameEn, VT.Debit,VT.Credit,  ";
                sqlQuery += " VT.ComponentNumber,VT.ComponentName_En,VT.PersonType,VT.PersonNumber,VT.PersonName,TD.CurrencyId";
                sqlQuery += " from vwTransactions VT,TransactionDetail TD where TD.TransactionID=VT.TransactionID and VT.TransactionDate > CONVERT(datetime,'01/07/2015',103)";
                sqlQuery += " AND  VT.TransactionDate <= CONVERT(datetime,'" + txtFromDate.Text + "',103)";
                sqlQuery += " and VT.TransactionID < (select max(g.TransactionID) maxValGL from vwTransactions g where g.TransactionDate <= CONVERT(datetime,'" + txtFromDate.Text + "',103))";
                if (maxIdGL.ToString().Length > 0)
                {
                    sqlQuery += " and VT.TransactionID > '" + maxIdGL + "'";
                }
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                DataTable dtData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dtData);

                //Create Connection -- Oracle
                //Account Receivables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into vwTransactions (TransactionID, TransactionDate, BatchNumber,ReferenceNumber, Description_En,AccountId, ";
                            sql += " AccountNum,AccountNameEn, Debit,Credit,ComponentNumber,ComponentName_En,PersonType,PersonNumber,PersonName,CurrencyId) Values ('";
                            sql += dtData.Rows[iLoop]["TransactionID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["TransactionDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["BatchNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["ReferenceNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Description_En"].ToString().Replace("'", "''") + "','";
                            sql += dtData.Rows[iLoop]["AccountId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["AccountNum"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["AccountNameEn"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Debit"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Credit"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["ComponentNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["ComponentName_En"].ToString().Replace("'", "''") + "','";
                            sql += dtData.Rows[iLoop]["PersonType"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PersonNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PersonName"].ToString().Replace("'", "''") + "','";
                            sql += dtData.Rows[iLoop]["CurrencyId"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                DataTable dtDataView = DBMethod.ExecuteQuery(FIN.DAL.GL.PaymentGL_DAL.getTransactionData(txtFromDate.Text.ToString())).Tables[0];
                BindGrid(dtDataView);
                Session["dtData"] = dtDataView;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATAIMPORT);

                //Close Connection  
                con.Close();
                da.Dispose();
                //close

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLImport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gvData.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gvData.Rows[rloop].FindControl("chkSelect");
                chk_tmp.Checked = chk_SAA.Checked;
            }
        }

        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["dtData"] != null)
                {
                    //gvData.HeaderRow.FindControl["txtTransactionID"]
                    gvData.PageIndex = e.NewPageIndex;
                    gvData.DataSource = Session["dtData"];
                    gvData.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MaisonetteImportToGLPgIndex", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



    }
}