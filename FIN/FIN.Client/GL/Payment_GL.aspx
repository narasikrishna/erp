﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="Payment_GL.aspx.cs" Inherits="FIN.Client.GL.Payment_GL" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <table>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 130px" id="lblPRNumber">
                                Payment Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 380px">
                                <asp:TextBox ID="txtPayNo" CssClass="validate[] txtBox" MaxLength="50" runat="server"
                                    Enabled="False" TabIndex="1"></asp:TextBox>
                            </div>
                            <div class="lblBox LNOrient" style="width: 120px" id="lblDCNumber">
                                Payment Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaydate" CssClass="validate[required] RequiredField txtBox" runat="server"
                                    TabIndex="2"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtPaydate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtPaydate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 130px" id="Div2">
                                Bank Account
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 380px">
                                <asp:DropDownList ID="ddlBankAccount" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlBankAccount_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 120px" id="lblItemDescription">
                                Amount
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtAmt" CssClass="validate[required] txtBox_N" runat="server" Enabled="False"
                                    MaxLength="4" TabIndex="6"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                    FilterType="Numbers,Custom" TargetControlID="txtAmt" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 130px" id="Div3">
                                Cheque Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlChkNo" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="3">
                                </asp:DropDownList>
                            </div>
                            <div class="lblBox LNOrient" style="width: 130px" id="Div4">
                                Cheque Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                    TabIndex="25"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender81" runat="server" Format="dd/MM/yyyy"
                                    TargetControlID="txtChequeDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtChequeDate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer" style="display: none;">
                            <div class="lblBox LNOrient" style="width: 130px" id="lblLineNumber">
                                Bank
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlBank" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged1">
                                </asp:DropDownList>
                            </div>
                            <div class="lblBox LNOrient" style="width: 130px" id="lblDCDate">
                                Branch
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlBranch" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer" style="display: none;">
                            <div class="lblBox LNOrient" style="width: 130px" id="lblOrderNumber">
                                Account Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 250px">
                                <asp:DropDownList ID="ddlAcctNo" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddlAcctNo_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <%--<div class="lblBox LNOrient" style="  width: 130px" id="lblItemDescription">
                            Amount
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 150px">
                            <asp:TextBox ID="txtAmt" CssClass="validate[required] txtBox_N" runat="server" Enabled="False"
                                MaxLength="13" TabIndex="6"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmt" />
                        </div>--%>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 130px" id="lblOrderLineNumber">
                                Global Segment
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 650px">
                                <asp:DropDownList ID="ddlCostCenter" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="7" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 130px" id="Div1">
                                Remarks
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 650px">
                                <asp:TextBox ID="txtRemarks" CssClass="validate[] txtBox" runat="server" MaxLength="200"
                                    Enabled="true" TabIndex="8"></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrintReport" runat="server" ImageUrl="~/Images/Print.png"
                                    Style="border: 0px" OnClick="btnPrintReport_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgbtnCheckPrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    Style="border: 0px" OnClick="imgbtnCheckPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Style="border: 0px"
                                    OnClick="imgBtnPost_Click" OnClientClick="fn_PostVisible('FINContent_imgBtnPost')" />
                                <asp:HiddenField runat="server" ID="hf_posted" />
                                <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox LNOrient"
                                    Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnStopPayment" runat="server" ImageUrl="~/Images/stopPay.png"
                                    OnClick="imgStopPayment_Click" Style="border: 0px" />
                                <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox LNOrient"
                                    Font-Size="18px" Font-Bold="true" Style="color: Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnStopPayJV" runat="server" ImageUrl="~/Images/stopPayJV.png"
                                    Style="border: 0px" OnClick="imgBtnStopPayJV_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <div  class="LNOrient">
            <asp:Panel runat="server" ID="pnlgridview">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="100%" DataKeyNames="PAY_DTL_ID,CHECK_DTL_ID,ACCT_CODE_ID,DELETED,AMOUNT,JE_SEGMENT_ID_1,JE_SEGMENT_ID_2,JE_SEGMENT_ID_3,JE_SEGMENT_ID_4,JE_SEGMENT_ID_5,JE_SEGMENT_ID_6"
                    OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                    OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                    OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Add / Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="36" runat="server" AlternateText="Edit"
                                    CausesValidation="false" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="37" runat="server" AlternateText="Delete"
                                    CausesValidation="false" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="22" runat="server" AlternateText="Update"
                                    CommandName="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="23" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="21" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cheque No" Visible="false">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlChqNo" runat="server" CssClass="ddlStype" Width="180px"
                                    TabIndex="24">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlChqNo" runat="server" CssClass="ddlStype" Width="180px"
                                    TabIndex="9">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblChqNo" Width="180px" runat="server" Text='<%# Eval("CHECK_NUMBER") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cheque Date" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="dtpCheqDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                    TabIndex="25" Text='<%#  Eval("CHEQUE_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpCheqDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpCheqDate" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="dtpCheqDate" TabIndex="10" runat="server" CssClass="EntryFont RequiredField txtBox"
                                    Width="80px"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpCheqDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpCheqDate" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblChqDate" runat="server" Text='<%# Eval("CHEQUE_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account Code">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAccCode_SelectedIndexChanged"
                                    TabIndex="26">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAccCode_SelectedIndexChanged"
                                    TabIndex="11">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAcctCode" Width="180px" runat="server" Text='<%# Eval("ACCT_CODE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmount" MaxLength="11" runat="server" Text='<%# Eval("AMOUNT") %>'
                                    TabIndex="27" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=",." TargetControlID="txtAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmount" TabIndex="12" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=",." TargetControlID="txtAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Beneficiary Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtBenfName" Width="180px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="28" Text='<%# Eval("BENEFICIARY_NAME") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtBenfName" Width="180px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="13"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBenfName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("BENEFICIARY_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reason For Payment">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtReasonfrpayment" Width="180px" MaxLength="200" runat="server"
                                    TabIndex="29" CssClass="txtBox" Text='<%# Eval("REASON_FOR_PAYMENT") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtReasonfrpayment" Width="180px" MaxLength="200" runat="server"
                                    CssClass="txtBox" TabIndex="14"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblReasonfrpayment" CssClass="adminFormFieldHeading" runat="server"
                                    Text='<%# Eval("REASON_FOR_PAYMENT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" Visible="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    OnClick="imgBtnChequePrint_Click" Style="border: 0px" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    OnClick="imgBtnChequePrint_Click" Style="border: 0px" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    Enabled="false" Style="border: 0px" />
                            </FooterTemplate>
                            <FooterStyle VerticalAlign="Top" />
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="btnPost" runat="server" CssClass="btn" Text="Post To GL" CommandName="btnPop"
                                    Enabled="false" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnPost" runat="server" CssClass="btn" Text="Post To GL" CommandName="btnPop" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnPost" runat="server" CssClass="btn" Text="Post To GL" CommandName="btnPop"
                                    TabIndex="16" />
                            </FooterTemplate>
                            <FooterStyle VerticalAlign="Top" />
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 1">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment1" runat="server" TabIndex="30" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="15">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment1" Width="180px" runat="server" Text='<%# Eval("SEGMENT_1_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 2">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment2" runat="server" TabIndex="31" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="16">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment2" Width="180px" runat="server" Text='<%# Eval("SEGMENT_2_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 3">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment3" runat="server" TabIndex="32" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="17">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment3" Width="180px" runat="server" Text='<%# Eval("SEGMENT_3_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 4">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment4" runat="server" TabIndex="33" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="18">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment4" Width="180px" runat="server" Text='<%# Eval("SEGMENT_4_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 5">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment5" runat="server" TabIndex="34" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="19">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment5" Width="180px" runat="server" Text='<%# Eval("SEGMENT_5_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Segment 6">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSegment6" runat="server" TabIndex="35" CssClass="ddlStype"
                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged"
                                    TabIndex="20">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSegment6" Width="180px" runat="server" Text='<%# Eval("SEGMENT_6_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="22" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="23" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="24" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="25" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
