﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class AccountingCalendar : PageBase
    {
        GL_ACCT_CALENDAR_HDR gL_ACCT_CALENDAR_HDR = new GL_ACCT_CALENDAR_HDR();
        GL_ACCT_CALENDAR_DTL gL_ACCT_CALENDAR_DTL = new GL_ACCT_CALENDAR_DTL();
        GL_ACCT_PERIOD_DTL gL_ACCT_PERIOD_DTL = new GL_ACCT_PERIOD_DTL();
        DataTable dtGridData = new DataTable();
        DataTable dtperiod = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean bol_err;
        Boolean saveBool = false;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    hf_AcId.Value = "0";
                    txtCalendarName.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Session["Deleted"] = null;

                Startup();
                btnSave.Visible = false;

                EntityData = null;
                hf_AcId.Value = Master.StrRecordId;
                BindGrid();
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    btnSave.Visible = true;

                    using (IRepository<GL_ACCT_CALENDAR_HDR> userCtx = new DataRepository<GL_ACCT_CALENDAR_HDR>())
                    {
                        gL_ACCT_CALENDAR_HDR = userCtx.Find(r =>
                            (r.CAL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_ACCT_CALENDAR_HDR;

                    txtCalendarName.Text = gL_ACCT_CALENDAR_HDR.CAL_DESC;
                    if (gL_ACCT_CALENDAR_HDR.CAL_DESC_OL != null || gL_ACCT_CALENDAR_HDR.CAL_DESC_OL != string.Empty)
                    {
                        txtCalendarNameAR.Text = gL_ACCT_CALENDAR_HDR.CAL_DESC_OL;
                    }

                    txtAdjPeriod.Text = gL_ACCT_CALENDAR_HDR.CAL_ADJ_PERIOD.ToString();
                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT.ToString());
                    if (gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT.ToString());
                    }
                    chkActive.Checked = true;
                    if (gL_ACCT_CALENDAR_HDR.ENABLED_FLAG == FINAppConstants.N)
                    {
                        chkActive.Checked = false;
                    }
                    hf_AcId.Value = Master.StrRecordId;
                    BindGrid();
                }




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_ACCT_CALENDAR_HDR = (GL_ACCT_CALENDAR_HDR)EntityData;
                }



                gL_ACCT_CALENDAR_HDR.CAL_DESC = txtCalendarName.Text;
                if (txtCalendarNameAR.Text != null)
                {
                    gL_ACCT_CALENDAR_HDR.CAL_DESC_OL = txtCalendarNameAR.Text;
                }

                gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text.ToString().Length > 0)
                {
                    gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT = null;
                }
                gL_ACCT_CALENDAR_HDR.CAL_ADJ_PERIOD = decimal.Parse(txtAdjPeriod.Text.ToString());



                if (chkActive.Checked == true)
                {
                    gL_ACCT_CALENDAR_HDR.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_ACCT_CALENDAR_HDR.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Session["Deleted"] != null)
                {


                }
                else if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_ACCT_CALENDAR_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_ACCT_CALENDAR_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    gL_ACCT_CALENDAR_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CALENDAR_HDR_SEQ);
                    gL_ACCT_CALENDAR_HDR.CAL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCAL_M.ToString(), false, true);
                    gL_ACCT_CALENDAR_HDR.CREATED_BY = this.LoggedUserName;
                    gL_ACCT_CALENDAR_HDR.CREATED_DATE = DateTime.Today;


                }

                gL_ACCT_CALENDAR_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_ACCT_CALENDAR_HDR.CAL_ID);
                gL_ACCT_CALENDAR_HDR.CAL_ORG_ID = VMVServices.Web.Utils.OrganizationID;



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion


        private void ValidateGridData()
        {
            try
            {
                ErrorCollection.Clear();
                string countValue = string.Empty;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (gvData.Rows.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                        {
                            TextBox ntxtAcctYear = gvData.Rows[iLoop].FindControl("ntxtAcctYear") as TextBox;
                            TextBox dtpBeginDate = gvData.Rows[iLoop].FindControl("dtpBeginDate") as TextBox;
                            TextBox dtpEnddate = gvData.Rows[iLoop].FindControl("dtpEnddate") as TextBox;
                            Button btnGeneratePeriod = gvData.Rows[iLoop].FindControl("btnGeneratePeriod") as Button;

                            countValue = DBMethod.GetStringValue(FIN.DAL.GL.AccountingCalendar_DAL.IsPeriodFound(gvData.DataKeys[iLoop].Values["CAL_DTL_ID"].ToString()));

                            if (countValue != string.Empty && int.Parse(countValue.ToString()) > 0)
                            {
                                ErrorCollection.Remove("caleditdtls1234");
                                ErrorCollection.Add("caleditdtls1234", "Cannot be update the record,child records found");
                            }
                            else
                            {
                                if (countValue != string.Empty && countValue != "0")
                                {
                                    ProReturn = FINSP.GetSPFOR_ERR_ACC_CAL_DTL_EDIT(gvData.DataKeys[iLoop].Values["CAL_DTL_ID"].ToString());

                                    if (ProReturn != string.Empty)
                                    {
                                        if (ProReturn != "0")
                                        {
                                            ErrorCollection.Add("caleditdtls12", ProReturn);
                                            if (ErrorCollection.Count > 0)
                                            {
                                                bol_err = false;
                                                return;
                                            }
                                            else
                                            {
                                                ErrorCollection.Clear();
                                            }
                                        }
                                        else
                                        {
                                            ErrorCollection.Clear();
                                        }
                                    }
                                }

                            }

                            if (ErrorCollection.Count == 0)
                            {
                                Session["Deleted"] = "1";
                                if (ntxtAcctYear != null)
                                {
                                    ntxtAcctYear.Enabled = true;
                                }
                                if (dtpBeginDate != null)
                                {
                                    dtpBeginDate.Enabled = true;
                                }
                                if (dtpEnddate != null)
                                {
                                    dtpEnddate.Enabled = true;
                                }
                                if (btnGeneratePeriod != null)
                                {
                                    btnGeneratePeriod.Enabled = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //    }
            //}
        }

        private void BindGrid()
        {
            DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.getAcctcalendardtl(hf_AcId.Value.ToString())).Tables[0];

            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();

            ValidateGridData();
        }

        private void BindGrid_PERIOD(DataTable dtperiodData)
        {
            bol_rowVisiable = false;
            Session["periodgd"] = dtperiodData;
            //DataTable dt_tmp1 = dtperiodData.Copy();
            //if (dt_tmp1.Rows.Count == 0)
            //{
            //    DataRow dr = dt_tmp1.NewRow();
            //    dr[0] = "0";

            //    dt_tmp1.Rows.Add(dr);
            //    bol_rowVisiable = true;
            //}
            gvperiods.DataSource = dtperiodData;
            gvperiods.DataBind();


        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    //drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    //    return;
                    //}
                    //else
                    //{
                    //    dtGridData.Rows.Add(drList);
                    //    BindGrid(dtGridData);
                    //}
                    GeneratePeriod(gvr);
                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Acct_cal_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Acct_Cal_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                GeneratePeriod(gvr);
                gvData.EditIndex = -1;
                BindGrid();

                //drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                //if (ErrorCollection.Count > 0)
                //{
                //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                //    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                //    return;
                //}
                ////else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                ////{
                ////    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                ////    return;
                ////}
                //else
                //{
                //    gvData.EditIndex = -1;
                //    BindGrid(dtGridData);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Act_cal_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid();
                //GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FacilityMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void gvperiod_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }
                DropDownList ddlStatus = e.Row.FindControl("ddlStatus") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlStatus, "CPS");
                ddlStatus.SelectedValue = gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString();
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "AUDITED")
                {
                    ddlStatus.Enabled = false;
                }
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "OPEN")
                {
                    ddlStatus.Items.Remove(new ListItem("NEVEROPEN", "NOP"));
                }
            }
        }


        protected void gvperiodData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvrP = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtperiod = (DataTable)Session["periodgd"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvrP = gvperiods.FooterRow;
                    if (gvrP == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToperiodGridControl(gvrP, dtperiod, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtperiod.Rows.Add(drList);
                        BindGrid_PERIOD(dtperiod);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Acct_cal_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox ntxtAcctYear = gvr.FindControl("ntxtAcctYear") as TextBox;
            TextBox dtp_BeginDate = gvr.FindControl("dtpBeginDate") as TextBox;
            TextBox dtp_Enddate = gvr.FindControl("dtpEnddate") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }
            slControls[0] = ntxtAcctYear;
            slControls[1] = dtp_BeginDate;
            slControls[2] = dtp_BeginDate;
            slControls[3] = dtp_Enddate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = "TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Accounting_Year_P"] + " ~ " + Prop_File_Data["Begin_Date_P"] + " ~ " + Prop_File_Data["Begin_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Account Year ~ Begin Date ~ Begin Date ~ End Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            ErrorCollection = CommonUtils.ValidateGridDate(dt_tmp, gvr.RowIndex, DBMethod.ConvertStringToDate(dtp_BeginDate.Text.ToString()), DBMethod.ConvertStringToDate(dtp_Enddate.Text.ToString()), "CAL_EFF_START_DT", "CALL_EFF_END_DT");

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }



            drList["CAL_ACCT_YEAR"] = ntxtAcctYear.Text;
            drList["CAL_EFF_START_DT"] = DBMethod.ConvertStringToDate(dtp_BeginDate.Text.ToString());


            if (dtp_Enddate.Text.ToString().Length > 0)
            {

                drList["CALL_EFF_END_DT"] = DBMethod.ConvertStringToDate(dtp_Enddate.Text.ToString());
            }
            else
            {
                drList["CALL_EFF_END_DT"] = DBNull.Value;
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        private DataRow AssignToperiodGridControl(GridViewRow gvrp, DataTable tmpdtGridDatap, string GMode, int rowindex)
        {
            ErrorCollection.Clear();
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlStatus = gvrp.FindControl("ddlStatus") as DropDownList;

            DataRow drList;
            DataTable dt_tmpp = tmpdtGridDatap.Copy();
            if (GMode == "A")
            {
                drList = dtperiod.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtperiod.Rows[rowindex];
                dt_tmpp.Rows.RemoveAt(rowindex);

            }


            drList["PERIOD_STATUS"] = ddlStatus.SelectedValue.ToString();


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        private void FillFooterGridCombo_frperiod(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlStatus = tmpgvr.FindControl("ddlStatus") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlStatus, "CPS");


                if (gvData.EditIndex >= 0)
                {
                    ddlStatus.SelectedValue = gvperiods.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.StrRecordId.ToString());
                DBMethod.DeleteEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Accounting Calendar");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }



                AssignToBE();

                ErrorCollection = AccountingCalendar_BLL.Validate(gL_ACCT_CALENDAR_HDR, Master.Mode, DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString()), DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()));

                if (ErrorCollection.Count > 0)
                {
                    bol_err = false;
                    return;
                }

                ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_CALENDAR(txtCalendarName.Text, gL_ACCT_CALENDAR_HDR.CAL_ID, txtEffectiveDate.Text, txtEndDate.Text);


                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("CurrCode", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            bol_err = false;
                            return;
                        }
                        else
                        {
                            ErrorCollection.Clear();
                        }
                    }
                    else
                    {
                        ErrorCollection.Clear();
                    }
                }
                btnAdd_Click(btnAdd, new EventArgs());

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);

                //AssignToBE();


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {


                //            DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {



                //            DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR, true);
                //            break;
                //        }
                //}

                //if (Session[FINSessionConstants.GridData] != null)
                //{
                //    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                //}

                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{

                //    if (dtGridData.Rows[iLoop][FINColumnConstants.CAL_DTL_ID].ToString() != "0")
                //    {
                //        using (IRepository<GL_ACCT_CALENDAR_DTL> userCtx = new DataRepository<GL_ACCT_CALENDAR_DTL>())
                //        {
                //            gL_ACCT_CALENDAR_DTL = userCtx.Find(r =>
                //                (r.CAL_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.CAL_DTL_ID].ToString())
                //                ).SingleOrDefault();
                //        }
                //    }



                //    gL_ACCT_CALENDAR_DTL.CAL_ACCT_YEAR = decimal.Parse(dtGridData.Rows[iLoop]["CAL_ACCT_YEAR"].ToString());
                //    if (dtGridData.Rows[iLoop]["CAL_EFF_START_DT"] != DBNull.Value)
                //    {
                //        gL_ACCT_CALENDAR_DTL.CAL_EFF_START_DT = DateTime.Parse(dtGridData.Rows[iLoop]["CAL_EFF_START_DT"].ToString());
                //    }

                //    if (dtGridData.Rows[iLoop]["CALL_EFF_END_DT"] != DBNull.Value)
                //    {
                //        gL_ACCT_CALENDAR_DTL.CALL_EFF_END_DT = DateTime.Parse(dtGridData.Rows[iLoop]["CALL_EFF_END_DT"].ToString());
                //    }
                //    else
                //    {
                //        gL_ACCT_CALENDAR_DTL.CALL_EFF_END_DT = null;
                //    }
                //    gL_ACCT_CALENDAR_DTL.ENABLED_FLAG = FINAppConstants.Y;



                //    gL_ACCT_CALENDAR_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                //    gL_ACCT_CALENDAR_DTL.CAL_ID = gL_ACCT_CALENDAR_HDR.CAL_ID;

                //    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                //    {
                //        gL_ACCT_CALENDAR_DTL.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                //        DBMethod.DeleteEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL);
                //    }
                //    else
                //    {
                //        if (dtGridData.Rows[iLoop][FINColumnConstants.CAL_DTL_ID].ToString() != "0")
                //        {
                //            gL_ACCT_CALENDAR_DTL.CAL_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.CAL_DTL_ID].ToString();
                //            gL_ACCT_CALENDAR_DTL.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                //            gL_ACCT_CALENDAR_DTL.MODIFIED_BY = this.LoggedUserName;
                //            gL_ACCT_CALENDAR_DTL.MODIFIED_DATE = DateTime.Today;
                //            DBMethod.SaveEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL, true);

                //        }
                //        else
                //        {
                //            gL_ACCT_CALENDAR_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CALENDAR_DTL_SEQ);
                //            gL_ACCT_CALENDAR_DTL.CAL_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCAL_D.ToString(), false, true);
                //            gL_ACCT_CALENDAR_DTL.CREATED_BY = this.LoggedUserName;
                //            gL_ACCT_CALENDAR_DTL.CREATED_DATE = DateTime.Today;
                //            DBMethod.SaveEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL);
                //        }
                //    }

                //}

                ////DisplaySaveCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        protected void btnGeneratePeriod_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                bol_err = true;
                GeneratePeriod(gvr);

                if (bol_err == true)
                {


                    BindGrid();

                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }

                    if (ErrorCollection.Count == 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GPCLick1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        private void GeneratePeriod(GridViewRow gvr)
        {
            try
            {

                System.Collections.SortedList slControls = new System.Collections.SortedList();
                TextBox ntxtAcctYear = gvr.FindControl("ntxtAcctYear") as TextBox;
                TextBox dtpBeginDate = gvr.FindControl("dtpBeginDate") as TextBox;
                TextBox dtpEnddate = gvr.FindControl("dtpEnddate") as TextBox;

                if (gvr.RowType.ToString() == "Footer")
                {
                    GL_ACCT_CALENDAR_DTL gL_ACCT_CALENDAR_DTL = new GL_ACCT_CALENDAR_DTL();
                }
                else
                {
                    using (IRepository<GL_ACCT_CALENDAR_DTL> userCtx = new DataRepository<GL_ACCT_CALENDAR_DTL>())
                    {
                        gL_ACCT_CALENDAR_DTL = userCtx.Find(r =>
                            (r.CAL_DTL_ID == gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.CAL_DTL_ID].ToString())
                            ).SingleOrDefault();
                    }
                }

                slControls[0] = txtCalendarName;
                slControls[1] = txtAdjPeriod;
                slControls[2] = txtEffectiveDate;
                slControls[3] = ntxtAcctYear;
                slControls[4] = dtpBeginDate;
                slControls[5] = dtpEnddate;
                slControls[6] = dtpEnddate;
                slControls[7] = dtpBeginDate;
                slControls[8] = dtpEnddate;




                ErrorCollection.Clear();
                string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~TextBox~TextBox~DateTime~DateTime~DateRangeValidate";

                string strMessage = "Calendar Name ~ Adjusting Period ~ Effective Date ~ Account Year ~ Begin Date ~ End Date ~ End Date ~ Begin Date ~ End Date";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    bol_err = false;
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                if (hf_AcId.Value == "0")
                {
                }
                else
                {
                    Master.Mode = FINAppConstants.Update;
                    Master.StrRecordId = hf_AcId.Value.ToString();
                    using (IRepository<GL_ACCT_CALENDAR_HDR> userCtx = new DataRepository<GL_ACCT_CALENDAR_HDR>())
                    {
                        gL_ACCT_CALENDAR_HDR = userCtx.Find(r =>
                            (r.CAL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_ACCT_CALENDAR_HDR;

                }

                AssignToBE();

                ErrorCollection = AccountingCalendar_BLL.Validate(gL_ACCT_CALENDAR_HDR, Master.Mode, DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString()), DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()));

                if (ErrorCollection.Count > 0)
                {
                    bol_err = false;
                    return;
                }

                ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_CALENDAR(txtCalendarName.Text, gL_ACCT_CALENDAR_HDR.CAL_ID, txtEffectiveDate.Text, txtEndDate.Text);


                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("CurrCode", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            bol_err = false;
                            return;
                        }
                        else
                        {
                            ErrorCollection.Clear();
                        }
                    }
                    else
                    {
                        ErrorCollection.Clear();
                    }
                }
                btnAdd_Click(btnAdd, new EventArgs());




                gL_ACCT_CALENDAR_DTL.CAL_ACCT_YEAR = ntxtAcctYear.Text;
                if (dtpBeginDate.Text.ToString().Length > 0)
                {
                    gL_ACCT_CALENDAR_DTL.CAL_EFF_START_DT = DBMethod.ConvertStringToDate((dtpBeginDate.Text));
                }

                if (dtpEnddate.Text.ToString().Length > 0)
                {
                    gL_ACCT_CALENDAR_DTL.CALL_EFF_END_DT = DBMethod.ConvertStringToDate(dtpEnddate.Text);
                }
                else
                {
                    gL_ACCT_CALENDAR_DTL.CALL_EFF_END_DT = null;
                }

                gL_ACCT_CALENDAR_DTL.ENABLED_FLAG = FINAppConstants.Y;



                gL_ACCT_CALENDAR_DTL.WORKFLOW_COMPLETION_STATUS = "1";// FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_ITEM_CATEGORY.ITEM_CATEGORY_ID);


                if (txtEndDate.Text != string.Empty)
                {
                    gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                }
                if (txtEndDate.Text != string.Empty)
                {
                    gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                ErrorCollection = AccountingCalendar_BLL.Validate(gL_ACCT_CALENDAR_HDR, Master.Mode, DBMethod.ConvertStringToDate(dtpBeginDate.Text.ToString()), DBMethod.ConvertStringToDate(dtpEnddate.Text.ToString()));

                if (ErrorCollection.Count > 0)
                {
                    bol_err = false;
                    return;
                }

                gL_ACCT_CALENDAR_DTL.CAL_ID = hf_AcId.Value.ToString();

                ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_CALENDAR_DTL(txtCalendarName.Text, ntxtAcctYear.Text, gL_ACCT_CALENDAR_DTL.CAL_DTL_ID, dtpBeginDate.Text, dtpEnddate.Text);


                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("caldtls", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            bol_err = false;
                            return;
                        }
                        else
                        {
                            ErrorCollection.Clear();
                        }
                    }
                    else
                    {
                        ErrorCollection.Clear();
                    }
                }



                GL_ACCT_PERIOD_DTL GL_ACCT_PERIOD_DTL = new GL_ACCT_PERIOD_DTL();


                if (ErrorCollection.Count == 0)
                {
                    if (Session["Deleted"] != null)
                    {
                        if (gL_ACCT_CALENDAR_DTL.CAL_DTL_ID != null)
                        {
                            DBMethod.ExecuteNonQuery(FIN.DAL.GL.AccountingCalendar_DAL.DeleteCalPeriodDtl(gL_ACCT_CALENDAR_DTL.CAL_DTL_ID));

                            DBMethod.DeleteEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL);

                            //  DBMethod.DeleteEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);
                        }
                        //gL_ACCT_CALENDAR_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CALENDAR_HDR_SEQ);
                        //gL_ACCT_CALENDAR_HDR.CAL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCAL_M.ToString(), false, true);
                        //gL_ACCT_CALENDAR_HDR.CREATED_BY = this.LoggedUserName;
                        //gL_ACCT_CALENDAR_HDR.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);

                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR, true);

                        gL_ACCT_CALENDAR_DTL.CAL_ID = gL_ACCT_CALENDAR_HDR.CAL_ID;

                        gL_ACCT_CALENDAR_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CALENDAR_DTL_SEQ);
                        gL_ACCT_CALENDAR_DTL.CAL_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCAL_D.ToString(), false, true);
                        gL_ACCT_CALENDAR_DTL.CREATED_BY = this.LoggedUserName;
                        gL_ACCT_CALENDAR_DTL.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL);

                        FINSP.GetSPFOR_PERIOD_DTL(hf_AcId.Value, txtAdjPeriod.Text, gL_ACCT_CALENDAR_DTL.CAL_DTL_ID, DBMethod.ConvertStringToDate(dtpBeginDate.Text.ToString()), DBMethod.ConvertStringToDate(dtpEnddate.Text.ToString()));

                    }
                    else if (gvr.RowType.ToString() != "Footer")
                    {
                        // gL_ACCT_CALENDAR_DTL.CAL_DTL_ID = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.CAL_DTL_ID].ToString();
                        gL_ACCT_CALENDAR_DTL.PK_ID = 0;
                        gL_ACCT_CALENDAR_DTL.MODIFIED_BY = this.LoggedUserName;
                        gL_ACCT_CALENDAR_DTL.MODIFIED_DATE = DateTime.Today;

                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR, true);

                        gL_ACCT_CALENDAR_DTL.CAL_ID = gL_ACCT_CALENDAR_HDR.CAL_ID;

                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL, true);

                        FINSP.GetSPFOR_PERIOD_DTL(hf_AcId.Value, txtAdjPeriod.Text, gL_ACCT_CALENDAR_DTL.CAL_DTL_ID, DBMethod.ConvertStringToDate(dtpBeginDate.Text.ToString()), DBMethod.ConvertStringToDate(dtpEnddate.Text.ToString()));

                    }
                    else
                    {
                        gL_ACCT_CALENDAR_DTL.CAL_ID = gL_ACCT_CALENDAR_HDR.CAL_ID;

                        gL_ACCT_CALENDAR_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CALENDAR_DTL_SEQ);
                        gL_ACCT_CALENDAR_DTL.CAL_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCAL_D.ToString(), false, true);
                        gL_ACCT_CALENDAR_DTL.CREATED_BY = this.LoggedUserName;
                        gL_ACCT_CALENDAR_DTL.CREATED_DATE = DateTime.Today;

                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);

                        gL_ACCT_CALENDAR_DTL.CAL_ID = gL_ACCT_CALENDAR_HDR.CAL_ID;

                        DBMethod.SaveEntity<GL_ACCT_CALENDAR_DTL>(gL_ACCT_CALENDAR_DTL);

                        FINSP.GetSPFOR_PERIOD_DTL(hf_AcId.Value, txtAdjPeriod.Text, gL_ACCT_CALENDAR_DTL.CAL_DTL_ID, DBMethod.ConvertStringToDate(dtpBeginDate.Text.ToString()), DBMethod.ConvertStringToDate(dtpEnddate.Text.ToString()));

                    }
                }

                DataTable dtperiod = new DataTable();
                // FINSP.GetSPFOR_PERIOD_DTL(gL_ACCT_CALENDAR_DTL.CAL_DTL_ID);
                // ModalPopupExtender4.Show();
                // dtperiod = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.getPerioddtl()).Tables[0];

                //BindGrid_PERIOD(dtperiod);
            }
            catch (Exception ex)
            {
                bol_err = false;
                ErrorCollection.Add("GP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //if (Session["Deleted"] != null)
                //{

                //    DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);
                //}
                //else
                //{

                //    switch (Master.Mode)
                //    {
                //        case FINAppConstants.Add:
                //            {

                //                DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR);
                //                break;
                //            }
                //        case FINAppConstants.Update:
                //            {


                //                DBMethod.SaveEntity<GL_ACCT_CALENDAR_HDR>(gL_ACCT_CALENDAR_HDR, true);
                //                break;
                //            }
                //    }
                //}

                hf_AcId.Value = gL_ACCT_CALENDAR_HDR.CAL_ID;
                // dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.getAcctcalendardtl(hf_AcId.Value.ToString())).Tables[0];
                //BindGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACAdd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtEffectiveDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtCalendarName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ModalPopupExtender4.Show();
                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
                dtperiod = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.getPerioddtl(gvData.DataKeys[gvrP.RowIndex].Values["CAL_DTL_ID"].ToString())).Tables[0];
                BindGrid_PERIOD(dtperiod);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FilslFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session["periodgd"] != null)
                {
                    dtperiod = (DataTable)Session["periodgd"];
                }
                if (gvperiods.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvperiods.Rows.Count; iLoop++)
                    {
                        gL_ACCT_PERIOD_DTL = new GL_ACCT_PERIOD_DTL();

                        if (gvperiods.DataKeys[iLoop].Values[FINColumnConstants.PERIOD_ID].ToString() != "0")
                        {
                            DropDownList ddlStatus = gvperiods.Rows[iLoop].FindControl("ddlStatus") as DropDownList;

                            if (dtperiod.Rows[iLoop]["LOOKUP_ID"].ToString() != ddlStatus.SelectedValue.ToString())
                            {
                                using (IRepository<GL_ACCT_PERIOD_DTL> userCtx = new DataRepository<GL_ACCT_PERIOD_DTL>())
                                {
                                    gL_ACCT_PERIOD_DTL = userCtx.Find(r =>
                                        (r.PERIOD_ID == gvperiods.DataKeys[iLoop].Values[FINColumnConstants.PERIOD_ID].ToString())
                                        ).SingleOrDefault();
                                }

                                gL_ACCT_PERIOD_DTL.PERIOD_STATUS = ddlStatus.SelectedValue.ToString();
                                gL_ACCT_PERIOD_DTL.MODIFIED_BY = this.LoggedUserName;
                                gL_ACCT_PERIOD_DTL.MODIFIED_DATE = DateTime.Today;
                                if (ddlStatus.SelectedValue.ToString() == "CLOSE")
                                {
                                    gL_ACCT_PERIOD_DTL.PERIOD_FIRST_CLOSE_DT = DateTime.Now;
                                }
                                DBMethod.SaveEntity<GL_ACCT_PERIOD_DTL>(gL_ACCT_PERIOD_DTL, true);
                                saveBool = true;
                            }
                        }
                    }
                    if (saveBool)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGriad", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}