﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;
using System.Text;

namespace FIN.Client.GL
{
    public partial class BudgetDetails : PageBase
    {
        GL_BUDGET_HDR gL_BUDGET_HDR = new GL_BUDGET_HDR();
        GL_BUDGET_DTL gL_BUDGET_DTL = new GL_BUDGET_DTL();
        DataTable dtGridData = new DataTable();
        DataTable dtGS = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataSet dsAccCodeLevel = new DataSet();
        Boolean isAccCode;
        int i, modulecount = 0, applicationCount = 0, moduleappcount = 0, functionCount = 0, submenuCount = 0, subsubmenuCount = 0, accCodeCount = 0;
        string IsCodeORGroup = string.Empty;
        DataSet dsModuleList = new DataSet();
        DataSet dsLevel2 = new DataSet();
        DataSet dsLevel3 = new DataSet();
        DataSet dsLevel4 = new DataSet();
        DataSet dsLevel5 = new DataSet();
        DataSet dsAccCode = new DataSet();

        string previousModule = string.Empty, previousMainApplication = string.Empty, previousFunction = string.Empty, previousSubMenu = string.Empty;
        string previousSubSubMenu = string.Empty;
        string previousAccCodeMenu = string.Empty;
        TreeNode moduleNode = null;
        TreeNode applicationNode = null;
        TreeNode functionNode = null;
        TreeNode subMenuNode = null;
        TreeNode subsubMenuNode = null;
        TreeNode accCodeNodeNode = null;
        Boolean errorPopup = false;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {

                    AssignToControl();

                    dtGS = DBMethod.ExecuteQuery(Budget_DAL.getGlobalSegment()).Tables[0];
                    hfgs.Value = dtGS.Rows[0]["GLOBAL_SEGMENT_BALANCED"].ToString();

                    if (hfgs.Value != "0")
                    {
                        ddlGlobalSegment.Enabled = true;
                    }
                    else
                    {
                        ddlGlobalSegment.Enabled = false;
                    }

                    BindTreeview();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                    ChnageLanguage();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void ChnageLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    divAccCode.InnerHtml = Prop_File_Data["Account_Code_P"];
                    divAccName.InnerHtml = Prop_File_Data["Account_Name_P"];
                    rblBudget.Items[0].Text = Prop_File_Data["Amount_P"];
                    rblBudget.Items[1].Text = Prop_File_Data["Based_on_Previous_Year_P"];

                    lblSegment1.Text = Prop_File_Data["Segment_1_P"];
                    lblSegment2.Text = Prop_File_Data["Segment_2_P"];
                    lblSegment3.Text = Prop_File_Data["Segment_3_P"];
                    lblSegment4.Text = Prop_File_Data["Segment_4_P"];
                    lblSegment5.Text = Prop_File_Data["Segment_5_P"];
                    lblSegment6.Text = Prop_File_Data["Segment_6_P"];

                  
                    Label15.Text = Prop_File_Data["Current_Year_P"];
                    Label16.Text = Prop_File_Data["Previous_Year_P"];

                    lblPeriod1.Text = Prop_File_Data["Period_1_P"];
                    lblPeriod2.Text = Prop_File_Data["Period_2_P"];
                    lblPeriod3.Text = Prop_File_Data["Period_3_P"];
                    lblPeriod4.Text = Prop_File_Data["Period_4_P"];
                    lblPeriod5.Text = Prop_File_Data["Period_5_P"];
                    lblPeriod6.Text = Prop_File_Data["Period_6_P"];
                    lblPeriod7.Text = Prop_File_Data["Period_7_P"];
                    lblPeriod8.Text = Prop_File_Data["Period_8_P"];
                    lblPeriod9.Text = Prop_File_Data["Period_9_P"];
                    lblPeriod10.Text = Prop_File_Data["Period_10_P"];
                    lblPeriod11.Text = Prop_File_Data["Period_11_P"];
                    lblPeriod12.Text = Prop_File_Data["Period_12_P"];


                    lblPrevPeriod1.Text = Prop_File_Data["Period_1_P"];
                    lblPrevPeriod2.Text = Prop_File_Data["Period_2_P"];
                    lblPrevPeriod3.Text = Prop_File_Data["Period_3_P"];
                    lblPrevPeriod4.Text = Prop_File_Data["Period_4_P"];
                    lblPrevPeriod5.Text = Prop_File_Data["Period_5_P"];
                    lblPrevPeriod6.Text = Prop_File_Data["Period_6_P"];
                    lblPrevPeriod7.Text = Prop_File_Data["Period_7_P"];
                    lblPrevPeriod8.Text = Prop_File_Data["Period_8_P"];
                    lblPrevPeriod9.Text = Prop_File_Data["Period_9_P"];
                    lblPrevPeriod10.Text = Prop_File_Data["Period_10_P"];
                    lblPrevPeriod11.Text = Prop_File_Data["Period_11_P"];
                    lblPrevPeriod12.Text = Prop_File_Data["Period_12_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    if (!Page.IsPostBack)
        //    {
        //        int selectedNode = 0, moduleNode = 0, appNode = 0, functionId = 0;

        //        string selectAppNode = string.Empty, moduleId = string.Empty, functionAppNode = string.Empty;
        //        if (tvModules != null)
        //        {
        //            if (tvModules.Nodes.Count > 0)
        //            {
        //                //if (Request.QueryString["Module"] != null)
        //                //{
        //                //    selectedNode = int.Parse(Request.QueryString["Module"]);
        //                //    if (tvModules.Nodes[selectedNode] != null)
        //                //    {
        //               // tvModules.CollapseAll();
        //                //tvModules.SelectedNode.Target[e.];// Focus();
        //                //    }
        //                //}
        //            }
        //        }
        //    }
        //}

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = true;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = true;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion

        # region events
        private void BindLevel5AccCode(int i5, int i4)
        {
            try
            {
                if (subsubMenuNode != null)
                {
                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                    {

                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                    accCodeCount += 1;

                                    if (subsubMenuNode != null)
                                    {
                                        subsubMenuNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel1AccCode(string grpId, int i1)
        {
            try
            {
                if (applicationNode != null)
                {
                    dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                }
                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();


                                    accCodeCount += 1;

                                    //if (applicationNode != null)
                                    //{
                                    //    applicationNode.ChildNodes.Add(accCodeNodeNode);
                                    //}
                                    moduleNode.ChildNodes.Add(accCodeNodeNode);
                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel2AccCode(int i2, int i1)
        {
            try
            {
                if (applicationNode != null)
                {
                    if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                    {
                        //dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel2.Tables[0].Rows[i2]["level1_Id"].ToString(), "l5"));
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString(), "l5"));
                    }
                }
                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();


                                    accCodeCount += 1;

                                    if (applicationNode != null)
                                    {
                                        applicationNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel3AccCode(int i3, int i2)
        {
            try
            {
                if (functionNode != null)
                {
                    if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                    {
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                    accCodeCount += 1;

                                    if (functionNode != null)
                                    {
                                        functionNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel4AccCode(int i4, int i3)
        {
            try
            {
                if (subMenuNode != null)
                {
                    if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                    {
                        // dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString(), "l5"));
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();
                                    // accCodeNodeNode.PopulateOnDemand = true;

                                    accCodeCount += 1;

                                    if (subMenuNode != null)
                                    {
                                        subMenuNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindTreeview()
        {
            try
            {
                ErrorCollection.Clear();

                AccountingGroups_DAL.GetSP_AccountGroupLinksCodeName();
                dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1());

                if (dsModuleList.Tables[0].Rows.Count > 0)
                {
                    for (i = 0; i <= dsModuleList.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != previousModule.ToString())
                            {
                                if (previousModule != string.Empty)
                                {
                                    tvModules.Nodes.Add(moduleNode);
                                }
                                moduleNode = new TreeNode();
                                moduleNode.Text = dsModuleList.Tables[0].Rows[i]["level1"].ToString();
                                moduleNode.Value = dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString();
                                moduleNode.PopulateOnDemand = true;
                                modulecount += 1;
                                applicationCount = 0;
                                functionCount = 0;
                                submenuCount = 0;
                                subsubmenuCount = 0;
                                moduleNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                previousModule = (dsModuleList.Tables[0].Rows[i]["level1"].ToString());
                            }
                        }

                        // level 2
                        dsLevel2 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1Child(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString()));//.Replace(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().Substring(0, dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().IndexOf("ACC")), "")));
                        if (dsLevel2.Tables[0].Rows.Count > 0)
                        {
                            for (int i2 = 0; i2 <= dsLevel2.Tables[0].Rows.Count - 1; i2++)
                            {
                                if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != string.Empty || dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                {
                                    if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != previousMainApplication.ToString())
                                    {
                                        applicationNode = new TreeNode();
                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                        {
                                            dsLevel3 = new DataSet();
                                        }
                                        else
                                        {
                                            applicationNode.Text = dsLevel2.Tables[0].Rows[i2]["level2"].ToString();
                                            applicationNode.Value = dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString();
                                            dsLevel3 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel2Child(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString()));
                                        }
                                        applicationNode.PopulateOnDemand = true;

                                        moduleappcount = modulecount - 1;
                                        functionCount = 0;

                                        applicationCount += 1;
                                        applicationNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() != "-")
                                        {
                                            if (dsLevel3.Tables.Count > 0)
                                            {
                                                if (dsLevel3.Tables[0].Rows.Count > 0)
                                                {
                                                    if (dsLevel3.Tables[0].Rows[0]["level3_Id"].ToString() != "-")
                                                    {
                                                        moduleNode.ChildNodes.Add(applicationNode);
                                                    }
                                                }
                                            }
                                        }
                                        previousMainApplication = (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString());

                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                        {
                                            BindLevel1AccCode(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString(), 1);
                                        }

                                        //level 3 
                                        if (dsLevel3.Tables.Count > 0)
                                        {
                                            if (dsLevel3.Tables[0].Rows.Count > 0)
                                            {
                                                for (int i3 = 0; i3 <= dsLevel3.Tables[0].Rows.Count - 1; i3++)
                                                {
                                                    if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != string.Empty || dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                    {
                                                        if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != previousFunction.ToString())
                                                        {
                                                            functionNode = new TreeNode();

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                            {
                                                                dsLevel4 = new DataSet();
                                                            }
                                                            else
                                                            {
                                                                functionNode.Text = dsLevel3.Tables[0].Rows[i3]["level3"].ToString();
                                                                functionNode.Value = dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString();
                                                                dsLevel4 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel3Child(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString()));
                                                            }

                                                            functionNode.PopulateOnDemand = true;

                                                            functionCount += 1;
                                                            submenuCount = 0;
                                                            functionNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-")
                                                            {
                                                                if (dsLevel4.Tables.Count > 0)
                                                                {
                                                                    if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                    {
                                                                        if (dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                        {
                                                                            applicationNode.ChildNodes.Add(functionNode);
                                                                        }
                                                                        else if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-" || dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                        {
                                                                            applicationNode.ChildNodes.Add(functionNode);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            previousFunction = (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString());

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                            {
                                                                BindLevel2AccCode(i2, i);
                                                            }
                                                            if (dsLevel4.Tables.Count > 0)
                                                            {
                                                                if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-" || dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                    {
                                                                        BindLevel3AccCode(i3, i3);
                                                                    }
                                                                }
                                                            }
                                                            // start level4
                                                            if (dsLevel4.Tables.Count > 0)
                                                            {
                                                                if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    for (int i4 = 0; i4 <= dsLevel4.Tables[0].Rows.Count - 1; i4++)
                                                                    {
                                                                        if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != string.Empty || dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                        {
                                                                            if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != previousSubMenu.ToString())
                                                                            {
                                                                                subMenuNode = new TreeNode();
                                                                                dsLevel5 = new DataSet();

                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                                {
                                                                                    dsLevel5 = new DataSet();
                                                                                }
                                                                                else
                                                                                {
                                                                                    subMenuNode.Text = dsLevel4.Tables[0].Rows[i4]["level4"].ToString();
                                                                                    subMenuNode.Value = dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString();
                                                                                    dsLevel5 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel4Child(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString()));
                                                                                }

                                                                                subMenuNode.PopulateOnDemand = true;

                                                                                submenuCount += 1;
                                                                                subsubmenuCount = 0;
                                                                                subMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString().Trim() != "-")
                                                                                {
                                                                                    if (dsLevel5.Tables.Count > 0)
                                                                                    {
                                                                                        if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                                        {
                                                                                            if (dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                            {
                                                                                                //applicationNode.ChildNodes.Add(functionNode);
                                                                                                functionNode.ChildNodes.Add(subMenuNode);
                                                                                            }
                                                                                            else if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() != "-" || dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                            {
                                                                                                functionNode.ChildNodes.Add(subMenuNode);
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                }
                                                                                previousSubMenu = (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString());


                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                                {
                                                                                    BindLevel3AccCode(i3, i);
                                                                                }
                                                                                if (dsLevel5.Tables.Count > 0)
                                                                                {
                                                                                    if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                                    {
                                                                                        if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() != "-" || dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                        {
                                                                                            //  BindLevel4AccCode(i4, i4);
                                                                                        }
                                                                                    }
                                                                                }
                                                                                //start level 5
                                                                                if (dsLevel5.Tables.Count > 0)
                                                                                {
                                                                                    if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                                    {
                                                                                        for (int i5 = 0; i5 <= dsLevel5.Tables[0].Rows.Count - 1; i5++)
                                                                                        {
                                                                                            if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != string.Empty || dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                            {
                                                                                                if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != previousSubSubMenu.ToString() || dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                {
                                                                                                    subsubMenuNode = new TreeNode();

                                                                                                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                    {
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        subsubMenuNode.Text = dsLevel5.Tables[0].Rows[i5]["level5"].ToString();
                                                                                                        subsubMenuNode.Value = dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString();
                                                                                                    }

                                                                                                    subsubMenuNode.PopulateOnDemand = true;

                                                                                                    subsubmenuCount += 1;
                                                                                                    accCodeCount = 0;
                                                                                                    subsubMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;


                                                                                                    subMenuNode.ChildNodes.Add(subsubMenuNode);

                                                                                                    previousSubSubMenu = (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString());

                                                                                                    //start acc code
                                                                                                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                    {
                                                                                                        BindLevel4AccCode(i4, i3);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        BindLevel5AccCode(i5, i4);
                                                                                                    }

                                                                                                    //if (dsAccCode.Tables.Count == 0 || dsAccCode.Tables[0].Rows.Count== 0)
                                                                                                    //{
                                                                                                    //    if (dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                                    //    {
                                                                                                    //        functionNode.ChildNodes.RemoveAt(i4);
                                                                                                    //    }
                                                                                                    //    //subMenuNode.ChildNodes.Remove(i3);
                                                                                                    //}
                                                                                                    //end acc code
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        BindLevel4AccCode(i4, i3);
                                                                                    }
                                                                                }
                                                                                //// end level5
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    BindLevel3AccCode(i3, i3);
                                                                }
                                                            }
                                                            //  end level4
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                BindLevel2AccCode(i2, i);
                                            }
                                        }
                                        //end level3
                                    }
                                }
                            }
                        }
                        else
                        {
                            BindLevel2AccCode(i, i);
                        }
                        //end level2
                    }
                    tvModules.Nodes.Add(moduleNode);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsscodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindTreeview1()
        {
            try
            {
                ErrorCollection.Clear();
                AccountingGroups_DAL.GetSP_AccountGroupLinksCodeName();

                int i, modulecount = 0, applicationCount = 0, moduleappcount = 0, functionCount = 0, submenuCount = 0, subsubmenuCount = 0, accCodeCount = 0;

                DataSet dsModuleList = new DataSet();
                DataSet dsLevel2 = new DataSet();
                DataSet dsLevel3 = new DataSet();
                DataSet dsLevel4 = new DataSet();
                DataSet dsLevel5 = new DataSet();
                DataSet dsAccCode = new DataSet();



                //  dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.GetAccountGroupLinkWithCodeName());
                dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1());

                if (dsModuleList.Tables[0].Rows.Count > 0)
                {
                    string previousModule = string.Empty, previousMainApplication = string.Empty, previousFunction = string.Empty, previousSubMenu = string.Empty;
                    string previousSubSubMenu = string.Empty;
                    string previousAccCodeMenu = string.Empty;
                    TreeNode moduleNode = null;
                    TreeNode applicationNode = null;
                    TreeNode functionNode = null;
                    TreeNode subMenuNode = null;
                    TreeNode subsubMenuNode = null;
                    TreeNode accCodeNodeNode = null;

                    for (i = 0; i <= dsModuleList.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != previousModule.ToString())
                            {
                                if (previousModule != string.Empty)
                                {
                                    tvModules.Nodes.Add(moduleNode);
                                }
                                moduleNode = new TreeNode();
                                moduleNode.Text = dsModuleList.Tables[0].Rows[i]["level1"].ToString();
                                moduleNode.Value = dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString();
                                moduleNode.PopulateOnDemand = true;
                                modulecount += 1;
                                applicationCount = 0;
                                functionCount = 0;
                                submenuCount = 0;
                                subsubmenuCount = 0;

                                //moduleNode.ExpandAll();


                                moduleNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                previousModule = (dsModuleList.Tables[0].Rows[i]["level1"].ToString());
                            }
                        }

                        // level 2
                        dsLevel2 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1Child(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString()));//.Replace(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().Substring(0, dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().IndexOf("ACC")), "")));
                        if (dsLevel2.Tables[0].Rows.Count > 0)
                        {
                            for (int i2 = 0; i2 <= dsLevel2.Tables[0].Rows.Count - 1; i2++)
                            {
                                if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != string.Empty)
                                {
                                    if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != previousMainApplication.ToString())
                                    {

                                        applicationNode = new TreeNode();
                                        applicationNode.Text = dsLevel2.Tables[0].Rows[i2]["level2"].ToString();
                                        applicationNode.Value = dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString();
                                        applicationNode.PopulateOnDemand = true;

                                        moduleappcount = modulecount - 1;
                                        functionCount = 0;

                                        applicationCount += 1;
                                        applicationNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                        moduleNode.ChildNodes.Add(applicationNode);
                                        previousMainApplication = (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString());

                                        //level 3 
                                        dsLevel3 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel2Child(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString().Replace(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString().Substring(0, dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString().IndexOf("ACC")), "")));
                                        if (dsLevel3.Tables[0].Rows.Count > 0)
                                        {
                                            for (int i3 = 0; i3 <= dsLevel3.Tables[0].Rows.Count - 1; i3++)
                                            {
                                                if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != string.Empty)
                                                {
                                                    if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != previousFunction.ToString())
                                                    {
                                                        functionNode = new TreeNode();
                                                        functionNode.Text = dsLevel3.Tables[0].Rows[i3]["level3"].ToString();
                                                        functionNode.Value = dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString();
                                                        functionNode.PopulateOnDemand = true;

                                                        functionCount += 1;
                                                        submenuCount = 0;
                                                        functionNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                                        applicationNode.ChildNodes.Add(functionNode);
                                                        previousFunction = (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString());

                                                        //    start level4
                                                        dsLevel4 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel3Child(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString().Replace(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString().Substring(0, dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString().IndexOf("ACC")), "")));
                                                        if (dsLevel4.Tables[0].Rows.Count > 0)
                                                        {
                                                            for (int i4 = 0; i4 <= dsLevel4.Tables[0].Rows.Count - 1; i4++)
                                                            {
                                                                if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != string.Empty)
                                                                {
                                                                    if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != previousSubMenu.ToString())
                                                                    {
                                                                        subMenuNode = new TreeNode();

                                                                        subMenuNode.Text = dsLevel4.Tables[0].Rows[i4]["level4"].ToString();
                                                                        subMenuNode.Value = dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString();
                                                                        subMenuNode.PopulateOnDemand = true;

                                                                        submenuCount += 1;
                                                                        subsubmenuCount = 0;
                                                                        subMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                                                        functionNode.ChildNodes.Add(subMenuNode);
                                                                        previousSubMenu = (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString());


                                                                        //start level 5
                                                                        dsLevel5 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel4Child(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString().Replace(dsLevel4.Tables[0].Rows[i]["level4_Id"].ToString().Substring(0, dsLevel4.Tables[0].Rows[i]["level4_Id"].ToString().IndexOf("ACC")), "")));
                                                                        if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                        {
                                                                            for (int i5 = 0; i5 <= dsLevel5.Tables[0].Rows.Count - 1; i5++)
                                                                            {
                                                                                if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != string.Empty)
                                                                                {
                                                                                    if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != previousSubSubMenu.ToString())
                                                                                    {
                                                                                        subsubMenuNode = new TreeNode();

                                                                                        subsubMenuNode.Text = dsLevel5.Tables[0].Rows[i5]["level5"].ToString();
                                                                                        subsubMenuNode.Value = dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString();
                                                                                        subsubMenuNode.PopulateOnDemand = true;

                                                                                        subsubmenuCount += 1;
                                                                                        accCodeCount = 0;
                                                                                        subsubMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                                                                        subMenuNode.ChildNodes.Add(subsubMenuNode);
                                                                                        previousSubSubMenu = (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString());


                                                                                        //start acc code

                                                                                        if (subsubMenuNode != null)
                                                                                        {
                                                                                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString().Replace(dsLevel5.Tables[0].Rows[i]["level5_Id"].ToString().Substring(0, dsLevel5.Tables[0].Rows[i]["level5_Id"].ToString().IndexOf("ACC")), ""), "l5"));//
                                                                                        }

                                                                                        if (dsAccCode.Tables[0].Rows.Count > 0)
                                                                                        {
                                                                                            for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                                                                                            {
                                                                                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                                                                                                {
                                                                                                    if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                                                                                    {
                                                                                                        accCodeNodeNode = new TreeNode();

                                                                                                        accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                                                                                        accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();
                                                                                                        // accCodeNodeNode.PopulateOnDemand = true;

                                                                                                        accCodeCount += 1;
                                                                                                        // accCodeNodeNode.SelectAction = TreeNodeSelectAction.Expand;
                                                                                                        if (subsubMenuNode != null)
                                                                                                        {
                                                                                                            subsubMenuNode.ChildNodes.Add(accCodeNodeNode);
                                                                                                        }

                                                                                                        previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        //end acc code
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (subMenuNode != null)
                                                                            {
                                                                                dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString().Replace(dsLevel4.Tables[0].Rows[i]["level4_Id"].ToString().Substring(0, dsLevel4.Tables[0].Rows[i]["level4_Id"].ToString().IndexOf("ACC")), ""), "l4"));//
                                                                            }


                                                                            if (dsAccCode.Tables[0].Rows.Count > 0)
                                                                            {
                                                                                for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                                                                                {
                                                                                    if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                                                                                    {
                                                                                        if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                                                                        {
                                                                                            accCodeNodeNode = new TreeNode();

                                                                                            accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                                                                            accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();
                                                                                            // accCodeNodeNode.PopulateOnDemand = true;

                                                                                            accCodeCount += 1;

                                                                                            if (subMenuNode != null)
                                                                                            {
                                                                                                subMenuNode.ChildNodes.Add(accCodeNodeNode);
                                                                                            }

                                                                                            previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //// end level5


                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (functionNode != null)
                                                            {
                                                                dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString().Replace(dsLevel3.Tables[0].Rows[i]["level3_Id"].ToString().Substring(0, dsLevel3.Tables[0].Rows[i]["level3_Id"].ToString().IndexOf("ACC")), ""), "l3"));//
                                                            }

                                                            if (dsAccCode.Tables[0].Rows.Count > 0)
                                                            {
                                                                for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                                                                {
                                                                    if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                                                                    {
                                                                        if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                                                        {
                                                                            accCodeNodeNode = new TreeNode();

                                                                            accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                                                            accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                                                            accCodeCount += 1;

                                                                            if (functionNode != null)
                                                                            {
                                                                                functionNode.ChildNodes.Add(accCodeNodeNode);
                                                                            }

                                                                            previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        //  end level4
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (applicationNode != null)
                                            {
                                                dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString().Replace(dsLevel2.Tables[0].Rows[i]["level2_Id"].ToString().Substring(0, dsLevel2.Tables[0].Rows[i]["level2_Id"].ToString().IndexOf("ACC")), ""), "l2"));//
                                            }


                                            if (dsAccCode.Tables[0].Rows.Count > 0)
                                            {
                                                for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                                                {
                                                    if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                                                    {
                                                        if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                                        {
                                                            accCodeNodeNode = new TreeNode();

                                                            accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                                            accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();


                                                            accCodeCount += 1;

                                                            if (applicationNode != null)
                                                            {
                                                                applicationNode.ChildNodes.Add(accCodeNodeNode);
                                                            }

                                                            previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //end level3

                                    }
                                }
                            }
                        }
                        //end level2

                    }

                    tvModules.Nodes.Add(moduleNode);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsscodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void ddlOrganisation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlCompanyName.SelectedValue.ToString());

                //dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg(ddlCompanyName.SelectedValue.ToString());
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                Session["errorPopup"] = null;
                Session["IsCodeORGroup"] = null;

                Session["tmpChildEntity"] = null;
                Session["BudDtlId"] = null;

                Session["Depth0"] = null;
                Session["Depth1"] = null;
                Session["Depth2"] = null;
                Session["Depth3"] = null;
                Session["Depth4"] = null;
                Session["Depth5"] = null;
                Session["Level0"] = null;
                Session["Level1"] = null;
                Session["Level2"] = null;
                Session["Level3"] = null;
                Session["Level4"] = null;
                Session["Level5"] = null;

                Session["Period1"] = null;
                Session["Period2"] = null;
                Session["Period3"] = null;
                Session["Period4"] = null;
                Session["Period5"] = null;
                Session["Period6"] = null;
                Session["Period7"] = null;
                Session["Period8"] = null;
                Session["Period9"] = null;
                Session["Period10"] = null;
                Session["Period11"] = null;
                Session["Period12"] = null;

                Session["PeriodCount"] = "0";

                EntityData = null;

                if (Master.Mode == FINAppConstants.Add)
                {
                    Session["BudCode"] = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003.ToString(), false, true);
                }

                //  dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg("0", "0");

                //dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId);



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {


                    //   divFilter.Visible = false;

                    //  rblAccCodeGroup.Enabled = false;
                    ddlCompanyName.Enabled = false;
                    ddlGlobalSegment.Enabled = false;


                    dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId);
                    using (IRepository<GL_BUDGET_HDR> userCtx = new DataRepository<GL_BUDGET_HDR>())
                    {
                        gL_BUDGET_HDR = userCtx.Find(r =>
                            (r.BUDGET_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //AccountingGroupLinks_BLL.getClassEntity(Master.StrRecordId);

                    //if (gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    //{
                    //    //  gvData.Enabled = false;
                    //    btnSave.Visible = false;
                    //}


                    EntityData = gL_BUDGET_HDR;

                    txtBudgetName.Text = gL_BUDGET_HDR.BUDGET_NAME;
                    ddlCompanyName.SelectedValue = gL_BUDGET_HDR.BUDGET_COMP;

                    ddlBudgetType.SelectedValue = gL_BUDGET_HDR.BUDGET_TYPE;
                    ddlCalyr.SelectedValue = gL_BUDGET_HDR.CAL_DTL_ID;
                    Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlCompanyName.SelectedValue.ToString());

                    ddlGlobalSegment.SelectedValue = gL_BUDGET_HDR.SEMENT_ID;



                    if (gL_BUDGET_HDR.BUDGET_PERIOD_START_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_START_DT.ToString());
                    }
                    if (gL_BUDGET_HDR.BUDGET_PERIOD_END_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_END_DT.ToString());
                    }
                    //if (dtGridData.Rows.Count > 0)
                    //{
                    //    txtPeriodPop1.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_01"].ToString();
                    //    txtPeriodPop2.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_02"].ToString();
                    //    txtPeriodPop3.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_03"].ToString();
                    //    txtPeriodPop4.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_04"].ToString();
                    //    txtPeriodPop5.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_05"].ToString();
                    //    txtPeriodPop6.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_06"].ToString();
                    //    txtPeriodPop7.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_07"].ToString();
                    //    txtPeriodPop8.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_08"].ToString();
                    //    txtPeriodPop9.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_09"].ToString();
                    //    txtPeriodPop10.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_10"].ToString();
                    //    txtPeriodPop11.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_11"].ToString();
                    //    txtPeriodPop12.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_12"].ToString();


                    //    if (dtGridData.Rows[0]["IS_AMT_OR_PREV_YR"].ToString() == "1")
                    //    {
                    //        rblBudget.SelectedValue = "1";
                    //    }
                    //    else
                    //    {
                    //        rblBudget.SelectedValue = "2";
                    //    }

                    //    txtAccountCode.Text = dtGridData.Rows[0]["ACCT_CODE"].ToString();
                    //    txtAccountName.Text = dtGridData.Rows[0]["ACCT_CODE_DESC"].ToString();
                    //    txtPer.Text = dtGridData.Rows[0]["PERCENTAGE"].ToString();
                    //    txtPreBudAmt.Text = dtGridData.Rows[0]["PREV_YR_BUDGET_AMT"].ToString();
                    //    txtPreRevBudAmt.Text = dtGridData.Rows[0]["PREV_REV_BUDGET_AMT"].ToString();
                    //    txtPreYrActual.Text = dtGridData.Rows[0]["ACTUAL_PREVIOUS_AMOUNT"].ToString();

                    //    txtBudgetAmt.Text = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[0]["BUDGET_AMOUNT"].ToString()).ToString();

                    //}

                }

                if (rblBudget.SelectedValue == "1")
                {
                    txtBudgetAmt.Enabled = true;
                    txtPer.Enabled = false;
                    divPers.Visible = false;
                }
                else
                {
                    txtBudgetAmt.Enabled = false;
                    txtPer.Enabled = true;
                    divPers.Visible = true;
                }
                // BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindSegmentValues()
        {
            DataTable dtdataAc = new DataTable();

            if (Session["AccCodes"] != null)
            {
                dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(Session["AccCodes"].ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
            }
            else if (Session["AccGroups"] != null)
            {
                dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccGroup(Session["AccGroups"].ToString())).Tables[0];
            }

            Session["AccCode_Seg"] = dtdataAc;

            if (dtdataAc != null)
            {
                if (dtdataAc.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {
                        Label lblSegment = (Label)Master.FindControl("FINContent").FindControl("lblSegment" + (iLoop + 1));
                        if (lblSegment != null)
                        {
                            lblSegment.Text = dtdataAc.Rows[iLoop][FINColumnConstants.SEGMENT_NAME].ToString();
                        }
                        if (iLoop == 0)
                        {
                            DropDownList ddlSegment = (DropDownList)Master.FindControl("FINContent").FindControl("ddlSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());

                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString() != string.Empty)
                            //{
                            //    ddlSegment1.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString();
                            //}
                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString() != string.Empty)
                            //{
                            //    ddlSegment2.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString();
                            //}
                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString() != string.Empty)
                            //{
                            //    ddlSegment3.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString();
                            //}
                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString() != string.Empty)
                            //{
                            //    ddlSegment4.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString();
                            //}
                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString() != string.Empty)
                            //{
                            //    ddlSegment5.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString();
                            //}
                            //if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString() != string.Empty)
                            //{
                            //    ddlSegment6.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString();
                            //}
                        }
                    }
                }
            }


            var tmpChildEntity = new List<Tuple<object, string>>();

            if (Session["tmpChildEntity"] != null)
            {
                tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];

                if (tmpChildEntity != null)
                {
                    if (tmpChildEntity.Count > 0)
                    {
                        for (int i = 0; i < tmpChildEntity.Count; i++)
                        {
                            if (tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_CODE_ID || tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_GRP_ID)
                            {
                                
                                DropDownList ddlSegment = (DropDownList)Master.FindControl("FINContent").FindControl("ddlSegment" + (i + 1));
                                if (ddlSegment != null)
                                {
                                    Segments_BLL.GetSegmentvalues(ref ddlSegment, dtdataAc.Rows[i]["SEGMENT_ID"].ToString());

                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_1.ToString() != string.Empty)
                                    {
                                        ddlSegment1.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_1.ToString();
                                    }
                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_2.ToString() != string.Empty)
                                    {
                                        ddlSegment2.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_2.ToString();
                                    }
                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_3.ToString() != string.Empty)
                                    {
                                        ddlSegment3.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_3.ToString();
                                    }
                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_4.ToString() != string.Empty)
                                    {
                                        ddlSegment4.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_4.ToString();
                                    }
                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_5.ToString() != string.Empty)
                                    {
                                        ddlSegment5.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_5.ToString();
                                    }
                                    if (((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_6.ToString() != string.Empty)
                                    {
                                        ddlSegment6.SelectedValue = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_SEGMENT_ID_6.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        protected void ddlSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtender2.Show();
        }
        private int GetAccCodeCount(DataSet dsAccChild)
        {
            string grpId = string.Empty;
            int count = 0;

            if (dsAccChild.Tables.Count > 0)
            {
                if (dsAccChild.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsAccChild.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsAccChild.Tables[0].Rows[i]["child1"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child1"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }

                        if (dsAccChild.Tables[0].Rows[i]["child2"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child2"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child3"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child3"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child4"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child4"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child5"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child5"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }

                    }

                    if (dsAccChild.Tables[0].Rows.Count > 1)
                    {
                        if (dsAccChild.Tables[0].Rows[0]["child6"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[0]["child6"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    count = dsAccCode.Tables[0].Rows.Count + count;
                                }
                            }
                        }
                    }
                }
            }
            return count;
        }
        private List<Tuple<object, string>> GetAccCodes(string nodeValue, List<Tuple<object, string>> tmpChildEntity, bool isAccCodes = false)
        {
            DataSet dsAccChild = new DataSet();
            DataSet dsAccCodeDirect = new DataSet();
            string grpId = string.Empty;

            int count = 0;
            decimal budAmt = 0;

            dsAccChild = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccGrpChildHierarchy(nodeValue));

            count = (GetAccCodeCount(dsAccChild));

            if (count > 0)
            {
                budAmt = CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text) / count;
            }
            else
            {
                budAmt = CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text);
            }

            if (Session["IsCodeORGroup"] != null)
            {
                IsCodeORGroup = Session["IsCodeORGroup"].ToString();
            }
            else
            {
                IsCodeORGroup = "0";
            }
            dsAccCodeDirect = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(nodeValue, IsCodeORGroup));

            if (dsAccCodeDirect.Tables.Count > 0)
            {
                if (dsAccCodeDirect.Tables[0].Rows.Count > 0)
                {
                    tmpChildEntity = GetData(dsAccCodeDirect, nodeValue, tmpChildEntity, budAmt);
                }
            }
            //}
            //else
            //{
            if (dsAccChild.Tables.Count > 0)
            {
                if (dsAccChild.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsAccChild.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsAccChild.Tables[0].Rows[i]["child1"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child1"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    tmpChildEntity = GetData(dsAccCode, nodeValue, tmpChildEntity, budAmt);
                                }
                            }
                        }

                        if (dsAccChild.Tables[0].Rows[i]["child2"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child2"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    tmpChildEntity = GetData(dsAccCode, nodeValue, tmpChildEntity, budAmt);
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child3"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child3"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    tmpChildEntity = GetData(dsAccCode, nodeValue, tmpChildEntity, budAmt);
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child4"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child4"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    tmpChildEntity = GetData(dsAccCode, nodeValue, tmpChildEntity, budAmt);
                                }
                            }
                        }
                        if (dsAccChild.Tables[0].Rows[i]["child5"].ToString() != string.Empty)
                        {
                            grpId = dsAccChild.Tables[0].Rows[i]["child5"].ToString();

                            dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(grpId));

                            if (dsAccCode.Tables.Count > 0)
                            {
                                if (dsAccCode.Tables[0].Rows.Count > 0)
                                {
                                    tmpChildEntity = GetData(dsAccCode, nodeValue, tmpChildEntity, budAmt);
                                }
                            }
                        }
                    }
                }
            }
            //}

            return tmpChildEntity;
        }
        private List<Tuple<object, string>> GetData(DataSet dsAccCode, string nodeValue, List<Tuple<object, string>> tmpChildEntity, decimal budAmt)
        {
            var tmpChildEntity1 = new List<Tuple<object, string>>();
            tmpChildEntity1.Clear();

            DataTable dtPeriods = new DataTable();
            DataTable dtSegments = new DataTable();


            try
            {
                ErrorCollection.Clear();

                //   dsAccCode = GetData(nodeValue, isAccCodes);

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        tmpChildEntity1 = new List<Tuple<object, string>>();


                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            gL_BUDGET_DTL = new GL_BUDGET_DTL();

                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                //GetAccGrpHierarchy(nodeValue);
                                GetAccCodeParent(dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());

                                gL_BUDGET_DTL.BUDGET_ACCT_CODE_ID = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                if (rblBudget.SelectedValue == "1")
                                {
                                    gL_BUDGET_DTL.IS_AMT_OR_PREV_YR = "1";
                                }
                                else
                                {
                                    gL_BUDGET_DTL.IS_AMT_OR_PREV_YR = "2";
                                }
                                gL_BUDGET_DTL.ACCT_CODE = txtAccountCode.Text;
                                gL_BUDGET_DTL.ACCT_CODE_DESC = txtAccountName.Text;
                                gL_BUDGET_DTL.PREV_YR_BUDGET_AMT = CommonUtils.ConvertStringToDecimal(txtPreBudAmt.Text);
                                gL_BUDGET_DTL.PREV_REV_BUDGET_AMT = CommonUtils.ConvertStringToDecimal(txtPreRevBudAmt.Text);
                                gL_BUDGET_DTL.BUDGET_AMOUNT = budAmt;// CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text);
                                gL_BUDGET_DTL.FINAL_BUDGET_AMOUNT = budAmt;
                                gL_BUDGET_DTL.ENTIRE_OR_ADDITIONAL = "0";

                                //for review the group budget amt...not for storage purpose
                                gL_BUDGET_DTL.PERCENTAGE_VAL = CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text);


                                gL_BUDGET_DTL.PERCENTAGE = CommonUtils.ConvertStringToDecimal(txtPer.Text);
                                gL_BUDGET_DTL.ACTUAL_PREVIOUS_AMOUNT = CommonUtils.ConvertStringToDecimal(txtPreYrActual.Text);
                                gL_BUDGET_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                                gL_BUDGET_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                {
                                    gL_BUDGET_DTL.BUDGET_CODE = Master.StrRecordId.ToString();
                                }
                                else
                                {
                                    if (Session["BudCode"] != null)
                                    {
                                        gL_BUDGET_HDR.BUDGET_CODE = Session["BudCode"].ToString();
                                        gL_BUDGET_DTL.BUDGET_CODE = gL_BUDGET_HDR.BUDGET_CODE;
                                    }
                                }
                                int iLoop = 0;

                                if (txtBudgetAmt.Text.Trim() != string.Empty)
                                {
                                    //for (int jj = 0; jj < int.Parse(Session["PeriodCount"].ToString()); jj++)
                                    //{

                                    //if (Session[iLoop + "_Periods"] != null)
                                    //{
                                    //    dtPeriods = (DataTable)Session[iLoop + "_Periods"];
                                    //}
                                    if (Session[nodeValue + "_Periods"] != null)
                                    {
                                        dtPeriods = (DataTable)Session[nodeValue + "_Periods"];
                                    }
                                    if (Session[nodeValue + "_Segments"] != null)
                                    {
                                        dtSegments = (DataTable)Session[nodeValue + "_Segments"];
                                    }
                                    //if (Session[iLoop + "_Segments"] != null)
                                    //{
                                    //    dtSegments = (DataTable)Session[iLoop + "_Segments"];
                                    //}

                                    if (dtPeriods != null)
                                    {
                                        if (dtPeriods.Rows.Count > 0)
                                        {
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_01 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_01"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_02 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_02"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_03 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_03"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_04 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_04"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_05 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_05"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_06 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_06"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_07 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_07"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_08 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_08"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_09 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_09"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_10 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_10"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_11 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_11"].ToString());
                                            gL_BUDGET_DTL.BUDGET_MONTH_AMT_12 = CommonUtils.ConvertStringToDecimal(dtPeriods.Rows[0]["BUDGET_MONTH_AMT_12"].ToString());
                                        }
                                    }
                                    //}
                                }
                                if (dtSegments != null)
                                {
                                    if (dtSegments.Rows.Count > 0)
                                    {
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_1 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString();
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_2 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString();
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_3 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString();
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_4 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString();
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_5 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString();
                                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_6 = dtSegments.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString();
                                    }
                                }


                                if (ErrorCollection.Count > 0)
                                {
                                    return tmpChildEntity;
                                }

                                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                {
                                    //if (Session["BudDtlId"] != null)
                                    //{
                                    //    gL_BUDGET_DTL.BUDGET_DTL_ID = Session["BudDtlId"].ToString();
                                    //}
                                    GL_BUDGET_DTL gL_BUDGET_DTLs = new GL_BUDGET_DTL();
                                    using (IRepository<GL_BUDGET_DTL> userCtx = new DataRepository<GL_BUDGET_DTL>())
                                    {
                                        gL_BUDGET_DTLs = userCtx.Find(r =>
                                            (r.BUDGET_ACCT_CODE_ID == gL_BUDGET_DTL.BUDGET_ACCT_CODE_ID && (r.ATTRIBUTE1 == gL_BUDGET_DTL.ATTRIBUTE1 || r.ATTRIBUTE2 == gL_BUDGET_DTL.ATTRIBUTE2 || r.ATTRIBUTE3 == gL_BUDGET_DTL.ATTRIBUTE3 || r.ATTRIBUTE4 == gL_BUDGET_DTL.ATTRIBUTE4 || r.ATTRIBUTE5 == gL_BUDGET_DTL.ATTRIBUTE5 || r.ATTRIBUTE6 == gL_BUDGET_DTL.ATTRIBUTE6 || r.ATTRIBUTE7 == gL_BUDGET_DTL.ATTRIBUTE7 || r.ATTRIBUTE8 == gL_BUDGET_DTL.ATTRIBUTE8 || r.ATTRIBUTE9 == gL_BUDGET_DTL.ATTRIBUTE9 || r.ATTRIBUTE10 == gL_BUDGET_DTL.ATTRIBUTE10) && r.BUDGET_CODE == Master.StrRecordId.ToString())
                                            ).SingleOrDefault();
                                    }
                                    if (gL_BUDGET_DTLs != null)
                                    {
                                        if (gL_BUDGET_DTLs.BUDGET_DTL_ID != null)
                                        {
                                            gL_BUDGET_DTL.BUDGET_DTL_ID = gL_BUDGET_DTLs.BUDGET_DTL_ID;

                                            gL_BUDGET_DTL.CREATED_BY = gL_BUDGET_DTLs.CREATED_BY;
                                            gL_BUDGET_DTL.CREATED_DATE = gL_BUDGET_DTLs.CREATED_DATE;
                                        }

                                        gL_BUDGET_DTL.MODIFIED_DATE = DateTime.Today;
                                        tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Update));
                                    }
                                    else
                                    {
                                        gL_BUDGET_DTL.BUDGET_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003_D.ToString(), false, true);
                                        gL_BUDGET_DTL.CREATED_BY = this.LoggedUserName;
                                        gL_BUDGET_DTL.CREATED_DATE = DateTime.Today;
                                        tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Add));
                                    }

                                }
                                else
                                {
                                    gL_BUDGET_DTL.BUDGET_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003_D.ToString(), false, true);
                                    gL_BUDGET_DTL.CREATED_BY = this.LoggedUserName;
                                    gL_BUDGET_DTL.CREATED_DATE = DateTime.Today;
                                    tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Add));
                                }
                            }
                        }
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return tmpChildEntity;
                }
                return tmpChildEntity;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
            return tmpChildEntity1;

        }

        private void GetAccCodeParent(string nodeValue)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtPeriods = new DataTable();
                DataTable dtSegments = new DataTable();

                dsAccCodeLevel = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodeParent(nodeValue));
                if (dsAccCodeLevel != null && dsAccCodeLevel.Tables.Count > 0)
                {
                    if (dsAccCodeLevel.Tables[0].Rows.Count > 0)
                    {
                        gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = dsAccCodeLevel.Tables[0].Rows[0]["level1_id"].ToString();

                        gL_BUDGET_DTL.ATTRIBUTE10 = dsAccCodeLevel.Tables[0].Rows[0]["level10_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE8 = dsAccCodeLevel.Tables[0].Rows[0]["level9_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE7 = dsAccCodeLevel.Tables[0].Rows[0]["level8_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE6 = dsAccCodeLevel.Tables[0].Rows[0]["level7_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE4 = dsAccCodeLevel.Tables[0].Rows[0]["level6_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE5 = dsAccCodeLevel.Tables[0].Rows[0]["level5_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE4 = dsAccCodeLevel.Tables[0].Rows[0]["level4_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE3 = dsAccCodeLevel.Tables[0].Rows[0]["level3_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE2 = dsAccCodeLevel.Tables[0].Rows[0]["level2_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE1 = dsAccCodeLevel.Tables[0].Rows[0]["level1_id"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void GetAccGrpHierarchy(string nodeValue)
        {
            try
            {
                ErrorCollection.Clear();
                dsAccCodeLevel = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccGrpHierarchy(nodeValue));
                if (dsAccCodeLevel != null && dsAccCodeLevel.Tables.Count > 0)
                {
                    if (dsAccCodeLevel.Tables[0].Rows.Count > 0)
                    {
                        gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = nodeValue;

                        gL_BUDGET_DTL.ATTRIBUTE10 = dsAccCodeLevel.Tables[0].Rows[0]["level10_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE8 = dsAccCodeLevel.Tables[0].Rows[0]["level9_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE7 = dsAccCodeLevel.Tables[0].Rows[0]["level8_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE6 = dsAccCodeLevel.Tables[0].Rows[0]["level7_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE4 = dsAccCodeLevel.Tables[0].Rows[0]["level6_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE5 = dsAccCodeLevel.Tables[0].Rows[0]["level5_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE4 = dsAccCodeLevel.Tables[0].Rows[0]["level4_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE3 = dsAccCodeLevel.Tables[0].Rows[0]["level3_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE2 = dsAccCodeLevel.Tables[0].Rows[0]["level2_id"].ToString();
                        gL_BUDGET_DTL.ATTRIBUTE1 = dsAccCodeLevel.Tables[0].Rows[0]["level1_id"].ToString();
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnAdd_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection = ValidateHeader();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();

                var tmpChildEntity = new List<Tuple<object, string>>();
                tmpChildEntity.Clear();

                if (Session["tmpChildEntity"] != null)
                {
                    tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];
                }

                if (tvModules != null)
                {
                    if (tvModules.SelectedNode.Value != null)
                    {


                        if (tvModules.SelectedNode.Depth == 5)
                        {
                            if (Session["Depth5"] != null)
                            {
                                if (Session["Level5"] != null)
                                {
                                    gL_BUDGET_DTL.BUDGET_ACCT_CODE_ID = tvModules.SelectedNode.Value;

                                    //if (tmpChildEntity.Count == 0)
                                    //{
                                    tmpChildEntity = GetAccCodes(tvModules.SelectedNode.Value, tmpChildEntity);
                                    //}
                                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = gL_BUDGET_DTL.ATTRIBUTE5;
                                }
                            }
                        }
                        else if (tvModules.SelectedNode.Depth == 4)
                        {
                            if (Session["Depth4"] != null)
                            {
                                if (Session["Level4"] != null)
                                {

                                    gL_BUDGET_DTL.ATTRIBUTE5 = Session["Level4"].ToString();

                                    //if (tmpChildEntity.Count == 0)
                                    //{
                                    // GetAccGrpHierarchy(Session["Level4"].ToString());
                                    tmpChildEntity = GetAccCodes(Session["Level4"].ToString(), tmpChildEntity);
                                    //  }
                                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = Session["Level4"].ToString();
                                }
                            }
                        }
                        else if (tvModules.SelectedNode.Depth == 3)
                        {
                            if (Session["Depth3"] != null)
                            {
                                if (Session["Level3"] != null)
                                {
                                    gL_BUDGET_DTL.ATTRIBUTE4 = Session["Level3"].ToString();


                                    // tmpChildEntity = GetAccCodeParent(tvModules.SelectedNode.Value, tmpChildEntity);
                                    //if (tmpChildEntity.Count == 0)
                                    //{
                                    // GetAccGrpHierarchy(Session["Level3"].ToString());
                                    tmpChildEntity = GetAccCodes(Session["Level3"].ToString(), tmpChildEntity);
                                    //}
                                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = Session["Level3"].ToString();
                                }
                            }
                        }
                        else if (tvModules.SelectedNode.Depth == 2)
                        {

                            if (Session["Depth2"] != null)
                            {
                                if (Session["Level2"] != null)
                                {
                                    gL_BUDGET_DTL.ATTRIBUTE3 = Session["Level2"].ToString();


                                    // tmpChildEntity = GetAccCodeParent(tvModules.SelectedNode.Value, tmpChildEntity);
                                    //if (tmpChildEntity.Count == 0)
                                    //{
                                    // GetAccGrpHierarchy(Session["Level2"].ToString());
                                    tmpChildEntity = GetAccCodes(Session["Level2"].ToString(), tmpChildEntity);

                                    //}
                                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = Session["Level2"].ToString();
                                }
                            }
                        }
                        else if (tvModules.SelectedNode.Depth == 1)
                        {
                            if (Session["Depth1"] != null)
                            {
                                if (Session["Level1"] != null)
                                {
                                    gL_BUDGET_DTL.ATTRIBUTE2 = Session["Level1"].ToString();



                                    // tmpChildEntity = GetAccCodeParent(tvModules.SelectedNode.Value, tmpChildEntity);
                                    //if (tmpChildEntity.Count == 0)
                                    //{
                                    // GetAccGrpHierarchy(Session["Level1"].ToString());
                                    tmpChildEntity = GetAccCodes(Session["Level1"].ToString(), tmpChildEntity);
                                    //}
                                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = Session["Level1"].ToString();
                                }
                            }
                        }
                        else if (tvModules.SelectedNode.Depth == 0)
                        {
                            if (Session["Depth0"] != null && Session["Level0"] != null)
                            {
                                gL_BUDGET_DTL.ATTRIBUTE1 = Session["Level0"].ToString();

                                //tmpChildEntity = GetAccCodeParent(tvModules.SelectedNode.Value, tmpChildEntity);
                                //if (tmpChildEntity.Count == 0)
                                //{
                                // GetAccGrpHierarchy(Session["Level0"].ToString());
                                tmpChildEntity = GetAccCodes(Session["Level0"].ToString(), tmpChildEntity);
                                //}
                                gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = Session["Level0"].ToString();
                            }
                        }
                    }
                }

                Session["tmpChildEntity"] = tmpChildEntity;


                txtBudgetAmt.Text = string.Empty;
                txtAccountCode.Text = string.Empty;
                txtAccountName.Text = string.Empty;
                txtPreBudAmt.Text = string.Empty;
                txtPreRevBudAmt.Text = string.Empty;
                txtPreYrActual.Text = string.Empty;



                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        private void AssignToBE()
        {
            try
            {
                DataTable dtPeriods = new DataTable();
                DataTable dtSegments = new DataTable();

                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_BUDGET_HDR = (GL_BUDGET_HDR)EntityData;
                }

                //gL_BUDGET_HDR.BUDGET_AMOUNT = CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text.ToString());
                //gL_BUDGET_HDR.ATTRIBUTE1 = txtAccountCode.Text;
                //gL_BUDGET_HDR.ATTRIBUTE2 = txtAccountName.Text;
                //gL_BUDGET_HDR.ATTRIBUTE3 = txtPer.Text;
                //gL_BUDGET_HDR.ATTRIBUTE4 = txtPreBudAmt.Text;
                //gL_BUDGET_HDR.ATTRIBUTE5 = txtPreRevBudAmt.Text;
                //gL_BUDGET_HDR.ATTRIBUTE6 = txtPreYrActual.Text;

                //gL_BUDGET_HDR.ATTRIBUTE7 = rblBudget.SelectedValue.ToString();


                gL_BUDGET_HDR.BUDGET_NAME = txtBudgetName.Text;
                gL_BUDGET_HDR.BUDGET_COMP = ddlCompanyName.SelectedValue;
                gL_BUDGET_HDR.BUDGET_TYPE = ddlBudgetType.SelectedValue;
                gL_BUDGET_HDR.CAL_DTL_ID = ddlCalyr.SelectedValue;
                gL_BUDGET_HDR.SEMENT_ID = ddlGlobalSegment.SelectedValue;

                if (txtFromDate.Text.Trim() != string.Empty)
                {
                    gL_BUDGET_HDR.BUDGET_PERIOD_START_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text.Trim() != string.Empty)
                {
                    gL_BUDGET_HDR.BUDGET_PERIOD_END_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                gL_BUDGET_HDR.ENABLED_FLAG = "1";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_BUDGET_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.MODIFIED_DATE = DateTime.Today;
                    gL_BUDGET_HDR.BUDGET_CODE = Master.StrRecordId.ToString();
                }
                else
                {
                    if (Session["BudCode"] != null)
                    {
                        gL_BUDGET_HDR.BUDGET_CODE = Session["BudCode"].ToString();
                    }
                    else
                    {
                        gL_BUDGET_HDR.BUDGET_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003.ToString(), false, true);
                    }

                    gL_BUDGET_HDR.CREATED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.CREATED_DATE = DateTime.Today;
                }
                gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_BUDGET_HDR.BUDGET_CODE.ToString());
                gL_BUDGET_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //Save Detail Table

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, gL_BUDGET_HDR.BUDGET_CODE, gL_BUDGET_HDR.BUDGET_COMP, gL_BUDGET_HDR.BUDGET_NAME, gL_BUDGET_HDR.BUDGET_TYPE);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("BUDssGET", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (Session["tmpChildEntity"] != null)
                {
                    tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private TreeNode FindMyNode(string searchstring)
        {
            try
            {

                for (int vLoop = 0; vLoop < tvModules.Nodes.Count; vLoop++)
                {
                    TreeNode trNode = tvModules.Nodes[vLoop];
                    if (trNode.Value == searchstring)
                        return trNode;
                    else
                    {
                        TreeNode trAnswerNode = FindChildNodes(trNode, searchstring);
                        if (trAnswerNode != null)
                            return trNode;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private TreeNode FindChildNodes(TreeNode trNode, string searchstring)
        {
            try
            {
                for (int vLoop = 0; vLoop < trNode.ChildNodes.Count; vLoop++)
                {
                    TreeNode trChildNode = trNode.ChildNodes[vLoop];
                    if (trChildNode.Value == searchstring)
                        return trNode;
                    else
                    {
                        TreeNode trAnswerNode = FindChildNodes(trChildNode, searchstring);
                        if (trAnswerNode != null)
                            return trNode;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<Tuple<object, string>> AssignToDtlBE(List<Tuple<object, string>> tmpChildEntity)
        {

            try
            {

                ErrorCollection.Clear();

                if (ErrorCollection.Count > 0)
                {
                    return tmpChildEntity;
                }

                return tmpChildEntity;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
            return tmpChildEntity;

        }
        private void FillComboBox()
        {
            // ComboFilling.fn_getGlobalSegment(ref ddlGlobalSegment);
            ComboFilling.fn_getOrganisationDetails(ref ddlCompanyName);

            //ComboFilling.fn_getBudgetType(ref ddlBudgetType);

            Lookup_BLL.GetLookUpValues(ref ddlBudgetType, "BT");
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalYear(ref ddlCalyr);
            //    FIN.BLL.GL.AccountingGroups_BLL.getGroupDetails(ref ddlAccountGroupFilter);
            ddlCompanyName.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlCompanyName.Enabled = false;


            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeswithOldAccCode(ref ddlAccountCode);

        }
        private void FillComboJEDetails()
        {
            ComboFilling.fn_getSegment1Details(ref ddlSegment1);
            ComboFilling.fn_getSegment2Details(ref ddlSegment2);
            ComboFilling.fn_getSegment3Details(ref ddlSegment3);
            ComboFilling.fn_getSegment4Details(ref ddlSegment4);
            ComboFilling.fn_getSegment5Details(ref ddlSegment5);
            ComboFilling.fn_getSegment6Details(ref ddlSegment6);
        }
        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtDat = new DataTable();

                DropDownList ddlAccountGroup = new DropDownList();
                DropDownList ddlAccount = new DropDownList();

                ddlAccount.ID = "ddlAccount";
                ddlAccountGroup.ID = "ddlAccountGroup";


                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {
                //        ddlAccount = (DropDownList)gvData.FooterRow.FindControl("ddlAccount");
                //        ddlAccountGroup = (DropDownList)gvData.FooterRow.FindControl("ddlAccountGroup");
                //    }
                //    else
                //    {
                //        ddlAccount = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccount");
                //        ddlAccountGroup = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountGroup");
                //    }
                //}
                //else
                //{
                //    ddlAccount = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccount");
                //    ddlAccountGroup = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountGroup");
                //}

                if (ddlAccount != null && ddlAccountGroup != null)
                {
                    FIN.BLL.GL.AccountCodes_BLL.getAccountBasedGroup(ref ddlAccount, ddlAccountGroup.SelectedValue.ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYacCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                // FillComboJEDetails();

                DropDownList ddlAccountGroup = tmpgvr.FindControl("ddlAccountGroup") as DropDownList;
                DropDownList ddlAccount = tmpgvr.FindControl("ddlAccount") as DropDownList;


                FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlAccountGroup);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAccount);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlAccountGroup.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.GROUP_ID].ToString();
                //    ddlAccount.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();



                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindSegmentBasedGroup(string codeId)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();

                if (codeId != string.Empty)
                {
                    dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccGroup(codeId)).Tables[0];
                    if (dtdataAc != null)
                    {
                        if (dtdataAc.Rows.Count > 0)
                        {
                            int j = dtdataAc.Rows.Count;

                            for (int i = 0; i < dtdataAc.Rows.Count; i++)
                            {
                                if (i == 0)
                                {
                                    //segment1
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment1, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment1, codeId, "0");
                                    }

                                }
                                if (i == 1)
                                {
                                    //segment2
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, "0");
                                    }
                                }
                                if (i == 2)
                                {
                                    //Segments3
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                    }
                                }
                                if (i == 3)
                                {
                                    //segment4
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                    }
                                }
                                if (i == 4)
                                {
                                    //segment5
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                    }
                                }
                                if (i == 5)
                                {
                                    //segment6
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                                    }
                                }
                            }
                            if (j == 1)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment2, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 2)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 3)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 4)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 5)
                            {
                                Segments_BLL.GetSegmentvaluesBasedGroupSegment(ref ddlSegment6, codeId, "0");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindSegmentCode(string codeId)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                //DropDownList ddlAccountCodes = new DropDownList();
                //ddlAccountCodes.ID = "ddlAccountCodes";

                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {

                //        ddlAccountCodes = (DropDownList)gvData.FooterRow.FindControl("ddlAccountCodes");
                //    }
                //    else
                //    {
                //        ddlAccountCodes = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccountCodes");
                //    }
                //}
                //else
                //{
                //    ddlAccountCodes = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccountCodes");
                //}

                if (codeId != string.Empty)
                {
                    //ddlAccountCodes.SelectedValueCODE_ID
                    //if (ddlAccountCodes.SelectedValue != null)
                    //{
                    dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(codeId)).Tables[0];
                    if (dtdataAc != null)
                    {
                        if (dtdataAc.Rows.Count > 0)
                        {
                            int j = dtdataAc.Rows.Count;

                            for (int i = 0; i < dtdataAc.Rows.Count; i++)
                            {
                                if (i == 0)
                                {
                                    //segment1
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, codeId, "0");
                                    }

                                }
                                if (i == 1)
                                {
                                    //segment2
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, "0");
                                    }
                                }
                                if (i == 2)
                                {
                                    //Segments3
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                    }
                                }
                                if (i == 3)
                                {
                                    //segment4
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                    }
                                }
                                if (i == 4)
                                {
                                    //segment5
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                    }
                                }
                                if (i == 5)
                                {
                                    //segment6
                                    if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, dtdataAc.Rows[i]["segment_id"].ToString());
                                    }
                                    else
                                    {
                                        Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                                    }
                                }
                            }
                            if (j == 1)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 2)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 3)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 4)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, codeId, "0");
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                            else if (j == 5)
                            {
                                Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, codeId, "0");
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnSegment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindSegmentValues();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnPeriodOk_Click(object sender, EventArgs e)
        {
            try
            {
             
                //ErrorCollection.Clear();
                Calculate_Difference();
                if (lblSumError.Text.Length > 0)
                {
                    mpePeriods.Show();
                    return;
                }

                if (ErrorCollection.Count > 0)
                {
                    mpePeriods.Show();
                    return;
                }

                int countValue = 0;
                DataTable dtDataPeriods = new DataTable();

                DataRow dtRow;

                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_01");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_02");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_03");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_04");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_05");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_06");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_07");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_08");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_09");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_10");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_11");
                dtDataPeriods.Columns.Add("BUDGET_MONTH_AMT_12");

                dtRow = dtDataPeriods.NewRow();
                dtRow["BUDGET_MONTH_AMT_01"] = txtPeriodPop1.Text;
                dtRow["BUDGET_MONTH_AMT_02"] = txtPeriodPop2.Text;
                dtRow["BUDGET_MONTH_AMT_03"] = txtPeriodPop3.Text;
                dtRow["BUDGET_MONTH_AMT_04"] = txtPeriodPop4.Text;
                dtRow["BUDGET_MONTH_AMT_05"] = txtPeriodPop5.Text;
                dtRow["BUDGET_MONTH_AMT_06"] = txtPeriodPop6.Text;
                dtRow["BUDGET_MONTH_AMT_07"] = txtPeriodPop7.Text;
                dtRow["BUDGET_MONTH_AMT_08"] = txtPeriodPop8.Text;
                dtRow["BUDGET_MONTH_AMT_09"] = txtPeriodPop9.Text;
                dtRow["BUDGET_MONTH_AMT_10"] = txtPeriodPop10.Text;
                dtRow["BUDGET_MONTH_AMT_11"] = txtPeriodPop11.Text;
                dtRow["BUDGET_MONTH_AMT_12"] = txtPeriodPop12.Text;

                dtDataPeriods.Rows.Add(dtRow);
                //if (Session["PeriodCount"] != null)
                //{
                //    countValue = int.Parse(Session["PeriodCount"].ToString());
                //}
                Session[tvModules.SelectedValue + "_Periods"] = null;
                Session[tvModules.SelectedValue + "_Periods"] = dtDataPeriods;
                //Session["PeriodCount"] = countValue + 1;

                mpePeriods.Hide();
                Session["errorPopup"] = null;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void fillperiods()
        {
            txtPeriodPop1.Text = string.Empty;
            txtPeriodPop2.Text = string.Empty;
            txtPeriodPop3.Text = string.Empty;
            txtPeriodPop4.Text = string.Empty;
            txtPeriodPop5.Text = string.Empty;
            txtPeriodPop6.Text = string.Empty;
            txtPeriodPop7.Text = string.Empty;
            txtPeriodPop8.Text = string.Empty;
            txtPeriodPop9.Text = string.Empty;
            txtPeriodPop10.Text = string.Empty;
            txtPeriodPop11.Text = string.Empty;
            txtPeriodPop12.Text = string.Empty;

            txtPrevPeriodPop1.Text = string.Empty;
            txtPrevPeriodPop2.Text = string.Empty;
            txtPrevPeriodPop3.Text = string.Empty;
            txtPrevPeriodPop4.Text = string.Empty;
            txtPrevPeriodPop5.Text = string.Empty;
            txtPrevPeriodPop6.Text = string.Empty;
            txtPrevPeriodPop7.Text = string.Empty;
            txtPrevPeriodPop8.Text = string.Empty;
            txtPrevPeriodPop9.Text = string.Empty;
            txtPrevPeriodPop10.Text = string.Empty;
            txtPrevPeriodPop11.Text = string.Empty;
            txtPrevPeriodPop12.Text = string.Empty;

            txtPrevPeriodPop1.Enabled = false;
            txtPrevPeriodPop2.Enabled = false;
            txtPrevPeriodPop3.Enabled = false;
            txtPrevPeriodPop4.Enabled = false;
            txtPrevPeriodPop5.Enabled = false;
            txtPrevPeriodPop6.Enabled = false;
            txtPrevPeriodPop7.Enabled = false;
            txtPrevPeriodPop8.Enabled = false;
            txtPrevPeriodPop9.Enabled = false;
            txtPrevPeriodPop10.Enabled = false;
            txtPrevPeriodPop11.Enabled = false;
            txtPrevPeriodPop12.Enabled = false;


            if (txtBudgetAmt.Text.Trim() != string.Empty)
            {
                Session["singlePeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text) / 12, 2);

                if (Session["singlePeriod"] != null)
                {
                    txtPeriodPop1.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop2.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop3.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop4.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop5.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop6.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop7.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop8.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop9.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop10.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop11.Text = Session["singlePeriod"].ToString();
                    txtPeriodPop12.Text = Session["singlePeriod"].ToString();
                }

                if (Session["Period1"] != null)
                {
                    txtPeriodPop1.Text = Session["Period1"].ToString();
                }
                if (Session["Period2"] != null)
                {
                    txtPeriodPop2.Text = Session["Period2"].ToString();
                }
                if (Session["Period3"] != null)
                {
                    txtPeriodPop3.Text = Session["Period3"].ToString();
                }
                if (Session["Period4"] != null)
                {
                    txtPeriodPop4.Text = Session["Period4"].ToString();
                }
                if (Session["Period5"] != null)
                {
                    txtPeriodPop5.Text = Session["Period5"].ToString();
                }
                if (Session["Period6"] != null)
                {
                    txtPeriodPop6.Text = Session["Period6"].ToString();
                }
                if (Session["Period7"] != null)
                {
                    txtPeriodPop7.Text = Session["Period7"].ToString();
                }
                if (Session["Period8"] != null)
                {
                    txtPeriodPop8.Text = Session["Period8"].ToString();
                }
                if (Session["Period9"] != null)
                {
                    txtPeriodPop9.Text = Session["Period9"].ToString();
                }
                if (Session["Period10"] != null)
                {
                    txtPeriodPop10.Text = Session["Period10"].ToString();
                }
                if (Session["Period11"] != null)
                {
                    txtPeriodPop11.Text = Session["Period11"].ToString();
                }
                if (Session["Period12"] != null)
                {
                    txtPeriodPop12.Text = Session["Period12"].ToString();
                }



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (Session["IsCodeORGroup"] != null)
                    {
                        if (Session["IsCodeORGroup"].ToString() == "1")
                        {
                            if (Session["AccCodes"] != null)
                            {
                                if (Session["AccCodes"].ToString() != string.Empty)
                                {
                                    dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId, Session["AccCodes"].ToString(), Session["IsCodeORGroup"].ToString());

                                    if (dtGridData.Rows.Count > 0)
                                    {
                                        txtPeriodPop1.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_01"].ToString();
                                        txtPeriodPop2.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_02"].ToString();
                                        txtPeriodPop3.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_03"].ToString();
                                        txtPeriodPop4.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_04"].ToString();
                                        txtPeriodPop5.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_05"].ToString();
                                        txtPeriodPop6.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_06"].ToString();
                                        txtPeriodPop7.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_07"].ToString();
                                        txtPeriodPop8.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_08"].ToString();
                                        txtPeriodPop9.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_09"].ToString();
                                        txtPeriodPop10.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_10"].ToString();
                                        txtPeriodPop11.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_11"].ToString();
                                        txtPeriodPop12.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_12"].ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Session["AccGroups"] != null)
                            {
                                if (Session["AccGroups"].ToString() != string.Empty)
                                {
                                    dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId, Session["AccGroups"].ToString(), Session["IsCodeORGroup"].ToString());

                                    if (dtGridData.Rows.Count > 0)
                                    {
                                        txtPeriodPop1.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_01"].ToString();
                                        txtPeriodPop2.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_02"].ToString();
                                        txtPeriodPop3.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_03"].ToString();
                                        txtPeriodPop4.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_04"].ToString();
                                        txtPeriodPop5.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_05"].ToString();
                                        txtPeriodPop6.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_06"].ToString();
                                        txtPeriodPop7.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_07"].ToString();
                                        txtPeriodPop8.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_08"].ToString();
                                        txtPeriodPop9.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_09"].ToString();
                                        txtPeriodPop10.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_10"].ToString();
                                        txtPeriodPop11.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_11"].ToString();
                                        txtPeriodPop12.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_12"].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {

                    var tmpChildEntity = new List<Tuple<object, string>>();

                    if (Session["tmpChildEntity"] != null)
                    {
                        tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];

                        if (tmpChildEntity != null)
                        {
                            if (tmpChildEntity.Count > 0)
                            {
                                for (int i = 0; i < tmpChildEntity.Count; i++)
                                {
                                    if (tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_CODE_ID || tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_GRP_ID)
                                    {
                                        txtPeriodPop1.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_01.ToString();
                                        txtPeriodPop2.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_02.ToString();
                                        txtPeriodPop3.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_03.ToString();
                                        txtPeriodPop4.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_04.ToString();
                                        txtPeriodPop5.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_05.ToString();
                                        txtPeriodPop6.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_06.ToString();
                                        txtPeriodPop7.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_07.ToString();
                                        txtPeriodPop8.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_08.ToString();
                                        txtPeriodPop9.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_09.ToString();
                                        txtPeriodPop10.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_10.ToString();
                                        txtPeriodPop11.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_11.ToString();
                                        txtPeriodPop12.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_MONTH_AMT_12.ToString();
                                    }
                                }
                            }
                        }
                    }
                }

                //if (Session[tvModules.SelectedValue + "Period1"] != null)
                //{
                //    txtPeriodPop1.Text = Session[tvModules.SelectedValue + "Period1"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period2"] != null)
                //{
                //    txtPeriodPop2.Text = Session[tvModules.SelectedValue + "Period2"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period3"] != null)
                //{
                //    txtPeriodPop3.Text = Session[tvModules.SelectedValue + "Period3"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period4"] != null)
                //{
                //    txtPeriodPop4.Text = Session[tvModules.SelectedValue + "Period4"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period5"] != null)
                //{
                //    txtPeriodPop5.Text = Session[tvModules.SelectedValue + "Period5"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period6"] != null)
                //{
                //    txtPeriodPop6.Text = Session[tvModules.SelectedValue + "Period6"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period7"] != null)
                //{
                //    txtPeriodPop7.Text = Session[tvModules.SelectedValue + "Period7"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period8"] != null)
                //{
                //    txtPeriodPop8.Text = Session[tvModules.SelectedValue + "Period8"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period9"] != null)
                //{
                //    txtPeriodPop9.Text = Session[tvModules.SelectedValue + "Period9"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period10"] != null)
                //{
                //    txtPeriodPop10.Text = Session[tvModules.SelectedValue + "Period10"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period11"] != null)
                //{
                //    txtPeriodPop11.Text = Session[tvModules.SelectedValue + "Period11"].ToString();
                //}
                //if (Session[tvModules.SelectedValue + "Period12"] != null)
                //{
                //    txtPeriodPop12.Text = Session[tvModules.SelectedValue + "Period12"].ToString();
                //}

            }
            else
            {
                Session["singlePeriod"] = null;
            }

            if (txtPreYrActual.Text.Trim() != string.Empty)
            {
                if (Session["PrevPeriod"] != null)
                {
                    txtPrevPeriodPop1.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop2.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop3.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop4.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop5.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop6.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop7.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop8.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop9.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop10.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop11.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                    txtPrevPeriodPop12.Text = DBMethod.GetAmtDecimalCommaSeparationValue(Session["PrevPeriod"].ToString());
                }
            }
            else
            {
                Session["PrevPeriod"] = null;
            }

            if (txtPeriodPop1.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop1.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop1.Text.ToString());
            }

            if (txtPeriodPop2.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop2.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop2.Text.ToString());
            }

            if (txtPeriodPop3.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop3.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop3.Text.ToString());
            }

            if (txtPeriodPop4.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop4.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop4.Text.ToString());
            }
            if (txtPeriodPop5.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop5.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop5.Text.ToString());
            }
            if (txtPeriodPop6.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop6.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop6.Text.ToString());
            }
            if (txtPeriodPop7.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop7.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop7.Text.ToString());
            }
            if (txtPeriodPop8.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop8.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop8.Text.ToString());
            }
            if (txtPeriodPop9.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop9.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop9.Text.ToString());
            }
            if (txtPeriodPop10.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop10.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop10.Text.ToString());
            }

            if (txtPeriodPop11.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop11.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop11.Text.ToString());
            }
            if (txtPeriodPop12.Text.ToString().Trim().Length > 0)
            {
                txtPeriodPop12.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPeriodPop12.Text.ToString());
            }
        }


        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                // ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Budget Entry Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR, true);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnOkay_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtDataPeriods = new DataTable();

                DataRow dtRow;

                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_1");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_2");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_3");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_4");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_5");
                dtDataPeriods.Columns.Add("BUDGET_SEGMENT_ID_6");


                dtRow = dtDataPeriods.NewRow();

                dtRow["BUDGET_SEGMENT_ID_1"] = ddlSegment1.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_2"] = ddlSegment2.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_3"] = ddlSegment3.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_4"] = ddlSegment4.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_5"] = ddlSegment5.SelectedValue;
                dtRow["BUDGET_SEGMENT_ID_6"] = ddlSegment6.SelectedValue;

                dtDataPeriods.Rows.Add(dtRow);

                Session[tvModules.SelectedValue + "_Segments"] = null;
                Session[tvModules.SelectedValue + "_Segments"] = dtDataPeriods;

                ModalPopupExtender2.Hide();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdfYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtAnnualBudgetAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (txtBudgetAmt.Text.Trim() != string.Empty)
                {
                    Session["singlePeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text) / 12, 2);
                    btnBudgetPeriods.Enabled = true;
                    fillperiods();
                }
                else
                {
                    Session["singlePeriod"] = null;
                    btnBudgetPeriods.Enabled = false;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                string accGroup = tvModules.SelectedValue;

                if (Session["tmpChildEntity"] != null)
                {
                    tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];

                    if (tmpChildEntity.Count > 0)
                    {
                        for (int i = 0; i <= tmpChildEntity.Count; i++)
                        {
                            if (i == 0)
                            {
                                if (tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_CODE_ID)
                                {
                                    tmpChildEntity.RemoveAt(i);
                                }
                                if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE1 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE2 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE3 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE4 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE5 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE6 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE7 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE8 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE9 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE10)
                                {
                                    tmpChildEntity.RemoveAt(i);
                                }
                            }

                            if (i > 0)
                            {
                                if (tvModules.SelectedValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i - 1].Item1)).BUDGET_ACCT_CODE_ID)
                                {
                                    tmpChildEntity.RemoveAt(i - 1);
                                }

                                int j = i - 1;

                                if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE1 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE2 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE3 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE4 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE5 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE6 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE7 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE8 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE9 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[j].Item1)).ATTRIBUTE10)
                                {
                                    tmpChildEntity.RemoveAt(i - 1);
                                }
                            }
                        }
                        if (tmpChildEntity.Count > 0)
                        {
                            Session["tmpChildEntity"] = tmpChildEntity;
                        }
                        else
                        {
                            Session["tmpChildEntity"] = null;
                        }
                    }
                }

                //if (Master.Mode == FINAppConstants.Update)
                //{
                //    hdRowIndex.Value = gvr.RowIndex.ToString();
                //    Session[hdRowIndex.Value + "_annualAmt"] = true;
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void ClearGrid()
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                //gvData.DataSource = dtGridData;
                //gvData.DataBind();

                //dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                //BindGrid(dtGridData);

            }
        }
        protected void ddlAccountGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //  dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetailsBasedOrg(VMVServices.Web.Utils.OrganizationID, ddlAccountGroupFilter.SelectedValue.ToString());
                if (dtGridData != null)
                {
                    if (dtGridData.Rows.Count > 0)
                    {
                        //  BindGrid(dtGridData);
                    }
                    else
                    {
                        ClearGrid();
                    }
                }
                else
                {
                    ClearGrid();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void rblAccCodeGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList ddlAccount = new DropDownList();
                Label lblAccountCodeName = new Label();

                ddlAccount.ID = "ddlAccount";
                lblAccountCodeName.ID = "lblAccountCodeName";

                //if (gvData.FooterRow != null)
                //{
                //    if (gvData.EditIndex < 0)
                //    {
                //        lblAccountCodeName = (Label)gvData.FooterRow.FindControl("lblAccountCodeName");
                //        ddlAccount = (DropDownList)gvData.FooterRow.FindControl("ddlAccount");
                //    }
                //    else
                //    {
                //        lblAccountCodeName = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAccountCodeName");
                //        ddlAccount = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccount");
                //    }
                //}
                //else
                //{
                //    lblAccountCodeName = (Label)gvData.Controls[0].Controls[1].FindControl("lblAccountCodeName");
                //    ddlAccount = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccount");
                //}



                //if (rblAccCodeGroup.SelectedValue == "1")
                //{
                //    ddlAccount.Visible = false;
                //    lblAccountCodeName.Visible = false;

                //    dtGridData = FIN.BLL.GL.Budget_BLL.getGroupDetails();
                //    //  BindGrid(dtGridData);
                //    divFilter.Visible = false;
                //}
                //else if (rblAccCodeGroup.SelectedValue == "2")
                //{
                //    ddlAccount.Visible = true;
                //    lblAccountCodeName.Visible = true;

                //    dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetails();
                //    //   BindGrid(dtGridData);
                //    divFilter.Visible = true;
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetExntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void GetNodeAmt(string nodeValue, string IsCodeORGroup)
        {
            try
            {

                ErrorCollection.Clear();

                string accCode = string.Empty;
                string accGroup = string.Empty;
                DataSet dsAccCodeDirect = new DataSet();

                var tmpChildEntity = new List<Tuple<object, string>>();


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    dtGridData = FIN.BLL.GL.Budget_BLL.getBudgetDetails(Master.StrRecordId, nodeValue, Session["IsCodeORGroup"].ToString());

                    if (dtGridData.Rows.Count > 0)
                    {
                        txtBudgetAmt.Text = string.Empty;
                        for (int k = 0; k <= dtGridData.Rows.Count - 1; k++)
                        {
                            txtBudgetAmt.Text = Math.Ceiling(Math.Round(CommonUtils.ConvertStringToDecimal(dtGridData.Rows[k]["BUDGET_AMOUNT"].ToString()) + CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text), 2)).ToString();
                        }

                        txtPeriodPop1.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_01"].ToString();
                        txtPeriodPop2.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_02"].ToString();
                        txtPeriodPop3.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_03"].ToString();
                        txtPeriodPop4.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_04"].ToString();
                        txtPeriodPop5.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_05"].ToString();
                        txtPeriodPop6.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_06"].ToString();
                        txtPeriodPop7.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_07"].ToString();
                        txtPeriodPop8.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_08"].ToString();
                        txtPeriodPop9.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_09"].ToString();
                        txtPeriodPop10.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_10"].ToString();
                        txtPeriodPop11.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_11"].ToString();
                        txtPeriodPop12.Text = dtGridData.Rows[0]["BUDGET_MONTH_AMT_12"].ToString();

                        Session["BudDtlId"] = dtGridData.Rows[0]["BUDGET_DTL_ID"].ToString();

                        if (dtGridData.Rows[0]["IS_AMT_OR_PREV_YR"].ToString() == "1")
                        {
                            rblBudget.SelectedValue = "1";
                        }
                        else
                        {
                            rblBudget.SelectedValue = "2";
                        }

                        txtAccountCode.Text = dtGridData.Rows[0]["ACCT_CODE"].ToString();
                        txtAccountName.Text = dtGridData.Rows[0]["ACCT_CODE_DESC"].ToString();
                        txtPer.Text = dtGridData.Rows[0]["PERCENTAGE"].ToString();
                        txtPreBudAmt.Text = dtGridData.Rows[0]["PREV_YR_BUDGET_AMT"].ToString();
                        txtPreRevBudAmt.Text = dtGridData.Rows[0]["PREV_REV_BUDGET_AMT"].ToString();
                        txtPreYrActual.Text = dtGridData.Rows[0]["ACTUAL_PREVIOUS_AMOUNT"].ToString();


                        BindSegmentValues();

                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString() != string.Empty)
                        {
                            ddlSegment1.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_1"].ToString();
                        }
                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString() != string.Empty)
                        {
                            ddlSegment2.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_2"].ToString();
                        }
                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString() != string.Empty)
                        {
                            ddlSegment3.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_3"].ToString();
                        }
                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString() != string.Empty)
                        {
                            ddlSegment4.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_4"].ToString();
                        }
                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString() != string.Empty)
                        {
                            ddlSegment5.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_5"].ToString();
                        }
                        if (dtGridData.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString() != string.Empty)
                        {
                            ddlSegment6.SelectedValue = dtGridData.Rows[0]["BUDGET_SEGMENT_ID_6"].ToString();
                        }
                    }
                }

                if (Session["tmpChildEntity"] != null)
                {
                    tmpChildEntity = (List<Tuple<object, string>>)Session["tmpChildEntity"];

                    if (tmpChildEntity != null)
                    {
                        if (tmpChildEntity.Count > 0)
                        {
                            for (int i = 0; i < tmpChildEntity.Count; i++)
                            {
                                if (nodeValue == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_ACCT_CODE_ID)
                                {
                                    txtBudgetAmt.Text = Math.Round(CommonUtils.ConvertStringToDecimal(((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_AMOUNT.ToString()), 2).ToString();
                                }

                                accGroup = nodeValue;

                                if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE1 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE2 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE3 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE4 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE5 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE6 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE7 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE8 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE9 || accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE10)
                                {
                                    txtBudgetAmt.Text = Math.Ceiling(Math.Round(CommonUtils.ConvertStringToDecimal(((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_AMOUNT.ToString()) + CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text), 2)).ToString();

                                    //dsAccCodeDirect = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(nodeValue, IsCodeORGroup));

                                    //if (dsAccCodeDirect.Tables.Count > 0)
                                    //{
                                    //    if (dsAccCodeDirect.Tables[0].Rows.Count > 0)
                                    //    {
                                    //        //txtBudgetAmt.Text = Math.Round(CommonUtils.ConvertStringToDecimal(((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).PERCENTAGE_VAL.ToString()), 2).ToString();

                                    //    }
                                    //}
                                    //}
                                }
                            }
                        }
                    }
                }
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).ATTRIBUTE2)
                //{
                //    txtBudgetAmt.Text = ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[i].Item1)).BUDGET_AMOUNT.ToString();
                //    //ErrorCollection.Add("exist2", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE3)
                //{
                //    ErrorCollection.Add("exist3", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE4)
                //{
                //    ErrorCollection.Add("exist4", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE5)
                //{
                //    ErrorCollection.Add("exist5", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE6)
                //{
                //    ErrorCollection.Add("exist6", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE7)
                //{
                //    ErrorCollection.Add("exist7", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE8)
                //{
                //    ErrorCollection.Add("exist8", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE9)
                //{
                //    ErrorCollection.Add("exist9", "Amt exists");
                //}
                //else if (accGroup == ((FIN.DAL.GL_BUDGET_DTL)(tmpChildEntity[0].Item1)).ATTRIBUTE10)
                //{
                //    ErrorCollection.Add("exist10", "Amt exists");
                //}




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void tvModules_SelectedNodeChanged(object sender, EventArgs e)
        {
            Session["errorPopup"] = null;
            mpePeriods.Hide();
            GetTVEvents();
        }
        private void GetTVEvents()
        {
            try
            {
                ErrorCollection.Clear();

                decimal CalculateAmount = 0;

                btnAdd.Enabled = true;
                btnBudgetPeriods.Enabled = true;
                btnBudgetSegment.Enabled = true;

                Session["Period1"] = null;
                Session["Period2"] = null;
                Session["Period3"] = null;
                Session["Period4"] = null;
                Session["Period5"] = null;
                Session["Period6"] = null;
                Session["Period7"] = null;
                Session["Period8"] = null;
                Session["Period9"] = null;
                Session["Period10"] = null;
                Session["Period11"] = null;
                Session["Period12"] = null;

                if (tvModules != null)
                {
                    txtBudgetAmt.Text = string.Empty;
                    txtPreBudAmt.Text = string.Empty;
                    txtAccountCode.Text = string.Empty;
                    txtAccountName.Text = string.Empty;

                    if (tvModules.SelectedNode.Depth == 0)
                    {
                        Session["Depth0"] = null;
                        Session["Depth1"] = null;
                        Session["Depth2"] = null;
                        Session["Depth3"] = null;
                        Session["Depth4"] = null;
                        Session["Depth5"] = null;

                        Session["Level0"] = null;
                        Session["Level1"] = null;
                        Session["Level2"] = null;
                        Session["Level3"] = null;
                        Session["Level4"] = null;
                        Session["Level5"] = null;

                        Session["Depth0"] = "Depth0";
                        Session["Level0"] = tvModules.SelectedNode.Value;
                    }
                    if (tvModules.SelectedNode.Depth == 1)
                    {
                        Session["Depth1"] = null;
                        Session["Depth2"] = null;
                        Session["Depth3"] = null;
                        Session["Depth4"] = null;
                        Session["Depth5"] = null;

                        Session["Level1"] = null;
                        Session["Level2"] = null;
                        Session["Level3"] = null;
                        Session["Level4"] = null;
                        Session["Level5"] = null;

                        Session["Depth1"] = "Depth1";
                        Session["Level1"] = tvModules.SelectedNode.Value;
                    }
                    if (tvModules.SelectedNode.Depth == 2)
                    {
                        Session["Depth2"] = null;
                        Session["Depth3"] = null;
                        Session["Depth4"] = null;
                        Session["Depth5"] = null;

                        Session["Level2"] = null;
                        Session["Level3"] = null;
                        Session["Level4"] = null;
                        Session["Level5"] = null;

                        Session["Depth2"] = "Depth2";
                        Session["Level2"] = tvModules.SelectedNode.Value;
                    }
                    if (tvModules.SelectedNode.Depth == 3)
                    {
                        Session["Depth3"] = null;
                        Session["Depth4"] = null;
                        Session["Depth5"] = null;

                        Session["Level3"] = null;
                        Session["Level4"] = null;
                        Session["Level5"] = null;

                        Session["Depth3"] = "Depth3";
                        Session["Level3"] = tvModules.SelectedNode.Value;
                    }
                    if (tvModules.SelectedNode.Depth == 4)
                    {

                        Session["Depth4"] = null;
                        Session["Depth5"] = null;

                        Session["Level4"] = null;
                        Session["Level5"] = null;

                        Session["Depth4"] = "Depth4";
                        Session["Level4"] = tvModules.SelectedNode.Value;
                    }
                    if (tvModules.SelectedNode.Depth == 5)
                    {
                        Session["Depth5"] = null;

                        Session["Level5"] = null;

                        Session["Depth5"] = "Depth5";
                        Session["Level5"] = tvModules.SelectedNode.Value;
                    }

                    DataTable dtDatas = new DataTable();
                    IsCodeORGroup = DBMethod.GetStringValue(AccountingGroups_DAL.IsCodeORGroup(tvModules.SelectedNode.Value));
                    Session["IsCodeORGroup"] = IsCodeORGroup;


                    GetNodeAmt(tvModules.SelectedNode.Value, IsCodeORGroup);

                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }



                    divAccCode.Visible = true;
                    txtAccountCode.Visible = true;

                    if (IsCodeORGroup == "1")
                    {
                        divAccCode.InnerHtml = "Account Code";
                        divAccName.InnerHtml = "Account Name";

                        Session["AccCodes"] = tvModules.SelectedNode.Value;

                        dtDatas = DBMethod.ExecuteQuery(AccountCodes_DAL.getAccCodeName(tvModules.SelectedNode.Value)).Tables[0];
                        if (dtDatas != null)
                        {
                            if (dtDatas.Rows.Count > 0)
                            {
                                txtAccountCode.Text = dtDatas.Rows[0]["CODE_NAME"].ToString();
                                txtAccountName.Text = dtDatas.Rows[0]["acct_code_desc"].ToString();
                            }
                        }
                    }
                    else if (IsCodeORGroup == "0")
                    {

                        Session["AccGroups"] = tvModules.SelectedNode.Value;

                        divAccCode.InnerHtml = "Account Group Code";
                        divAccName.InnerHtml = "Account Group Name";

                        dtDatas = DBMethod.ExecuteQuery(AccountingGroups_DAL.getGroupName(tvModules.SelectedNode.Value)).Tables[0];
                        if (dtDatas != null)
                        {
                            if (dtDatas.Rows.Count > 0)
                            {
                                txtAccountCode.Text = dtDatas.Rows[0]["GROUP_ID"].ToString();
                                txtAccountName.Text = dtDatas.Rows[0]["ACCT_GRP_DESC"].ToString();
                            }
                        }
                        divAccCode.Visible = false;
                        txtAccountCode.Visible = false;

                    }

                    txtPreYrActual.Text = "0";

                    if (tvModules.SelectedNode.Value != null)
                    {

                        string prevYrId = string.Empty;

                        if (ddlCalyr.SelectedValue != null)
                        {
                            prevYrId = DBMethod.GetStringValue(AccountingGroups_DAL.GetPreviousYear(ddlCalyr.SelectedValue.ToString()));
                        }


                        //Current Year Budget data.....
                        DataTable dtGridCurrentData = new DataTable();
                        dtGridCurrentData = FIN.BLL.GL.Budget_BLL.GetGroupAcCodeAmt(tvModules.SelectedNode.Value, ddlCalyr.SelectedValue, IsCodeORGroup, Master.StrRecordId);
                        txtBudgetAmt.Text = "";
                        if (dtGridCurrentData != null)
                        {
                            if (dtGridCurrentData.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtGridCurrentData.Rows.Count; i++)
                                {
                                    txtBudgetAmt.Text = Math.Ceiling(Math.Round(CommonUtils.ConvertStringToDecimal(dtGridCurrentData.Rows[i]["BUDGET_AMOUNT"].ToString()) + CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text), 2)).ToString();

                                }
                            }
                        }
                        if (txtBudgetAmt.Text.Length > 0)
                        {
                            txtBudgetAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBudgetAmt.Text);
                        }
                        //Previous Year Budget data.....
                        dtGridData = FIN.BLL.GL.Budget_BLL.GetGroupAcCodeAmt(tvModules.SelectedNode.Value, prevYrId, IsCodeORGroup);//.Replace(tvModules.SelectedNode.Value.Substring(0, tvModules.SelectedNode.Value.IndexOf("ACC")), ""));

                        dtGS = FIN.BLL.GL.Budget_BLL.GetGroupAcCodeJournalAmt(tvModules.SelectedNode.Value, prevYrId, IsCodeORGroup);//.Replace(tvModules.SelectedNode.Value.Substring(0, tvModules.SelectedNode.Value.IndexOf("ACC")), ""));

                        if (dtGS != null)
                        {
                            if (dtGS.Rows.Count > 0)
                            {
                                txtPreYrActual.Text = "0";

                                for (int i = 0; i < dtGS.Rows.Count; i++)
                                {
                                    if (dtGS.Rows[i]["ACTUAL_PREVIOUS_AMOUNT"].ToString() != null)
                                    {
                                        CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGS.Rows[i]["ACTUAL_PREVIOUS_AMOUNT"].ToString());
                                        txtPreYrActual.Text = CalculateAmount.ToString();
                                        Session["PrevPeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtPreYrActual.Text) / 12, 2);
                                    }
                                }
                            }
                        }
                        if (txtPreYrActual.Text.Length > 0)
                        {
                            txtPreYrActual.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPreYrActual.Text);
                        }
                        CalculateAmount = 0;

                        if (dtGridData != null)
                        {
                            if (dtGridData.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtGridData.Rows.Count; i++)
                                {
                                    if (dtGridData.Rows[i]["ACTUAL_PREVIOUS_AMOUNT"].ToString() != null)
                                    {
                                        CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["ACTUAL_PREVIOUS_AMOUNT"].ToString());
                                        txtPreBudAmt.Text = CalculateAmount.ToString();
                                    }
                                }
                            }
                        }
                        if (txtPreBudAmt.Text.Length > 0)
                        {
                            txtPreBudAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPreBudAmt.Text);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
        {
            ImageButton imgShowHide = (sender as ImageButton);
            GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
            if (imgShowHide.CommandArgument == "Show")
            {
                row.FindControl("pnlOrders").Visible = true;
                imgShowHide.CommandArgument = "Hide";
                imgShowHide.ImageUrl = "~/images/minus.png";
                string customerId = string.Empty;
                // = gvOrders.DataKeys[row.RowIndex].Value.ToString();
                //GridView gvOrders = row.FindControl("gvOrders") as GridView;
                //  BindOrders(customerId, gvOrders);
            }
            else
            {
                row.FindControl("pnlOrders").Visible = false;
                imgShowHide.CommandArgument = "Show";
                imgShowHide.ImageUrl = "~/images/plus.png";
            }
        }

        private void BindOrders(string customerId, GridView gvOrders)
        {
            //  gvOrders.ToolTip = customerId;
            DataSet dsModuleList = new DataSet();
            dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.GetAccountGroupLinkWithCodeName());

            gvOrders.DataSource = dsModuleList;
            gvOrders.DataBind();
        }
        private Dictionary<string, bool> GetSelectedMenus()
        {
            Dictionary<string, bool> dictionary = new Dictionary<string, bool>();

            foreach (TreeNode node in tvModules.Nodes)
            {
                if (node.Selected)
                    dictionary.Add(node.Value, node.Selected);
                GetSelectedChildMenus(dictionary, node);
            }

            return dictionary;
        }

        private void GetSelectedChildMenus(Dictionary<string, bool> dictionary, TreeNode node)
        {
            foreach (TreeNode childnode in node.ChildNodes)
            {
                if (childnode.Selected)
                    dictionary.Add(childnode.Value, childnode.Selected);
                GetSelectedSubChildMenus(dictionary, childnode);
            }
        }
        private void GetSelectedSubChildMenus(Dictionary<string, bool> dictionary, TreeNode node)
        {
            foreach (TreeNode childnode in node.ChildNodes)
            {
                if (childnode.Selected)
                {
                    dictionary.Add(childnode.Value, childnode.Selected);
                    dtGridData = FIN.BLL.GL.Budget_BLL.GetGroupAcCodeAmt(childnode.Value);
                }
                GetSelectedSubSubChildMenus(dictionary, childnode);
            }
        }
        private void GetSelectedSubSubChildMenus(Dictionary<string, bool> dictionary, TreeNode node)
        {
            foreach (TreeNode childnode in node.ChildNodes)
            {
                if (childnode.Selected)
                {
                    dictionary.Add(childnode.Value, childnode.Selected);
                    dtGridData = FIN.BLL.GL.Budget_BLL.GetGroupAcCodeAmt(childnode.Value);
                }
                GetSelectedSubSubSubChildMenus(dictionary, childnode);

            }
        }
        private void GetSelectedSubSubSubChildMenus(Dictionary<string, bool> dictionary, TreeNode node)
        {
            foreach (TreeNode childnode in node.ChildNodes)
            {
                if (childnode.Selected)
                    dictionary.Add(childnode.Value, childnode.Selected);
                dtGridData = FIN.BLL.GL.Budget_BLL.getGroupAcCodeDetails();

            }
        }
        #endregion

        protected void tvModules_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {

        }

        protected void tvModules_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            // string values = tvModules.SelectedNode.Parent.Value;
        }

        protected void rblBudget_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblBudget.SelectedValue == "1")
            {
                txtBudgetAmt.Enabled = true;
                txtPer.Enabled = false;
                divPers.Visible = false;
            }
            else
            {
                txtBudgetAmt.Enabled = false;
                txtPer.Enabled = true;
                divPers.Visible = true;
            }
        }

        protected void txtPer_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                txtBudgetAmt.Text = (CommonUtils.ConvertStringToDecimal(txtPer.Text) * CommonUtils.ConvertStringToDecimal(txtPreYrActual.Text)).ToString();


                if (txtBudgetAmt.Text.Trim() != string.Empty)
                {
                    Session["singlePeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text) / 12, 2);
                    btnBudgetPeriods.Enabled = true;
                    fillperiods();
                }
                else
                {
                    Session["singlePeriod"] = null;
                    btnBudgetPeriods.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYeCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnBudgetSegment_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection = ValidateHeader();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();
                BindSegmentValues();
                ModalPopupExtender2.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYeCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private System.Collections.SortedList ValidateHeader()
        {
            try
            {
                ErrorCollection.Clear();

                if (txtBudgetName.Text == string.Empty)
                {
                    ErrorCollection.Add("BUdn", "Budget Name cannot be empty");
                }
                if (ddlBudgetType.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("BUdn1", "Budget Type cannot be empty");
                }
                if (ddlGlobalSegment.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("BUdn2", "Global Segment cannot be empty");
                }
                if (txtFromDate.Text == string.Empty)
                {
                    ErrorCollection.Add("BUdn3", "From Date cannot be empty");
                }
                if (ddlCalyr.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("BUdn4", "Accounting Year cannot be empty");
                }

                return ErrorCollection;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYeCs", ex.Message);
                return ErrorCollection;
            }

        }
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection = ValidateHeader();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();
                txtSearch.Text = txtSearch.Text.Replace(" ", "");
                if (txtSearch.Text != string.Empty)
                {
                    foreach (TreeNode node in tvModules.Nodes)
                    {
                        //foreach (node.ChildNodes in node)
                        //{
                        if (node.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToUpper()) || node.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToUpper()))
                        {
                            node.Selected = true;
                            node.Expanded = true;
                            GetTVEvents();
                            return;
                        }
                        if (node.ChildNodes.Count >= 0)
                        {
                            for (int vLoop = 0; vLoop <= node.ChildNodes.Count - 1; vLoop++)
                            {
                                TreeNode firstChildNode = node.ChildNodes[vLoop];
                                if (firstChildNode.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()) || firstChildNode.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()))
                                {

                                    firstChildNode.Selected = true;
                                    node.Expanded = true;
                                    firstChildNode.Expanded = true;
                                    GetTVEvents();
                                    return;
                                }

                                if (firstChildNode.ChildNodes.Count >= 0)
                                {
                                    for (int jLoop = 0; jLoop <= firstChildNode.ChildNodes.Count - 1; jLoop++)
                                    {
                                        TreeNode secondChildNode = firstChildNode.ChildNodes[jLoop];
                                        if (secondChildNode.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()) || secondChildNode.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()))
                                        {
                                            secondChildNode.Selected = true;
                                            node.Expanded = true;
                                            firstChildNode.Expanded = true;
                                            secondChildNode.Expanded = true;
                                            GetTVEvents();
                                            return;
                                        }
                                        if (secondChildNode.ChildNodes.Count >= 0)
                                        {
                                            for (int kLoop = 0; kLoop <= secondChildNode.ChildNodes.Count - 1; kLoop++)
                                            {
                                                TreeNode thirdChildNode = secondChildNode.ChildNodes[kLoop];
                                                if (thirdChildNode.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()) || thirdChildNode.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()))
                                                {
                                                    thirdChildNode.Selected = true;
                                                    node.Expanded = true;
                                                    firstChildNode.Expanded = true;
                                                    secondChildNode.Expanded = true;
                                                    thirdChildNode.Expanded = true;
                                                    GetTVEvents();
                                                    return;
                                                }
                                                if (thirdChildNode.ChildNodes.Count >= 0)
                                                {
                                                    for (int lLoop = 0; lLoop <= thirdChildNode.ChildNodes.Count - 1; lLoop++)
                                                    {
                                                        TreeNode fourthChildNode = thirdChildNode.ChildNodes[lLoop];
                                                        //if (fourthChildNode.Text.ToUpper().Contains(txtSearch.Text.ToString().ToUpper()) || fourthChildNode.Value.ToUpper().Contains(txtSearch.Text.ToString().ToUpper()))
                                                        if (fourthChildNode.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()) || fourthChildNode.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()))
                                                        {
                                                            fourthChildNode.Selected = true;
                                                            node.Expanded = true;
                                                            firstChildNode.Expanded = true;
                                                            secondChildNode.Expanded = true;
                                                            thirdChildNode.Expanded = true;
                                                            fourthChildNode.Expanded = true;
                                                            GetTVEvents();
                                                            return;
                                                        }
                                                        if (fourthChildNode.ChildNodes.Count >= 0)
                                                        {
                                                            for (int mLoop = 0; mLoop <= fourthChildNode.ChildNodes.Count - 1; mLoop++)
                                                            {
                                                                TreeNode fifthChildNode = fourthChildNode.ChildNodes[mLoop];
                                                                if (fifthChildNode.Text.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()) || fifthChildNode.Value.ToUpper().Replace(" ", "").Contains(txtSearch.Text.ToString().ToUpper()))
                                                                {
                                                                    fifthChildNode.Selected = true;
                                                                    node.Expanded = true;
                                                                    firstChildNode.Expanded = true;
                                                                    secondChildNode.Expanded = true;
                                                                    thirdChildNode.Expanded = true;
                                                                    fourthChildNode.Expanded = true;
                                                                    fifthChildNode.Expanded = true;

                                                                    GetTVEvents();
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }



                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlBudgetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (ddlBudgetType.SelectedValue != null)
                {
                    if (ddlBudgetType.SelectedItem.Text.ToUpper() == "MONTHLY")
                    {
                        txtBudgetAmt.Enabled = false;
                    }
                    else
                    {
                        txtBudgetAmt.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void Calculate_Difference()
        {
            try
            {
                ErrorCollection.Clear();
                ErrorCollection.Remove("periods");
              
                lblSumError.Visible = false;
                lblSumError.Text = string.Empty;

                decimal totAmt = 0;
                totAmt = CommonUtils.ConvertStringToDecimal(txtPeriodPop1.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop2.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop3.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop4.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop5.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop6.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop7.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop8.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop9.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop10.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop11.Text) + CommonUtils.ConvertStringToDecimal(txtPeriodPop12.Text);

                if (totAmt > CommonUtils.ConvertStringToDecimal(txtBudgetAmt.Text))
                {
                    if (Session["errorPopup"] != null)
                    {
                        lblSumError.Visible = true;
                        lblSumError.Text = "Sum of period amount should not be greater than budget amount";
                        //ErrorCollection.Add("periods", "Sum of period amount should not be greater than budget amount");
                    }
                    // return;
                }
            }
            catch (Exception ex)
            {
                //ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtPeriodPop1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop1.Text.Trim() != string.Empty)
                {

                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period1"] = txtPeriodPop1.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop2.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period2"] = txtPeriodPop2.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop3.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period3"] = txtPeriodPop3.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop4_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop4.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period4"] = txtPeriodPop4.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop5.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period5"] = txtPeriodPop5.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop6_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop6.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period6"] = txtPeriodPop6.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop7_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop7.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period7"] = txtPeriodPop7.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop8_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop8.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period8"] = txtPeriodPop8.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop9_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop9.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period9"] = txtPeriodPop9.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop10_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop10.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period10"] = txtPeriodPop10.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop11_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop11.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period11"] = txtPeriodPop11.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPeriodPop12_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtPeriodPop12.Text.Trim() != string.Empty)
                {
                    Calculate_Difference();
                    //if (ErrorCollection.Count > 0)
                    //{
                    //    return;
                    //}
                    Session["Period12"] = txtPeriodPop12.Text;
                }
                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("seBdYddCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnBudgetPeriods_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection = ValidateHeader();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();
                Session["errorPopup"] = "1";
                fillperiods();

                mpePeriods.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnPeriodCancel_Click(object sender, EventArgs e)
        {
            mpePeriods.Hide();
            Session["errorPopup"] = null;
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            ModalPopupExtender2.Hide();

        }

        protected void ddlAccountCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSearch.Text = ddlAccountCode.SelectedItem.Text;
            btnSearch_Click(btnSearch, new ImageClickEventArgs(0, 0));
        }



    }
}