﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="FinancialTemplate_SegmentNew.aspx.cs" Inherits="FIN.Client.GL.FinancialTemplate_SegmentNew" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#FINContent_hf_BS_TMPL_GROUP_ID").val(str_tmpl_id);
            $("#FINContent_PARENT_GROUP_NAME").val(str_Group_name);
            $("#FINContent_lblSelectedGroup").text("Selected Group : " + str_Group_name);
            $("#btnSelectData").click();

        }

        function fn_showGraph() {
            $("#BSTemp").orgChart({ container: $("#main") });
            // addEvents();
            fn_JqueryTreeView();
        }

        $(window).bind("load", function () {
            $("#<%=btnGet.ClientID %>").click();
        });
    </script>
    <link rel="stylesheet" href="../Scripts/ORGChart/jquery.orgchart.css" />
    <script type="text/javascript" src="../Scripts/ORGChart/jquery.orgchart.js"></script>
    <style type="text/css">
        ul.LinkedList
        {
            display: block;
        }
        /* ul.LinkedList ul { display: none; } */
        .HandCursorStyle
        {
            cursor: pointer;
            cursor: hand;
        }
        /* For IE */
    </style>
    <script type="text/JavaScript">
        // Add this to the onload event of the BODY element
        function addEvents() {
            activateTree(document.getElementById("BSTemp"));
        }

        // This function traverses the list and add links 
        // to nested list items
        function activateTree(oList) {
            // Collapse the tree
            for (var i = 0; i < oList.getElementsByTagName("ul").length; i++) {
                oList.getElementsByTagName("ul")[i].style.display = "none";
            }
            // Add the click-event handler to the list items
            if (oList.addEventListener) {
                oList.addEventListener("click", toggleBranch, false);
            } else if (oList.attachEvent) { // For IE
                oList.attachEvent("onclick", toggleBranch);
            }
            // Make the nested items look like links
            addLinksToBranches(oList);
        }

        // This is the click-event handler
        function toggleBranch(event) {
            var oBranch, cSubBranches;
            if (event.target) {
                oBranch = event.target;
            } else if (event.srcElement) { // For IE
                oBranch = event.srcElement;
            }
            cSubBranches = oBranch.getElementsByTagName("ul");
            if (cSubBranches.length > 0) {
                if (cSubBranches[0].style.display == "block") {
                    cSubBranches[0].style.display = "none";
                } else {
                    cSubBranches[0].style.display = "block";
                }
            }
        }

        // This function makes nested list items look like links
        function addLinksToBranches(oList) {
            var cBranches = oList.getElementsByTagName("li");
            var i, n, cSubBranches;
            if (cBranches.length > 0) {
                for (i = 0, n = cBranches.length; i < n; i++) {
                    cSubBranches = cBranches[i].getElementsByTagName("ul");
                    if (cSubBranches.length > 0) {
                        addLinksToBranches(cSubBranches[0]);
                        cBranches[i].className = "HandCursorStyle";
                        cBranches[i].style.color = "blue";
                        cSubBranches[0].style.color = "black";
                        cSubBranches[0].style.cursor = "auto";
                    }
                }
            }
        }
    </script>
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblStructureName">
                Template Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 500px">
                <asp:TextBox ID="txtTempName" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1" MaxLength="500" AutoPostBack="True" OnTextChanged="txtTempName_TextChanged"></asp:TextBox>
                <asp:Button ID="btnGet" runat="server" Text="Get" OnClick="btnGet_Click" Style="display: none" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <table width="100%" border="1">
                <tr>
                    <td style="width: 50%;" valign="top">
                        <div id="div_Group" runat="server" visible="true">
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="  width: 100px" id="Div1">
                                    Group name
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 350px">
                                    <asp:TextBox ID="txtGroupName" CssClass="validate[required] RequiredField txtBox"
                                        runat="server" TabIndex="2" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10" style="width: 30%">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="  width: 100px" id="Div2">
                                    Group Number
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 50px">
                                    <asp:TextBox ID="txtGroupNumber" CssClass="validate[required] RequiredField txtBox"
                                        runat="server" TabIndex="3" MaxLength="5"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="txtGroupNumber" />
                                </div>
                                <div class="lblBox LNOrient" style="  width: 50px" id="Div9">
                                    Notes
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 50px">
                                    <asp:TextBox ID="txtNotes" CssClass="txtBox" runat="server" TabIndex="3" MaxLength="3"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                        TargetControlID="txtNotes" />
                                </div>
                                <div class="lblBox LNOrient" style="  width: 100px" id="Div3">
                                    Total Required
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 20px">
                                    <asp:CheckBox ID="chkTotReq" runat="server" Text=" " AutoPostBack="True" OnCheckedChanged="chkTotReq_CheckedChanged" />
                                </div>
                                <div class="lblBox LNOrient" style="  width: 70px" id="Div5">
                                    Formula
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 20px">
                                    <asp:CheckBox ID="chkFormulaReq" runat="server" Text=" " AutoPostBack="True" OnCheckedChanged="chkFormulaReq_CheckedChanged" />
                                </div>
                            </div>
                            <div class="divClear_10" style="width: 30%">
                            </div>
                            <div class="divRowContainer" id="div_Formula" runat="server" visible="false">
                                <div class="divRowContainer">
                                    <div class="lblBox LNOrient" style="  width: 100px" id="Div6">
                                        Group NUmber
                                    </div>
                                    <div class="divtxtBox LNOrient" style="  width: 80px">
                                        <asp:DropDownList ID="ddlGroupNuber" runat="server" CssClass="ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox LNOrient" style="  width: 80px" id="Div7">
                                        Operator
                                    </div>
                                    <div class="divtxtBox LNOrient" style="  width: 80px">
                                        <asp:DropDownList ID="ddlOperator" runat="server" CssClass=" ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox LNOrient" style="  width: 100px" id="Div8">
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 30%">
                                </div>
                                <div class="divRowContainer">
                                    <asp:TextBox ID="txtFormula" CssClass="txtBox" runat="server" Text=""></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="div_selSubtype" class="divRowContainer" runat="server" visible="false" align="center">
                            <asp:RadioButtonList ID="rbl_subType" runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rbl_subType_SelectedIndexChanged">
                                <asp:ListItem Value="GROUP" Selected="True">GROUP</asp:ListItem>
                                <asp:ListItem Value="ACCOUNT">ACCOUNT</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div id="div_SubGroup" class="divRowContainer" runat="server" visible="false">
                            <asp:GridView ID="gvSubGroup" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                Width="100%" OnRowCancelingEdit="gvSubGroup_RowCancelingEdit" OnRowCreated="gvSubGroup_RowCreated"
                                OnRowDataBound="gvSubGroup_RowDataBound" ShowFooter="True" OnRowCommand="gvSubGroup_RowCommand"
                                OnRowUpdating="gvSubGroup_RowUpdating" OnRowEditing="gvSubGroup_RowEditing" DataKeyNames="BS_TMPL_ID"
                                OnRowDeleting="gvSubGroup_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGGroupname" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                Text='<%# Eval("BS_GROUP_NAME") %>' MaxLength="500" Width="95%"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtGGroupname" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                Width="95%" MaxLength="500"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGGroupName" runat="server" Text='<%# Eval("BS_GROUP_NAME") %>'
                                                Width="130px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Number">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGGroupNumber" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                Text='<%# Eval("BS_GROUP_NO") %>' MaxLength="15" Width="95%"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FTDur" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                TargetControlID="txtGroupNumber" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtGGroupNumber" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                Width="95%" MaxLength="15"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FTDurft" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                TargetControlID="txtGroupNumber" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGGroupNumber" runat="server" Text='<%# Eval("BS_GROUP_NO") %>'
                                                Width="80px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="80px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Notes">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGGroupNotes" runat="server" CssClass="EntryFont  txtBox_N" Text='<%# Eval("NOTES") %>'
                                                MaxLength="15" Width="95%"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FTGNotesDur" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                TargetControlID="txtGroupNumber" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtGGroupNotes" runat="server" CssClass="EntryFont  txtBox_N" Width="95%"
                                                MaxLength="15"></asp:TextBox>
                                            <cc2:FilteredTextBoxExtender ID="FTGnotesDurft" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                TargetControlID="txtGroupNumber" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGGroupNotes" runat="server" Text='<%# Eval("NOTES") %>' Width="80px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="80px" />
                                        <FooterStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Required">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkGtotReq" runat="server" Checked='<%# Convert.ToBoolean(Eval("BS_TOT_REQ").ToString()=="1"?"TRUE":"FALSE") %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkGtotReq" runat="server" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkGtotReq" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("BS_TOT_REQ").ToString()=="1"?"TRUE":"FALSE") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                        <FooterStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Formula">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkGFormula" runat="server" Checked='<%# Convert.ToBoolean(Eval("BS_FORMULA_GROUP").ToString()=="1"?"TRUE":"FALSE") %>'
                                                AutoPostBack="True" OnCheckedChanged="chkGFormula_CheckedChanged" />
                                            <asp:ImageButton ID="imgEditGFormula" runat="server" Width="15px" Height="15px" ImageUrl="~/Images/edit.png"
                                                OnClick="imgEditGFormula_Click" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkGFormula" runat="server" AutoPostBack="True" OnCheckedChanged="chkGFormula_CheckedChanged" />
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkGFormula" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("BS_FORMULA_GROUP").ToString()=="1"?"TRUE":"FALSE") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                        <FooterStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Formula">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGFormula" runat="server" CssClass="EntryFont  txtBox" Enabled="false"
                                                Text='<%# Eval("BS_FORMULA") %>' MaxLength="100" Width="95%"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtGFormula" runat="server" CssClass="EntryFont  txtBox" Enabled="false"
                                                Width="95%" MaxLength="100"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGFormula" runat="server" Text='<%# Eval("BS_FORMULA") %>' Width="130px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <FooterStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Add / Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                Height="20px" ImageUrl="~/Images/Update.png" />
                                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                ImageUrl="~/Images/Add.jpg" />
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <FooterStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                        <div id="div_subAccount" class="divRowContainer" runat="server" visible="false">
                            <div class="divRowContainer" align="right">
                                <asp:ImageButton ID="btnSelect" runat="server" ImageUrl="~/Images/ViewIcon.png" OnClick="btnSelect_Click"
                                    AlternateText="Search" ToolTip="Show Account Search" Width="25px" Height="25px" />
                            </div>
                            <div  class="LNOrient">
                                <asp:GridView ID="gvsubAccount" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    Width="100%" OnRowCancelingEdit="gvsubAccount_RowCancelingEdit" OnRowCreated="gvsubAccount_RowCreated"
                                    OnRowDataBound="gvsubAccount_RowDataBound" ShowFooter="True" OnRowCommand="gvsubAccount_RowCommand"
                                    OnRowUpdating="gvsubAccount_RowUpdating" OnRowEditing="gvsubAccount_RowEditing"
                                    DataKeyNames="BS_TMPL_ID,BS_ACC_ID" OnRowDeleting="gvsubAccount_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Account Name">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlAccName" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlAccName" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblGGroupName" runat="server" Text='<%# Eval("BS_GROUP_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <FooterStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="div_SubSegment" class="divRowContainer" runat="server" visible="false">
                            <div class="divRowContainer ">
                                <div class="lblBox LNOrient" style="  width: 100px" id="Div4">
                                    Account name
                                </div>
                                <div class="divtxtBox LNOrient" style="  width: 350px">
                                    <asp:TextBox ID="txtSelectedAccount" CssClass="validate[required] RequiredField txtBox"
                                        Enabled="false" runat="server" TabIndex="2" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10" style="width: 30%">
                            </div>
                            <div class="divRowContainer">
                                <div class="divRowContainer" align="right">
                                    <asp:ImageButton ID="btnSegmentSel" runat="server" ImageUrl="~/Images/ViewIcon.png"
                                        OnClick="btnSegmentSel_Click" AlternateText="Search" ToolTip="Show Segment Search"
                                        Width="25px" Height="25px" />
                                </div>
                                <div  class="LNOrient">
                                    <asp:GridView ID="gvSubSegment" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                        Width="100%" OnRowCancelingEdit="gvSubSegment_RowCancelingEdit" OnRowCreated="gvSubSegment_RowCreated"
                                        OnRowDataBound="gvSubSegment_RowDataBound" ShowFooter="True" OnRowCommand="gvSubSegment_RowCommand"
                                        OnRowUpdating="gvSubSegment_RowUpdating" OnRowEditing="gvSubSegment_RowEditing"
                                        DataKeyNames="BS_TMPL_ID,BS_ACC_ID" OnRowDeleting="gvSubSegment_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Segment Name">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlGSegment" runat="server" CssClass="ddlStype" Width="95%">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlGSegment" runat="server" CssClass="ddlStype" Width="95%">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGSegment" runat="server" Text='<%# Eval("BS_ACC_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Add / Edit">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                        CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                        CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                        Height="20px" ImageUrl="~/Images/Update.png" />
                                                    <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                        CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                        ImageUrl="~/Images/Add.jpg" />
                                                </FooterTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <FooterStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="divRowContainer" align="right">
                        </div>
                        <div class="divClear_10" style="width: 30%">
                        </div>
                        <div class="divRowContainer" align="right">
                            <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Images/Add.png" AlternateText="Add"
                                ToolTip="Add" OnClick="imgbtnAdd_Click" Width="25px" Height="25px" />
                        </div>
                        <asp:HiddenField ID="hf_formulapop" runat="server" />
                        <asp:HiddenField ID="hf_Temp_RefNo" runat="server" />
                        <cc2:ModalPopupExtender ID="mpeFormula" runat="server" TargetControlID="hf_formulapop"
                            PopupControlID="panelDetailsPopup" CancelControlID="btnCancel" BackgroundCssClass="ConfirmBackground">
                        </cc2:ModalPopupExtender>
                        <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="LightBlue" Width="60%"
                            Height="25%" ScrollBars="Auto">
                            <div class="divRowContainer" id="div_FormulaPOPUP" runat="server" visible="false">
                                <div class="divRowContainer">
                                    <div class="lblBox LNOrient" style="  width: 100px" id="Div11">
                                        Group NUmber
                                    </div>
                                    <div class="divtxtBox LNOrient" style="  width: 180px">
                                        <asp:DropDownList ID="ddlGFgroupName" runat="server" CssClass="ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox LNOrient" style="  width: 80px" id="Div12">
                                        Operator
                                    </div>
                                    <div class="divtxtBox LNOrient" style="  width: 180px">
                                        <asp:DropDownList ID="ddlGFOperation" runat="server" CssClass=" ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="lblBox LNOrient" style="  width: 100px" id="Div13">
                                        <asp:Button ID="btnGFAdd" runat="server" Text="Add" OnClick="btnGFAdd_Click" />
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 30%">
                                </div>
                                <div class="divRowContainer">
                                    <asp:TextBox ID="txtGpopFormula" CssClass="txtBox" runat="server" Text=""></asp:TextBox>
                                </div>
                                <div class="divClear_10" style="width: 30%">
                                </div>
                                <div class="divRowContainer">
                                    <div class="lblBox LNOrient" style="float: left">
                                        <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" />
                                    </div>
                                    <div class="lblBox LNOrient" style="float: right">
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                    <td style="width: 50%;" valign="top">
                        <div id="div_Template" runat="server" class="tree">
                        </div>
                    </td>
                </tr>
                <tr style="display: none">
                    <td colspan="2">
                        <div id="main" style="display: none">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hf_BS_TMPL_ID" runat="server" />
        <asp:HiddenField ID="hf_BS_TMPL_GROUP_ID" runat="server" />
        <asp:HiddenField ID="PARENT_GROUP_NAME" runat="server" />
        <div style="display: none">
            <asp:Button ID="btnSelectData" runat="server" Text="Select Data" OnClick="btnSelectData_Click"
                ClientIDMode="Static" />
        </div>
        <asp:HiddenField ID="hf_AcctCode_Sel" runat="server" />
        <cc2:ModalPopupExtender ID="mpeAcctCode" runat="server" TargetControlID="hf_AcctCode_Sel"
            PopupControlID="pnlAcctCode" CancelControlID="btnCancelAcctCode" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlAcctCode" runat="server" BackColor="LightBlue" Width="80%" Height="70%"
            ScrollBars="Auto">
            <div id="div_AcctPop" runat="server" visible="false">
                <div class="divRowContainer">
                </div>
                <div class="divClear_10" style="width: 60%">
                </div>
                <div style="  width: 100%">
                    <div class="lblBox LNOrient" style="  width: 130px">
                        From Account
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 200px">
                        <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                            TabIndex="10">
                        </asp:DropDownList>
                    </div>
                    <div class="colspace" style=" ">
                        &nbsp;</div>
                    <div class="lblBox LNOrient" style="  width: 130px">
                        To Account
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 200px">
                        <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                            TabIndex="11">
                        </asp:DropDownList>
                    </div>
                    <div class="colspace" style=" ">
                        &nbsp;</div>
                    <div class="divtxtBox LNOrient" style=" ">
                        <asp:ImageButton ID="btnAcctSearch" runat="server" ImageUrl="~/Images/RoundSearch.png"
                            OnClick="btnAcctSearch_Click" AlternateText="Search" ToolTip="Search Account"
                            Width="25px" Height="25px" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="btnAcctOK" runat="server" ImageUrl="~/Images/RoundOk.png" OnClick="btnAcctOK_Click"
                            AlternateText="Add" ToolTip="Add Account" Width="25px" Height="25px" />
                    </div>
                    <div align="right">
                        <asp:ImageButton ID="btnCancelAcctCode" runat="server" ImageUrl="~/Images/Close.png"
                            AlternateText="Close" ToolTip="Close" Width="25px" Height="25px" />
                    </div>
                </div>
                <div class="divClear_10" style="width: 10%">
                </div>
                <div style="  width: 45%; height: 500px; overflow: auto">
                    <div style="  width: 95%; padding-left: 10px">
                        <asp:GridView ID="gv_All_Acct" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            Width="100%" DataKeyNames="CODE_ID,CODE_NAME" OnRowDataBound="gv_All_Acct_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Accoount Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("CODE_NAME")%></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAll_Acct" runat="server" />
                                        
                                    </ItemTemplate >
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chk_Acct_Sel_ALL" runat="server" AutoPostBack="True" 
                                            oncheckedchanged="chk_Acct_Sel_ALL_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </div>
                </div>
                <div style="  width: 8%" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="img_OneAdd" runat="server" ImageUrl="~/Images/Next_Icon.png"
                                    AlternateText="One" ToolTip="Move Selected Record" OnClick="img_OneAdd_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="img_oneRemove" runat="server" ImageUrl="~/Images/Back_Icon.png"
                                    AlternateText="ALL" ToolTip="Remove Selected Record" OnClick="img_oneRemove_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="  width: 45%; height: 500px; overflow: auto">
                    <asp:GridView ID="gv_Sel_Acct" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                        Width="95%" DataKeyNames="CODE_ID,CODE_NAME" OnRowDataBound="gv_Sel_Acct_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Accoount Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("CODE_NAME")%></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSel_Acct" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hf_Segment_Sel" runat="server" />
        <cc2:ModalPopupExtender ID="mpeSegment" runat="server" TargetControlID="hf_Segment_Sel"
            PopupControlID="pnlSegment" CancelControlID="btnCancelSegment" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlSegment" runat="server" BackColor="LightBlue" Width="80%" Height="70%"
            ScrollBars="Auto">
            <div id="div_Segment_POPUP" runat="server" visible="false">
                <div class="divRowContainer">
                </div>
                <div class="divClear_10" style="width: 60%">
                </div>
                <div style="  width: 100%">
                    <div class="lblBox LNOrient" style="  width: 130px">
                        From Segment
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 200px">
                        <asp:DropDownList ID="ddlFromSegment" runat="server" CssClass=" RequiredField ddlStype"
                            TabIndex="10">
                        </asp:DropDownList>
                    </div>
                    <div class="colspace" style=" ">
                        &nbsp;</div>
                    <div class="lblBox LNOrient" style="  width: 130px">
                        To Segment
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 200px">
                        <asp:DropDownList ID="ddlToSegment" runat="server" CssClass=" RequiredField ddlStype"
                            TabIndex="11">
                        </asp:DropDownList>
                    </div>
                    <div class="colspace" style=" ">
                        &nbsp;</div>
                    <div class="divtxtBox LNOrient" style=" ">
                        <asp:ImageButton ID="btnSegmentSearch" runat="server" ImageUrl="~/Images/RoundSearch.png"
                            OnClick="btnSegmentSearch_Click" AlternateText="Search" ToolTip="Search Segemnt"
                            Width="25px" Height="25px" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="btnSegmentOk" runat="server" ImageUrl="~/Images/RoundOk.png"
                            OnClick="btnSegmentOk_Click" AlternateText="Add" ToolTip="Add Segment" Width="25px"
                            Height="25px" />
                    </div>
                    <div align="right">
                        <asp:ImageButton ID="btnCancelSegment" runat="server" ImageUrl="~/Images/Close.png"
                            AlternateText="Close" ToolTip="Close" Width="25px" Height="25px" />
                    </div>
                </div>
                <div class="divClear_10" style="width: 10%">
                </div>
                <div style="  width: 45%; height: 500px; overflow: auto">
                    <div style="  width: 95%; padding-left: 10px">
                        <asp:GridView ID="gv_All_Segment" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            Width="100%" DataKeyNames="SEGMENT_VALUE_ID,SEGMENT_VALUE">
                            <Columns>
                                <asp:TemplateField HeaderText="Segment Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("SEGMENT_VALUE")%></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAll_Acct" runat="server" />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chk_Seg_Sel_ALL" runat="server" AutoPostBack="True" 
                                            oncheckedchanged="chk_Seg_Sel_ALL_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </div>
                </div>
                <div style="  width: 8%" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="img_segment_oneAdd" runat="server" ImageUrl="~/Images/Next_Icon.png"
                                    AlternateText="One" ToolTip="Move Selected Record" OnClick="img_segment_oneAdd_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="img_Segment_Remove" runat="server" ImageUrl="~/Images/Back_Icon.png"
                                    AlternateText="ALL" ToolTip="Remove Selected Record" OnClick="img_Segment_Remove_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="  width: 45%; height: 500px; overflow: auto">
                    <asp:GridView ID="gv_Sel_Segment" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                        Width="95%" DataKeyNames="SEGMENT_VALUE_ID,SEGMENT_VALUE">
                        <Columns>
                            <asp:TemplateField HeaderText="Segment Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lb_AcctName" runat="server"><%# Eval("SEGMENT_VALUE")%></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSel_Acct" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_imgbtnAdd").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_imgBtnUpdate").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
