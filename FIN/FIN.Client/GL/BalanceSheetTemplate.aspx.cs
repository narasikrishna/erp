﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
namespace FIN.Client.GL
{
    public partial class BalanceSheetTemplate : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AssingtoControl();
                ChangeLanguage();
            }
            else
            {
                Reload_BS_Structure();
            }

        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            UserRightsChecking();



        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbl_Type.Items[0].Text = Prop_File_Data["Group_P"];
                    rbl_Type.Items[1].Text = Prop_File_Data["Account_P"];

                    rbAmountType.Items[0].Text = Prop_File_Data["Yes_P"];
                    rbAmountType.Items[1].Text = Prop_File_Data["No_P"];
                    rbAmountType.Items[2].Text = Prop_File_Data["No_P"];
                    rbAmountType.Items[3].Text = Prop_File_Data["No_P"];
                    rbAmountType.Items[4].Text = Prop_File_Data["No_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    imgbtnAdd.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    imgbtnAdd.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    imgBtnDelete.Visible = false;
                }
            }
        }


        private void AssingtoControl()
        {
            Startup();
            FillComboBox();
            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
            {
                txtTempName.Text = Master.StrRecordId;
                Reload_BS_Structure();
                txtTempName.Enabled = false;

            }
        }
        private void FillComboBox()
        {



            ddlOperator.Items.Add(new ListItem("---Select---", ""));
            ddlOperator.Items.Add(new ListItem("+", "+"));
            ddlOperator.Items.Add(new ListItem("-", "-"));
            ddlOperator.Items.Add(new ListItem("*", "*"));
            ddlOperator.Items.Add(new ListItem("/", "/"));
            ddlOperator.Items.Add(new ListItem("%", "%"));

        }

        private void AddGroupAccDet()
        {

            try
            {
                if (txtTempName.Text.ToString().Length == 0)
                {
                    ErrorCollection.Add("InvlaidTempName", "Please Enter Template Name");
                    return;
                }
                if (rbl_Type.SelectedValue.ToString().ToUpper() == "GROUP")
                {
                    if (txtGroupName.Text.ToString().Length == 0)
                    {
                        ErrorCollection.Add("Please Enter the Group Name", " Please Enter Group Name ");
                        return;
                    }
                    if (txtGroupNumber.Text.ToString().Length == 0)
                    {
                        ErrorCollection.Add("Please Enter the Group Number", " Please Enter Group Number ");
                        return;
                    }

                    DataTable dt_dubData = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_Template4Group(txtTempName.Text, txtGroupNumber.Text)).Tables[0];
                    if (dt_dubData.Rows.Count > 0)
                    {
                        if (hf_BS_TMPL_ID.Value.ToString().Length > 0)
                        {
                            if (dt_dubData.Rows.Count > 1)
                            {
                                ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                                return;
                            }
                            else
                            {
                                if (dt_dubData.Rows[0]["BS_TMPL_ID"].ToString() != hf_BS_TMPL_ID.Value.ToString())
                                {
                                    ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                                    return;
                                }
                            }
                        }
                        else
                        {
                            ErrorCollection.Add("Group Number Duplicate", " Given Group Number is already Available ");
                            return;
                        }
                    }

                }
                else if (rbl_Type.SelectedValue.ToString().ToUpper() == "ACCOUNT")
                {
                    if (gv_Sel_Acct.Rows.Count == 0)
                    {
                        ErrorCollection.Add("Please select the Account Name", " Please Select Account Name ");
                        return;
                    }
                }
                else if (rbl_Type.SelectedValue.ToString().ToUpper() == "SEGMENT")
                {
                    if (gv_Sel_Segment.Rows.Count == 0)
                    {
                        ErrorCollection.Add("Please select the Segment Name", " Please Select Segment Name ");
                        return;
                    }
                }

                if (txtFormula.Text.ToString().Contains("##"))
                {
                    ErrorCollection.Add("InvalidFormula", "Invalid Formula");
                    return;
                }
                try
                {
                    if (txtFormula.Text.ToString().Length > 0)
                    {
                        string str_Fromula = txtFormula.Text.ToString().Replace("#", "");
                        DataTable dt = DBMethod.ExecuteQuery("SELECT " + str_Fromula + " FROM DUAL ").Tables[0];


                    }
                }
                catch (Exception ex_F)
                {
                    ErrorCollection.Add("iNVALID fORMAULA", "Given Formula is invalid");
                    return;
                }

                /* if (hf_BS_TMPL_ID.Value.ToString().Length == 0)
                 {

                     if (hf_BS_TMPL_GROUP_ID.Value.ToString().Trim().Length > 0)
                     {


                         GL_BALANCE_SHEET_TEMPLATE gl_Parent_BS = new GL_BALANCE_SHEET_TEMPLATE();
                         using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                         {
                             gl_Parent_BS = userCtx.Find(r =>
                                 (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                 ).SingleOrDefault();
                         }

                         if (gl_Parent_BS.BS_FORMULA_GROUP == FINAppConstants.Y)
                         {
                             ErrorCollection.Add("Invalid Group Selection", " Selected Group is Formula Group, So we are not able to add group / account");
                             return;
                         }
                     }
                 }
                 */
                if (rbl_Type.SelectedValue.ToString().ToUpper() == "SEGMENT")
                {

                    DataTable dt_Segment = (DataTable)Session["SEGMENT_LIST"];
                    for (int iLoop = 0; iLoop < dt_Segment.Rows.Count; iLoop++)
                    {
                        
                        GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
                        if (dt_Segment.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                        {

                            using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                            {
                                gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                                    (r.BS_TMPL_ID == dt_Segment.Rows[iLoop]["BS_TMPL_ID"].ToString())
                                    ).SingleOrDefault();
                            }

                        }

                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                        gL_BALANCE_SHEET_TEMPLATE.BS_TYPE = rbl_Type.SelectedValue.ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_TOT_REQ = chkTotReq.Checked ? FINAppConstants.Y : FINAppConstants.N;
                        gL_BALANCE_SHEET_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        gL_BALANCE_SHEET_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                        gL_BALANCE_SHEET_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_ID = dt_Segment.Rows[iLoop]["SEGMENT_ID"].ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_NAME = dt_Segment.Rows[iLoop]["SEGMENT_NAME"].ToString();
                        // gL_BALANCE_SHEET_TEMPLATE.AMOUNT_TYPE = rbAmountType.SelectedValue.ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();
                        GL_BALANCE_SHEET_TEMPLATE gl_Parent_BS = new GL_BALANCE_SHEET_TEMPLATE();
                        using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() != "ACCOUNT")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can only add Segment inside Account");

                                return;
                            }
                            gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                        else
                        {
                            ErrorCollection.Add("Invalid Group Add", "We can only add Segment inside Account");

                            return;
                        }

                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;

                        if (dt_Segment.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                        {
                            if (dt_Segment.Rows[iLoop]["SELECTED_REC"].ToString() == "N")
                            {
                                DBMethod.DeleteEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                            }
                            else
                            {
                                gL_BALANCE_SHEET_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                                gL_BALANCE_SHEET_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE, true);
                            }
                        }
                        else
                        {
                            if (dt_Segment.Rows[iLoop]["SELECTED_REC"].ToString() == "Y")
                            {
                                gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                                gL_BALANCE_SHEET_TEMPLATE.CREATED_BY = this.LoggedUserName;
                                gL_BALANCE_SHEET_TEMPLATE.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                            }
                        }
                    }

                }
                else if (rbl_Type.SelectedValue.ToString().ToUpper() == "ACCOUNT")
                {
                    DataTable dt_AccountCode = (DataTable)Session["ACCTCODE_LIST"];
                    for (int iLoop = 0; iLoop < dt_AccountCode.Rows.Count; iLoop++)
                    {
                        GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
                        if (dt_AccountCode.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                        {

                            using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                            {
                                gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                                    (r.BS_TMPL_ID == dt_AccountCode.Rows[iLoop]["BS_TMPL_ID"].ToString())
                                    ).SingleOrDefault();
                            }

                        }

                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                        gL_BALANCE_SHEET_TEMPLATE.BS_TYPE = rbl_Type.SelectedValue.ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_TOT_REQ = chkTotReq.Checked ? FINAppConstants.Y : FINAppConstants.N;
                        gL_BALANCE_SHEET_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        gL_BALANCE_SHEET_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                        gL_BALANCE_SHEET_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_ID = dt_AccountCode.Rows[iLoop]["CODE_ID"].ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_NAME = dt_AccountCode.Rows[iLoop]["CODE_NAME"].ToString();
                         gL_BALANCE_SHEET_TEMPLATE.AMOUNT_TYPE = rbAmountType.SelectedValue.ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();
                        GL_BALANCE_SHEET_TEMPLATE gl_Parent_BS = new GL_BALANCE_SHEET_TEMPLATE();
                        using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() != "GROUP")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can add Account only inside the Group");

                                return;
                            }
                            gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                        else
                        {
                            ErrorCollection.Add("Invalid Group Add", "We can add Account only inside the Group");

                            return;
                        }

                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;

                        if (dt_AccountCode.Rows[iLoop]["BS_TMPL_ID"].ToString() != "0")
                        {
                            if (dt_AccountCode.Rows[iLoop]["SELECTED_REC"].ToString() == "N")
                            {
                                DataTable dt_childRec = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_TemplateData4TmplId(dt_AccountCode.Rows[iLoop]["BS_TMPL_ID"].ToString())).Tables[0];
                                if (dt_childRec.Rows.Count > 0)
                                {
                                    ErrorCollection.Add("Can'Delete" + dt_AccountCode.Rows[iLoop]["BS_TMPL_ID"].ToString(), "Can't Remove" + gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NAME + " , Child Record Found");
                                }
                                else
                                {
                                    DBMethod.DeleteEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                                }

                            }
                            else
                            {
                                gL_BALANCE_SHEET_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                                gL_BALANCE_SHEET_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE, true);
                            }
                        }
                        else
                        {
                            if (dt_AccountCode.Rows[iLoop]["SELECTED_REC"].ToString() == "Y")
                            {
                                gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                                gL_BALANCE_SHEET_TEMPLATE.CREATED_BY = this.LoggedUserName;
                                gL_BALANCE_SHEET_TEMPLATE.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                            }
                        }
                    }
                }
                else if (rbl_Type.SelectedValue.ToString().ToUpper() == "GROUP")
                {
                    GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
                    if (hf_BS_TMPL_ID.Value.ToString().Length > 0)
                    {

                        using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                        {
                            gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_ID.Value.ToString())
                                ).SingleOrDefault();
                        }

                    }

                    gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;
                    gL_BALANCE_SHEET_TEMPLATE.BS_TYPE = rbl_Type.SelectedValue.ToString();
                    gL_BALANCE_SHEET_TEMPLATE.BS_TOT_REQ = chkTotReq.Checked ? FINAppConstants.Y : FINAppConstants.N;
                    gL_BALANCE_SHEET_TEMPLATE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    gL_BALANCE_SHEET_TEMPLATE.ENABLED_FLAG = FINAppConstants.Y;
                    gL_BALANCE_SHEET_TEMPLATE.BS_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    if (rbl_Type.SelectedValue.ToString().ToUpper() == "GROUP")
                    {
                        gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NAME = txtGroupName.Text;
                        if (txtGroupNumber.Text.ToString().Trim().Length > 0)
                        {
                            gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NO = int.Parse(txtGroupNumber.Text);
                        }
                        else
                        {
                            gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NO = null;
                        }

                        if (txtNotes.Text.ToString().Trim().Length > 0)
                        {
                            gL_BALANCE_SHEET_TEMPLATE.NOTES = int.Parse(txtNotes.Text);
                        }
                        else
                        {
                            gL_BALANCE_SHEET_TEMPLATE.NOTES = null;
                        }

                        if (chkFormulaReq.Checked)
                        {
                            gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA_GROUP = FINAppConstants.Y;
                            gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA = txtFormula.Text;
                        }
                        else
                        {
                            gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA_GROUP = FINAppConstants.N;
                            gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA = null;
                        }

                        if (hf_BS_TMPL_ID.Value.ToString().Length == 0)
                        {
                            if (hf_BS_TMPL_GROUP_ID.Value.ToString().Trim().Length > 0)
                            {
                                gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();

                                GL_BALANCE_SHEET_TEMPLATE gl_Parent_BS = new GL_BALANCE_SHEET_TEMPLATE();
                                using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                                {
                                    gl_Parent_BS = userCtx.Find(r =>
                                        (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                        ).SingleOrDefault();
                                }
                                if (gl_Parent_BS != null)
                                {
                                    if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                                    {
                                        ErrorCollection.Add("Invalid Group Add", "We can't add Group Inside the account");

                                        return;
                                    }
                                    gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                                }
                            }
                        }
                    }
                    else
                    {
                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_ID = gv_Sel_Acct.DataKeys[0].Values["acct_code_id"].ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_ACC_NAME = gv_Sel_Acct.DataKeys[0].Values["CODE_NAME"].ToString();
                        // gL_BALANCE_SHEET_TEMPLATE.AMOUNT_TYPE = rbAmountType.SelectedValue.ToString();
                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID = hf_BS_TMPL_GROUP_ID.Value.ToString();
                        GL_BALANCE_SHEET_TEMPLATE gl_Parent_BS = new GL_BALANCE_SHEET_TEMPLATE();
                        using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                        {
                            gl_Parent_BS = userCtx.Find(r =>
                                (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                                ).SingleOrDefault();
                        }
                        if (gl_Parent_BS != null)
                        {
                            if (gl_Parent_BS.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                            {
                                ErrorCollection.Add("Invalid Group Add", "We can't add Account Inside the account");

                                return;
                            }
                            gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_GROUP_NAME = gl_Parent_BS.BS_GROUP_NAME;
                        }
                    }
                    gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_NAME = txtTempName.Text;

                    if (hf_BS_TMPL_ID.Value.ToString().Length > 0)
                    {
                        // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        gL_BALANCE_SHEET_TEMPLATE.MODIFIED_BY = this.LoggedUserName;
                        gL_BALANCE_SHEET_TEMPLATE.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE, true);
                    }
                    else
                    {
                        gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_ID = FINSP.GetSPFOR_SEQCode("GL_024", false, true);
                        gL_BALANCE_SHEET_TEMPLATE.CREATED_BY = this.LoggedUserName;
                        gL_BALANCE_SHEET_TEMPLATE.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                    }
                }

                rbl_Type.SelectedIndex = 0;
                fn_Show_GROUPAcc();
                txtGroupName.Text = "";
                txtGroupNumber.Text = "";
                chkTotReq.Checked = false;
                hf_BS_TMPL_ID.Value = "";
                hf_BS_TMPL_GROUP_ID.Value = "";
                lblSelectedGroup.Text = "";
                imgbtnAdd.Visible = true;
                imgBtnUpdate.Visible = false;
                imgBtnDelete.Visible = false;
                chkFormulaReq.Checked = false;
                ShowFormulaColumn();
                txtFormula.Text = "";
                txtNotes.Text = "";
                ddlOperator.SelectedIndex = 0;



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void Reload_BS_Structure()
        {
            try
            {
                DataTable dt_BS_TMPL_DATA = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_TemplateStructure(txtTempName.Text)).Tables[0];
                string str_ORG_STR = "";

                /// MODEL 1
                //if (dt_BS_TMPL_DATA.Rows.Count > 0)
                //{
                //    DataTable dt_EmpProcessList = new DataTable();
                //    dt_EmpProcessList.Columns.Add("GROUP_NAME");
                //    dt_EmpProcessList.Columns.Add("BS_TMPL_ID");
                //    dt_EmpProcessList.Columns.Add("BS_DISPLAY_NAME");

                //    dt_EmpProcessList.Columns.Add("STATUS");

                //    var var_HeadEmpList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_GROUP_NAME"].ToString().Contains("HEAD"));
                //    if (var_HeadEmpList.Any())
                //    {
                //        DataTable dt_data = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);



                //        string str_Replace_str = "";
                //        string str_new_replace_str = "";
                //        string str_tmp;
                //        str_ORG_STR += "  <ul  id='BSTemp' class='LinkedList' style='list-style: none;'> ";
                //        //  str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + txtTempName.Text + "</div>";
                //        str_ORG_STR += "  <li> " + txtTempName.Text + " <div style='float:right' onClick=fn_GroupAccClick('','')> <img src='../Images/RightMark.png' alt='select'/> </div> ";
                //        str_ORG_STR += " <ul>";
                //        for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                //        {
                //            DataRow dr = dt_EmpProcessList.NewRow();
                //            dr["GROUP_NAME"] = dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                //            dr["BS_TMPL_ID"] = dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                //            dr["BS_DISPLAY_NAME"] = dt_data.Rows[iLoop]["BS_DISPLAY_NAME"].ToString();
                //            dr["STATUS"] = "N";
                //            dt_EmpProcessList.Rows.Add(dr);

                //            str_ORG_STR += "<li>";
                //            str_ORG_STR += dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                //            str_ORG_STR += "</li>";
                //            DataRow[] dr_col;
                //            do
                //            {
                //                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                //                if (dr_col.Length > 0)
                //                {
                //                    str_Replace_str = "<li>";
                //                    str_Replace_str += dr_col[0]["GROUP_NAME"].ToString();
                //                    str_Replace_str += "</li>";

                //                    str_new_replace_str = "<li style='list-style: none;'>";
                //                    // str_new_replace_str += "<TABLE border ='0' align='left'>";
                //                    // str_new_replace_str += " <TR><TD><div onClick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["GROUP_NAME"].ToString().Replace(" ", "_") +"') >  " + dr_col[0]["BS_DISPLAY_NAME"].ToString() + "</div></TD></TR>";                              

                //                    //  str_new_replace_str += "</TABLE>";

                //                    str_new_replace_str += dr_col[0]["BS_DISPLAY_NAME"].ToString() + " <div style='float:right'  onClick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["GROUP_NAME"].ToString().Replace(" ", "_") + "') > <img src='../Images/RightMark.png' alt='select'/> </div> ";
                //                    str_new_replace_str += "<UL>";



                //                    var var_empList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_ID"].ToString().Contains(dr_col[0]["BS_TMPL_ID"].ToString()));
                //                    str_tmp = "";
                //                    if (var_empList.Any())
                //                    {
                //                        DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                //                        for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                //                        {
                //                            str_tmp += "<li>";
                //                            str_tmp += dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                //                            str_tmp += "</li>";

                //                            dr = dt_EmpProcessList.NewRow();
                //                            dr["GROUP_NAME"] = dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                //                            dr["BS_TMPL_ID"] = dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                //                            dr["BS_DISPLAY_NAME"] = dt_EMP_List.Rows[jLoop]["BS_DISPLAY_NAME"].ToString();
                //                            dr["STATUS"] = "N";
                //                            dt_EmpProcessList.Rows.Add(dr);

                //                        }
                //                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                //                    }
                //                    else
                //                    {
                //                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                //                    }

                //                    dr_col[0]["STATUS"] = "Y";
                //                    dt_EmpProcessList.AcceptChanges();
                //                }
                //                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                //            } while (dr_col.Length > 0);

                //        }
                //        str_ORG_STR += " </ul>";
                //        str_ORG_STR += "  </li> ";
                //        str_ORG_STR += "  </ul> ";

                //    }
                //}
                //else
                //{
                //    str_ORG_STR += "  <ul  id='BSTemp'> ";
                //    str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + txtTempName.Text + "</div>";
                //    str_ORG_STR += "  </li> ";
                //    str_ORG_STR += "  </ul> ";
                //}

                // ------------------------MODEL 2 -------------------
                if (dt_BS_TMPL_DATA.Rows.Count > 0)
                {
                    DataTable dt_EmpProcessList = new DataTable();
                    dt_EmpProcessList.Columns.Add("GROUP_NAME");
                    dt_EmpProcessList.Columns.Add("BS_TMPL_ID");
                    dt_EmpProcessList.Columns.Add("BS_DISPLAY_NAME");

                    dt_EmpProcessList.Columns.Add("STATUS");

                    var var_HeadEmpList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_GROUP_NAME"].ToString().Contains("HEAD"));
                    if (var_HeadEmpList.Any())
                    {
                        DataTable dt_data = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);



                        string str_Replace_str = "";
                        string str_new_replace_str = "";
                        string str_tmp;
                        str_ORG_STR += "  <ul  id='BSTemp'  > ";
                        str_ORG_STR += "  <li><a onclick=fn_GroupAccClick('','" + txtTempName.Text + "')> " + txtTempName.Text + "</a>";
                        str_ORG_STR += " <ul>";
                        for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                        {
                            DataRow dr = dt_EmpProcessList.NewRow();
                            dr["GROUP_NAME"] = dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                            dr["BS_TMPL_ID"] = dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            dr["BS_DISPLAY_NAME"] = dt_data.Rows[iLoop]["BS_DISPLAY_NAME"].ToString();
                            dr["STATUS"] = "N";
                            dt_EmpProcessList.Rows.Add(dr);

                            str_ORG_STR += "<li>";
                            str_ORG_STR += dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            str_ORG_STR += "</li>";
                            DataRow[] dr_col;
                            do
                            {
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                                if (dr_col.Length > 0)
                                {
                                    str_Replace_str = "<li>";
                                    str_Replace_str += dr_col[0]["BS_TMPL_ID"].ToString();
                                    str_Replace_str += "</li>";
                                    str_new_replace_str = "<li> <a onclick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["BS_DISPLAY_NAME"].ToString().Replace(" ", "_") + "')> " + dr_col[0]["BS_DISPLAY_NAME"].ToString() + "</a>";
                                    str_new_replace_str += "<UL>";

                                    var var_empList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_ID"].ToString().Contains(dr_col[0]["BS_TMPL_ID"].ToString()));
                                    str_tmp = "";
                                    if (var_empList.Any())
                                    {
                                        DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                                        for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                                        {
                                            str_tmp += "<li>";
                                            str_tmp += dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            str_tmp += "</li>";

                                            dr = dt_EmpProcessList.NewRow();
                                            dr["GROUP_NAME"] = dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                                            dr["BS_TMPL_ID"] = dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            dr["BS_DISPLAY_NAME"] = dt_EMP_List.Rows[jLoop]["BS_DISPLAY_NAME"].ToString();
                                            dr["STATUS"] = "N";
                                            dt_EmpProcessList.Rows.Add(dr);

                                        }
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                                    }
                                    else
                                    {
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                                    }

                                    dr_col[0]["STATUS"] = "Y";
                                    dt_EmpProcessList.AcceptChanges();
                                }
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                            } while (dr_col.Length > 0);

                        }
                        str_ORG_STR += " </ul>";
                        str_ORG_STR += "  </li> ";
                        str_ORG_STR += "  </ul> ";

                    }
                }
                else
                {
                    str_ORG_STR += "  <ul  id='BSTemp'> ";
                    str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + txtTempName.Text + "</div>";
                    str_ORG_STR += "  </li> ";
                    str_ORG_STR += "  </ul> ";
                }

                div_Template.InnerHtml = str_ORG_STR;
                ScriptManager.RegisterStartupScript(imgbtnAdd, typeof(Button), "LoadGraph", "fn_showGraph()", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        protected void rbl_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_Show_GROUPAcc();
            Reload_BS_Structure();
        }
        private void LoadAccount(DataTable dt_Acct)
        {
            Session["ACCTCODE_LIST"] = dt_Acct;

            gv_All_Acct.DataSource = dt_Acct;
            gv_All_Acct.DataBind();

            gv_Sel_Acct.DataSource = dt_Acct;
            gv_Sel_Acct.DataBind();

        }

        private void LoadSegment(DataTable dt_Seg)
        {
            Session["SEGMENT_LIST"] = dt_Seg;

            gv_All_Segment.DataSource = dt_Seg;
            gv_All_Segment.DataBind();

            gv_Sel_Segment.DataSource = dt_Seg;
            gv_Sel_Segment.DataBind();

        }
        private void fn_Show_GROUPAcc()
        {
            if (rbl_Type.SelectedValue.ToString().ToUpper() == "GROUP")
            {
                div_Group.Visible = true;
                div_acc.Visible = false;
                div_Segment.Visible = false;
            }
            else if (rbl_Type.SelectedValue.ToString().ToUpper() == "ACCOUNT")
            {
                div_Group.Visible = false;
                div_acc.Visible = true;
                div_Segment.Visible = false;
                DataTable dt_Acct = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.getAccCode4FinTempSegment(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                LoadAccount(dt_Acct);

                DataRow[] dr = dt_Acct.Select("SELECTED_REC='Y'");
                if (dr.Length > 0)
                {
                    GL_BALANCE_SHEET_TEMPLATE obj_new = new GL_BALANCE_SHEET_TEMPLATE();
                    using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                    {
                        obj_new = userCtx.Find(r =>
                            (r.BS_TMPL_ID == dr[0]["BS_TMPL_ID"].ToString())
                            ).SingleOrDefault();
                    }
                    if (obj_new != null)
                    {
                        rbAmountType.SelectedValue = obj_new.AMOUNT_TYPE;
                    }
                }

            }
            else if (rbl_Type.SelectedValue.ToString().ToUpper() == "SEGMENT")
            {
                div_Segment.Visible = true;
                div_Group.Visible = false;
                div_acc.Visible = false;
                DataTable dt_Segment = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.getSegmentValue(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                LoadSegment(dt_Segment);
            }



        }

        protected void txtTempName_TextChanged(object sender, EventArgs e)
        {
            Reload_BS_Structure();

        }

        protected void imgBtnEdit_Click(object sender, ImageClickEventArgs e)
        {
            LoadRecord();
        }


        private void LoadRecord()
        {
            try
            {
                if (hf_BS_TMPL_GROUP_ID.Value.ToString().Length > 0)
                {
                    GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
                    using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                    {
                        gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                            (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                            ).SingleOrDefault();
                    }
                    if (gL_BALANCE_SHEET_TEMPLATE != null)
                    {
                        rbl_Type.SelectedValue = gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString().ToUpper();
                        fn_Show_GROUPAcc();
                        if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString().ToUpper() == "GROUP")
                        {
                            txtGroupName.Text = gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NAME.ToString();
                            txtGroupNumber.Text = "";
                            if (gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NO != null)
                            {
                                txtGroupNumber.Text = gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NO.ToString();
                            }
                            chkTotReq.Checked = false;
                            if (gL_BALANCE_SHEET_TEMPLATE.BS_TOT_REQ.ToString() == FINAppConstants.Y)
                            {
                                chkTotReq.Checked = true;
                            }
                            lblSelectedGroup.Text = "Selected Group : " + txtGroupName.Text;
                            imgBtnUpdate.Visible = true;

                            txtNotes.Text = "";
                            if (gL_BALANCE_SHEET_TEMPLATE.NOTES != null)
                            {
                                txtNotes.Text = gL_BALANCE_SHEET_TEMPLATE.NOTES.ToString();
                            }
                            chkFormulaReq.Checked = false;

                            if (gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA_GROUP == FINAppConstants.Y)
                            {
                                chkFormulaReq.Checked = true;
                                ShowFormulaColumn();
                                txtFormula.Text = gL_BALANCE_SHEET_TEMPLATE.BS_FORMULA.ToString();
                            }
                            imgbtnAdd.Visible = false;
                            imgBtnDelete.Visible = true;

                        }
                        else if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString().ToUpper() == "ACCOUNT")
                        {
                            DataTable dt_Acct = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCode4FinTempSegment(gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID)).Tables[0];
                            LoadAccount(dt_Acct);
                            hf_BS_TMPL_GROUP_ID.Value = gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID.ToString();

                            rbAmountType.SelectedValue = gL_BALANCE_SHEET_TEMPLATE.AMOUNT_TYPE;

                            imgbtnAdd.Visible = true;
                            imgBtnUpdate.Visible = false;
                            imgBtnDelete.Visible = false;
                        }
                        else if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString().ToUpper() == "SEGMENT")
                        {
                            DataTable dt_Segment = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.getSegmentValue(gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID)).Tables[0];
                            LoadSegment(dt_Segment);

                            hf_BS_TMPL_GROUP_ID.Value = gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_PARENT_ID.ToString();
                            imgbtnAdd.Visible = true;
                            imgBtnUpdate.Visible = false;
                            imgBtnDelete.Visible = false;
                        }

                        hf_BS_TMPL_ID.Value = gL_BALANCE_SHEET_TEMPLATE.BS_TMPL_ID.ToString();


                    }
                }
                Reload_BS_Structure();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void imgbtnAdd_Click(object sender, ImageClickEventArgs e)
        {
            AddGroupAccDet();
            Reload_BS_Structure();
        }

        protected void imgBtnUpdate_Click(object sender, ImageClickEventArgs e)
        {
            AddGroupAccDet();
            Reload_BS_Structure();
        }

        protected void imgBtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (hf_BS_TMPL_ID.Value.ToString().Length > 0)
                {
                    GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
                    using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
                    {
                        gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                            (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                            ).SingleOrDefault();
                    }
                    if (gL_BALANCE_SHEET_TEMPLATE != null)
                    {
                        if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString().ToUpper() == "ACCOUNT")
                        {
                            DBMethod.DeleteEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                        }
                        else
                        {

                            DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_TemplateData4TmplId(hf_BS_TMPL_GROUP_ID.Value.ToString())).Tables[0];
                            if (dt_data.Rows.Count > 0)
                            {
                                ErrorCollection.Add("InvalidDelet", " Please Delete Child Data First ");
                                return;
                            }
                            else
                            {
                                DBMethod.DeleteEntity<GL_BALANCE_SHEET_TEMPLATE>(gL_BALANCE_SHEET_TEMPLATE);
                            }

                        }
                    }
                }
                rbl_Type.SelectedIndex = 0;
                fn_Show_GROUPAcc();
                txtGroupName.Text = "";

                txtGroupNumber.Text = "";
                chkTotReq.Checked = false;
                hf_BS_TMPL_ID.Value = "";
                hf_BS_TMPL_GROUP_ID.Value = "";
                txtFormula.Text = "";
                lblSelectedGroup.Text = "";
                imgbtnAdd.Visible = true;
                imgBtnUpdate.Visible = false;
                imgBtnDelete.Visible = false;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {
                Reload_BS_Structure();
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void chkFormulaReq_CheckedChanged(object sender, EventArgs e)
        {
            ShowFormulaColumn();
        }
        private void ShowFormulaColumn()
        {
            if (chkFormulaReq.Checked)
            {
                div_Formula.Visible = true;
                txtFormula.Visible = true;
                FillGroupNumber();
                chkTotReq.Checked = false;
            }
            else
            {
                div_Formula.Visible = false;
                txtFormula.Visible = false;
            }
        }

        private void FillGroupNumber()
        {
            FIN.BLL.GL.FinancialTemplate_Segment_BLL.fn_getTemplateGroupNumber(ref ddlGroupNuber, txtTempName.Text);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFormula.Text.ToString().Contains("#" + ddlGroupNuber.SelectedValue.ToString() + "#"))
                {
                    ErrorCollection.Add("InvalidGroup", "Selected Group already available in the formula");
                    return;
                }

                if (ddlGroupNuber.SelectedValue.ToString().Length > 0)
                {
                    txtFormula.Text = txtFormula.Text + "#" + ddlGroupNuber.SelectedValue.ToString() + "#";
                }
                if (ddlOperator.SelectedValue.ToString().Length > 0)
                {
                    txtFormula.Text = txtFormula.Text + ddlOperator.SelectedValue.ToString();
                }
                ddlOperator.SelectedIndex = 0;
                ddlGroupNuber.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeleteBS", ex.Message);
            }
            finally
            {
                Reload_BS_Structure();
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void chkTotReq_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTotReq.Checked)
            {
                chkFormulaReq.Checked = false;

            }

            ShowFormulaColumn();

        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            Reload_BS_Structure();
        }



        protected void gv_All_Acct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (gv_All_Acct.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "Y")
                {
                    e.Row.Visible = false;
                }
                if (txtSearchAcct.Text.ToString().Length > 0)
                {
                    if (!gv_All_Acct.DataKeys[e.Row.RowIndex].Values["CODE_NAME"].ToString().ToUpper().Contains(txtSearchAcct.Text.ToString().ToUpper()))
                    {
                        e.Row.Visible = false;
                    }
                }
            }
        }

        protected void gv_Sel_Acct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (gv_Sel_Acct.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "N")
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void txtSearchAcct_TextChanged(object sender, EventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];
            LoadAccount(dt_Acct);
        }

        protected void lb_AcctName_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_All_Acct.Value = gv_All_Acct.DataKeys[gvr.RowIndex]["CODE_ID"].ToString();
        }

        protected void lb_AcctName_Click1(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Sel_Acct.Value = gv_Sel_Acct.DataKeys[gvr.RowIndex]["CODE_ID"].ToString();
        }

        protected void img_OneAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];

            for (int rloop = 0; rloop < gv_All_Acct.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Acct.Rows[rloop].FindControl("chkAll_Acct");
                if (chk_tmp.Checked)
                {
                    hf_All_Acct.Value = gv_All_Acct.DataKeys[rloop]["CODE_ID"].ToString();
                    DataRow[] dr = dt_Acct.Select("CODE_ID='" + hf_All_Acct.Value + "'");
                    dr[0]["SELECTED_REC"] = "Y";
                }

            }
            LoadAccount(dt_Acct);

        }

        protected void img_oneRemove_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];

            for (int rloop = 0; rloop < gv_Sel_Acct.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_Sel_Acct.Rows[rloop].FindControl("chkSel_Acct");
                if (chk_tmp.Checked)
                {
                    hf_Sel_Acct.Value = gv_Sel_Acct.DataKeys[rloop]["CODE_ID"].ToString();
                    DataRow[] dr = dt_Acct.Select("CODE_ID='" + hf_Sel_Acct.Value + "'");
                    dr[0]["SELECTED_REC"] = "N";
                }
            }
            LoadAccount(dt_Acct);
        }

        protected void img_AllAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];
            for (int iLoop = 0; iLoop < dt_Acct.Rows.Count; iLoop++)
            {
                dt_Acct.Rows[iLoop]["SELECTED_REC"] = "Y";
            }
            dt_Acct.AcceptChanges();
            LoadSegment(dt_Acct);
        }

        protected void img_Allremove_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Acct = (DataTable)Session["ACCTCODE_LIST"];
            for (int iLoop = 0; iLoop < dt_Acct.Rows.Count; iLoop++)
            {
                dt_Acct.Rows[iLoop]["SELECTED_REC"] = "N";
            }
            dt_Acct.AcceptChanges();
            LoadSegment(dt_Acct);
        }

        protected void gv_All_Segment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (gv_All_Segment.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "Y")
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void gv_Sel_Segment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (gv_Sel_Segment.DataKeys[e.Row.RowIndex].Values["SELECTED_REC"].ToString() == "N")
                {
                    e.Row.Visible = false;
                }
            }
        }

        protected void lbl_AllSegName_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Seg_AllSegmentId.Value = gv_All_Segment.DataKeys[gvr.RowIndex]["SEGMENT_ID"].ToString();
        }

        protected void lbl_Sel_Segment_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Seg_SelSegmentId.Value = gv_Sel_Segment.DataKeys[gvr.RowIndex]["SEGMENT_ID"].ToString();
        }
        protected void img_Seg_OneAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Segment = (DataTable)Session["SEGMENT_LIST"];

            for (int rloop = 0; rloop < gv_All_Segment.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_All_Segment.Rows[rloop].FindControl("chkAll_Segment");
                if (chk_tmp.Checked)
                {
                    hf_Seg_AllSegmentId.Value = gv_All_Segment.DataKeys[rloop]["SEGMENT_ID"].ToString();
                    DataRow[] dr = dt_Segment.Select("SEGMENT_ID='" + hf_Seg_AllSegmentId.Value + "'");
                    dr[0]["SELECTED_REC"] = "Y";
                }
            }

            LoadSegment(dt_Segment);
        }

        protected void img_Seg_OneRemove_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Segment = (DataTable)Session["SEGMENT_LIST"];

            for (int rloop = 0; rloop < gv_Sel_Segment.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gv_Sel_Segment.Rows[rloop].FindControl("chkSel_Segment");
                if (chk_tmp.Checked)
                {
                    hf_Seg_AllSegmentId.Value = gv_Sel_Segment.DataKeys[rloop]["SEGMENT_ID"].ToString();
                    DataRow[] dr = dt_Segment.Select("SEGMENT_ID='" + hf_Seg_AllSegmentId.Value + "'");
                    dr[0]["SELECTED_REC"] = "N";
                }
            }

            LoadSegment(dt_Segment);
        }

        protected void img_Seg_AllAdd_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Segment = (DataTable)Session["SEGMENT_LIST"];
            for (int iLoop = 0; iLoop < dt_Segment.Rows.Count; iLoop++)
            {
                dt_Segment.Rows[iLoop]["SELECTED_REC"] = "Y";
            }
            dt_Segment.AcceptChanges();
            LoadSegment(dt_Segment);

        }

        protected void img_Seg_AllRemove_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Segment = (DataTable)Session["SEGMENT_LIST"];
            for (int iLoop = 0; iLoop < dt_Segment.Rows.Count; iLoop++)
            {
                dt_Segment.Rows[iLoop]["SELECTED_REC"] = "N";
            }
            dt_Segment.AcceptChanges();
            LoadSegment(dt_Segment);
        }

        protected void btnSelectData_Click(object sender, EventArgs e)
        {
            imgbtnAdd.Visible = true;
            imgBtnUpdate.Visible = false;
            imgBtnDelete.Visible = false;
            GL_BALANCE_SHEET_TEMPLATE gL_BALANCE_SHEET_TEMPLATE = new GL_BALANCE_SHEET_TEMPLATE();
            using (IRepository<GL_BALANCE_SHEET_TEMPLATE> userCtx = new DataRepository<GL_BALANCE_SHEET_TEMPLATE>())
            {
                gL_BALANCE_SHEET_TEMPLATE = userCtx.Find(r =>
                    (r.BS_TMPL_ID == hf_BS_TMPL_GROUP_ID.Value.ToString())
                    ).SingleOrDefault();
            }
            if (gL_BALANCE_SHEET_TEMPLATE != null)
            {
                if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString() == "GROUP")
                {
                    lblSelectedGroup.Text = "Selected Group : ";
                }
                else if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString() == "ACCOUNT")
                {
                    lblSelectedGroup.Text = "Selected Account : ";
                }
                else if (gL_BALANCE_SHEET_TEMPLATE.BS_TYPE.ToString() == "SEGMENT")
                {
                    lblSelectedGroup.Text = "Selected Segment : ";
                }
                lblSelectedGroup.Text = lblSelectedGroup.Text + gL_BALANCE_SHEET_TEMPLATE.BS_GROUP_NAME + gL_BALANCE_SHEET_TEMPLATE.BS_ACC_NAME;
                rbl_Type.SelectedValue = "GROUP";
                txtGroupName.Text = "";
                txtGroupNumber.Text = "";
                txtNotes.Text = "";
                hf_BS_TMPL_ID.Value = "";

            }
            else
            {
                lblSelectedGroup.Text = " Selected Template : " + txtTempName.Text;
                txtGroupName.Text = "";
                txtGroupNumber.Text = "";
                txtNotes.Text = "";
                hf_BS_TMPL_ID.Value = "";
            }
        }


    }
}