﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class CurrencyEntry : PageBase
    {
        SSM_CURRENCIES sSM_CURRENCIES = new SSM_CURRENCIES();

        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SSM_CURRENCIES> userCtx = new DataRepository<SSM_CURRENCIES>())
                    {
                        sSM_CURRENCIES = userCtx.Find(r =>
                            (r.CURRENCY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_CURRENCIES;

                    txtCurrencyCode.Text = sSM_CURRENCIES.CURRENCY_CODE;
                    txtCurrencyDescription.Text = sSM_CURRENCIES.CURRENCY_DESC;
                    txtCurrencyDescriptionol.Text = sSM_CURRENCIES.CURRENCY_DESC_OL;
                    txtCurrencySymbol.Text = sSM_CURRENCIES.CURRENCY_SYMBOL;
                    ddlInitialAmountSeparator.SelectedValue = sSM_CURRENCIES.CURRENCY_AMT_SEPARATOR;
                    ddlSubsequentAmountSeparator.SelectedValue = sSM_CURRENCIES.CURRENCY_SUB_SEQ_SEPRATOR;
                    if (sSM_CURRENCIES.STANDARD_PRECISION != null)
                    {
                        ddldecpre.SelectedValue = sSM_CURRENCIES.STANDARD_PRECISION.ToString();
                    }
                    if (sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR != null)
                    {
                        ddldecsep.SelectedValue = sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR.ToString();
                    }
                    //txtDecimalPrecision.Text = sSM_CURRENCIES.STANDARD_PRECISION.ToString();
                    //txtDecimalSeperator.Text = sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR.ToString();

                    chkActive.Checked = true;
                    if (sSM_CURRENCIES.ENABLED_FLAG == FINAppConstants.N)
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_CURRENCIES = (SSM_CURRENCIES)EntityData;
                }



                sSM_CURRENCIES.CURRENCY_CODE = txtCurrencyCode.Text;
                sSM_CURRENCIES.CURRENCY_DESC = txtCurrencyDescription.Text;
                sSM_CURRENCIES.CURRENCY_DESC_OL = txtCurrencyDescriptionol.Text;
                sSM_CURRENCIES.CURRENCY_SYMBOL = txtCurrencySymbol.Text;
                sSM_CURRENCIES.CURRENCY_AMT_SEPARATOR = ddlInitialAmountSeparator.SelectedValue.ToString();

                sSM_CURRENCIES.CURRENCY_SUB_SEQ_SEPRATOR = ddlSubsequentAmountSeparator.SelectedValue.ToString();
                //if (txtDecimalPrecision.Text.ToString().Trim().Length > 0)
                //{
                //    sSM_CURRENCIES.STANDARD_PRECISION = int.Parse(txtDecimalPrecision.Text.ToString());
                //}
                //if (txtDecimalSeperator.Text.ToString().Trim().Length > 0)
                //{
                //    sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR = int.Parse(txtDecimalSeperator.Text.ToString());
                //}
                sSM_CURRENCIES.STANDARD_PRECISION = ddldecpre.SelectedValue.ToString();
                sSM_CURRENCIES.CURRENCY_DEC_SEPARATOR = ddldecsep.SelectedValue.ToString();
                sSM_CURRENCIES.CURRENCY_SYMBOL = txtCurrencySymbol.Text;

                if (chkActive.Checked == true)
                {
                    sSM_CURRENCIES.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    sSM_CURRENCIES.ENABLED_FLAG = FINAppConstants.N;
                }


                //sSM_CURRENCIES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sSM_CURRENCIES.MODIFIED_BY = this.LoggedUserName;
                    sSM_CURRENCIES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sSM_CURRENCIES.CURRENCY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0001.ToString(), false, true);
                    sSM_CURRENCIES.CREATED_BY = this.LoggedUserName;
                    sSM_CURRENCIES.CREATED_DATE = DateTime.Today;

                }

                sSM_CURRENCIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_CURRENCIES.CURRENCY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlInitialAmountSeparator, "AMT_SEP");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlSubsequentAmountSeparator, "AMT_SEP");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddldecpre, "IAS");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddldecsep, "SAS");
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            ProReturn = FINSP.GetSPFOR_ERR_MGR_CURRENCY_CODE(txtCurrencyCode.Text, sSM_CURRENCIES.CURRENCY_ID);


                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("CurrCode", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }

                            DBMethod.SaveEntity<SSM_CURRENCIES>(sSM_CURRENCIES);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }

                    case FINAppConstants.Update:
                        {
                            ProReturn = FINSP.GetSPFOR_ERR_MGR_CURRENCY_CODE(txtCurrencyCode.Text, sSM_CURRENCIES.CURRENCY_ID);


                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("CurrCode", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }


                            DBMethod.SaveEntity<SSM_CURRENCIES>(sSM_CURRENCIES, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_CURRENCIES>(sSM_CURRENCIES);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtCurrencySymbol_TextChanged(object sender, EventArgs e)
        {

        }


    }
}