﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client.GL
{
    public partial class DashBoard_GL : PageBase
    {

        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        DataTable dtData = new DataTable();
        DataTable dtData1 = new DataTable();
        DataSet dsData = new DataSet();
        DataTable dtTB = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {

                    FillComboBox();


                    GenerateJNLTransaction();

                    GenerateGBDetails();

                    GenerateTBChart();


                    GenerateCashbalanceChart();
                    GenerateExpensesChart();
                    GenerateRevenueChart();
                    GenerateBankbalanceChart();

                    //ChartGL.Visible = false;
                    //ChartGroupGL.Visible = false;


                    //ddlGlobalSegment.SelectedValue = "SEG_V_ID-0000000246";
                    //ddlGroupName.SelectedValue = "ACC_GRP-0000002350";
                    // ddlGroupName.SelectedItem.Text = "A - Cash in Hand";
                    //ddlAccountCode.SelectedValue = "ACC-0000004502";







                    //dtData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvaluesBasedOrg()).Tables[0];
                    //DataTable dt_Det = new DataTable();// DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.getGroupDetails()).Tables[0];

                    //AccountingGroups_DAL.GetSP_AccountGroupLinksCodeName();
                    //dt_Det = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1()).Tables[0];

                    //if (dtData.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtData.Rows.Count; i++)
                    //    {
                    //        if (dt_Det.Rows.Count > 0)
                    //        {
                    //            for (int iG = 0; iG < dt_Det.Rows.Count; iG++)
                    //            {
                    //                dsData = (FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(dtData.Rows[i]["segment_value_id"].ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), dt_Det.Rows[iG]["level1_Id"].ToString(), "0", FIN.BLL.GL.AccountingGroups_BLL.GetAccountingGroupsSummaryReportData()));
                    //            }
                    //        }
                    //    }
                    //}

                    //generateChart();
                    //GenerateGroupChart(dsData);

                    //GenerateTBChart();
                    //
                    //

                    LoadGraph();
                }
                else
                {
                    LoadGraph();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void LoadGraph()
        {


            if (Session["GLTRANS"] != null)
            {
                chrt_GLtrans.DataSource = (DataTable)Session["GLTRANS"];
                chrt_GLtrans.DataBind();
            }

            if (Session["GRPBAL"] != null)
            {
                ChartGroupGL.DataSource = (DataTable)Session["GRPBAL"];
                ChartGroupGL.DataBind();
            }

            


            if (Session["BankBal"] != null)
            {
                chartBankBal.DataSource = (DataTable)Session["BankBal"];
                chartBankBal.DataBind();

            }
           
            if (Session["RevenueBal"] != null)
            {
                chartRevenue.DataSource = (DataTable)Session["RevenueBal"];
                chartRevenue.DataBind();

            }
            if (Session["ExpensesBal"] != null)
            {
                chartExpenses.DataSource = (DataTable)Session["ExpensesBal"];
                chartExpenses.DataBind();

            }

        }
        private void FillComboBox()
        {

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_GB_FINYear);
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            if (ddl_GB_FINYear.Items.Count > 0)
            {
               // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
               ddl_GB_FINYear.SelectedValue =  str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_GB_FINPeriod, ddl_GB_FINYear.SelectedValue);
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_GB_FINPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_TBFinYear);
            if (ddl_TBFinYear.Items.Count > 0)
            {
                //ddl_TBFinYear.SelectedIndex = ddl_TBFinYear.Items.Count - 1;
                ddl_TBFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_TBFinPeriod, ddl_TBFinYear.SelectedValue);          
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_TBFinPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }


            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_GL_Trans_Year);
            if (ddl_GL_Trans_Year.Items.Count > 0)
            {
                //ddl_TBFinYear.SelectedIndex = ddl_TBFinYear.Items.Count - 1;
                ddl_GL_Trans_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_GL_Trans_Period, ddl_GL_Trans_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_GL_Trans_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }


            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_R_Year);
            if (ddl_R_Year.Items.Count > 0)
            {
                //ddl_TBFinYear.SelectedIndex = ddl_TBFinYear.Items.Count - 1;
                ddl_R_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_R_Period, ddl_R_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_R_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_E_Year);
            if (ddl_E_Year.Items.Count > 0)
            {
                //ddl_TBFinYear.SelectedIndex = ddl_TBFinYear.Items.Count - 1;
                ddl_E_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_E_Period, ddl_E_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_E_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
            //txtFromDate.Text = DateTime.Now.Date.AddDays(-30).ToString("dd/MM/yyyy");
            //txtToDate.Text =  DateTime.Now.Date.ToString("dd/MM/yyyy");

            //txtTBFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            //txtTBTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, false);
            ////FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlGroupName);
            //FIN.BLL.GL.AccountingGroups_BLL.getGroupDetails(ref ddlGroupName);

            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvalues(ref ddlTBGlobal, false);
            //if (ddlGlobalSegment.Items.Count > 0)
            //    ddlGlobalSegment.SelectedIndex = 0;
            //if (ddlGroupName.Items.Count > 1)
            //    ddlGroupName.SelectedIndex = 1;

        }
        private void GenerateJNLTransaction()
        {


            DataTable dt = new DataTable();
            dt = DBMethod.ExecuteQuery(JournalEntryDetails_DAL.getJournalTrans(ddl_GL_Trans_Period.SelectedValue)).Tables[0];
            Session["GLTRANS"] = dt;
            /*chrt_GLtrans.DataSource = dt;
            chrt_GLtrans.DataBind();

            string[] x1 = new string[dt.Rows.Count];
            int[] y1 = new int[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                x1[i] = dt.Rows[i][0].ToString();
                y1[i] = Convert.ToInt32(dt.Rows[i][1]);
            }
            pieJNT.Series[0].Points.DataBindXY(x1, y1);
            pieJNT.Series[0].ChartType = SeriesChartType.Pie;
            pieJNT.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            pieJNT.Legends[0].Enabled = true;
            ChartGroupGL.Series["Series1"].IsValueShownAsLabel = true;


            foreach (Series charts in pieJNT.Series)
            {
                foreach (DataPoint point in charts.Points)
                {
                    switch (point.AxisLabel)
                    {
                        case "Q1": point.Color = Color.Yellow; break;
                        case "Q2": point.Color = Color.BlueViolet; break;
                        case "Q3": point.Color = Color.SpringGreen; break;
                        case "Q4": point.Color = Color.Coral; break;
                    }
                    // point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

                }
            }*/
        }

        protected void ddl_GL_Trans_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_GL_Trans_Period, ddl_GL_Trans_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_GL_Trans_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateJNLTransaction();
            LoadGraph();
            
        }


        protected void ddl_R_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_R_Period, ddl_R_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_R_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateRevenueChart();
            LoadGraph();

        }

        protected void ddl_E_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_E_Period, ddl_E_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_E_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateExpensesChart();
            LoadGraph();

        }
        private void GenerateGBDetails()
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave1GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue)).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L1.DataSource = dt_GB_Details;
            GV_L1.DataBind();
            div_Level1.Visible = true;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }
        protected void ddl_GB_FINYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_GB_FINPeriod, ddl_GB_FINYear.SelectedValue);
            LoadGraph();
        }

        protected void ddl_GB_FINPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateGBDetails();
            LoadGraph();
        }
        protected void lnk_GB1_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL2Value(gvr);
        }

        private void getGBL2Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave2GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L1.DataKeys[gvr.RowIndex].Values["GB_ID1"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L2.DataSource = dt_GB_Details;
            GV_L2.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = true;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();

        }



        protected void lnk_GB2_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL3Value(gvr);
        }

        private void getGBL3Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave3GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L2.DataKeys[gvr.RowIndex].Values["GB_ID2"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L3.DataSource = dt_GB_Details;
            GV_L3.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = true;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();

        }


        protected void lnk_GB3_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL4Value(gvr);
        }

        private void getGBL4Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave4GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L3.DataKeys[gvr.RowIndex].Values["GB_ID3"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L4.DataSource = dt_GB_Details;
            GV_L4.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = true;
            div_Level5.Visible = false;

            LoadGraph();

        }

        protected void lnk_GB4_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL5Value(gvr);
        }

        private void getGBL5Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave5GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L4.DataKeys[gvr.RowIndex].Values["GB_ID4"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L5.DataSource = dt_GB_Details;
            GV_L5.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = true;

            LoadGraph();

        }

        protected void imgL2_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = true;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false ;
            LoadGraph();
        }

        protected void imgL3_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = true;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgL4_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = true;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgL5_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = true;
            div_Level5.Visible = false;
            LoadGraph();
        }
        private void GenerateTBChart()
        {
            try
            {
                ErrorCollection.Clear();


                DataTable dsTB = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getTrailBalance4Period(ddl_TBFinPeriod.SelectedValue)).Tables[0]; ;

                Session["TB"] = dsTB;

                if (dsTB.Rows.Count > 0)
                {
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OP_BAL", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OP_BAL"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANS_DR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANS_DR"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANS_CR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANS_CR"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOS_BAL", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CLOS_BAL"))));
                    dsTB.AcceptChanges();
                }

                gvTrailBal.DataSource = dsTB;
                gvTrailBal.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddl_TBFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_TBFinPeriod, ddl_TBFinYear.SelectedValue);
            LoadGraph();
        }

        protected void ddl_TBFinPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateTBChart();
            LoadGraph();
        }
        
        private void GenerateCashbalanceChart()
        {
            try
            {
                ErrorCollection.Clear();

                dtData = DBMethod.ExecuteQuery(Balances_DAL.getGlCashBalance()).Tables[0];

                Session["CashBal"] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BALANCE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BALANCE_AMT"))));                    
                    dtData.AcceptChanges();
                }
                gvCashBal.DataSource = dtData;
                gvCashBal.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void GenerateExpensesChart()
        {
            try
            {
                ErrorCollection.Clear();

                dtData = DBMethod.ExecuteQuery(AccountingGroups_DAL.GetExpensesBalance(ddl_E_Period.SelectedValue)).Tables[0];

                Session["ExpensesBal"] = dtData;
                chartExpenses.DataSource = dtData;
                chartExpenses.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void GenerateRevenueChart()
        {
            try
            {
                ErrorCollection.Clear();

                dtData = DBMethod.ExecuteQuery(AccountingGroups_DAL.GetIncomeBalance(ddl_R_Period.SelectedValue)).Tables[0];

                Session["RevenueBal"] = dtData;
                chartRevenue.DataSource = dtData;
                chartRevenue.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void GenerateBankbalanceChart()
        {
            try
            {
                ErrorCollection.Clear();

                dtData = DBMethod.ExecuteQuery(Balances_DAL.getGlBankBalance()).Tables[0];

                Session["BankBal"] = dtData;
                chartBankBal.DataSource = dtData;
                chartBankBal.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }























        protected void ddlTBAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                FIN.BLL.GL.AccountCodes_BLL.getAccountBasedGroup(ref ddlTBCode, ddlTBGlobal.SelectedValue.ToString());

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYacCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }









        protected void btnGenerateChart_Click(object sender, EventArgs e)
        {
            try
            {
                //generateChart();
                //GenerateJNLTransaction();
                //GenerateGroupChart();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void lnk_Group_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            if (Session["dsData"] != null)
            {
                dsData = (DataSet)Session["dsData"];
                LoadGroupwiseChart(dsData, gvr);
            }
        }
        protected void imgBtnGenChart_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                generateChart();
                GenerateJNLTransaction();
                dsData = (FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), ddlGroupName.SelectedValue, "0", FIN.BLL.GL.AccountingGroups_BLL.GetAccountingGroupsSummaryReportData()));
                Session["dsData"] = dsData;
                GenerateGroupChart(dsData);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void imgBtnGenTBChart_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                GenerateTBChart();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadGroupwiseChart(DataSet dsData, GridViewRow gvr)
        {
            DataTable dtGroupData = dsData.Tables[0];
            dtGroupData = dsData.Tables[0];
            var query = from r in dsData.Tables[0].AsEnumerable()
                            .Where(r => r["acct_group_ID"].ToString().Contains(gvDeptDet.DataKeys[gvr.RowIndex].Values["acct_group_ID"].ToString()))
                        select new
                        {
                            AccountDesc = r.Field<string>("ACCOUNT_GROUP_DESCRIPTION"),
                            TransCredit = r.Field<Decimal>("TRANSACTION_CREDIT"),
                            TransDebit = r.Field<Decimal>("TRANSACTION_DEBIT"),
                            OPCredit = r.Field<Decimal>("OPENING_BALANCE"),
                            OPDebit = r.Field<Decimal>("CLOSING_BALANCE"),
                            CBCredit = r.Field<Decimal>("CLOSING_BALANCE_CREDIT"),
                            CBDebit = r.Field<Decimal>("CLOSING_BALANCE_DEBIT")
                        };

            DataTable dtGroupData1 = new DataTable();
            dtGroupData1.Columns.Add("Account_Group");
            dtGroupData1.Columns.Add("TransCredit");
            dtGroupData1.Columns.Add("TransDebit");
            dtGroupData1.Columns.Add("OPCredit");
            dtGroupData1.Columns.Add("OPDebit");
            dtGroupData1.Columns.Add("CBCredit");
            dtGroupData1.Columns.Add("CBDebit");
            dtGroupData1.Columns.Add("acct_group_ID");

            foreach (var item in query)
            {
                dtGroupData1.Rows.Add(item.AccountDesc, item.TransCredit, item.TransDebit, item.OPCredit, item.OPDebit, item.CBCredit, item.CBDebit);
            }
            ChartGroupGL.DataSource = dtGroupData1;
            ChartGroupGL.DataBind();
        }
        protected void generateChart()
        {
            try
            {
                DataSet dsData = new DataSet();
                dsData = (FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), ddlGroupName.SelectedValue, "0", FIN.BLL.GL.AccountingGroups_BLL.GetAccountingGroupsSummaryReportData()));

                ChartGL.Series["GLCreditSeries"].ChartType = SeriesChartType.Spline;
                ChartGL.Series["GLDebitSeries"].ChartType = SeriesChartType.Spline;
                ChartGL.Series["GLCreditSeries"]["DrawingStyle"] = "Default";
                ChartGL.Series["GLDebitSeries"]["DrawingStyle"] = "Default";
                ChartGL.ChartAreas["ChartAreaGL"].Area3DStyle.Enable3D = false;
                ChartGL.Series["GLCreditSeries"].IsValueShownAsLabel = false;
                ChartGL.Series["GLDebitSeries"].IsValueShownAsLabel = false;

                ChartGL.Series["GLCreditSeries"].BorderWidth = 2;
                ChartGL.Series["GLDebitSeries"].BorderWidth = 2;

                ChartGL.Series["GLCreditSeries"].IsXValueIndexed = true;
                ChartGL.Series["GLDebitSeries"].IsXValueIndexed = true;

                ChartGL.Series["GLCreditSeries"]["StackedGroupName"] = "Group1";
                ChartGL.Series["GLDebitSeries"]["StackedGroupName"] = "Group1";

                ChartGL.DataSource = dsData.Tables[0];
                ChartGL.DataBind();

                ChartGL.Series["GLCreditSeries"].XValueMember = "ACCOUNT_GROUP_DESCRIPTION";
                ChartGL.Series["GLCreditSeries"].YValueMembers = "TRANSACTION_CREDIT";

                ChartGL.Series["GLDebitSeries"].XValueMember = "ACCOUNT_GROUP_DESCRIPTION";
                ChartGL.Series["GLDebitSeries"].YValueMembers = "TRANSACTION_DEBIT";


                ChartGL.Series["GLCreditSeries"].Color = System.Drawing.Color.IndianRed;
                ChartGL.Series["GLCreditSeries"].Legend = "Default";
                ChartGL.Series["GLCreditSeries"].LegendText = "GL Credit";

                ChartGL.Series["GLDebitSeries"].Color = System.Drawing.Color.DarkCyan;
                ChartGL.Series["GLDebitSeries"].Legend = "Default";
                ChartGL.Series["GLDebitSeries"].LegendText = "GL Debit";


                ChartGL.Legends["Default"].BackColor = Color.Transparent;
                ChartGL.Legends["Default"].LegendStyle = LegendStyle.Column;
                ChartGL.Legends["Default"].ForeColor = Color.Brown;
                ChartGL.Legends["Default"].Docking = Docking.Bottom;


                ChartGL.Legends["Default"].BorderColor = Color.Transparent;
                ChartGL.Legends["Default"].BackSecondaryColor = Color.Transparent;
                ChartGL.Legends["Default"].BackGradientStyle = GradientStyle.None;
                ChartGL.Legends["Default"].BorderColor = Color.Brown;
                ChartGL.Legends["Default"].BorderWidth = 1;
                ChartGL.Legends["Default"].BorderDashStyle = ChartDashStyle.Solid;
                ChartGL.Legends["Default"].ShadowOffset = 1;

                //ChartGL.Series["Series1"]["PixelPointWidth"] = "50";
                //ChartGL.Series["Series2"]["PixelPointWidth"] = "50";


                ChartGL.BackColor = Color.Transparent;
                ChartGL.ChartAreas["ChartAreaGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                ChartGL.ChartAreas["ChartAreaGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

                ChartGL.ChartAreas["ChartAreaGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                ChartGL.ChartAreas["ChartAreaGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };


                ChartGL.ChartAreas["ChartAreaGL"].Area3DStyle.Enable3D = false;
                ChartGL.ChartAreas["ChartAreaGL"].BackColor = Color.Transparent;
                ChartGL.ChartAreas["ChartAreaGL"].AxisX.MajorGrid.Enabled = false;
                ChartGL.ChartAreas["ChartAreaGL"].AxisY.MajorGrid.Enabled = true;
                ChartGL.ChartAreas["ChartAreaGL"].Position.Width = 100;
                ChartGL.ChartAreas["ChartAreaGL"].Position.Height = 100;


                ChartGL.ChartAreas["ChartAreaGL"].AxisX.IsMarginVisible = true;
                ChartGL.ChartAreas["ChartAreaGL"].Position.X = 6;
                ChartGL.ChartAreas["ChartAreaGL"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


                ChartGL.ChartAreas["ChartAreaGL"].AxisY.IsStartedFromZero = true;
                ChartGL.ChartAreas["ChartAreaGL"].AxisY.Title = "Total Amount";
                ChartGL.ChartAreas["ChartAreaGL"].AxisX.Title = "GL Credit/Debit";
                ChartGL.ChartAreas["ChartAreaGL"].AxisY.LabelStyle.Format = "#,##,##0.##0";// "{900:C}";"#,##0;-#,##0;0"

                ChartGL.ChartAreas["ChartAreaGL"].AxisY.TitleFont = new Font("Verdana", 9, FontStyle.Regular);
                ChartGL.ChartAreas["ChartAreaGL"].AxisY.TitleForeColor = Color.Blue;

                ChartGL.ChartAreas["ChartAreaGL"].AxisX.TitleFont = new Font("Verdana", 9, FontStyle.Regular);
                ChartGL.ChartAreas["ChartAreaGL"].AxisX.TitleForeColor = Color.Blue;


                // ChartGL.ChartAreas["ChartAreaGL"].AxisX.LabelStyle.Angle = -90;
                //ChartGL.DataBind();


                //CustomLabel fccl = new CustomLabel();
                //fccl.FromPosition = 0.50;
                //fccl.ToPosition = 1;
                //fccl.Text = "1 \n\n";
                //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fccl);




            }
            catch (Exception ex)
            {

            }
            finally
            {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
        }

        protected void GenerateGroupChart(DataSet dsData)
        {
            try
            {

                DataTable dtGroup = new DataTable();

                //gvGridCostCentre.DataSource = dsData;
                //gvGridCostCentre.DataBind();

                DataTable dtGroupData = new DataTable();
                dtGroupData = dsData.Tables[0];
                var query = from r in dsData.Tables[0].AsEnumerable()
                            where r.Field<string>("ACCOUNT_GROUP_DESCRIPTION") == (ddlGroupName.SelectedItem.Text)
                            select new
                            {
                                AccountDesc = r.Field<string>("ACCOUNT_GROUP_DESCRIPTION"),
                                TransCredit = r.Field<Decimal>("TRANSACTION_CREDIT"),
                                TransDebit = r.Field<Decimal>("TRANSACTION_DEBIT"),
                                OPCredit = r.Field<Decimal>("OPENING_BALANCE"),
                                OPDebit = r.Field<Decimal>("CLOSING_BALANCE"),
                                CBCredit = r.Field<Decimal>("CLOSING_BALANCE_CREDIT"),
                                CBDebit = r.Field<Decimal>("CLOSING_BALANCE_DEBIT")
                            };

                DataTable dtGroupData1 = new DataTable();
                dtGroupData1.Columns.Add("Account_Group");
                dtGroupData1.Columns.Add("TransCredit");
                dtGroupData1.Columns.Add("TransDebit");
                dtGroupData1.Columns.Add("OPCredit");
                dtGroupData1.Columns.Add("OPDebit");
                dtGroupData1.Columns.Add("CBCredit");
                dtGroupData1.Columns.Add("CBDebit");

                foreach (var item in query)
                {
                    dtGroupData1.Rows.Add(item.AccountDesc, item.TransCredit, item.TransDebit, item.OPCredit, item.OPDebit, item.CBCredit, item.CBDebit);
                }

                Session["GRPBAL"] = dtGroupData1;
                // ChartGroupGL1.DataSource = dtGroupData1;
                //  ChartGroupGL1.DataBind();


                ChartGroupGL.Series["Series1"].ChartType = SeriesChartType.Column;
                ChartGroupGL.Series["Series2"].ChartType = SeriesChartType.Column;
                ChartGroupGL.Series["Series3"].ChartType = SeriesChartType.Column;
                ChartGroupGL.Series["Series4"].ChartType = SeriesChartType.Column;
                ChartGroupGL.Series["Series5"].ChartType = SeriesChartType.Column;
                ChartGroupGL.Series["Series6"].ChartType = SeriesChartType.Column;

                ChartGroupGL.Series["Series1"]["DrawingStyle"] = "Default";
                ChartGroupGL.Series["Series2"]["DrawingStyle"] = "Default";
                ChartGroupGL.Series["Series3"]["DrawingStyle"] = "Default";
                ChartGroupGL.Series["Series4"]["DrawingStyle"] = "Default";
                ChartGroupGL.Series["Series5"]["DrawingStyle"] = "Default";
                ChartGroupGL.Series["Series6"]["DrawingStyle"] = "Default";

                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Area3DStyle.Enable3D = true;

                ChartGroupGL.Series["Series1"].IsValueShownAsLabel = false;
                ChartGroupGL.Series["Series2"].IsValueShownAsLabel = false;
                ChartGroupGL.Series["Series3"].IsValueShownAsLabel = false;
                ChartGroupGL.Series["Series4"].IsValueShownAsLabel = false;
                ChartGroupGL.Series["Series5"].IsValueShownAsLabel = false;
                ChartGroupGL.Series["Series6"].IsValueShownAsLabel = false;

                ChartGroupGL.Series["Series1"].BorderWidth = 2;
                ChartGroupGL.Series["Series2"].BorderWidth = 2;
                ChartGroupGL.Series["Series3"].BorderWidth = 2;
                ChartGroupGL.Series["Series4"].BorderWidth = 2;
                ChartGroupGL.Series["Series5"].BorderWidth = 2;
                ChartGroupGL.Series["Series6"].BorderWidth = 2;
                //ChartGroupGL.Series["Series1"].IsXValueIndexed = true;
                //ChartGroupGL.Series["Series2"].IsXValueIndexed = true;

                ChartGroupGL.Series["Series1"]["StackedGroupName"] = "Group1";
                ChartGroupGL.Series["Series2"]["StackedGroupName"] = "Group1";
                ChartGroupGL.Series["Series3"]["StackedGroupName"] = "Group1";
                ChartGroupGL.Series["Series4"]["StackedGroupName"] = "Group1";
                ChartGroupGL.Series["Series5"]["StackedGroupName"] = "Group1";
                ChartGroupGL.Series["Series6"]["StackedGroupName"] = "Group1";


                ChartGroupGL.DataSource = dtGroupData1;
                ChartGroupGL.DataBind();

                ChartGroupGL.Series["Series1"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series1"].YValueMembers = "TransCredit";

                ChartGroupGL.Series["Series2"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series2"].YValueMembers = "TransDebit";

                ChartGroupGL.Series["Series3"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series3"].YValueMembers = "OPCredit";

                ChartGroupGL.Series["Series4"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series4"].YValueMembers = "OPDebit";

                ChartGroupGL.Series["Series5"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series5"].YValueMembers = "CBCredit";

                ChartGroupGL.Series["Series6"].XValueMember = "Account_Group";
                ChartGroupGL.Series["Series6"].YValueMembers = "CBDebit";


                ChartGroupGL.Series["Series1"].Color = System.Drawing.Color.Pink;
                ChartGroupGL.Series["Series1"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series1"].LegendText = "Transaction Credit";

                ChartGroupGL.Series["Series2"].Color = System.Drawing.Color.Goldenrod;
                ChartGroupGL.Series["Series2"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series2"].LegendText = "Transaction Debit";

                ChartGroupGL.Series["Series3"].Color = System.Drawing.Color.ForestGreen;
                ChartGroupGL.Series["Series3"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series3"].LegendText = "Opening Balance";

                ChartGroupGL.Series["Series4"].Color = System.Drawing.Color.Green;
                ChartGroupGL.Series["Series4"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series4"].LegendText = "Closing Balance";

                ChartGroupGL.Series["Series5"].Color = System.Drawing.Color.Gray;
                ChartGroupGL.Series["Series5"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series5"].LegendText = "Closing Balance Credit";

                ChartGroupGL.Series["Series6"].Color = System.Drawing.Color.Red;
                ChartGroupGL.Series["Series6"].Legend = "DefaultGroup";
                ChartGroupGL.Series["Series6"].LegendText = "Closing Balance Debit";



                ChartGroupGL.Legends["DefaultGroup"].BackColor = Color.Transparent;
                ChartGroupGL.Legends["DefaultGroup"].LegendStyle = LegendStyle.Row;
                ChartGroupGL.Legends["DefaultGroup"].ForeColor = Color.Brown;
                ChartGroupGL.Legends["DefaultGroup"].Docking = Docking.Bottom;


                ChartGroupGL.Legends["DefaultGroup"].BorderColor = Color.Transparent;
                ChartGroupGL.Legends["DefaultGroup"].BackSecondaryColor = Color.Transparent;
                ChartGroupGL.Legends["DefaultGroup"].BackGradientStyle = GradientStyle.None;
                ChartGroupGL.Legends["DefaultGroup"].BorderColor = Color.Brown;
                ChartGroupGL.Legends["DefaultGroup"].BorderWidth = 2;
                ChartGroupGL.Legends["DefaultGroup"].BorderDashStyle = ChartDashStyle.Solid;
                ChartGroupGL.Legends["DefaultGroup"].ShadowOffset = 2;

                //ChartGroupGL.Series["Series1"]["PixelPointWidth"] = "150";
                //ChartGroupGL.Series["Series2"]["PixelPointWidth"] = "150";


                ChartGroupGL.BackColor = Color.Transparent;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.DarkViolet } };

                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.DarkViolet } };



                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Area3DStyle.Enable3D = false;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].BackColor = Color.Transparent;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.MajorGrid.Enabled = false;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.MajorGrid.Enabled = true;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.Width = 100;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.Height = 100;


                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.IsMarginVisible = true;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.X = 6;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.IsStartedFromZero = true;
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.Title = "Total Amount";
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.Title = "Total GL Credit/Debit";
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.LabelStyle.Format = "#,##,##0.##0";// "{900:C}";"#,##0;-#,##0;0"

                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.TitleFont = new Font("Verdana", 9, FontStyle.Regular);
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.TitleForeColor = Color.DarkViolet;

                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.TitleFont = new Font("Verdana", 9, FontStyle.Regular);
                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.TitleForeColor = Color.DarkViolet;

                ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.IsLabelAutoFit = true;
                //   ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.LabelStyle.Angle = -90;


            }
            catch (Exception ex)
            {

            }
            finally
            {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

       
       

    }
}