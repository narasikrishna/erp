﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="JournalEntry.aspx.cs" Inherits="FIN.Client.GL.JournalEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" id="divMainContainer" style="width: 1200px">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox OnTextChanged="txtDate_TextChanged1" AutoPostBack="true" ID="txtDate"
                    runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="1"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtenader1" TargetControlID="txtDate" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[]  ddlStype"
                    TabIndex="2" OnSelectedIndexChanged="ddlGlobalSegment_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblJournalReference">
                Journal Reference
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtJournalNumber" MaxLength="200" CssClass="txtBox" runat="server"
                    TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                Voucher Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlVouchertype" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblJournalType">
                Journal Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlJournalType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlJournalType_SelectedIndexChanged"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Source
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlSource" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="6" AutoPostBack="true" OnSelectedIndexChanged="ddlSource_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                Recurring End Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtRecurringDate" Enabled="false" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                    TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarEgxtefdnder1"
                    TargetControlID="txtRecurringDate" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                Frequency
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="validate[]  ddlStype"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblPeriods">
                Periods
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlPeriods" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="9">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCurrency">
                Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" TabIndex="10" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblExchangeRate">
                Exchange Rate
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px" id="divExRate" runat="server"
                visible="false">
                <asp:DropDownList ID="ddlExchangeRate" runat="server" CssClass=" ddlStype" TabIndex="11"
                    OnSelectedIndexChanged="ddlExchangeRate_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox LNOrient" style="">
                <asp:TextBox ID="txtExchangeRate" MaxLength="10" CssClass="txtBox_N" Text="1" runat="server"
                    TabIndex="11" Width="246px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtExchangeRate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div id="divJNo" class="LNOrient" runat="server" visible="false">
                <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                    Journal No
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox AutoPostBack="true" ID="txtJVNo" runat="server" CssClass="txtBox" TabIndex="11"></asp:TextBox>
                </div>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div style="width: 410px" class="divtxtBox LNOrient">
                <div style="width: 110px" class="divtxtBox LNOrient">
                    <asp:ImageButton ID="btnReversal" runat="server" ImageUrl="../Images/btnReversal.png"
                        OnClick="btnReversal_Click" TabIndex="11" Visible="false" Style="border: 0px" />
                    <%--<asp:Button ID="btnReversal" runat="server" Text="Reversal" TabIndex="11" CssClass="btn"
                        Width="100px" Visible="false" OnClick="btnReversal_Click" />--%>
                </div>
                <div style="width: 110px" class="divtxtBox LNOrient">
                    <asp:ImageButton ID="btnPrintReport" runat="server" ImageUrl="../Images/Print.png"
                        Visible="false" TabIndex="12" OnClick="btnPrintReport_Click" Style="border: 0px" />
                    <%--<asp:Button ID="btnPrintReport" runat="server" Text="Print" Visible="false" TabIndex="12"
                        CssClass="btn" Width="100px" OnClick="btnPrintReport_Click" />--%>
                </div>
                <div style="width: 110px;" class="divtxtBox LNOrient">
                    <asp:ImageButton ID="btnSendApprove" runat="server" ImageUrl="../Images/Post.png"
                        Visible="false" TabIndex="13" OnClick="btnSendApprove_Click" Style="border: 0px" />
                    <%--<asp:Button ID="btnSendApprove" runat="server" Text="Post" Visible="false" TabIndex="12"
                        CssClass="btn" Width="100px" OnClick="btnSendApprove_Click" />--%>
                    <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox LNOrient"
                        Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 350px;" id="Div15">
                <asp:Label ID="lblWaitApprove" runat="server" Text="WAITING FOR APPROVAL" Visible="false"
                    CssClass="lblBox LNOrient" Font-Size="18px" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style="width: 150px" id="lblOrganisation" runat="server"
            visible="false">
            Organization
        </div>
        <div class="divtxtBox LNOrient" style="width: 250px" id="divorg" runat="server" visible="false">
            <asp:DropDownList ID="ddlOrganisation" MaxLength="50" TabIndex="111" CssClass="validate[required] RequiredField ddlStype"
                runat="server" OnSelectedIndexChanged="ddlOrganisation_SelectedIndexChanged"
                AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <div class="divClear_10">
        </div>
        <div id="transBaseCur" runat="server" visible="false">
            <div style="width: 300px; float: right" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblBaseCurr" runat="server" CssClass="lblBox LNOrient" Text="Base Currency"></asp:Label>
            </div>
            <div style="width: 350px; float: right" align="center">
                <asp:Label ID="lblTransCurr" runat="server" CssClass="lblBox LNOrient" Text="Transaction Currency"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
        <div align="left" style="margin-left: 5px;">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                CellPadding="5" GridLines="None" DataKeyNames="ACCT_CODE_ID,JE_DTL_ID,JE_CREDIT_DEBIT,JE_SEGMENT_ID_1,JE_SEGMENT_ID_2,JE_SEGMENT_ID_3,JE_SEGMENT_ID_4,JE_SEGMENT_ID_5,JE_SEGMENT_ID_6"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                Width="1000px" OnSelectedIndexChanged="gvData_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-Width="150px">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" TabIndex="25" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" TabIndex="25" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" TabIndex="25" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" TabIndex="25" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ToolTip="Add" TabIndex="25" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Line No" Visible="false">
                        <EditItemTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_6") %>'></asp:Label>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("line_no") %>'
                                TabIndex="110" Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_6") %>'></asp:Label>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="110" Width="50px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSegmentValueId1" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_1") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId2" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_2") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId3" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_3") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId4" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_4") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId5" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_5") %>'></asp:Label>
                            <asp:Label ID="lblSegmentValueId6" Visible="false" runat="server" Text='<%# Eval("JE_SEGMENT_ID_6") %>'></asp:Label>
                            <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("line_no") %>'></asp:Label>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dr/Cr" Visible="false">
                        <EditItemTemplate>
                            <asp:RadioButtonList ID="chkDrCr" runat="server" rowIndex='<%# Container.DisplayIndex %>'
                                TabIndex="113" Width="100px" RepeatDirection="Horizontal" CssClass="RequiredField"
                                value='<%# Eval("JE_CREDIT_DEBIT") %>' OnSelectedIndexChanged="chkDrCr_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="2">Dr</asp:ListItem>
                                <asp:ListItem Value="1">Cr</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Label ID="lblCreditDebit" Visible="false" runat="server" Text='<%# Eval("JE_CREDIT_DEBIT") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:RadioButtonList ID="chkDrCr" runat="server" CssClass="RequiredField" RepeatDirection="Horizontal"
                                TabIndex="113" Width="100px" OnSelectedIndexChanged="chkDrCr_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="2" Selected="True">Dr</asp:ListItem>
                                <asp:ListItem Value="1">Cr</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Label ID="lblCreditDebit" Visible="false" runat="server" Text='<%# Eval("JE_CREDIT_DEBIT") %>'></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCreditDebit" runat="server" Text='<%# Eval("JE_CREDIT_DEBIT") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Codes">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountCodes" runat="server" CssClass="RequiredField ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged"
                                TabIndex="14" Width="600px">
                            </asp:DropDownList>
                            <br />
                            <asp:TextBox ID="txtNarration" runat="server" MaxLength="13" CssClass="txtBox" TabIndex="15"
                                TextMode="MultiLine" Text='<%# Eval("JE_PARTICULARS") %>' Width="600px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountCodes" runat="server" CssClass=" RequiredField ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged"
                                TabIndex="14" Width="600px">
                            </asp:DropDownList>
                            <br />
                            <asp:TextBox ID="txtNarration" runat="server" MaxLength="13" CssClass="txtBox" TabIndex="15"
                                TextMode="MultiLine" Width="600px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountCodes" Style="word-wrap: break-word" CssClass="adminFormFieldHeading"
                                runat="server" Text='<%# Eval("ACCT_CODE_DESC") %>' Width="600px">
                            </asp:Label>
                            <br />
                            <asp:Label ID="lblNarration" Style="word-wrap: break-word" runat="server" Text='<%# Eval("JE_PARTICULARS") %>'
                                Width="600px"> </asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="600px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Debit" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="150px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblTransCurDr" runat="server" Text='<%# Eval("JE_AMOUNT_DR") %>' Width="110px"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTransCurDr" runat="server" MaxLength="13" CssClass="txtBox_N"
                                TabIndex="15" Text='<%# Eval("JE_AMOUNT_DR") %>' Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderdr126" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtTransCurDr" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTransCurDr" runat="server" MaxLength="13" CssClass="txtBox_N"
                                TabIndex="15" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderdr121" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtTransCurDr" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <HeaderStyle Width="150px" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="150px"
                        Visible="true" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblTransCurCr" runat="server" Text='<%# Eval("JE_AMOUNT_cr") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTransCurCr" runat="server" MaxLength="13" Text='<%# Eval("JE_AMOUNT_cr") %>'
                                TabIndex="16" CssClass="txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtendercr122" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtTransCurCr" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTransCurCr" runat="server" MaxLength="13" CssClass="txtBox_N"
                                TabIndex="16" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtendercr142" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtTransCurCr" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <HeaderStyle Width="150px" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="KD Debit" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="150px"
                        Visible="true" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblBaseCurDr" runat="server" Text='<%# Eval("JE_ACCOUNTED_AMT_DR") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBaseCurDr" runat="server" CssClass="txtBox_N" Text='<%# Eval("JE_ACCOUNTED_AMT_DR") %>'
                                TabIndex="17" Enabled="false" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtBaseCurDr" runat="server" CssClass="txtBox_N" TabIndex="17" Width="100px"
                                Enabled="false"></asp:TextBox>
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <HeaderStyle Width="150px" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="KD Credit" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="150px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblBaseCurCr" runat="server" Text='<%# Eval("JE_ACCOUNTED_AMT_CR") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBaseCurCr" runat="server" CssClass="txtBox_N" Text='<%# Eval("JE_ACCOUNTED_AMT_CR") %>'
                                TabIndex="18" Enabled="false" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtBaseCurCr" runat="server" CssClass="txtBox_N" Width="100px" TabIndex="18"
                                Enabled="false"></asp:TextBox>
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <HeaderStyle Width="180px" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="" ItemStyle-Width="50px" ControlStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" /><br />
                        </FooterTemplate>
                         <FooterStyle  VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Segment 1">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="19" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="19">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment1" Width="180px" runat="server" Text='<%# Eval("SEGMENT_1_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 2">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="20" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="20">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment2" Width="180px" runat="server" Text='<%# Eval("SEGMENT_2_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 3">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="21" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="21" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment3" Width="180px" runat="server" Text='<%# Eval("SEGMENT_3_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 4">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="22" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="22">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment4" Width="180px" runat="server" Text='<%# Eval("SEGMENT_4_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 5">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="23" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="23">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment5" Width="180px" runat="server" Text='<%# Eval("SEGMENT_5_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 6">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                TabIndex="24" AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="24">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment6" Width="180px" runat="server" Text='<%# Eval("SEGMENT_6_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                        Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click"
                                TabIndex="117" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click"
                                TabIndex="117" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click"
                                TabIndex="117" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10" style="height: 5px;">
        </div>
        <div class="divRowContainer" align="center" style="margin-left: 540px" id="totdiv"
            runat="server">
            <div class="lblBox LNOrient" style="width: 100px" id="lblTotalDr">
                Total
            </div>
            <div class="divtxtBox LNOrient" style="width: 100px">
                <asp:TextBox ID="txtTotDrAmt" MaxLength="13" CssClass="txtBox_N" Width="100px" Enabled="false"
                    runat="server" TabIndex="15"></asp:TextBox>
            </div>
            <div class="divtxtBox LNOrient" style="width: 10px">
                &nbsp;
            </div>
            <div class="divtxtBox LNOrient" style="width: 100px;">
                <asp:TextBox ID="txtTotCrAmt" MaxLength="13" CssClass="txtBox_N" Width="100px" Enabled="false"
                    runat="server" TabIndex="16"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10" style="height: 15px;">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAnnualBudgetAmount">
                Narration
            </div>
            <div class="divtxtBox LNOrient" style="width: 370px">
                <asp:TextBox ID="txtNarration" MaxLength="240" CssClass="txtBox LongtxtBox " TextMode="MultiLine"
                    Height="50px" runat="server" TabIndex="21"></asp:TextBox>
            </div>
        </div>
        <div id="divPopUp" runat="server" visible="false">
            <asp:HiddenField ID="btnDetails" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="70%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" CssClass="lblBox LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" OnClick="btnOK_Click" CssClass="button" ID="btnOkay" Text="Ok"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px"
                                    OnClick="btnCancel1_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div2" style="display: none;">
            <asp:HiddenField ID="btnBaseDrCr" runat="server" />
            <cc2:ModalPopupExtender ID="mpeBaseCurrency" runat="server" TargetControlID="btnBaseDrCr"
                PopupControlID="pbpBase" CancelControlID="btnBaseClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pbpBase" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Dr"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBaseDr" runat="server" CssClass="txtBox_N" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="."
                                    FilterType="Numbers,Custom" TargetControlID="txtBaseDr" />
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Cr"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBaseCr" runat="server" CssClass="txtBox_N" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="."
                                    FilterType="Numbers,Custom" TargetControlID="txtBaseCr" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnBase_Click" CssClass="button" ID="btnBase"
                                    Text="Ok" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnBaseClose" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divTmpJournal" runat="server" visible="false">
            <asp:HiddenField ID="TempJournal" runat="server" />
            <cc2:ModalPopupExtender ID="mpeTempJournal" runat="server" TargetControlID="TempJournal"
                PopupControlID="pnlTempJournal" CancelControlID="btnTempClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlTempJournal" runat="server" BackColor="White" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm" style="background-color: White">
                    <div align="right" style="padding-right: 10px">
                        <asp:ImageButton ID="btnTempClose" runat="server" AlternateText="Close" ImageUrl="~/Images/close.png"
                            ToolTip="Close" Width="15px" Height="15px" />
                    </div>
                    <div class="divClear_10">
                    </div>
                    <asp:GridView ID="gvTempJournal" runat="server" DataKeyNames="JE_HDR_ID" CssClass="Grid"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="JE_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="period_name" HeaderText="Period" />
                            <asp:BoundField DataField="JE_REFERENCE" HeaderText="Reference" />
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:Button ID="btnTmpJselect" runat="server" OnClick="btnTmpJselect_Click" Text="Select" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Deleted">
                                <ItemTemplate>
                                    <asp:Button ID="btnTmpJDelete" runat="server" OnClick="btnTmpJDelete_Click" Text="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="10" />
                        <asp:HiddenField ID="hf_WFStatus" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:HiddenField ID="hf_Temp_RefNo" runat="server" Value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <%--<asp:TemplateField HeaderText="" ItemStyle-Width="50px" ControlStyle-Height="25px"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnBaseDrCr" runat="server" CssClass="btn" Text="Base Dr/Cr" OnClick="btnClick_Click" /><br />
                        </FooterTemplate>
                         <FooterStyle  VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
