﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class AccountingGroupLinksEntry : PageBase
    {
        GL_ACCT_GROUP_LINK_HDR gL_ACCT_GROUP_LINK_HDR = new GL_ACCT_GROUP_LINK_HDR();
        GL_ACCT_GROUP_LINK_DTL gL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        String ProReturn_Valdn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    ddlGroupName.Focus();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.GL.AccountingGroupLinks_BLL.getAccountingGroupLinksDetails(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    gL_ACCT_GROUP_LINK_HDR = AccountingGroupLinks_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = gL_ACCT_GROUP_LINK_HDR;

                    ddlGroupName.SelectedValue = gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_ID;
                    ddlGroupName.Enabled = false;
                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_START_DT.ToString());
                    if (gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_END_DT.ToString());
                    }
                    if (gL_ACCT_GROUP_LINK_HDR.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_ACCT_GROUP_LINK_HDR = (GL_ACCT_GROUP_LINK_HDR)EntityData;
                }



                gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_ID = ddlGroupName.SelectedValue.ToString();

                if (txtEffectiveDate.Text != string.Empty)
                {
                    gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                }

                if (txtEndDate.Text != string.Empty)
                {
                    gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                //  gL_ACCT_GROUP_LINK_HDR.WORKFLOW_COMPLETION_STATUS = "1";

                if (chkActive.Checked == true)
                {
                    gL_ACCT_GROUP_LINK_HDR.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_ACCT_GROUP_LINK_HDR.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_ACCT_GROUP_LINK_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_ACCT_GROUP_LINK_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_LNK_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_005_M.ToString(), false, true);

                    gL_ACCT_GROUP_LINK_HDR.CREATED_BY = this.LoggedUserName;
                    gL_ACCT_GROUP_LINK_HDR.CREATED_DATE = DateTime.Today;


                }
                gL_ACCT_GROUP_LINK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_LNK_ID);
                gL_ACCT_GROUP_LINK_HDR.ACCT_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Accounting_Group_Links_P");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<GL_ACCT_GROUP_LINK_DTL> userCtx = new DataRepository<GL_ACCT_GROUP_LINK_DTL>())
                        {
                            gL_ACCT_GROUP_LINK_DTL = userCtx.Find(r =>
                                (r.ACCT_GRP_LNK_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_ID].ToString() != "0")
                    //{
                    //    AccountingGroupLinks_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString());
                    //}
                    gL_ACCT_GROUP_LINK_DTL.ACCT_GRP_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_ID].ToString());

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT] != DBNull.Value)
                    {
                        gL_ACCT_GROUP_LINK_DTL.EFFECTIVE_START_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT] != DBNull.Value)
                    {
                        gL_ACCT_GROUP_LINK_DTL.EFFECTIVE_END_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_ACCT_GROUP_LINK_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_ACCT_GROUP_LINK_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    gL_ACCT_GROUP_LINK_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    gL_ACCT_GROUP_LINK_DTL.ACCT_GRP_LNK_ID = gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_LNK_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        gL_ACCT_GROUP_LINK_DTL.ACCT_GRP_LNK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GROUP_LINK_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString() != string.Empty)
                        {
                            gL_ACCT_GROUP_LINK_DTL.ACCT_GRP_LNK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.ACCT_GRP_LNK_DTL_ID].ToString();
                            gL_ACCT_GROUP_LINK_DTL.MODIFIED_DATE = DateTime.Today;
                            gL_ACCT_GROUP_LINK_DTL.MODIFIED_BY = this.LoggedUserName;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GROUP_LINK_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            gL_ACCT_GROUP_LINK_DTL.ACCT_GRP_LNK_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_005_D.ToString(), false, true);
                            gL_ACCT_GROUP_LINK_DTL.CREATED_BY = this.LoggedUserName;
                            gL_ACCT_GROUP_LINK_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GROUP_LINK_DTL, FINAppConstants.Add));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_LNK_ID, gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_ID);
                            //if (ProReturn != string.Empty)
                            //{
                            //    if (ProReturn != "0")
                            //    {
                            //        ErrorCollection.Add("GROUPNAME", ProReturn);
                            //        if (ErrorCollection.Count > 0)
                            //        {
                            //            return;
                            //        }
                            //    }
                            //}
                            //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_GROUP_LINK_HDR.ACCT_GRP_LNK_ID.ToString(), gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_GROUP_LINK_HDR.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_GROUP_LINK_HDR.ENABLED_FLAG.ToString()));

                            //if (ProReturn_Valdn != string.Empty)
                            //{
                            //    if (ProReturn_Valdn != "0")
                            //    {

                            //        ErrorCollection.Add("GRPNAME", ProReturn_Valdn);
                            //        if (ErrorCollection.Count > 0)
                            //        {
                            //            return;
                            //        }
                            //    }

                            //}
                            ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_GRP_LINKS(txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("CurrCode", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }


                            CommonUtils.SavePCEntity<GL_ACCT_GROUP_LINK_HDR, GL_ACCT_GROUP_LINK_DTL>(gL_ACCT_GROUP_LINK_HDR, tmpChildEntity, gL_ACCT_GROUP_LINK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_GRP_LINKS(txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("CurrCode", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }



                            CommonUtils.SavePCEntity<GL_ACCT_GROUP_LINK_HDR, GL_ACCT_GROUP_LINK_DTL>(gL_ACCT_GROUP_LINK_HDR, tmpChildEntity, gL_ACCT_GROUP_LINK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            //ComboFilling.fn_getGroupNameDetails(ref ddlGroupName);
            AccountingGroupLinks_BLL.GetGroupName4Header(ref ddlGroupName, Master.Mode);
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_GroupName = tmpgvr.FindControl("ddlSGroupName") as DropDownList;
                AccountingGroupLinks_BLL.fn_getAccountGroupName4SubGroup(ref ddl_GroupName, Master.StrRecordId);
                if (ddlGroupName.SelectedValue.Length > 0)
                {
                    ddl_GroupName.Items.Remove(new ListItem(ddlGroupName.SelectedItem.Text, ddlGroupName.SelectedValue));
                }

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_GroupName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ACCT_GRP_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_GroupName = gvr.FindControl("ddlSGroupName") as DropDownList;
            TextBox dtp_StartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtp_EndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.ACCT_GRP_LNK_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_GroupName;
            slControls[1] = dtp_StartDate;
            slControls[2] = dtp_StartDate;
            slControls[3] = dtp_EndDate;


            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
            string strMessage = Prop_File_Data["Group_Name_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Group Name ~ Effective Date ~ Effective Date ~ End Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            if (dtp_StartDate.Text.Length > 0 && dtp_EndDate.Text.Length > 0)
            {
                ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_StartDate.Text), DBMethod.ConvertStringToDate(dtp_EndDate.Text), Master.Mode);
                if (ErrorCollection.Count > 0)
                    return drList;
            }

            string strCondition = "ACCT_GRP_ID='" + ddl_GroupName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["ACCT_GRP_LNK_DTL_ID"].ToString(), ddl_GroupName.SelectedValue);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("GROUPNAME", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            drList[FINColumnConstants.ACCT_GRP_ID] = ddl_GroupName.SelectedValue.ToString();
            drList[FINColumnConstants.GROUP_NAME] = ddl_GroupName.SelectedItem.Text;


            if (dtp_StartDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.EFFECTIVE_START_DT] = DBMethod.ConvertStringToDate(dtp_StartDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_START_DT] = DBNull.Value;
            }



            if (dtp_EndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBMethod.ConvertStringToDate(dtp_EndDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBNull.Value;
            }

            // drList[FINColumnConstants.EFFECTIVE_END_DT] = DBMethod.ConvertStringToDate(dtp_EndDate.Text.ToString());

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                DBMethod.DeleteEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR, true);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtEffectiveDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dtGridData = FIN.BLL.GL.AccountingGroupLinks_BLL.getAccountingGroupDetails(ddlGroupName.SelectedValue.ToString());
            //BindGrid(dtGridData);
        }


    }
        #endregion
}