﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class AccountingGroupDefaultSegment : PageBase
    {
        GL_ACCT_GROUP_DEFAULT_SEGMENTS gL_ACCT_GROUP_DEFAULT_SEGMENTS = new GL_ACCT_GROUP_DEFAULT_SEGMENTS();
        GL_ACCT_GRP_DEFAULT_SGMNTS_DTL gL_ACCT_GRP_DEFAULT_SGMNTS_DTL = new GL_ACCT_GRP_DEFAULT_SGMNTS_DTL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        string ProReturn_Valdn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    ddlGroupName.Focus();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;


                dtGridData = FIN.BLL.GL.AccountingGroupDefaultSegment_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<GL_ACCT_GROUP_DEFAULT_SEGMENTS> userCtx = new DataRepository<GL_ACCT_GROUP_DEFAULT_SEGMENTS>())
                    {
                        gL_ACCT_GROUP_DEFAULT_SEGMENTS = userCtx.Find(r =>
                            (r.ACCT_DEF_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_ACCT_GROUP_DEFAULT_SEGMENTS;

                    ddlGroupName.SelectedValue = gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_GRP_ID;
                    //ddlSegmentName.Text = gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_SEGMENT_ID;
                    //if (gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_IS_MANDATORY == "1")
                    //{
                    //    chkMandatory.Checked = true;
                    //}

                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_START_DT.ToString());
                    if (gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_END_DT.ToString());
                    }
                    if (gL_ACCT_GROUP_DEFAULT_SEGMENTS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS = (GL_ACCT_GROUP_DEFAULT_SEGMENTS)EntityData;
                }



                gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_GRP_ID = ddlGroupName.SelectedValue.ToString();
                //  gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_SEGMENT_ID = ddlSegmentName.SelectedValue.ToString();

                gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text.ToString().Length > 0)
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_END_DT = null;
                }

                // gL_ACCT_GROUP_DEFAULT_SEGMENTS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                //if (chkMandatory.Checked == true)
                //{
                //    //gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_IS_MANDATORY = FINAppConstants.Y; ;
                //}
                //else
                //{
                //    //gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_IS_MANDATORY = FINAppConstants.N;
                //}


                if (chkActive.Checked == true)
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.ENABLED_FLAG = "1";
                }
                else
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.ENABLED_FLAG = "0";
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.MODIFIED_BY = this.LoggedUserName;
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_GROUP_DEFAULT_SEG_SEQ);
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_DEF_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACT_GP_DSE.ToString(), false, true);
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.CREATED_BY = this.LoggedUserName;
                    gL_ACCT_GROUP_DEFAULT_SEGMENTS.CREATED_DATE = DateTime.Today;


                }

                gL_ACCT_GROUP_DEFAULT_SEGMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_ACCT_GROUP_DEFAULT_SEGMENTS.PK_ID.ToString());
                gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Account Code ");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}


                var tmpChildEntity = new List<Tuple<object, string>>();
                GL_ACCT_GRP_DEFAULT_SGMNTS_DTL gL_ACCT_GRP_DEFAULT_SGMNTS_DTL = new GL_ACCT_GRP_DEFAULT_SGMNTS_DTL();
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_ACCT_GRP_DEFAULT_SGMNTS_DTL = new GL_ACCT_GRP_DEFAULT_SGMNTS_DTL();
                    if (dtGridData.Rows[iLoop]["ACCT_DEF_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_ACCT_GRP_DEFAULT_SGMNTS_DTL> userCtx = new DataRepository<GL_ACCT_GRP_DEFAULT_SGMNTS_DTL>())
                        {
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL = userCtx.Find(r =>
                                (r.ACCT_DEF_DTL_ID == dtGridData.Rows[iLoop]["ACCT_DEF_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_SEGMENT_ID = dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_ID].ToString();
                    gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_DEF_ID = gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_DEF_ID;
                    //gL_ACCT_CODE_SEGMENTS.ACCT_CODE_SEG_ID = "1";
                    gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                    gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    if (dtGridData.Rows[iLoop]["ACCT_IS_MANDATORY"].ToString().ToUpper() == "TRUE")
                    {
                        gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_IS_MANDATORY = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_IS_MANDATORY = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        // DBMethod.DeleteEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GRP_DEFAULT_SGMNTS_DTL, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["ACCT_DEF_DTL_ID"].ToString() != "0")
                        {
                            // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_DEF_DTL_ID = dtGridData.Rows[iLoop]["ACCT_DEF_DTL_ID"].ToString();
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.MODIFIED_BY = this.LoggedUserName;
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.MODIFIED_DATE = DateTime.Today;
                            // DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS, true);
                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GRP_DEFAULT_SGMNTS_DTL, "U"));

                        }
                        else
                        {

                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.ACCT_DEF_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCODE_D.ToString(), false, true);
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.CREATED_BY = this.LoggedUserName;
                            gL_ACCT_GRP_DEFAULT_SGMNTS_DTL.CREATED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_GRP_DEFAULT_SGMNTS_DTL, "A"));
                        }
                    }
                }
                //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_GROUP_DEFAULT_SEGMENTS.ACCT_DEF_ID.ToString(), gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_GROUP_DEFAULT_SEGMENTS.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_GROUP_DEFAULT_SEGMENTS.ENABLED_FLAG.ToString()));

                //if (ProReturn_Valdn != string.Empty)
                //{
                //    if (ProReturn_Valdn != "0")
                //    {

                //        ErrorCollection.Add("GRPNAME", ProReturn_Valdn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {

                            CommonUtils.SavePCEntity<GL_ACCT_GROUP_DEFAULT_SEGMENTS, GL_ACCT_GRP_DEFAULT_SGMNTS_DTL>(gL_ACCT_GROUP_DEFAULT_SEGMENTS, tmpChildEntity, gL_ACCT_GRP_DEFAULT_SGMNTS_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            CommonUtils.SavePCEntity<GL_ACCT_GROUP_DEFAULT_SEGMENTS, GL_ACCT_GRP_DEFAULT_SGMNTS_DTL>(gL_ACCT_GROUP_DEFAULT_SEGMENTS, tmpChildEntity, gL_ACCT_GRP_DEFAULT_SGMNTS_DTL, true);
                            savedBool = true;
                            break;
                        }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            //ComboFilling.fn_getGroupName(ref ddlGroupName,Master.Mode);
            FIN.BLL.GL.AccountingGroupDefaultSegment_BLL.fn_getGroupName(ref ddlGroupName, Master.Mode);
            // ComboFilling.fn_getSegmentName(ref ddlSegmentName);


        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlSegment = tmpgvr.FindControl("ddlSegment") as DropDownList;
                ComboFilling.getSegments(ref ddlSegment);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlSegment.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SEGMENT_ID].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<GL_ACCT_GROUP_DEFAULT_SEGMENTS>(gL_ACCT_GROUP_DEFAULT_SEGMENTS);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
                // DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                DataTable dtchildgroup = new DataTable();
                dtchildgroup = null;
                dtchildgroup = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupDefaultSegment_DAL.getchild_AcctGrp_frmAcctcode(ddlGroupName.SelectedValue)).Tables[0];
                
                ErrorCollection.Remove("Enddtnull");

                if (dtchildgroup.Rows.Count > 0)
                {
                    if (txtEndDate.Text != "")
                    {
                        ErrorCollection.Add("Enddtnull", Prop_File_Data["Selected_Account_Group_P"]);
                        return;
                    }
                }

                int count = 0;
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkact = (CheckBox)gvData.Rows[iLoop].FindControl("chkact");

                    if (chkact.Checked == true)
                    {
                        count = count + 1;
                    }
                }
                if (count > 6)
                {

                    ErrorCollection.Add("AcctGrpDefSegChk", Prop_File_Data["Maximum_only_6_segments_P"]);
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                ErrorCollection.Clear();

                AssignToBE();




                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlSegmentName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ACCT_IS_MANDATORY"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AcctGrpDefSegEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AcctGrpDefSegEntryRC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlSegment = gvr.FindControl("ddlSegment") as DropDownList;

            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();


            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["ACCT_DEF_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            //if (dt_tmp.Select("DELETED='" + FINAppConstants.N + "'").Length >= 6)
            //{
            //    ErrorCollection.Clear();
            //    ErrorCollection.Add("LengthExced", "Only 6 Segment able to add");
            //    return drList;

            //}

            slControls[0] = ddlSegment;





            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "DropDownList";
            string strMessage = Prop_File_Data["Segment_P"] + "";
            //string strMessage = "Segment_P";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }




            //ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_FromDate.Text), DBMethod.ConvertStringToDate(dtp_ToDate.Text), "chk");
            //if (ErrorCollection.Count > 0)
            //    return drList;

            string strCondition = "SEGMENT_ID='" + ddlSegment.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.SegmentNameAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            int int_segRow = 0;
            if (GMode == "A")
            {
                int_segRow = dt_tmp.Rows.Count;
            }
            else
            {
                int_segRow = rowindex;
            }
            DataTable dt_segData;
            if (int_segRow > 0)
            {
                dt_segData = FIN.BLL.GL.Segments_BLL.getSegmentData(ddlSegment.SelectedValue.ToString());
                if (dt_segData.Rows.Count > 0)
                {
                    if (dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString().Length > 0)
                    {
                        //strCondition = "SEGMENT_ID='" + dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString() + "'";
                        //strMessage = "Selected Segment is Dependent Segment,First Select Dependent Segment";
                        //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
                        //if (ErrorCollection.Count == 0)
                        //{
                        //    ErrorCollection.Add("segmenterror", "Selected Segment is Dependent Segment,First Select independent Segment");
                        //    return drList;
                        //}
                        //else
                        //{
                        //    ErrorCollection.Clear();
                        //}
                        ErrorCollection.Clear();

                        if (dt_tmp.Rows[int_segRow - 1]["SEGMENT_ID"].ToString() != dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString())
                        {
                            ErrorCollection.Add("segmenterror", Prop_File_Data["Segment_Dependent_P"] + Prop_File_Data["Don'First_Select_independent_Segment_P"]);

                            return drList;
                        }

                    }
                }
            }
            ErrorCollection.Clear();
            if (GMode != "A")
            {
                if (int_segRow < dt_tmp.Rows.Count)
                {
                    dt_segData = FIN.BLL.GL.Segments_BLL.getSegmentData(dt_tmp.Rows[int_segRow]["SEGMENT_ID"].ToString());
                    if (dt_segData.Rows.Count > 0)
                    {
                        if (dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString().Length > 0)
                        {
                            if (ddlSegment.SelectedValue != dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString())
                            {
                                ErrorCollection.Add("segmenterror", Prop_File_Data["Next_Segment_Dependent_P"]);
                                return drList;
                            }
                        }
                    }
                }
            }


            drList[FINColumnConstants.SEGMENT_ID] = ddlSegment.SelectedValue.ToString();
            drList[FINColumnConstants.SEGMENT_NAME] = ddlSegment.SelectedItem.Text;

            if (chkact.Checked)
            {
                drList["ACCT_IS_MANDATORY"] = "TRUE";
            }
            else
            {
                drList["ACCT_IS_MANDATORY"] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AcctGrpDefSegEntryRC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AcctGrpDefSegEntryRDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



    }
}