﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BudgetDetailsEntry.aspx.cs" Inherits="FIN.Client.GL.BudgetDetails" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        //  $("#tvModules").on('click', this, OnNodeClicked);
    </script>
    <script type="text/javascript">
        //        function ScrollToSelectedNode() {
        //            var selectedNodeID = $('#<%=tvModules.ClientID %>_SelectedNode').val();
        //            alert('hi');
        //            if (selectedNodeID != '') {
        //                //* calculate selected top an left position (http://docs.jquery.com/CSS/position)
        //                //* in order to get correct relative position remember to set div position to absolute
        //                var scrollTop = $('#' + selectedNodeID).position().top;
        //                var scrollLeft = $('#' + selectedNodeID).position().left;

        //                //* here 'divTreeViewScrollTo' is the id of the div where we put our tree view
        //                $('#Div16').scrollTop(scrollTop);
        //                $('#Div16').scrollLeft(scrollLeft);
        //            }
        //        }

        //        function OnNodeClicked(event) {
        //            if (event.target.id == "" && event.target.tagName == "IMG") {
        //                var parentId = event.target.parentElement.id;

        //                if (parentId.charAt(parentId.length - 1) == 'i')
        //                    alert("node image is clicked");
        //                else {
        //                    var nodeIndex = parentId.charAt(parentId.length - 1);
        //                    var childNodesArg = parentId + "Nodes";

        //                    //var func = window["TreeView_ToggleNode"];
        //                    TreeView_ToggleNode(tvModules, nodeIndex, document.getElementById(parentId), ' ', document.getElementById(childNodesArg));
        //                }
        //            } else alert(event.target.id + " is clicked with name = " + $(event.target.id).text());


        //            return false;
        //        }
        function fn_ShowAccSearchDiv() {
            $('#div_Search').toggle();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCompanyName">
                Organization
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCompanyName" CssClass="validate[required] RequiredField ddlStype"
                    Enabled="false" runat="server" TabIndex="1" OnSelectedIndexChanged="ddlOrganisation_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblBudgetName">
                Budget Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtBudgetName" MaxLength="500" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblBudgetType">
                Budget Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlBudgetType" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlBudgetType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlGlobalSegment" CssClass="validate[]  ddlStype" runat="server"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calEx34tender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="Calend23arExtender1"
                    TargetControlID="txtToDate" OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divFilter">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Accounting Year
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCalyr" CssClass="ddlStype" runat="server" TabIndex="8">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px; display: none" id="Div14" runat="server"
                visible="true">
                Search
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px" id="D1iv4" runat="server" visible="false">
                <asp:TextBox ID="txtSearch" CssClass="txtBox" runat="server" TabIndex="9"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Di1v5" runat="server" visible="false">
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/iconSearchAccCodeGrp.png"
                    TabIndex="10" AlternateText="Search Acc Code or Acc Group" ToolTip="Search Acc Code or Acc Group"
                    OnClick="btnSearch_Click" Width="40px" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="position: fixed; top: 130px; left: 25px; background-color: ThreeDFace;
            width: 650px; display: none" id="div_Search">
            <div class="lblBox LNOrient" style="width: 150px" id="Div17">
                Account Code
            </div>
            <div class="divtxtBox LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlAccountCode" CssClass="ddlStype" runat="server" TabIndex="8"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCode_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div align="left">
            <div class="divFormcontainer" style="width: 1200px;" id="div2">
                <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 32px;
                    background-color: rgb(216,216,216);">
                    <table width="100%">
                        <tr>
                            <td>
                                <div align="center" class="lblBox LNOrient" id="Div3">
                                    <img src="../Images/Query_icon.png" id="img_Search" width="20px" height="20px" alt="Search"
                                        onclick="fn_ShowAccSearchDiv()" />
                                    &nbsp; &nbsp; &nbsp;<span class="lblBox">Budget Details</span>
                                </div>
                            </td>
                            <td>
                                <div class="lblBox OppLNOrient" style="width: 170px" id="Div16">
                                    <asp:ImageButton ID="btnBudgetSegment" runat="server" ImageUrl="~/Images/btnSegment.png"
                                        TabIndex="10" AlternateText="Segment" ToolTip="Segment" OnClick="btnBudgetSegment_Click"
                                        Width="30px" />&nbsp;
                                    <asp:ImageButton ID="btnBudgetPeriods" runat="server" ImageUrl="~/Images/btnPeriod.png"
                                        TabIndex="11" AlternateText="Periods" ToolTip="Periods" Width="30px" OnClick="btnBudgetPeriods_Click" />&nbsp;
                                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Images/btnAddBudget.png"
                                        TabIndex="12" AlternateText="Add Budget Amount" ToolTip="Add Budget Amount" OnClick="btnAdd_Click"
                                        Width="30px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="divClear_10">
                </div>
                <div style="width: 100%">
                    <table width="100%">
                        <tr valign="top">
                            <td valign="top">
                                <div class="divRowContainer " style="border: 6; border-color: rgb(46,116,96);">
                                    <div class="divtxtBox " style="width: 600px">
                                        <asp:Panel ID="pnlTreeview" runat="server" Width="100%" Height="300px" ScrollBars="Auto"
                                            Style="border: 5; border-color: Red;">
                                            <asp:TreeView ID="tvModules" Width="100%" CssClass="MenuView LNOrient" BackColor="Transparent"
                                                BorderColor="Transparent" ExpandDepth="0" ShowExpandCollapse="true" EnableClientScript="false"
                                                PopulateNodesFromClient="true" runat="server" OnSelectedNodeChanged="tvModules_SelectedNodeChanged">
                                                <ParentNodeStyle Font-Bold="false" BackColor="transparent" Height="20px" ForeColor="#333333" />
                                                <SelectedNodeStyle Font-Underline="false" ForeColor="#993300" Font-Bold="True" HorizontalPadding="0px"
                                                    VerticalPadding="0px" Width="212px" />
                                                <NodeStyle ForeColor="#333333" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px"
                                                    Width="212px" />
                                                <RootNodeStyle CssClass="ModuleBackground" BorderWidth="0px" ChildNodesPadding="10px" />
                                            </asp:TreeView>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                            <td valign="top">
                                <div class="divtxtBox LNOrient" style="">
                                    <div class="lblBox LNOrient" style="width: 100px" id="divAccCode" runat="server">
                                        Account Code
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 300px">
                                        <asp:TextBox ID="txtAccountCode" MaxLength="500" CssClass="validate[]  txtBox" Enabled="false"
                                            runat="server" TabIndex="13"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="divClear_10">
                                </div>
                                <div class="divtxtBox LNOrient" style="">
                                    <div class="lblBox LNOrient" style="width: 100px" id="divAccName" runat="server">
                                        Account Name
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 300px">
                                        <asp:TextBox ID="txtAccountName" CssClass="validate[]  txtBox" Enabled="false" runat="server"
                                            TabIndex="14"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="divClear_10">
                                </div>
                                <div class="divtxtBox LNOrient" style="" align="center">
                                    <div class="lblBox LNOrient" style="width: 500px" id="Div6">
                                        <asp:RadioButtonList ID="rblBudget" runat="server" TabIndex="15" RepeatDirection="Horizontal"
                                            AutoPostBack="True" OnSelectedIndexChanged="rblBudget_SelectedIndexChanged">
                                            <asp:ListItem Value="1" Selected="True">Amount</asp:ListItem>
                                            <asp:ListItem Value="2"> Based on Previous Year</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="divClear_10">
                                </div>
                                <div class="divtxtBox LNOrient" style="" id="divPers" runat="server">
                                    <div class="lblBox LNOrient" style="width: 100px" id="Div15">
                                        Percentage
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 300px" align="left">
                                        <asp:TextBox ID="txtPer" CssClass="validate[]  txtBox" runat="server" AutoPostBack="true"
                                            MaxLength="3" Width="150px" TabIndex="16" OnTextChanged="txtPer_TextChanged"></asp:TextBox>
                                        <cc2:FilteredTextBoxExtender ID="FilteredTextBosxExte23nder1" runat="server" FilterType="Numbers,Custom"
                                            ValidChars="." TargetControlID="txtPer" />
                                    </div>
                                </div>
                                <div class="divClear_10">
                                </div>
                                <div class="divtxtBox LNOrient" style="">
                                    <div class="lblBox LNOrient" style="width: 100px" id="Div7">
                                        Budget Amount
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 350px">
                                        <asp:TextBox ID="txtBudgetAmt" MaxLength="13" Width="150px" CssClass="validate[] EntryFont RequiredField txtBox_N"
                                            AutoPostBack="true" OnTextChanged="txtAnnualBudgetAmount_TextChanged" runat="server"
                                            TabIndex="17"></asp:TextBox>
                                        <cc2:FilteredTextBoxExtender ID="FilteredTexstBoxExtend23er1" runat="server" FilterType="Numbers,Custom"
                                            ValidChars=".," TargetControlID="txtBudgetAmt">
                                        </cc2:FilteredTextBoxExtender>
                                        <%-- <cc2:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtBudgetAmt"
                            Mask="9{9}.99" MaskType="Number" InputDirection="RightToLeft" />--%>
                                    </div>
                                </div>
                                <div class="divClear_10">
                                </div>
                                <div class="divtxtBox LNOrient" style="" align="center">
                                    <div class="lblBox LNOrient" style="width: 500px" id="Div8">
                                        <%-- <asp:Button ID="btnBudgetSegment" runat="server" Text="Segment" CssClass="btn" TabIndex="6"
                            Enabled="false" Width="120px" OnClick="btnBudgetSegment_Click" />&nbsp;
                        <asp:Button ID="btnBudgetPeriods" runat="server" Text="Periods" CssClass="btn" TabIndex="6"
                            Enabled="false" OnClick="btnPeriods_Click" Width="120px" />&nbsp;
                        <asp:Button ID="btnAdd" Enabled="false" runat="server" Text="Add Budget Amount" CssClass="btn"
                            TabIndex="6" Width="200px" OnClick="btnAdd_Click" />--%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <div class="divFormcontainer" style="width: 1200px;" id="div9">
                <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 28px;
                    background-color: rgb(216,216,216);">
                    <div align="center" class="lblBox LNOrient" style="width: 670px; text-align: left;"
                        id="Div10">
                        Previous Year Budget Details
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" align="left" style="">
                    <div class="lblBox LNOrient" style="width: 180px" id="Div11">
                        Previous Budget Amount
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 150px">
                        <asp:TextBox ID="txtPreBudAmt" MaxLength="13" CssClass="validate[]  txtBox" Enabled="false"
                            runat="server" TabIndex="2"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextB34oxExtendedr1" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtPreBudAmt" />
                    </div>
                    <div class="lblBox LNOrient" style="width: 220px" id="Div12">
                        Previous Revised Budget Amount
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 150px">
                        <asp:TextBox ID="txtPreRevBudAmt" MaxLength="13" CssClass="validate[]   txtBox" Enabled="false"
                            runat="server" TabIndex="2"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="Filter2edTextBoxExtender2d" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtPreRevBudAmt" />
                    </div>
                </div>
                <div class="divRowContainer" align="left" style="">
                    <div class="lblBox LNOrient" style="width: 150px" id="Div13">
                        Previous Year Actual
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 150px">
                        <asp:TextBox ID="txtPreYrActual" MaxLength="13" CssClass="validate[]  txtBox" Enabled="false"
                            runat="server" TabIndex="2"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="Filte3redTextBoxExtender3d" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtPreYrActual" />
                    </div>
                </div>
            </div>
        </div>
        <div id="divPopUssp">
            <asp:HiddenField runat="server" ID="hfgs" />
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" Width="70px"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" Width="250px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="dvPeriods">
            <asp:HiddenField ID="btnPeriods1" runat="server" />
            <cc2:ModalPopupExtender ID="mpePeriods" runat="server" TargetControlID="btnPeriods1"
                PopupControlID="pnlPeriods" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlPeriods" runat="server" BackColor="Violet" Width="45%" Height="85%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <div class="divFormcontainer" style="width: 100%;" id="div4">
                        <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 28px;
                            background-color: rgb(216,216,216);">
                            <div align="center" class="lblBox LNOrient" style="width: 100%; text-align: left;
                                font-weight: bold;" id="Div5">
                                Monthly Budget Amount
                            </div>
                        </div>
                        <table width="100%">
                            <tr class="ConfirmHeading">
                                <td colspan="4">
                                    <asp:Label ID="lblSumError" runat="server" Visible="false" Style="text-align: left;
                                        color: Red;" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td colspan="2">
                                    <asp:Label ID="Label15" runat="server" Text="Current Year" Style="text-align: center;"
                                        Width="175px"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:Label ID="Label16" runat="server" Text="Previous Year" Width="175px" Style="text-align: center;"></asp:Label>
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod1" runat="server" Text="Period 1" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop1" runat="server" TabIndex="1" Text='<%# Eval("BUDGET_MONTH_AMT_01") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a1FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop1" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod1" runat="server" Text="Period 1" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop1" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a2FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop1" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod2" runat="server" Text="Period 2" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop2" runat="server" TabIndex="2" Text='<%# Eval("BUDGET_MONTH_AMT_02") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a3FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop2" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod2" runat="server" Text="Period 2" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop2" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a4FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop2" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod3" runat="server" Text="Period 3" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop3" runat="server" TabIndex="3" Text='<%# Eval("BUDGET_MONTH_AMT_03") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a5FilteredTextBoxExtender27" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop3" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod3" runat="server" Text="Period 3" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop3" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a6FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop3" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod4" runat="server" Text="Period 4" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop4" runat="server" TabIndex="4" Text='<%# Eval("BUDGET_MONTH_AMT_04") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a7FilteredTextBoxExtender37" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop4" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod4" runat="server" Text="Period 4" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop4" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a8FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop4" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod5" runat="server" Text="Period 5" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop5" runat="server" TabIndex="5" Text='<%# Eval("BUDGET_MONTH_AMT_05") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a9FilteredTextBoxExtender47" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop5" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod5" runat="server" Text="Period 5" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop5" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="aFilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop5" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod6" runat="server" Text="Period 6" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop6" runat="server" TabIndex="6" Text='<%# Eval("BUDGET_MONTH_AMT_06") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="asFilteredTextBoxExtender57" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop6" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod6" runat="server" Text="Period 6" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop6" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="dFilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop6" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod7" runat="server" Text="Period 7" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop7" runat="server" TabIndex="7" Text='<%# Eval("BUDGET_MONTH_AMT_07") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="fFilteredTextBoxExtender67" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop7" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod7" runat="server" Text="Period 7" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop7" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="gFilteredTextBoxExtender7" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop7" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod8" runat="server" Text="Period 8" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop8" runat="server" TabIndex="8" Text='<%# Eval("BUDGET_MONTH_AMT_08") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="hFilteredTextBoxExtender77" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop8" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod8" runat="server" Text="Period 8" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop8" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="jFilteredTextBoxExtender8" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop8" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod9" runat="server" Text="Period 9" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop9" runat="server" TabIndex="9" Text='<%# Eval("BUDGET_MONTH_AMT_09") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="kFilteredTextBoxExtender87" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop9" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod9" runat="server" Text="Period 9" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop9" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="lFilteredTextBoxExtender9" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop9" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod10" runat="server" Text="Period 10" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop10" runat="server" TabIndex="10" Text='<%# Eval("BUDGET_MONTH_AMT_10") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="zFilteredTextBoxExtender97" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop10" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod10" runat="server" Text="Period 10" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop10" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="xFilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop10" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod11" runat="server" Text="Period 11" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop11" runat="server" TabIndex="11" Text='<%# Eval("BUDGET_MONTH_AMT_11") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="cFilteredTextBoxExtender31" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop11" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod11" runat="server" Text="Period 11" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop11" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="vFilteredTextBoxExtender11" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop11" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod12" runat="server" Text="Period 12" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop12" runat="server" TabIndex="12" Text='<%# Eval("BUDGET_MONTH_AMT_12") %>'
                                        CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="bFilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop12" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPrevPeriod12" runat="server" Text="Period 12" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrevPeriodPop12" runat="server" TabIndex="25" CssClass="EntryFont RequiredField txtBox_N"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="nFilteredTextBoxExtender12" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPrevPeriodPop12" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button runat="server" CssClass="button" ID="btnPeriodOk" Text="Update" Width="160px"
                                        TabIndex="13" OnClick="btnPeriodOk_Click" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btnPeriodCancel" Text="Cancel" Width="60px"
                                        TabIndex="14" OnClick="btnPeriodCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="9" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
