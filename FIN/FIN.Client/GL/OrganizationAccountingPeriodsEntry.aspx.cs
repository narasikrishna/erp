﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class OrganizationAccountingPeriodsEntry : PageBase
    {


        GL_COMP_ACCT_PERIOD_DTL gL_COMP_ACCT_PERIOD_DTL = new GL_COMP_ACCT_PERIOD_DTL();
        DataTable dtGridData = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool=false;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Pg_Load", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);

        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        private void BindGrid()
        {
            if (ddlCalendarYear.SelectedValue.ToString().Length > 0)
            {

                DataTable dtAccYr = new DataTable();
                DataTable dt_CompCal = new DataTable();


                DataRow[] dr = null;
                if (Session["CompCalPeriodDet"] != null)
                {
                    dt_CompCal = (DataTable)Session["CompCalPeriodDet"];
                    if (dt_CompCal != null)
                    {
                        if (dt_CompCal.Rows.Count > 0)
                            dr = dt_CompCal.Select("CAL_DTL_ID='" + ddlCalendarYear.SelectedValue.ToString() + "'");
                    }
                }
                if (dr == null)
                {
                    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(VMVServices.Web.Utils.OrganizationID, ddlCalendarYear.SelectedValue.ToString())).Tables[0];
                }
                else if (dr.Length == 0)
                {
                    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(VMVServices.Web.Utils.OrganizationID, ddlCalendarYear.SelectedValue.ToString())).Tables[0];
                }
                //}
                if (Session["CompCalPeriodDet"] == null)
                {
                    Session["CompCalPeriodDet"] = dtAccYr;
                    dt_CompCal = dtAccYr;
                }
                else
                {
                    dt_CompCal = (DataTable)Session["CompCalPeriodDet"];
                    if (dtAccYr.Rows.Count > 0)
                    {
                        dt_CompCal.Merge(dtAccYr);
                    }
                }

                gvperiods.DataSource = dt_CompCal;
                gvperiods.DataBind();
            }
        }
        protected void ddlCalendarYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FacilityMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                AccountingCalendar_BLL.GetCalAcctYear(ref ddlCalendarYear);
                EntityData = null;
                Session["CompCalPeriodDet"] = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<GL_COMP_ACCT_PERIOD_DTL> userCtx = new DataRepository<GL_COMP_ACCT_PERIOD_DTL>())
                    {
                        gL_COMP_ACCT_PERIOD_DTL = userCtx.Find(r =>
                            (r.PERIOD_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    EntityData = gL_COMP_ACCT_PERIOD_DTL;
                    ddlCalendarYear.SelectedValue = gL_COMP_ACCT_PERIOD_DTL.CAL_DTL_ID;

                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FacilityMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void btnPeriod_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                PeriodClick(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void PeriodClick(GridViewRow gvr)
        {
            try
            {

                DropDownList ddl_Acctcal = (DropDownList)gvr.FindControl("ddlAcctcal");


                if (ddl_Acctcal != null)
                {
                    if (ddl_Acctcal.SelectedValue.ToString().Length > 0)
                    {
                        if (Session["CompCalDet"] == null)
                        {
                        }
                        DataTable dt_CompCal = (DataTable)Session["CompCalDet"];
                        DataRow[] dr = null;
                        if (dt_CompCal != null)
                        {
                            if (dt_CompCal.Rows.Count > 0)
                                dr = dt_CompCal.Select("CAL_ID='" + ddl_Acctcal.SelectedValue.ToString() + "' AND SELECTED='TRUE'");
                        }
                        if (dr != null)
                        {
                            if (dr.Length > 0)
                            {
                                string str_CalDetId = "";
                                ddlCalendarYear.Items.Clear();
                                for (int iLoop = 0; iLoop < dr.Length; iLoop++)
                                {
                                    str_CalDetId += "'" + dr[iLoop]["CAL_DTL_ID"].ToString() + "',";
                                    ddlCalendarYear.Items.Add(new ListItem(dr[iLoop]["CAL_ACCT_YEAR"].ToString(), dr[iLoop]["CAL_DTL_ID"].ToString()));

                                }
                                ddlCalendarYear_SelectedIndexChanged(ddlCalendarYear, new EventArgs());
                                str_CalDetId = str_CalDetId + '0';
                            }
                        }
                        else
                        {

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Status = (DropDownList)gvr.FindControl("ddlStatus");
            string str_period_id = gvperiods.DataKeys[gvr.RowIndex].Values["PERIOD_ID"].ToString();
            DataTable dt_PeriodDet = (DataTable)Session["CompCalPeriodDet"];
            DataRow[] dr = dt_PeriodDet.Select("PERIOD_ID='" + str_period_id + "'  and CAL_DTL_ID='" + ddlCalendarYear.SelectedValue.ToString() + "'");
            if (dr != null)
                if (dr.Length > 0)
                {
                    dr[0]["LOOKUP_ID"] = ddl_Status.SelectedValue.ToString();
                    dr[0]["LOOKUP_NAME"] = ddl_Status.SelectedItem.Text.ToString();
                }
        }
        protected void gvperiod_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }
                if (((DataRowView)e.Row.DataItem).Row["CAL_DTL_ID"].ToString() != ddlCalendarYear.SelectedValue.ToString())
                {
                    e.Row.Visible = false;
                }
                DropDownList ddlStatus = e.Row.FindControl("ddlStatus") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlStatus, "CPS");
                ddlStatus.SelectedValue = gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString();
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "AUDITED")
                {
                    ddlStatus.Enabled = false;
                }
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "OPEN")
                {
                    ddlStatus.Items.Remove(new ListItem("NEVEROPEN", "NOP"));
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dt_comp_CalDet = (DataTable)Session["CompCalPeriodDet"];
                DataRow[] dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + ddlCalendarYear.SelectedValue + "'");
                if (dr == null)
                {
                    if (dr.Length == 0)
                    {
                        DataTable dt_tmp_calperdet = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(Master.StrRecordId, ddlCalendarYear.SelectedValue)).Tables[0];
                        dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + ddlCalendarYear.SelectedValue + "'");
                    }
                }
                for (int kLoop = 0; kLoop < dr.Length; kLoop++)
                {
                    gL_COMP_ACCT_PERIOD_DTL.COMP_ID = VMVServices.Web.Utils.OrganizationID;
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_ID = dr[kLoop]["PERIOD_ID"].ToString();
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NAME = dr[kLoop]["PERIOD_NAME"].ToString();
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TYPE = dr[kLoop]["PERIOD_TYPE"].ToString();
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_FROM_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_FROM_DT"].ToString());
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TO_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_TO_DT"].ToString());
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_STATUS = dr[kLoop]["LOOKUP_ID"].ToString();
                    gL_COMP_ACCT_PERIOD_DTL.CAL_DTL_ID = dr[kLoop]["CAL_DTL_ID"].ToString();
                    gL_COMP_ACCT_PERIOD_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    gL_COMP_ACCT_PERIOD_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    gL_COMP_ACCT_PERIOD_DTL.CREATED_BY = this.LoggedUserName;
                    gL_COMP_ACCT_PERIOD_DTL.CREATED_DATE = DateTime.Now;
                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NUMBER = decimal.Parse(dr[kLoop]["PERIOD_NUMBER"].ToString());
                    if (dr[kLoop]["PK_ID"].ToString() == "0")
                    {
                        DBMethod.SaveEntity<GL_COMP_ACCT_PERIOD_DTL>(gL_COMP_ACCT_PERIOD_DTL);
                        savedBool = true;
                    }
                    else
                    {
                        DBMethod.SaveEntity<GL_COMP_ACCT_PERIOD_DTL>(gL_COMP_ACCT_PERIOD_DTL, true);
                        savedBool = true;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.SAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}