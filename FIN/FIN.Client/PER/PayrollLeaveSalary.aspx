﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollLeaveSalary.aspx.cs" Inherits="FIN.Client.PER.PayrollLeaveSalary" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDeptName">
                Department Name</div>
            <div class="divtxtBox" style="float: left; width: 550px">
                <asp:DropDownList ID="ddlDeptName" AutoPostBack="true" OnSelectedIndexChanged="ddlDeptName_SelectedIndexChanged" 
                 runat="server" TabIndex="1" CssClass="EntryFont RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Employee Name</div>
            <div class="divtxtBox" style="float: left; width: 550px">
                <asp:DropDownList ID="ddlEmpName" runat="server" TabIndex="2" CssClass="EntryFont RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblStartDate">
                Start Date
            </div>
            <div class="divtxtBox" style="float: left; width: 100px">
                <asp:TextBox ID="txtStartDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtStartDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtStartDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox" style="float: left; width: 100px">
                <asp:TextBox runat="server" ID="txtEndDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEndDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divtxtBox" style="float: left; width: 750px" align="right">
                <asp:Button ID="btnCalculate" TabIndex="5" runat="server" Text="Calculate Leave Salary Amount" OnClick="btnCalculate_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
                Payroll Amount Paid to be Paid
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:TextBox runat="server" ID="txtPayrollAmtToBePaid" CssClass="txtBox" Enabled="false"
                    TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divtxtBox" style="float: right; width: 750px;">
                <asp:RadioButtonList ID="rbPayType" AutoPostBack="true" RepeatDirection="Horizontal" runat="server" TabIndex="7"
                OnSelectedIndexChanged="rbPayType_SelectedIndexChanged">
                    <asp:ListItem Value="0" Selected="True">Payroll</asp:ListItem>
                    <asp:ListItem Value="1">In-Person</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divPayPeriod" runat="server">
            <div class="lblBox" style="float: left; width: 200px" id="divPayPeriodID">
                Payroll Period</div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:DropDownList ID="ddlPayPeriod" runat="server" TabIndex="8" CssClass="EntryFont RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divPayPaid" runat="server">
            <div class="lblBox" style="float: left; width: 200px" id="Div6">
                Payroll Amount Paid
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:TextBox runat="server" ID="txtPayAmtPaid" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="9"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblPayDate">
                Payroll Date
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox runat="server" ID="txtPayrollDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    TabIndex="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEndDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPayrollDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divPayRemarks" runat="server">
            <div class="lblBox" style="float: left; width: 200px" id="Div5">
                Remarks
            </div>
            <div class="divtxtBox" style="float: left; width: 550px">
                <asp:TextBox runat="server" ID="txtRemarks" CssClass="txtBox" TextMode="MultiLine"
                    Height="50px" TabIndex="11"></asp:TextBox>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
