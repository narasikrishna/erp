﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollDeductionDetailsEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollDeductionDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPayrollNumber">
                Payroll Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlPayrollNumber" runat="server" AutoPostBack="True" CssClass=" validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlPayrollNumber_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDescription" CssClass="RequiredField txtBox" Enabled="true" MaxLength="3"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDeductionPartyNumber">
                Deduction Party Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlDeductionPartyNumber" runat="server" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlDeductionPartyNumber_SelectedIndexChanged"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDeductionPartyName">
                Deduction Party Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDeductionPartyName" CssClass="RequiredField txtBox" Enabled="true"
                    MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="  width: 300px" id="lblGetDetails">
                <asp:ImageButton ID="btnGetDetails" runat="server" ImageUrl="~/Images/btnGetDetails.png" OnClick="btnGetDetails_Click" CssClass="btnProcess"
                    Style="border: 0px" />
                <%--<asp:Button ID="btnGetDetails" runat="server" Text="Get Details" CssClass="btnProcess"
                    OnClick="btnGetDetails_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DELETED">
                <Columns>
                    <asp:TemplateField HeaderText="Employee No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEmployeeNo" TabIndex="5" Enabled="false" Width="200px" MaxLength="100"
                                runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("EMP_ID") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtEmployeeNo" TabIndex="5" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmployeeNo" Width="130px" Enabled="false" runat="server" Text='<%# Eval("EMP_ID") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEmployeeName" TabIndex="6" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("EMP_FIRST_NAME") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtEmployeeName" TabIndex="6" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmployeeName" Width="130px" Enabled="false" runat="server" Text='<%# Eval("EMP_FIRST_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtElement" TabIndex="7" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("PAY_ELEMENT_CODE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtElement" TabIndex="7" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElement" Width="130px" Enabled="false" runat="server" Text='<%# Eval("PAY_ELEMENT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="8" Enabled="false" Width="130px"
                                MaxLength="100" runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="8" Enabled="false" Width="130px"
                                MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElementDescription" Width="130px" Enabled="false" runat="server"
                                Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="9" Enabled="false" Width="130px" MaxLength="13"
                                runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="9" Enabled="false" Width="130px" MaxLength="13"
                                runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" Enabled="false" runat="server" Text='<%# Eval("PAY_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="10" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("PAY_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="10" runat="server" CssClass="EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblfromDate" runat="server" Text='<%# Eval("PAY_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="11" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("PAY_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="12" runat="server" CssClass="EntryFont  txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender11" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("PAY_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
