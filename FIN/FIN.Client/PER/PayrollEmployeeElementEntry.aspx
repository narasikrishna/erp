﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollEmployeeElementEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollEmployeeElementEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGroupCode">
                Group Code
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlGroupCode" runat="server" AutoPostBack="True" CssClass="RequiredField EntryFont ddlStype"
                    OnSelectedIndexChanged="ddlGroupCode_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 145px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription" MaxLength = "200" Enabled="true" TabIndex="2" CssClass="validate[]  txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="  width: 473px">
                <asp:DropDownList ID="ddldept" runat="server" AutoPostBack="True" CssClass="EntryFont RequiredField ddlStype"
                    TabIndex="3" OnSelectedIndexChanged="ddldept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEmployeeNo">
                Employee No
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlEmployeeNo" runat="server" AutoPostBack="True" CssClass="EntryFont RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlEmployeeNo_SelectedIndexChanged" TabIndex="4" Width="100%">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 145px" id="lblgrade">
                Grade
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtGrade" Enabled="true" TabIndex="5" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="200"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 475px; height: 18px;">
                <asp:TextBox ID="txtEmployeeName" Enabled="true" TabIndex="6" CssClass="validate[]  txtBox"
                    runat="server" Width = "470px" MaxLength="302"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblElement">
                Element
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtElement" Enabled="false" TabIndex="7" CssClass="validate[]  txtBox"
                    runat="server" Width= "150px" MaxLength="50"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 80px" id="lblMinLimit">
                Min. Limit
            </div>
            <div class="divtxtBox LNOrient" style="  width: 60px; height: 18px;">
                <asp:TextBox ID="txtMinLimit" Enabled="false" TabIndex="8" CssClass="validate[]  txtBox_N"
                    runat="server" Width = "60px" MaxLength="50"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 80px" id="lblMaxLimit">
                Max. Limit
            </div>
            <div class="divtxtBox LNOrient" style="  width: 60px; height: 18px;">
                <asp:TextBox ID="txtMaxLimit" Enabled="false" TabIndex="9" CssClass="validate[]  txtBox_N"
                    runat="server" Width = "60px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient" align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PAY_ELEMENT_ID,PAY_EMP_ELEMENT_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Element Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlElementCode" TabIndex="10" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlElementCode_SelectedIndexChanged"
                                Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlElementCode" TabIndex="10" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlElementCode_SelectedIndexChanged"
                                Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElementCode" CssClass="adminFormFieldHeading" TabIndex="10" runat="server"
                                Text='<%# Eval("PAY_ELEMENT_CODE") %>' Width="100%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="11" Enabled="true" Width="160px"
                                MaxLength="500" runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="11" Enabled="true" Width="160px"
                                MaxLength="500" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElementDescription" Width="160px" TabIndex="11" Enabled="true"
                                runat="server" Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pro Rate">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkprorate" TabIndex="12" Enabled = "true" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAY_ELEMENT_PRORATE")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkprorate" TabIndex="12" Enabled = "true" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkprorate" TabIndex="12" Enabled = "false" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAY_ELEMENT_PRORATE")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Annual Leave">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkannualleave" TabIndex="13" Enabled = "true" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAID_FOR_ANNUAL_LEAVE")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkannualleave" TabIndex="13" Enabled = "true" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkannualleave" TabIndex="13" Enabled = "false" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAID_FOR_ANNUAL_LEAVE")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="14" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField required txtBox_N" Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="14" Width="130px" MaxLength="13" runat="server"
                                CssClass="RequiredField required  txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="14" Width="130px" MaxLength="13" runat="server"
                                CssClass=" RequiredField required txtBox_N" Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="15" Width="130px" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="15" Width="130px" runat="server" CssClass="RequiredField  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="15" Width="130px" runat="server" CssClass="  RequiredField  txtBox"
                                Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpToDate" TabIndex="16" Width="130px" runat="server" CssClass="  txtBox"
                                Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate"
                                TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpToDate" Width="130px" TabIndex="16" runat="server" CssClass="  txtBox"
                                ></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate"
                                TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="dtpToDate" TabIndex="16" Width="130px" runat="server" CssClass="  txtBox" Enabled="false"
                                Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>' ></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate"
                                TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" TabIndex="14"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="15"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="24" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="25" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="13" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                  <%--  <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" Visible="false" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" Visible="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="19" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="20" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
