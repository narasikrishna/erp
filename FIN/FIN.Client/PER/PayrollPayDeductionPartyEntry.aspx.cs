﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using FIN.BLL.PER;
using FIN.DAL.PER;

namespace FIN.Client.PER
{
    public partial class PayrollPayDeductionPartyEntry : PageBase
    {
        PAY_DEDUCTION_PARTY pAY_DEDUCTION_PARTY = new PAY_DEDUCTION_PARTY();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    Bankdetails.InnerText = Prop_File_Data["Process_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<PAY_DEDUCTION_PARTY> userCtx = new DataRepository<PAY_DEDUCTION_PARTY>())
                    {
                        pAY_DEDUCTION_PARTY = userCtx.Find(r =>
                            (r.PAY_DEDUCTION_PARTY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                   
                    EntityData = pAY_DEDUCTION_PARTY;
                    txtPaymentNumber.Text = pAY_DEDUCTION_PARTY.PAY_DEDUCTION_PARTY_ID.ToString();
                    ddlDeductionPartyNumber.SelectedValue = pAY_DEDUCTION_PARTY.DED_PARTY_CODE.ToString();
                    
                    //ddlPaymentMode.SelectedValue = pAY_DEDUCTION_PARTY.
                    if (pAY_DEDUCTION_PARTY.BANK_CODE != null)
                    {
                        Bankdetails.Visible = true;
                        fn_fillBank();
                        fn_fillChequeNo();
                        ddlBankName.SelectedValue = pAY_DEDUCTION_PARTY.BANK_CODE.ToString();
                        fn_fillBranch();
                        ddlBankBranch.SelectedValue = pAY_DEDUCTION_PARTY.BRANCH_CODE.ToString();
                        fn_fillAccountNo();
                        ddlAccountNumber.SelectedValue = pAY_DEDUCTION_PARTY.ACCOUNT_NUMBER.ToString();
                        ddlChequeNumber.SelectedValue = pAY_DEDUCTION_PARTY.EFT_CHEQUE_NUMBER.ToString();

                        if (pAY_DEDUCTION_PARTY.EFT_CHEQUE_DATE != null)
                        {
                            txtChequeDate.Text = DBMethod.ConvertDateToString(pAY_DEDUCTION_PARTY.EFT_CHEQUE_DATE.ToString());
                        }

                        //txtChequeDate.Text = ;
                        //ddlDeductionNumber.SelectedValue = pAY_DEDUCTION_PARTY
                        txtchqamount.Text = pAY_DEDUCTION_PARTY.EFT_CHEQUE_AMOUNT.ToString();
                    }
                    else
                    {
                        Bankdetails.Visible = false;

                    }
                    if (pAY_DEDUCTION_PARTY.ATTRIBUTE1 != null)
                    {
                        ddlPaymentMode.SelectedValue = pAY_DEDUCTION_PARTY.ATTRIBUTE1.ToString();
                    }
                    txtPaymentAmount.Text = pAY_DEDUCTION_PARTY.AMOUNT.ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PPDPE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pAY_DEDUCTION_PARTY = (PAY_DEDUCTION_PARTY)EntityData;
                }



                pAY_DEDUCTION_PARTY.DED_PARTY_CODE = ddlDeductionPartyNumber.SelectedValue.ToString();
                pAY_DEDUCTION_PARTY.ATTRIBUTE1 = ddlPaymentMode.SelectedValue.ToString();
                if ( ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    pAY_DEDUCTION_PARTY.BANK_CODE = ddlBankName.SelectedValue.ToString();
                    pAY_DEDUCTION_PARTY.BRANCH_CODE = ddlBankBranch.SelectedValue.ToString();
                    pAY_DEDUCTION_PARTY.ACCOUNT_NUMBER = ddlAccountNumber.SelectedValue.ToString();
                    // pAY_DEDUCTION_PARTY.PAYMENT_MODE = ddlPaymentMode.SelectedValue.ToString();
                    pAY_DEDUCTION_PARTY.EFT_CHEQUE_NUMBER = ddlChequeNumber.SelectedValue.ToString();

                    if (txtChequeDate.Text.ToString().Trim().Length > 0)
                    {
                        pAY_DEDUCTION_PARTY.EFT_CHEQUE_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text);
                    }
                    else
                    {
                        pAY_DEDUCTION_PARTY.EFT_CHEQUE_DATE = null;
                    }
                    //pAY_DEDUCTION_PARTY.EFT_CHEQUE_DATE = DateTime.Parse(txtChequeDate.Text);
                    pAY_DEDUCTION_PARTY.EFT_CHEQUE_AMOUNT = decimal.Parse(txtchqamount.Text);
                }
             //   pAY_DEDUCTION_PARTY.EFT_CHEQUE_AMOUNT = DBMethod.GetAmountDecimalValue(txtchqamount.Text);
               // pAY_DEDUCTION_PARTY.DED_ID = ddlDeductionNumber.SelectedValue.ToString();
                pAY_DEDUCTION_PARTY.AMOUNT = Decimal.Parse(txtPaymentAmount.Text.ToString());
                pAY_DEDUCTION_PARTY.ENABLED_FLAG = "1";
                pAY_DEDUCTION_PARTY.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_DEDUCTION_PARTY.MODIFIED_BY = this.LoggedUserName;
                    pAY_DEDUCTION_PARTY.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    pAY_DEDUCTION_PARTY.PAY_DEDUCTION_PARTY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.PER_011.ToString(), false, true);
                    //tAX_TERMS.WORKFLOW_COMPLETION_STATUS = "1";
                    pAY_DEDUCTION_PARTY.CREATED_BY = this.LoggedUserName;
                    pAY_DEDUCTION_PARTY.CREATED_DATE = DateTime.Today;


                }
                pAY_DEDUCTION_PARTY.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_DEDUCTION_PARTY.PAY_DEDUCTION_PARTY_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PPDPE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlPaymentMode, "PAYMENT_MODE");
            PayrollPayDeductionParty_BLL.fn_getDeductionParty(ref ddlDeductionPartyNumber);
            //PayrollPayDeductionParty_BLL
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<PAY_DEDUCTION_PARTY>(pAY_DEDUCTION_PARTY);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<PAY_DEDUCTION_PARTY>(pAY_DEDUCTION_PARTY, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<PAY_DEDUCTION_PARTY>(pAY_DEDUCTION_PARTY);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void ddlDeductionPartyNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillBank();
            fn_fillChequeNo();
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillBranch();
        }

        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillAccountNo();
        }

        private void fn_fillBank()
        {
            PayrollPayDeductionParty_BLL.fn_getBank(ref ddlBankName, ddlDeductionPartyNumber.SelectedValue);
        }

        private void fn_fillChequeNo()
        {
            PayrollPayDeductionParty_BLL.fn_getChequeNumber(ref ddlChequeNumber, ddlDeductionPartyNumber.SelectedValue);
        }

        private void fn_fillBranch()
        {
            PayrollPayDeductionParty_BLL.fn_getBranch(ref ddlBankBranch, ddlDeductionPartyNumber.SelectedValue, ddlBankName.SelectedValue);
        }

        private void fn_fillAccountNo()
        {
            PayrollPayDeductionParty_BLL.fn_getAccount(ref ddlAccountNumber, ddlDeductionPartyNumber.SelectedValue, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }

        protected void txtPaymentDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMode.SelectedItem.Text == "Cheque")
            {
                //ddlBankName.Visible = false;
                //ddlBankBranch.Visible = false;
                //ddlAccountNumber.Visible = false;
                //ddlChequeNumber.Visible = false;
                //txtChequeDate.Visible = false;
                //txtchqamount.Visible = false;
                Bankdetails.Visible = true;
            }
            else
            {
                Bankdetails.Visible = false;
            }
        }

      
    }
}