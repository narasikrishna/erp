﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using FIN.BLL.PER;
using FIN.DAL.PER;


namespace FIN.Client.PER
{
    public partial class PayrollDeductionPartyEntry : PageBase
    {
        
        PAY_DED_PARTY_ENROLLMENT pAY_DED_PARTY_ENROLLMENT = new PAY_DED_PARTY_ENROLLMENT();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<PAY_DED_PARTY_ENROLLMENT> userCtx = new DataRepository<PAY_DED_PARTY_ENROLLMENT>())
                    {
                        pAY_DED_PARTY_ENROLLMENT = userCtx.Find(r =>
                            (r.DED_PARTY_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = pAY_DED_PARTY_ENROLLMENT;

                    txtDeductionPartyNumber.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_CODE.ToString();
                    txtDeductionPartyNameEn.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_NAME.ToString();
                    txtDeductionPartyNameAr.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_NAME_OL.ToString();
                    ddlPaymentMode.SelectedValue = pAY_DED_PARTY_ENROLLMENT.PAYMENT_MODE.ToString();
                    ddlElement.SelectedValue = pAY_DED_PARTY_ENROLLMENT.PAY_ELEMENT_ID.ToString();
                    txtAddress1.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_ADD1.ToString();
                    txtAddress2.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_ADD2.ToString();
                    ddlCountry.SelectedValue = pAY_DED_PARTY_ENROLLMENT.ATTRIBUTE1;
                    fill_state();
                    ddlState.SelectedValue = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_STATE;
                    fill_city();
                    ddlCity.SelectedValue = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_CITY;
                    if (pAY_DED_PARTY_ENROLLMENT.DED_PARTY_PIN != null)
                    {
                        txtPostalCode.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_PIN;
                    }
                    if (pAY_DED_PARTY_ENROLLMENT.DED_PARTY_MOBILE != null)
                    {
                        txtPhone.Text = pAY_DED_PARTY_ENROLLMENT.DED_PARTY_MOBILE;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pAY_DED_PARTY_ENROLLMENT = (PAY_DED_PARTY_ENROLLMENT)EntityData;
                }



                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_NAME = txtDeductionPartyNameEn.Text.ToString();
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_NAME_OL = txtDeductionPartyNameAr.Text.ToString();
                pAY_DED_PARTY_ENROLLMENT.PAYMENT_MODE = ddlPaymentMode.SelectedValue.ToString();
                pAY_DED_PARTY_ENROLLMENT.PAY_ELEMENT_ID = ddlElement.SelectedValue.ToString();
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_ADD1 = txtAddress1.Text.ToString();
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_ADD2 = txtAddress2.Text.ToString();
                //pAY_DED_PARTY_ENROLLMENT.DED_PARTY_ADD = txtAddress3.Text;
                pAY_DED_PARTY_ENROLLMENT.ATTRIBUTE1 = ddlCountry.SelectedValue.ToString();
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_STATE = ddlState.SelectedValue;
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_CITY = ddlCity.SelectedValue;
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_PIN = txtPostalCode.Text;
                pAY_DED_PARTY_ENROLLMENT.DED_PARTY_MOBILE = txtPhone.Text;
                pAY_DED_PARTY_ENROLLMENT.ENABLED_FLAG = "1";



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_DED_PARTY_ENROLLMENT.MODIFIED_BY = this.LoggedUserName;
                    pAY_DED_PARTY_ENROLLMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    pAY_DED_PARTY_ENROLLMENT.DED_PARTY_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.PER_008.ToString(), false, true);
                    //tAX_TERMS.WORKFLOW_COMPLETION_STATUS = "1";
                    pAY_DED_PARTY_ENROLLMENT.CREATED_BY = this.LoggedUserName;
                    pAY_DED_PARTY_ENROLLMENT.CREATED_DATE = DateTime.Today;


                }
                pAY_DED_PARTY_ENROLLMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_DED_PARTY_ENROLLMENT.DED_PARTY_CODE);
                pAY_DED_PARTY_ENROLLMENT.DED_ORG_ID = VMVServices.Web.Utils.OrganizationID;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PDPE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlPaymentMode, "PAYMENT_MODE");
            PayrollDeductionParty_BLL.fn_getCountry(ref ddlCountry);
            PayrollDeductionParty_BLL.fn_getPayElements_FR_DEDUCTION(ref ddlElement);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();



                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtDeductionPartyNameEn;
                slControls[1] = ddlPaymentMode;
                slControls[2] = ddlElement;
                slControls[3] = txtAddress1;
                slControls[4] = txtAddress2;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();
                string strCtrlTypes = "TextBox~DropDownList~DropDownList~TextBox~TextBox";
                string strMessage = Prop_File_Data["Deduction_Party_Name_P"] + " ~ " + Prop_File_Data["Payment_Mode_P"] + " ~ " + Prop_File_Data["Element_P"] + " ~ " + Prop_File_Data["Address_1_P"] + " ~ " + Prop_File_Data["Address_2_P"] + "";
                //string strMessage = "Deduction Party Name ~ Payment Mode ~ Element ~ Address1 ~ Address2";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }




                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<PAY_DED_PARTY_ENROLLMENT>(pAY_DED_PARTY_ENROLLMENT);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<PAY_DED_PARTY_ENROLLMENT>(pAY_DED_PARTY_ENROLLMENT, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<PAY_DED_PARTY_ENROLLMENT>(pAY_DED_PARTY_ENROLLMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            fill_state();
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            fill_city();
        }

        private void fill_state()
        {
            PayrollDeductionParty_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
            //fill_city();
        }

        private void fill_city()
        {
            PayrollDeductionParty_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        protected void txtDeductionPartyName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}