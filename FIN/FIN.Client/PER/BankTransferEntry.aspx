﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankTransferEntry.aspx.cs" Inherits="FIN.Client.CA.BankTransfer" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="Div2">
                Payment Advice
            </div>
            <div class="divtxtBox LNOrient" style="  width: 530px">
                <asp:DropDownList ID="ddlpayAdvice" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" OnSelectedIndexChanged="ddlpayAdvice_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="Div3">
                Bank Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtBankName" TabIndex="2" MaxLength="50" CssClass="txtBox" Enabled="false"
                    runat="server"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div4">
                IFSC Code
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtIfscCode" TabIndex="2" MaxLength="50" CssClass="txtBox" Enabled="false"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 530px">
                <asp:TextBox ID="txtAccountNumber" TabIndex="2" MaxLength="50" CssClass="txtBox"
                    Enabled="false" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 130px" id="lblFinancialYear">
                Transfer Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" TabIndex="3" ID="txtDate" AutoPostBack="true" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"
                    OnTextChanged="txtDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 450px" id="Div1" align="center">
                <asp:Button ID="btnProcess" runat="server" Text="Transfer" ToolTip="btnProcess" CssClass="btn"
                    TabIndex="4" OnClick="btnSave_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_download" runat="server" visible="false">
            <div class="lblBox LNOrient" style=" ">
                Download Does not start,Please click <a href="../UploadFile/DocFile/AccountDetails.txt"
                    target="_newAccount">here</a> to download
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvDtlData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="emp_no" HeaderText="Employee Number" />
                    <asp:BoundField DataField="emp_name" HeaderText="Employee Name" />
                    <asp:BoundField DataField="emp_iban_num" HeaderText="IBAN Number" />
                    <asp:BoundField DataField="salary" HeaderText="Salary">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tranDate" HeaderText="Date" />
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
