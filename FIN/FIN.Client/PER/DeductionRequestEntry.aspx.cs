﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
namespace FIN.Client.PER
{
    public partial class DeductionRequestEntry : PageBase
    {
        HR_ADVANCE_REQ hR_ADVANCE_REQ = new HR_ADVANCE_REQ();
        DataTable dt_dept_design_data = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                // pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                txtRequestDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtLoanStartDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());


                IdRentded1.Visible = false;
                IdRentded2.Visible = false;
                IdRentded3.Visible = false;

                IdMobded1.Visible = false;
                IdMobded2.Visible = false;
                IdMedDed1.Visible = false;
                IdMedDed2.Visible = false;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    ddlEmployeeName.Enabled = false;

                    using (IRepository<HR_ADVANCE_REQ> userCtx = new DataRepository<HR_ADVANCE_REQ>())
                    {
                        hR_ADVANCE_REQ = userCtx.Find(r =>
                            (r.REQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_ADVANCE_REQ;

                    txtRequestNumber.Text = hR_ADVANCE_REQ.REQ_ID;
                    ddlEmployeeName.SelectedValue = hR_ADVANCE_REQ.REQ_EMP_ID;
                    fn_fill_deptDesign();
                    //ddlDepartment.SelectedValue = hR_ADVANCE_REQ.REQ_DEPT_ID;
                    //FILLDESIG();
                    ddlType.SelectedValue = hR_ADVANCE_REQ.REQ_ADVANCE_TYPE;
                    // ddlDesignation.SelectedValue = hR_ADVANCE_REQ.REQ_DESIG_ID;
                    txtAmount.Text = hR_ADVANCE_REQ.REQ_AMOUNT.ToString();
                    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);

                    if (hR_ADVANCE_REQ.REQ_ADVANCE_TYPE.ToUpper() == "RENT")
                    {
                        IdRentded1.Visible = true;
                        IdRentded2.Visible = true;
                        IdRentded3.Visible = true;
                        IdMobded1.Visible = false;
                        IdMobded2.Visible = false;
                        IdMedDed1.Visible = false;
                        IdMedDed2.Visible = false;

                        txtActrentamt.Text = hR_ADVANCE_REQ.RD_ACTUAL_RENT_AMOUNT.ToString();
                        txtActrentamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtActrentamt.Text);
                        txtDiscount.Text = hR_ADVANCE_REQ.RD_DISCOUNT.ToString();
                        txtDiscount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtDiscount.Text);
                        txtRecovamt.Text = hR_ADVANCE_REQ.RD_RECOVERY_AMT.ToString();
                        txtRecovamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtRecovamt.Text);
                        txtHoldreason.Text = hR_ADVANCE_REQ.RD_HOLD_REASON;

                        if (hR_ADVANCE_REQ.RD_HOLD == "1")
                        {
                            chkHold.Checked = true;
                        }
                        else
                        {
                            chkHold.Checked = false;
                        }

                    }
                    if (hR_ADVANCE_REQ.REQ_ADVANCE_TYPE.ToUpper() == "MOBILE")
                    {
                        IdMobded1.Visible = true;
                        IdMobded2.Visible = true;

                        IdRentded1.Visible = false;
                        IdRentded2.Visible = false;
                        IdRentded3.Visible = false;
                        IdMedDed1.Visible = false;
                        IdMedDed2.Visible = false;

                        txtMobremarks.Text = hR_ADVANCE_REQ.MOB_REMARKS;
                        txtMobbilldtl.Text = hR_ADVANCE_REQ.MOB_BILL_DTLS;
                    }
                    if (hR_ADVANCE_REQ.REQ_ADVANCE_TYPE.ToUpper() == "MEDICAL")
                    {
                        IdMedDed1.Visible = true;

                        IdRentded1.Visible = false;
                        IdRentded2.Visible = false;
                        IdRentded3.Visible = false;
                        IdMobded1.Visible = false;
                        IdMobded2.Visible = false;
                        IdMedDed2.Visible = false;

                        txtMedinsdtl.Text = hR_ADVANCE_REQ.MD_INSURANCE_DTL;
                    }
                    if (hR_ADVANCE_REQ.REQ_ADVANCE_TYPE.ToUpper() == "MEDICAL INVOICE")
                    {
                        IdMedDed2.Visible = true;

                        IdMedDed1.Visible = false;
                        IdRentded1.Visible = false;
                        IdRentded2.Visible = false;
                        IdRentded3.Visible = false;
                        IdMobded1.Visible = false;
                        IdMobded2.Visible = false;

                        txtMedInvdtl.Text = hR_ADVANCE_REQ.MD_INS_INVOICE_DTL;
                    }

                    //  ddlStatus.SelectedValue = hR_ADVANCE_REQ.REQ_STATUS;
                    txtComments.Text = hR_ADVANCE_REQ.REQ_COMMENTS;
                    txtNoOfInstallments.Text = hR_ADVANCE_REQ.NO_OF_INSTALLMENTS.ToString();
                    txtInstallmentAmount.Text = hR_ADVANCE_REQ.INSTALLMENT_AMT.ToString();
                    txtInstallmentAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInstallmentAmount.Text);

                    if (hR_ADVANCE_REQ.REQ_DT != null)
                    {
                        txtRequestDate.Text = DBMethod.ConvertDateToString(hR_ADVANCE_REQ.REQ_DT.ToString());
                    }

                    if (hR_ADVANCE_REQ.LOAN_START_DT != null)
                    {
                        txtLoanStartDate.Text = DBMethod.ConvertDateToString(hR_ADVANCE_REQ.LOAN_START_DT.ToString());
                    }
                    if (hR_ADVANCE_REQ.PAYMENT_RELEASE_DT != null)
                    {
                        txtPaymentReleaseDate.Text = DBMethod.ConvertDateToString(hR_ADVANCE_REQ.PAYMENT_RELEASE_DT.ToString());
                    }

                    ChkEnabledFlag.Checked = hR_ADVANCE_REQ.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    //hR_ADVANCE_REQ.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    //hR_ADVANCE_REQ.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    hR_ADVANCE_REQ.REQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_ADVANCE_REQ = (HR_ADVANCE_REQ)EntityData;
                }

                hR_ADVANCE_REQ.REQ_ID = txtRequestNumber.Text;
                hR_ADVANCE_REQ.REQ_EMP_ID = ddlEmployeeName.SelectedValue;

                dt_dept_design_data = DBMethod.ExecuteQuery(LoanRequest_DAL.GetEmployeeDept_design(ddlEmployeeName.SelectedValue.ToString())).Tables[0];
                if (dt_dept_design_data != null)
                {
                    if (dt_dept_design_data.Rows.Count > 0)
                    {
                        hR_ADVANCE_REQ.REQ_DEPT_ID = dt_dept_design_data.Rows[0]["emp_dept_id"].ToString();
                        hR_ADVANCE_REQ.REQ_DESIG_ID = dt_dept_design_data.Rows[0]["emp_desig_id"].ToString();
                    }
                }

                hR_ADVANCE_REQ.REQ_ADVANCE_TYPE = ddlType.SelectedValue;

                //hR_ADVANCE_REQ.REQ_AMOUNT = int.Parse(txtAmount.Text.ToString());
                hR_ADVANCE_REQ.REQ_AMOUNT = CommonUtils.ConvertStringToDecimal(txtAmount.Text);

                // hR_ADVANCE_REQ.REQ_STATUS = ddlStatus.SelectedValue;
                hR_ADVANCE_REQ.REQ_COMMENTS = txtComments.Text;
                hR_ADVANCE_REQ.NO_OF_INSTALLMENTS = int.Parse(txtNoOfInstallments.Text.ToString());
                //hR_ADVANCE_REQ.INSTALLMENT_AMT = int.Parse(txtInstallmentAmount.Text.ToString());
                hR_ADVANCE_REQ.INSTALLMENT_AMT = CommonUtils.ConvertStringToDecimal(txtInstallmentAmount.Text);
                hR_ADVANCE_REQ.REQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (ddlType.SelectedValue.ToUpper() == "RENT")
                {
                    hR_ADVANCE_REQ.RD_ACTUAL_RENT_AMOUNT = CommonUtils.ConvertStringToDecimal(txtActrentamt.Text);
                    hR_ADVANCE_REQ.RD_DISCOUNT = CommonUtils.ConvertStringToDecimal(txtDiscount.Text);
                    hR_ADVANCE_REQ.RD_RECOVERY_AMT = CommonUtils.ConvertStringToDecimal(txtRecovamt.Text);
                    hR_ADVANCE_REQ.RD_HOLD_REASON = txtHoldreason.Text;
                    if (chkHold.Checked == true)
                    {
                        hR_ADVANCE_REQ.RD_HOLD = FINAppConstants.EnabledFlag;
                    }
                    else
                    {
                        hR_ADVANCE_REQ.RD_HOLD = FINAppConstants.DisabledFlag;
                    }
                }
                if (ddlType.SelectedValue.ToUpper() == "MOBILE")
                {
                    hR_ADVANCE_REQ.MOB_REMARKS = txtMobremarks.Text;
                    hR_ADVANCE_REQ.MOB_BILL_DTLS = txtMobbilldtl.Text;
                }
                if (ddlType.SelectedValue.ToUpper() == "MEDICAL")
                {
                    hR_ADVANCE_REQ.MD_INSURANCE_DTL = txtMedinsdtl.Text;
                }

                if (ddlType.SelectedValue.ToUpper() == "MEDICAL INVOICE")
                {
                    hR_ADVANCE_REQ.MD_INS_INVOICE_DTL = txtMedInvdtl.Text;
                }

                if (txtRequestDate.Text != string.Empty)
                {
                    hR_ADVANCE_REQ.REQ_DT = DBMethod.ConvertStringToDate(txtRequestDate.Text.ToString());
                }

                if (txtLoanStartDate.Text != string.Empty)
                {
                    hR_ADVANCE_REQ.LOAN_START_DT = DBMethod.ConvertStringToDate(txtLoanStartDate.Text.ToString());
                }
                if (txtPaymentReleaseDate.Text != string.Empty)
                {
                    hR_ADVANCE_REQ.PAYMENT_RELEASE_DT = DBMethod.ConvertStringToDate(txtPaymentReleaseDate.Text.ToString());
                }

                //  hR_ADVANCE_REQ.ENABLED_FLAG = FINAppConstants.Y;

                hR_ADVANCE_REQ.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_ADVANCE_REQ.MODIFIED_BY = this.LoggedUserName;
                    hR_ADVANCE_REQ.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_ADVANCE_REQ.REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_034.ToString(), false, true);

                    hR_ADVANCE_REQ.CREATED_BY = this.LoggedUserName;
                    hR_ADVANCE_REQ.CREATED_DATE = DateTime.Today;

                }

                hR_ADVANCE_REQ.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_ADVANCE_REQ.REQ_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            //LoanRequest_BLL.GetEmployeeName(ref ddlEmployeeName);
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            //    Department_BLL.GetDepartmentName(ref ddlDepartment);
            LoanRequest_BLL.GetLookUpValues(ref ddlType, "LRT", null);
            //   Lookup_BLL.GetLookUpValues(ref ddlStatus, "LRS");



        }




        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if ((DBMethod.ConvertStringToDate(txtLoanStartDate.Text.ToString()) < DBMethod.ConvertStringToDate(txtPaymentReleaseDate.Text.ToString())))
                {
                    ErrorCollection.Add("Invalid Date", "Loan start date must be greater than or equal to Request date and Loan start Date");
                    return;
                }


                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_ADVANCE_REQ>(hR_ADVANCE_REQ);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            InsertAutoRepayPlan();
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_ADVANCE_REQ>(hR_ADVANCE_REQ, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }






            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        private void InsertAutoRepayPlan()
        {
            DateTime dt_repaydate = DBMethod.ConvertStringToDate(txtLoanStartDate.Text);
            for (int rLoop = 0; rLoop < int.Parse(txtNoOfInstallments.Text); rLoop++)
            {
                HR_ADVANCE_REPAY_PLAN hR_ADVANCE_REPAY_PLAN = new HR_ADVANCE_REPAY_PLAN();
                hR_ADVANCE_REPAY_PLAN.RES_ID = hR_ADVANCE_REQ.REQ_ID;
                //hR_ADVANCE_REPAY_PLAN.REPAY_AMT = Convert.ToInt32(txtInstallmentAmount.Text.ToString());
                hR_ADVANCE_REPAY_PLAN.REPAY_AMT = CommonUtils.ConvertStringToDecimal(txtInstallmentAmount.Text);
                hR_ADVANCE_REPAY_PLAN.INSTALLMENT_NO = rLoop;
                hR_ADVANCE_REPAY_PLAN.REPAY_COMMENTS = "";
                hR_ADVANCE_REPAY_PLAN.REPAY_DT = dt_repaydate;
                hR_ADVANCE_REPAY_PLAN.REPAY_ACTUAL_DT = null;
                hR_ADVANCE_REPAY_PLAN.REPAY_PAIDYN = FINAppConstants.N;
                hR_ADVANCE_REPAY_PLAN.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_ADVANCE_REPAY_PLAN.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                hR_ADVANCE_REPAY_PLAN.REPAY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_035_D.ToString(), false, true);
                hR_ADVANCE_REPAY_PLAN.CREATED_BY = this.LoggedUserName;
                hR_ADVANCE_REPAY_PLAN.CREATED_DATE = DateTime.Today;
                DBMethod.SaveEntity<HR_ADVANCE_REPAY_PLAN>(hR_ADVANCE_REPAY_PLAN);
                dt_repaydate = dt_repaydate.AddMonths(1);

            }

        }


        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                divEligRemark.Visible = true;
                fn_fill_deptDesign();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtNoOfInstallments_TextChanged(object sender, EventArgs e)
        {
            if (txtNoOfInstallments.Text.Trim() != string.Empty && txtAmount.Text.Trim() != string.Empty)
            {
                txtInstallmentAmount.Text = Math.Round(CommonUtils.ConvertStringToDecimal(txtAmount.Text) / CommonUtils.ConvertStringToDecimal(txtNoOfInstallments.Text), 2).ToString();
                txtInstallmentAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInstallmentAmount.Text);
            }
        }

        protected void txtRequestDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtempdays_fromjoining = new DataTable();
                DataTable dtloaneligibleindays = new DataTable();
                divEligRemark.Visible = false;
                dtempdays_fromjoining = DBMethod.ExecuteQuery(LoanRequest_DAL.GetEmpdays_fromjoining(ddlEmployeeName.SelectedValue.ToString())).Tables[0];
                dtloaneligibleindays = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoaneligibleindays()).Tables[0];
                if (dtempdays_fromjoining.Rows.Count > 0)
                {
                    hfdempdays_fromjoining.Value = dtempdays_fromjoining.Rows[0]["DAYS_EMP"].ToString();
                }
                if (dtloaneligibleindays.Rows.Count > 0)
                {
                    hfloaneligibleindays.Value = dtloaneligibleindays.Rows[0]["LOAN_ELIGIBLE"].ToString();
                }
                if (dtempdays_fromjoining.Rows.Count > 0 && hfdempdays_fromjoining.Value != "")
                {
                    if (dtloaneligibleindays.Rows.Count > 0)
                    {
                        if (int.Parse(hfloaneligibleindays.Value) > int.Parse(hfdempdays_fromjoining.Value))
                        {
                            mpeEligible.Show();
                            return;

                        }
                    }
                }

                DataTable dtlastlnreqdt = new DataTable();
                DataTable dtlnreeligibledt = new DataTable();

                dtlastlnreqdt = DBMethod.ExecuteQuery(LoanRequest_DAL.Getlastloanreqdt(ddlEmployeeName.SelectedValue.ToString())).Tables[0];
                dtlnreeligibledt = DBMethod.ExecuteQuery(LoanRequest_DAL.Getloanreeligibledt()).Tables[0];
                if (dtlastlnreqdt.Rows.Count > 0)
                {
                    hflastlnreqdt.Value = dtlastlnreqdt.Rows[0]["LAST_LOAN_REQ_DT"].ToString();
                }
                if (dtlnreeligibledt.Rows.Count > 0)
                {
                    hflnreeligibledt.Value = dtlnreeligibledt.Rows[0]["REELIGIBLE_DT"].ToString();
                }
                if (dtlastlnreqdt.Rows.Count > 0 && hflastlnreqdt.Value != "")
                {
                    if (dtlnreeligibledt.Rows.Count > 0)
                    {
                        if (int.Parse(hflastlnreqdt.Value) < int.Parse(hflnreeligibledt.Value))
                        {
                            mpAllowloan.Show();
                            return;

                        }
                    }
                }

                fn_fill_deptDesign();
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("validln", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void fn_fill_deptDesign()
        {
            dt_dept_design_data = DBMethod.ExecuteQuery(LoanRequest_DAL.GetEmployeeDept_design(ddlEmployeeName.SelectedValue.ToString())).Tables[0];
            if (dt_dept_design_data != null)
            {
                if (dt_dept_design_data.Rows.Count > 0)
                {
                    txtDept.Text = dt_dept_design_data.Rows[0]["dept_name"].ToString();
                    txtDesignation.Text = dt_dept_design_data.Rows[0]["desig_name"].ToString();
                }
            }
        }

        protected void btNo_Click(object sender, EventArgs e)
        {
            txtRequestNumber.Text = "";
            txtRequestDate.Text = "";
            ddlEmployeeName.SelectedValue = "0";
            ddlType.SelectedValue = "0";
            txtDept.Text = "";
            txtDesignation.Text = "";
            //ddlStatus.SelectedValue = "0";
            txtAmount.Text = "";
            txtComments.Text = "";
            txtNoOfInstallments.Text = "";
            txtInstallmentAmount.Text = "";
            txtPaymentReleaseDate.Text = "";
            txtLoanStartDate.Text = "";


        }

        protected void Button1_Click(object sender, EventArgs e)
        {


            divEligRemark.Visible = true;
            fn_fill_deptDesign();


        }

        protected void btnEligibleNO_Click(object sender, EventArgs e)
        {
            divEligRemark.Visible = false;
            ddlEmployeeName.SelectedValue = "";
            txtDept.Text = "";
            txtDesignation.Text = "";

        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue.ToUpper() == "RENT")
            {
                IdRentded1.Visible = true;
                IdRentded2.Visible = true;
                IdRentded3.Visible = true;

                IdMobded1.Visible = false;
                IdMobded2.Visible = false;
                IdMedDed1.Visible = false;
                IdMedDed2.Visible = false;
            }

            if (ddlType.SelectedValue.ToUpper() == "MOBILE")
            {
                IdMobded1.Visible = true;
                IdMobded2.Visible = true;

                IdRentded1.Visible = false;
                IdRentded2.Visible = false;
                IdRentded3.Visible = false;
                IdMedDed1.Visible = false;
                IdMedDed2.Visible = false;

            }
            if (ddlType.SelectedValue.ToUpper() == "MEDICAL")
            {
                IdMedDed1.Visible = true;

                IdRentded1.Visible = false;
                IdRentded2.Visible = false;
                IdRentded3.Visible = false;
                IdMobded1.Visible = false;
                IdMobded2.Visible = false;
                IdMedDed2.Visible = false;



            }
            if (ddlType.SelectedValue.ToUpper() == "MEDICAL INVOICE")
            {
                IdMedDed2.Visible = true;

                IdMedDed1.Visible = false;
                IdRentded1.Visible = false;
                IdRentded2.Visible = false;
                IdRentded3.Visible = false;
                IdMobded1.Visible = false;
                IdMobded2.Visible = false;
            }


        }


    }
}