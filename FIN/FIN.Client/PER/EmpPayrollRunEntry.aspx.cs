﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.DAL.PER;
using VMVServices.Web;
using VMVServices.Services.Data;


namespace FIN.Client.PER
{
    public partial class EmpPayrollRunEntry : PageBase
    {
        Boolean bol_rowVisiable;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;

            }
            else
            {
                btnSave.Visible = true;

            }
            if (Master.Mode == FINAppConstants.Update)
            {
                //btnSave.Text = "Update";
            }

            UserRightsChecking();

        }


        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                EntityData = null;



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod);
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
            FIN.BLL.HR.Employee_BLL.GetEmpName(ref ddlEmpName);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                string str_Departments = hf_dept_id.Value.ToString();
                FIN.DAL.HR.PayrollTrailrunDetailsEntry.RunSPFOR_Trailpayroll("", ddlPayrollPeriod.SelectedValue, str_Departments, ddlEmpName.SelectedValue);
                loadEmpSalaryDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnOkay_Click(object sender, EventArgs e)
        {
            for (int iLoop = 0; iLoop < gvEmployeePay.Rows.Count; iLoop++)
            {
                TextBox txt_Amount = (TextBox)gvEmployeePay.Rows[iLoop].FindControl("txtAmount");

                if (gvEmployeePay.DataKeys[iLoop].Values["pay_element_Type"].ToString() == "Earning")
                {
                    DBMethod.ExecuteNonQuery(" update pay_trial_run_dtl_dtl set pay_amount=" + CommonUtils.ConvertStringToDecimal(txt_Amount.Text.ToString()) + "where PAYROLL_DTL_DTL_ID='" + gvEmployeePay.DataKeys[iLoop].Values["PAYROLL_DTL_DTL_ID"].ToString() + "'");
                }
            }

            for (int jLoop = 0; jLoop < gvEmployeePay2.Rows.Count; jLoop++)
            {
                TextBox txtAmount2 = (TextBox)gvEmployeePay2.Rows[jLoop].FindControl("txtAmount2");

                if (gvEmployeePay.DataKeys[jLoop].Values["pay_element_Type"].ToString() == "Deduction")
                {
                    DBMethod.ExecuteNonQuery(" update pay_trial_run_dtl_dtl set pay_amount=" + CommonUtils.ConvertStringToDecimal(txtAmount2.Text.ToString()) + "where PAYROLL_DTL_DTL_ID='" + gvEmployeePay2.DataKeys[jLoop].Values["PAYROLL_DTL_DTL_ID"].ToString() + "'");
                }
            }

            ErrorCollection.Add("ElementAmountSave", "Element Amount Saved Successfully");
            Master.ShowMessage(ErrorCollection, FINAppConstants.SAVED);
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            getBranchName();

        }
        private void getBranchName()
        {
            FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());
        }
        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            getAccountName();
        }
        private void getAccountName()
        {
            FIN.BLL.CA.BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }
        protected void ddlAccountnumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            getChequeNumber();
        }
        private void getChequeNumber()
        {
            FIN.BLL.CA.Cheque_BLL.GetUnusedCheckNumberDetails(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue, Master.Mode);
            if (ddlChequeNumber.Items.Count > 1)
            {
                ddlChequeNumber.SelectedIndex = 1;
            }
        }

        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadEmpSalaryDetails();
        }
        private void loadEmpSalaryDetails()
        {
            divEmpSalDet.Visible = false;
            btnSave.Enabled = true;
            btnOkay.Enabled = true;
            btnPost.Enabled = true;
            DivBank.Visible=false ;
            btnPost.Visible = false;
            DataTable dtgetdtlEmp = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetDtlEmp(ddlEmpName.SelectedValue)).Tables[0];
            if (dtgetdtlEmp.Rows.Count > 0)
            {
                lblEmp.Text = dtgetdtlEmp.Rows[0]["EMP_NAME"].ToString();
                lbldept.Text = dtgetdtlEmp.Rows[0]["DEPT_NAME"].ToString();
                lblDesig.Text = dtgetdtlEmp.Rows[0]["DESIG_NAME"].ToString();
                lblDoj.Text = DBMethod.ConvertDateToString(dtgetdtlEmp.Rows[0]["EMP_DOJ"].ToString());
                hf_dept_id.Value = dtgetdtlEmp.Rows[0]["DEPT_ID"].ToString();
            }
            DataTable dt_payrolldtlid = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getPayDtlid4periodemp(ddlPayrollPeriod.SelectedValue, ddlEmpName.SelectedValue)).Tables[0];
            if (dt_payrolldtlid.Rows.Count > 0)
            {
                DataTable dtEmployeePay = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtlDtl(dt_payrolldtlid.Rows[0]["PAYROLL_DTL_ID"].ToString(), ddlEmpName.SelectedValue)).Tables[0];

                BindGrid_EmployeePayData(dtEmployeePay);
                BindGrid_EmployeePayData2(dtEmployeePay);
                divEmpSalDet.Visible = true;
                DivBank.Visible = true;
                btnPost.Visible = true;
                hf_Payroll_Id.Value = dt_payrolldtlid.Rows[0]["PAYROLL_ID"].ToString();
               
                if (dt_payrolldtlid.Rows[0]["Workflow_Completion_Status"].ToString() == FINAppConstants.Y)
                {
                    btnSave.Enabled = false;
                    btnOkay.Enabled = false;
                    btnPost.Enabled = false;
                }

                HR_EMP_PAYROLL_DTLS hR_EMP_PAYROLL_DTLS = new HR_EMP_PAYROLL_DTLS();
                using (IRepository<HR_EMP_PAYROLL_DTLS> userCtx = new DataRepository<HR_EMP_PAYROLL_DTLS>())
                {
                    hR_EMP_PAYROLL_DTLS = userCtx.Find(r =>
                        (r.PAYROLL_PERIOD == ddlPayrollPeriod.SelectedValue && r.PAYROLL_ID == hf_Payroll_Id.Value && r.PAYROLL_EMP_ID == ddlEmpName.SelectedValue)
                        ).SingleOrDefault();
                }
                if(hR_EMP_PAYROLL_DTLS != null)
                {
                    ddlBankName.SelectedValue = hR_EMP_PAYROLL_DTLS.BANK_ID;
                    getBranchName();
                    ddlBankBranch.SelectedValue = hR_EMP_PAYROLL_DTLS.BRANCH_ID;
                    getAccountName();
                    ddlAccountnumber.SelectedValue = hR_EMP_PAYROLL_DTLS.ACCOUNT_ID;
                    getChequeNumber();
                    ddlChequeNumber.SelectedValue = hR_EMP_PAYROLL_DTLS.CHEQUE_DTLS_ID;
                    btnPost.Enabled=false;
                    btnOkay.Enabled=false ;
                    btnSave.Enabled=false ;
                }
            }

        }

        private void BindGrid_EmployeePayData(DataTable dtEmployeePayData)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData"] = dtEmployeePayData;

            if (dtEmployeePayData.Rows.Count > 0)
            {
                dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay.DataSource = dtEmployeePayData;
            gvEmployeePay.DataBind();
            lblTotalEar.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData, "pay_amount", " pay_element_Type='Earning'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();


        }

        private void BindGrid_EmployeePayData2(DataTable dtEmployeePayData2)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData2"] = dtEmployeePayData2;

            if (dtEmployeePayData2.Rows.Count > 0)
            {
                dtEmployeePayData2.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData2.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData2.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay2.DataSource = dtEmployeePayData2;
            gvEmployeePay2.DataBind();
            lblTotDed.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData2, "pay_amount", " pay_element_Type='Deduction'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();


        }


        protected void gvEmployeePay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Deduction")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void gvEmployeePay2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;


                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Earning")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                HR_EMP_PAYROLL_DTLS hR_EMP_PAYROLL_DTLS = new HR_EMP_PAYROLL_DTLS();
                hR_EMP_PAYROLL_DTLS.PAYROLL_PERIOD = ddlPayrollPeriod.SelectedValue;
                hR_EMP_PAYROLL_DTLS.PAYROLL_ID = hf_Payroll_Id.Value.ToString();
                hR_EMP_PAYROLL_DTLS.PAYROLL_EMP_ID = ddlEmpName.SelectedValue;
                hR_EMP_PAYROLL_DTLS.POST_FLAG = FINAppConstants.Y;
                hR_EMP_PAYROLL_DTLS.BANK_ID = ddlBankName.SelectedValue;
                hR_EMP_PAYROLL_DTLS.BRANCH_ID = ddlBankBranch.SelectedValue;
                hR_EMP_PAYROLL_DTLS.ACCOUNT_ID = ddlAccountnumber.SelectedValue;
                hR_EMP_PAYROLL_DTLS.CHEQUE_DTLS_ID = ddlChequeNumber.SelectedValue;
                hR_EMP_PAYROLL_DTLS.EMP_PAY_RUN_ID = FINSP.GetSPFOR_SEQCode("PR_023".ToString(), false, true);
                hR_EMP_PAYROLL_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                hR_EMP_PAYROLL_DTLS.CREATED_BY = this.LoggedUserName;
                hR_EMP_PAYROLL_DTLS.CREATED_DATE = DateTime.Today;
                hR_EMP_PAYROLL_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                DBMethod.SaveEntity<HR_EMP_PAYROLL_DTLS>(hR_EMP_PAYROLL_DTLS);
                DBMethod.ExecuteNonQuery(" update pay_trial_run_hdr set WORKFLOW_COMPLETION_STATUS='" +  FINAppConstants.Y + "' where payroll_id='" + hf_Payroll_Id.Value.ToString() + "'");
                DBMethod.ExecuteNonQuery(FIN.DAL.PER.PayrollRun_DAL.UpdateFinalRunStatus(hf_Payroll_Id.Value.ToString()));
                FIN.DAL.PER.PayrollTrailrunDetails_DAL.Payroll_Trail_MainPosting(hf_Payroll_Id.Value.ToString());
                FIN.DAL.HR.PayrollTrailrunDetailsEntry.RunSPFOR_GL_POSTING_PR(hf_Payroll_Id.Value);

                btnPost.Enabled = false;
                btnOkay.Enabled = false;
                btnSave.Enabled = false;
                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {

                    cA_CHECK_DTL.CHECK_PAY_TO = ddlEmpName.SelectedItem.Text;

                    cA_CHECK_DTL.CHECK_DT = DateTime.Now;
                    cA_CHECK_DTL.CHECK_AMT = decimal.Parse(lblNet.Text);
                    cA_CHECK_DTL.CHECK_STATUS = "USED";
                    cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                    cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                    cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                }


              
                ErrorCollection.Add("Posted", "Data Posted Successfully");
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}