﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollElementCalculationEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollElementCalculationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGroupCode">
                Group Code
            </div>
            <div class="divtxtBox LNOrient" style="  width:350px">
                <asp:DropDownList ID="ddlGroupCode" runat="server" AutoPostBack="True" 
                    CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlGroupCode_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
          <%--  <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription" Enabled="true" TabIndex="2" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="  width: 350px">
                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" Width="150px"
                    CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
          <%--  <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription2">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription2" Enabled="true" TabIndex="4" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox LNOrient" style="  width: 350px">
                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" Width="150px"
                    CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
           <%-- <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription3">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription3" Enabled="true" TabIndex="6" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="50" OnTextChanged="txtDescription3_TextChanged"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblElement">
                Element
            </div>
            <div class="divtxtBox LNOrient" style="  width: 350px">
                <asp:DropDownList ID="ddlElement" runat="server" AutoPostBack="True" Width="150px"
                    CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlElement_SelectedIndexChanged"
                    TabIndex="7">
                </asp:DropDownList>
            </div>
        <%--    <div class="lblBox LNOrient" style="  width: 150px" id="lblDescription4">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription4" Enabled="true" TabIndex="8" CssClass="validate[]  txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
                Calculation Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlCalculationType" runat="server" Width="150px" CssClass="RequiredField EntryFont ddlStype"
                    TabIndex="7" AutoPostBack="True" 
                    onselectedindexchanged="ddlCalculationType_SelectedIndexChanged1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalculationType">
                Calculation Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlCalculationType" runat="server" Width="150px" CssClass="RequiredField EntryFont ddlStype"
                    OnSelectedIndexChanged="ddlCalculationType_SelectedIndexChanged" TabIndex="9">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div id="IDformula" runat="server">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                </div>
                <div class="divtxtBox LNOrient" style=" ">
                    <asp:Button ID="btn0" runat="server" Text="0" CssClass="btn" CommandArgument="0"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn1" runat="server" Text="1" CssClass="btn" CommandArgument="1"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn2" runat="server" Text="2" CssClass="btn" CommandArgument="2"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn3" runat="server" Text="3" CssClass="btn" CommandArgument="3"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn4" runat="server" Text="4" CssClass="btn" CommandArgument="4"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn5" runat="server" Text="5" CssClass="btn" CommandArgument="5"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn6" runat="server" Text="6" CssClass="btn" CommandArgument="6"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn7" runat="server" Text="7" CssClass="btn" CommandArgument="7"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn8" runat="server" Text="8" CssClass="btn" CommandArgument="8"
                        OnClick="btn_Click" />
                    <asp:Button ID="btn9" runat="server" Text="9" CssClass="btn" CommandArgument="9"
                        OnClick="btn_Click" />
                    <asp:Button ID="btndot" runat="server" Text="." CssClass="btn" CommandArgument="."
                        OnClick="btn_Click" />
                    <asp:Button ID="btnPlus" runat="server" Text="+" CssClass="btn" CommandArgument="+"
                        OnClick="btn_Click" />
                    <asp:Button ID="btnMinus" runat="server" Text="-" CssClass="btn" CommandArgument="-"
                        OnClick="btn_Click" />
                    <asp:Button ID="btnMul" runat="server" Text="*" CssClass="btn" CommandArgument="*"
                        OnClick="btn_Click" />
                    <asp:Button ID="btnDiv" runat="server" Text="/" CssClass="btn" CommandArgument="/"
                        OnClick="btn_Click" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
                </div>
                <div class="divtxtBox LNOrient" style="  width: 480px;">
                    <asp:DropDownList ID="ddlelment" runat="server" AutoPostBack="True" CssClass="RequiredField EntryFont ddlStype"
                        OnSelectedIndexChanged="ddlelment_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="lblFormula">
                    Formula
                </div>
                <div class="divtxtBox LNOrient" style="  width: 490px; height: 50px">
                    <asp:TextBox ID="txtFormula" TabIndex="10" Height="50px" TextMode="MultiLine" CssClass="validate[]  txtBox"
                        runat="server" MaxLength="200"></asp:TextBox>
                </div>
                <asp:Button ID="btncheck" runat="server" Text="Check" CssClass="btn" OnClick="btncheck_Click" Visible = "false" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="11" Width="145px"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtenderFromDate" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtFromDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
            <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px; height: 18px;">
                <asp:TextBox ID="txttodate" CssClass="validate[custom[ReqDateDDMMYYY],,dateRange[dg1]]  txtBox"
                    runat="server" TabIndex="12" Width="150px" OnTextChanged="txttodate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txttodate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 148px; display: none" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 148px; height: 18px; display: none">
                <asp:TextBox ID="txtAmount" TabIndex="13" CssClass="validate[]  txtBox" runat="server"
                    MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
