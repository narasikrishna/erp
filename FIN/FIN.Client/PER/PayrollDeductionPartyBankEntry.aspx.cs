﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.BLL.CA;
using FIN.BLL.PER;
using VMVServices.Web;
namespace FIN.Client.PER
{
    public partial class PayrollDeductionPartyBankEntry : PageBase
    {

        PAY_DED_PARTY_BANK_DTLS pAY_DED_PARTY_BANK_DTLS = new PAY_DED_PARTY_BANK_DTLS();
        DataTable dtGridData = new DataTable();
        PayrollDeductionPartyBank_BLL PayrollDeductionPartyBank_BLL = new PayrollDeductionPartyBank_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                     EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Pg_Load", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();


        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtDeductionPartyName.Enabled = false;
                FillComboBox();

                EntityData = null;
                

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<PAY_DED_PARTY_BANK_DTLS> userCtx = new DataRepository<PAY_DED_PARTY_BANK_DTLS>())
                    {
                        pAY_DED_PARTY_BANK_DTLS = userCtx.Find(r =>
                            (r.DED_PARTY_DTL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = pAY_DED_PARTY_BANK_DTLS;

                    ddldedpartyNo.SelectedValue = pAY_DED_PARTY_BANK_DTLS.DED_PARTY_CODE;

                    fillDedpartyName();

                }
                dtGridData = FIN.BLL.PER.PayrollDeductionPartyBank_BLL.getChildEntityDet(ddldedpartyNo.SelectedValue);
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PDPBD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            PayrollDeductionPartyBank_BLL.fn_getDeductionPartyName(ref ddldedpartyNo);

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pAY_DED_PARTY_BANK_DTLS = new PAY_DED_PARTY_BANK_DTLS();
                    if (dtGridData.Rows[iLoop]["DED_PARTY_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_DED_PARTY_BANK_DTLS> userCtx = new DataRepository<PAY_DED_PARTY_BANK_DTLS>())
                        {
                            pAY_DED_PARTY_BANK_DTLS = userCtx.Find(r =>
                                (r.DED_PARTY_DTL_ID == dtGridData.Rows[iLoop]["DED_PARTY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    pAY_DED_PARTY_BANK_DTLS.BANK_CODE = dtGridData.Rows[iLoop]["BANK_ID"].ToString();
                    pAY_DED_PARTY_BANK_DTLS.BRANCH_CODE = dtGridData.Rows[iLoop]["BANK_BRANCH_ID"].ToString();
                    pAY_DED_PARTY_BANK_DTLS.BANK_NAME = dtGridData.Rows[iLoop]["BANK_NAME"].ToString();
                    pAY_DED_PARTY_BANK_DTLS.BRANCH_NAME = dtGridData.Rows[iLoop]["BANK_BRANCH_NAME"].ToString();
                    pAY_DED_PARTY_BANK_DTLS.ACCOUNT_NUMBER = dtGridData.Rows[iLoop]["ACCOUNT_NUMBER"].ToString();
                    pAY_DED_PARTY_BANK_DTLS.DED_PARTY_CODE = ddldedpartyNo.SelectedValue;


                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        pAY_DED_PARTY_BANK_DTLS.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        pAY_DED_PARTY_BANK_DTLS.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        pAY_DED_PARTY_BANK_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_DED_PARTY_BANK_DTLS.ENABLED_FLAG = FINAppConstants.N;
                    }



                    
                    pAY_DED_PARTY_BANK_DTLS.DED_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(pAY_DED_PARTY_BANK_DTLS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["DED_PARTY_DTL_ID"].ToString() != "0")
                        {
                            pAY_DED_PARTY_BANK_DTLS.DED_PARTY_DTL_ID = dtGridData.Rows[iLoop]["DED_PARTY_DTL_ID"].ToString();
                            pAY_DED_PARTY_BANK_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_DED_PARTY_BANK_DTLS.DED_PARTY_DTL_ID);
                            pAY_DED_PARTY_BANK_DTLS.MODIFIED_BY = this.LoggedUserName;
                            pAY_DED_PARTY_BANK_DTLS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_DED_PARTY_BANK_DTLS>(pAY_DED_PARTY_BANK_DTLS, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            pAY_DED_PARTY_BANK_DTLS.DED_PARTY_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.PER_009.ToString(), false, true);
                            pAY_DED_PARTY_BANK_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_DED_PARTY_BANK_DTLS.DED_PARTY_DTL_ID);                            
                            pAY_DED_PARTY_BANK_DTLS.CREATED_BY = this.LoggedUserName;
                            pAY_DED_PARTY_BANK_DTLS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_DED_PARTY_BANK_DTLS>(pAY_DED_PARTY_BANK_DTLS);
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }

                       

                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PDPBD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlBankName = tmpgvr.FindControl("ddlBankName") as DropDownList;
                
                Bank_BLL.fn_getBankName(ref ddlBankName);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlBankName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.BANK_ID].ToString();

                    DropDownList ddlBranchName = tmpgvr.FindControl("ddlBranchName") as DropDownList;


                    BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);
                        ddlBranchName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.BANK_BRANCH_ID].ToString();


                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Deduction Party Bank Details");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            TextBox txtAccountNumber = gvr.FindControl("txtAccountNumber") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkActive = gvr.FindControl("chkActive") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["DED_PARTY_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlBankName;
            slControls[1] = ddlBranchName;
            slControls[2] = txtAccountNumber;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpStartDate;
            slControls[5] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Account_Number_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Bank Name ~ Branch Name ~ Account  Number ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            if (ddlBankName.SelectedItem != null)
            {
                drList[FINColumnConstants.BANK_ID] = ddlBankName.SelectedItem.Value;
                drList[FINColumnConstants.BANK_NAME] = ddlBankName.SelectedItem.Text;
            }


            if (ddlBankName.SelectedItem != null)
            {
                drList[FINColumnConstants.BANK_BRANCH_ID] = ddlBranchName.SelectedItem.Value;
                drList[FINColumnConstants.BANK_BRANCH_NAME] = ddlBranchName.SelectedItem.Text;
            }

            drList[FINColumnConstants.ACCOUNT_NUMBER] = txtAccountNumber.Text;

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }
            if (chkActive.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_DED_PARTY_BANK_DTLS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<PAY_DED_PARTY_BANK_DTLS>(pAY_DED_PARTY_BANK_DTLS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




       

        private void fillDedpartyName()
        {
            DataTable dtDedpartyName = new DataTable();
            dtDedpartyName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDeductionPartyBank_DAL.GetDedpartyName(ddldedpartyNo.SelectedValue)).Tables[0];
            if (dtDedpartyName != null)
            {
                if (dtDedpartyName.Rows.Count > 0)
                {
                    txtDeductionPartyName.Text = dtDedpartyName.Rows[0]["DED_PARTY_NAME"].ToString();
                }
            }
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;


            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);
           

        }

        protected void ddldedpartyNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDedpartyName();

            dtGridData = FIN.BLL.PER.PayrollDeductionPartyBank_BLL.getChildEntityDet(ddldedpartyNo.SelectedValue);
            BindGrid(dtGridData);

        }

        protected void txtDeductionPartyName_TextChanged(object sender, EventArgs e)
        {

        }


    }
}