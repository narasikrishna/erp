﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.PER;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollLeaveSalary : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divPayPaid.Visible = false;
                divPayRemarks.Visible = false;
                divPayPeriod.Visible = true;

                FillComboBox();
            }   
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDeptName);
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayPeriod);

            if (ddlEmpName.SelectedIndex >= 0)
            {
                ddlEmpName.SelectedIndex = 0;
            }
            if (ddlPayPeriod.SelectedIndex >= 0)
            {
                ddlPayPeriod.SelectedIndex = 0;
            }
        }

        protected void ddlDeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmpName, ddlDeptName.SelectedValue.ToString());
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();

            if (txtStartDate.Text.ToString() != null && txtStartDate.Text.ToString().Length > 0)
                FromDate = Convert.ToDateTime(txtStartDate.Text.ToString());
            if (txtEndDate.Text.ToString() != null && txtEndDate.Text.ToString().Length > 0)
                ToDate = Convert.ToDateTime(txtEndDate.Text.ToString());

            decimal payrollAmount = 0;
            payrollAmount = FINSP.GetPAY_EMP_LEAVE_SALARY(" ", ddlDeptName.SelectedValue.ToString(), ddlEmpName.SelectedValue.ToString(), VMVServices.Web.Utils.OrganizationID.ToString(), FromDate, ToDate);
            txtPayrollAmtToBePaid.Text = payrollAmount.ToString();
            txtPayrollAmtToBePaid.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPayrollAmtToBePaid.Text);
        }

        protected void rbPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbPayType.SelectedValue == "1")
            {
                divPayPeriod.Visible = false;
                divPayPaid.Visible = true;
                divPayRemarks.Visible = true;

                txtPayAmtPaid.Text = txtPayrollAmtToBePaid.Text.ToString();
                txtPayAmtPaid.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPayAmtPaid.Text);
                txtPayrollDate.Text = DateTime.Now.ToShortDateString();
            }
            else
            {
                divPayPaid.Visible = false;
                divPayRemarks.Visible = false;
                
                divPayPeriod.Visible = true;
                txtPayAmtPaid.Text = "";
            }
        }
        
    }
}