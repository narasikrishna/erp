﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;


using System.Collections;

using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace FIN.Client.PER
{
    public partial class PayrollViewEntry : PageBase
    {

        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        ReportDocument rdReport = new ReportDocument();
        //  ReportBLL reportBLL = new ReportBLL();
        ReportDocument cRDoc = new ReportDocument();
        ParameterFields pfields = new ParameterFields();
        DataTable dtData = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Startup();
                FillComboBox();
            }

        }
        private void FillComboBox()
        {
            // FIN.BLL.PER.PayrollTrailrunDetails_BLL.fn_getFinalPayrollId(ref ddlPayrollPeriod);
            PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayPeriod);
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            DataTable dtGridData;

            try
            {
                if (ddlPayrollPeriod.SelectedValue.ToString().Length == 0)
                {
                    dtGridData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetPayFinalRunDtl4PayrollPeriod(ddlPayPeriod.SelectedValue)).Tables[0];

                }
                else
                {
                    dtGridData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetPayFinalRunDtl(ddlPayrollPeriod.SelectedValue)).Tables[0];
                    // dtGridData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetPayFinalRunDtl4PayrollPeriod(ddlPayPeriod.SelectedValue)).Tables[0];
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("processclick", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            if (dtData.Rows.Count > 0)
            {
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_net_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_net_amount"))));
                dtData.AcceptChanges();
            }


            gvData.DataSource = dtData;
            gvData.DataBind();
            gvEmpDetails.Visible = false;
        }

        protected void ddlPayrollPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void btnDetails_Click(object sender, EventArgs e)
        {
            //gvEmpDetails.Visible = true;
            try
            {
                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
                DataTable dtEmployeePay = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.getPayFinalrunEMP4Dept(gvData.DataKeys[gvrP.RowIndex].Values["PAYROLL_DTL_ID"].ToString())).Tables[0];

                if (dtEmployeePay.Rows.Count > 0)
                {
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_net_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_net_amount"))));
                    dtEmployeePay.AcceptChanges();
                }


                gvEmpDetails.DataSource = dtEmployeePay;
                gvEmpDetails.DataBind();
                gvEmpDetails.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnEmpDetails_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtgetdtlEmp = new DataTable();
                ModalPopupExtender2.Show();

                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
                dtgetdtlEmp = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollTrailrunDetails_DAL.GetDtlEmp(gvEmpDetails.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString())).Tables[0];
                if (dtgetdtlEmp.Rows.Count > 0)
                {
                    lblEmp.Text = dtgetdtlEmp.Rows[0]["EMP_NAME"].ToString();
                    lbldept.Text = dtgetdtlEmp.Rows[0]["DEPT_NAME"].ToString();
                    lblDesig.Text = dtgetdtlEmp.Rows[0]["DESIG_NAME"].ToString();
                    lblDoj.Text = DBMethod.ConvertDateToString(dtgetdtlEmp.Rows[0]["EMP_DOJ"].ToString());
                }
                DataTable dtEmployeePay = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetPayFinalRunDtlDtl(gvEmpDetails.DataKeys[gvrP.RowIndex].Values["PAYROLL_DTL_ID"].ToString(), gvEmpDetails.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString())).Tables[0];

                BindGrid_EmployeePayData(dtEmployeePay);
                BindGrid_EmployeePayData2(dtEmployeePay);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        //private void BindGrid_EmployeePayData(DataTable dtEmployeePayData)
        //{

        //    gvEmployeePay.DataSource = dtEmployeePayData;
        //    gvEmployeePay.DataBind();
        //}

        private void BindGrid_EmployeePayData(DataTable dtEmployeePayData)
        {
            bol_rowVisiable = false;
            // Session["dtEmployeePayData"] = dtEmployeePayData;

            if (dtEmployeePayData.Rows.Count > 0)
            {
                dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay.DataSource = dtEmployeePayData;
            gvEmployeePay.DataBind();
            lblTotalEar.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData, "pay_amount", " pay_element_Type='Earning'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();


        }

        protected void gvEmployeePay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Deduction")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindGrid_EmployeePayData2(DataTable dtEmployeePayData2)
        {
            bol_rowVisiable = false;
            //   Session["dtEmployeePayData2"] = dtEmployeePayData2;

            if (dtEmployeePayData2.Rows.Count > 0)
            {
                dtEmployeePayData2.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData2.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData2.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay2.DataSource = dtEmployeePayData2;
            gvEmployeePay2.DataBind();
            lblTotDed.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData2, "pay_amount", " pay_element_Type='Deduction'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();


        }
        protected void gvEmployeePay2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Earning")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlPayPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillPayrollId();
        }
        private void FillPayrollId()
        {
            FIN.BLL.PER.PayrollTrailrunDetails_BLL.fn_getApprovedPayrollId4PayrollPeriod(ref ddlPayrollPeriod, ddlPayPeriod.SelectedValue, false);
        }

        protected void btnEmail_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();

                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();
                string empId = string.Empty;
                string MailId = string.Empty;
                DataTable dtEmpId = new DataTable();



                //  ReportFile = Server.MapPath("~/HR_Reports/RPTPayslipReport.rpt");
                Session["photoUrl"] = null;

                if (ddlPayPeriod.SelectedValue.ToString().Length > 0)
                {

                    dtEmpId = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetEmpBasedPayrollId(ddlPayPeriod.SelectedValue)).Tables[0];
                    if (dtEmpId.Rows.Count > 0)
                    {
                        // Array.ForEach(Directory.GetFiles(Server.MapPath("~/TmpWordDocs/")), File.Delete);

                        for (int i = 0; i < dtEmpId.Rows.Count; i++)
                        {
                            DataTable dt = new DataTable();
                            decimal dtAmt = 0;
                            dt = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.getEmpTot(dtEmpId.Rows[i]["emp_id"].ToString())).Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                dtAmt = CommonUtils.ConvertStringToDecimal(dt.Rows[0]["tot_amt"].ToString());
                            }
                            if (dtAmt > 0)
                            {
                                htFilterParameter = new Hashtable();
                                htFilterParameter.Add(FINColumnConstants.PAY_PERIOD_ID, ddlPayPeriod.SelectedValue);
                                htFilterParameter.Add(FINColumnConstants.EMP_ID, dtEmpId.Rows[i]["emp_id"].ToString());
                                //SendEmail(dtEmpId.Rows[i]["emp_id"].ToString(), dtEmpId.Rows[i]["emp_email"].ToString());
                                MailId = dtEmpId.Rows[i]["emp_email"].ToString();
                                empId = dtEmpId.Rows[i]["emp_id"].ToString();
                                if (empId != string.Empty && MailId != string.Empty)
                                {
                                    string photoid = "";
                                    string photoUrl = string.Empty;

                                    DataTable dtphoto = new DataTable();

                                    photoid = empId;
                                    dtphoto = DBMethod.ExecuteQuery(FIN.DAL.HR.Applicant_DAL.GetPhotoFileNameBasedEmpId(photoid.ToString())).Tables[0];

                                    if (dtphoto.Rows.Count > 0)
                                    {
                                        photoUrl = "~/UploadFile/PHOTO/" + empId + "." + dtphoto.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "");
                                    }
                                    Session["photoUrl"] = photoUrl;


                                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                                    ReportData = FIN.BLL.HR.Payslip_BLL.GetEmployeeDtlsReportData();
                                    SubReportData = FIN.BLL.HR.Payslip_BLL.GetEarningReportData();
                                    ReportFormulaParameter = htHeadingParameters;
                                    //  MailId = System.Configuration.ConfigurationManager.AppSettings["EmaiId"].ToString();
                                    //  Response.Redirect("../RPTEmailCrystalReportViewer.aspx?EmailRpt=Payslip&MailId=" + MailId, false);
                                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.onload('../RPTEmailCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "&EmailRpt=Payslip&MailId=" + MailId + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

                                    CallReportViewer(MailId);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }
                        }

                        ErrorCollection.Add("MAILSENT", "Email has been send successfully");

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Clear();
                ErrorCollection.Add("PayslipReportemail", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }



        private void CallReportViewer(string MailId)
        {
            ErrorCollection.Clear();
            try
            {
                string reportUrl = string.Empty, strURL = string.Empty;
                DataSet dsReport = new DataSet();
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                bool isCrossTabReport = false;
                bool isChartObject = false;


                htHeadingParameters.Remove("UserName");
                htHeadingParameters.Remove("OrgName");
                htHeadingParameters.Remove("OrgAddress1");
                htHeadingParameters.Remove("OrgAddress2");
                htHeadingParameters.Remove("OrgAddress3");
                htHeadingParameters.Remove("OrgPhone");
                htHeadingParameters.Remove("OrgMobile");
                htHeadingParameters.Remove("OrgFax");
                htHeadingParameters.Remove("ReportName");
                htHeadingParameters.Remove("RowBackGround");
                htHeadingParameters.Remove("AlternateRowBackGround");


                htHeadingParameters.Add("UserName", VMVServices.Web.Utils.UserName.ToString());
                htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName.ToString());
                htHeadingParameters.Add("RowBackGround", "255");

                htHeadingParameters.Add("AlternateRowBackGround", "255");



                if (Session["ProgramID"].ToString().Length > 0)
                {
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());

                    if (dr_list.Length > 0)
                    {
                        if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE3"].ToString());
                        }
                        else
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE2"].ToString());
                        }
                    }
                }


                //  DataTable dt_data = FIN.DAL.DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrganisationData(VMVServices.Web.Utils.OrganizationID)).Tables[0];
                //if (dt_data.Rows.Count == 1)
                //{
                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg"))
                {
                    VMVServices.Web.Utils.OrgLogo = System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg";
                }
                //dt_data.Rows[0]["comp_logo"].ToString();
                //}

                htHeadingParameters.Add("OrgLogo", VMVServices.Web.Utils.OrgLogo.ToString());

                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg"))
                {
                    htHeadingParameters.Add("OrgGOVTLogo", System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg");
                }

                // htHeadingParameters.Add("OrgEmail", RPCServices.Web.Utils.Email.ToString());

                if (Session["photoUrl"] != null)
                {
                    htHeadingParameters.Add("EmpFoto", Session["photoUrl"]);
                }

                ReportFormulaParameter = htHeadingParameters;

                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    if (ReportFile.Contains("_OL"))
                    {
                        ReportFile = ReportFile.ToString().ToUpper().Replace("_OL", "");
                    }

                    ReportFile = ReportFile.ToString().ToUpper().Replace(".RPT", "_OL.rpt");

                }


                rdReport.Load(Server.MapPath("~/HR_Reports/RPTPayslipReport.rpt"));
                this.ApplyLogonToTables(rdReport);


                rdReport.SetDatabaseLogon(System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString());


                if (ReportParameter == null || ReportParameter.Count == 0)
                {
                    dsReport = ReportData;
                    if (dsReport != null)
                    {
                        if (dsReport.Tables.Count > 0)
                        {
                            for (int i = 0; i < dsReport.Tables.Count; i++)
                            {
                                rdReport.SetDataSource(dsReport.Tables[i]);
                            }
                        }
                    }
                    int k = 0;

                    if (SubReportData != null && SubReportData.Tables.Count > 0)
                    {
                        dsReport = SubReportData;

                        CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

                        foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
                        {
                            subReport.SetDataSource(dsReport.Tables[k]);
                            if (dsReport.Tables.Count > 1)
                            {
                                k = k + 1;
                            }
                        }
                    }
                }

                else if (ReportParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        rdReport.SetParameterValue(hsEnum.Key.ToString(), hsEnum.Value);
                        //rdReport.DataDefinition.ParameterFields[hsEnum.Key.ToString()].

                    }
                }

                if (Session["HeadingParameters"] != null)
                {
                    ReportFormulaParameter = (Hashtable)Session["HeadingParameters"];
                }

                if (ReportFormulaParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportFormulaParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex1)
                        {
                        }
                    }
                }



                if (VMVServices.Web.Utils.ReportFilterParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = VMVServices.Web.Utils.ReportFilterParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                isCrossTabReport = false;
                isChartObject = false;
                for (int iLoop = 0; iLoop < rdReport.ReportDefinition.ReportObjects.Count; iLoop++)
                {
                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.CrossTabObject)
                    {
                        isCrossTabReport = true;
                    }


                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.ChartObject)
                    {
                        isChartObject = true;
                    }
                }
                isCrossTabReport = true;
                isChartObject = true;
                rdReport.Refresh();
                rdReport.SetCssClass(ObjectScope.AllReportObjectsInPageFooterSections, "reportFooterTitle");
                CrystalReportViewer1.HasCrystalLogo = false;
                CrystalReportViewer1.ReportSource = rdReport;

                //if (Request.QueryString["EmailRpt"] != null && Request.QueryString["MailId"] != null)
                //{
                //    if (Request.QueryString["MailId"].ToString() != string.Empty && Request.QueryString["EmailRpt"].ToString() == "Payslip")
                //    {

                FIN.DAL.SSM.Alerts_DAL.InsertProcessLog("Mail Id", MailId, "0");
                if (MailId != string.Empty)
                {

                    rdReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TmpWordDocs/Payslip.pdf"));
                    rdReport.Close();
                    string fileName = Server.MapPath("~/TmpWordDocs/Payslip.pdf");

                    string str_Host = System.Configuration.ConfigurationManager.AppSettings["MAILHOST"].ToString();
                    int int_port = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MAILPORT"].ToString());

                    CommonUtils.GenerateEmailAttachment(MailId.ToString(), "Payslip " + ddlPayPeriod.SelectedItem.Text + ". <br /> This email has been generated by the system.", "", fileName, str_Host, int_port, "Payslip " + ddlPayPeriod.SelectedItem.Text);
                }
                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Clear();
                ErrorCollection.Add("emrunilaerror", ex.Message);
                if (ex.InnerException != null)
                {
                    ErrorCollection.Add("emrunilaerrorIE", ex.InnerException);
                }
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    System.Collections.IDictionaryEnumerator myEnumerator = ErrorCollection.GetEnumerator();
                    while (myEnumerator.MoveNext())
                    {
                        FIN.DAL.SSM.Alerts_DAL.InsertProcessLog(myEnumerator.Value.ToString(), MailId, "0");
                    }
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void ApplyLogonToTables(ReportDocument rdReport)
        {

            TableLogOnInfo ConInfo = new TableLogOnInfo();
            Tables crTables = rdReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTables)
            {
                {
                    ConInfo.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                    ConInfo.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                    ConInfo.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();
                }
                crTable.ApplyLogOnInfo(ConInfo);
            }

            CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

            foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
            {
                TableLogOnInfo ConInfoSub = new TableLogOnInfo();
                Tables crTablesSub = subReport.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTablesSub)
                {
                    {
                        ConInfoSub.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                        ConInfoSub.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                        ConInfoSub.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();



                    }
                    crTable.ApplyLogOnInfo(ConInfoSub);
                }

            }

            rdReport.Refresh();
        }


    }
}