﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLBankBalance_BI : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtData = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              //  Session["BankBal"] = null;
                Startup();
              //  GenerateBankbalanceChart();
              //  LoadGraph();
            }
            else
            {
               // LoadGraph();
            }
        }

        private void LoadGraph()
        {
            if (Session["BankBal"] != null)
            {
                chartBankBal.DataSource = (DataTable)Session["BankBal"];
                chartBankBal.DataBind();
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getGlBankBalance());

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void GenerateBankbalanceChart()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();

                dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getGlBankBalance()).Tables[0];

                Session["BankBal"] = dtData;
                chartBankBal.DataSource = dtData;
                chartBankBal.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BankBalanceChart", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}