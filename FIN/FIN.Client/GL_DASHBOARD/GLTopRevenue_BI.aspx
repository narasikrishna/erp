﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLTopRevenue_BI.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLTopRevenue_BI" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 120px" id="lblDate">
                    Year
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:DropDownList ID="ddl_R_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                        OnSelectedIndexChanged="ddl_R_Period_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 120px" id="Div2">
                    Month
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:DropDownList ID="ddl_R_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                        OnSelectedIndexChanged="ddl_R_Year_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 290px" id="div1">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dividRevenue" style="width: 100%">
        <div id="dividRevChrt" style="width: 100%; display: none;">
            <asp:Chart ID="chartRevenue" runat="server" Width="500px" Height="310px">
                <Series>
                    <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="false"
                        XValueMember="group_name" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
