﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLTransaction_DB.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLTransaction_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px;" id="divHeader" runat="server">
        <div style="float: left; padding-left: 10px; padding-top: 2px" >
            GL Transaction
        </div>
        <div id="divPrintIcon" runat="server" style=" float: right; padding-right: 10px; padding-top: 2px">
            <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" onclick="btnGraphRep_Click"  />
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_GL_Trans_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_GL_Trans_Period_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddl_GL_Trans_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_GL_Trans_Year_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divempAgeGraph">
        <asp:Chart ID="chrt_GLtrans" runat="server" Height="310px" Width="500px" EnableViewState="true">
            <Series>
                <asp:Series Name="S_Dept_Count" ChartType="Pie" XValueMember="SOURCE_CODE" YValueMembers="TOT_AMT"
                    IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                    Legend="Legend1" LegendText="#VALX ( #VAL )">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                </asp:ChartArea>
            </ChartAreas>
            <Legends>
                <asp:Legend Docking="Bottom" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                </asp:Legend>
            </Legends>
        </asp:Chart>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"  ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="SOURCE_CODE" HeaderText="Source"></asp:BoundField>
                <asp:BoundField DataField="TOT_AMT" HeaderText="Total Record">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
