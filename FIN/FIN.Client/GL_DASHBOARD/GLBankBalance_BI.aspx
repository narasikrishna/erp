﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLBankBalance_BI.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLBankBalance_BI" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divFormcontainer" style="width: 290px" id="diDv1">
            <div class="divRowContainer divReportAction">
                <div>
                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                        Width="35px" Height="25px" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
    <div id="divIdBBchrt" style="width: 98%; display: none;">
        <asp:Chart ID="chartBankBal" runat="server" Width="500px" Height="310px">
            <Series>
                <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="True"
                    XValueMember="BANK_NAME" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
