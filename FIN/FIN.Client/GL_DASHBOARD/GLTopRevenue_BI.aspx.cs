﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLTopRevenue_BI : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Startup();
                FillComboBox();
                GenerateRevenueChart();
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_R_Year);
            if (ddl_R_Year.Items.Count > 0)
            {
                ddl_R_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_R_Period, ddl_R_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_R_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddl_R_Period.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("PERIOD_MONTH", ddl_R_Period.SelectedItem.Text.ToString());
                }

                if (ddl_R_Year.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("PERIOD_YEAR", ddl_R_Year.SelectedItem.Text.ToString());
                }
            

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetExpensesBalance4Report());
               
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void GenerateRevenueChart()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();

                dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetIncomeBalance(ddl_R_Period.SelectedValue)).Tables[0];

                Session["RevenueBal"] = dtData;
                chartRevenue.DataSource = dtData;
                chartRevenue.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TopRevenueDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddl_R_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_R_Period, ddl_R_Year.SelectedValue);
        }

        protected void ddl_R_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateRevenueChart();
        }
       
    }
}