﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;

namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLTrialBalance_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboBox();
                GenerateTBChart();
            }
        }

        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_TBFinYear);
            if (ddl_TBFinYear.Items.Count > 0)
            {
                ddl_TBFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_TBFinPeriod, ddl_TBFinYear.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_TBFinPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        protected void ddl_TBFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_TBFinPeriod, ddl_TBFinYear.SelectedValue);
        }

        protected void ddl_TBFinPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateTBChart();
        }

        private void GenerateTBChart()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dsTB = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getTrailBalance4Period(ddl_TBFinPeriod.SelectedValue)).Tables[0];

                Session["TB"] = dsTB;

                if (dsTB.Rows.Count > 0)
                {
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OP_BAL", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OP_BAL"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANS_DR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANS_DR"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANS_CR", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANS_CR"))));
                    dsTB.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOS_BAL", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CLOS_BAL"))));
                    dsTB.AcceptChanges();
                }

                gvTrailBal.DataSource = dsTB;
                gvTrailBal.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalaaanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}