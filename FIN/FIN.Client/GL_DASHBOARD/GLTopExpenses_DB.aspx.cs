﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using VMVServices.Services.Data;

using System.Collections;
namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLTopExpenses_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["ExpensesBal"] = null;
                AssignToControl();
                GenerateExpensesChart();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }


        private void FillComboBox()
        {


            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));

            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];


            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_E_Year);
            if (ddl_E_Year.Items.Count > 0)
            {
                //ddl_TBFinYear.SelectedIndex = ddl_TBFinYear.Items.Count - 1;
                ddl_E_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_E_Period, ddl_E_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_E_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }


        }
        private void LoadGraph()
        {
            if (Session["ExpensesBal"] != null)
            {
                chartExpenses.DataSource = (DataTable)Session["ExpensesBal"];
                chartExpenses.DataBind();


                Random random = new Random();
                foreach (var item in chartExpenses.Series[0].Points)
                {
                    System.Drawing.Color c = System.Drawing.Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    item.Color = c;
                }

                gvGraphdata.DataSource = (DataTable)Session["ExpensesBal"];
                gvGraphdata.DataBind();
            }
        }

        protected void ddl_E_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_E_Period, ddl_E_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_E_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateExpensesChart();
            LoadGraph();
        }

        private void GenerateExpensesChart()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetExpensesBalance(ddl_E_Period.SelectedValue)).Tables[0];

                Session["ExpensesBal"] = dtData;
                chartExpenses.DataSource = dtData;
                chartExpenses.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GenerateExpensesChart", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();



                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                htFilterParameter.Add("FF_YEAR", ddl_E_Year.SelectedItem.Text);
                htFilterParameter.Add("FF_MONTH", ddl_E_Period.SelectedItem.Text);

                htFilterParameter.Add("PERIOD", ddl_E_Period.SelectedValue.ToString());

                chartExpenses.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetExpensesBalance(ddl_E_Period.SelectedValue));

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}