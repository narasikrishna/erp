<%@ Page Language="C#" AutoEventWireup="true" Inherits="Aon.Client.ExportGridData" Codebehind="ExportGridData.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iAcademe</title>
     <style>
    REPORTBODY
{
	padding-right: 0px;
	padding-left: 0px;
	font-size: 11px;
	background: url(Images/bodyBg.gif) #4396CA repeat-x left top;
	padding-bottom: 0px;
	margin: 0px;
	color: #ffffff;
	padding-top: 0px;
	font-family: Arial, Helvetica, sans-serif;
	border: 0px;
	font-weight: normal;	
}
.trValue
{
	font-weight: normal;
	font-size: 12px;
	height: 20px;
	font-family: arial;
}
.trValue A
{
	color: #333333;
	height: 20px;
	text-decoration: none;
}
</style>
</head>

<body class="REPORTBODY">
    <form id="form1" runat="server">
        <table class="trValue">
             
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:BulletedList ID="blErrorMessage" ForeColor="RED" BulletStyle="Disc" runat="server">
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="Center">
                   <a href="javascript:window.close();"><span class="trValue">Close</span></a></td>
            </tr>
        </table>
    </form>
</body>
</html>
