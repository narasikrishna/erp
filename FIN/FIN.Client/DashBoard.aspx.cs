﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client
{
    public partial class DashBoard : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["str_LinkWFQuery"] != null)
                {
                    Response.Redirect(Session["str_LinkWFQuery"].ToString());
                }
              
                else
                {
                    Response.Redirect("~/" + Session[FINSessionConstants.ModuleName].ToString() + "_DASHBOARD/DashBoard_" + Session[FINSessionConstants.ModuleName].ToString() + ".aspx");
                }
            }
        }

    }
}