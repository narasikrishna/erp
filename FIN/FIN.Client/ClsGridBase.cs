﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;

using FIN.DAL;
using FIN.BLL;

using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Office.Interop.Word;
using Novacode;
using System.Diagnostics;
using System.Net;
using System.IO;
namespace FIN.Client
{
    public class ClsGridBase
    {
        #region "Setup Grid "

        private static System.Data.DataTable _dataMenuList;
        public static System.Data.DataTable DataMenuList
        {
            get
            {
                return _dataMenuList;
            }
            set
            {
                _dataMenuList = value;
            }
        }


        private static System.Data.DataTable BuildColumnList(DataColumnCollection dcColumns, string scrnCode = "")
        {
            string str = string.Empty;
            //for (int I = 0; I <= dcColumns.Count - 1; I++)
            //{
            //    if (!string.IsNullOrEmpty(str))
            //    {
            //        str += " OR " + "(upper(LIST_COLUMN_NAME) ='" + dcColumns[I].ColumnName.ToUpper() + "')";
            //        str += " OR (upper(LIST_COLUMN_NAME) like '%" + dcColumns[I].ColumnName.ToUpper() + "')";
            //    }
            //    else
            //    {
            //        str += "(upper(LIST_COLUMN_NAME) like '%" + dcColumns[I].ColumnName.ToUpper() + "')";
            //    }
            //}


            string cmdText = " SELECT LIST_DTL_ID,LIST_COLUMN_NAME, " + " DISPLAY_COLUMN_NM,COLUMN_WIDTH,COLUMN_ALIGNMENT,COLUMN_DISPLAY_FORMAT, DISPLAY_COLUMN_NM_OL " + " FROM   ssm_list_dtl ";
            if (scrnCode.Length > 0)
            {
                cmdText += " ,ssm_list_hdr ";
            }

            //if (!string.IsNullOrEmpty(str))
            //    cmdText += " Where ( " + str + " )";


            if (scrnCode.Length > 0)
            {
                cmdText += " where ssm_list_dtl.list_hdr_id=SSM_LIST_HDR.list_hdr_id and ssm_list_dtl.DISPLAY_COLUMN_NM is not null and LIST_DISPLAY=1";
                cmdText += " AND upper(SSM_LIST_HDR.List_Screen_Code)='" + scrnCode.ToUpper() + "'";
            }

            cmdText += "  Order by LIST_COLUMN_NAME ";

            // return ClsGlobal.HelpExecuteNonQueryDt(cmdText);
            return DBMethod.ExecuteQuery(cmdText).Tables[0];
        }

        private static System.Data.DataTable AddColumnsToCustomGrid()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add(GridColumns.LIST_DTL_ID.ToString(), Type.GetType("System.String"));
            dt.Columns.Add(GridColumns.LIST_COLUMN_NAME.ToString(), Type.GetType("System.String"));
            dt.Columns.Add(GridColumns.DISPLAY_COLUMN_NM.ToString(), Type.GetType("System.String"));
            dt.Columns.Add(GridColumns.COLUMN_VISIBLE.ToString(), Type.GetType("System.Int32"));
            return dt;
        }






        public static string BuildMenunew(int programID, string addURL, string searchURL)
        {
            string str_menu = string.Empty;
            try
            {

                bool isAddApplicable = false;
                bool isExortToPdfApplicable = true;
                bool isExortToExcelApplicable = true;
                bool isExortToCSVApplicable = true;
                bool isExortToTxtApplicable = true;

                DataSet ds_MenuList = (DataSet)HttpContext.Current.Session[FINSessionConstants.MenuData];
                DataRow[] dr_MenuDet = ds_MenuList.Tables[0].Select("SUB_PROCESS_ID=" + programID);

                if (dr_MenuDet[0]["ADD_FLAG"].ToString() == "Y")
                {
                    isAddApplicable = true;
                }
                System.Text.StringBuilder str_MenuDesign = new System.Text.StringBuilder();
                str_MenuDesign.Append("<ul id='Lnavigation-1'>");
                if (isAddApplicable)
                {
                    str_MenuDesign.Append("<li><a href='" + addURL + "' title='New'>New</a></li>");
                }

                str_MenuDesign.Append("<li><a href='#' title='Search' ><div id='div_search'>Search</div></a></li>");
                str_MenuDesign.Append("<li><a href='#' title='Export' >Export</a>");
                str_MenuDesign.Append("<ul class='Lnavigation-2'>");
                if (isExortToPdfApplicable)
                    str_MenuDesign.Append("<li><a href='#' title='Pdf'>PDF</a></li>");
                if (isExortToExcelApplicable)
                    str_MenuDesign.Append("<li><a href='#' title='Excel'>EXCEL</a></li>");
                if (isExortToCSVApplicable)
                    str_MenuDesign.Append("<li><a href='#' title='CSV'>CSV</a></li>");
                if (isExortToTxtApplicable)
                    str_MenuDesign.Append("<li><a href='#' title='TXT'>TXT</a></li>");
                str_MenuDesign.Append("</ul>");
                str_MenuDesign.Append("</li>");
                str_MenuDesign.Append("<li><a href='#' title='Help'>HELP</a></li>");
                str_MenuDesign.Append("</ul>");
                str_menu = str_MenuDesign.ToString();
            }
            catch (Exception ex)
            {
            }
            return str_menu;
        }


        public static GridView SetupGridnew(GridView grid, System.Data.DataTable dt, string keyFieldName, int int_ProgramID, string scrnCode = "")
        {
            try
            {
                int colIndex = 0;


                if (scrnCode == "GL_015")
                {
                    if (dt.Columns["JE_TOT_DR_AMOUNT"] != null)
                    {
                        dt.AsEnumerable().ToList().ForEach(p => p.SetField<String>("JE_TOT_DR_AMOUNT", FIN.DAL.DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("JE_TOT_DR_AMOUNT"))));
                    }
                }
                else if (scrnCode == "HR_097")
                {
                    if (dt.Columns["Amount"] != null)
                    {
                        dt.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Amount", FIN.DAL.DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Amount"))));
                    }
                }

                bool isEditAllowed = false;
                bool isDeleteAllowed = false;

                System.Data.DataTable ds_MenuList = (System.Data.DataTable)HttpContext.Current.Session[FINSessionConstants.MenuData];
                DataRow[] dr_MenuDet = ds_MenuList.Select("MENU_KEY_ID=" + int_ProgramID);

                if (dr_MenuDet[0]["UPDATE_ALLOWED_FLAG"].ToString() == FINAppConstants.Y)
                {
                    isEditAllowed = true;
                }
                if (dr_MenuDet[0]["DELETE_ALLOWED_FLAG"].ToString() == FINAppConstants.Y)
                {
                    isDeleteAllowed = true;
                }
                //BuildColumnList & Retrieve visible columns
                System.Data.DataTable dtColumns = BuildColumnList(dt.Columns, scrnCode);

                System.Data.DataTable dtCustom = AddColumnsToCustomGrid();

                //Add Edit, Delete Columns first
                //if (isEditAllowed)
                //{

                //    TemplateField tfObject = new TemplateField();
                //    tfObject.HeaderText = "Edit";
                //    tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                //    tfObject.ItemStyle.Width = 20;
                //    tfObject.ItemTemplate = new GridViewTemplate(ListItemType.Item, "MODIFYURL", "Edit");
                //    grid.Columns.Add(tfObject);


                //}

                //if (isDeleteAllowed)
                //{

                //    TemplateField tfObject = new TemplateField();
                //    tfObject.HeaderText = "Delete";
                //    tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                //    tfObject.ItemStyle.Width = 20;
                //    tfObject.ItemTemplate = new GridViewTemplate(ListItemType.Item, "DELETEURL", "Delete");
                //    grid.Columns.Add(tfObject);


                //}

                string str_selCol = "";
                for (int c = 0; c <= dt.Columns.Count - 1; c++)
                {
                    DataRow[] aRows = dtColumns.Select("" + GridColumns.LIST_COLUMN_NAME.ToString() + "='" + dt.Columns[c].ColumnName + "'");
                    str_selCol = GridColumns.LIST_COLUMN_NAME.ToString();
                    if (aRows.Length == 0)
                    {
                        aRows = dtColumns.Select("" + GridColumns.DISPLAY_COLUMN_NM.ToString() + "='" + dt.Columns[c].ColumnName + "'");
                        str_selCol = GridColumns.DISPLAY_COLUMN_NM.ToString();
                    }
                    if (aRows.Length > 0)
                    {

                        TemplateField tfObject = new TemplateField();
                        string str_Col_Name = "";
                        if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                        {
                            str_Col_Name = aRows[0]["DISPLAY_COLUMN_NM_OL"].ToString();
                        }
                        else
                        {
                            str_Col_Name = aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()].ToString();
                        }

                        tfObject.HeaderText = str_Col_Name;
                        tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        int width = int.Parse(aRows[0][GridColumns.COLUMN_WIDTH.ToString()].ToString());
                        tfObject.ItemStyle.Wrap = false;
                        tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        tfObject.HeaderTemplate = new GridViewTemplate(System.Web.UI.WebControls.ListItemType.Header, aRows[0][str_selCol].ToString(), str_Col_Name);
                        tfObject.ItemTemplate = new GridViewTemplate(System.Web.UI.WebControls.ListItemType.Item, aRows[0][str_selCol].ToString(), str_Col_Name);
                        grid.Columns.Add(tfObject);
                        grid.Columns[c].ControlStyle.Width = width;
                        grid.Columns[c].ItemStyle.Wrap = true;
                        //grid.Columns[c].ItemStyle.Width = 400;
                        //grid.Columns[c].HeaderStyle.Width = 400;

                        ////BoundField bField = new BoundField();

                        ////bField.HeaderText = aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()].ToString();
                        ////bField.DataField = aRows[0][GridColumns.LIST_COLUMN_NAME.ToString()].ToString();


                        //////Column width
                        ////int width = int.Parse(aRows[0][GridColumns.COLUMN_WIDTH.ToString()].ToString());
                        ////bField.ItemStyle.Width = new Unit(width);

                        //////Text Alignment
                        ////bField.ItemStyle.HorizontalAlign = (HorizontalAlign)int.Parse(aRows[0][GridColumns.COLUMN_ALIGNMENT.ToString()].ToString());

                        ////if (aRows[0][GridColumns.COLUMN_DISPLAY_FORMAT.ToString()].ToString().Length > 0)
                        ////{
                        ////    bField.DataFormatString = "{0:dd/MMM/yyyy}";
                        ////}
                        //////if (aRows[0][GridColumns.COLUMN_GRP_FLAG.ToString()].ToString()== "1")
                        //////{
                        //////    bField.ItemStyle.Wrap = false;
                        //////}
                        ////grid.Columns.Add(bField);

                        ////DataRow customRow = dtCustom.NewRow();
                        ////customRow[GridColumns.DISPLAY_COLUMN_NM.ToString()] = aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()];
                        ////customRow[GridColumns.LIST_COLUMN_NAME.ToString()] = aRows[0][GridColumns.LIST_COLUMN_NAME.ToString()];
                        ////customRow[GridColumns.COLUMN_VISIBLE.ToString()] = 1;
                        ////dtCustom.Rows.Add(customRow);

                    }
                    else
                    {
                        BoundField bField = new BoundField();
                        bField.DataField = dt.Columns[c].Caption;
                        bField.HeaderText = dt.Columns[c].Caption;
                        bField.Visible = false;
                        grid.Columns.Add(bField);
                    }
                }
                grid.DataSource = dt;
                grid.DataBind();

            }
            catch (Exception ex)
            {

            }
            return grid;
        }


        public static GridView SetupReportGrid(GridView grid, System.Data.DataTable dt, string scrnCode = "")
        {
            try
            {
                int colIndex = 0;

                bool isEditAllowed = false;
                bool isDeleteAllowed = false;


                isEditAllowed = false;

                isDeleteAllowed = false;

                //BuildColumnList & Retrieve visible columns
                System.Data.DataTable dtColumns = BuildRepColumnList(dt.Columns, scrnCode);

                System.Data.DataTable dtCustom = AddColumnsToCustomGrid();



                for (int c = 0; c <= dt.Columns.Count - 1; c++)
                {
                    System.Data.DataRow[] aRows = dtColumns.Select("" + GridColumns.LIST_COLUMN_NAME.ToString() + "='" + dt.Columns[c].ColumnName + "'");

                    if (aRows.Length > 0)
                    {

                        TemplateField tfObject = new TemplateField();
                        tfObject.HeaderText = aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()].ToString();
                        tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        int width = int.Parse(aRows[0][GridColumns.COLUMN_WIDTH.ToString()].ToString());
                        tfObject.ItemStyle.Wrap = false;
                        tfObject.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                        tfObject.HeaderTemplate = new GridViewTemplate(System.Web.UI.WebControls.ListItemType.Header, aRows[0][GridColumns.LIST_COLUMN_NAME.ToString()].ToString(), aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()].ToString());
                        tfObject.ItemTemplate = new GridViewTemplate(System.Web.UI.WebControls.ListItemType.Item, aRows[0][GridColumns.LIST_COLUMN_NAME.ToString()].ToString(), aRows[0][GridColumns.DISPLAY_COLUMN_NM.ToString()].ToString(), "LABEL");
                        grid.Columns.Add(tfObject);
                        grid.Columns[c].ControlStyle.Width = width;
                        grid.Columns[c].ItemStyle.Wrap = true;

                    }
                    else
                    {
                        BoundField bField = new BoundField();
                        bField.DataField = dt.Columns[c].Caption;
                        bField.HeaderText = dt.Columns[c].Caption;
                        bField.Visible = false;
                        grid.Columns.Add(bField);
                    }
                }
                grid.DataSource = dt;
                grid.DataBind();

            }
            catch (Exception ex)
            {

            }
            return grid;
        }


        private static System.Data.DataTable BuildRepColumnList(DataColumnCollection dcColumns, string scrnCode = "")
        {
            string str = string.Empty;



            string cmdText = " SELECT LIST_DTL_ID,LIST_COLUMN_NAME, " + " DISPLAY_COLUMN_NM,COLUMN_WIDTH,COLUMN_ALIGNMENT,COLUMN_DISPLAY_FORMAT " + " FROM   ssm_rep_list_dtl ";
            if (scrnCode.Length > 0)
            {
                cmdText += " ,ssm_rep_list_hdr ";
            }


            if (scrnCode.Length > 0)
            {
                cmdText += " where ssm_rep_list_dtl.list_hdr_id=SSM_rep_LIST_HDR.list_hdr_id and ssm_rep_list_dtl.DISPLAY_COLUMN_NM is not null and LIST_DISPLAY=1";
                cmdText += " AND upper(SSM_rep_LIST_HDR.List_Screen_Code)='" + scrnCode.ToUpper() + "'";
            }

            cmdText += "  Order by LIST_COLUMN_NAME ";


            return DBMethod.ExecuteQuery(cmdText).Tables[0];
        }
        #endregion

        static int doc_tablecount = 0;

        public static string EntryFormMenuHeader(FIN.Client.MasterPage.FINMaster tmpMaster, int intProgramId)
        {


            Hashtable ht_pgmDet;
            ht_pgmDet = Menu_BLL.GetMenuDetail(intProgramId);
            tmpMaster.FormCode = ht_pgmDet[ProgramParameters.FormCode.ToString()].ToString();
            tmpMaster.ListPageToOpen = ht_pgmDet[ProgramParameters.AddURL.ToString()].ToString();
            string str_tmp = "";
            str_tmp += "<table width='100%' class='FormHeader'>";
            str_tmp += " <tr>";
            str_tmp += "<td style='width:80%'  > &nbsp;&nbsp;";
            str_tmp += ht_pgmDet[ProgramParameters.HeaderName.ToString()].ToString();
            str_tmp += "</td>";
            str_tmp += "<td  align='right'  style='width:20%;' >";
            str_tmp += "<a href='" + ht_pgmDet[ProgramParameters.ListURL.ToString()].ToString() + "' title='Search'  ><div id='div_search' style='color:white'>Search &nbsp; &nbsp;</div></a>";
            str_tmp += "</td>";
            str_tmp += "</tr>";
            str_tmp += "</table>";
            return str_tmp;

        }

        public static string ListFormMenuHeader(FIN.Client.MasterPage.FINMaster tmpMaster, int intProgramId)
        {

            Hashtable ht_pgmDet;
            ht_pgmDet = Menu_BLL.GetMenuDetail(intProgramId);
            tmpMaster.ListPageToOpen = ht_pgmDet[ProgramParameters.AddURL.ToString()].ToString();
            string str_tmp = "";
            str_tmp += "<table width='100%' class='FormHeader'>";
            str_tmp += " <tr>";
            str_tmp += "<td style='width:80%'  >";
            str_tmp += ht_pgmDet[ProgramParameters.HeaderName.ToString()].ToString();
            str_tmp += "</td>";
            //str_tmp += "<td  align='right'  style='width:20%;' >";
            //str_tmp += "<a href='" + ht_pgmDet[ProgramParameters.ListURL.ToString()].ToString() + "' title='Search' ><div id='div_search' style='color:white'>Search</div></a>";
            //str_tmp += "</td>";
            str_tmp += "</tr>";
            str_tmp += "</table>";
            return str_tmp;

        }

        public static void ChangeLanguage()
        {
        }
        public static void ConvertToWord(string ENReportPath, string ARReportPath, string saveFileName, Novacode.Table tbl_LP)
        {

            string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
            string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

            string HRReportEN = HRReportPath.ToString() + ENReportPath;
            string HRReportAR = HRReportPath.ToString() + ARReportPath;

            string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
            string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + saveFileName + "_" + formatted + ".docx";

            switch (VMVServices.Web.Utils.LanguageCode)
            {
                case "AR":
                    System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
                    System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
                    break;
                default:
                    System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
                    System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
                    break;
            }

            using (DocX document = DocX.Load(saveHRTemplateAs))
            {
                document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));


                document.Tables[doc_tablecount] = tbl_LP;

                // Save all changes made to this document.
                document.Save();


                string linkToDownload = "../TmpWordDocs/" + Path.GetFileName(saveHRTemplateAs);

                //  ScriptManager.RegisterStartupScript(System.Web.UI.Page, System.Web.UI.Page.GetType(), "DownloadAsPdf", "window.open('" + linkToDownloadAsPdf + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        public static int CountDays(DayOfWeek day, DateTime start, DateTime end)
        {
            TimeSpan ts = end - start;
            int count = (int)Math.Floor(ts.TotalDays / 7);
            int remainder = (int)(ts.TotalDays % 7);
            int sinceLastDay = (int)(end.DayOfWeek - day);
            if (sinceLastDay < 0) sinceLastDay += 7;

            if (remainder >= sinceLastDay) count++;

            return count > 0 ? count : 0;
        }
    }
}