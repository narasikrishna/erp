﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class AvgInvoicePayment_DB : PageBase
    {
        DataTable dtDate = new DataTable();
        string Fromdate;
        string ToDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["AVGData"] = null;
                AssignToControl();
                FillComboBox();
                getGRNDataChart();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetSupplier(ref ddlSupplier);
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYr);
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AVGData"] != null)
                {
                    chartGRN.DataSource = (DataTable)Session["AVGData"];
                    chartGRN.DataBind();

                    gvGraphdata.DataSource = (DataTable)Session["AVGData"];
                    gvGraphdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTopSuppliers", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void findate()
        {

            dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(ddlFinancialYr.SelectedValue)).Tables[0];
            Fromdate = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
            ToDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chartGRN.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                if (ddlFinancialYr.SelectedValue.ToString().Length > 0)
                {
                    dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(ddlFinancialYr.SelectedValue)).Tables[0];
                    Fromdate = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                    ToDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());

                    if (Fromdate != string.Empty && ddlSupplier.SelectedValue.ToString().Length > 0)
                    {
                        ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetAvgInvoicePayment(ddlSupplier.SelectedValue, Fromdate, ToDate));
                    }
                }
                if (ddlSupplier.SelectedValue.ToString().Length > 0)
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetAvgInvoicePayment(ddlSupplier.SelectedValue));
                }

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void getGRNDataChart()
        {
            DataTable dt_GRNData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetAvgInvoicePayment()).Tables[0];
            Session["AVGData"] = dt_GRNData;
        }

        protected void ddlFinancialYr_SelectedIndexChanged(object sender, EventArgs e)
        {
            findate();
            DataTable dt_avg_list = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetAvgInvoicePayment("", Fromdate, ToDate)).Tables[0];
            Session["AVGData"] = dt_avg_list;

            LoadGraph();
        }

        protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            findate();
            DataTable dt_avg_list = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetAvgInvoicePayment(ddlSupplier.SelectedValue)).Tables[0];
            Session["AVGData"] = dt_avg_list;

            LoadGraph();
        }





    }
}