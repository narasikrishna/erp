﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TopSupplierAged_DB.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.TopSupplierAged" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Top 5 Suppliers
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px; width: 30px">
                <asp:TextBox ID="txtBuget3" runat="server" Text="90" CssClass="txtBox_N" 
                    AutoPostBack="True" ontextchanged="txtBuget3_TextChanged"></asp:TextBox>
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px; width: 30px">
                <asp:TextBox ID="txtBuget2" runat="server" Text="60" CssClass="txtBox_N"></asp:TextBox>
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px; width: 30px">
                <asp:TextBox ID="txtBuget1" runat="server" Text="30" CssClass="txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divChrtTopSupp" style="width: 100%">
            <asp:Chart ID="chrtTop5Supply" runat="server" Height="310px" Width="800px" EnableViewState="True"
                ImageStorageMode="UseImageLocation">
                <Series>
                    <asp:Series Name="S_FirstBuget" XValueMember="VENDOR_NAME" YValueMembers="FIRST_BUGET"
                        IsValueShownAsLabel="true" LabelAngle="60" ChartType="StackedBar">
                    </asp:Series>
                    <asp:Series Name="S_SecondBuget" XValueMember="VENDOR_NAME" YValueMembers="SECOND_BUGET"
                        IsValueShownAsLabel="true" LabelAngle="60" ChartType="StackedBar">
                    </asp:Series>
                    <asp:Series Name="S_ThirdBuget" XValueMember="VENDOR_NAME" YValueMembers="THIRD_BUGET"
                        IsValueShownAsLabel="true" LabelAngle="60" ChartType="StackedBar">
                    </asp:Series>
                    <asp:Series Name="S_LastBuget" XValueMember="VENDOR_NAME" YValueMembers="LAST_BUGET"
                        IsValueShownAsLabel="true" LabelAngle="60" ChartType="StackedBar">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false" Area3DStyle-Inclination="20">
                        <Area3DStyle Enable3D="false" Inclination="20"></Area3DStyle>
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="VENDOR_NAME" HeaderText="Supplier"></asp:BoundField>
                    <asp:BoundField DataField="FIRST_BUGET" HeaderText="Buget1">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SECOND_BUGET" HeaderText="Buget2">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="THIRD_BUGET" HeaderText="Buget3">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LAST_BUGET" HeaderText="Buget4">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
