﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class APItemUtilization_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["IssuedQuantity"] = null;
                AssignToControl();
                getIssuedQuantity();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void getIssuedQuantity()
        {
            DataTable dt_IssuedQuantity = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetIssuedQuantity()).Tables[0];
            Session["IssuedQuantity"] = dt_IssuedQuantity;
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["IssuedQuantity"] != null)
                {
                    chartIssuedQuantity.DataSource = (DataTable)Session["IssuedQuantity"];

                    chartIssuedQuantity.Series["IssuedQuantity"].XValueMember = "item_name";
                    chartIssuedQuantity.Series["IssuedQuantity"].YValueMembers = "issued_quantity";
                    chartIssuedQuantity.DataBind();

                    gvGraphdata.DataSource = (DataTable)Session["IssuedQuantity"];
                    gvGraphdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APItemUtilization", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chartIssuedQuantity.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetIssuedQuantity());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}