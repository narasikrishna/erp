﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.AP_DASHBOARD
{
    public partial class APAgingAnalysis_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["CUST_AGE"] = null;
                AssignToControl();
                FillComboBox();
                getEmployeeAgeDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                txt30.Text = "30";
                txt60.Text = "60";
                txt90.Text = "90";
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            if (Session["CUST_AGE"] != null)
            {
                chrtEmpAge.DataSource = (DataTable)Session["CUST_AGE"];
                chrtEmpAge.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["CUST_AGE"];
                gvGraphdata.DataBind();
            }
        }
        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetSupplier(ref ddlSupplier);
        }
        private void getEmployeeAgeDetails()
        {
            DataTable dt_empAge = DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.GetAgingAnalysisChart()).Tables[0];
            Session["CUST_AGE"] = dt_empAge;

        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();
                ErrorCollection.Remove("datemsg1");
                ErrorCollection.Remove("datemsg2");
                ErrorCollection.Remove("datemsg");
                ErrorCollection.Remove("datemsg1st");
                ErrorCollection.Remove("datemsg2nd");
                ErrorCollection.Remove("datemsgcase");

                chrtEmpAge.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                if (ddlSupplier.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.VENDOR_ID, ddlSupplier.SelectedValue.ToString());
                }
                if (txt30.Text != string.Empty)
                {
                    htFilterParameter.Add("firstCase", txt30.Text);

                    if (txt60.Text != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToInt(txt30.Text) < CommonUtils.ConvertStringToInt(txt60.Text))
                        {
                            htFilterParameter.Add("secondCase", txt60.Text);
                        }
                        else
                        {
                            ErrorCollection.Add("datemsg1st", "Aging Bucket2 should be greater than Aging Bucket1");
                        }
                    }
                    else
                    {
                        ErrorCollection.Add("datemsg1", "Aging Bucket2 cannot be empty");
                    }
                }
                else
                {
                    ErrorCollection.Add("datemsgcase", "Aging Bucket1 cannot be empty");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (txt90.Text != string.Empty && txt60.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToInt(txt60.Text) < CommonUtils.ConvertStringToInt(txt90.Text))
                    {
                        htFilterParameter.Add("thirdCase", txt90.Text);
                    }
                    else
                    {
                        ErrorCollection.Add("datemsg2", "Aging Bucket3 should be greater than Aging Bucket2");
                    }
                }
                else
                {
                    ErrorCollection.Add("datemsg2nd", "Aging Bucket3 cannot be empty");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                }
               



                //ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getReportData());
                //  ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.GetAgingAnalysisChart());


                //ReportFormulaParameter = htHeadingParameters;

                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


    }
}