﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class APGRN_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["GRNData"] = null;
                AssignToControl();
                FillComboBox();
                getGRNDataChart();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetSupplier(ref ddlSupplier);
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GRNData"] != null)
                {
                    chartGRN.DataSource = (DataTable)Session["GRNData"];
                    chartGRN.DataBind();

                    gvGraphdata.DataSource = (DataTable)Session["GRNData"];
                    gvGraphdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTopSuppliers", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chartGRN.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                if (ddlItem.SelectedValue.ToString().Length > 0 && ddlSupplier.SelectedValue.ToString().Length > 0)
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue, ddlItem.SelectedValue));
                }
                else if (ddlSupplier.SelectedValue.ToString().Length > 0)
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue));
                }

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void getGRNDataChart()
        {
            DataTable dt_GRNData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetGRNDataChart()).Tables[0];
            Session["GRNData"] = dt_GRNData;
        }

        protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.AP.PurchaseOrder_BLL.GetItemBasedVendor(ref ddlItem, ddlSupplier.SelectedValue);

            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue)).Tables[0];
            Session["GRNData"] = dt_Emp_Dist_list;

            LoadGraph();
        }

        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGRNDetails();
        }

        private void LoadGRNDetails()
        {
            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue, ddlItem.SelectedValue)).Tables[0];
            Session["GRNData"] = dt_Emp_Dist_list;
            LoadGraph();
        }
    }
}