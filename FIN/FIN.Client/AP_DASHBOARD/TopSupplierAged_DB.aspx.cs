﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class TopSupplierAged : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["FIVE_SUPPLIER_GRAPH"] = null;
                AssignToControl();
                DisplayFiveSupplyDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["FIVE_SUPPLIER_GRAPH"] != null)
                {
                    chrtTop5Supply.DataSource = (DataTable)Session["FIVE_SUPPLIER_GRAPH"];
                    //chrtTop5Supply.Series["S_supply_Count"].XValueMember = "VENDOR_NAME";
                    //chrtTop5Supply.Series["S_supply_Count"].YValueMembers = "LAST_BUGET";
                    chrtTop5Supply.DataBind();


                    gvGraphdata.DataSource = (DataTable)Session["FIVE_SUPPLIER_GRAPH"];
                    gvGraphdata.Columns[1].HeaderText = "0 - " + txtBuget1.Text;
                    gvGraphdata.Columns[2].HeaderText = txtBuget1.Text + " - " + txtBuget2.Text;
                    gvGraphdata.Columns[3].HeaderText = txtBuget2.Text + " - " + txtBuget3.Text;
                    gvGraphdata.Columns[4].HeaderText = "> " + txtBuget3.Text;
                    gvGraphdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTopSuppliers", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();
                chrtTop5Supply.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.GetTop5SupplierAmt());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void DisplayFiveSupplyDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getTop5SupplierAged(txtBuget1.Text, txtBuget2.Text, txtBuget3.Text)).Tables[0];
            Session["FIVE_SUPPLIER_GRAPH"] = dt_EmpDet;
            LoadGraph();

        }

        protected void txtBuget3_TextChanged(object sender, EventArgs e)
        {
            DisplayFiveSupplyDetails();
        }

    }
}