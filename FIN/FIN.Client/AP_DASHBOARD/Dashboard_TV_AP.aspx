﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="Dashboard_TV_AP.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.Dashboard_TV_AP" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .tv_menu_list
        {
            background: -moz-linear-gradient(top, #FFFFFF, #A7A8A8);
            background: -ms-linear-gradient(top, #FFFFFF, #A7A8A8);
            background: -o-linear-gradient(top, #FFFFFF, #A7A8A8);
            background: -webkit-linear-gradient(top, #FFFFFF, #A7A8A8);
            color: Maroon;
            float: left;
            width: 100%;
            height: 450px;
            font-family: Verdana;
        }
        .tv_menu_text
        {
            font-family: Verdana;
            color: Black;
            font-size: 13px;
        }
        .box
        {
            background-color: #CCE0FF;
            width: 400px;
            height: 200px;
            background-image: url('../Images/MHeaderRight.jpg');
            float: left;
            margin-top: 100px;
            margin-left: 450px;
        }
        
        .tv_right_frame
        {
            width: 98%;
            height: 450px;
        }
        .tv_frame
        {
            width: 100%;
            height: 480px;
        }
        .tv_table_leftframe
        {
            border: 4px;
            border-style: inset;
            width: 97%;
            bordercolordark: "#000080";
            bordercolorlight: "#F2F2F2";
            font-family: Verdana;
        }
        .tv_table_rightframe
        {
            border: 4px;
            border-style: inset;
            width: 100%;
            bordercolordark: "#000080";
            bordercolorlight: "#F2F2F2";
        }
        .tv_Title
        {
            background: -moz-linear-gradient(top, #ffffff, #A7A8A8);
            background: -ms-linear-gradient(top, #ffffff, #A7A8A8);
            background: -o-linear-gradient(top, #ffffff, #A7A8A8);
            background: -webkit-linear-gradient(top, #ffffff, #A7A8A8);
            color: White;
            width: 100%;
            height: 25px;
        }
    </style>
    <style type="text/css">
        .frmStyle
        {
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: 0px solid red;
            top: 0;
            left: 0;
        }
        
        
        .MyTabStyle .ajax__tab_header
        {
            cursor: pointer;
            display: block;
        }
        .MyTabStyle .ajax__tab_header .ajax__tab_outer
        {
            color: Black;
            margin-left: 1px;
            border: 1px solid black;
            background-color: #A9CFEA;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        
        .MyTabStyle .ajax__tab_header .ajax__tab_outer1
        {
            color: Black;
            padding-left: 2px;
            margin-right: 3px;
            border: 1px solid black;
            background-color: White;
        }
        .MyTabStyle .ajax__tab_header .ajax__tab_inner
        {
            border-color: #666;
            color: Black;
        }
        .MyTabStyle .ajax__tab_hover .ajax__tab_outer
        {
            background-color: #D1D1D1;
            color: #FFFFFF;
        }
        .MyTabStyle .ajax__tab_hover .ajax__tab_inner
        {
            color: #FFFFFF;
        }
        .MyTabStyle .ajax__tab_active .ajax__tab_outer
        {
            background-color: #D1D1D1;
            color: #FFFFFF;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            padding: 3px 10px 2px 10px;
        }
        .MyTabStyle .ajax__tab_active .ajax__tab_inner
        {
            color: #FFFFFF;
        }
        .MyTabStyle .ajax__tab_body
        {
            color: Black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" runat="server">
        <div class="GridHeader tv_Title" align="center">
            <div class="DBlblBox" style="padding-left: 10px; padding-top: 2px;">
                Business Intelligence
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer tv_frame" id="divmainfrmame">
            <div id="divLeftFrame" style="overflow: auto; float: left; width: 15%; height: 100%;">
                <table class="tv_table_leftframe">
                    <tr>
                        <td>
                            <div id="divLeftMenu" class="tv_menu_list" runat="server">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divRightFrame" style="overflow: auto; float: right; border: 1; width: 85%;
                height: 100%;">
                <table class="tv_table_rightframe">
                    <tr>
                        <td>
                            <div id="div3" style="float: left; width: 100%; height: 100%;" runat="server">
                                <iframe runat="server" id="tvself" name="tvself" class="frames tv_right_frame" frameborder="0"
                                    src="GLDashBoard.aspx" allowtransparency="true" style="border-style: solid; border-width: 0px;
                                    background-color: transparent; padding-left: 5px;"></iframe>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
      
        <div class="divFormcontainer" style="width: 98%; height: 700px; display: none;" id="div2">
            <asp:Panel ID="pnlTab1" runat="server">
                <div>
                    <cc2:TabContainer ID="TCAppDet" runat="server" ActiveTabIndex="2" Width="100%" Height="100%"
                        CssClass="MyTabStyle">
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1" Height="500px">
                            <HeaderTemplate>
                                <asp:Label ID="Label16" runat="server" Text="GL Transaction"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="frm_GLTransaction" class="frmStyle" src="GLTransaction_DB.aspx?TVYN=Y&ProgramID=466&ReportName=GL_DASHBOARD/GLTransaction_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel2" Height="700px">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Group Balances">
                                </asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="frm_GLGroupBal" class="frmStyle" src="GLGroupBalances_DB.aspx?TVYN=Y&ProgramID=467&ReportName=GL_DASHBOARD/GLGroupBalances_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel3" Height="700px">
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text="Top 5 Expenses">
                                </asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="frmTopExp" class="frmStyle" src="GLTopExpenses_DB.aspx?TVYN=Y&ProgramID=469&ReportName=GL_DASHBOARD/GLTopExpenses_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel4" Height="700px">
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text="Bank Balances">
                                </asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="Iframe1" class="frmStyle" src="GLBankBalance_DB.aspx?TVYN=Y&ProgramID=470&ReportName=GL_DASHBOARD/GLBankBalances_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel5" Height="700px">
                            <HeaderTemplate>
                                <asp:Label ID="Label4" runat="server" Text="Cash Balances">
                                </asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="Iframe2" class="frmStyle" src="GLCashBalance_DB.aspx?TVYN=Y&ProgramID=471&ReportName=GL_DASHBOARD/GLCashBalance_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                        <cc2:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel6" Height="700px">
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server" Text="Top 5 Revenue">
                                </asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <iframe id="Iframe3" class="frmStyle" src="GLTopRevenue_DB.aspx?TVYN=Y&ProgramID=472&ReportName=GL_DASHBOARD/GLTopRevenue_BIRPT.rpt"
                                    scrolling="no"></iframe>
                            </ContentTemplate>
                        </cc2:TabPanel>
                    </cc2:TabContainer>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
