﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_DASHBOARD
{
    public partial class Dashboard_TV_AP : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtData = new DataTable();
        DataTable dt_menulist = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["TVMenu"] = null;
                Startup();
                LoadMainMenu();
            }
        }
        private void Startup()
        {
            if (Request.QueryString["TVYN"] == null)
            {
                Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
                Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
                Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
                Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            }
        }
        private void LoadMainMenu()
        {
            System.Text.StringBuilder str_MainMenu = new System.Text.StringBuilder();
            System.Text.StringBuilder str_SubMenuQuery = new System.Text.StringBuilder();
            string str_LinkQuery = "";

            if (Session[FINSessionConstants.Sel_Lng] != null)
            {
                dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetTVMenuList(Session[FINSessionConstants.UserName].ToString(), Session[FINSessionConstants.ModuleName].ToString(), Session[FINSessionConstants.Sel_Lng].ToString(), Master.ProgramID.ToString())).Tables[0];
            }
            string str_linkhead = "";

            str_MainMenu.Append("<div id='firstpane'  >");

            for (int i = 0; i < dt_menulist.Rows.Count; i++)
            {
                if (dt_menulist.Rows[i]["ENABLED_FLAG"].ToString() == "1")
                {
                    if (str_linkhead == dt_menulist.Rows[i]["SCREEN_NAME"].ToString().Trim())
                    {
                    }
                    else
                    {

                        if (str_linkhead.Length != 0)
                        {
                            //str_MainMenu.Append("  </ul>");
                            str_MainMenu.Append(" </div>");
                        }
                        str_MainMenu.Append("<div >");
                        str_MainMenu.Append("<p> <a target='tvself' class='tv_menu_text' style='border-style: none; border-color: inherit; text-decoration: none' href='");
                        str_MainMenu.Append("GLDashBoard.aspx?");
                        str_MainMenu.Append("ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "'");                       
                        str_MainMenu.Append("><span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a> </p>");
                        str_linkhead = dt_menulist.Rows[i]["SCREEN_NAME"].ToString();


                        //   str_MainMenu.Append("<div ><ul >");
                        //    str_MainMenu.Append("<li>");
                        //  string str_LinkURL = "<li><a href='" + dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&" + QueryStringTags.ID.ToString() + "=0" + "&" + QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["ADD_FLAG"].ToString() + "&" + QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "'  target='centerfrm'><span>" + dt_menulist.Rows[i]["MENU_DESCRIPTION"].ToString() + "</span></a></li>";
                        //  str_MainMenu.Append("<p> <a  onclick='fn_onDisplay()' target='tvself' style='border-style: none; border-color: inherit;color:#0A9EF3; text-decoration: none' href='");
                        //  // str_LinkQuery += dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                        //str_MainMenu.Append("GLDashBoard.aspx?");
                        //str_MainMenu.Append("ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "'");
                        //str_LinkQuery += "TVYN=Y&";
                        //str_LinkQuery += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                        //str_LinkQuery += QueryStringTags.ID.ToString() + "=0" + "&";
                        //str_LinkQuery += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                        //str_LinkQuery += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                        //str_LinkQuery += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                        //str_LinkQuery += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                        //str_LinkQuery += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "&";
                        //str_LinkQuery += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[i]["ATTRIBUTE3"].ToString() + "'";
                        //str_MainMenu.Append(str_LinkQuery);
                        ////Session["str_LinkTVQuery"] = str_LinkQuery;
                        //str_MainMenu.Append("><span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a> </p>");
                        ////  str_MainMenu.Append("</li>");

                        //str_linkhead = dt_menulist.Rows[i]["SCREEN_NAME"].ToString();
                    }
                }
            }

            //  str_MainMenu.Append("</ul>");
            str_MainMenu.Append("</div>");
            str_MainMenu.Append("</div>");
            divLeftMenu.InnerHtml = str_MainMenu.ToString();
            Session["TVMenu"] = dt_menulist;
        }
    }
}