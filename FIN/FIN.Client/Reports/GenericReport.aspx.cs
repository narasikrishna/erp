﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL;
using FIN.DAL;
using System.Collections;
using VMVServices.Web;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace FIN.Client.Reports
{
    public partial class GenericReport : PageBase
    {
        string modifyURL = string.Empty;
        string deleteURL = string.Empty;
        string addURL = string.Empty;
        string searchURL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadContent();
            }
        }
        private void Startup(int progID)
        {
            try
            {
                ErrorCollection.Clear();
                //Get Programs Information & set Properties
                Hashtable htProgram = Menu_BLL.GetMenuDetail(progID);
                Master.KeyName = htProgram[ProgramParameters.AccessibleName.ToString()].ToString();
                Master.ProgramID = int.Parse(htProgram[ProgramParameters.ProgramID.ToString()].ToString());
                Master.ListPageToOpen = htProgram[ProgramParameters.ListURL.ToString()].ToString();

                this.Title = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
                modifyURL = htProgram[ProgramParameters.ModifyURL.ToString()].ToString();
                deleteURL = htProgram[ProgramParameters.DeleteURL.ToString()].ToString();
                addURL = htProgram[ProgramParameters.AddURL.ToString()].ToString();
                searchURL = htProgram[ProgramParameters.AddURL.ToString()].ToString();
                //hf_MURL.Value = modifyURL;
                //hf_DURL.Value = deleteURL;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void LoadContent()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["ProgramID"] != null)
                {
                    DataTable dt_menulist = new DataTable();
                    dt_menulist = (DataTable)Session[FINSessionConstants.MenuData];
                    DataRow[] dr = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());
                    if (dr.Length > 0)
                    {
                        Startup(int.Parse(Session["ProgramID"].ToString()));
                        //if (dr[0]["MODULECODE"].ToString() == FIN.BLL.FINListConstants_BLL.GL)
                        //{
                     //   FN_GLListScreen(dr[0]["SCREEN_CODE"].ToString());
                       // hf_ScreenCode.Value = dr[0]["SCREEN_CODE"].ToString();
                        // }
                    }

                }
                div_Filter_dtlbl_1.Visible = true;
                div_Filter_dtlbl_2.Visible = true;
                div_Filter_dtlbl_1.InnerHtml = "From Date";
                div_Filter_dtlbl_2.InnerHtml = "To Date";
                div_Filter_dt_1.Visible = true;
                div_Filter_dt_2.Visible = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FN_GLListScreen(string str_ScrenCode)
        {
            try
            {
                ErrorCollection.Clear();
             
                DataSet dsData = new DataSet();

                //if (VMVServices.Web.Utils.LanguageCode == "AR")
                //{
                //    dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetAREmpDetailsDOJ(dtFilter1.Text.ToString(), dtFilter2.Text.ToString()));
                //}
                //else
                //{
                //    dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmpDetailsDOJ(dtFilter1.Text.ToString(), dtFilter2.Text.ToString()));
                //}
                //if (dsData.Tables[0].Rows.Count > 0)
                //{
                //    Session[FINSessionConstants.GridData] = dsData;
                //    imgbtnPDF.Enabled = false;
                //    imgbtnEXCEL.Enabled = false;
                //}
                //gridData.DataSource = dsData;
                //gridData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Clear();
                    ErrorCollection.Add("not allowed", "No Record to Found");
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        #region "Paging"
        /// <summary>
        /// Set the paging into Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                gridData.PageIndex = e.NewPageIndex;
                gridData.Columns.Clear();
              //  gridData = Master.SetupGridnew(gridData, (DataTable)Session["QueryFormData"], Master.KeyName, hf_ScreenCode.Value.ToString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion


     
        protected void btnGridContSearch_Click(object sender, EventArgs e)
        {
            DataSet dsData = new DataSet();
            lblDOA.Text = dtFilter1.Text;

            dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmpDetailsDOJ(dtFilter1.Text.ToString(), dtFilter2.Text.ToString()));
            if (dsData.Tables[0].Rows.Count > 0)
            {
                Session[FINSessionConstants.GridData] = dsData;
                imgbtnPDF.Enabled = false;
                imgbtnEXCEL.Enabled = false;
            }
            gridData.DataSource = dsData;
            gridData.DataBind();

            //div_Filter.Visible = true;
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //div_Filter.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 80f, 80f, -2f, 35f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();
        }
        private void FilterGridData()
        {
            //DataTable dt_QueryData = (DataTable)Session["FullQueryData"];
            //var tmp_data = dt_QueryData.AsEnumerable().Where(r => r[hf_ColName.Value].ToString().ToUpper().Contains(hf_ColValue.Value.ToString().ToUpper()));
            //DataTable dt_ResultQuery = new DataTable();
            //gridData.Columns.Clear();
            //if (tmp_data.Any())
            //{
            //    dt_ResultQuery = tmp_data.CopyToDataTable();
            //    Session["QueryFormData"] = dt_ResultQuery;
            //}
            //else
            //{
            //    dt_ResultQuery = dt_QueryData.Copy();
            //    dt_ResultQuery.Rows.Clear();
            //}
            //gridData = Master.SetupGridnew(gridData, dt_ResultQuery, Master.KeyName, hf_ScreenCode.Value);
            //TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
            //txt_tmp.Text = hf_ColValue.Value;
            //txt_tmp.Focus();
        }

        private void FilterDBData()
        {

            //try
            //{
            //    ErrorCollection.Clear();
            //    string strlist = "";
            //    DataTable dtData = new DataTable();
            //    //  gridData = Master.SetupGridnew(gridData, FindData(modifyURL, deleteURL), Master.KeyName);
            //    if (hf_ColName.Value.ToString().Length > 0)
            //    {
            //        strlist = FIN.DAL.FINSP.GetSPFOR_Filter_Listscreen(hf_ScreenCode.Value.ToString(), hf_ColName.Value, hf_ColValue.Value.ToString().ToUpper());
            //    }
            //    else
            //    {
            //        strlist = FIN.DAL.FINSP.GetSPFOR_Listscreen(hf_ScreenCode.Value.ToString(), false, true);
            //    }
            //    strlist = strlist.Replace("'MODIFYDELETE'", hf_MURL.Value + "," + hf_DURL.Value);
            //    dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];
            //    Session["QueryFormData"] = dtData;
            //    Session["FullQueryData"] = dtData;
            //    gridData.Columns.Clear();
            //    gridData = Master.SetupGridnew(gridData, dtData, Master.KeyName, hf_ScreenCode.Value);
            //    TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
            //    txt_tmp.Text = hf_ColValue.Value;
            //    txt_tmp.Focus();

            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        ErrorCollection.Clear();
            //        ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //    }
            //}

        }

        protected void imgbtnPDF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (Session[FINSessionConstants.GridData] != null)
                //{
                //    ImageButton img_btn_tmp = (ImageButton)sender;
                //    Response.Redirect("../ExportGridData.aspx?ExportId=" + img_btn_tmp.CommandName, true);
                //}
                //else
                //{
                //    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                //}


                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //StringWriter sw = new StringWriter();
                //HtmlTextWriter hw = new HtmlTextWriter(sw);
                //div_Filtser.InnerHtml = hw;
                //this.Page.RenderControl(hw);
                //StringReader sr = new StringReader(sw.ToString());
                //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //htmlparser.Parse(sr);
                //pdfDoc.Close();
                //Response.Write(pdfDoc);
                //Response.End();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}