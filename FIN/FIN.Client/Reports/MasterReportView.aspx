﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MasterReportView.aspx.cs" Inherits="FIN.Client.Reports.MasterReportView" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divClear_10">
    </div>
    <div class="ListGrid" align="left" style="width: 100%;">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 140px" id="Div2">
                Report Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlReportName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlModuelName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divRowContainer " style="display: none">
            <div class="lblBox LNOrient" style="width: 130px" id="lblModuelName">
                Module Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlModuelName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlModuelName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 130px" id="lblFormname">
                Form
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:DropDownList ID="ddlFormName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlFormName_SelectedIndexChanged">
                    <asp:ListItem>--- Select ---</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer ">
            <div class="lblBox LNOrient" style="width: 120px" id="Div1">
                Period Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkAllData" runat="server" Text="Open Period" Checked="true" CssClass="lblBox" />
            </div>
            <div class="lblBox" style="float: left; width: 360px">
            </div>
            <div class="lblBox LNOrient" style="width: 100px">
                <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" OnClick="btnView_Click"
                    Style="border: 0px;" />
                <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn" OnClick="btnView_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div style="width: 100%" align="center">
        </div>
        <div>
            <asp:GridView ID="gridData" runat="server" AutoGenerateColumns="False" GridLines="None"
                Style="width: 100%;" AllowPaging="true" CssClass="mGrid" PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" SelectedRowStyle-CssClass="selrow" OnPageIndexChanging="gridData_PageIndexChanging"
                EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gridData_RowDataBound"
                PageSize="10">
                <Columns>
                </Columns>
                <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <PagerStyle CssClass="pgr"></PagerStyle>
                <SelectedRowStyle CssClass="selrow"></SelectedRowStyle>
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer ">
            <div class="lblBox LNOrient" style="width: 120px">
                Export Data
            </div>
            <div class="lblBox LNOrient" style="width: 300px">
                <asp:ImageButton ID="imgbtnPDF" runat="server" ImageUrl="~/Images/File_Pdf.png" CommandName="PDF"
                    Height="30px" OnClick="imgbtnPDF_Click" Width="30px" />
                &nbsp;&nbsp;
                <asp:ImageButton ID="imgbtnEXCEL" runat="server" ImageUrl="~/Images/File_Excel.png"
                    CommandName="EXCEL" Height="30px" OnClick="imgbtnPDF_Click" Width="30px" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hf_ScreenCode" runat="server" />
    <asp:HiddenField ID="hf_MURL" runat="server" />
    <asp:HiddenField ID="hf_DURL" runat="server" />
    <asp:HiddenField ID="hf_ColName" runat="server" Value="" />
    <asp:HiddenField ID="hf_ColValue" runat="server" Value="" />
    <asp:Button ID="btnGridContSearch" runat="server" Text="Search" Style="display: none"
        OnClick="btnGridContSearch_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script type="text/javascript">
        function fn_GridRecSearch(event, txt_obj, colName) {

            var colfiled = document.getElementById("FINContent_hf_ColName");
            var valuefiled = document.getElementById("FINContent_hf_ColValue");
            if (colfiled != null)
                colfiled.value = "";
            if (valuefiled != null)
                valuefiled.value = "";

            //            var btn_SearchCont = document.getElementById("FINContent_btnGridContSearch")
            //            // alert(btn_SearchCont);
            //            if (btn_SearchCont != null) {
            //                if (colfiled != null)
            //                    colfiled.value = colName;
            //                if (valuefiled != null)
            //                    valuefiled.value = txt_obj.value;

            //                btn_SearchCont.click();

            if (event.keyCode == 13 || event.which == 13) {

                var btn_SearchCont = document.getElementById("FINContent_btnGridContSearch")
                // alert(btn_SearchCont);
                if (btn_SearchCont != null) {
                    if (colfiled != null)
                        colfiled.value = colName;
                    if (valuefiled != null)
                        valuefiled.value = txt_obj.value;

                    btn_SearchCont.click();
                }
            }
        }
        function fn_searchclick() {
            var btn_SearchCont = document.getElementById("FINContent_btnGridContSearch")
            alert(btn_SearchCont);
            if (btn_SearchCont != null) {
                btn_SearchCont.click();
            }
        }

        function fn_onLinkClick() {

            window.parent.fn_QueryFormDisplay();
        }
    </script>
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_imgbtnAdd").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_imgBtnUpdate").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
