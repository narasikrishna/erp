﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL;
using System.Collections;
using VMVServices.Web;

namespace FIN.Client.Reports
{
    public partial class MasterReportView : PageBase
    {
        string modifyURL = string.Empty;
        string deleteURL = string.Empty;
        string addURL = string.Empty;
        string searchURL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session[FINSessionConstants.GridData] = null;
                Startup();
                FillCombobox();
                ChnageLanguage();
            }
        }
        private void FillCombobox()
        {
            Lookup_BLL.GetModuleName(ref ddlModuelName, Session[FINSessionConstants.Sel_Lng].ToString());
            FIN.BLL.SSM.MasterReport_BLL.GeReportName(ref ddlReportName);
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

          
        }

        private void ChnageLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));
                    chkAllData.Text = Prop_File_Data["Open_Period_P"];
                    


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FN_GLListScreen(string str_ScrenCode)
        {
            try
            {
                ErrorCollection.Clear();
                string strlist = "";
                DataTable dtData = new DataTable();
             //   strlist = FIN.DAL.FINSP.GetSPFOR_Listscreen(str_ScrenCode.ToString(), false, true);
                strlist = FIN.DAL.FINSP.GetSPFOR_REPListscreen(str_ScrenCode.ToString(), false, true);                
                strlist = strlist.Replace("'MODIFYDELETE'", modifyURL + "," + deleteURL);
                dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];
                Session["QueryFormData"] = dtData;
                Session["FullQueryData"] = dtData;
                Master.KeyName = "RVIEW";
                //DataTable dt_menudet = FIN.DAL.DBMethod.ExecuteQuery("select * from ssm_menu_items where form_code='" + ddlFormName.SelectedValue.ToString() + "'").Tables[0];
                //if (dt_menudet.Rows.Count > 0)
                //{
                //    Master.ProgramID = int.Parse(dt_menudet.Rows[0]["PK_ID"].ToString());

                //}
                gridData.Columns.Clear();

                gridData = ClsGridBase.SetupReportGrid(gridData, dtData, str_ScrenCode);
                DataTable dt_tmp = (DataTable)gridData.DataSource;
                for (int iLoop = 0; iLoop < gridData.Columns.Count; iLoop++)
                {
                    dt_tmp.Columns[iLoop].ColumnName = gridData.Columns[iLoop].HeaderText;
                }
                gridData.SelectedIndex = 1;
                Session[FINSessionConstants.GridData] = dt_tmp;
                chkAllData.Visible = true;
                gridData.Columns.Clear();
                gridData = ClsGridBase.SetupReportGrid(gridData, (DataTable)Session["QueryFormData"], ddlFormName.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Clear();
                    ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Bind the records into gridviw
        /// </summary>
        /// <param name="modifyURL"></param>
        /// <param name="deleteURL"></param>
        /// <returns></returns>

        private DataTable FindData(string modifyURL, string deleteURL)
        {
            //return DBMethod.ExecuteQuery(iAcademeSQL.GetCourseSubjectList(modifyURL, deleteURL)).Tables[0];
            return FIN.BLL.GL.AccountingGroups_BLL.getListData(modifyURL, deleteURL);
        }

        #region "Paging"
        /// <summary>
        /// Set the paging into Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                gridData.PageIndex = e.NewPageIndex;
                gridData.Columns.Clear();
                gridData = ClsGridBase.SetupReportGrid(gridData, (DataTable)Session["QueryFormData"], ddlFormName.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //for (int rowIndex = gridData.Rows.Count - 2;
            //                                   rowIndex >= 0; rowIndex--)
            //{
            //    GridViewRow gvRow = gridData.Rows[rowIndex];
            //    GridViewRow gvPreviousRow = gridData.Rows[rowIndex + 1];
            //    for (int cellCount = 0; cellCount < gvRow.Cells.Count;
            //                                                  cellCount++)
            //    {
            //        if (gvRow.Cells[cellCount].Text ==
            //                               gvPreviousRow.Cells[cellCount].Text)
            //        {
            //            if (gvPreviousRow.Cells[cellCount].RowSpan < 2)
            //            {
            //                gvRow.Cells[cellCount].RowSpan = 2;
            //            }
            //            else
            //            {
            //                gvRow.Cells[cellCount].RowSpan =
            //                    gvPreviousRow.Cells[cellCount].RowSpan + 1;
            //            }
            //            gvPreviousRow.Cells[cellCount].Visible = false;
            //        }
            //    }

            //}
        }

        protected void btnGridContSearch_Click(object sender, EventArgs e)
        {
            if (!chkAllData.Checked)
            {
                FilterDBData();
            }
            else
            {
                FilterGridData();
            }


        }
        private void FilterGridData()
        {
            DataTable dt_QueryData = (DataTable)Session["FullQueryData"];
            var tmp_data = dt_QueryData.AsEnumerable().Where(r => r[hf_ColName.Value].ToString().ToUpper().Contains(hf_ColValue.Value.ToString().ToUpper()));
            DataTable dt_ResultQuery = new DataTable();
            gridData.Columns.Clear();
            if (tmp_data.Any())
            {
                dt_ResultQuery = tmp_data.CopyToDataTable();
                Session["QueryFormData"] = dt_ResultQuery;
            }
            else
            {
                dt_ResultQuery = dt_QueryData.Copy();
                dt_ResultQuery.Rows.Clear();
            }


            gridData = ClsGridBase.SetupReportGrid(gridData, dt_ResultQuery, ddlFormName.SelectedValue);
            DataTable dt_tmp = (DataTable)gridData.DataSource;
            for (int iLoop = 0; iLoop < gridData.Columns.Count; iLoop++)
            {
                dt_tmp.Columns[iLoop].ColumnName = gridData.Columns[iLoop].HeaderText;
            }
            Session[FINSessionConstants.GridData] = dt_tmp;

            TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
            txt_tmp.Text = hf_ColValue.Value;
            txt_tmp.Focus();
        }

        private void FilterDBData()
        {

            try
            {
                ErrorCollection.Clear();
                string strlist = "";
                DataTable dtData = new DataTable();
                //  gridData = Master.SetupGridnew(gridData, FindData(modifyURL, deleteURL), Master.KeyName);
                if (hf_ColName.Value.ToString().Length > 0)
                {
                    strlist = FIN.DAL.FINSP.GetSPFOR_Filter_Listscreen(ddlFormName.SelectedValue, hf_ColName.Value, hf_ColValue.Value.ToString().ToUpper());
                }
                else
                {
                    strlist = FIN.DAL.FINSP.GetSPFOR_Listscreen(ddlFormName.SelectedValue.ToString(), false, true);
                }
                strlist = strlist.Replace("'MODIFYDELETE'", hf_MURL.Value + "," + hf_DURL.Value);
                dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];
                Session["QueryFormData"] = dtData;
                Session["FullQueryData"] = dtData;
                gridData.Columns.Clear();

                gridData = ClsGridBase.SetupReportGrid(gridData, dtData, ddlFormName.SelectedValue);
                DataTable dt_tmp = (DataTable)gridData.DataSource;
                for (int iLoop = 0; iLoop < gridData.Columns.Count; iLoop++)
                {
                    dt_tmp.Columns[iLoop].ColumnName = gridData.Columns[iLoop].HeaderText;
                }
                Session[FINSessionConstants.GridData] = dt_tmp;
                TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
                txt_tmp.Text = hf_ColValue.Value;
                txt_tmp.Focus();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ReportView", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Clear();
                    ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlModuelName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Lookup_BLL.GetMenuname(ref ddlFormName, ddlModuelName.SelectedValue.ToString(), Session[FINSessionConstants.Sel_Lng].ToString());
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlReportName.SelectedValue.ToString().Length == 0)
                {
                    ErrorCollection.Add("SelectForm", "Please Select FormName");
                    return;
                }
                modifyURL = " '#' as modifyURL";
                deleteURL = " '#' as deleteURL";
                hf_MURL.Value = modifyURL;
                hf_DURL.Value = deleteURL;
                FN_GLListScreen(ddlReportName.SelectedValue);
                hf_ScreenCode.Value = ddlFormName.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ReportViewClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void ClearGrid()
        {
            DataTable dtData = new DataTable();
            if (Session["dtData"] != null)
            {
                dtData = (DataTable)Session["dtData"];
            }
            dtData.Rows.Clear();
            gridData.DataSource = dtData;
            gridData.DataBind();
        }



        protected void imgbtnPDF_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    ImageButton img_btn_tmp = (ImageButton)sender;
                    Response.Redirect("../ExportGridData.aspx?ExportId=" + img_btn_tmp.CommandName, true);
                }
                else
                {
                    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlFormName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearGrid();
        }

    }
}