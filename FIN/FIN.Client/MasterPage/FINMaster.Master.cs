﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.BLL;
using System.Data;
using FIN.DAL;

namespace FIN.Client.MasterPage
{
    public partial class FINMaster : System.Web.UI.MasterPage
    {
        string _KeyName = string.Empty;

        public void RegisterPostbackTrigger(Control trigger)
        {
            ScriptManager1.RegisterPostBackControl(trigger);
        }

        public int ProgramID
        {
            get { return int.Parse(Session["ProgramID"].ToString()); }
            set { Session["ProgramID"] = value; }
        }

        public int RecordID
        {
            get { return int.Parse(Session["RecordID"].ToString()); }
            set
            {
                Session["RecordID"] = value;
                VMVServices.Web.Utils.RecordId = value;

            }
        }
        public string ReportName
        {
            get { return (Session["ReportName"].ToString()); }
            set { Session["ReportName"] = value; }
        }
        public string LC
        {
            get { return (Session["LC"].ToString()); }
            set { Session["LC"] = value; }
        }
        public string StrRecordId
        {
            get { return Session["StrRecordId"].ToString(); }
            set
            {
                Session["StrRecordId"] = value;
                VMVServices.Web.Utils.StrRecordId = value;
            }
        }
        public string Mode
        {
            get { return Session["Mode"].ToString(); }
            set
            {
                Session["Mode"] = value;
                VMVServices.Web.Utils.Mode = value;
            }
        }

        public string ListPageToOpen
        {
            get { return Session["ListPageToOpen"].ToString(); }
            set { Session["ListPageToOpen"] = value; }
        }
        public string KeyName
        {
            get { return _KeyName; }
            set { _KeyName = value; }
        }

        public string FormCode
        {
            get { return Session["FormCode"].ToString(); }
            set
            {
                Session["FormCode"] = value;

                VMVServices.Web.Utils.FormCode = value;
            }
        }

        public GridView SetupGridnew(GridView gridData, System.Data.DataTable dt, string acceName, string scrnCode = "")
        {
            return ClsGridBase.SetupGridnew(gridData, dt, this.KeyName, ProgramID, scrnCode);
        }
        public string LoadMenuNew(int progID, string addURL, string searchURL)
        {
            return ClsGridBase.BuildMenunew(progID, addURL, searchURL);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            div_Message.Visible = false;
            if (HttpContext.Current.Session["OrganizationID"] == null || HttpContext.Current.Session["UserName"] == null || HttpContext.Current.Session["Multilanguage"] == null)
            {
                Response.Redirect("../SessionTimeOut.aspx", false);
            }
            //  ScriptManager.RegisterStartupScript(div_Message, typeof(Button), "openfullscreen", "window.close();", true);
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }

            if (Session["FormCode"] != null && Session["UserName"] != null && Session["StrRecordId"] != null && Session["Mode"] != null)
            {
                VMVServices.Web.Utils.RecordID = "0";
                Session["AppRecordId"] = "0";
                if (Session["FormCode"].ToString().Length > 0 && Session["UserName"].ToString().Length > 0 && Session["StrRecordId"].ToString() != "0" && Session["Mode"].ToString() == FINAppConstants.Update)
                {
                    FINSP.Get_SP_LOG_CHANGES(Session["FormCode"].ToString(), HttpContext.Current.Session["UserName"].ToString(), Session["StrRecordId"].ToString());
                    VMVServices.Web.Utils.RecordID = "0";
                    Session["AppRecordId"] = "0";
                }
            }
            else if (Session["FormCode"] != null && Session["UserName"] != null && Session["RecordID"] != null && Session["Mode"] != null)
            {
                VMVServices.Web.Utils.StrRecordId = "0";
                Session["AppStrRecordId"] = "0";

                if (Session["FormCode"].ToString().Length > 0 && Session["UserName"].ToString().Length > 0 && int.Parse(Session["RecordID"].ToString()) > 0 && Session["Mode"].ToString() == FINAppConstants.Update)
                {
                    FINSP.Get_SP_LOG_CHANGES(Session["FormCode"].ToString(), HttpContext.Current.Session["UserName"].ToString(), Session["RecordID"].ToString());
                    VMVServices.Web.Utils.StrRecordId = "0";
                    Session["AppStrRecordId"] = "0";
                }
            }
            hf_Sel_Lng.Value = Session["Sel_Lng"].ToString();

            if (Request.QueryString.ToString().Contains("MODULENAME"))
            {
                Session[FINSessionConstants.ModuleName] = Request.QueryString["MODULENAME"].ToString();
            }
            if (!IsPostBack)
            {
                Session["ProgramName"] = "";
                imgMsgTypeIcon.ImageUrl = "~/Images/RightMark.png";
                if (Request.QueryString.ToString().Contains("LinkType"))
                {

                    div_Message.Style.Add("background-color", "rgb(126,182,46)");
                    string str_Collection = "<table width='100%' class='msg' > <tr> <td widht='95%'> ";

                    if (Session["Sel_Lng"].ToString() == "AR")
                    {
                        str_Collection += "البيانات المحفوظة بنجاح ";
                    }
                    else
                    {
                        str_Collection += "Data Saved Successfully ";
                    }

                    str_Collection += "</td>";
                    str_Collection += "</tr></table>";

                    div_Message_Cont.InnerHtml = str_Collection;

                    div_Message.Visible = true;
                }
                else if (Request.QueryString.ToString().Contains("RecordMsgType"))
                {
                    div_Message.Style.Add("background-color", "rgb(126,182,46)");
                    string str_Collection = "<table width='100%' class='msg' > <tr> <td widht='95%'>";

                    string str_msg = "";
                    if (Session["Sel_Lng"].ToString() == "AR")
                    {
                        str_msg = "البيانات المحفوظة بنجاح. ";
                    }
                    else
                    {
                        str_msg = "Data Saved Successfully. ";
                    }

                    if (VMVServices.Web.Utils.RecordMsg != string.Empty && VMVServices.Web.Utils.RecordMsg != null)
                    {
                        str_Collection += str_msg + VMVServices.Web.Utils.RecordMsg;
                    }
                    else
                    {
                        str_Collection += str_msg;
                    }
                    str_Collection += "</td>";
                    str_Collection += "</tr></table>";

                    VMVServices.Web.Utils.RecordMsg = null;
                    VMVServices.Web.Utils.RecordMsg = string.Empty;

                    div_Message_Cont.InnerHtml = str_Collection;

                    div_Message.Visible = true;
                }

            }
            hf_Chosen_txt_Single.Value = "Select an Option";
            hf_Chosen_txt_NOR.Value = "No results matching";
            if (Session["Sel_Lng"] != null)
            {
                if (Session["Sel_Lng"].ToString() == "AR")
                {
                    hf_Chosen_txt_Single.Value = "حدد الخيار";
                    hf_Chosen_txt_NOR.Value = "لا نتائج مطابقة";

                    Page.ClientScript.RegisterClientScriptInclude("validationLN", "../Scripts/JQueryValidatioin/jquery.validationEngine_AR.js?1.2");
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "changeCSS", "document.getElementById('MainCss').href='../Styles/Main_R.css?1.1';document.getElementById('ChosenCss').href='../Scripts/DDLChoosen/chosen_R.css';", true);
                    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "changeChosenCSS", "document.getElementById('ChosenCss').href='../Styles/chosen_R.css';", true);
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "changeCSS", "document.getElementById('MainCss').href='../Styles/main.css';", true);
                //}
            }
        }

        public void ShowMessage(System.Collections.SortedList SL_Col, string str_MessageType, Boolean EnabledFlag = false)
        {


            div_Message.Visible = true;
            if (str_MessageType == FIN.BLL.FINAppConstants.ERROR)
            {
                imgMsgTypeIcon.ImageUrl = "~/Images/alert-icon.png";
                div_Message.Style.Add("background-color", "rgb(251,158,12)");
                if (SL_Col.Count > 0)
                {
                    if (str_MessageType == FIN.BLL.FINAppConstants.ERROR)
                    {
                        string str_Collection = "<Left class='msg'><Blink>";//<div style='float:right' > <img class='msgClose' onclick='fn_closeerror()' alt='Close' /> </div><Left class='msg'><Blink>";
                        System.Collections.IDictionaryEnumerator myEnumerator = SL_Col.GetEnumerator();
                        while (myEnumerator.MoveNext())
                        {
                            str_Collection += myEnumerator.Value.ToString() + "<BR>";
                        }

                        str_Collection += "</blink></Left>";
                        div_Message_Cont.InnerHtml = str_Collection;

                    }
                }
                else
                {
                    div_Message_Cont.InnerHtml = "Critical Error Found";
                }
            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.SAVED)
            {
                imgMsgTypeIcon.ImageUrl = "~/Images/RightMark.png";
                div_Message.Style.Add("background-color", "rgb(126,182,46)");

                div_Message.Style.Add("background-color", "rgb(126,182,46)");
                string str_Collection = "<table width='100%' class='msg' > <tr> <td widht='95%'> ";


                if (Session["Sel_Lng"].ToString() == "AR")
                {
                    str_Collection += "البيانات المحفوظة بنجاح ";
                }
                else
                {
                    str_Collection += "Data Saved Successfully ";
                }

                str_Collection += "</td>";
                str_Collection += "</tr></table>";

                VMVServices.Web.Utils.RecordMsg = null;
                VMVServices.Web.Utils.RecordMsg = string.Empty;

                div_Message_Cont.InnerHtml = str_Collection;

                div_Message.Visible = true;

            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.POSTED)
            {
                imgMsgTypeIcon.ImageUrl = "~/Images/RightMark.png";
                div_Message.Style.Add("background-color", "rgb(126,182,46)");

                div_Message.Style.Add("background-color", "rgb(126,182,46)");
                string str_Collection = "<table width='100%' class='msg' > <tr> <td widht='95%'>";




                if (Session["Sel_Lng"].ToString() == "AR")
                {
                    str_Collection += "البيانات أرسلت بنجاح ";
                }
                else
                {
                    str_Collection += "Data Posted Successfully ";
                }

                str_Collection += "</td>";
                str_Collection += "</tr></table>";

                VMVServices.Web.Utils.RecordMsg = null;
                VMVServices.Web.Utils.RecordMsg = string.Empty;

                div_Message_Cont.InnerHtml = str_Collection;

                div_Message.Visible = true;

            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.DATAIMPORT)
            {
                imgMsgTypeIcon.ImageUrl = "~/Images/RightMark.png";
                div_Message.Style.Add("background-color", "rgb(126,182,46)");

                div_Message.Style.Add("background-color", "rgb(126,182,46)");
                string str_Collection = "<table width='100%' class='msg' > <tr> <td widht='95%'> ";



                if (Session["Sel_Lng"].ToString() == "AR")
                {
                    str_Collection += "البيانات المستوردة بنجاح ";
                }
                else
                {
                    str_Collection += "Data Imported Successfully ";
                }

                str_Collection += "</td>";
                str_Collection += "</tr></table>";

                VMVServices.Web.Utils.RecordMsg = null;
                VMVServices.Web.Utils.RecordMsg = string.Empty;

                div_Message_Cont.InnerHtml = str_Collection;

                div_Message.Visible = true;

            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.DATASAVED)
            {

                //RecordID = 0;
                //StrRecordId = "0";
                //Mode = FIN.BLL.FINAppConstants.Add;
                //string str_Collection = "<marquee  behavior='alternate' scrolldelay='100'>";
                //str_Collection += "Data Saved Successfully ";
                //str_Collection += "</marquee>";

                //div_Message.InnerHtml = str_Collection;


                //ContentPlaceHolder cph = (ContentPlaceHolder)this.FindControl("FINContent");
                //TextBox txt_tmp;
                //DropDownList drp;
                //foreach (Control c in cph.Controls)
                //{
                //    if (c.GetType() == typeof(TextBox))
                //    {
                //        txt_tmp = (TextBox)c;
                //        txt_tmp.Text = "";
                //    }
                //    if (c.GetType() == typeof(DropDownList))
                //    {
                //        drp = (DropDownList)c;
                //        drp.SelectedIndex = 0;
                //    }
                //    if (c.GetType() == typeof(GridView))
                //    {
                //        System.Data.DataTable dt_GV = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.GridData];
                //        GridView tmp_gv = (GridView)c;
                //        dt_GV.Rows.Clear();
                //        Session[FIN.BLL.FINSessionConstants.GridData] = dt_GV;
                //        System.Data.DataTable dt_tmp = dt_GV.Copy();
                //        if (dt_tmp.Rows.Count == 0)
                //        {
                //            System.Data.DataRow dr = dt_tmp.NewRow();
                //            dr[0] = "0";
                //            if (EnabledFlag)
                //            {
                //                dr["ENABLED_FLAG"] = "FALSE";
                //            }
                //            dt_tmp.Rows.Add(dr);
                //        }
                //        tmp_gv.DataSource = dt_tmp;
                //        tmp_gv.DataBind();
                //        tmp_gv.Rows[0].Visible = false;

                //    }

                //}
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + ProgramID);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                if (VMVServices.Web.Utils.SavedRecordId.ToString().Length > 0)
                {
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Update.ToString().Substring(0, 1) + "&";
                    str_LinkURL += QueryStringTags.ID.ToString() + "=" + VMVServices.Web.Utils.SavedRecordId + "&";
                }
                else
                {
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                    str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                }
                //str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName_OL.ToString() + "=" + dr_list[0]["ATTRIBUTE3"].ToString().Trim() + "&";
                str_LinkURL += "LinkType=Saved";
                Response.Redirect(str_LinkURL);

            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.DATAPROCESSED)
            {
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + ProgramID);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";
                str_LinkURL += "LinkType=Processed";
                Response.Redirect(str_LinkURL);

            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.DATADELETED)
            {
                string str_Collection = "<marquee  scrollamount='200' scrolldelay='500'>";
                str_Collection += "Data Deleted Successfully ";
                str_Collection += "</marquee>";

                div_Message_Cont.InnerHtml = str_Collection;

                RecordID = 0;
                Mode = FIN.BLL.FINAppConstants.Add;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "RestForm", "$('#form1)[0].reset()", true);
            }
            else if (str_MessageType == FIN.BLL.FINAppConstants.RECORDMSG)
            {
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + ProgramID);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";
                str_LinkURL += "RecordMsgType=Saved";
                Response.Redirect(str_LinkURL);
            }
            VMVServices.Web.PageBase.ErrorCollection.Clear();

        }



        protected void btnClose_Click1(object sender, ImageClickEventArgs e)
        {
            div_Message_Cont.InnerHtml = "";
            div_Message.Visible = false;
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Session["QUERY_FORM"] = FINAppConstants.N;
        }
    }
}