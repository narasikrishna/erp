﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.LOAN
{
    public partial class LoanRepayPlanParam : PageBase
    {
        LN_REPAY_HDR lN_REPAY_HDR = new LN_REPAY_HDR();
        LN_REPAY_DTL lN_REPAY_DTL = new LN_REPAY_DTL();
        DataTable dtGridData = new DataTable();
        string career_path_type;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.LOAN.PropertyRevaluationBLL.GetRequestID(ref ddlLoanProperty);
            //FIN.BLL.LOAN.BankChargesBLL.GetContract(ref ddlContractNumber);
            Lookup_BLL.GetLookUpValues(ref ddlRepayType, "REPAY_TYPE");

            //FIN.BLL.AP.InventoryPriceList_BLL.GetPriceListCurrency(ref ddlPriceListCurrency);
            //Lookup_BLL.GetLookUpValues(ref ddlPricelistType, "PRICELIST_TYPE");
            //FIN.BLL.AP.Supplier_BLL.GetSupplierName(ref ddlsupplier);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = FIN.BLL.LOAN.LoanRepayPlan_BLL.getLoanRepayPlanDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<LN_REPAY_HDR> userCtx = new DataRepository<LN_REPAY_HDR>())
                    {
                        lN_REPAY_HDR = userCtx.Find(r =>
                            (r.LN_PRN_REPAY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = lN_REPAY_HDR;

                    txtLoanRepayId.Text = lN_REPAY_HDR.LN_PRN_REPAY_ID;
                    ddlLoanProperty.SelectedValue = lN_REPAY_HDR.LN_REQUEST_ID;
                    GetContractName();
                    ddlContractNumber.SelectedValue = lN_REPAY_HDR.LN_CONTRACT_ID;

                    txtLoanAmt.Text = lN_REPAY_HDR.LN_LOAN_AMOUNT.ToString();
                    txtLoanPaidAmt.Text = lN_REPAY_HDR.LN_PAID_AMOUNT.ToString();
                    ddlRepayType.SelectedValue = lN_REPAY_HDR.LN_REPAY_TYPE;
                    fillRepayType();
                    ddlRepayPeriodType.SelectedValue = lN_REPAY_HDR.LN_PAY_PERIOD_TYPE;





                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_AMOUNT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PS_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PS_AMOUNT"))));                   
                    dtData.AcceptChanges();
                }


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = false;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void txtItemCost_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            TextBox txtItemCost = (TextBox)gvr.FindControl("txtItemCost");
            if (CommonUtils.ConvertStringToDecimal(txtItemCost.Text) > 0)
            {
                txtItemCost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtItemCost.Text);
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                //ErrorCollection.Clear();
                //DropDownList ddlItemCode = tmpgvr.FindControl("ddlItemCode") as DropDownList;
                //FIN.BLL.AP.InventoryPriceList_BLL.GetItemCode(ref ddlItemCode);

                ////Label lblItemCost = tmpgvr.FindControl("lblItemCost") as Label;
                ////if (CommonUtils.ConvertStringToDecimal(lblItemCost.Text) > 0)
                ////{
                ////    lblItemCost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(lblItemCost.Text);
                ////}

                //if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                //{
                //    ddlItemCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ITEM_ID"].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlPriceListCurrency;
                //slControls[1] = ddlPricelistType;
                ////slControls[2] = ddlCareerPathName;


                //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                //string strMessage = "PriceList Currency ~ PriceList Type  ";

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Price List ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    //txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    lN_REPAY_HDR = (LN_REPAY_HDR)EntityData;
                }

                lN_REPAY_HDR.LN_PRN_REPAY_ID = txtLoanRepayId.Text;
                lN_REPAY_HDR.LN_REQUEST_ID = ddlLoanProperty.SelectedValue;
                lN_REPAY_HDR.LN_CONTRACT_ID = ddlContractNumber.SelectedValue;
                lN_REPAY_HDR.LN_LOAN_AMOUNT = CommonUtils.ConvertStringToInt(txtLoanAmt.Text);
                lN_REPAY_HDR.LN_PAID_AMOUNT = CommonUtils.ConvertStringToInt(txtLoanPaidAmt.Text);
                lN_REPAY_HDR.LN_REPAY_TYPE = ddlRepayType.SelectedValue;
                lN_REPAY_HDR.LN_PAY_PERIOD_TYPE = ddlRepayPeriodType.SelectedValue;


                lN_REPAY_HDR.LN_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                lN_REPAY_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_REPAY_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_REPAY_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    lN_REPAY_HDR.LN_PRN_REPAY_ID = FINSP.GetSPFOR_SEQCode("AP_023_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    lN_REPAY_HDR.CREATED_BY = this.LoggedUserName;
                    lN_REPAY_HDR.CREATED_DATE = DateTime.Today;
                }

                lN_REPAY_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_REPAY_HDR.LN_PRN_REPAY_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    lN_REPAY_DTL = new LN_REPAY_DTL();
                    if (dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<LN_REPAY_DTL> userCtx = new DataRepository<LN_REPAY_DTL>())
                        {
                            lN_REPAY_DTL = userCtx.Find(r =>
                                (r.LN_PRN_REPAY_DTL_ID == dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    lN_REPAY_DTL.LN_INSTALLMENT_NO = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_INSTALLMENT_NO"].ToString());
                    if (dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"] != DBNull.Value)
                    {
                        lN_REPAY_DTL.LN_INSTALLMENT_DT = DateTime.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"].ToString());
                        lN_REPAY_DTL.LN_PS_DT = DateTime.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"].ToString());
                    }
                    lN_REPAY_DTL.LN_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_AMOUNT"].ToString());
                    lN_REPAY_DTL.LN_PS_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_PS_AMOUNT"].ToString());
                    lN_REPAY_DTL.LN_REVISED_PS_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_PS_AMOUNT"].ToString());
                   


                    lN_REPAY_DTL.LN_PRN_REPAY_ID = lN_REPAY_HDR.LN_PRN_REPAY_ID;
                    lN_REPAY_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    //iNV_PRICE_LIST_DTL.ENABLED_FLAG = "1";
                    lN_REPAY_DTL.ENABLED_FLAG = (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == "TRUE" ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag);


                    if (dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString() != string.Empty)
                    {
                        lN_REPAY_DTL.LN_PRN_REPAY_DTL_ID = dtGridData.Rows[iLoop]["LN_PRN_REPAY_DTL_ID"].ToString();
                        lN_REPAY_DTL.MODIFIED_BY = this.LoggedUserName;
                        lN_REPAY_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(lN_REPAY_DTL, "U"));
                    }
                    else
                    {
                        lN_REPAY_DTL.LN_PRN_REPAY_DTL_ID = FINSP.GetSPFOR_SEQCode("AP_023_D".ToString(), false, true);
                        lN_REPAY_DTL.CREATED_BY = this.LoggedUserName;
                        lN_REPAY_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(lN_REPAY_DTL, "A"));
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<LN_REPAY_HDR, LN_REPAY_DTL>(lN_REPAY_HDR, tmpChildEntity, lN_REPAY_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<LN_REPAY_HDR, LN_REPAY_DTL>(lN_REPAY_HDR, tmpChildEntity, lN_REPAY_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }




        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<LN_REPAY_DTL>(lN_REPAY_DTL);
                }
                DBMethod.DeleteEntity<LN_REPAY_HDR>(lN_REPAY_HDR);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();




            TextBox txtInstallmentNo = gvr.FindControl("txtInstallmentNo") as TextBox;
            TextBox txtInstallmentDate = gvr.FindControl("txtInstallmentDate") as TextBox;
            TextBox txtInstAmt = gvr.FindControl("txtInstAmt") as TextBox;
            TextBox txtProfitShowAmt = gvr.FindControl("txtProfitShowAmt") as TextBox;
            //TextBox txtReqValue = gvr.FindControl("txtReqValue") as TextBox;
            CheckBox chkGridActive = gvr.FindControl("chkGridActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LN_PRN_REPAY_DTL_ID"] = "0";
                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = txtInstallmentNo;
            slControls[1] = txtInstallmentDate;
            slControls[2] = txtInstAmt;
            slControls[3] = txtProfitShowAmt;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~DateTime~TextBox~TextBox";
            // string strMessage = Prop_File_Data["Item_Code_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Item_UOM_P"] + " ~ " + Prop_File_Data["Item_Cost_P"] + "";
            string strMessage = " Installment Number ~ Installment Date ~ Installment Amount ~ Profit Show Amount";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }




            drList["LN_INSTALLMENT_NO"] = txtInstallmentNo.Text;
            drList["LN_INSTALLMENT_DT"] = txtInstallmentDate.Text;

            drList["LN_AMOUNT"] = txtInstAmt.Text;

            drList["LN_PS_AMOUNT"] = txtProfitShowAmt.Text;
            //drList["PRICE_ITEM_COST"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(txtItemCost.Text).ToString());
            if (chkGridActive.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }





        protected void ddlItemCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //GridViewRow gvr = gvData.FooterRow;

                //DropDownList ddlItemCode = (DropDownList)gvr.FindControl("ddlItemCode");
                //TextBox txtItemName = (TextBox)gvr.FindControl("txtItemName");
                //TextBox txtItemUOM = (TextBox)gvr.FindControl("txtItemUOM");
                //TextBox txtItemCost = (TextBox)gvr.FindControl("txtItemCost");

                //INV_ITEM_MASTER iNV_ITEM_MASTER = new INV_ITEM_MASTER();

                //using (IRepository<INV_ITEM_MASTER> ssm = new DataRepository<INV_ITEM_MASTER>())
                //{
                //    iNV_ITEM_MASTER = ssm.Find(x => (x.ITEM_ID == ddlItemCode.SelectedValue.ToString())).SingleOrDefault();
                //    if (iNV_ITEM_MASTER != null)
                //    {
                //        Session["Item Code"] = iNV_ITEM_MASTER.ITEM_CODE;
                //    }
                //}
                //DataTable dt_std_rate = new DataTable();
                //dt_std_rate = FIN.BLL.AP.InventoryPriceList_BLL.getItemDtls(ddlItemCode.SelectedValue);
                //if (dt_std_rate != null)
                //{
                //    if (dt_std_rate.Rows.Count > 0)
                //    {
                //        txtItemName.Text = dt_std_rate.Rows[0]["Item_Name"].ToString();
                //        txtItemUOM.Text = dt_std_rate.Rows[0]["uom_desc"].ToString();
                //        txtItemCost.Text = dt_std_rate.Rows[0]["ITEM_UNIT_PRICE"].ToString();
                //    }

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlRepayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillRepayType();
        }
        private void fillRepayType()
        {
            if (ddlRepayType.SelectedValue == "EQUAL")
            {
                ddlRepayPeriodType.Enabled = true;
                Lookup_BLL.GetLookUpValues(ref ddlRepayPeriodType, "PY_PRD_TYPE");
                btnProcess.Visible = true;
            }
            else
            {
                ddlRepayPeriodType.Enabled = false;
                btnProcess.Visible = false;
            }
        }

        protected void ddlLoanProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetContractName();
        }
        private void GetContractName()
        {
            FIN.BLL.HR.LoanRequest_BLL.GetLoanRequestContractNumber(ref ddlContractNumber, ddlLoanProperty.SelectedValue.ToString());
        }

        protected void ddlContractNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLoanAmount();
        }
        private void GetLoanAmount()
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails(ddlLoanProperty.SelectedValue, ddlContractNumber.SelectedValue)).Tables[0];
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                txtLoanAmt.Text = dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString();
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails(ddlLoanProperty.SelectedValue, ddlContractNumber.SelectedValue)).Tables[0];
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                DateTime compareTo = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_end_date"].ToString());
                DateTime now = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_start_date"].ToString());
                var dateSpan = DateTimeSpan.CompareDates(compareTo, now);
                double dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString());
                double dbl_PSPercentage = double.Parse(dt_LoanReqDtl.Rows[0]["ln_roi"].ToString());
                int int_Tot_Months = dateSpan.Months+1;
                int int_calmonth = 0;
                int int_CalMonth = 0;
                if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "MONTHLY")
                {
                    int_CalMonth = 1;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "QUARTERLY")
                {
                    int_CalMonth = 3;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "HALFYEARLY")
                {
                    int_CalMonth = 6;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "ANNUAL")
                {
                    int_CalMonth = 12;
                }

                int_calmonth = int_Tot_Months / int_CalMonth;

                double dbl_totLoanAmt = double.Parse(txtLoanAmt.Text);
                double dbl_MonthAmt = dbl_totLoanAmt / int_calmonth;


                for (int iLoop = 0; iLoop < int_calmonth; iLoop++)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                    DataRow drList;
                    drList = dtGridData.NewRow();
                    drList["LN_PRN_REPAY_DTL_ID"] = "0";

                    drList["LN_INSTALLMENT_NO"] = iLoop + 1;
                    drList["LN_INSTALLMENT_DT"] = now;

                    drList["LN_AMOUNT"] = dbl_MonthAmt.ToString();

                    drList["LN_PS_AMOUNT"] = ((dbl_LoanAmount * dbl_PSPercentage) / 100).ToString();

                    dbl_LoanAmount = dbl_LoanAmount - (dbl_LoanAmount * dbl_PSPercentage) / 100;

                    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";

                    drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                    dtGridData.Rows.Add(drList);
                    now = now.AddMonths(int_CalMonth);
                }

                BindGrid(dtGridData);


            }

            /*
            Console.WriteLine("Years: " + dateSpan.Years);
            Console.WriteLine("Months: " + dateSpan.Months);
            Console.WriteLine("Days: " + dateSpan.Days);
            Console.WriteLine("Hours: " + dateSpan.Hours);
            Console.WriteLine("Minutes: " + dateSpan.Minutes);
            Console.WriteLine("Seconds: " + dateSpan.Seconds);
            Console.WriteLine("Milliseconds: " + dateSpan.Milliseconds);
             * */
        }



    }


    public struct DateTimeSpan
    {
        private readonly int years;
        private readonly int months;
        private readonly int days;
        private readonly int hours;
        private readonly int minutes;
        private readonly int seconds;
        private readonly int milliseconds;

        public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
        {
            this.years = years;
            this.months = months;
            this.days = days;
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
            this.milliseconds = milliseconds;
        }

        public int Years { get { return years; } }
        public int Months { get { return months; } }
        public int Days { get { return days; } }
        public int Hours { get { return hours; } }
        public int Minutes { get { return minutes; } }
        public int Seconds { get { return seconds; } }
        public int Milliseconds { get { return milliseconds; } }

        enum Phase { Years, Months, Days, Done }

        public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
        {
            if (date2 < date1)
            {
                var sub = date1;
                date1 = date2;
                date2 = sub;
            }

            DateTime current = date1;
            int years = 0;
            int months = 0;
            int days = 0;

            Phase phase = Phase.Years;
            DateTimeSpan span = new DateTimeSpan();

            while (phase != Phase.Done)
            {
                switch (phase)
                {
                    case Phase.Years:
                        if (current.AddYears(years + 1) > date2)
                        {
                            phase = Phase.Months;
                            current = current.AddYears(years);
                        }
                        else
                        {
                            years++;
                        }
                        break;
                    case Phase.Months:
                        if (current.AddMonths(months + 1) > date2)
                        {
                            phase = Phase.Days;
                            current = current.AddMonths(months);
                        }
                        else
                        {
                            months++;
                        }
                        break;
                    case Phase.Days:
                        if (current.AddDays(days + 1) > date2)
                        {
                            current = current.AddDays(days);
                            var timespan = date2 - current;
                            span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                            phase = Phase.Done;
                        }
                        else
                        {
                            days++;
                        }
                        break;
                }
            }

            return span;
        }
    }
}


