﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanPropertyParam.aspx.cs" Inherits="FIN.Client.LOAN.LoanPropertyParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divRowContainer" style="display: none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestId">
                Property Id
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 240px">
                <asp:TextBox ID="txtPropertyId" CssClass="txtBox" runat="server" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Property Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 475px">
                <asp:DropDownList ID="ddlPropertyType" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPropertyType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="share" visible="false">
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                    Share Name
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtShareName" CssClass="validate[required] RequiredField txtBox"
                        runat="server" TabIndex="3"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div23">
                    Share Name (Arabic)
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtShareNameOL" CssClass="EntryFont txtBox_ol"
                        runat="server" TabIndex="4"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                    Security Identifier
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtSecurityIdentifier" CssClass="txtBox" runat="server" TabIndex="5"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                    Number of Shares
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtNoOfShares" CssClass=" validate[required] RequiredField txtBox_N"
                        runat="server" TabIndex="6"></asp:TextBox>
                </div>
                <div class="colspace  LNOrient" >
                    &nbsp</div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                    Value of Shares
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtValueOfShares" CssClass="txtBox_N" runat="server" TabIndex="7"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                    Certificate Number
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtCertificateNumber" CssClass="txtBox" runat="server" TabIndex="8"></asp:TextBox>
                </div>
                <div class="colspace  LNOrient" >
                    &nbsp</div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div7">
                    Folio Number
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFolioNo" CssClass="txtBox" runat="server" TabIndex="9"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div8">
                    From Number
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFromNumber" runat="server" TabIndex="10" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtFromNumber" />
                </div>
                <div class="colspace  LNOrient" >
                    &nbsp</div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                    To Number
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtToNumber" runat="server" TabIndex="11" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtToNumber" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="Propert" runat="server" visible="false">
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div10">
                    Property Name
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtPropertyName" CssClass="validate[required] RequiredField txtBox"
                        runat="server" TabIndex="12" Enabled="true"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div24">
                    Property Name (Arabic)
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtPropertyNameOL" CssClass="EntryFont txtBox_ol"
                        runat="server" TabIndex="13" Enabled="true"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div11">
                    Property Short Name
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtPropertyShortName" CssClass="validate[required] RequiredField txtBox"
                        runat="server" TabIndex="14"></asp:TextBox>
                </div>
                <div class="colspace  LNOrient" >
                    &nbsp</div>
                <div class="lblBox  LNOrient" style=" width: 150px; display: none" id="Div22">
                    Facility Type
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 155px; display: none">
                    <asp:DropDownList ID="ddlFaciltiyType" runat="server" AutoPostBack="true" TabIndex="15"
                        CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div12">
                    Address 1
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtAddress1" CssClass="validate[required] RequiredField txtBox"
                        Height="50px" runat="server" TabIndex="16" Enabled="true" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div18">
                    Address 2
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtAddress2" CssClass="txtBox" Height="50px" runat="server" TabIndex="17"
                        Enabled="true" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div19">
                    Address 3
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtAddress3" CssClass="txtBox" Height="50px" runat="server" TabIndex="18"
                        Enabled="true" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="display:none">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div13">
                    State
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" TabIndex="19"
                        CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                 <div class="lblBox  LNOrient" style=" width: 150px" id="Div114">
                    City
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 155px">
                    <asp:DropDownList ID="ddlCity1" runat="server" TabIndex="20" AutoPostBack="true" 
                        CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div124">
                    City
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 155px">
                    <asp:DropDownList ID="ddlCity" runat="server" TabIndex="20" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div15">
                    Postal Code
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtPostalCode" CssClass="txtBox" runat="server" TabIndex="21"></asp:TextBox>
                </div>
                <div class="colspace  LNOrient" >
                    &nbsp</div>
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div16">
                    Phone Number
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtPhoneNo" runat="server" TabIndex="22" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtPhoneNo" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div17">
                    Title deed Reference
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtTitledeedRef" CssClass="txtBox" runat="server" TabIndex="23"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 150px" id="Div20">
                    Contact Person
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 480px">
                    <asp:TextBox ID="txtContactPerson" CssClass="txtBox" runat="server" TabIndex="24"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px;" id="lblRequestDate">
                Effective From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtEffFromDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    TabIndex="25"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEffFromDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffFromDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px;" id="Div21">
                Effective To Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtEffToDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    TabIndex="26"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEffToDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="27"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="28" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="29"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="30" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
