﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanReqEntry.aspx.cs" Inherits="FIN.Client.HR.LoanReqEntry" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
       <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestId">
                Request Id
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtRequestId" CssClass="txtBox"  runat="server"
                    TabIndex="1" Enabled="true" ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px;" id="lblRequestDate">
               Request Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtReqDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtReqDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtReqDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblReqDescription">
                Request Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtRequestDescription" CssClass="validate[required] RequiredField txtBox" Height="50px"  runat="server"
                    TabIndex="3" Enabled="true" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblCurrency">
               Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblLoanAmount">
               Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLoanAmount" CssClass="txtBox_N" MaxLength="10" runat="server"
                    TabIndex="5" Enabled="true"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestType">
               Request Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlRequestType" runat="server" TabIndex="6" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlRequestType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
     
           <div class="divClear_10">
        </div>
         <div class="divRowContainer" runat="server" visible="false" id="Share1">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblShareName">
                Share Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtShareName" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="7" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblNoofShares">
               No. of Shares
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtNoofshares" CssClass="validate[required] RequiredField txtBox_N" MaxLength="10" runat="server"
                    TabIndex="8" Enabled="true"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
             <div class="divRowContainer" runat="server" visible="false" id="Share2">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSecurityIdentification">
                Security Identification
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtSecIdentification" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="9" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblValueofShares">
              Value of Shares
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtValueofShares" CssClass="validate[required] RequiredField txtBox_N" MaxLength="10" runat="server"
                    TabIndex="10" Enabled="true"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
            <div class="divRowContainer" runat="server" visible="false" id="Share3">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblCertificateNumber">
               Certificate Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCertificateNumber" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="11" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblFolioNumber">
                Folio Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFolioNo" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="12" Enabled="true"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
             <div class="divRowContainer" runat="server" visible="false" id="Share4">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblFromNumber">
               From Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromNo" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="13" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblToNumber">
                To Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToNumber" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="14" Enabled="true"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
           <div class="divRowContainer" runat="server" visible="false" id="Property1">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyName">
                Property Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPropertyName" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="15" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyId">
                Property Id
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPropertyId" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="16" Enabled="true"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
            <div class="divRowContainer" runat="server" visible="false" id="Property2">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblTitleDeedRef">
               Title Deed Reference
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtTitleDeedRef" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="17" Enabled="true"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox  LNOrient" style=" width: 150px" id="lblPropertyValue">
                Property Value
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPropertyValue" CssClass="validate[required] RequiredField txtBox" MaxLength="10" runat="server"
                    TabIndex="18" Enabled="true"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="Property3">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblLocations">
               Locations
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlLocations" runat="server" TabIndex="19" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIden1">
                Additional Identifier1
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtAdditionalIden1" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="20" Enabled="true"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIden2">
                Additional Identifier2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtAdditionalIden2" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="21" Enabled="true"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIden3">
               Additional Identifier3
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtAdditionalIden3" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="22" Enabled="true"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIden4">
                 Additional Identifier4
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtAdditionalIden4" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="23" Enabled="true"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIden5">
                 Additional Identifier5
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 480px">
                <asp:TextBox ID="txtAdditionalIden5" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="24" Enabled="true"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
          <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblEnabledFlag">
                Enabled Flag
            </div>
            <div class="divChkbox" style=" width: 150px">
                <asp:CheckBox ID="ChkEnabledFlag" runat="server" Checked="True" TabIndex="25" />
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="26" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="28"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
  
</asp:Content>--%>
