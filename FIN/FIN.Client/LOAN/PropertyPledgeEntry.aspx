﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PropertyPledgeEntry.aspx.cs" Inherits="FIN.Client.LOAN.PropertyPledgeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divRowContainer" style="display:none" >
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestId">
                Pledge ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPledgeID" CssClass="txtBox" runat="server" TabIndex="1" Enabled="false"
                    MaxLength="50" ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlBank" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" Width="350px" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Property
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlProperty" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    Width="350px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px;" id="lblRequestDate">
                Pledge Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtPledgeDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  RequiredField  txtBox"
                    TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtPledgeDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPledgeDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px;" id="Div3">
                Release Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtReleaseDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtReleaseDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtReleaseDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px;" id="Div4">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:TextBox runat="server" ID="txtRemrks" CssClass="txtBox" MaxLength="200" TabIndex="6"
                    Height="50px" Width="350px" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblReqDescription">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox runat="server" ID="chkActive" Checked="true" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="26"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="28"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
  
</asp:Content>--%>
