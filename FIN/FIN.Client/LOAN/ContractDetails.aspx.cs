﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.LOAN
{
    public partial class LoanRequestEntry : PageBase
    {
        LN_LOAN_REQUEST_HDR lN_LOAN_REQUEST_HDR = new LN_LOAN_REQUEST_HDR();
        LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();
        LN_LOAN_REQUEST_TERMS lN_LOAN_REQUEST_TERMS = new LN_LOAN_REQUEST_TERMS();
        

        DataTable dtGridData = new DataTable();
        DataTable dtLotGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        int rowIndexVal = 1;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session[FINSessionConstants.LotGridData] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        #endregion Pageload
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void FillComboBox()
        {
            FIN.BLL.LOAN.Facility_BLL.fn_GetFacilityName(ref ddlFacilityname);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session[FINSessionConstants.LotGridData] = null;

                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = null;
               
                Session["GridData"] = null;
                Session["rowindex"] = null;


                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                //dtLotGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(Master.StrRecordId)).Tables[0];
                //BindLotGrid(dtLotGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_REQUEST_HDR = LoanRequest_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = lN_LOAN_REQUEST_HDR;

                    if (lN_LOAN_REQUEST_HDR.LN_FACILITY_ID != null)
                    {
                        ddlFacilityname.SelectedValue = lN_LOAN_REQUEST_HDR.LN_FACILITY_ID.ToString();
                        getPropertyName();                      
                    }
                    if (lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID != null)
                    {
                        ddlPropertyID.SelectedValue = lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID.ToString();
                    }
                    if (lN_LOAN_REQUEST_HDR.LN_PURPOSE != null)
                    {
                        txtLoanPurpose.Text = lN_LOAN_REQUEST_HDR.LN_PURPOSE;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData"] = dtData;

                ////Changes the below calcuation to AssignToGridControl
                //double principalAmt = 0;
                //Session["principalAmount"] = string.Empty;
                //if (dtData.Rows.Count > 0)
                //{
                //    for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                //    {
                //        if (dtData.Rows[iLoop]["LN_PRINCIPAL_AMOUNT"].ToString().Length > 0)
                //        {
                //            principalAmt += double.Parse(dtData.Rows[iLoop]["LN_PRINCIPAL_AMOUNT"].ToString());
                //            if (Session["facilityAmount"] != null)
                //            {
                //                if (principalAmt > double.Parse(Session["facilityAmount"].ToString()))
                //                {
                //                    ErrorCollection.Add("Amt Exceeds", "Principal Amount exceeds Facility Amount");
                //                }
                //            }
                //        }
                //    }
                //    Session["principalAmount"] = principalAmt;
                //}

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PRINCIPAL_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PRINCIPAL_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlBankName = tmpgvr.FindControl("ddlBankName") as DropDownList;
                DropDownList ddlCurrency = tmpgvr.FindControl("ddlCurrency") as DropDownList;
                DropDownList ddlContractStatus = tmpgvr.FindControl("ddlContractStatus") as DropDownList;
                DropDownList ddl_ContractType = tmpgvr.FindControl("ddlContractType") as DropDownList;

                DropDownList ddlDebitAcctCode = tmpgvr.FindControl("ddlDebitAcctCode") as DropDownList;
                DropDownList ddlCreditAcctCode = tmpgvr.FindControl("ddlCreditAcctCode") as DropDownList;
                DropDownList ddlDebitCostCentre = tmpgvr.FindControl("ddlDebitCostCentre") as DropDownList;
                DropDownList ddlCreditCostCentre = tmpgvr.FindControl("ddlCreditCostCentre") as DropDownList;

                DropDownList ddlPrvDebitAcctCode = tmpgvr.FindControl("ddlPrvDebitAcctCode") as DropDownList;
                DropDownList ddlPrvCreditAcctCode = tmpgvr.FindControl("ddlPrvCreditAcctCode") as DropDownList;
                DropDownList ddlPrvDebitCostCentre = tmpgvr.FindControl("ddlPrvDebitCostCentre") as DropDownList;
                DropDownList ddlPrvCreditCostCentre = tmpgvr.FindControl("ddlPrvCreditCostCentre") as DropDownList;

                FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
                Currency_BLL.getCurrencyDesc(ref ddlCurrency);
                Lookup_BLL.GetLookUpValues(ref ddlContractStatus, "LRequest");
                Lookup_BLL.GetLookUpValues(ref ddl_ContractType, "Contract_Type");

                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlDebitAcctCode);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlCreditAcctCode);
                FIN.BLL.LOAN.Contract_BLL.fn_getDebitCreditCostCentre(ref ddlDebitCostCentre);
                FIN.BLL.LOAN.Contract_BLL.fn_getDebitCreditCostCentre(ref ddlCreditCostCentre);

                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlPrvDebitAcctCode);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlPrvCreditAcctCode);
                FIN.BLL.LOAN.Contract_BLL.fn_getDebitCreditCostCentre(ref ddlPrvDebitCostCentre);
                FIN.BLL.LOAN.Contract_BLL.fn_getDebitCreditCostCentre(ref ddlPrvCreditCostCentre);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlBankName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PARTY_ID"].ToString();
                    ddlCurrency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CURRENCY_ID"].ToString();
                    ddlContractStatus.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CONTRACT_STATUS"].ToString();
                    ddl_ContractType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CONTRACT_TYPE"].ToString();
                    ddlDebitAcctCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_DEBIT_ACTID"].ToString();
                    ddlCreditAcctCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CREDIT_ACTID"].ToString();
                    ddlDebitCostCentre.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_DEBIT_CCID"].ToString();
                    ddlCreditCostCentre.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CREDIT_CCID"].ToString();

                    ddlPrvDebitAcctCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PRV_DEBIT_ACTID"].ToString();
                    ddlPrvCreditAcctCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PRV_CREDIT_ACTID"].ToString();
                    ddlPrvDebitCostCentre.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PRV_DEBIT_CCID"].ToString();
                    ddlPrvCreditCostCentre.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PRV_CREDIT_CCID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session["GridData"] != null)
            {
                dtGridData = (DataTable)Session["GridData"];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            System.Collections.SortedList slControls_n = new System.Collections.SortedList();

            TextBox txtCM = gvr.FindControl("txtCN") as TextBox;
            TextBox txtContractDesc = gvr.FindControl("txtContractDesc") as TextBox;
            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            TextBox txtContractDate = gvr.FindControl("dtpContractDate") as TextBox;
            DropDownList ddlContractStatus = gvr.FindControl("ddlContractStatus") as DropDownList;
            DropDownList ddlContractType = gvr.FindControl("ddlContractType") as DropDownList;
            DropDownList ddlCurrency = gvr.FindControl("ddlCurrency") as DropDownList;
            TextBox txtPrincipalAmt = gvr.FindControl("txtPrincipalAmt") as TextBox;
            TextBox txtProfit = gvr.FindControl("txtProfit") as TextBox;
            TextBox txtStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox txtEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            TextBox txtContractDescOL = gvr.FindControl("txtContractDescOL") as TextBox;
            DropDownList ddlDebitAcctCode = gvr.FindControl("ddlDebitAcctCode") as DropDownList;
            DropDownList ddlCreditAcctCode = gvr.FindControl("ddlCreditAcctCode") as DropDownList;
            DropDownList ddlDebitCostCentre = gvr.FindControl("ddlDebitCostCentre") as DropDownList;
            DropDownList ddlCreditCostCentre = gvr.FindControl("ddlCreditCostCentre") as DropDownList;

            DropDownList ddlPrvDebitAcctCode = gvr.FindControl("ddlPrvDebitAcctCode") as DropDownList;
            DropDownList ddlPrvCreditAcctCode = gvr.FindControl("ddlPrvCreditAcctCode") as DropDownList;
            DropDownList ddlPrvDebitCostCentre = gvr.FindControl("ddlPrvDebitCostCentre") as DropDownList;
            DropDownList ddlPrvCreditCostCentre = gvr.FindControl("ddlPrvCreditCostCentre") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LN_REQUEST_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            if (ddlBankName.SelectedValue.ToString().Length > 0)
            {
                slControls[0] = txtCM;
                slControls[1] = txtContractDesc;
                slControls[2] = ddlBankName;
                slControls[3] = txtContractDate;
                slControls[4] = ddlContractType;
                slControls[5] = ddlContractStatus;
                slControls[6] = ddlCurrency;
                slControls[7] = txtPrincipalAmt;
                slControls[8] = txtProfit;
                slControls[9] = ddlDebitAcctCode;
                slControls[10] = ddlCreditAcctCode;
                slControls[11] = ddlDebitCostCentre;
                slControls[12] = ddlCreditCostCentre;
                slControls[13] = ddlPrvDebitAcctCode;
                slControls[14] = ddlPrvCreditAcctCode;
                slControls[15] = ddlPrvDebitCostCentre;
                slControls[16] = ddlPrvCreditCostCentre;
                slControls[17] = txtStartDate;
                slControls[18] = txtEndDate;

                ErrorCollection.Clear();

                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

                string strMessage = "Contract Number ~ Contract Description ~ Bank Name ~ Contract Date ~ Contract Type ~ Contract Status ~ Currency ~ Principal Amount ~ Profit ~ Debit Acct Code ~ Credit Acct Code ~ Debit Cost Centre ~ Credit Cost Centre ~ Provision Debit Acct Code ~ Provision Credit Acct Code ~ Provision Debit Cost Centre ~ Provision Credit Cost Centre ~ Start Date ~ End Date";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }

                //string strCondition = "LN_PARTY_ID='" + ddlBankName.SelectedValue.Trim() + "'";
                //strMessage = FINMessageConstatns.RecordAlreadyExists;
                //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

                //if (ErrorCollection.Count > 0)
                //{
                //    return drList;
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData ];
                    if (dtLotGridData.Select("LN_PARTY_ID='" + ddlBankName.SelectedValue.ToString() + "'").Length == 0)
                    {
                        ErrorCollection.Add("BankDetails", "Bank Details Not Found");
                        return drList;
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
            }
            else
            {
                slControls_n[0] = txtPrincipalAmt;
                string strCtrlTypes1 = FINAppConstants.TEXT_BOX;
                string strMessage1 = "Principal Amount";
                EmptyErrorCollection = CommonUtils.IsValid(slControls_n, strCtrlTypes1, strMessage1);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }
            }


            if (GMode == "A")
            {
                DataTable dtFacData = new DataTable();
                dtFacData = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.getFacilityDtls(ddlFacilityname.SelectedValue)).Tables[0];
                if (dtFacData.Rows.Count > 0)
                {
                    if (double.Parse(txtPrincipalAmt.Text.ToString()) > double.Parse(dtFacData.Rows[0]["LN_BALANCE_FACILITY_AMOUNT"].ToString()))
                    {
                        ErrorCollection.Add("Amt Exceeds", "Principal Amount exceeds Facility Amount");
                        return drList;
                    }
                }
            }

            ErrorCollection.Clear();

            if (hdBankId.Value != string.Empty)
            {
                drList["LN_PARTY_ID"] = (hdBankId.Value);
            }
            drList["BANK_NAME"] = ddlBankName.SelectedItem.Text;
            drList["LN_PARTY_ID"] = ddlBankName.SelectedValue;
            if (txtCM.Text != String.Empty)
            {
                drList["LN_CONTRACT_NUM"] = txtCM.Text;
            }
            else
            {
                drList["LN_CONTRACT_NUM"] = "0";
            }

            if (txtContractDesc.Text != String.Empty)
            {
                drList["LN_CONTRACT_DESC"] = txtContractDesc.Text;
            }
            else
            {
                drList["LN_CONTRACT_DESC"] = "0";
            }
            if (txtContractDescOL.Text != String.Empty)
            {
                drList["LN_CONTRACT_DESC_OL"] = txtContractDescOL.Text;
            }
            else
            {
                drList["LN_CONTRACT_DESC_OL"] = "";
            }
            if (txtContractDate.Text != String.Empty)
            {
                drList["LN_CONTRACT_DT"] = txtContractDate.Text;
            }
            else
            {
                drList["LN_CONTRACT_DT"] = "";
            }

            if (ddlCurrency.SelectedValue.ToString().Length > 0)
            {
                drList["CURRENCY_DESC"] = ddlCurrency.SelectedItem.Text;
                drList["LN_CURRENCY_ID"] = ddlCurrency.SelectedValue;
            }
            else
            {
                drList["CURRENCY_DESC"] = "";
                drList["LN_CURRENCY_ID"] = "";
            }

            if (ddlContractType.SelectedValue.ToString().Length > 0)
            {
                drList["LN_CONTRACT_TYPE_DESC"] = ddlContractType.SelectedItem.Text;
                drList["LN_CONTRACT_TYPE"] = ddlContractType.SelectedValue;
            }
            else
            {
                drList["LN_CONTRACT_TYPE_DESC"] = "";
                drList["LN_CONTRACT_TYPE"] = "";
            }

            if (ddlContractStatus.SelectedValue.ToString().Length > 0)
            {
                drList["LN_CONTRACT_STATUS_DESC"] = ddlContractStatus.SelectedItem.Text;
                drList["LN_CONTRACT_STATUS"] = ddlContractStatus.SelectedValue;
            }
            else
            {
                drList["LN_CONTRACT_STATUS_DESC"] = "";
                drList["LN_CONTRACT_STATUS"] = "";
            }

            if (txtPrincipalAmt.Text != string.Empty)
            {
                drList["LN_PRINCIPAL_AMOUNT"] = txtPrincipalAmt.Text;
                drList["dbl_pr_amt"] = txtPrincipalAmt.Text;
            }
            else
            {
                drList["LN_PRINCIPAL_AMOUNT"] = "0";
                drList["dbl_pr_amt"] = "0";
            }
            if (txtProfit.Text != string.Empty)
            {
                drList["LN_ROI"] = txtProfit.Text;
            }
            else
            {
                drList["LN_ROI"] = "0";
            }

            if (ddlDebitAcctCode.SelectedValue.ToString().Length > 0)
            {
                drList["LN_DEBIT_ACCT_CODE"] = ddlDebitAcctCode.SelectedItem.Text;
                drList["LN_DEBIT_ACTID"] = ddlDebitAcctCode.SelectedValue;
            }
            else
            {
                drList["LN_DEBIT_ACCT_CODE"] = "";
                drList["LN_DEBIT_ACTID"] = "";
            }
            if (ddlCreditAcctCode.SelectedValue.ToString().Length > 0)
            {
                drList["LN_CREDIT_ACCT_CODE"] = ddlCreditAcctCode.SelectedItem.Text;
                drList["LN_CREDIT_ACTID"] = ddlCreditAcctCode.SelectedValue;
            }
            else
            {
                drList["LN_CREDIT_ACCT_CODE"] = "";
                drList["LN_CREDIT_ACTID"] = "";
            }
            if (ddlDebitCostCentre.SelectedValue.ToString().Length > 0)
            {
                drList["LN_DEBIT_COST_CENTRE"] = ddlDebitCostCentre.SelectedItem.Text;
                drList["LN_DEBIT_CCID"] = ddlDebitCostCentre.SelectedValue;
            }
            else
            {
                drList["LN_DEBIT_COST_CENTRE"] = "";
                drList["LN_DEBIT_CCID"] = "";
            }
            if (ddlCreditCostCentre.SelectedValue.ToString().Length > 0)
            {
                drList["LN_CREDIT_COST_CENTRE"] = ddlCreditCostCentre.SelectedItem.Text;
                drList["LN_CREDIT_CCID"] = ddlCreditCostCentre.SelectedValue;
            }
            else
            {
                drList["LN_CREDIT_COST_CENTRE"] = "";
                drList["LN_CREDIT_CCID"] = "";
            }

            if (ddlPrvDebitAcctCode.SelectedValue.ToString().Length > 0)
            {
                drList["LN_PRV_DEBIT_ACCT_CODE"] = ddlPrvDebitAcctCode.SelectedItem.Text;
                drList["LN_PRV_DEBIT_ACTID"] = ddlPrvDebitAcctCode.SelectedValue;
            }
            else
            {
                drList["LN_PRV_DEBIT_ACCT_CODE"] = "";
                drList["LN_PRV_DEBIT_ACTID"] = "";
            }
            if (ddlPrvCreditAcctCode.SelectedValue.ToString().Length > 0)
            {
                drList["LN_PRV_CREDIT_ACCT_CODE"] = ddlPrvCreditAcctCode.SelectedItem.Text;
                drList["LN_PRV_CREDIT_ACTID"] = ddlPrvCreditAcctCode.SelectedValue;
            }
            else
            {
                drList["LN_PRV_CREDIT_ACCT_CODE"] = "";
                drList["LN_PRV_CREDIT_ACTID"] = "";
            }
            if (ddlPrvDebitCostCentre.SelectedValue.ToString().Length > 0)
            {
                drList["LN_PRV_DEBIT_COST_CENTRE"] = ddlPrvDebitCostCentre.SelectedItem.Text;
                drList["LN_PRV_DEBIT_CCID"] = ddlPrvDebitCostCentre.SelectedValue;
            }
            else
            {
                drList["LN_PRV_DEBIT_COST_CENTRE"] = "";
                drList["LN_PRV_DEBIT_CCID"] = "";
            }
            if (ddlPrvCreditCostCentre.SelectedValue.ToString().Length > 0)
            {
                drList["LN_PRV_CREDIT_COST_CENTRE"] = ddlPrvCreditCostCentre.SelectedItem.Text;
                drList["LN_PRV_CREDIT_CCID"] = ddlPrvCreditCostCentre.SelectedValue;
            }
            else
            {
                drList["LN_PRV_CREDIT_COST_CENTRE"] = "";
                drList["LN_PRV_CREDIT_CCID"] = "";
            }


            if (txtStartDate.Text != string.Empty)
            {
                drList["LN_START_DATE"] = DBMethod.ConvertStringToDate(txtStartDate.Text);
            }
            else
            {
                drList["LN_START_DATE"] = "";
            }
            if (txtEndDate.Text != string.Empty)
            {
                drList["LN_END_DATE"] = DBMethod.ConvertStringToDate(txtEndDate.Text);
            }
            else
            {
                drList["LN_END_DATE"] = "";
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList["DELETED"] = FINAppConstants.N;

            return drList;
        }

    

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList["DELETED"] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        #endregion

       

      
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                var tmpSecondChildEntity = new List<Tuple<object, string>>();
                if (EntityData != null)
                {
                    lN_LOAN_REQUEST_HDR = (LN_LOAN_REQUEST_HDR)EntityData;
                }

                lN_LOAN_REQUEST_HDR.LN_FACILITY_ID = ddlFacilityname.SelectedValue.ToString();
                lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID = ddlPropertyID.SelectedValue.ToString();
                lN_LOAN_REQUEST_HDR.LN_PROPERTY_TYPE = ddlPropertyID.SelectedItem.Text.ToString();
                lN_LOAN_REQUEST_HDR.LN_PURPOSE = txtLoanPurpose.Text;
                lN_LOAN_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_REQUEST_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    lN_LOAN_REQUEST_HDR.LN_REQUEST_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001_M.ToString(), false, true);
                    lN_LOAN_REQUEST_HDR.CREATED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST_HDR.CREATED_DATE = DateTime.Today;
                }

                lN_LOAN_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_REQUEST_HDR.LN_REQUEST_ID);

                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Loan Request");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();

                    if (dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != string.Empty)
                    {
                        lN_LOAN_REQUEST_DTL = LoanRequest_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString());
                    }

                    lN_LOAN_REQUEST_DTL.LN_PARTY_ID = (dtGridData.Rows[iLoop]["LN_PARTY_ID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_PARTY_TYPE = (dtGridData.Rows[iLoop]["BANK_NAME"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CURRENCY_ID = (dtGridData.Rows[iLoop]["LN_CURRENCY_ID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_NUM = (dtGridData.Rows[iLoop]["LN_CONTRACT_NUM"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_DESC = (dtGridData.Rows[iLoop]["LN_CONTRACT_DESC"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_DT = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_CONTRACT_DT"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_STATUS = (dtGridData.Rows[iLoop]["LN_CONTRACT_STATUS"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_TYPE = (dtGridData.Rows[iLoop]["LN_CONTRACT_TYPE"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_DEBIT_ACCT_CODE = (dtGridData.Rows[iLoop]["LN_DEBIT_ACTID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CREDIT_ACCT_CODE = (dtGridData.Rows[iLoop]["LN_CREDIT_ACTID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_DEBIT_COST_CENTRE= (dtGridData.Rows[iLoop]["LN_DEBIT_CCID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CREDIT_COST_CENTRE = (dtGridData.Rows[iLoop]["LN_CREDIT_CCID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_DESC_OL = (dtGridData.Rows[iLoop]["LN_CONTRACT_DESC_OL"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_PRV_DEBIT_ACCT_CODE = (dtGridData.Rows[iLoop]["LN_PRV_DEBIT_ACTID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_PRV_CREDIT_ACCT_CODE = (dtGridData.Rows[iLoop]["LN_PRV_CREDIT_ACTID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_PRV_DEBIT_COST_CENTRE = (dtGridData.Rows[iLoop]["LN_PRV_DEBIT_CCID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_PRV_CREDIT_COST_CENTRE = (dtGridData.Rows[iLoop]["LN_PRV_CREDIT_CCID"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_PRINCIPAL_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_PRINCIPAL_AMOUNT"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_ROI = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_ROI"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_START_DATE = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_START_DATE"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_END_DATE = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_END_DATE"].ToString());
                    

                    lN_LOAN_REQUEST_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    lN_LOAN_REQUEST_DTL.LN_REQUEST_ID = lN_LOAN_REQUEST_HDR.LN_REQUEST_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != string.Empty)
                        {
                            lN_LOAN_REQUEST_DTL.LN_CONTRACT_ID = dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString();
                            lN_LOAN_REQUEST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            lN_LOAN_REQUEST_DTL.LN_CONTRACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001_D.ToString(), false, true);
                            lN_LOAN_REQUEST_DTL.CREATED_BY = this.LoggedUserName;
                            lN_LOAN_REQUEST_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Add));
                        }
                    }

                    lN_LOAN_REQUEST_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    #region TermsEntryNoNeed
                    ////Terms Entry
                    //if (Session[FINSessionConstants.LotGridData] != null)
                    //{
                    //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];

                    //    if (dtLotGridData.Rows.Count > 0)
                    //    {
                    //        for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                    //        {
                    //            if (dtGridData.Rows[iLoop]["ITEM_ID"].ToString() == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString())
                    //            {
                    //                lN_LOAN_REQUEST_TERMS = new LN_LOAN_REQUEST_TERMS();

                    //                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    //                {
                    //                    if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                    //                    {
                    //                        lN_LOAN_REQUEST_TERMS = MaterialIssue_BLL.getWHClassEntity(dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString());
                    //                    }
                    //                }

                    //                lN_LOAN_REQUEST_TERMS.INDENT_DTL_ID = iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID;
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_WH_ID = dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString();
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_LOT_ID = dtLotGridData.Rows[jLoop]["lot_id"].ToString();
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_LOT_QTY = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                    //                lN_LOAN_REQUEST_TERMS.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                    //                if (dtLotGridData.Rows[jLoop]["DELETED"].ToString() == FINAppConstants.Y)
                    //                {
                    //                    tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Delete));
                    //                }
                    //                else
                    //                {
                    //                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    //                    {
                    //                        if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                    //                        {
                    //                            lN_LOAN_REQUEST_TERMS.MODIFIED_DATE = DateTime.Today;
                    //                            tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Update));
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        lN_LOAN_REQUEST_TERMS.ISSUE_MAT_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_028_T.ToString(), false, true);
                    //                        lN_LOAN_REQUEST_TERMS.CREATED_BY = Session["UserId"].ToString();
                    //                        lN_LOAN_REQUEST_TERMS.CREATED_DATE = DateTime.Today;
                    //                        tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Add));
                    //                    }
                    //                }

                    //                lN_LOAN_REQUEST_TERMS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<LN_LOAN_REQUEST_HDR, LN_LOAN_REQUEST_DTL>(lN_LOAN_REQUEST_HDR, tmpChildEntity, lN_LOAN_REQUEST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleEntity<LN_LOAN_REQUEST_HDR, LN_LOAN_REQUEST_DTL, LN_LOAN_REQUEST_TERMS>(lN_LOAN_REQUEST_HDR, tmpChildEntity, lN_LOAN_REQUEST_DTL, tmpSecondChildEntity, lN_LOAN_REQUEST_TERMS,true);
                            savedBool = true;
                            break;
                        }
                }

                string str_app_amt = dtGridData.Compute("sum(dbl_pr_amt)", "LN_CONTRACT_STATUS='Approved' and DELETED in ('N', '0')").ToString();
                if (str_app_amt.Length > 0)
                {
                    DBMethod.ExecuteQuery("update LN_LOAN_FACILITY_HDR set ln_balance_facility_amount=ln_facility_amount-" + str_app_amt +" where ln_facility_id='" + ddlFacilityname.SelectedValue +"'");
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MISAVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnTerm_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotButtonClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void btnTermNo_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotClose", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events

        private void BindLotGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();


                bol_rowVisiable = false;
                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvLot.DataSource = dt_tmp;
                gvLot.DataBind();
                GridViewRow gvr = gvLot.FooterRow;
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterLotGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlWH = tmpgvr.FindControl("ddlWH") as DropDownList;
                //DropDownList ddlLot = tmpgvr.FindControl("ddlLot") as DropDownList;

                ////Warehouse_BLL.GetWareHouseName(ref ddlWH);
                ////MaterialIssue_BLL.GetLotNumber(ref ddlLot);
                //MaterialIssue_BLL.getWHDetails_fritem(ref ddlWH, hdItemId.Value.ToString());


                //if (gvLot.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                //{
                //    // ddlWH.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["INV_WH_NAME"].ToString();
                //    ddlWH.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["INV_WH_ID"].ToString();
                //    //ddlLot.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["LOT_NUMBER"].ToString();
                //    fillLot();
                //    ddlLot.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["LOT_ID"].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootLOTGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvLot_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
            //{
            //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
            //}
            if (Session[FINSessionConstants.LotGridData] != null)
            {
                dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
            }
            gvLot.EditIndex = -1;

            BindLotGrid(dtLotGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvLot_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvLot.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }

                }

                //if (e.CommandName.Equals("btnPop"))
                //{
                //    int rowindex = Convert.ToInt32(e.CommandArgument);
                //    hdRowIndex.Value = rowindex.ToString();
                //}

                if (Session["rowindex"] == null)
                {
                    Session["rowindex"] = 0;
                    hdRowIndex.Value = Session["rowindex"].ToString();
                }
                else
                {
                    hdRowIndex.Value = (int.Parse(Session["rowindex"].ToString()) + 1).ToString();
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    if (dtLotGridData != null)
                    {
                        int rowLotIndex = 0;
                        //if (gvr.RowIndex == -1)
                        //{
                        //    rowLotIndex = gvr.RowIndex + rowIndexVal;
                        //}
                        //else if (gvr.RowIndex >= 0)
                        //{
                        //    rowLotIndex = gvr.RowIndex;
                        //}

                        drList = AssignToLotGridControl(gvr, dtLotGridData, "A", 0);
                        if (ErrorCollection.Count > 0)
                        {
                            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                            return;
                        }
                        dtLotGridData.Rows.Add(drList);
                       // Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                        Session[FINSessionConstants.LotGridData] = dtLotGridData;
                        BindLotGrid(dtLotGridData);
                    }
                  
                }
                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToLotGridControl(GridViewRow gvr, DataTable tmpdtLotGridData, string GMode, int rowindex)
        {

            //System.Collections.SortedList slControls = new System.Collections.SortedList();

            //DropDownList ddlWH = gvr.FindControl("ddlWH") as DropDownList;
            //DropDownList ddlLot = gvr.FindControl("ddlLot") as DropDownList;
            //TextBox txtQty = gvr.FindControl("txtQty") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtLotGridData.Copy();
            if (GMode == "A")
            {
                drList = dtLotGridData.NewRow();
                drList["INV_WH_ID"] = "0";
            }
            else
            {
                //if (dtLotGridData.Rows.Count > 0)
                //{
                drList = dtLotGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
                //}

            }

            //slControls[0] = ddlWH;
            //slControls[1] = ddlLot;
            //slControls[2] = txtQty;

            //ErrorCollection.Clear();
            //string strCtrlTypes = "DropDownList ~ DropDownList ~ TextBox";

            //string strMessage = "Warehouse ~ Lot ~ Quantity";

            //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}

            //string strCondition = string.Empty;

            ////strCondition = "INV_WH_ID='" + ddlWH.SelectedValue + "'";
            ////strMessage = "Record Already Exists";

            ////ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}


            //strCondition = " INV_WH_ID='" + ddlWH.SelectedValue + "' AND LOT_ID= '" + ddlLot.SelectedValue + "'";

            //strMessage = "Record Already Exists";
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            ////DataTable dtqty = new DataTable();
            ////dtqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.getLotqty(ddlLineNumber.SelectedValue)).Tables[0];
            //////hf_qty.Value = dtqty.Rows[0]["tot_qty"].ToString();
            ////hf_qty.Value = "0";

            ////if (decimal.Parse(txtQuantityforthislot.Text) > decimal.Parse(hf_qty.Value))
            ////{
            ////    ErrorCollection.Add("lotqty", "Quantity Cannot be greater than purchase item received quantity");
            ////    return drList;
            ////}
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}

            ////DataTable dtlotqty = new DataTable();

            //// dtlotqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.GetTotalReceivedLotqty(txtGRNNumber.Text, ddlLineNumber.SelectedValue, hid_Item_Id.Value)).Tables[0];

            ////if (decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString()) > 0)
            ////{
            ////    decimal diffLot = decimal.Parse(hf_qty.Value) - decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString());

            ////    if (decimal.Parse(txtQuantityforthislot.Text) > Math.Abs(diffLot))
            ////    {
            ////        ErrorCollection.Add("totlotqty", "Quantity for this lot exceeding the GRN quantity.Received quantity is " + dtlotqty.Rows[0]["lot_qty"].ToString() + ".Pending Quantity is " + diffLot);
            ////        return drList;
            ////    }
            ////}
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}

            //drList["INV_WH_NAME"] = ddlWH.SelectedItem.Text.ToString();
            //drList["INV_WH_ID"] = ddlWH.SelectedValue.ToString();
            //drList["LOT_NUMBER"] = ddlLot.SelectedItem.Text.ToString();
            //drList["LOT_ID"] = ddlLot.SelectedValue.ToString();
            //drList["LOT_QTY"] = txtQty.Text.ToString();
            //drList["ITEM_ID"] = hdItemId.Value.ToString();

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvLot_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData ];
                }
                if (gvr == null)
                {
                    return;
                }
                //int rowindex = Convert.ToInt32(e.CommandArgument);

                //hdRowIndex.Value = rowindex.ToString();
                if (dtLotGridData != null)
                {
                    drList = AssignToLotGridControl(gvr, dtLotGridData, "U", e.RowIndex);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    gvLot.EditIndex = -1;
                    //Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                    Session[FINSessionConstants.LotGridData] = dtLotGridData;
                    BindLotGrid(dtLotGridData);
                }
                //if (Session[FINSessionConstants.LotGridData + hdRowIndex.Value] == null)
                //{
                //    Session[FINSessionConstants.LotGridData + hdRowIndex.Value] = Session[FINSessionConstants.LotGridData];
                //}

                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvLot_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }
                DataRow drList = null;
                drList = dtLotGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindLotGrid(dtLotGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvLot_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                //if (Session[FINSessionConstants.LotGridData + ] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdRowIndex.Value];
                //}
                gvLot.EditIndex = e.NewEditIndex;
                BindLotGrid(dtLotGridData);
                GridViewRow gvr = gvLot.Rows[e.NewEditIndex];
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvLot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvLot.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvLot_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    else
                    {
                        //if (((DataRowView)e.Row.DataItem).Row["ITEM_ID"].ToString() != hdItemId.Value)
                        //{
                        //    e.Row.Visible = false;
                        //}
                    }
                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        #endregion

       
        protected void getLotData()
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLot = new DropDownList();
                TextBox txtQuantity = new TextBox();

                ddlLot.ID = "ddlLot";
                txtQuantity.ID = "txtQty";

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIGetLotData", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlFacilityname_SelectedIndexChanged(object sender, EventArgs e)
        {
            getPropertyName();

            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanReqDtlsForFacility(ddlFacilityname.SelectedValue, ddlPropertyID.SelectedValue)).Tables[0];
            BindGrid(dtGridData);

            if(dtGridData.Rows.Count>0)
            {
                if (dtGridData.Rows[0]["LN_PURPOSE"].ToString().Length > 0)
                {
                    txtLoanPurpose.Text = dtGridData.Rows[0]["LN_PURPOSE"].ToString();
                }
            }
        }

        private void getPropertyName()
        {
            FIN.BLL.LOAN.Facility_BLL.fn_getPorpertyname4Facility(ref ddlPropertyID, ddlFacilityname.SelectedValue);
            ddlPropertyID.SelectedIndex = 1;
            ddlPropertyID.Enabled = false;
        }

    }
}