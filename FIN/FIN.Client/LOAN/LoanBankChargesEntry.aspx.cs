﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.BLL.CA;
using FIN.BLL.LOAN;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR
{
    public partial class LoanBankChargesEntry : PageBase
    {

        LN_BANK_CHARGES LN_BANK_CHARGES = new LN_BANK_CHARGES();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();


                EntityData = null;

                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    imgBtnPost.Visible = true;
                    using (IRepository<LN_BANK_CHARGES> userCtx = new DataRepository<LN_BANK_CHARGES>())
                    {
                        LN_BANK_CHARGES = userCtx.Find(r =>
                            (r.BK_CHARGE_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = LN_BANK_CHARGES;
                    txtBankCharge.Text = LN_BANK_CHARGES.BK_CHARGE_ID;
                    if (LN_BANK_CHARGES.BK_CHARGE_DT != null)
                    {
                        txtChargingDate.Text = DBMethod.ConvertDateToString(LN_BANK_CHARGES.BK_CHARGE_DT.ToString());
                    }

                   
                    ddlContract.SelectedValue = LN_BANK_CHARGES.BK_CONTRACT_ID;
                    GetContractDetails();
                    ddlChargeType.SelectedValue = LN_BANK_CHARGES.BK_CHARGE_TYPE;
                    txtChargePercent.Text = LN_BANK_CHARGES.BK_CHARGE_PERCENT.ToString();
                    txtAmount.Text = LN_BANK_CHARGES.BK_CHARGE_AMT.ToString();

                    chkActive.Checked = LN_BANK_CHARGES.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;

                    imgBtnJVPrint.Visible = false;
                    if (LN_BANK_CHARGES.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        imgBtnPost.Visible = true;
                        //lblPosted.Visible = false;
                        //lblCancelled.Visible = false;
                    }
                    if (LN_BANK_CHARGES.POSTED_FLAG == FINAppConstants.Y)
                    {
                        imgBtnPost.Visible = false;
                        imgBtnJVPrint.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.LOAN.PropertyRevaluationBLL.GetRequestID(ref ddlPropertyName);
            FIN.BLL.LOAN.Contract_BLL.fn_getContractNumber(ref ddlContract);
            //BankChargesBLL.GetContract(ref ddlContract);
            Lookup_BLL.GetLookUpValues(ref ddlChargeType, "CHARGE_TYPE");
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    LN_BANK_CHARGES = (LN_BANK_CHARGES)EntityData;
                }

                if (txtChargingDate.Text != string.Empty)
                {
                    LN_BANK_CHARGES.BK_CHARGE_DT = DBMethod.ConvertStringToDate(txtChargingDate.Text.ToString());
                }

                LN_BANK_CHARGES.BK_PROPERTY_ID = ddlPropertyName.SelectedValue.ToString();
                LN_BANK_CHARGES.BK_CONTRACT_ID = ddlContract.SelectedValue.ToString();
                LN_BANK_CHARGES.BK_CHARGE_TYPE = ddlChargeType.SelectedValue.ToString();

                LN_BANK_CHARGES.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                LN_BANK_CHARGES.BK_CHARGE_PERCENT = CommonUtils.ConvertStringToDecimal(txtChargePercent.Text);
                LN_BANK_CHARGES.BK_CHARGE_AMT = CommonUtils.ConvertStringToDecimal(txtAmount.Text);
                LN_BANK_CHARGES.BK_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    LN_BANK_CHARGES.MODIFIED_BY = this.LoggedUserName;
                    LN_BANK_CHARGES.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    LN_BANK_CHARGES.BK_CHARGE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001.ToString(), false, true);
                    LN_BANK_CHARGES.CREATED_BY = this.LoggedUserName;
                    LN_BANK_CHARGES.CREATED_DATE = DateTime.Today;
                }

                LN_BANK_CHARGES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, LN_BANK_CHARGES.BK_CHARGE_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<LN_BANK_CHARGES>(LN_BANK_CHARGES);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<LN_BANK_CHARGES>(LN_BANK_CHARGES, true);
                            savedBool = true;
                            break;

                        }
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_BANK_CHARGES>(LN_BANK_CHARGES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    LN_BANK_CHARGES = (LN_BANK_CHARGES)EntityData;
                }
                if (LN_BANK_CHARGES.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(LN_BANK_CHARGES.BK_CHARGE_ID, "AP_021");

                }

                LN_BANK_CHARGES = BankChargesBLL.getClassEntity(Master.StrRecordId);
                LN_BANK_CHARGES.POSTED_FLAG = FINAppConstants.Y;
                LN_BANK_CHARGES.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<LN_BANK_CHARGES>(LN_BANK_CHARGES, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;

                //pnlgridview.Enabled = false;
                //pnltdHeader.Enabled = false;
                //pnlReason.Enabled = false;
                //pnlRejReason.Enabled = false;

                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_BANK_CHARGES.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            LN_BANK_CHARGES = BankChargesBLL.getClassEntity(Master.StrRecordId);

            if (LN_BANK_CHARGES != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_BANK_CHARGES.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    LN_BANK_CHARGES = BankChargesBLL.getClassEntity(Master.StrRecordId);
                    // iNV_INVOICES_HDR = (INV_INVOICES_HDR)EntityData;

                    ErrorCollection.Remove("Paythere");
                    if (Invoice_DAL.IsPaymentRecordFound(LN_BANK_CHARGES.BK_CONTRACT_ID))
                    {
                        ErrorCollection.Add("Paythere", "Payment is already exists for this invoice.Please stop the payment, before cancel the invoice");
                        return;
                    }


                    if (LN_BANK_CHARGES.POSTED_FLAG != null)
                    {
                        if (LN_BANK_CHARGES.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(LN_BANK_CHARGES.JE_HDR_ID, "AP_021_R");
                        }
                    }

                    imgBtnPost.Visible = false;
                  

                    //pnlgridview.Enabled = false;
                    //pnltdHeader.Enabled = false;
                    //pnlReason.Enabled = false;
                    //pnlRejReason.Enabled = false;

                    btnSave.Visible = false;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                    LN_BANK_CHARGES = BankChargesBLL.getClassEntity(Master.StrRecordId);
                }
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";



                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_BANK_CHARGES.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnCancelJV_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                LN_BANK_CHARGES = BankChargesBLL.getClassEntity(Master.StrRecordId);

                if (LN_BANK_CHARGES != null)
                {

                    Hashtable htParameters = new Hashtable();
                    Hashtable htHeadingParameters = new Hashtable();
                    Hashtable htFilterParameter = new Hashtable();

                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_BANK_CHARGES.JE_HDR_ID));


                    Session["ProgramName"] = "Journal Voucher";
                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlPropertyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetContractName();
        }
        private void GetContractName()
        {
            FIN.BLL.HR.LoanRequest_BLL.fn_GetLoanDetails4PropertyId(ref ddlContract, ddlPropertyName.SelectedValue.ToString());
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                //ErrorCollection.Clear();
                //ReportFile = Master.ReportName;


                ////if (ddlSupplierName.SelectedValue != string.Empty)
                ////{
                ////    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                ////}
                //if (txtInvoiceDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Date", txtInvoiceDate.Text);
                //}
                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    htFilterParameter.Add("inv_id", Master.StrRecordId.ToString());
                //}
                ////if (txtToDate.Text != string.Empty)
                ////{
                ////    htFilterParameter.Add("To_Date", txtToDate.Text);
                ////}

                //VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                //VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                //ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetInvoiceListReportData();

                //htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                //ReportFormulaParameter = htHeadingParameters;

                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetContractDetails();
        }

        private void GetContractDetails()
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails("", ddlContract .SelectedValue)).Tables[0];
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                ddlPropertyName.SelectedValue = dt_LoanReqDtl.Rows[0]["ln_request_id"].ToString();
                hf_PrinceAmt.Value = dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString();
               
              
            }
        }

        protected void txtChargePercent_TextChanged(object sender, EventArgs e)
        {
            txtAmount.Text = (decimal.Parse(hf_PrinceAmt.Value) * decimal.Parse(txtChargePercent.Text) / 100).ToString();
        }
    }
}
