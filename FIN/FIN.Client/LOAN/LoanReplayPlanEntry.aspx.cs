﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.LOAN;
using VMVServices.Web;
using Microsoft.VisualBasic;

namespace FIN.Client.HR
{
    public partial class LoanReplayPlanEntry : PageBase
    {
        DataTable dtGridData = new DataTable();
        string ProReturn = null;
        LN_REPAY_PLAN_HDR lN_REPAY_PLAN_HDR = new LN_REPAY_PLAN_HDR();
        LN_REPAY_PLAN_DTL lN_REPAY_PLAN_DTL = new LN_REPAY_PLAN_DTL();
        LoanRepayPlan_BLL loanRepayPlan_BLL = new LoanRepayPlan_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));



            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            loanRepayPlan_BLL.fn_GetLoanDetails(ref ddlLoanId);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.LOAN.LoanRepayPlan_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<LN_REPAY_PLAN_HDR> userCtx = new DataRepository<LN_REPAY_PLAN_HDR>())
                    {
                        lN_REPAY_PLAN_HDR = userCtx.Find(r =>
                            (r.LN_REPAY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = lN_REPAY_PLAN_HDR;

                    ddlLoanId.SelectedValue = lN_REPAY_PLAN_HDR.LN_LOAN_ID;
                    fn_fillInstallment_dtls();
                    ddlInstallment.SelectedValue = lN_REPAY_PLAN_HDR.LN_LOAN_DTL_ID;
                    FillLoanInstallDet();
                    txtPaidLoanAmt.Text = lN_REPAY_PLAN_HDR.LN_PAID_AMOUNT.ToString();
                    txtPaidPrinRepay.Text = lN_REPAY_PLAN_HDR.LN_PAID_PRINCIPAL.ToString();
                    txtPaidMonInt.Text = lN_REPAY_PLAN_HDR.LN_PAID_INTEREST.ToString();
                    CallTotInstallment();
                    //txtBalance.Text = lN_REPAY_PLAN_HD

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {

                    lN_REPAY_PLAN_HDR = (LN_REPAY_PLAN_HDR)EntityData;
                }

                lN_REPAY_PLAN_HDR.LN_LOAN_ID = ddlLoanId.SelectedValue.ToString();
                lN_REPAY_PLAN_HDR.LN_LOAN_DTL_ID = ddlInstallment.SelectedValue.ToString();
                // lN_REPAY_PLAN_HDR.LN_PAID_AMOUNT = decimal.Parse(txtPaidLoanAmt.Text.ToString());

                lN_REPAY_PLAN_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //lN_REPAY_PLAN_HDR. = VMVServices.Web.Utils.OrganizationID;
                // hR_TRM_COURSE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                lN_REPAY_PLAN_HDR.LN_PAID_INTEREST = decimal.Parse(txtPaidMonInt.Text);
                lN_REPAY_PLAN_HDR.LN_PAID_PRINCIPAL = Decimal.Parse(txtPaidPrinRepay.Text);
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_REPAY_PLAN_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_REPAY_PLAN_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    lN_REPAY_PLAN_HDR.LN_REPAY_ID = FINSP.GetSPFOR_SEQCode("LN_003_M".ToString(), false, true);
                    //hR_TRM_COURSE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_COURSE_SEQ);
                    lN_REPAY_PLAN_HDR.CREATED_BY = this.LoggedUserName;
                    lN_REPAY_PLAN_HDR.CREATED_DATE = DateTime.Today;

                }
                lN_REPAY_PLAN_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_REPAY_PLAN_HDR.LN_LOAN_ID);


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    //LN_REPAY_PLAN_DTL 
                    lN_REPAY_PLAN_DTL = new LN_REPAY_PLAN_DTL();
                    if (dtGridData.Rows[iLoop]["LN_REPAY_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<LN_REPAY_PLAN_DTL> userCtx = new DataRepository<LN_REPAY_PLAN_DTL>())
                        {
                            lN_REPAY_PLAN_DTL = userCtx.Find(r =>
                                (r.LN_REPAY_DTL_ID == dtGridData.Rows[iLoop]["LN_REPAY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    lN_REPAY_PLAN_DTL.LN_REPAY_INSTALLMENT_NO = decimal.Parse(dtGridData.Rows[iLoop]["LN_REPAY_INSTALLMENT_NO"].ToString());
                    lN_REPAY_PLAN_DTL.LN_REPAY_DT = DateTime.Parse(dtGridData.Rows[iLoop]["LN_REPAY_DT"].ToString());
                    lN_REPAY_PLAN_DTL.LN_REPAY_AMOUNT = Decimal.Parse(dtGridData.Rows[iLoop]["LN_REPAY_AMOUNT"].ToString());
                    lN_REPAY_PLAN_DTL.LN_REPAY_ID = lN_REPAY_PLAN_HDR.LN_REPAY_ID;
                    if (dtGridData.Rows[iLoop]["LN_PAID"].ToString().ToUpper() == "TRUE")
                    {
                        lN_REPAY_PLAN_DTL.LN_PAID = FINAppConstants.Y;
                    }
                    else
                    {
                        lN_REPAY_PLAN_DTL.LN_PAID = FINAppConstants.N;
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        lN_REPAY_PLAN_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        lN_REPAY_PLAN_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }





                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(lN_REPAY_PLAN_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.DepartmentDetails_DAL.GetSPFOR_DUPLICATE_CHECK(hR_DEPT_DESIGNATIONS.DEPT_ID, hR_DEPT_DESIGNATIONS.DESIG_SHORT_NAME, hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("DEPTSCHD", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["LN_REPAY_DTL_ID"].ToString() != "0")
                        {
                            lN_REPAY_PLAN_DTL.LN_REPAY_DTL_ID = dtGridData.Rows[iLoop]["LN_REPAY_DTL_ID"].ToString();
                            lN_REPAY_PLAN_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_REPAY_PLAN_DTL.LN_REPAY_DTL_ID);
                            lN_REPAY_PLAN_DTL.MODIFIED_BY = this.LoggedUserName;
                            lN_REPAY_PLAN_DTL.MODIFIED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<LN_REPAY_PLAN_DTL>(lN_REPAY_PLAN_DTL, true);

                            tmpChildEntity.Add(new Tuple<object, string>(lN_REPAY_PLAN_DTL, "U"));
                            savedBool = true;
                        }
                        else
                        {

                            lN_REPAY_PLAN_DTL.LN_REPAY_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_003_D.ToString(), false, true);
                            //lN_REPAY_PLAN_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_DEPT_DESIGNATIONS_SEQ);
                            lN_REPAY_PLAN_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_REPAY_PLAN_DTL.LN_REPAY_DTL_ID);
                            lN_REPAY_PLAN_DTL.CREATED_BY = this.LoggedUserName;
                            lN_REPAY_PLAN_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<LN_REPAY_PLAN_DTL>(lN_REPAY_PLAN_DTL);

                            tmpChildEntity.Add(new Tuple<object, string>(lN_REPAY_PLAN_DTL, "A"));
                            savedBool = true;
                        }
                    }

                }
                VMVServices.Web.Utils.SavedRecordId = "";

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<LN_REPAY_PLAN_HDR, LN_REPAY_PLAN_DTL>(lN_REPAY_PLAN_HDR, tmpChildEntity, lN_REPAY_PLAN_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<LN_REPAY_PLAN_HDR, LN_REPAY_PLAN_DTL>(lN_REPAY_PLAN_HDR, tmpChildEntity, lN_REPAY_PLAN_DTL, true);
                            savedBool = true;
                            break;

                        }
                }

                 ChangeLoanInstallHistroy();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void ChangeLoanInstallHistroy()
        {
            DataTable dt_his_data = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanRepayPlan_DAL.getLoanInstallmentHistory(int.Parse(ddlInstallment.SelectedItem.Text.ToString()))).Tables[0];
            if (dt_his_data.Rows.Count > 0)
            {
                LN_INSTALLMENT_HISTROY_DTL LN_INSTALLMENT_HISTROY_DTL = new LN_INSTALLMENT_HISTROY_DTL();
                LN_INSTALLMENT_HISTROY_HDR lN_INSTALLMENT_HISTROY_HDR = new LN_INSTALLMENT_HISTROY_HDR();
                lN_INSTALLMENT_HISTROY_HDR.LN_INSTALLMENT_NO = int.Parse(ddlInstallment.SelectedItem.Text);
                lN_INSTALLMENT_HISTROY_HDR.ENABLED_FLAG = FINAppConstants.Y;
                lN_INSTALLMENT_HISTROY_HDR.CREATED_BY = this.LoggedUserName;
                lN_INSTALLMENT_HISTROY_HDR.CREATED_DATE = DateTime.Today;
                lN_INSTALLMENT_HISTROY_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                lN_INSTALLMENT_HISTROY_HDR.LN_INST_HIS_ID = FINSP.GetSPFOR_SEQCode("LN_IHH".ToString(), false, true);
                var tmpChildEntity = new List<Tuple<object, string>>();
                for (int iLoop = 0; iLoop <= dt_his_data.Rows.Count; iLoop++)
                {
                    LN_INSTALLMENT_HISTROY_DTL = new LN_INSTALLMENT_HISTROY_DTL();
                    LN_INSTALLMENT_HISTROY_DTL.LN_INST_HIS_ID = lN_INSTALLMENT_HISTROY_HDR.LN_INST_HIS_ID;
                    LN_INSTALLMENT_HISTROY_DTL.LN_INSTALLMENT_NO = int.Parse(dt_his_data.Rows[iLoop]["LN_INSTALLMENT_NO"].ToString());
                    if (dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"] != DBNull.Value)
                    {
                        LN_INSTALLMENT_HISTROY_DTL.LN_INSTALLMENT_DT = DateTime.Parse(dt_his_data.Rows[iLoop]["LN_INSTALLMENT_DT"].ToString());
                    }
                    // LN_INSTALLMENT_HISTROY_DTL.LN_PRIN_BALANCE = decimal.Parse(dtGridData.Rows[iLoop]["LN_PRIN_BALANCE"].ToString());
                    LN_INSTALLMENT_HISTROY_DTL.LN_MONTHLY_INT = decimal.Parse(dt_his_data.Rows[iLoop]["LN_MONTHLY_INT"].ToString());

                    LN_INSTALLMENT_HISTROY_DTL.LN_BALANCE = decimal.Parse(dt_his_data.Rows[iLoop]["LN_BALANCE"].ToString());
                    LN_INSTALLMENT_HISTROY_DTL.LN_INSTALMENT = decimal.Parse(dt_his_data.Rows[iLoop]["LN_INSTALMENT"].ToString());
                    LN_INSTALLMENT_HISTROY_DTL.LN_PRINCIPAL_REPAYMENT = decimal.Parse(dt_his_data.Rows[iLoop]["LN_PRINCIPAL_REPAYMENT"].ToString());
                    //LN_INSTALLMENT_HISTROY_DTL.LN_PAID_AMOUNT = decimal.Parse(dtGridData.Rows[iLoop]["LN_PAID_AMOUNT"].ToString());



                    LN_INSTALLMENT_HISTROY_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        LN_INSTALLMENT_HISTROY_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        LN_INSTALLMENT_HISTROY_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(LN_INSTALLMENT_HISTROY_DTL, "D"));
                    }
                    else
                    {
                            LN_INSTALLMENT_HISTROY_DTL.LN_INST_HIS_DTL_ID = FINSP.GetSPFOR_SEQCode("LN_IHD".ToString(), false, true);
                            LN_INSTALLMENT_HISTROY_DTL.CREATED_BY = this.LoggedUserName;
                            LN_INSTALLMENT_HISTROY_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(LN_INSTALLMENT_HISTROY_DTL, "A"));
                        
                    }
                }
            }

        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Loan Replay Plan Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["LN_PAID"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            //DropDownList ddlParentname = gvr.FindControl("ddlParentname") as DropDownList;
            TextBox txtSno = gvr.FindControl("txtSno") as TextBox;
            TextBox txtInstallmntNo = gvr.FindControl("txtInstallmntNo") as TextBox;
            TextBox dtpRepayDate = gvr.FindControl("dtpRepayDate") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;
            CheckBox chkPaid = gvr.FindControl("chkPaid") as CheckBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LN_REPAY_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtInstallmntNo;
            slControls[1] = dtpRepayDate;
            slControls[2] = txtAmount;


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox";
            string strMessage = "Installment Number ~ Repay Date ~ Amount";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //if (ddlParentname.SelectedItem != null)
            //{
            //    drList["PARENT_DEPT_DESIG_ID"] = ddlParentname.SelectedItem.Value;
            //    drList["DESIG_NAME"] = ddlParentname.SelectedItem.Text;
            //}



            drList["LN_REPAY_INSTALLMENT_NO"] = txtInstallmntNo.Text;
            drList["LN_REPAY_DT"] = dtpRepayDate.Text;
            drList["LN_REPAY_AMOUNT"] = txtAmount.Text;
            drList["SNO"] = txtSno.Text;


            if (chkPaid.Checked)
            {
                drList[FINColumnConstants.LN_PAID] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.LN_PAID] = "FALSE";
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }
        #endregion

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    lN_REPAY_PLAN_DTL.LN_REPAY_DTL_ID = dtGridData.Rows[iLoop]["LN_REPAY_DTL_ID"].ToString();
                    DBMethod.DeleteEntity<LN_REPAY_PLAN_DTL>(lN_REPAY_PLAN_DTL);
                }

                DBMethod.DeleteEntity<LN_REPAY_PLAN_HDR>(lN_REPAY_PLAN_HDR);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlLoanId_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillInstallment_dtls();
        }

        private void fn_fillInstallment_dtls()
        {
            loanRepayPlan_BLL.fn_GetInstallmentDtls(ref ddlInstallment, ddlLoanId.SelectedValue.ToString());
        }

        protected void ddlInstallment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLoanInstallDet();
            fillLoanPaidDetails();
        }

        private void FillLoanInstallDet()
        {
            DataTable dt_InstDet = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanDetails_DAL.getLoanInstallmentDetails(ddlInstallment.SelectedValue.ToString())).Tables[0];
            if (dt_InstDet.Rows.Count > 0)
            {
                txtInstallment.Text = dt_InstDet.Rows[0]["LN_INSTALMENT"].ToString();
                txtMonthlYIntest.Text = dt_InstDet.Rows[0]["LN_MONTHLY_INT"].ToString();
                txtprinRepay.Text = dt_InstDet.Rows[0]["LN_PRINCIPAL_REPAYMENT"].ToString();
            }
        }

        private void fillLoanPaidDetails()
        {
            DataTable dt_InstDet = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanRepayPlan_DAL.getLoanRepayDet4InstallmentId(ddlInstallment.SelectedValue.ToString())).Tables[0];
            if (dt_InstDet.Rows.Count > 0)
            {
                txtPaidMonInt.Text = dt_InstDet.Rows[0]["LN_PAID_INTEREST"].ToString();
                txtPaidPrinRepay.Text = dt_InstDet.Rows[0]["LN_PAID_PRINCIPAL"].ToString();
                Master.StrRecordId = dt_InstDet.Rows[0]["LN_REPAY_ID"].ToString();
                Master.Mode = FINAppConstants.Update;

                using (IRepository<LN_REPAY_PLAN_HDR> userCtx = new DataRepository<LN_REPAY_PLAN_HDR>())
                {
                    lN_REPAY_PLAN_HDR = userCtx.Find(r =>
                        (r.LN_REPAY_ID == Master.StrRecordId)
                        ).SingleOrDefault();
                }

                EntityData = lN_REPAY_PLAN_HDR;

            }
        }

        protected void txtPaidMonInt_TextChanged(object sender, EventArgs e)
        {
            CallTotInstallment();
        }

        private void CallTotInstallment()
        {
            if (txtPaidMonInt.Text.ToString().Length == 0)
            {
                return;
            }
            if (txtPaidPrinRepay.Text.ToString().Length == 0)
            {
                return;

            }
            txttotalInst.Text = (decimal.Parse(txtPaidMonInt.Text) + decimal.Parse(txtPaidPrinRepay.Text)).ToString();

        }
    }
}