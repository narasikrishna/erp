﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.LOAN
{
    public partial class LoanPropertyParam : PageBase
    {

        LN_LOAN_PROPERTY_HDR lN_LOAN_PROPERTY_HDR = new LN_LOAN_PROPERTY_HDR();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    //share.Visible = false;
                    //Propert.Visible = false;



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

              


                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    ddlPropertyType.Enabled = false;
                    using (IRepository<LN_LOAN_PROPERTY_HDR> userCtx = new DataRepository<LN_LOAN_PROPERTY_HDR>())
                    {
                        lN_LOAN_PROPERTY_HDR = userCtx.Find(r =>
                            (r.LN_PROPERTY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                      
                    }

                    EntityData = lN_LOAN_PROPERTY_HDR;
                    txtPropertyId.Text = lN_LOAN_PROPERTY_HDR.LN_PROPERTY_ID;
                    //if (lN_LOAN_REQUEST.LN_REQUEST_DT != null)
                    //{
                    //    txtReqDate.Text = DBMethod.ConvertDateToString(lN_LOAN_REQUEST.LN_REQUEST_DT.ToString());
                    //}
                 
                    ddlPropertyType.SelectedValue = lN_LOAN_PROPERTY_HDR.LN_PROPERTY_TYPE;
                    showPropertyType();
                    txtShareName.Text = lN_LOAN_PROPERTY_HDR.LN_SHARE_NAME;
                    txtShareNameOL.Text = lN_LOAN_PROPERTY_HDR.LN_SHARE_NAME_OL;
                    txtNoOfShares.Text = lN_LOAN_PROPERTY_HDR.LN_NO_OF_SHARES.ToString();
                    txtSecurityIdentifier.Text = lN_LOAN_PROPERTY_HDR.LN_SHARE_SEC_ID;
                    txtValueOfShares.Text = lN_LOAN_PROPERTY_HDR.LN_SHARE_VALUE.ToString();
                    txtCertificateNumber.Text = lN_LOAN_PROPERTY_HDR.LN_CERT_NUMBER;
                    txtFolioNo.Text = lN_LOAN_PROPERTY_HDR.LN_FOLIO_NUMBER;
                    txtFromNumber.Text = lN_LOAN_PROPERTY_HDR.LN_FROM_NUMBER;
                    txtToNumber.Text = lN_LOAN_PROPERTY_HDR.LN_TO_NUMBER;


                    txtPropertyName.Text = lN_LOAN_PROPERTY_HDR.LN_PROPERTY_NAME;
                    txtPropertyNameOL.Text = lN_LOAN_PROPERTY_HDR.LN_PROPERTY_NAME_OL;
                    txtPropertyShortName.Text = lN_LOAN_PROPERTY_HDR.LN_SHORT_NAME;
                    ddlFaciltiyType.SelectedValue = lN_LOAN_PROPERTY_HDR.LN_FACILITY_TYPE;
                    txtAddress1.Text = lN_LOAN_PROPERTY_HDR.LN_ADDRESS1;
                    txtAddress2.Text = lN_LOAN_PROPERTY_HDR.LN_ADDRESS2;
                    txtAddress3.Text = lN_LOAN_PROPERTY_HDR.LN_ADDRESS3;
                    ddlState.SelectedValue = lN_LOAN_PROPERTY_HDR.LN_STATE;
                    ddlCity.SelectedValue = lN_LOAN_PROPERTY_HDR.LN_CITY;
                    txtPostalCode.Text = lN_LOAN_PROPERTY_HDR.LN_POSTAL_CODE;
                    txtPhoneNo.Text = lN_LOAN_PROPERTY_HDR.LN_PHONE;
                    txtTitledeedRef.Text = lN_LOAN_PROPERTY_HDR.LN_TITLE_DEED_REF;
                    txtContactPerson.Text = lN_LOAN_PROPERTY_HDR.LN_PROPERTY_CONTACT_PERSON;
                    if (lN_LOAN_PROPERTY_HDR.EFFECTIVE_FROM_DT != null)
                    {
                        txtEffFromDate.Text = DBMethod.ConvertDateToString(lN_LOAN_PROPERTY_HDR.EFFECTIVE_FROM_DT.ToString());
                    }
                    if (lN_LOAN_PROPERTY_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txtEffToDate.Text = DBMethod.ConvertDateToString(lN_LOAN_PROPERTY_HDR.EFFECTIVE_TO_DT.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.SSM.States_BLL.getStateDetails(ref ddlState);
            FIN.BLL.SSM.TownCity_BLL.getCity(ref ddlCity);

            Lookup_BLL.GetLookUpValues(ref ddlPropertyType, "Request_Type");
            Lookup_BLL.GetLookUpValues(ref ddlFaciltiyType, "FACILITY_TYPE");
          


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    lN_LOAN_PROPERTY_HDR = (LN_LOAN_PROPERTY_HDR)EntityData;
                }
                lN_LOAN_PROPERTY_HDR.LN_PROPERTY_ID = txtPropertyId.Text;
                lN_LOAN_PROPERTY_HDR.LN_PROPERTY_TYPE = ddlPropertyType.SelectedValue;
                if (ddlPropertyType.SelectedValue == "SHARE")
                {
                    lN_LOAN_PROPERTY_HDR.LN_SHARE_NAME = txtShareName.Text;
                    lN_LOAN_PROPERTY_HDR.LN_SHARE_NAME_OL = txtShareNameOL.Text;
                    lN_LOAN_PROPERTY_HDR.LN_NO_OF_SHARES = CommonUtils.ConvertStringToInt(txtNoOfShares.Text);
                    lN_LOAN_PROPERTY_HDR.LN_SHARE_SEC_ID = txtSecurityIdentifier.Text;
                    lN_LOAN_PROPERTY_HDR.LN_SHARE_VALUE = CommonUtils.ConvertStringToInt(txtValueOfShares.Text);
                    lN_LOAN_PROPERTY_HDR.LN_CERT_NUMBER = txtCertificateNumber.Text;
                    lN_LOAN_PROPERTY_HDR.LN_FOLIO_NUMBER = txtFolioNo.Text;
                    lN_LOAN_PROPERTY_HDR.LN_FROM_NUMBER = txtFromNumber.Text;
                    lN_LOAN_PROPERTY_HDR.LN_TO_NUMBER = txtToNumber.Text;
                    lN_LOAN_PROPERTY_HDR.LN_SHORT_NAME = " ";
                    lN_LOAN_PROPERTY_HDR.LN_ADDRESS1 = " ";

                }

                if (ddlPropertyType.SelectedValue == "PROPERTY")
                {
                    lN_LOAN_PROPERTY_HDR.LN_PROPERTY_NAME = txtPropertyName.Text;
                    lN_LOAN_PROPERTY_HDR.LN_PROPERTY_NAME_OL = txtPropertyNameOL.Text;
                    lN_LOAN_PROPERTY_HDR.LN_SHORT_NAME = txtPropertyShortName.Text;
                    lN_LOAN_PROPERTY_HDR.LN_FACILITY_TYPE = ddlFaciltiyType.SelectedValue;
                    lN_LOAN_PROPERTY_HDR.LN_ADDRESS1 = txtAddress1.Text;
                    lN_LOAN_PROPERTY_HDR.LN_ADDRESS2 = txtAddress2.Text;
                    lN_LOAN_PROPERTY_HDR.LN_ADDRESS3 = txtAddress3.Text;
                    lN_LOAN_PROPERTY_HDR.LN_STATE = ddlState.SelectedValue;
                    lN_LOAN_PROPERTY_HDR.LN_CITY = ddlCity.SelectedValue;
                    lN_LOAN_PROPERTY_HDR.LN_POSTAL_CODE = txtPostalCode.Text;
                    lN_LOAN_PROPERTY_HDR.LN_PHONE = txtPhoneNo.Text;
                    lN_LOAN_PROPERTY_HDR.LN_TITLE_DEED_REF = txtTitledeedRef.Text;
                    lN_LOAN_PROPERTY_HDR.LN_PROPERTY_CONTACT_PERSON = txtContactPerson.Text;
                   
                }
                if (txtEffFromDate.Text != string.Empty)
                {
                    lN_LOAN_PROPERTY_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtEffFromDate.Text.ToString());
                }

                if (txtEffToDate.Text != string.Empty)
                {
                    lN_LOAN_PROPERTY_HDR.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEffToDate.Text.ToString());
                }
                lN_LOAN_PROPERTY_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                lN_LOAN_PROPERTY_HDR.LN_ORG_ID = VMVServices.Web.Utils.OrganizationID;





                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_PROPERTY_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_LOAN_PROPERTY_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    lN_LOAN_PROPERTY_HDR.LN_PROPERTY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_005.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    lN_LOAN_PROPERTY_HDR.CREATED_BY = this.LoggedUserName;
                    lN_LOAN_PROPERTY_HDR.CREATED_DATE = DateTime.Today;

                }

                lN_LOAN_PROPERTY_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_PROPERTY_HDR.LN_PROPERTY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<LN_LOAN_PROPERTY_HDR>(lN_LOAN_PROPERTY_HDR);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<LN_LOAN_PROPERTY_HDR>(lN_LOAN_PROPERTY_HDR, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_LOAN_PROPERTY_HDR>(lN_LOAN_PROPERTY_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }


        private void showPropertyType()
        {
            share.Visible = false;
            Propert.Visible = false;

            if (ddlPropertyType.SelectedValue.ToString() == "PROPERTY")
            {

                Propert.Visible = true;
                share.Visible = false;
               
            }
            else if (ddlPropertyType.SelectedValue.ToString() == "SHARE")
            {
                share.Visible = true;
                Propert.Visible = false;

            }
          
        }

      

        protected void ddlPropertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            showPropertyType();
        }





       





    }





}
