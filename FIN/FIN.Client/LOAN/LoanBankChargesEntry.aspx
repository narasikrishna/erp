﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanBankChargesEntry.aspx.cs" Inherits="FIN.Client.HR.LoanBankChargesEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <table>
            <tr>
                <td>
                    <div class="divRowContainer" style="display: none">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestId">
                            Bank Charge
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtBankCharge" CssClass="txtBox" runat="server" TabIndex="1" Enabled="false"
                                MaxLength="50" ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                            Contract Number
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 450px">
                            <asp:DropDownList ID="ddlContract" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                            Property Name
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 450px">
                            <asp:DropDownList ID="ddlPropertyName" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype" Enabled="false"
                               >
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                            Charge Type
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 155px">
                            <asp:DropDownList ID="ddlChargeType" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace  LNOrient" style="float: left">
                            &nbsp;</div>
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                            Charge Percentage
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 155px">
                            <asp:TextBox ID="txtChargePercent" CssClass="txtBox_N" runat="server" TabIndex="5"
                                Enabled="true" MaxLength="6" AutoPostBack="True" 
                                ontextchanged="txtChargePercent_TextChanged"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6"
                                    runat="server" FilterType="Numbers,custom" ValidChars=".," TargetControlID="txtChargePercent" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                            Amount
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 155px">
                            <asp:TextBox ID="txtAmount" CssClass="txtBox_N" runat="server" TabIndex="6" Enabled="true"
                                MaxLength="13"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2"
                                    runat="server" FilterType="Numbers,custom" ValidChars=".," TargetControlID="txtAmount" />
                        </div>
                        <div class="colspace  LNOrient" style="float: left">
                            &nbsp;</div>
                        <div class="lblBox  LNOrient" style=" width: 150px;" id="lblRequestDate">
                            Charging Date
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox runat="server" ID="txtChargingDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                                TabIndex="7"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtChargingDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtChargingDate" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="lblReqDescription">
                            Active
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:CheckBox runat="server" ID="chkActive" Checked="true" TabIndex="8" />
                        </div>
                    </div>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="divAction SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="26"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="28"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hf_PrinceAmt" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
  
</asp:Content>--%>
