﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LoanReqEntry : PageBase
    {

        LN_LOAN_REQUEST lN_LOAN_REQUEST = new LN_LOAN_REQUEST();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    Share1.Visible = false;
                    Share2.Visible = false;
                    Share3.Visible = false;
                    Share4.Visible = false;
                    Property1.Visible = false;
                    Property2.Visible = false;
                    Property3.Visible = false;
                    


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<LN_LOAN_REQUEST> userCtx = new DataRepository<LN_LOAN_REQUEST>())
                    {
                        lN_LOAN_REQUEST = userCtx.Find(r =>
                            (r.LN_REQUEST_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = lN_LOAN_REQUEST;
                    txtRequestId.Text = lN_LOAN_REQUEST.LN_REQUEST_ID;
                    if (lN_LOAN_REQUEST.LN_REQUEST_DT != null)
                    {
                        txtReqDate.Text = DBMethod.ConvertDateToString(lN_LOAN_REQUEST.LN_REQUEST_DT.ToString());
                    }
                    txtRequestDescription.Text = lN_LOAN_REQUEST.LN_REQUEST_DESC;
                    ddlCurrency.SelectedValue = lN_LOAN_REQUEST.LN_CURRENCY_ID;
                    showRequestType();
                    //txtLoanAmount.Text = lN_LOAN_REQUEST.LN_REQUEST_AMOUNT.ToString();
                    txtLoanAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(lN_LOAN_REQUEST.LN_REQUEST_AMOUNT.ToString());
                    ddlRequestType.SelectedValue = lN_LOAN_REQUEST.LN_REQUEST_TYPE;
                    
                    txtShareName.Text = lN_LOAN_REQUEST.LN_SHARE_NAME;
                    txtNoofshares.Text = lN_LOAN_REQUEST.LN_NO_OF_SHARES.ToString();
                    txtSecIdentification.Text = lN_LOAN_REQUEST.LN_SHARE_SEC_ID;
                    txtValueofShares.Text = lN_LOAN_REQUEST.LN_SHARE_VALUE.ToString();
                    txtCertificateNumber.Text = lN_LOAN_REQUEST.LN_CERT_NUMBER;
                    txtFolioNo.Text = lN_LOAN_REQUEST.LN_FOLIO_NUMBER;
                    txtFromNo.Text = lN_LOAN_REQUEST.LN_FROM_NUMBER;
                    txtToNumber.Text = lN_LOAN_REQUEST.LN_TO_NUMBER;


                    txtPropertyName.Text = lN_LOAN_REQUEST.LN_PROPERTY_NAME;
                    txtPropertyId.Text = lN_LOAN_REQUEST.LN_PROPERTY_ID;
                    txtTitleDeedRef.Text = lN_LOAN_REQUEST.LN_TITLE_DEED_REF;
                    txtPropertyValue.Text = lN_LOAN_REQUEST.LN_PROPERTY_VALUE;
                    ddlLocations.SelectedValue = lN_LOAN_REQUEST.LN_LOCATION_ID;

                    txtAdditionalIden1.Text = lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS1;
                    txtAdditionalIden2.Text = lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS2;
                    txtAdditionalIden3.Text = lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS3;
                    txtAdditionalIden4.Text = lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS4;
                    txtAdditionalIden5.Text = lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS5;





                    ChkEnabledFlag.Checked = lN_LOAN_REQUEST.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Location_BLL.GetLocations(ref ddlLocations);
            Lookup_BLL.GetLookUpValues(ref ddlRequestType, "Request_Type");
            Country_BLL.GetCurrencyDetails(ref ddlCurrency);
            


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    lN_LOAN_REQUEST = (LN_LOAN_REQUEST)EntityData;
                }
                lN_LOAN_REQUEST.LN_REQUEST_ID = txtRequestId.Text;
                if (txtReqDate.Text != string.Empty)
                {
                    lN_LOAN_REQUEST.LN_REQUEST_DT = DBMethod.ConvertStringToDate(txtReqDate.Text.ToString());
                }
                lN_LOAN_REQUEST.LN_REQUEST_DESC = txtRequestDescription.Text;
                lN_LOAN_REQUEST.LN_CURRENCY_ID = ddlCurrency.SelectedValue;
                lN_LOAN_REQUEST.LN_REQUEST_AMOUNT = CommonUtils.ConvertStringToDecimal(txtLoanAmount.Text);
                lN_LOAN_REQUEST.LN_REQUEST_TYPE = ddlRequestType.SelectedValue;
                if (ddlRequestType.SelectedValue != "PROPERTY")
                {
                    lN_LOAN_REQUEST.LN_SHARE_NAME = txtShareName.Text;
                    lN_LOAN_REQUEST.LN_NO_OF_SHARES = decimal.Parse(txtNoofshares.Text.ToString());
                    lN_LOAN_REQUEST.LN_SHARE_SEC_ID = txtSecIdentification.Text;
                    lN_LOAN_REQUEST.LN_SHARE_VALUE = decimal.Parse(txtValueofShares.Text);
                    lN_LOAN_REQUEST.LN_CERT_NUMBER = txtCertificateNumber.Text;
                    lN_LOAN_REQUEST.LN_FOLIO_NUMBER = txtFolioNo.Text;
                    lN_LOAN_REQUEST.LN_FROM_NUMBER = txtFromNo.Text;
                    lN_LOAN_REQUEST.LN_TO_NUMBER = txtToNumber.Text;
                }
                else if (ddlRequestType.SelectedValue != "SHARE")
                {
                    lN_LOAN_REQUEST.LN_PROPERTY_NAME = txtPropertyName.Text;
                    lN_LOAN_REQUEST.LN_PROPERTY_ID = txtPropertyId.Text;
                    lN_LOAN_REQUEST.LN_TITLE_DEED_REF = txtTitleDeedRef.Text;
                    lN_LOAN_REQUEST.LN_PROPERTY_VALUE = txtPropertyValue.Text;
                    lN_LOAN_REQUEST.LN_LOCATION_ID = ddlLocations.SelectedValue;
                }
                lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS1 = txtAdditionalIden1.Text;
                lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS2 = txtAdditionalIden2.Text;
                lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS3 = txtAdditionalIden3.Text;
                lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS4 = txtAdditionalIden4.Text;
                lN_LOAN_REQUEST.LN_ADDITIONAL_IDENTIFIERS5 = txtAdditionalIden5.Text;





               // ChkEnabledFlag.Checked = lN_LOAN_REQUEST.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                lN_LOAN_REQUEST.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                //lN_LOAN_REQUEST.ENABLED_FLAG = FINAppConstants.Y;











                //lN_LOAN_REQUEST.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;




                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_REQUEST.MODIFIED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    //lN_LOAN_REQUEST.LN_REQUEST_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    lN_LOAN_REQUEST.CREATED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST.CREATED_DATE = DateTime.Today;

                }

                lN_LOAN_REQUEST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_REQUEST.LN_REQUEST_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<LN_LOAN_REQUEST>(lN_LOAN_REQUEST);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<LN_LOAN_REQUEST>(lN_LOAN_REQUEST, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_LOAN_REQUEST>(lN_LOAN_REQUEST);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

       
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        
        private void showRequestType()
        {
            Share1.Visible = false;
            Share2.Visible = false;
            Share3.Visible = false;
            Share4.Visible = false;
            Property1.Visible = false;
            Property2.Visible = false;
            Property3.Visible = false;
          
            if (ddlRequestType.SelectedItem.Text.ToString().ToUpper() != "SHARE")
            {
                Property1.Visible = true;
                Property2.Visible = true;
                Property3.Visible = true;
            }
            else if (ddlRequestType.SelectedItem.Text.ToString().ToUpper() != "PROPERTY")
            {
                Share1.Visible = true;
                Share2.Visible = true;
                Share3.Visible = true;
                Share4.Visible = true;
               

            }
             
        }

        protected void ddlRequestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtShareName.Text = "";
            txtNoofshares.Text = "";
            txtSecIdentification.Text = "";
            txtValueofShares.Text = "";
            txtCertificateNumber.Text = "";
            txtFolioNo.Text = "";
            txtFromNo.Text = "";
            txtToNumber.Text = "";
            txtPropertyName.Text = "";
            txtPropertyId.Text = "";
            txtTitleDeedRef.Text = "";
            txtPropertyValue.Text = "";
            ddlLocations.ClearSelection();
            showRequestType();
        }





        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
