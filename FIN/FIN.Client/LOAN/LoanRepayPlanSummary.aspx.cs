﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.LOAN;
using FIN.DAL;
using FIN.DAL.LOAN;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.HR
{
    public partial class LoanRepayPlanSummary : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        LN_REPAY_HDR LN_REPAY_HDR = new LN_REPAY_HDR();
        LN_REPAY_DTL LN_REPAY_DTL = new LN_REPAY_DTL();
        System.Collections.SortedList slControls = new System.Collections.SortedList();

        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();


                EntityData = null;
                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (LN_REPAY_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        //lblCancelled.Visible = false;
                    }
                    if (LN_REPAY_DTL.POSTED_FLAG != null)
                    {
                        if (LN_REPAY_DTL.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;

                            imgBtnPost.Visible = false;
                            lblPosted.Visible = true;

                            imgBtnJVPrint.Visible = true;

                           // imgBtnCancel.Visible = true;
                           // lblCancelled.Visible = false;


                            //pnlgridview.Enabled = false;
                            pnltdHeader.Enabled = false;
                          //  pnlReason.Enabled = false;
                           // pnlRejReason.Enabled = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBank);

        }



        # region Save,Update and Delete

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_AMOUNT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_REV_PROFIT_SHARE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_REV_PROFIT_SHARE_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PS_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PS_PAID_AMT"))));
                    
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                dtGridData = DBMethod.ExecuteQuery(LoanRepayPlan_DAL.GetLoanNewRepayPlanDetails(ddlBank.SelectedValue, txtDate.Text)).Tables[0];
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                var tmpChildEntity = new List<Tuple<object, string>>();
                if (gvData.Rows.Count > 0)
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {


                        if (gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != string.Empty)
                        {
                            LN_REPAY_DTL  = new LN_REPAY_DTL();
                            using (IRepository<LN_REPAY_DTL> userCtx = new DataRepository<LN_REPAY_DTL>())
                            {
                                LN_REPAY_DTL = userCtx.Find(r =>
                                    (r.LN_PRN_REPAY_DTL_ID == gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }



                            TextBox txtPayAmt = gvData.Rows[i].FindControl("txtPayAmt") as TextBox;     
    
                            TextBox txtPayDate = gvData.Rows[i].FindControl("txtPayDate") as TextBox;

                            if (txtPayDate.Text.ToString().Length == 0)
                            {
                                txtPayDate.Text = txtDate.Text;
                            }
                            if (txtPayDate.Text.Length > 0 && txtPayAmt.Text.ToString().Length > 0)
                            {
                                LN_REPAY_DTL.LN_PAID_DT = DBMethod.ConvertStringToDate(txtPayDate.Text);
                                LN_REPAY_DTL.LS_PS_PAID_DT = DBMethod.ConvertStringToDate(txtPayDate.Text);
                                LN_REPAY_DTL.LN_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayAmt.Text);                               

                                LN_REPAY_DTL.MODIFIED_BY = this.LoggedUserName;
                                LN_REPAY_DTL.MODIFIED_DATE = DateTime.Today;

                                DBMethod.SaveEntity<LN_REPAY_DTL>(LN_REPAY_DTL, true);
                            }
                        }


                        if (gvData.DataKeys[i].Values["PS_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["PS_DTL_ID"].ToString() != string.Empty)
                        {
                            LN_INTEREST_DTL lN_INTEREST_DTL = new LN_INTEREST_DTL();
                            using (IRepository<LN_INTEREST_DTL> userCtx = new DataRepository<LN_INTEREST_DTL>())
                            {
                                lN_INTEREST_DTL = userCtx.Find(r =>
                                    (r.LN_INT_DTL_ID == gvData.DataKeys[i].Values["PS_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }



                            TextBox txtPayProfitShareAmt = gvData.Rows[i].FindControl("txtPayProfitShareAmt") as TextBox;

                            TextBox txtPayDate = gvData.Rows[i].FindControl("txtPayDate") as TextBox;
                            if (txtPayDate.Text.Length > 0 && txtPayProfitShareAmt.Text.ToString().Length > 0)
                            {
                                lN_INTEREST_DTL.LN_PS_PAID_DATE = DBMethod.ConvertStringToDate(txtPayDate.Text);
                                lN_INTEREST_DTL.LN_PS_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayProfitShareAmt.Text);

                                lN_INTEREST_DTL.MODIFIED_BY = this.LoggedUserName;
                                lN_INTEREST_DTL.MODIFIED_DATE = DateTime.Today;

                                DBMethod.SaveEntity<LN_INTEREST_DTL>(lN_INTEREST_DTL, true);
                            }
                        }

                    }
                }
                

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.SAVED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    LN_REPAY_DTL = (LN_REPAY_DTL)EntityData;
                }
                if (LN_REPAY_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.Call_AP_INVOICE_GL_POSTING(LN_REPAY_HDR.LN_PRN_REPAY_ID);

                }

                LN_REPAY_DTL =  LoanRepayPlan_BLL.getClassEntity(Master.StrRecordId);
                LN_REPAY_DTL.POSTED_FLAG = FINAppConstants.Y;
                LN_REPAY_DTL.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<LN_REPAY_DTL>(LN_REPAY_DTL, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


                imgBtnPost.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;

                //pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;
                //pnlReason.Enabled = false;
                //pnlRejReason.Enabled = false;

                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_REPAY_DTL.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            LN_REPAY_DTL = LoanRepayPlan_BLL.getClassEntity(Master.StrRecordId);

            if (LN_REPAY_DTL != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_REPAY_DTL.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }
        protected void btnPost_Click(object sender, EventArgs e)
        {
            if (gvData.Rows.Count > 0)
            {
                for (int i = 0; i < gvData.Rows.Count; i++)
                {
                    if (gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != string.Empty)
                    {
                        LN_REPAY_DTL = new LN_REPAY_DTL();
                        using (IRepository<LN_REPAY_DTL> userCtx = new DataRepository<LN_REPAY_DTL>())
                        {
                            LN_REPAY_DTL = userCtx.Find(r =>
                                (r.LN_PRN_REPAY_DTL_ID == gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                        
                        TextBox txtPayAmt = gvData.Rows[i].FindControl("txtPayAmt") as TextBox;

                        TextBox txtPayDate = gvData.Rows[i].FindControl("txtPayDate") as TextBox;
                        if (txtPayDate.Text.Length > 0 && txtPayAmt.Text.ToString().Length > 0)
                        {
                            if (decimal.Parse(txtPayAmt.Text) != FIN.BLL.CommonUtils.ConvertStringToDecimal(gvData.DataKeys[i].Values["LN_AMOUNT"].ToString()))
                            {
                                RegenerateProfitShare(i);
                            }
                        }
                    }
                }
            }
        }


        private void RegenerateProfitShare(int rowLoop)
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails("", gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString())).Tables[0];
            DataTable dt_Contract_Det = FIN.BLL.LOAN.LoanRepayPlan_BLL.fn_GetLoanRepayDtls4Contract(gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString());
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                DateTime compareTo = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_end_date"].ToString());
                DateTime now = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_start_date"].ToString());

                double dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString());
                double dbl_PSPercentage = double.Parse(dt_LoanReqDtl.Rows[0]["ln_roi"].ToString());
                var dateSpan = FIN.Client.LOAN.DateTimeSpan.CompareDates(compareTo, now);
                int int_Tot_Months = dateSpan.Months + 1;

                int int_calmonth = 0;
                int int_CalMonth = 0;

                DataTable dt_InterstDtl = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Interest_DAL.getInterestDetails4Contract(gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString())).Tables[0];
                  if (dt_InterstDtl.Rows.Count > 0)
                  {
                      if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper () == "MONTHLY")
                      {
                          int_CalMonth = 1;
                      }
                      else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "QUARTERLY")
                      {
                          int_CalMonth = 3;
                      }
                      else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "HALFYEARLY")
                      {
                          int_CalMonth = 6;
                      }
                      else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "ANNUAL")
                      {
                          int_CalMonth = 12;
                      }
                  }

                int_calmonth = int_Tot_Months / int_CalMonth;

                double dbl_totLoanAmt = double.Parse(dt_LoanReqDtl.Rows[0]["dbl_pr_amt"].ToString());
                double dbl_MonthAmt = dbl_totLoanAmt / int_calmonth;

                double dbl_PaidLoanAmt = 0;

                DateTime PreviousDt = now;
                double No_ofDays = 0;

                dtGridData = dt_InterstDtl.Copy();
                dtGridData.Rows.Clear();
                double dbl_PreviousPaidAmt = 0;
                for (int iLoop = 0; iLoop < int_calmonth; iLoop++)
                {

                    now = now.AddMonths(int_CalMonth);
                   
                    DataRow drList;
                    drList = dtGridData.NewRow();
                    drList["LN_INT_DTL_ID"] = "0";

                    drList["LN_INSTALLMENT_NO"] = iLoop + 1;
                    drList["LN_INSTALLMENT_DATE"] = now;


                    dateSpan = FIN.Client.LOAN.DateTimeSpan.CompareDates(now, PreviousDt);
                    No_ofDays = dateSpan.Days;

                    if (iLoop == 0)
                        No_ofDays += 1;

                    drList["LN_PROFIT_SHARE_AMT"] = ((No_ofDays / 365) * ((dbl_LoanAmount * dbl_PSPercentage) / 100)).ToString();

                    string str_Amt = dt_Contract_Det.Compute("sum(PAID_AMT)", "Paid_Date<'" + now + "'").ToString();
                    if (str_Amt.Length > 0)
                    {
                        dbl_PaidLoanAmt = double.Parse(str_Amt);
                    }

                    if (dbl_PreviousPaidAmt != dbl_PaidLoanAmt)
                    {
                        dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString()) - dbl_PaidLoanAmt;
                        dbl_PreviousPaidAmt = dbl_PaidLoanAmt;
                    }
                   
                    if(dbl_LoanAmount <=0)
                    {
                        dbl_LoanAmount=0;
                    }

                    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";

                    drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                    dtGridData.Rows.Add(drList);

                    PreviousDt = now;
                   

                }


                for (int kLoop = 0; kLoop < dtGridData.Rows.Count; kLoop++)
                {
                    dt_InterstDtl.Rows[kLoop]["LN_REV_PROFIT_SHARE_AMT"] = dtGridData.Rows[kLoop]["LN_PROFIT_SHARE_AMT"];
                }
                dt_InterstDtl.AcceptChanges();

                for (int kLoop = 0; kLoop < dt_InterstDtl.Rows.Count; kLoop++)
                {
                    LN_INTEREST_DTL lN_INTEREST_DTL = new LN_INTEREST_DTL();
                    using (IRepository<LN_INTEREST_DTL> userCtx = new DataRepository<LN_INTEREST_DTL>())
                    {
                        lN_INTEREST_DTL = userCtx.Find(r =>
                            (r.LN_INT_DTL_ID == dt_InterstDtl.Rows[0]["LN_INT_DTL_ID"].ToString())
                            ).SingleOrDefault();
                    }

                    lN_INTEREST_DTL.LN_REV_PROFIT_SHARE_AMT = decimal.Parse(dt_InterstDtl.Rows[kLoop]["LN_REV_PROFIT_SHARE_AMT"].ToString());
                    DBMethod.SaveEntity<LN_INTEREST_DTL>(lN_INTEREST_DTL, true);
                }

            }
        }


    }
}
