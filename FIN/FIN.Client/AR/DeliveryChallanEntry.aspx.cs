﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AR
{
    public partial class DeliveryChallanEntry : PageBase
    {
        OM_DC_HDR oM_DC_HDR = new OM_DC_HDR();
        OM_DC_DTL pM_DC_DTL = new OM_DC_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            Lookup_BLL.GetLookUpValues(ref ddlSalesOrder, "SLORD");
            Lookup_BLL.GetLookUpValues(ref ddlModeofTransport, "MOT");
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
        
                ddlEmployeeName.Enabled = false;
       
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                //txtRequisitionDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(DeliveryChallan_DAL.GetDeliveryChallanDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_DC_HDR = DeliveryChallan_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = oM_DC_HDR;


                    txtDCNumber.Text = oM_DC_HDR.OM_DC_ID;
                    txtDCDate.Text = DBMethod.ConvertDateToString(oM_DC_HDR.OM_DC_DATE.ToString());
                    if (oM_DC_HDR.OM_DC_CHECKED == FINAppConstants.Y)
                    {
                        chkInspected.Checked = true;
                        
                    }
                    else
                    {
                        chkInspected.Checked = false;
                        ddlEmployeeName.Enabled = false;
                    }
                    if (oM_DC_HDR.OM_EMP_ID != null)
                    {
                        ddlEmployeeName.SelectedValue = oM_DC_HDR.OM_EMP_ID.ToString();
                        ddlEmployeeName.Enabled = true;
                    }
                    txtShipmentNumber.Text = oM_DC_HDR.OM_SHIP_NUM;
                    txtShipmentNumber.Text = oM_DC_HDR.OM_SHIP_NUM;
                    txtShipmentDate.Text = DBMethod.ConvertDateToString(oM_DC_HDR.OM_SHIP_DATE.ToString());
                    txtShipmentTrackingNo.Text = oM_DC_HDR.OM_SHIP_TRACK_NO;
                    txtShipmentReceivedBy.Text = oM_DC_HDR.OM_SHIP_RECEIVED_BY;
                    ddlModeofTransport.SelectedValue = oM_DC_HDR.OM_TRANS_MODE;
                    txtShipmentfCompany.Text = oM_DC_HDR.OM_SHIP_COMPANY;
                    if (oM_DC_HDR.OM_SINGLE_ORD == FINAppConstants.Y)
                    {
                        chkSingleOrder.Checked = true;
                    }
                    else
                    {
                        chkSingleOrder.Checked = false;
                    }
                    ddlSalesOrder.SelectedValue = oM_DC_HDR.OM_SALE_ORDER;



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
               
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DCEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlOrderNumber = tmpgvr.FindControl("ddlOrderNumber") as DropDownList;

                SalesOrder_BLL.fn_GetOrderNo(ref ddlOrderNumber);
                


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    
                    ddlOrderNumber.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.OM_ORDER_ID].ToString();
                    fillItem(tmpgvr);
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void chkInspection_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (chkInspected.Checked)
                {
                    ddlEmployeeName.Enabled = true;
                }
                else
                {
                    ddlEmployeeName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtOrderLineNo = gvr.FindControl("txtOrderLineNo") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtShippedQuantity = gvr.FindControl("txtShippedQuantity") as TextBox;
            TextBox txtReturnedQuantity = gvr.FindControl("txtReturnedQuantity") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;

            DropDownList ddlOrderNumber = gvr.FindControl("ddlOrderNumber") as DropDownList;

            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["OM_DC_DTL_ID"] = "0";
                txtLineNo.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

           // txtLineNo.Text = (rowindex + 1).ToString();
            

            slControls[0] = txtLineNo;
            slControls[1] = ddlItemName;
            // slControls[2] = txtReturnedQuantity;
            slControls[2] = txtQuantity;
            slControls[3] = ddlOrderNumber;
            slControls[4] = txtShippedQuantity;
            //slControls[5] = txtReturnedQuantity;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Order_Number_P"] + " ~ " + Prop_File_Data["Shipped_Quantity_P"] + "";
            //string strMessage = "Line No ~ Item Name ~ Quantity ~ Order No ~ Shipped Quantity";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < CommonUtils.ConvertStringToInt(txtShippedQuantity.Text))
            {
                ErrorCollection.Add("rec234", Prop_File_Data["Quantity_Less_or_equal_P"]);
                    
            }

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (CommonUtils.ConvertStringToInt(txtReturnedQuantity.Text) > CommonUtils.ConvertStringToInt(txtShippedQuantity.Text))
            {
                ErrorCollection.Add("rec263", Prop_File_Data["Returned_Quantity_Less_or_equal_P"]);
                   
            }

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            drList["OM_DC_LINE_NUM"] = txtLineNo.Text;
            drList["OM_ORDER_ID"] = ddlOrderNumber.SelectedValue.ToString();
            drList["OM_ORDER_NUM"] = ddlOrderNumber.SelectedItem.Text.ToString();
            drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text.ToString();
            drList[FINColumnConstants.OM_ORDER_LINE_NUM] = txtOrderLineNo.Text;
            drList["OM_ITEM_QTY"] = txtQuantity.Text;
            drList["OM_SHIPPED_QTY"] = txtShippedQuantity.Text;

            drList["OM_RETURN_QTY"] = txtReturnedQuantity.Text == string.Empty ? "0" : txtReturnedQuantity.Text;
            drList["OM_REMARKS"] = txtRemarks.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DCEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_RD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_RE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_RC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    //tfn_UnitPricetxtChanged();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_RB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_BS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_DC_HDR = (OM_DC_HDR)EntityData;
                }



                if (txtDCDate.Text != string.Empty)
                {
                    oM_DC_HDR.OM_DC_DATE = DBMethod.ConvertStringToDate(txtDCDate.Text.ToString());
                }
                if (chkInspected.Checked == true)
                {
                    oM_DC_HDR.OM_DC_CHECKED = FINAppConstants.Y;
                }
                else
                {
                    oM_DC_HDR.OM_DC_CHECKED = FINAppConstants.N;
                }

                oM_DC_HDR.OM_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                oM_DC_HDR.OM_SHIP_NUM = txtShipmentNumber.Text;
                if (txtShipmentDate.Text != string.Empty)
                {
                    oM_DC_HDR.OM_SHIP_DATE = DBMethod.ConvertStringToDate(txtShipmentDate.Text.ToString());
                }
                oM_DC_HDR.OM_SHIP_TRACK_NO = txtShipmentTrackingNo.Text;
                oM_DC_HDR.OM_SHIP_RECEIVED_BY = txtShipmentReceivedBy.Text;
                oM_DC_HDR.OM_TRANS_MODE = ddlModeofTransport.SelectedValue;
                oM_DC_HDR.OM_SHIP_COMPANY = txtShipmentfCompany.Text;

                if (chkSingleOrder.Checked == true)
                {
                    oM_DC_HDR.OM_SINGLE_ORD = FINAppConstants.Y;
                }
                else
                {
                    oM_DC_HDR.OM_SINGLE_ORD = FINAppConstants.N;
                }

                oM_DC_HDR.OM_SALE_ORDER = ddlSalesOrder.SelectedValue;

                oM_DC_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                oM_DC_HDR.OM_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_DC_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_DC_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_DC_HDR.OM_DC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_006_M.ToString(), false, true);
                    oM_DC_HDR.CREATED_BY = this.LoggedUserName;
                    oM_DC_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_DC_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_DC_HDR.OM_DC_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Delivery Challan ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                var tmpChildEntity = new List<Tuple<object, string>>();
                OM_DC_DTL pM_DC_DTL = new OM_DC_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pM_DC_DTL = new OM_DC_DTL();
                    if (dtGridData.Rows[iLoop]["OM_DC_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_DC_DTL_ID"].ToString() != string.Empty)
                    {
                        pM_DC_DTL = DeliveryChallan_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["OM_DC_DTL_ID"].ToString());
                    }
                    pM_DC_DTL.OM_DC_LINE_NUM = (iLoop + 1);// CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_NUM].ToString());
                    //pM_DC_DTL.OM_DC_LINE_NUM = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["OM_DC_LINE_NUM"].ToString());
                    pM_DC_DTL.OM_ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    pM_DC_DTL.OM_ORDER_ID = (dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_ID].ToString());
                    pM_DC_DTL.OM_ORDER_LINE_NUM = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_LINE_NUM].ToString());
                    pM_DC_DTL.OM_ITEM_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_ITEM_QTY"].ToString());
                    pM_DC_DTL.OM_SHIPPED_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_SHIPPED_QTY"].ToString());
                    pM_DC_DTL.OM_RETURN_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_RETURN_QTY"].ToString());
                    pM_DC_DTL.OM_REMARKS = (dtGridData.Rows[iLoop]["OM_REMARKS"].ToString());
                    pM_DC_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    pM_DC_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    pM_DC_DTL.OM_DC_ID = oM_DC_HDR.OM_DC_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(pM_DC_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["OM_DC_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_DC_DTL_ID"].ToString() != string.Empty)
                        {
                            pM_DC_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pM_DC_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            pM_DC_DTL.OM_DC_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_006_D.ToString(), false, true);
                            pM_DC_DTL.CREATED_BY = this.LoggedUserName;
                            pM_DC_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pM_DC_DTL, FINAppConstants.Add));
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_DC_HDR, OM_DC_DTL>(oM_DC_HDR, tmpChildEntity, pM_DC_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<OM_DC_HDR, OM_DC_DTL>(oM_DC_HDR, tmpChildEntity, pM_DC_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<oM_DC_HDR>(oM_DC_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_BY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }








        #endregion

        protected void ddlOrderNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                //DropDownList ddlOrderNumber = gvr.FindControl("ddlOrderNumber") as DropDownList;
                //TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
                //TextBox txtOrderLineNo = gvr.FindControl("txtOrderLineNo") as TextBox;
                //DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;

                fillItem(gvr);

                //DataTable dtqty = new DataTable();

                //dtqty = DBMethod.ExecuteQuery(SalesOrder_DAL.GetSaleOrderDetails(ddlOrderNumber.SelectedValue)).Tables[0];

                //if (dtqty != null)
                //{
                //    if (dtqty.Rows.Count > 0)
                //    {
                //        txtQuantity.Text = dtqty.Rows[0]["om_order_qty"].ToString();
                //        txtOrderLineNo.Text = dtqty.Rows[0]["om_order_line_num"].ToString();
                //       // ddlItemName.SelectedValue = dtqty.Rows[0]["item_id"].ToString();
                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_sBY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void fillItem(GridViewRow gvr)
        {
            DropDownList ddlOrderNumber = gvr.FindControl("ddlOrderNumber") as DropDownList;
           
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            Item_BLL.getItemNameDetails_frARDeleiverychallan(ref ddlItemName, ddlOrderNumber.SelectedValue);
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
                TextBox txtShippedQuantity = gvr.FindControl("txtShippedQuantity") as TextBox;

                if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < CommonUtils.ConvertStringToInt(txtShippedQuantity.Text))
                {
                    ErrorCollection.Add("rec2", "Shipped quantity should be less than or equal to Sale order quantity");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DCs_BY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void txtReturnedQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtReturnedQuantity = gvr.FindControl("txtReturnedQuantity") as TextBox;
                TextBox txtShippedQuantity = gvr.FindControl("txtShippedQuantity") as TextBox;

                if (CommonUtils.ConvertStringToInt(txtReturnedQuantity.Text) > CommonUtils.ConvertStringToInt(txtShippedQuantity.Text))
                {
                    ErrorCollection.Add("rec23", "Returned quantity should be less than or equal to Shipped quantity");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DCs_BY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
                    

            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                //DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                //TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;

                //DataTable dtqty = new DataTable();
                //dtqty = DBMethod.ExecuteQuery(DeliveryChallan_DAL.GetItemQty(ddlItemName.SelectedValue)).Tables[0];
                //txtQuantity.Text = dtqty.Rows[0]["ITEM_REORDER_QTY"].ToString();
                //txtQuantity.Enabled = false;                

                DropDownList ddlOrderNumber = gvr.FindControl("ddlOrderNumber") as DropDownList;
                TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
                TextBox txtOrderLineNo = gvr.FindControl("txtOrderLineNo") as TextBox;
                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;

                

                DataTable dtqty = new DataTable();

                dtqty = DBMethod.ExecuteQuery(SalesOrder_DAL.GetSaleOrderDts_forDeliverychallan(ddlOrderNumber.SelectedValue, ddlItemName.SelectedValue)).Tables[0];

                if (dtqty != null)
                {
                    if (dtqty.Rows.Count > 0)
                    {
                        txtQuantity.Text = dtqty.Rows[0]["om_order_qty"].ToString();
                        txtOrderLineNo.Text = dtqty.Rows[0]["om_order_line_num"].ToString();
                        // ddlItemName.SelectedValue = dtqty.Rows[0]["item_id"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DC_sBY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

                        
        }

        protected void ddlSalesOrder_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}