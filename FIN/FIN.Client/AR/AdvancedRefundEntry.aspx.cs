﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.CA;
using FIN.BLL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.CA;
using VMVServices.Web;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;

namespace FIN.Client.AR
{
    public partial class AdvancedRefundEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        ADVANCED_REFUND aDVANCED_REFUND = new ADVANCED_REFUND();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        Boolean stopPayBool;
        string ProReturn = null;
        string trnType = string.Empty;
        int trnTypeBool = 0;

        bool lineNum = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    Session["PAY_OC"] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            object s = new object();
            EventArgs e = new EventArgs();

            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlCustomerNumber, false);
            Lookup_BLL.GetLookUpValues(ref ddlPaymentMode, "PAYMENT_MODE");
            Bank_BLL.fn_getBankName(ref ddlBankName);
            // Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);


            Currency_BLL.GetPaymentCurrency(ref ddlPaymentCurrency);
            ddlPaymentCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
            ddlPaymentCurrency_SelectedIndexChanged(s, e);
        }
        protected void ddlCustomerNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCustomerName.Text = ddlCustomerNumber.SelectedItem.Text.ToString();
            ddlPaymentCurrency_SelectedIndexChanged(sender, e);
            txtInvoiceAmount.Text = "";
            txtBalanceAmt.Text = "";
            txtAmtPaid.Text = "";
            txtRefundAmount.Text = "";
        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());
        }
        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }
        protected void ddlAccountnumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMode.SelectedValue.ToString().ToUpper() != "EFT")
            {
                Cheque_BLL.GetUnusedCheckNumberDetails(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue, Master.Mode);
                if (ddlChequeNumber.Items.Count > 1)
                {
                    ddlChequeNumber.SelectedIndex = 1;
                    //ddlChequeNumber.Enabled = false;
                }
            }

        }
        protected void ddlInvoiceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData = new DataTable();
            DataTable dtDataAmtPaid = new DataTable();
            Session["INVNUM"] = ddlInvoiceNumber.SelectedValue;
            dtData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceHeaderDetails(ddlInvoiceNumber.SelectedValue)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtInvoiceAmount.Text = (dtData.Rows[0][FINColumnConstants.INV_AMT].ToString());
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) > 0)
                    {
                        txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                    }
                }
                else
                {
                    txtInvoiceAmount.Text = "";
                }
            }

            dtDataAmtPaid = DBMethod.ExecuteQuery(FIN.DAL.AR.AdvancedRefund_DAL.getAdvancedRefundData(ddlInvoiceNumber.SelectedValue)).Tables[0];
            if (dtDataAmtPaid != null)
            {
                if (dtDataAmtPaid.Rows.Count > 0)
                {
                    txtAmtPaid.Text = (dtDataAmtPaid.Rows[0]["REFUND_AMT"].ToString());
                    txtBalanceAmt.Text = (dtDataAmtPaid.Rows[0]["BALANCE_AMT"].ToString());
                    if (CommonUtils.ConvertStringToDecimal(txtAmtPaid.Text) > 0)
                    {
                        txtAmtPaid.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmtPaid.Text);
                    }
                    if (CommonUtils.ConvertStringToDecimal(txtBalanceAmt.Text) > 0)
                    {
                        txtBalanceAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalanceAmt.Text);
                    }
                }
                else
                {
                    txtAmtPaid.Text = "";
                    txtBalanceAmt.Text = "";
                }
            }

        }
        protected void txtPayRetentionAmount_TextChanged(object sender, EventArgs e)
        {
        }

        protected void txtRefundAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (txtInvoiceAmount.Text.ToString().Length > 0)
                {
                    if (txtBalanceAmt.Text.ToString().Length > 0)
                    {
                        if (decimal.Parse(txtRefundAmount.Text.ToString()) > decimal.Parse(txtBalanceAmt.Text.ToString()))
                        {
                            ErrorCollection.Add("refamount", "Refund Amount should be less than Balance Amount");
                            return;
                        }
                    }
                    if (decimal.Parse(txtRefundAmount.Text.ToString()) > decimal.Parse(txtInvoiceAmount.Text.ToString()))
                    {
                        ErrorCollection.Add("refamount", "Refund Amount should be less than Invoice Amount");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("refunAmt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void PaymentCtrlEnableDisable()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            if (ddlPaymentMode.SelectedItem.Text == "Cash")
            {
                ddlBankName.Enabled = false;
                ddlBankBranch.Enabled = false;
                ddlAccountnumber.Enabled = false;

                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "Cheque")
            {
                ddlBankName.Enabled = true;
                ddlBankBranch.Enabled = true;
                ddlAccountnumber.Enabled = true;
                txtChequeReceivedBy.Enabled = true;
                txtCivilId.Enabled = true;

                ddlChequeNumber.Enabled = true;
                txtChequeDate.Enabled = true;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "EFT")
            {
                ddlBankName.Enabled = true;
                ddlBankBranch.Enabled = true;
                ddlAccountnumber.Enabled = true;

                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;
                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
            }
        }
        protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                PaymentCtrlEnableDisable();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlChequeNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dtData = new DataTable();

            //dtData = DBMethod.ExecuteQuery(Cheque_DAL.GetCheckNumberDetails(ddlChequeNumber.SelectedValue)).Tables[0];
            //if (dtData != null)
            //{
            //    if (dtData.Rows.Count > 0)
            //    {
            //        txtChequeDate.Text = dtData.Rows[0][FINColumnConstants.CHECK_DT].ToString();
            //    }
            //}
            if (ddlChequeNumber.SelectedValue != null)
            {
                txtChequeDate.Enabled = true;
            }

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                object s = new object();
                EventArgs e = new EventArgs();

                Session["InvoiceProgramID"] = null;
                Session["InvoiceMode"] = null;
                Session["InvoiceID"] = null;
                Session["StopPayment"] = null;


                ddlBankName.Enabled = false;
                ddlBankBranch.Enabled = false;
                ddlAccountnumber.Enabled = false;

                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;
               
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                txtPaymentDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                DataTable dt_PAY_OC = DBMethod.ExecuteQuery(FIN.DAL.AR.AdvancedRefund_DAL.getAdvancedRefundOtherCharges(Master.StrRecordId)).Tables[0];
                BindGrid_OC(dt_PAY_OC);

                imgBtnPost.Visible = false;
                btnChequePrint.Visible = false;
                btnStopPayment.Visible = false;
                btnPrint.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnStopPayJV.Visible = false;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = aDVANCED_REFUND;
                    //Posting -- temporarily commented
                    //btnStopPayment.Visible = true;
                    btnPrint.Visible = true;
                    txtPaymentNumber.Text = aDVANCED_REFUND.REFUND_ID;

                    if (aDVANCED_REFUND.PAY_DATE != null)
                    {
                        txtPaymentDate.Text = DBMethod.ConvertDateToString(aDVANCED_REFUND.PAY_DATE.ToString());
                    }
                    // Supplier_BLL.GetSupplierNumber_Name(ref ddlSupplierNumber);

                    ddlCustomerNumber.SelectedValue = aDVANCED_REFUND.PAY_VENDOR_ID.ToString();

                    if (aDVANCED_REFUND.ATTRIBUTE1 != null)
                    {
                        //ddlSupplierNumber.SelectedValue = INV_PAYMENTS_HDR.ATTRIBUTE1.ToString();
                        //ddlSupplierNumber.SelectedItem.Text = INV_PAYMENTS_HDR.ATTRIBUTE1.ToString();
                        txtCustomerName.Text = aDVANCED_REFUND.ATTRIBUTE1;
                    }

                    ddlPaymentMode.SelectedValue = aDVANCED_REFUND.PAY_MODE;

                    ddlBankName.SelectedValue = aDVANCED_REFUND.PAY_BANK_ID;
                    BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());

                    ddlBankBranch.SelectedValue = aDVANCED_REFUND.PAY_BANK_BRANCH_ID;
                    BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);

                    ddlAccountnumber.SelectedValue = aDVANCED_REFUND.PAY_ACCOUNT_ID;
                    Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue);


                    if (aDVANCED_REFUND.PAY_CHECK_NUMBER != null)
                    {
                        ddlChequeNumber.SelectedValue = aDVANCED_REFUND.PAY_CHECK_NUMBER;
                    }
                    if (aDVANCED_REFUND.PAY_CHECK_DATE != null)
                    {
                        txtChequeDate.Text = DBMethod.ConvertDateToString(aDVANCED_REFUND.PAY_CHECK_DATE.ToString());
                    }

                    txtCivilId.Text = aDVANCED_REFUND.PAY_CHECK_RECEV_CIVIL_ID;
                    //txtTotalPaymentAmt.Text = aDVANCED_REFUND.PAYMENT_AMT.ToString();
                    txtRefundAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(aDVANCED_REFUND.REFUND_AMT.ToString());

                    //if (aDVANCED_REFUND.GLOBAL_SEGMENT_ID != null)
                    //{
                    //    ddlGlobalSegment.SelectedValue = aDVANCED_REFUND.GLOBAL_SEGMENT_ID.ToString();
                    //}

                    if (aDVANCED_REFUND.EXC_RATE_TYPE_ID != null)
                    {
                        ddlExchangeRateType.SelectedValue = aDVANCED_REFUND.EXC_RATE_TYPE_ID.ToString();
                    }
                    if (aDVANCED_REFUND.EXC_RATE_VALUE != null)
                    {
                        txtExchangeRate.Text = aDVANCED_REFUND.EXC_RATE_VALUE.ToString();
                    }
                    if (aDVANCED_REFUND.PAYMENT_CURRENCY != null)
                    {
                        ddlPaymentCurrency.SelectedValue = aDVANCED_REFUND.PAYMENT_CURRENCY.ToString();
                    }
                    if (aDVANCED_REFUND.INVOICE_NUMBER != null)
                    {
                        ddlPaymentCurrency_SelectedIndexChanged(s, e);
                        ddlInvoiceNumber.SelectedValue = aDVANCED_REFUND.INVOICE_NUMBER;
                        ddlInvoiceNumber_SelectedIndexChanged(s, e);
                    }
                    if (aDVANCED_REFUND.PAY_CHECK_RECEIVED_BY != null)
                    {
                        txtChequeReceivedBy.Text = aDVANCED_REFUND.PAY_CHECK_RECEIVED_BY.ToString();
                        if (aDVANCED_REFUND.PAY_CHECK_RECEIVED_BY.ToString().Length > 0)
                        {
                            txtChequeReceivedBy.Enabled = false;
                            txtCivilId.Enabled = false;

                            CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                            using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                            {
                                cA_CHECK_DTL = userCtx.Find(r =>
                                    (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                                    ).SingleOrDefault();
                            }
                            if (cA_CHECK_DTL != null)
                            {
                                if (cA_CHECK_DTL.CHECK_STATUS.ToString() == "USED")
                                {
                                    btnStopPayment.Enabled = false;
                                }
                            }
                        }
                    }
                    PaymentCtrlEnableDisable();
                    ddlChequeNumber.Enabled = false;

                    if (aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                        {
                            btnChequePrint.Visible = true;
                        }
                        //imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                        btnStopPayment.Visible = false;
                    }
                    if (aDVANCED_REFUND.POSTED_FLAG != null)
                    {
                        if (aDVANCED_REFUND.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            //Posting -- temporarily commented
                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            //imgBtnJVPrint.Visible = true;
                            //btnStopPayment.Visible = true;
                            //lblPosted.Visible = true;
                            lblCancelled.Visible = false;


                            //pnltdHeader.Enabled = false;
                            //pnlAcc.Enabled = false;
                        }
                    }
                    if (aDVANCED_REFUND.REV_FLAG == FINAppConstants.Y)
                    {
                        //Posting -- temporarily commented
                        //lblCancelled.Visible = true;
                        btnStopPayment.Visible = false;
                        //imgBtnStopPayJV.Visible = true;
                    }

                    //Posting -- temporarily commented
                    //if (FINSP.Is_workflow_defined("AR_013"))
                    //{
                    //    if (aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS == "0")
                    //    {
                    //        lblWaitApprove.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        lblWaitApprove.Visible = false;
                    //    }
                    //}
                    //else
                    //{
                    //    lblWaitApprove.Visible = false;
                    //}
                }
                btnPrint.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    aDVANCED_REFUND = (ADVANCED_REFUND)EntityData;
                }
                if (txtPaymentDate.Text != string.Empty)
                {
                    aDVANCED_REFUND.PAY_DATE = DBMethod.ConvertStringToDate(txtPaymentDate.Text.ToString());
                }

                aDVANCED_REFUND.PAY_VENDOR_ID = ddlCustomerNumber.SelectedValue;
                aDVANCED_REFUND.ATTRIBUTE1 = txtCustomerName.Text;
                aDVANCED_REFUND.PAY_MODE = ddlPaymentMode.SelectedValue;
                aDVANCED_REFUND.PAY_BANK_ID = ddlBankName.SelectedValue;
                aDVANCED_REFUND.PAY_BANK_BRANCH_ID = ddlBankBranch.SelectedValue;
                aDVANCED_REFUND.PAY_ACCOUNT_ID = ddlAccountnumber.SelectedValue;
                aDVANCED_REFUND.PAY_CHECK_NUMBER = ddlChequeNumber.SelectedValue;
                aDVANCED_REFUND.PAY_CHECK_RECEV_CIVIL_ID = txtCivilId.Text;
                if (txtChequeDate.Text != string.Empty)
                {
                    aDVANCED_REFUND.PAY_CHECK_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text.ToString());
                }

                aDVANCED_REFUND.REFUND_AMT = CommonUtils.ConvertStringToDecimal(txtRefundAmount.Text);
                aDVANCED_REFUND.PAY_CHECK_RECEIVED_BY = txtChequeReceivedBy.Text;

                aDVANCED_REFUND.PAYMENT_CURRENCY = ddlPaymentCurrency.SelectedValue;
                aDVANCED_REFUND.EXC_RATE_TYPE_ID = ddlExchangeRateType.SelectedValue;
                aDVANCED_REFUND.EXC_RATE_VALUE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());
                aDVANCED_REFUND.INVOICE_NUMBER = ddlInvoiceNumber.SelectedValue;

                aDVANCED_REFUND.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                aDVANCED_REFUND.ENABLED_FLAG = FINAppConstants.Y;

                //aDVANCED_REFUND.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    aDVANCED_REFUND.MODIFIED_BY = this.LoggedUserName;
                    aDVANCED_REFUND.MODIFIED_DATE = DateTime.Today;

                    if (Session["StopPayment"] != null || Session["StopPayment"] != string.Empty)
                    {
                        if (txtStopDate.Text != string.Empty)
                        {
                            aDVANCED_REFUND.PAY_STOP_DATE = DBMethod.ConvertStringToDate(txtStopDate.Text.ToString());
                        }
                        aDVANCED_REFUND.PAY_STOP_REASON = txtStopPaymentReason.Text;
                        aDVANCED_REFUND.PAY_STOP_REMARKS = txtStopPayRemarks.Text;
                    }
                }
                else
                {
                    aDVANCED_REFUND.REFUND_ID = FINSP.GetSPFOR_SEQCode("AR_013".ToString(), false, true);
                    aDVANCED_REFUND.POSTED_FLAG = FINAppConstants.N;
                    aDVANCED_REFUND.CREATED_BY = this.LoggedUserName;
                    aDVANCED_REFUND.CREATED_DATE = DateTime.Today;
                }

                //aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS = "0";
                aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aDVANCED_REFUND.REFUND_ID);
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Payment ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                //string str_Cur_id = "";
                //if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                //{
                //    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //    {
                //        if (dtGridData.Rows[iLoop]["DELETED"].ToString() != FINAppConstants.Y)
                //        {
                //            DataTable dtData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceHeaderDetails(dtGridData.Rows[iLoop][FINColumnConstants.PAY_INVOICE_ID].ToString())).Tables[0];
                //            if (dtData.Rows.Count > 0)
                //            {
                //                if (dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString() != ddlPaymentCurrency.SelectedValue)
                //                {
                //                    if (str_Cur_id.Length == 0)
                //                    {
                //                        str_Cur_id = dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString();
                //                    }
                //                    else
                //                    {
                //                        if (dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString() != str_Cur_id)
                //                        {
                //                            ErrorCollection.Add("ONLY ONE OTHER CURRENCY IS ALLOWED", "Only One Other Currency Invoice Allowed");
                //                            return;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}


                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(aDVANCED_REFUND.REFUND_ID, txtPaymentDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<ADVANCED_REFUND>(aDVANCED_REFUND);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<ADVANCED_REFUND>(aDVANCED_REFUND, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DataTable dt_POC = new DataTable();
                if (Session["PAY_OC"] != null)
                {
                    dt_POC = (DataTable)Session["PAY_OC"];
                }

                if (dt_POC != null)
                {
                    for (int iLoop = 0; iLoop < dt_POC.Rows.Count; iLoop++)
                    {
                        ADVANCED_REFUND_OTHER_CHARGES aDVANCED_REFUND_OTHER_CHARGES = new ADVANCED_REFUND_OTHER_CHARGES();

                        if ((dt_POC.Rows[iLoop]["AROC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["AROC_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<ADVANCED_REFUND_OTHER_CHARGES> userCtx = new DataRepository<ADVANCED_REFUND_OTHER_CHARGES>())
                            {
                                aDVANCED_REFUND_OTHER_CHARGES = userCtx.Find(r =>
                                    (r.AROC_ID == (dt_POC.Rows[iLoop]["AROC_ID"].ToString()))
                                    ).SingleOrDefault();
                            }
                        }

                        aDVANCED_REFUND_OTHER_CHARGES.REFUND_ID = aDVANCED_REFUND.REFUND_ID;
                        aDVANCED_REFUND_OTHER_CHARGES.ATTRIBUTE1 = dt_POC.Rows[iLoop]["ACCT_CODE_ID"].ToString();
                        aDVANCED_REFUND_OTHER_CHARGES.ATTRIBUTE2 = dt_POC.Rows[iLoop]["AddSub_value"].ToString();
                        aDVANCED_REFUND_OTHER_CHARGES.AROC_AMOUNT = CommonUtils.ConvertStringToDecimal(dt_POC.Rows[iLoop]["AROC_AMOUNT"].ToString());

                        aDVANCED_REFUND_OTHER_CHARGES.REMARKS = dt_POC.Rows[iLoop]["REMARKS"].ToString();

                        aDVANCED_REFUND_OTHER_CHARGES.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                        aDVANCED_REFUND_OTHER_CHARGES.WORKFLOW_COMPLETION_STATUS = "1";
                        aDVANCED_REFUND_OTHER_CHARGES.ENABLED_FLAG = "1";

                        if (dt_POC.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            if ((dt_POC.Rows[iLoop]["AROC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["AROC_ID"].ToString() != string.Empty)
                            {
                                DBMethod.DeleteEntity<ADVANCED_REFUND_OTHER_CHARGES>(aDVANCED_REFUND_OTHER_CHARGES);
                            }
                        }
                        else
                        {
                            if ((dt_POC.Rows[iLoop]["AROC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["AROC_ID"].ToString() != string.Empty)
                            {
                                aDVANCED_REFUND_OTHER_CHARGES.MODIFIED_BY = this.LoggedUserName;
                                aDVANCED_REFUND_OTHER_CHARGES.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<ADVANCED_REFUND_OTHER_CHARGES>(aDVANCED_REFUND_OTHER_CHARGES, true);
                            }
                            else
                            {
                                aDVANCED_REFUND_OTHER_CHARGES.AROC_ID = FINSP.GetSPFOR_SEQCode("AR_013_OC".ToString(), false, true);
                                aDVANCED_REFUND_OTHER_CHARGES.CREATED_BY = this.LoggedUserName;
                                aDVANCED_REFUND_OTHER_CHARGES.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<ADVANCED_REFUND_OTHER_CHARGES>(aDVANCED_REFUND_OTHER_CHARGES);
                            }
                        }
                    }
                }
                if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                    using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                    {
                        cA_CHECK_DTL = userCtx.Find(r =>
                            (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                            ).SingleOrDefault();
                    }
                    if (cA_CHECK_DTL != null)
                    {
                        SUPPLIER_CUSTOMERS sUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                        using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                        {
                            sUPPLIER_CUSTOMERS = userCtx.Find(r =>
                                (r.VENDOR_ID == ddlCustomerNumber.SelectedValue.ToString())
                                ).SingleOrDefault();
                        }
                        if (sUPPLIER_CUSTOMERS != null)
                        {
                            cA_CHECK_DTL.CHECK_PAY_TO = sUPPLIER_CUSTOMERS.ATTRIBUTE1;
                        }
                        else
                        {
                            cA_CHECK_DTL.CHECK_PAY_TO = txtCustomerName.Text;
                        }
                        cA_CHECK_DTL.CHECK_DT = DBMethod.ConvertStringToDate(txtPaymentDate.Text);
                        cA_CHECK_DTL.CHECK_AMT = decimal.Parse(txtRefundAmount.Text);
                        cA_CHECK_DTL.CHECK_STATUS = "USED";
                        cA_CHECK_DTL.CHECK_CURRENCY_CODE = ddlPaymentCurrency.SelectedValue;
                        cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                        cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                    }
                }
                if (aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.PAYMENT_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (Master.Mode == FINAppConstants.WF)
                {
                    return;
                }
                if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    if (ddlChequeNumber.SelectedValue == string.Empty || txtChequeDate.Text == string.Empty)
                    {
                        ErrorCollection.Add("cheque", "Please select the cheque details(Cheque Number and Value Date");
                    }
                }
                else if (ddlPaymentMode.SelectedItem.Text == "EFT")
                {
                    if (ddlBankName.SelectedValue == string.Empty || ddlBankBranch.SelectedValue == string.Empty || ddlAccountnumber.SelectedValue == string.Empty)
                    {
                        ErrorCollection.Add("eft", "Please select the bank details (Bank Name,Bank Branch and Account Number");
                    }
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnUpdateStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    aDVANCED_REFUND = (ADVANCED_REFUND)EntityData;
                }
                if (txtStopDate.Text != string.Empty)
                {
                    aDVANCED_REFUND.PAY_STOP_DATE = DBMethod.ConvertStringToDate(txtStopDate.Text.ToString());
                }
                aDVANCED_REFUND.PAY_STOP_REASON = txtStopPaymentReason.Text;
                aDVANCED_REFUND.PAY_STOP_REMARKS = txtStopPayRemarks.Text;
                DBMethod.SaveEntity<ADVANCED_REFUND>(aDVANCED_REFUND, true);

                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {
                    SUPPLIER_CUSTOMERS sUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                    using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                    {
                        sUPPLIER_CUSTOMERS = userCtx.Find(r =>
                            (r.VENDOR_ID == ddlCustomerNumber.SelectedValue.ToString())
                            ).SingleOrDefault();
                    }
                    if (cA_CHECK_DTL.CHECK_STATUS.ToString() == "USED")
                    {
                        cA_CHECK_DTL.CHECK_STATUS = "CANCEL";
                        cA_CHECK_DTL.CHECK_CANCEL_REMARKS = txtStopPayRemarks.Text + " " + txtStopPaymentReason.Text;
                    }
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                }
                stopPayBool = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                popStopPayment.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //Session["StopPayment"] = "stop";
                popStopPayment.Hide();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        //protected void btnInvoiceOpen_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        string url = Server.MapPath("~/AP/InvoicePopup.aspx");
        //        Session["InvoiceProgramID"] = "30";
        //        Session["InvoiceMode"] = "U";
        //        Session["InvoiceID"] = Session["INVNUM"].ToString();

        //        ModalPopupExtender2.Show();

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("WRI_BTNS", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<ADVANCED_REFUND>(aDVANCED_REFUND);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPaymentDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillExchangeRate()
        {
            DataTable dtDat = new DataTable();
            dtDat = DBMethod.ExecuteQuery(Currency_DAL.getCurrencyBasedOrg(VMVServices.Web.Utils.OrganizationID, txtPaymentDate.Text)).Tables[0];

            if (dtDat != null)
            {
                if (dtDat.Rows.Count > 0)
                {
                    if (ddlPaymentCurrency.SelectedValue == dtDat.Rows[0][FINColumnConstants.CURRENCY_ID].ToString())
                    {
                        txtExchangeRate.Enabled = false;
                        txtExchangeRate.Text = "1";
                    }
                    else
                    {
                        txtExchangeRate.Enabled = true;
                        DataTable dtDat1 = new DataTable();
                        dtDat1 = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtPaymentDate.Text, ddlPaymentCurrency.SelectedValue.ToString())).Tables[0];

                        if (dtDat1 != null)
                        {
                            if (dtDat1.Rows.Count > 0)
                            {
                                if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "standard")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_STD_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "selling")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_SEL_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "buying")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_BUY_RATE"].ToString();
                                }
                            }
                            else
                            {
                                txtExchangeRate.Text = "1";
                            }
                        }
                        else
                        {
                            txtExchangeRate.Text = "1";
                        }
                    }
                }
            }
        }
        protected void ddlExchangeRateType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlPaymentCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtData = new DataTable();

                Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");
                if (ddlExchangeRateType.Items.Count >= 1)
                {
                    ddlExchangeRateType.SelectedIndex = 1;
                }
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = true;

                string str_currecny = "";
                if (ddlPaymentCurrency.SelectedValue.ToString().Length > 0)
                {
                    if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                    {
                        str_currecny = "";
                    }
                    else
                    {
                        str_currecny = ddlPaymentCurrency.SelectedValue;
                    }
                    //Invoice_BLL.getInvoice4Payment(ref ddlInvoiceNumber, ddlCustomerNumber.SelectedValue, Master.Mode, str_currecny);
                    FIN.BLL.AR.Invoice_BLL.getInvoiceForPayment(ref ddlInvoiceNumber, ddlCustomerNumber.SelectedValue, Master.Mode, str_currecny);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        //protected void OpenWindow(object sender, EventArgs e)
        //{
        //    string url = Server.MapPath("~/AP/InvoicePopup.aspx");
        //    Session["InvoiceProgramID"] = "30";
        //    Session["InvoiceMode"] = "U";
        //    Session["InvoiceID"] = Session["INVNUM"].ToString();

        //    //string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
        //    //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + url + "','','" + DbConsts.ReportProperties + "');", true);

        //}

        #endregion


        //Payment Other Charges


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gv_pay_OC_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                gv_pay_OC.EditIndex = -1;

                BindGrid_OC(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gv_pay_OC_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gv_pay_OC.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl_OC(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid_OC(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private DataRow AssignToGridControl_OC(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddl_AccountCode = gvr.FindControl("ddlAccountCode") as DropDownList;
            DropDownList ddlAddSub = gvr.FindControl("ddlAddSub") as DropDownList;
            TextBox txt_POCAmount = gvr.FindControl("txtAROCAmount") as TextBox;
            TextBox txt_POCRemarks = gvr.FindControl("txtAROCRemarks") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["AROC_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddl_AccountCode;
            slControls[1] = txt_POCAmount;
            slControls[2] = ddlAddSub;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DropDownList";
            string strMessage = "Account Code" + " ~ " + "Amount" + " ~ " + "Add or Deduct";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "ACCT_CODE_ID='" + ddl_AccountCode.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (CommonUtils.ConvertStringToDecimal(txt_POCAmount.Text) > 0)
            {
                if (CommonUtils.ConvertStringToDecimal(txt_POCAmount.Text.ToString()) > CommonUtils.ConvertStringToDecimal(txtRefundAmount.Text.ToString()))
                {
                    ErrorCollection.Add("refunAmtToPay", "Amount Paid should be less than or equal to Refund Amount");
                    return drList;
                }
            }

            drList["ACCT_CODE_ID"] = ddl_AccountCode.SelectedItem.Value;
            drList["ACCT_CODE_DESC"] = ddl_AccountCode.SelectedItem.Text;

            drList["AddSub_value"] = ddlAddSub.SelectedValue;
            drList["AROC_AMOUNT"] = txt_POCAmount.Text;

            drList["REMARKS"] = txt_POCRemarks.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gv_pay_OC_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gv_pay_OC.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl_OC(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }

                else
                {
                    gv_pay_OC.EditIndex = -1;
                    BindGrid_OC(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gv_pay_OC_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid_OC(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gv_pay_OC_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                gv_pay_OC.EditIndex = e.NewEditIndex;
                BindGrid_OC(dtGridData);
                GridViewRow gvr = gv_pay_OC.Rows[e.NewEditIndex];
                FillFooterGridCombo_OC(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gv_pay_OC_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gv_pay_OC.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_RowCrt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>



        private void FillFooterGridCombo_OC(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_AccountCode = tmpgvr.FindControl("ddlAccountCode") as DropDownList;
                AccountCodes_BLL.fn_getAccount(ref ddl_AccountCode);

                if (gv_pay_OC.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_AccountCode.SelectedValue = gv_pay_OC.DataKeys[gv_pay_OC.EditIndex].Values["ACCT_CODE_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gv_pay_OC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        private void BindGrid_OC(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["PAY_OC"] = dtData;

                if (dtData.Rows.Count > 0)
                {

                    //   hdOCAmt.Value = CommonUtils.CalculateTotalAmount(dtData, "POC_AMOUNT");
                    //txtTotalPaymentAmt.Text = (CommonUtils.ConvertStringToDecimal(HDTotPayment.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdTotRetnAmt.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdOCAmt.Value)).ToString();


                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AROC_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AROC_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gv_pay_OC.DataSource = dt_tmp;
                gv_pay_OC.DataBind();
                GridViewRow gvr = gv_pay_OC.FooterRow;
                FillFooterGridCombo_OC(gvr);

                string totalAmountPaid = CalculateTotalAmount(dtData);
                if (totalAmountPaid.ToString().Length > 0)
                {
                    if (decimal.Parse(totalAmountPaid) > decimal.Parse(txtRefundAmount.Text.ToString()))
                    {
                        ErrorCollection.Add("rAmt", "Amount should be equal to Refund Amount");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        public string CalculateTotalAmount(DataTable dtGridData)
        {
            double totInvItemAmt = 0;
            if (dtGridData.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    totInvItemAmt += double.Parse(dtGridData.Rows[iLoop]["AROC_AMOUNT"].ToString());

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == "1")
                    {
                        totInvItemAmt -= double.Parse(dtGridData.Rows[iLoop]["AROC_AMOUNT"].ToString());
                    }
                }
            }

            return Math.Round(totInvItemAmt,3,MidpointRounding.ToEven).ToString();
        }

        #endregion
        protected void imgBtnChequePrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = "AP_REPORTS/RPTChequePrintout_B.rpt";
                CA_BANK cA_BANK = new CA_BANK();
                using (IRepository<CA_BANK> userCtx = new DataRepository<CA_BANK>())
                {
                    cA_BANK = userCtx.Find(r =>
                        (r.BANK_ID == ddlBankName.SelectedValue.ToString())
                        ).SingleOrDefault();
                }


                if (cA_BANK != null)
                {
                    if (cA_BANK.ATTRIBUTE1 != null)
                    {
                        if (cA_BANK.ATTRIBUTE1.ToString().Trim().Length > 0)
                        {
                            ReportFile = cA_BANK.ATTRIBUTE1.ToString();
                        }
                    }

                }
                if (ddlChequeNumber.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("CHEQUEID", ddlChequeNumber.SelectedValue.ToString());

                    NumberToWord.ToWord toword = new NumberToWord.ToWord(CommonUtils.ConvertStringToDecimal(txtRefundAmount.Text));

                    htFilterParameter.Add("AMTINWORD", toword.ConvertToEnglish());

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.CA.Cheque_BLL.GetChequesReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnChequePrint_Click(object sender, EventArgs e)
        {

        }

        protected void btnPost_Click(object sender, EventArgs e)
        {

        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    aDVANCED_REFUND = (ADVANCED_REFUND)EntityData;
                }
                if (aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(aDVANCED_REFUND.REFUND_ID, "AR_013");
                }

                aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntityData(Master.StrRecordId);
                aDVANCED_REFUND.POSTED_FLAG = FINAppConstants.Y;
                aDVANCED_REFUND.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<ADVANCED_REFUND>(aDVANCED_REFUND, true);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                imgBtnPost.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;

                pnltdHeader.Enabled = false;
                pnlAcc.Enabled = false;


                ////Update
                //string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
                ////Create Connection  -- SQL

                //dtGridData = (DataTable)Session[FINSessionConstants.GridData];

                //SqlConnection con = new SqlConnection(connectionStringSQL);
                //con.Open();
                //DataSet dtSet = new DataSet();
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    string sqlQuery = "";
                //    sqlQuery += " select nvl(attribute6,0)  as attribute6 from inv_invoices_hdr where inv_id = '" + dtGridData.Rows[iLoop]["INV_ID"].ToString() + "'";
                //    sqlQuery += " and attribute5='MASONET'";

                //    dtSet = DBMethod.ExecuteQuery(sqlQuery);

                //    if (dtSet.Tables[0].Rows.Count > 0)
                //    {
                //        if (dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString() != "0")
                //        {
                //            decimal paid_amount = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INV_PAID_AMT"].ToString()) + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_INVOICE_PAID_AMT"].ToString());
                //            sqlQuery = "";
                //            if (paid_amount == CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["inv_amt"].ToString()))
                //            {
                //                sqlQuery = " update accountpayable set paidamount =" + paid_amount + ",status=4 where accountpayableid=" + dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString();
                //            }
                //            else
                //            {
                //                sqlQuery = " update accountpayable set paidamount =" + paid_amount + ",status=3 where accountpayableid=" + dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString();
                //            }

                //            SqlCommand cmd = new SqlCommand(sqlQuery, con);
                //            cmd.ExecuteNonQuery();
                //        }
                //    }
                //}
                //con.Close();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", aDVANCED_REFUND.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtPaymentDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtPaymentDate.Text);
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("REFUND_ID", Master.StrRecordId.ToString());
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetChequesReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

        }
        protected void imgStopPayment_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                if (EntityData != null)
                {
                    // INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                    aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntity(Master.StrRecordId);
                }
                if (aDVANCED_REFUND.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    if (aDVANCED_REFUND.POSTED_FLAG != null)
                    {
                        if (aDVANCED_REFUND.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(aDVANCED_REFUND.JE_HDR_ID, "AR_013_R", this.LoggedUserName);
                        }
                    }
                }
                btnStopPayment.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntity(Master.StrRecordId);

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                lblPosted.Visible = true;
                imgBtnPost.Visible = false;
                lblCancelled.Visible = true;

                imgBtnStopPayJV.Visible = true;
                btnStopPayment.Visible = false;

                pnltdHeader.Enabled = false;
                pnlAcc.Enabled = false;

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", aDVANCED_REFUND.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntity(Master.StrRecordId);

            if (aDVANCED_REFUND != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", aDVANCED_REFUND.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

        protected void imgBtnStopPayJV_Click(object sender, ImageClickEventArgs e)
        {
            aDVANCED_REFUND = FIN.BLL.AR.AdvancedRefund_BLL.getClassEntity(Master.StrRecordId);

            if (aDVANCED_REFUND != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", aDVANCED_REFUND.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

    }
}