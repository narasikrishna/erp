﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.Client.AR
{
    public partial class PickReleaseEntry : PageBase
    {
   
        //PO_HEADERS PO_HEADERS = new PO_HEADERS(); 
        //PO_LINES PO_LINES = new PO_LINES();
        OM_PICK_RELEASE_HDR oM_PICK_RELEASE_HDR = new OM_PICK_RELEASE_HDR();
        OM_PICK_RELEASE_DTL oM_PICK_RELEASE_DTL = new OM_PICK_RELEASE_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        
           # region Page Load
           /// <summary>
           /// when the pages is rendered and loaded for the first time execution goes here
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e"></param>
           /// 
           protected void Page_Load(object sender, EventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   if (!IsPostBack)
                   {
                       AssignToControl();
                       EntryLoadHeader();
                       if (VMVServices.Web.Utils.Multilanguage)
                           AssignLanguage();
                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("PL", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }
           private void EntryLoadHeader()
           {
               string str_Header = "";
               str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
               //div_FormHeader.InnerHtml = str_Header;
           }
           private void AssignLanguage()
           {
               ClsGridBase.ChangeLanguage();
           }

           private void Startup()
           {
               Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
               Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
               Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

               if (Master.Mode == FINAppConstants.Delete)
               {
                   btnSave.Visible = false;
                   btnDelete.Visible = true;
                   pnlConfirm.Attributes["display"] = "none";
               }
               else
               {
                   btnSave.Visible = true;
                   btnDelete.Visible = false;
                   pnlConfirm.Visible = false;
               }
               if (Master.Mode == FINAppConstants.Update)
               {
                   btnSave.Text = "Update";
               }
               UserRightsChecking();

           }
           /// <summary>
           /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e"></param>
           /// 
           private void UserRightsChecking()
           {
               if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
               {
                   VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                   if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                   {
                       btnSave.Visible = false;
                   }
               }
               if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
               {
                   if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                   {
                       btnSave.Visible = false;
                   }
               }
               if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
               {
                   if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                   {
                       btnDelete.Visible = false;
                   }
               }
           }
           /// <summary>
           /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
           /// </summary>
           private void FillComboBox()
           {

               DeliveryChallan_BLL.GetDCNo(ref ddlDCNumber);
           

           }
           /// <summary>
           /// To assign the controls to the Grade Master table entities
           /// </summary>
           private void AssignToControl()
           {
               try
               {
                   ErrorCollection.Clear();

                   Startup();
                   FillComboBox();

                   EntityData = null;
                   Session[FINSessionConstants.GridData] = null;

                   dtGridData = DBMethod.ExecuteQuery(PickRelease_DAL.getPickReleaseDetails(Master.StrRecordId)).Tables[0];
                   BindGrid(dtGridData);

                   if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                   {
                       oM_PICK_RELEASE_HDR = PickRelease_BLL.getClassEntity(Master.StrRecordId);

                       EntityData = oM_PICK_RELEASE_HDR;


                       txtPRNO.Text = oM_PICK_RELEASE_HDR.OM_PR_ID;


                       ddlDCNumber.SelectedValue = oM_PICK_RELEASE_HDR.OM_DC_ID.ToString();
                       Filldcdate();
                       ddlLineNumber.SelectedValue = oM_PICK_RELEASE_HDR.OM_DC_LINE_NUM.ToString();
                       Filldcdtls();
                       txtQuantityforthisLot.Text = oM_PICK_RELEASE_HDR.OM_ITEM_QTY.ToString();



                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }

           }
           private void BindGrid(DataTable dtData)
           {
               try
               {
                   ErrorCollection.Clear();
                   bol_rowVisiable = false;
                   Session[FINSessionConstants.GridData] = dtData;
                   DataTable dt_tmp = dtData.Copy();
                   if (dt_tmp.Rows.Count == 0)
                   {
                       DataRow dr = dt_tmp.NewRow();
                       dr[0] = "0";
                       dt_tmp.Rows.Add(dr);
                       bol_rowVisiable = true;
                   }
                   gvData.DataSource = dt_tmp;
                   gvData.DataBind();
                   GridViewRow gvr = gvData.FooterRow;
                   FillFooterGridCombo(gvr);

                   txtQuantityforthisLot.Text = CommonUtils.CalculateTotalAmount(dtData, "LOT_QTY");
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("SQEntry", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                   }
               }
           }
           private void FillFooterGridCombo(GridViewRow tmpgvr)
           {
               try
               {
                   ErrorCollection.Clear();

                   DropDownList ddlLotNumber = tmpgvr.FindControl("ddlLotNumber") as DropDownList;

                   DropDownList ddlWH = tmpgvr.FindControl("ddlWH") as DropDownList;

                   TextBox txtQuantity = tmpgvr.FindControl("txtQuantity") as TextBox;


                 //  PickRelease_BLL.fn_getLotNo(ref ddlLotNumber, hfitem.Value);
                //   FIN.BLL.AP.Warehouse_BLL.GetWareHouseData(ref ddlWH);
                   MaterialIssue_BLL.getWHDetails_fritem(ref ddlWH, hfitem.Value.ToString());



                   if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                   {
                       
                       ddlWH.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.INV_WH_ID].ToString();
                       filllot(tmpgvr);
                       ddlLotNumber.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();
                       Session["LOT_QTY"] = txtQuantity.Text;


                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("SQ_FillFootGrid", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }
           #endregion

           # region Grid Events
           /// <summary>
           /// The GridView control is entering Canceling mode
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
           protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
           {
               if (Session[FINSessionConstants.GridData] != null)
               {
                   dtGridData = (DataTable)Session[FINSessionConstants.GridData];
               }
               gvData.EditIndex = -1;

               BindGrid(dtGridData);

           }
           /// <summary>
           ///   The GridView control is entering row Command mode      
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

           protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
           {
               try
               {

                   ErrorCollection.Clear();
                   GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                   DataRow drList = null;
                   if (Session[FINSessionConstants.GridData] != null)
                   {
                       dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                   }

                   if (e.CommandName.Equals("FooterInsert"))
                   {
                       gvr = gvData.FooterRow;
                       if (gvr == null)
                       {
                           return;
                       }
                   }


                   if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                   {
                       drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                       if (ErrorCollection.Count > 0)
                       {
                           Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                           //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                           return;
                       }
                       dtGridData.Rows.Add(drList);
                       BindGrid(dtGridData);
                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }


           /// <summary>
           /// To assign the controls to grid view
           /// </summary>
           /// <param name="tmpdrlist">Datarow details</param>
           /// <param name="tmpgvr">Grid view objects</param>
           /// <returns></returns>

           private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
           {
               System.Collections.SortedList slControls = new System.Collections.SortedList();

               TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
               DropDownList ddlLotNumber = gvr.FindControl("ddlLotNumber") as DropDownList;
               DropDownList ddlWH = gvr.FindControl("ddlWH") as DropDownList;



               DataRow drList;
               DataTable dt_tmp = tmpdtGridData.Copy();
               if (GMode == "A")
               {
                   drList = dtGridData.NewRow();
                   drList["OM_PR_DTL_ID"] = "0";
               }
               else
               {
                   drList = dtGridData.Rows[rowindex];
                   dt_tmp.Rows.RemoveAt(rowindex);

               }


               slControls[0] = ddlWH;
               slControls[1] = ddlLotNumber;
               slControls[2] = txtQuantity;


               Dictionary<string, string> Prop_File_Data;
               Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
               ErrorCollection.Clear();
               string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
               string strMessage = Prop_File_Data["Warehouse_P"] + " ~ " + Prop_File_Data["Lot_Number_P"] + " ~ " + Prop_File_Data["Quantity_P"] + "";
              // string strMessage = "Lot No ~ Quantity";

               EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

               if (EmptyErrorCollection.Count > 0)
               {
                   ErrorCollection = EmptyErrorCollection;
                   return drList;
               }

               string strCondition = "LOT_ID='" + ddlLotNumber.SelectedValue.ToString() + "'";
               strMessage = FINMessageConstatns.RecordAlreadyExists;
               ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
               if (ErrorCollection.Count > 0)
               {
                   return drList;
               }


               DataTable dtlotqty = new DataTable();
               dtlotqty = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(ddlLotNumber.SelectedValue)).Tables[0];
               
               if (dtlotqty.Rows.Count > 0)
               {
                   //lot_qty = );
                   if ( int.Parse(txtQuantity.Text) > int.Parse(dtlotqty.Rows[0]["LOT_QTY"].ToString()))
                   {
                       ErrorCollection.Add("invLot", "Invalid lot quantity.");
                       return drList;
                   }
                   
               }
               // txtPOTotalAmount.Text = PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtPOAmount.Text));

               drList["INV_WH_ID"] = ddlWH.SelectedValue.ToString();
               drList["INV_WH_NAME"] = ddlWH.SelectedItem.Text.ToString();

               drList["LOT_ID"] = ddlLotNumber.SelectedValue.ToString();
               drList["LOT_NUMBER"] = ddlLotNumber.SelectedItem.Text.ToString();

               drList["LOT_QTY"] = txtQuantity.Text;



               drList[FINColumnConstants.DELETED] = FINAppConstants.N;


               return drList;

           }

           /// <summary>
           ///  The GridView control is entering row updating mode  
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e"></param>
           protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                   DataRow drList = null;

                   if (Session[FINSessionConstants.GridData] != null)
                   {
                       dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                   }
                   if (gvr == null)
                   {
                       return;
                   }

                   drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                       return;
                   }
                   gvData.EditIndex = -1;
                   BindGrid(dtGridData);
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("SQEntry", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }

           protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   if (Session[FINSessionConstants.GridData] != null)
                   {
                       dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                   }
                   DataRow drList = null;
                   drList = dtGridData.Rows[e.RowIndex];
                   drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                   BindGrid(dtGridData);
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }


           /// <summary>
           ///  The GridView control is entering edit mode
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

           protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   if (Session[FINSessionConstants.GridData] != null)
                   {
                       dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                   }
                   gvData.EditIndex = e.NewEditIndex;
                   BindGrid(dtGridData);
                   GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                   FillFooterGridCombo(gvr);
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }
           }

           /// <summary>
           /// The GridView control is entering row created mode
           /// To identify rowtype and created a row in the grid view control       
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

           protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                   {
                       GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                       gvData.Controls[0].Controls.AddAt(0, gvr);
                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }

           }


           /// <summary>
           /// The GridView control is entering edit mode
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e"></param>

           protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();
                   if (e.Row.RowType == DataControlRowType.DataRow)
                   {
                       e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                       if (bol_rowVisiable)
                           e.Row.Visible = false;

                       if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                       {
                           e.Row.Visible = false;
                       }

                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                       //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }

           }

           #endregion
        
        # region Save,Update and Delete
        /// <summary>
           /// Validate the controls ,Save the records and update the records into the database
           /// </summary>
           /// <param name="sender"></param>
           /// <param name="e"></param>

           protected void btnSave_Click(object sender, EventArgs e)
           {
               try
               {
                   ErrorCollection.Clear();

                   System.Collections.SortedList slControls = new System.Collections.SortedList();

                   slControls[0] = ddlDCNumber;                   
                   slControls[1] = txtDCDate;

                   Dictionary<string, string> Prop_File_Data;
                   Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                   ErrorCollection.Clear();

                   string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DATE_TIME;
                   string strMessage = Prop_File_Data["DC_Number_P"] + " ~ " + Prop_File_Data["DC_Date_P"] + "";
                   //string strMessage = "Quote Date ~ Quote Type ~ Customer Name ~ Description ~ Status";

                   EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                   if (EmptyErrorCollection.Count > 0)
                   {
                       ErrorCollection = EmptyErrorCollection;
                       return;
                   }
                   if (ErrorCollection.Count > 0)
                   {
                       return;
                   }
                   DataTable dtgetshiqty = new DataTable();

                   dtgetshiqty = DBMethod.ExecuteQuery(PickRelease_DAL.getShippedQty(hfitem.Value, ddlDCNumber.SelectedValue)).Tables[0];
                   if (dtgetshiqty.Rows.Count > 0)
                   {
                       HFShippedQty.Value= dtgetshiqty.Rows[0][0].ToString();
                       if (CommonUtils.ConvertStringToDecimal(txtQuantityforthisLot.Text) > CommonUtils.ConvertStringToDecimal(HFShippedQty.Value))
                       {
                           ErrorCollection.Add("validqty", "Sum of quantity should not exceed shipped quantity.");
                           return;
                       }
                   }
                       

                   AssignToBE();            

                   if (savedBool)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                   }
               }
               catch (Exception ex)
               {
                   ErrorCollection.Add("POR", ex.Message);
               }
               finally
               {
                   if (ErrorCollection.Count > 0)
                   {
                       Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                   }
               }

           }
            
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_PICK_RELEASE_HDR = (OM_PICK_RELEASE_HDR)EntityData;
                }

                //PO_HEADERS.PO_HEADER_ID = txtPONumber.Text;


                //  oM_PICK_RELEASE_HDR.QUOTE_DATE = DBMethod.ConvertStringToDate(txtQuoteDate.Text.ToString());
                oM_PICK_RELEASE_HDR.OM_DC_ID = ddlDCNumber.SelectedValue;

                oM_PICK_RELEASE_HDR.OM_DC_LINE_NUM = decimal.Parse(ddlLineNumber.SelectedValue.ToString());
                oM_PICK_RELEASE_HDR.OM_ORD_ID = hforderNo.Value;
                oM_PICK_RELEASE_HDR.OM_ITEM_ID = hfitem.Value;
                oM_PICK_RELEASE_HDR.OM_ORD_LINE_NUM = decimal.Parse(txtOrderLineNumber.Text.ToString());

                if (txtQuantityforthisLot.Text != "")
                {
                    oM_PICK_RELEASE_HDR.OM_ITEM_QTY = decimal.Parse(txtQuantityforthisLot.Text);
                }


                oM_PICK_RELEASE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_PICK_RELEASE_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_PICK_RELEASE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_PICK_RELEASE_HDR.OM_PR_ID = FINSP.GetSPFOR_SEQCode("AR_007_M".ToString(), false, true);

                    oM_PICK_RELEASE_HDR.CREATED_BY = this.LoggedUserName;
                    oM_PICK_RELEASE_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_PICK_RELEASE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_PICK_RELEASE_HDR.OM_PR_ID);
                oM_PICK_RELEASE_HDR.QUOTE_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Pick Release ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                OM_PICK_RELEASE_DTL oM_PICK_RELEASE_DTL = new OM_PICK_RELEASE_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    oM_PICK_RELEASE_DTL = new OM_PICK_RELEASE_DTL();

                    if (dtGridData.Rows[iLoop]["OM_PR_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_PR_DTL_ID"].ToString() != string.Empty)
                    {
                        oM_PICK_RELEASE_DTL = PickRelease_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["OM_PR_DTL_ID"].ToString());
                    }

                    oM_PICK_RELEASE_DTL.OM_LOT_ID = (dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString());

                    oM_PICK_RELEASE_DTL.OM_LOT_QTY = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["LOT_QTY"].ToString());
                    oM_PICK_RELEASE_DTL.OM_WH_ID = dtGridData.Rows[iLoop][FINColumnConstants.INV_WH_ID].ToString();
                    oM_PICK_RELEASE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    oM_PICK_RELEASE_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    oM_PICK_RELEASE_DTL.OM_PR_ID = oM_PICK_RELEASE_HDR.OM_PR_ID;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(oM_PICK_RELEASE_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["OM_PR_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_PR_DTL_ID"].ToString() != string.Empty)
                        {
                            oM_PICK_RELEASE_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_PICK_RELEASE_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            oM_PICK_RELEASE_DTL.OM_PR_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_007_D".ToString(), false, true);
                            oM_PICK_RELEASE_DTL.CREATED_BY = this.LoggedUserName;
                            oM_PICK_RELEASE_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_PICK_RELEASE_DTL, FINAppConstants.Add));
                        }
                    }
                }


                DataTable dtshipqty = new DataTable();
                dtshipqty = DBMethod.ExecuteQuery(PickRelease_DAL.getshippedqty(ddlDCNumber.SelectedValue)).Tables[0];
                if (dtshipqty.Rows.Count > 0)
                {
                    if (int.Parse(txtQuantityforthisLot.Text) > int.Parse(dtshipqty.Rows[0]["OM_SHIPPED_QTY"].ToString()))
                    {
                        ErrorCollection.Add("invsumqty", "Sum of quantity should not exceed shipped quantity.");
                        return;
                    }
                }
                


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_PICK_RELEASE_HDR, OM_PICK_RELEASE_DTL>(oM_PICK_RELEASE_HDR, tmpChildEntity, oM_PICK_RELEASE_DTL);
                            savedBool = true;
                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                DataTable dtQTY = new DataTable();
                                DataTable dtbalqty = new DataTable();
                                decimal dtrecqty = 0;
                                decimal balqty = 0;
                                dtQTY = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY"].ToString());
                                balqty = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LOT_QTY"].ToString());
                                HFlotqty.Value = (dtrecqty - balqty).ToString();
                                PickRelease_DAL.UPDATE_Receiptlot_dtl(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                                FINSP.GetSPItemtxnLedger("ISSUE", dtQTY.Rows[0]["ITEM_ID"].ToString(), dtGridData.Rows[iLoop]["LOT_QTY"].ToString(), dtGridData.Rows[iLoop][FINColumnConstants.INV_WH_ID].ToString(), DateTime.Now.ToString(), dtQTY.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ISSUES of " + dtQTY.Rows[0]["ITEM_ID"].ToString());

                            }

                            break;
                        }
                    case FINAppConstants.Update:
                        {


                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                DataTable dtQTY = new DataTable();
                                DataTable QtyfromPR = new DataTable();
                                decimal dtrecqty = 0;
                                decimal balqty = 0;
                                decimal qtyPR = 0;
                                decimal Finlqty = 0;

                                dtQTY = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                QtyfromPR = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty_fromPR(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY"].ToString());
                                qtyPR = CommonUtils.ConvertStringToDecimal(QtyfromPR.Rows[0]["OM_LOT_QTY"].ToString());
                                balqty = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LOT_QTY"].ToString());
                                Finlqty = (qtyPR - balqty);

                                HFlotqty.Value = (dtrecqty + Finlqty).ToString();
                                PickRelease_DAL.UPDATE_Receiptlot_dtl(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));
                                // FINSP.GetSPItemtxnLedger("ADJUST", dtQTY.Rows[0]["ITEM_ID"].ToString(), dtGridData.Rows[iLoop]["LOT_QTY"].ToString(), dtGridData.Rows[iLoop][FINColumnConstants.INV_WH_ID].ToString(), DateTime.Now.ToString(), dtQTY.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ADJUST of " + dtQTY.Rows[0]["ITEM_ID"].ToString());

                            }


                            CommonUtils.SavePCEntity<OM_PICK_RELEASE_HDR, OM_PICK_RELEASE_DTL>(oM_PICK_RELEASE_HDR, tmpChildEntity, oM_PICK_RELEASE_DTL, true);
                            savedBool = true;


                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                DataTable dtQTY = new DataTable();
                                DataTable QtyfromPR = new DataTable();
                                decimal dtrecqty = 0;
                                decimal balqty = 0;
                                decimal qtyPR = 0;
                                decimal Finlqty = 0;

                                dtQTY = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                QtyfromPR = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty_fromPR(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY"].ToString());
                                qtyPR = CommonUtils.ConvertStringToDecimal(QtyfromPR.Rows[0]["OM_LOT_QTY"].ToString());
                                balqty = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LOT_QTY"].ToString());
                                Finlqty = (qtyPR - balqty);

                                HFlotqty.Value = (dtrecqty + Finlqty).ToString();
                               // PickRelease_DAL.UPDATE_Receiptlot_dtl(dtGridData.Rows[iLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));
                                FINSP.GetSPItemtxnLedger("ADJUST", dtQTY.Rows[0]["ITEM_ID"].ToString(), dtGridData.Rows[iLoop]["LOT_QTY"].ToString(), dtGridData.Rows[iLoop][FINColumnConstants.INV_WH_ID].ToString(), DateTime.Now.ToString(), dtQTY.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ADJUST of " + dtQTY.Rows[0]["ITEM_ID"].ToString());

                            }


                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

      

       

        protected void ddlLotNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlLotNumber = gvr.FindControl("ddlLotNumber") as DropDownList;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            DataTable dtQTY = new DataTable();
            dtQTY = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(ddlLotNumber.SelectedValue)).Tables[0];
            txtQuantity.Text = dtQTY.Rows[0]["LOT_QTY"].ToString();

        }

        protected void ddlDCNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filldcdate();
        }
        private void Filldcdate()
        {
            DeliveryChallan_BLL.GetDCLineNo_fr_PR(ref ddlLineNumber, ddlDCNumber.SelectedValue);

            DataTable dtdate = new DataTable();
            dtdate = DBMethod.ExecuteQuery(PickRelease_DAL.getDcdate(ddlDCNumber.SelectedValue)).Tables[0];
            txtDCDate.Text = DBMethod.ConvertDateToString(dtdate.Rows[0]["OM_DC_DATE"].ToString());
        }

        #endregion

        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filldcdtls();
            dtGridData = DBMethod.ExecuteQuery(PickRelease_DAL.getPickReleaseDetails(Master.StrRecordId)).Tables[0];
            BindGrid(dtGridData);
        }

        private void Filldcdtls()
        {
            
            DataTable dtdcdtl = new DataTable();
            dtdcdtl = DBMethod.ExecuteQuery(PickRelease_DAL.getDcdtl(int.Parse(ddlLineNumber.SelectedValue.ToString()),ddlDCNumber.SelectedValue)).Tables[0];
            txtOrderNumber.Text = dtdcdtl.Rows[0]["OM_ORDER_NUM"].ToString();
            txtItemDescription.Text = dtdcdtl.Rows[0]["ITEM_NAME"].ToString();
            txtOrderLineNumber.Text = dtdcdtl.Rows[0]["OM_ORDER_LINE_NUM"].ToString();
            hforderNo.Value = dtdcdtl.Rows[0]["OM_ORDER_ID"].ToString();
            hfitem.Value = dtdcdtl.Rows[0]["ITEM_ID"].ToString();
        }

        protected void ddlWH_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            filllot(gvr);
        }

        private void filllot(GridViewRow gvr)
        {
            DropDownList ddlLotNumber = gvr.FindControl("ddlLotNumber") as DropDownList;

            DropDownList ddlWH = gvr.FindControl("ddlWH") as DropDownList;
            MaterialIssue_BLL.GetLotDetails(ref ddlLotNumber, ddlWH.SelectedValue.ToString(), hfitem.Value.ToString());
        }
       


    }

}

