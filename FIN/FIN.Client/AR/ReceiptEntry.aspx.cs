﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AR;
using FIN.DAL.CA;
using FIN.BLL.GL;
using FIN.BLL;
using FIN.BLL.AR;
using FIN.BLL.CA;
using VMVServices.Web;

using VMVServices.Services.Data;

namespace FIN.Client.AP
{
    public partial class ReceiptEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        OM_CUST_PAYMENT_DTL OM_CUST_PAYMENT_DTL = new OM_CUST_PAYMENT_DTL();
        OM_CUST_PAYMENT_HDR OM_CUST_PAYMENT_HDR = new OM_CUST_PAYMENT_HDR();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        Boolean stopPayBool;
        string ProReturn = null;
        string trnType = string.Empty;
        int trnTypeBool = 0;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlSupplierNumber);
            Lookup_BLL.GetLookUpValues(ref ddlPaymentMode, "PAYMENT_MODE");
            //  Bank_BLL.fn_getBankName(ref ddlBankName);
            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);

            //  AccountCodes_BLL.getAccCodeBasedOrg(ref  ddlAccountCode, VMVServices.Web.Utils.OrganizationID);


            Currency_BLL.GetARPaymentCurrency(ref ddlPaymentCurrency);

        }
        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtSupplierName.Text = ddlSupplierNumber.SelectedValue.ToString();
            Fillcustomer();
        }
        private void Fillcustomer()
        {
            txtSupplierName.Text = ddlSupplierNumber.SelectedItem.Text.ToString();
            HFCustomerid.Value = ddlSupplierNumber.SelectedValue.ToString();
            dtGridData = DBMethod.ExecuteQuery(SalesPayment_DAL.getPaymentDetails(Master.StrRecordId)).Tables[0];

            BindGrid(dtGridData);
        }

        protected void ddlSOType_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            SOTypeChange(gvr);


        }

        public void SOTypeChange(GridViewRow gvr)
        {
            DropDownList ddl_SOType = gvr.FindControl("ddlSOType") as DropDownList;
            DropDownList ddl_InvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
            TextBox txt_Others = gvr.FindControl("txtOthers") as TextBox;
            TextBox txt_InvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
            if (ddl_SOType.SelectedValue.ToString().ToUpper() == "INVOICE")
            {
                ddl_InvoiceNumber.Visible = true;
                txt_Others.Visible = false;
                txt_InvoiceDate.Text = "";
            }
            else
            {
                ddl_InvoiceNumber.Visible = false;
                txt_Others.Visible = true;
                txt_InvoiceDate.Text = txtPaymentDate.Text;
            }
        }
        //protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, txtBank.Text.ToString());
        //}
        //protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, txtBank.Text, ddlBankBranch.SelectedValue);
        //}
        //protected void ddlAccountnumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, txtBank.Text, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue);
        //}
        protected void ddlInvoiceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
            TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
            TextBox txtInvoiceAmount = gvr.FindControl("txtInvoiceAmount") as TextBox;
            TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;
            TextBox txtAmountPaid = gvr.FindControl("txtAmountPaid") as TextBox;

            DataTable dtData = new DataTable();
            DataTable dtgetAmtreceived = new DataTable();

            Session["INVNUM"] = ddlInvoiceNumber.SelectedValue;
            dtData = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getInvoiceHeaderDetails(ddlInvoiceNumber.SelectedValue)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtInvoiceDate.Text = DBMethod.ConvertDateToString(dtData.Rows[0]["OM_INV_DATE"].ToString());
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(dtData.Rows[0]["OM_INV_AMT"].ToString());
                    txtRetentionAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(dtData.Rows[0]["OM_INV_RETENTION_AMT"].ToString());
                }
            }

            dtgetAmtreceived = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesPayment_DAL.getAmtreceivedfrom_Receipt(ddlInvoiceNumber.SelectedValue)).Tables[0];
            if (dtgetAmtreceived != null)
            {
                if (dtgetAmtreceived.Rows.Count > 0)
                {

                    txtAmountPaid.Text = DBMethod.GetAmtDecimalCommaSeparationValue(dtgetAmtreceived.Rows[0]["cust_inv_amt"].ToString());

                }
            }


        }
        private void PaymentCtrlEnableDisable()
        {
            //Bank_BLL.fn_getBankName(ref ddlBankName);
            //BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, "0");
            //BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, "0", "0");
            //Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, "0", "0", "0");

            txtChequeDate.Text = string.Empty;
            txtChequeReceivedBy.Text = string.Empty;



            if (ddlPaymentMode.SelectedItem.Text == "Cash")
            {
                txtBank.Enabled = false;
                txtBankBranch.Enabled = false;
                //  ddlAccountnumber.Enabled = false;

                txtChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "Cheque")
            {
                txtBank.Enabled = true;
                txtBankBranch.Enabled = true;
                //  ddlAccountnumber.Enabled = true;
                txtChequeReceivedBy.Enabled = true;

                txtChequeNumber.Enabled = true;
                txtChequeDate.Enabled = true;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "EFT")
            {
                txtBank.Enabled = true;
                txtBankBranch.Enabled = true;
                //  ddlAccountnumber.Enabled = true;

                txtChequeReceivedBy.Enabled = false;
                txtChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
            }
        }
        protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                PaymentCtrlEnableDisable();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlChequeNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dtData = new DataTable();

            //dtData = DBMethod.ExecuteQuery(Cheque_DAL.GetCheckNumberDetails(ddlChequeNumber.SelectedValue)).Tables[0];
            //if (dtData != null)
            //{
            //    if (dtData.Rows.Count > 0)
            //    {
            //        txtChequeDate.Text = dtData.Rows[0][FINColumnConstants.CHECK_DT].ToString();
            //    }
            //}
            if (txtChequeNumber.Text != string.Empty)
            {
                txtChequeDate.Enabled = true;
            }

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                Session[FINSessionConstants.GridData] = null;
                Session["InvoiceProgramID"] = null;
                Session["InvoiceMode"] = null;
                Session["InvoiceID"] = null;
                EntityData = null;

                txtBank.Enabled = false;
                txtBankBranch.Enabled = false;
                //  ddlAccountnumber.Enabled = false;

                txtChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                txtPaymentDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(SalesPayment_DAL.getPaymentDetails(Master.StrRecordId)).Tables[0];

                BindGrid(dtGridData);
                btnPrint.Visible = false;


                imgBtnPost.Visible = false;

                btnStopPayment.Visible = false;
                btnPrint.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnStopPayJV.Visible = false;
                imgBtnCancelReceipt.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    btnPrint.Visible = true;
                    imgBtnCancelReceipt.Visible = true;

                    OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = OM_CUST_PAYMENT_HDR;


                    txtPaymentNumber.Text = OM_CUST_PAYMENT_HDR.CUST_PAY_ID;

                    if (OM_CUST_PAYMENT_HDR.PAY_DATE != null)
                    {
                        txtPaymentDate.Text = DBMethod.ConvertDateToString(OM_CUST_PAYMENT_HDR.PAY_DATE.ToString());
                    }
                    FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlSupplierNumber);

                    //ddlSupplierNumber.SelectedItem.Text = OM_CUST_PAYMENT_HDR.CUST_VENDOR_ID.ToString();
                    //txtSupplierName.Text = ddlSupplierNumber.SelectedValue.ToString();

                    ddlSupplierNumber.SelectedValue = OM_CUST_PAYMENT_HDR.CUST_VENDOR_ID.ToString();
                    txtSupplierName.Text = ddlSupplierNumber.SelectedItem.Text.ToString();
                    Fillcustomer();
                    ddlPaymentMode.SelectedValue = OM_CUST_PAYMENT_HDR.CUST_PAY_MODE;

                    txtBank.Text = OM_CUST_PAYMENT_HDR.CUST_PAY_BANK_ID;
                    //BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, txtBank.Text.ToString());

                    txtBankBranch.Text = OM_CUST_PAYMENT_HDR.CUST_PAY_BANK_BRANCH_ID;
                    //BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, txtBank.Text, ddlBankBranch.SelectedValue);

                    //ddlAccountnumber.SelectedValue = OM_CUST_PAYMENT_HDR.CUST_PAY_ACCT_ID;
                    //Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, txtBank.Text, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue);


                    if (OM_CUST_PAYMENT_HDR.CUST_PAY_CHECK_NUMBER != null)
                    {
                        txtChequeNumber.Text = OM_CUST_PAYMENT_HDR.CUST_PAY_CHECK_NUMBER;
                    }
                    if (OM_CUST_PAYMENT_HDR.PAY_CHECK_DATE != null)
                    {
                        txtChequeDate.Text = DBMethod.ConvertDateToString(OM_CUST_PAYMENT_HDR.PAY_CHECK_DATE.ToString());
                    }
                    if (OM_CUST_PAYMENT_HDR.PAY_CHECK_RECEIVED_BY != null)
                    {
                        txtChequeReceivedBy.Text = (OM_CUST_PAYMENT_HDR.PAY_CHECK_RECEIVED_BY.ToString());
                    }
                    txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(OM_CUST_PAYMENT_HDR.PAYMENT_AMT.ToString());

                    if (OM_CUST_PAYMENT_HDR.GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = OM_CUST_PAYMENT_HDR.GLOBAL_SEGMENT_ID.ToString();
                    }
                    if (OM_CUST_PAYMENT_HDR.PAYMENT_CURRENCY != null)
                    {
                        ddlPaymentCurrency.SelectedValue = OM_CUST_PAYMENT_HDR.PAYMENT_CURRENCY.ToString();
                    }
                    Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");
                    if (OM_CUST_PAYMENT_HDR.EXC_RATE_TYPE_ID != null)
                    {
                        ddlExchangeRateType.SelectedValue = OM_CUST_PAYMENT_HDR.EXC_RATE_TYPE_ID.ToString();
                    }
                    if (OM_CUST_PAYMENT_HDR.EXC_RATE_VALUE != null)
                    {
                        txtExchangeRate.Text = OM_CUST_PAYMENT_HDR.EXC_RATE_VALUE.ToString();
                    }

                    if (OM_CUST_PAYMENT_HDR.PAY_CHECK_RECEIVED_BY != null)
                    {
                        txtChequeReceivedBy.Text = OM_CUST_PAYMENT_HDR.PAY_CHECK_RECEIVED_BY.ToString();
                    }
                    PaymentCtrlEnableDisable();


                    if (OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {

                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                        btnStopPayment.Visible = false;
                    }
                    if (OM_CUST_PAYMENT_HDR.POSTED_FLAG != null)
                    {
                        if (OM_CUST_PAYMENT_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            imgBtnCancelReceipt.Visible = false;
                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            imgBtnJVPrint.Visible = true;
                            btnStopPayment.Visible = true;
                            lblPosted.Visible = true;
                            lblCancelled.Visible = false;

                            pnlgridview.Enabled = false;
                            pnltdHeader.Enabled = false;

                        }
                    }
                    if (OM_CUST_PAYMENT_HDR.REV_FLAG != null)
                    {
                        if (OM_CUST_PAYMENT_HDR.REV_FLAG == FINAppConstants.Y)
                        {
                            imgBtnCancelReceipt.Visible = false;
                            lblCancelled.Visible = true;
                            btnStopPayment.Visible = false;
                            imgBtnStopPayJV.Visible = true;
                        }

                    }
                    if (OM_CUST_PAYMENT_HDR.REV_JE_HDR_ID != null)
                    {
                        if (OM_CUST_PAYMENT_HDR.REV_JE_HDR_ID .ToString().Length >0)
                        {                           
                            imgBtnStopPayJV.Visible = true;
                        }

                    }

                    if (FINSP.Is_workflow_defined("AP_022"))
                    {
                        if (OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "0")
                        {
                            lblWaitApprove.Visible = true;
                        }
                        else
                        {
                            lblWaitApprove.Visible = false;
                        }
                    }
                    else
                    {
                        lblWaitApprove.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    OM_CUST_PAYMENT_HDR = (OM_CUST_PAYMENT_HDR)EntityData;
                }
                if (txtPaymentDate.Text != string.Empty)
                {
                    OM_CUST_PAYMENT_HDR.PAY_DATE = DBMethod.ConvertStringToDate(txtPaymentDate.Text.ToString());
                }

                OM_CUST_PAYMENT_HDR.CUST_VENDOR_ID = ddlSupplierNumber.SelectedValue;
                OM_CUST_PAYMENT_HDR.CUST_PAY_MODE = ddlPaymentMode.SelectedValue;
                OM_CUST_PAYMENT_HDR.CUST_PAY_BANK_ID = txtBank.Text;
                OM_CUST_PAYMENT_HDR.CUST_PAY_BANK_BRANCH_ID = txtBankBranch.Text;
                //     OM_CUST_PAYMENT_HDR.CUST_PAY_ACCT_ID = txtAccountnumber.Text;
                OM_CUST_PAYMENT_HDR.CUST_PAY_CHECK_NUMBER = txtChequeNumber.Text;//.SelectedValue;
                if (txtChequeDate.Text != string.Empty)
                {
                    OM_CUST_PAYMENT_HDR.PAY_CHECK_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text.ToString());
                }

                OM_CUST_PAYMENT_HDR.PAYMENT_AMT = CommonUtils.ConvertStringToDecimal(txtTotalPaymentAmt.Text);
                OM_CUST_PAYMENT_HDR.PAY_CHECK_RECEIVED_BY = txtChequeReceivedBy.Text;

                OM_CUST_PAYMENT_HDR.PAYMENT_CURRENCY = ddlPaymentCurrency.SelectedValue;
                OM_CUST_PAYMENT_HDR.EXC_RATE_TYPE_ID = ddlExchangeRateType.SelectedValue;
                OM_CUST_PAYMENT_HDR.EXC_RATE_VALUE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());


                OM_CUST_PAYMENT_HDR.CUST_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                OM_CUST_PAYMENT_HDR.ENABLED_FLAG = FINAppConstants.Y;

                OM_CUST_PAYMENT_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    OM_CUST_PAYMENT_HDR.MODIFIED_BY = this.LoggedUserName;
                    OM_CUST_PAYMENT_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    OM_CUST_PAYMENT_HDR.CUST_PAY_ID = FINSP.GetSPFOR_SEQCode("AR_010_M", false, true);
                    txtPaymentNumber.Text = OM_CUST_PAYMENT_HDR.CUST_PAY_ID;
                    OM_CUST_PAYMENT_HDR.CREATED_BY = this.LoggedUserName;
                    OM_CUST_PAYMENT_HDR.CREATED_DATE = DateTime.Today;
                }

                OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS = "0";

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Receipt ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    OM_CUST_PAYMENT_DTL = new OM_CUST_PAYMENT_DTL();

                    if ((dtGridData.Rows[iLoop]["CUST_PAY_DTL_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["CUST_PAY_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<OM_CUST_PAYMENT_DTL> userCtx = new DataRepository<OM_CUST_PAYMENT_DTL>())
                        {
                            OM_CUST_PAYMENT_DTL = userCtx.Find(r =>
                                (r.CUST_PAY_DTL_ID == (dtGridData.Rows[iLoop]["CUST_PAY_DTL_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    OM_CUST_PAYMENT_DTL.CUST_PAY_ID = OM_CUST_PAYMENT_HDR.CUST_PAY_ID;
                    OM_CUST_PAYMENT_DTL.ATTRIBUTE1 = (iLoop + 1).ToString();

                    OM_CUST_PAYMENT_DTL.CUST_INV_ID = dtGridData.Rows[iLoop]["OM_INV_ID"].ToString();

                    if (dtGridData.Rows[iLoop]["OM_INV_DATE"].ToString() != string.Empty)
                    {
                        OM_CUST_PAYMENT_DTL.INV_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["OM_INV_DATE"].ToString());
                    }
                    OM_CUST_PAYMENT_DTL.INV_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_INV_AMT"].ToString());
                    OM_CUST_PAYMENT_DTL.CUST_INV_PAID_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["CUST_INV_PAID_AMT"].ToString());
                    OM_CUST_PAYMENT_DTL.CUST_INV_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["CUST_INV_AMT"].ToString());
                    OM_CUST_PAYMENT_DTL.INV_RETENTION_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_INV_RETENTION_AMT"].ToString());

                    OM_CUST_PAYMENT_DTL.ATTRIBUTE2 = dtGridData.Rows[iLoop]["ATTRIBUTE2"].ToString();
                    OM_CUST_PAYMENT_DTL.SALES_ORDER_TYPE = dtGridData.Rows[iLoop]["SALES_ORDER_TYPE"].ToString();
                    OM_CUST_PAYMENT_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    OM_CUST_PAYMENT_DTL.ENABLED_FLAG = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(OM_CUST_PAYMENT_DTL, "D"));
                    }
                    else
                    {
                        if ((dtGridData.Rows[iLoop]["CUST_PAY_DTL_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["CUST_PAY_DTL_ID"].ToString() != string.Empty)
                        {

                            OM_CUST_PAYMENT_DTL.MODIFIED_BY = this.LoggedUserName;
                            OM_CUST_PAYMENT_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(OM_CUST_PAYMENT_DTL, "U"));
                        }
                        else
                        {
                            OM_CUST_PAYMENT_DTL.CUST_PAY_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_010_D", false, true);
                            OM_CUST_PAYMENT_DTL.CREATED_BY = this.LoggedUserName;
                            OM_CUST_PAYMENT_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(OM_CUST_PAYMENT_DTL, "A"));
                        }
                    }
                    OM_CUST_PAYMENT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                }

                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(OM_CUST_PAYMENT_HDR.CUST_PAY_ID, txtPaymentDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<OM_CUST_PAYMENT_HDR, OM_CUST_PAYMENT_DTL>(OM_CUST_PAYMENT_HDR, tmpChildEntity, OM_CUST_PAYMENT_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<OM_CUST_PAYMENT_HDR, OM_CUST_PAYMENT_DTL>(OM_CUST_PAYMENT_HDR, tmpChildEntity, OM_CUST_PAYMENT_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, OM_CUST_PAYMENT_HDR.CUST_PAY_ID);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                DBMethod.SaveEntity<OM_CUST_PAYMENT_HDR>(OM_CUST_PAYMENT_HDR, true);

                if (OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    //FINSP.GetSP_GL_Posting(OM_CUST_PAYMENT_HDR.CUST_PAY_ID, "AR_009");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WsRI_AATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlInvoiceNumber = tmpgvr.FindControl("ddlInvoiceNumber") as DropDownList;
                // Invoice_BLL.getInvoice(ref ddlInvoiceNumber);
                Invoice_BLL.getInvoice4Payemntt(ref ddlInvoiceNumber, Master.Mode, "");
                //Invoice_BLL.fn_getInvoiceNumber4CustomerPayemnt(ref ddlInvoiceNumber, HFCustomerid.Value, "");
                TextBox txtInvoiceDate = tmpgvr.FindControl("txtInvoiceDate") as TextBox;
                DropDownList ddl_SOType = tmpgvr.FindControl("ddlSOType") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddl_SOType, "SOT");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlInvoiceNumber.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_ID"].ToString();

                    ddl_SOType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["sales_order_type"].ToString();
                    SOTypeChange(tmpgvr);
                    txtInvoiceDate.Text = DBMethod.ConvertDateToString(gvData.DataKeys[gvData.EditIndex].Values["OM_INV_DATE"].ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (Master.Mode == FINAppConstants.WF)
                {
                    return;
                }
                System.Collections.SortedList slControls = new System.Collections.SortedList();


                slControls[0] = ddlSupplierNumber;
                slControls[1] = ddlExchangeRateType;

                string strCtrlTypes = "DropDownList~DropDownList";

                string strMessage = "Customer Number ~ Exchange Rate Type";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    if (txtChequeNumber.Text == string.Empty || txtChequeDate.Text == string.Empty)
                    {
                        ErrorCollection.Add("cheque", "Please enter the cheque details(Cheque Number and Value Date");
                    }
                }
                else if (ddlPaymentMode.SelectedItem.Text == "EFT")
                {
                    if (txtBank.Text == string.Empty || txtBankBranch.Text == string.Empty)
                    {
                        ErrorCollection.Add("eft", "Please enter the bank details (Bank Name,Bank Branch");
                    }
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_INV_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_INV_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_INV_RETENTION_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_INV_RETENTION_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CUST_INV_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CUST_INV_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CUST_INV_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CUST_INV_AMT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

                txtTotalPaymentAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "CUST_INV_AMT");

                if (txtTotalPaymentAmt.Text.Length > 0)
                {
                    txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalPaymentAmt.Text);
                }

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddl_SOType = gvr.FindControl("ddlSOType") as DropDownList;
            TextBox txt_Others = gvr.FindControl("txtOthers") as TextBox;
            DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
            TextBox txtInvoiceAmount = gvr.FindControl("txtInvoiceAmount") as TextBox;
            TextBox txtAmountPaid = gvr.FindControl("txtAmountPaid") as TextBox;
            TextBox txtAmountToPay = gvr.FindControl("txtAmountToPay") as TextBox;
            TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;




            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();

            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["CUST_PAY_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            txtLineNo.Text = (rowindex + 1).ToString();

            ErrorCollection.Clear();


            if (ddl_SOType.SelectedValue.ToString().Length == 0)
            {
                ErrorCollection.Add("SELECT SOT", "Please Select The INVOICE Type ");
                return drList;
            }


            slControls[0] = txtLineNo;
            if (ddl_SOType.SelectedValue.ToString().ToUpper() == "INVOICE")
            {
                slControls[1] = ddlInvoiceNumber;
            }
            else
            {
                slControls[1] = txt_Others;
            }
            slControls[2] = txtInvoiceDate;
            slControls[3] = txtAmountToPay;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~";
            if (ddl_SOType.SelectedValue.ToString().ToUpper() == "INVOICE")
            {
                strCtrlTypes += FINAppConstants.DROP_DOWN_LIST;
            }
            else
            {
                strCtrlTypes += FINAppConstants.TEXT_BOX;
            }
            strCtrlTypes += "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Line_Number_P"] + " ~ " + Prop_File_Data["Sales_Order_Number_P"] + " ~ " + Prop_File_Data["Sales_Order_Date_P"] + " ~ " + Prop_File_Data["Receipt_Amount_P"] + "";
            //string strMessage = "Line No ~ Invoice Number ~ Invoice Date ~ Invoice Amount ~ Amount Paid ~ Amount to Pay";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            if (ddl_SOType.SelectedValue.ToString().ToUpper() == "INVOICE")
            {

                string strCondition = "OM_INV_ID='" + ddlInvoiceNumber.SelectedValue + "'";
                strMessage = FINMessageConstatns.RecordAlreadyExists;
                ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }

                //if (txtRetentionAmount.Text != string.Empty)
                //{
                //    if (CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) < CommonUtils.ConvertStringToDecimal(txtAmountToPay.Text))
                //    {
                //        ErrorCollection.Add("ren_amt_toPay", Prop_File_Data["ren_amt_toPay_P"]);

                //    }
                //}


                if (txtAmountToPay.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) < CommonUtils.ConvertStringToDecimal(txtAmountToPay.Text))
                    {
                        ErrorCollection.Add("ren_amt_toPay", "Receipt Amount should  not greater than the Sales Amount");
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }

                if (txtAmountPaid.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) < CommonUtils.ConvertStringToDecimal(txtAmountPaid.Text))
                    {
                        ErrorCollection.Add("ren_amt_toPay1", "Amount Received  should not greater than the Sales Amount");
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }

                ErrorCollection.Clear();
                if (txtAmountPaid.Text != string.Empty)
                {
                    hfSumreceipt_receivedamt.Value = (decimal.Parse(txtAmountPaid.Text) + decimal.Parse(txtAmountToPay.Text)).ToString();
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) < CommonUtils.ConvertStringToDecimal(hfSumreceipt_receivedamt.Value))
                    {
                        ErrorCollection.Add("ren_amt_toPay1", "Sum of Amount Received and Receipt Amount should not greater than the Sales Amount");
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
            }
            ErrorCollection.Clear();
            //    txtTotalPaymentAmt.Text = FIN.BLL.AP.PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtAmountToPay.Text));

            drList["line_no"] = txtLineNo.Text;
            drList["SALES_ORDER_TYPE"] = ddl_SOType.SelectedValue.ToString();
            drList["SOT_DESC"] = ddl_SOType.SelectedValue.ToString();
            if (ddl_SOType.SelectedValue.ToString().ToUpper() == "INVOICE")
            {
                drList["OM_INV_ID"] = ddlInvoiceNumber.SelectedValue.ToString();
                drList["OM_INV_NUM"] = ddlInvoiceNumber.SelectedItem.Text.ToString();
                drList["OM_INV_AMT"] = txtInvoiceAmount.Text;
            }
            else
            {
                drList["OM_INV_NUM"] = txt_Others.Text;
                drList["ATTRIBUTE2"] = txt_Others.Text;
                drList["OM_INV_AMT"] = txtAmountToPay.Text;
            }
            drList["OM_INV_DATE"] = DBMethod.ConvertStringToDate(txtInvoiceDate.Text.ToString());


            drList["CUST_INV_PAID_AMT"] = txtAmountPaid.Text;
            drList["CUST_INV_AMT"] = txtAmountToPay.Text;
            drList["OM_INV_RETENTION_AMT"] = txtRetentionAmount.Text == string.Empty ? "0" : txtRetentionAmount.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;

                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnUpdateStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                stopPayBool = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                Session["StopPayment"] = "stop";

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnInvoiceOpen_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                string url = Server.MapPath("~/AP/InvoicePopup.aspx");
                Session["InvoiceProgramID"] = "30";
                Session["InvoiceMode"] = "U";
                Session["InvoiceID"] = Session["INVNUM"].ToString();

                ModalPopupExtender2.Show();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<OM_CUST_PAYMENT_HDR>(OM_CUST_PAYMENT_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
                TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;
                DataTable dtqty = new DataTable();
                dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getQuatity(ddlLotNo.SelectedValue)).Tables[0];
                txtquantity.Text = dtqty.Rows[0]["LOT_QTY"].ToString();


                //if (txtWHQuantity.Text.Trim() == string.Empty)
                //{
                //    txtWHQuantity.Text = "0";
                //}
                //if (CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtquantity.Text) >= 0)
                //{
                //    txtWHQuantity.Text = (CommonUtils.ConvertStringToDecimal(txtquantity.Text) + CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text)).ToString();
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlWarehouseNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //  txtWarehouseName.Text = ddlWarehouseNumber.SelectedValue;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillLineNumberDetails()
        {
            DataTable dtgrndata = new DataTable();

            //dtgrndata = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getgrndata(ddlGRNNumber.SelectedValue)).Tables[0];
            //txtGRNDate.Text = DateTime.Parse(dtgrndata.Rows[0]["GRN_DATE"].ToString()).ToString("dd/MM/yyyy");
            //PurchaseItemReceipt_BLL.fn_GetPOItemLine(ref ddlLineNumber, ddlGRNNumber.SelectedValue.ToString());


        }
        protected void ddlGRNNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillLineNumberDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPaymentDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillExchangeRate()
        {
            DataTable dtDat = new DataTable();
            dtDat = DBMethod.ExecuteQuery(Currency_DAL.getCurrencyBasedOrg(VMVServices.Web.Utils.OrganizationID, txtPaymentDate.Text)).Tables[0];

            if (dtDat != null)
            {
                if (dtDat.Rows.Count > 0)
                {
                    if (ddlPaymentCurrency.SelectedValue == dtDat.Rows[0][FINColumnConstants.CURRENCY_ID].ToString())
                    {
                        txtExchangeRate.Enabled = false;
                        txtExchangeRate.Text = "1";
                    }
                    else
                    {

                        txtExchangeRate.Enabled = true;
                        DataTable dtDat1 = new DataTable();
                        dtDat1 = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtPaymentDate.Text, ddlPaymentCurrency.SelectedValue.ToString())).Tables[0];

                        if (dtDat1 != null)
                        {
                            if (dtDat1.Rows.Count > 0)
                            {
                                if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "standard")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_STD_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "selling")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_SEL_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "buying")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_BUY_RATE"].ToString();
                                }
                            }
                            else
                            {
                                txtExchangeRate.Text = "1";
                            }
                        }
                        else
                        {
                            txtExchangeRate.Text = "1";
                        }
                    }
                }
            }
        }
        protected void ddlExchangeRateType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlPaymentCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtData = new DataTable();

                Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = true;


                GridViewRow gvr = gvData.FooterRow;
                DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
                TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;

                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);

                //    AccountCodes_BLL.getAccCodeBasedOrg(ref  ddlAccountCode, VMVServices.Web.Utils.OrganizationID);

                if (ddlPaymentCurrency.SelectedValue != string.Empty)
                {

                    string str_currecny = "";
                    if (ddlPaymentCurrency.SelectedValue.ToString().Length > 0)
                    {
                        if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                        {
                            str_currecny = "";
                        }
                        else
                        {
                            str_currecny = ddlPaymentCurrency.SelectedValue;
                        }
                    }

                    Invoice_BLL.fn_getInvoiceNumber4CustomerPayemnt(ref ddlInvoiceNumber, HFCustomerid.Value, str_currecny);
                    SalesPayment_BLL.getAccountCodeDetails(ref ddlAccountCode, ddlPaymentCurrency.SelectedValue.ToString());

                    dtData = DBMethod.ExecuteQuery(SalesPayment_DAL.getInvoiceDetails(ddlPaymentCurrency.SelectedValue.ToString())).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            //ddlGlobalSegment.SelectedValue = dtData.Rows[0]["global_segment_id"].ToString();
                            ddlAccountCode.SelectedValue = dtData.Rows[0]["OM_INV_ACCT_CODE_ID"].ToString();
                            txtRetentionAmount.Text = dtData.Rows[0]["OM_INV_RETENTION_AMT"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void OpenWindow(object sender, EventArgs e)
        {
            string url = Server.MapPath("~/AP/InvoicePopup.aspx");
            Session["InvoiceProgramID"] = "30";
            Session["InvoiceMode"] = "U";
            Session["InvoiceID"] = Session["INVNUM"].ToString();

            //string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + url + "','','" + DbConsts.ReportProperties + "');", true);

        }

        #endregion

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }







        protected void imgBtnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("CUST_PAY_ID", Master.StrRecordId.ToString());
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.SalesOrder_BLL.GetReceiptReportData();


                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                string str_sp_output = FINSP.SP_POSTING_ACCTCODE_VALIDATION("AR_010", ddlSupplierNumber.SelectedValue, Master.StrRecordId);

                if (str_sp_output != "SUCCESS")
                {
                    ErrorCollection.Add("PostingAcctErr", str_sp_output);
                    return;
                }
                if (EntityData != null)
                {
                    OM_CUST_PAYMENT_HDR = (OM_CUST_PAYMENT_HDR)EntityData;
                }
                if (OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(OM_CUST_PAYMENT_HDR.CUST_PAY_ID, "AR_010");
                }

                OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);
                OM_CUST_PAYMENT_HDR.POSTED_FLAG = FINAppConstants.Y;
                OM_CUST_PAYMENT_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<OM_CUST_PAYMENT_HDR>(OM_CUST_PAYMENT_HDR, true);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                imgBtnPost.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;
                imgBtnCancelReceipt.Visible = false;
                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", OM_CUST_PAYMENT_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);

            if (OM_CUST_PAYMENT_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", OM_CUST_PAYMENT_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

        protected void imgStopPayment_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                if (EntityData != null)
                {
                    // INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                    OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);
                }
                if (OM_CUST_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    if (OM_CUST_PAYMENT_HDR.POSTED_FLAG != null)
                    {
                        if (OM_CUST_PAYMENT_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(OM_CUST_PAYMENT_HDR.JE_HDR_ID, "AR_010_R", this.LoggedUserName);
                        }
                    }
                }
                btnStopPayment.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                lblPosted.Visible = true;
                imgBtnPost.Visible = false;
                lblCancelled.Visible = true;

                imgBtnStopPayJV.Visible = true;
                btnStopPayment.Visible = false;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", OM_CUST_PAYMENT_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnStopPayJV_Click(object sender, ImageClickEventArgs e)
        {
            OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);

            if (OM_CUST_PAYMENT_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", OM_CUST_PAYMENT_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

        protected void imgBtnCancelReceipt_Click(object sender, ImageClickEventArgs e)
        {
            OM_CUST_PAYMENT_HDR = SalesPayment_BLL.getClassEntity(Master.StrRecordId);
            OM_CUST_PAYMENT_HDR.POSTED_FLAG = FINAppConstants.CANCELED;
            OM_CUST_PAYMENT_HDR.REV_FLAG = FINAppConstants.Y;
            OM_CUST_PAYMENT_HDR.REV_DATE = DateTime.Today;
            DBMethod.SaveEntity<OM_CUST_PAYMENT_HDR>(OM_CUST_PAYMENT_HDR, true);
            ErrorCollection.Add("Invoice Canceled", "Invoice Cancelled Successfully");
            imgBtnPost.Visible = false;
            imgBtnCancelReceipt.Visible = false;
            lblCancelled.Visible = true;
            btnSave.Visible = false;
            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        }







    }
}