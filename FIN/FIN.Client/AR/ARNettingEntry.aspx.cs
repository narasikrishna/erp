﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.AR
{
    public partial class ARNettingEntry : PageBase
    {
        Boolean savedBool;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (!IsPostBack)
                {

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    AR_NETTING_HDR aR_NETTING_HDR = new AR_NETTING_HDR();
                    using (IRepository<AR_NETTING_HDR> userCtx = new DataRepository<AR_NETTING_HDR>())
                    {
                        aR_NETTING_HDR = userCtx.Find(r =>
                            (r.NETTING_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    if (aR_NETTING_HDR != null)
                    {
                        EntityData = aR_NETTING_HDR;
                        ddlSupplierName.SelectedValue = aR_NETTING_HDR.VENDOR_ID;
                        GetAPARData();
                        ddlSupplierName.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //  pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        private void FillComboBox()
        {
            Supplier_BLL.GetSupplierName(ref ddlSupplierName);
        }
        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSupplierSite();
            GetAPARData();
        }
        private void FillSupplierSite()
        {
            FIN.BLL.AP.SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref  ddlSupplierSite, ddlSupplierName.SelectedValue);
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            GetAPARData();
        }

        private void GetAPARData()
        {

            Master.Mode = FINAppConstants.Add;
            Master.StrRecordId = "0";
            EntityData = null;
            DataTable dt_AP_data = DBMethod.ExecuteQuery(FIN.DAL.AR.ARNetting_DAL.get_NettingAP(ddlSupplierName.SelectedValue.ToString())).Tables[0];
            DataTable dt_AR_data = DBMethod.ExecuteQuery(FIN.DAL.AR.ARNetting_DAL.get_NettingAR(ddlSupplierName.SelectedValue.ToString())).Tables[0];
            if (dt_AP_data.Rows.Count == 0)
            {
                dt_AP_data = DBMethod.ExecuteQuery(FIN.DAL.AR.ARNetting_DAL.get_BAL_APInvoiceDet(ddlSupplierName.SelectedValue.ToString())).Tables[0];
                dt_AR_data = DBMethod.ExecuteQuery(FIN.DAL.AR.ARNetting_DAL.get_BAL_ARInvoiceDet(ddlSupplierName.SelectedValue.ToString())).Tables[0];
            }
            else
            {
                AR_NETTING_HDR aR_NETTING_HDR = new AR_NETTING_HDR();
                using (IRepository<AR_NETTING_HDR> userCtx = new DataRepository<AR_NETTING_HDR>())
                {
                    aR_NETTING_HDR = userCtx.Find(r =>
                        (r.NETTING_ID == dt_AP_data.Rows[0]["NETTING_ID"].ToString())
                        ).SingleOrDefault();
                }
                if (aR_NETTING_HDR != null)
                {
                    EntityData = aR_NETTING_HDR;
                    txtAdjAmt.Text = aR_NETTING_HDR.ATTRIBUTE1;
                    txtRemaks.Text = aR_NETTING_HDR.INV_REMARKS;
                    Master.Mode = FINAppConstants.Update;
                    Master.StrRecordId = aR_NETTING_HDR.NETTING_ID;

                }
            }
            BindAPGrid(dt_AP_data);
            BindARGrid(dt_AR_data);
        }

        private void BindAPGrid(DataTable dt_Data)
        {
            txtAPTotARAmt.Text = "";
            if (dt_Data.Rows.Count > 0)
            {
                txtAPTotARAmt.Text = CommonUtils.CalculateTotalAmount(dt_Data, "AP_BAL_INV_AMT");
                if (txtAPTotARAmt.Text.Length > 0)
                {
                    txtAPTotARAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAPTotARAmt.Text);
                }
                dt_Data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AP_BAL_INV_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AP_BAL_INV_AMT"))));
                dt_Data.AcceptChanges();
            }

            gv_APData.DataSource = dt_Data;
            gv_APData.DataBind();

        }
        private void BindARGrid(DataTable dt_Data)
        {
            txtARTotARAmt.Text = "";
            if (dt_Data.Rows.Count > 0)
            {
                txtARTotARAmt.Text = CommonUtils.CalculateTotalAmount(dt_Data, "AR_BAL_INV_AMT");
                if (txtARTotARAmt.Text.Length > 0)
                {
                    txtARTotARAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtARTotARAmt.Text);
                }
                dt_Data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AR_BAL_INV_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AR_BAL_INV_AMT"))));
                dt_Data.AcceptChanges();
            }
            gv_ARData.DataSource = dt_Data;
            gv_ARData.DataBind();

        }
        private void AssignToBE()
        {
            try
            {
                AR_NETTING_HDR aR_NETTING_HDR = new AR_NETTING_HDR();
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    aR_NETTING_HDR = (AR_NETTING_HDR)EntityData;
                }
                aR_NETTING_HDR.VENDOR_ID = ddlSupplierName.SelectedValue;
                aR_NETTING_HDR.AP_INV_AMT = FIN.BLL.CommonUtils.ConvertStringToDecimal(txtAPTotARAmt.Text);
                aR_NETTING_HDR.AR_INV_AMT = FIN.BLL.CommonUtils.ConvertStringToDecimal(txtARTotARAmt.Text);
                aR_NETTING_HDR.INV_REMARKS = txtRemaks.Text;
                aR_NETTING_HDR.ATTRIBUTE1 = txtAdjAmt.Text;
                aR_NETTING_HDR.ENABLED_FLAG = FINAppConstants.Y;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    aR_NETTING_HDR.MODIFIED_BY = this.LoggedUserName;
                    aR_NETTING_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    aR_NETTING_HDR.NETTING_ID = FINSP.GetSPFOR_SEQCode("AR_011_M", false, true);
                    aR_NETTING_HDR.NETTING_DATE = DateTime.Today;
                    aR_NETTING_HDR.POSTED_FLAG = FINAppConstants.N;

                    aR_NETTING_HDR.CREATED_BY = this.LoggedUserName;
                    aR_NETTING_HDR.CREATED_DATE = DateTime.Today;
                }

                aR_NETTING_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aR_NETTING_HDR.NETTING_ID);
                aR_NETTING_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                var tmpChildEntity = new List<Tuple<object, string>>();
                AR_NETTING_DTL aR_NETTING_DTL = new AR_NETTING_DTL();

                for (int iLoop = 0; iLoop < gv_APData.Rows.Count; iLoop++)
                {
                    aR_NETTING_DTL = new AR_NETTING_DTL();
                    if (gv_APData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != "0" && gv_APData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<AR_NETTING_DTL> userCtx = new DataRepository<AR_NETTING_DTL>())
                        {
                            aR_NETTING_DTL = userCtx.Find(r =>
                                (r.NETTING_DTL_ID == gv_APData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    aR_NETTING_DTL.NETTING_ID = aR_NETTING_HDR.NETTING_ID;

                    aR_NETTING_DTL.INV_TYPE = "AP";
                    aR_NETTING_DTL.INV_ID = gv_APData.DataKeys[iLoop].Values["INV_ID"].ToString();
                    aR_NETTING_DTL.INV_AMT = FIN.BLL.CommonUtils.ConvertStringToDecimal(gv_APData.DataKeys[iLoop].Values["AP_BAL_INV_AMT"].ToString());
                    aR_NETTING_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    if (gv_APData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != "0" && gv_APData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != string.Empty)
                    {
                        aR_NETTING_DTL.MODIFIED_BY = this.LoggedUserName;
                        aR_NETTING_DTL.MODIFIED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(aR_NETTING_DTL, "U"));
                    }

                    else
                    {
                        aR_NETTING_DTL.NETTING_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_011_D", false, true);
                        aR_NETTING_DTL.CREATED_BY = this.LoggedUserName;
                        aR_NETTING_DTL.CREATED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(aR_NETTING_DTL, "A"));
                    }
                    aR_NETTING_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                }

                for (int iLoop = 0; iLoop < gv_ARData.Rows.Count; iLoop++)
                {
                    aR_NETTING_DTL = new AR_NETTING_DTL();
                    if (gv_ARData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != "0" && gv_ARData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<AR_NETTING_DTL> userCtx = new DataRepository<AR_NETTING_DTL>())
                        {
                            aR_NETTING_DTL = userCtx.Find(r =>
                                (r.NETTING_DTL_ID == gv_ARData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    aR_NETTING_DTL.NETTING_ID = aR_NETTING_HDR.NETTING_ID;

                    aR_NETTING_DTL.INV_TYPE = "AR";
                    aR_NETTING_DTL.INV_ID = gv_ARData.DataKeys[iLoop].Values["OM_INV_ID"].ToString();
                    aR_NETTING_DTL.INV_AMT = FIN.BLL.CommonUtils.ConvertStringToDecimal(gv_ARData.DataKeys[iLoop].Values["AR_BAL_INV_AMT"].ToString());
                    aR_NETTING_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    if (gv_ARData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != "0" && gv_ARData.DataKeys[iLoop].Values["NETTING_DTL_ID"].ToString() != string.Empty)
                    {
                        aR_NETTING_DTL.MODIFIED_BY = this.LoggedUserName;
                        aR_NETTING_DTL.MODIFIED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(aR_NETTING_DTL, "U"));
                    }

                    else
                    {
                        aR_NETTING_DTL.NETTING_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_011_D", false, true);
                        aR_NETTING_DTL.CREATED_BY = this.LoggedUserName;
                        aR_NETTING_DTL.CREATED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(aR_NETTING_DTL, "A"));
                    }
                    aR_NETTING_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.CommonUtils.SavePCEntity<AR_NETTING_HDR, AR_NETTING_DTL>(aR_NETTING_HDR, tmpChildEntity, aR_NETTING_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.CommonUtils.SavePCEntity<AR_NETTING_HDR, AR_NETTING_DTL>(aR_NETTING_HDR, tmpChildEntity, aR_NETTING_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AssignToBE();
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
    }
}