﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ARNettingEntry.aspx.cs" Inherits="FIN.Client.AR.ARNettingEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="lblSupplierName">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 130px" id="lblSupplierSite">
                Supplier Site
            </div>
            <div class="divtxtBox LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="8" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 130px" id="Div1">
            </div>
            <div class="divtxtBox LNOrient" style="width: 500px" align="right">
                <asp:Button ID="btnGet" runat="server" Text="Get" OnClick="btnGet_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer " style="width:800px">
            <table width="100%">
                <tr>
                    <td class="GridHeader lblBox ">
                        Account Payable
                    </td>
                    <td class="GridHeader lblBox">
                        Account Receivable
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_APData" runat="server" AutoGenerateColumns="False"
                            CssClass="DisplayFont Grid" DataKeyNames="NETTING_DTL_ID,INV_ID,AP_BAL_INV_AMT">
                            <Columns>
                                <asp:BoundField DataField="INV_NUM" HeaderText="Invoice Number" ItemStyle-Width="200px" />
                                <asp:BoundField DataField="AP_BAL_INV_AMT" HeaderText="Balance Amount">
                                    <ItemStyle HorizontalAlign="Right" Width="200px" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </td>
                    <td>
                        <asp:GridView ID="gv_ARData" runat="server" AutoGenerateColumns="False"
                            CssClass="DisplayFont Grid" DataKeyNames="NETTING_DTL_ID,OM_INV_ID,AR_BAL_INV_AMT">
                            <Columns>
                                <asp:BoundField DataField="OM_INV_NUM" HeaderText="Invoice Number" ItemStyle-Width="200px" />
                                <asp:BoundField DataField="AR_BAL_INV_AMT" HeaderText="Balance Amount">
                                    <ItemStyle HorizontalAlign="Right" Width="200px"  />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="lblBox LNOrient" style="width: 180px;display:none;">
                            
                        </div>
                        <div class="divtxtBox LNOrient" style="width: 350px;">
                           Total <asp:TextBox ID="txtAPTotARAmt" runat="server" Width="190px" Enabled="false" TabIndex="15"
                                CssClass="txtBox_N"></asp:TextBox>
                        </div>
                    </td>
                    <td>
                        <div class="lblBox LNOrient" style="width: 180px;display:none;">
                            
                        </div>
                        <div class="divtxtBox LNOrient" style="width: 350px;">
                            Total<asp:TextBox ID="txtARTotARAmt" runat="server" Width="190px" TabIndex="15" Enabled="false"
                                CssClass="txtBox_N"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <%--<tr>
                    <td align="right">
                        <div class="divtxtBox" style="float: right; width: 200px;">
                            <asp:TextBox ID="txtAPTotARAmt" runat="server" Enabled="false" TabIndex="15" CssClass="txtBox_N"></asp:TextBox>
                        </div>
                        <div class="lblBox" style="float: right; width: 100px;" align="right">
                            Total
                        </div>
                    </td>
                    <td align="right">
                        <div class="divtxtBox" style="float: right; width: 200px;">
                            <asp:TextBox ID="txtARTotARAmt" runat="server" TabIndex="15" Enabled="false" CssClass="txtBox_N"></asp:TextBox>
                        </div>
                        <div class="lblBox" style="float: right; width: 100px;" align="right">
                            Total
                        </div>
                    </td>
                </tr>--%>
            </table>
        </div>
        <div class="divRowContainer" style="width: 1000px">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="Div2">
                Adjust Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtAdjAmt" runat="server" TabIndex="15" CssClass="txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="Div3">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="width: 400px">
                <asp:TextBox ID="txtRemaks" runat="server" TabIndex="15" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
