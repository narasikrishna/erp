﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="~/AR/DashBoard_AR.aspx.cs" Inherits="FIN.Client.AR.DashBoard_AR" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowInvoiceReceived() {
            $("#divInvoiceReceived").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="98%" border="0px">
            <tr>
                <td style="width: 40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divempGraph" style="float: left; width: 100%;">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left; padding-top: 5px">
                                            Top 5 Customer
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div style="float: left; width: 100%; overflow: auto">
                                        <asp:Chart ID="chrtTop5Customer" runat="server" Height="250px" Width="500px">
                                            <Series>
                                                <asp:Series Name="S_Customer_Count" XValueMember="vendor_name" YValueMembers="payment_amt"
                                                    IsValueShownAsLabel="true" LabelAngle="60" Legend="Legend1" LegendText="Total Amount ( #TOTAL{N0} )">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Invoice/Received
                                    </div>
                                    <div style="float: right; padding: 10px">
                                        <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowInvoiceReceived()" />
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divInvoiceReceive" style="width: 98%">
                                    <asp:Chart ID="chrtInvoicedReceive" runat="server" Width="500px" Height="230px">
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="invoice_amt" IsValueShownAsLabel="True"
                                                XValueMember="vendor_name" YValueMembers="invoice_amt" Legend="Legend1" LegendText="Invoice Amount ( #TOTAL{N0} )">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="payment_amt"
                                                XValueMember="vendor_name" YValueMembers="cust_inv_amt" Legend="Legend1" LegendText="Received Amount ( #TOTAL{N0} )\n">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                                <div class="divClear_10" style="width: 70%">
                                </div>
                                <div id="divInvoiceReceived" style="width: 40%; background-color: ThreeDFace; position: absolute;
                                    z-index: 5000; top: 400px; left: 10px; display: none">
                                    <div id="divEmpDept" runat="server" visible="true">
                                        <asp:GridView ID="gvInvoiceReceived" runat="server" CssClass="Grid" Width="100%"
                                            DataKeyNames="vendor_id" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnk_Dept_Name" runat="server" Text='<%# Eval("vendor_name") %>'
                                                            OnClick="lnk_InvoiceReceived_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="invoice_amt" HeaderText="Invoice Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cust_inv_amt" HeaderText="Received Amount">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divEmpAge" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Aging Analysis
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div id="divempAgeGraph">
                                        <asp:Chart ID="chrtEmpAge" runat="server" Height="200px" Width="400px">
                                            <Series>
                                                <asp:Series Name="s_EmpAge" ChartType="StackedColumn" XValueMember="AgeDiff" YValueMembers="AMOUNT_PAID"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                                <asp:Series Name="s_EmpdAge" ChartType="StackedColumn" XValueMember="AgeDiff" YValueMembers="AMOUNT_PAID"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <div id="divCustSOA" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        Customer - Sales Order Awaited
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divEmpExpirGraph">
                                        <asp:Chart ID="chrtSOAwait" runat="server" Width="300px" Height="200px">
                                            <Series>
                                                <asp:Series Name="s_SOAwaitr" ChartType="Funnel" XValueMember="vendor_name" YValueMembers="counts"
                                                    IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1" Docking="Bottom">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
