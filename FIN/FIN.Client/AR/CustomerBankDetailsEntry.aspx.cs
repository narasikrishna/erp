﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AR
{
    public partial class CustomerBankDetailsEntry : PageBase
    {

        SUPPLIER_CUSTOMER_BANK SUPPLIER_CUSTOMER_BANK = new SUPPLIER_CUSTOMER_BANK();
        SupplierBankDetails_BLL SupplierBankDetails_BLL = new SupplierBankDetails_BLL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            //  SupplierBankDetails_BLL.fn_getSupplierName(ref ddlCustomerNumber);
            Supplier_BLL.GetCustomerNumber(ref ddlCustomerNumber);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtCustomerName.Enabled = false;
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AR.CustomerBankDetails_DAL.GetCustomerbankdtls(Master.RecordID)).Tables[0];
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    using (IRepository<SUPPLIER_CUSTOMER_BANK> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BANK>())
                    {
                        SUPPLIER_CUSTOMER_BANK = userCtx.Find(r =>
                           (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = SUPPLIER_CUSTOMER_BANK;
                    if (SUPPLIER_CUSTOMER_BANK.VENDOR_ID != null)
                    {
                        ddlCustomerNumber.SelectedValue = SUPPLIER_CUSTOMER_BANK.VENDOR_ID;
                        fillsupNm();
                    }
                    FIN.BLL.AR.Customer_Branch_BLL.GetCustomerBranch(ref ddlCustomersite, ddlCustomerNumber.SelectedValue.ToString());
                    ddlCustomersite.SelectedValue = SUPPLIER_CUSTOMER_BANK.VENDOR_LOC_ID;
                    FILLBranchNM();

                    SUPPLIER_CUSTOMERS SUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                    using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                    {
                        SUPPLIER_CUSTOMERS = userCtx.Find(r =>
                            (r.VENDOR_ID == SUPPLIER_CUSTOMER_BANK.VENDOR_ID)
                            ).SingleOrDefault();
                    }

                    if (SUPPLIER_CUSTOMERS != null)
                    {
                        if (SUPPLIER_CUSTOMERS.VENDOR_CODE != null)
                        {
                            ddlCustomerNumber.SelectedValue = SUPPLIER_CUSTOMERS.VENDOR_CODE;

                            txtCustomerName.Text = SUPPLIER_CUSTOMERS.VENDOR_NAME;
                        }
                    }
                }
               

               
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Supplier Bank ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    SUPPLIER_CUSTOMER_BANK = new SUPPLIER_CUSTOMER_BANK();
                    if (dtGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                    {
                        using (IRepository<SUPPLIER_CUSTOMER_BANK> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BANK>())
                        {
                            SUPPLIER_CUSTOMER_BANK = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    SUPPLIER_CUSTOMER_BANK.BANK_ID = dtGridData.Rows[iLoop]["BANK_ID"].ToString();
                    SUPPLIER_CUSTOMER_BANK.BANK_BRANCH_ID = dtGridData.Rows[iLoop]["BANK_BRANCH_ID"].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_BANK_ACCOUNT_CODE = dtGridData.Rows[iLoop]["VENDOR_BANK_ACCOUNT_CODE"].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_BANK_ACCTOUNT_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.VALUE_KEY_ID].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_ID = ddlCustomerNumber.SelectedValue;
                    SUPPLIER_CUSTOMER_BANK.VENDOR_LOC_ID = ddlCustomersite.SelectedValue;
                    SUPPLIER_CUSTOMER_BANK.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    //  SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = "1";



                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        SUPPLIER_CUSTOMER_BANK.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        SUPPLIER_CUSTOMER_BANK.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(SUPPLIER_CUSTOMER_BANK, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                        {

                            SUPPLIER_CUSTOMER_BANK.MODIFIED_BY = this.LoggedUserName;
                            SUPPLIER_CUSTOMER_BANK.MODIFIED_DATE = DateTime.Today;
                            SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SUPPLIER_CUSTOMER_BANK.PK_ID.ToString());
                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK, true);
                            saveBool = true;

                        }
                        else
                        {
                            SUPPLIER_CUSTOMER_BANK.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.CUSTOMER_BANK_SEQ);
                            SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SUPPLIER_CUSTOMER_BANK.PK_ID.ToString());
                            SUPPLIER_CUSTOMER_BANK.CREATED_BY = this.LoggedUserName;
                            SUPPLIER_CUSTOMER_BANK.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK);
                            saveBool = true;
                        }




                    }

                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAccountType = tmpgvr.FindControl("ddlAccountType") as DropDownList;
                DropDownList ddlBranchName = tmpgvr.FindControl("ddlBranchName") as DropDownList;
                DropDownList ddlBankName = tmpgvr.FindControl("ddlBankName") as DropDownList;

                //SupplierBankDetails_BLL.fn_getfrlookup(ref ddlAccountType, "AT");
                Lookup_BLL.GetLookUpValues(ref ddlAccountType, "AT");

                SupplierBankDetails_BLL.fn_getBankName(ref ddlBankName);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccountType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VALUE_KEY_ID].ToString();
                    ddlBankName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["BANK_ID"].ToString();
                    fillbankbranch(tmpgvr);
                    ddlBranchName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["BANK_BRANCH_ID"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


       
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            DropDownList ddlAccountType = gvr.FindControl("ddlAccountType") as DropDownList;
            TextBox txtAccountNumber = gvr.FindControl("txtAccountNumber") as TextBox;
            TextBox txtifsc = gvr.FindControl("txtifsc") as TextBox;
            TextBox txtSwift = gvr.FindControl("txtSwift") as TextBox;


            CheckBox chkAct = gvr.FindControl("chkAct") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlBankName;
            slControls[1] = ddlBranchName;
            slControls[2] = ddlAccountType;
            slControls[3] = txtAccountNumber;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~TextBox";
            string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Account_Type_P"] + " ~ " + Prop_File_Data["Account_Number_P"] + "";
            //string strMessage = "Bank Name ~ Branch Name ~ Account Type ~ Account No";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "VENDOR_BANK_ACCOUNT_CODE='" + txtAccountNumber.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (ddlBankName.SelectedItem != null)
            {
                drList["BANK_ID"] = ddlBankName.SelectedItem.Value;
                drList["BANK_NAME"] = ddlBankName.SelectedItem.Text;
            }

            if (ddlBranchName.SelectedItem != null)
            {
                drList["BANK_BRANCH_ID"] = ddlBranchName.SelectedItem.Value;
                drList["BANK_BRANCH_NAME"] = ddlBranchName.SelectedItem.Text;
            }
            if (ddlAccountType.SelectedItem != null)
            {
                drList["VALUE_KEY_ID"] = ddlAccountType.SelectedItem.Value;
                drList["VALUE_NAME"] = ddlAccountType.SelectedItem.Text;
            }


            drList["VENDOR_BANK_ACCOUNT_CODE"] = txtAccountNumber.Text;

            drList["BANK_IFSC_CODE"] = txtifsc.Text;
            drList["BANK_SWIFT_CODE"] = txtSwift.Text;

            if (chkAct.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountCodesEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    SUPPLIER_CUSTOMER_BANK.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK);
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CBD_BTN_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        private void fillsupNm()
        {
            DataTable dtSupplierNmae = new DataTable();
            dtSupplierNmae = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBankDetails_DAL.getSuppNM(ddlCustomerNumber.SelectedValue)).Tables[0];
            if (dtSupplierNmae.Rows.Count > 0)
            {
                txtCustomerName.Text = dtSupplierNmae.Rows[0]["vendor_name"].ToString();
            }

        }

        protected void txtSupplierName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCustomerNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillsupNm();
            FIN.BLL.AR.Customer_Branch_BLL.GetCustomerBranch(ref ddlCustomersite, ddlCustomerNumber.SelectedValue.ToString());
            txtName.Text = "";
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillbankbranch(gvr);
        }

        private void fillbankbranch(GridViewRow gvr)
        {

            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            TextBox txtifsc = gvr.FindControl("txtifsc") as TextBox;
            TextBox txtSwift = gvr.FindControl("txtSwift") as TextBox;

            SupplierBankDetails_BLL.fn_getBankBranchNameasper_bank(ref ddlBranchName, ddlBankName.SelectedValue);
            if (ddlBankName.SelectedIndex.ToString() == "0")
            {
                txtifsc.Text = "";
                txtSwift.Text = "";
            }


        }

        protected void ddlBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            TextBox txtifsc = gvr.FindControl("txtifsc") as TextBox;
            TextBox txtSwift = gvr.FindControl("txtSwift") as TextBox;


            DataTable dtIFSCCode = new DataTable();
            dtIFSCCode = DBMethod.ExecuteQuery(FIN.DAL.AR.CustomerBankDetails_DAL.GetIFSC_SWIFTCODE(ddlBranchName.SelectedValue).ToString()).Tables[0];
            if (dtIFSCCode.Rows.Count > 0)
            {
                txtifsc.Text = dtIFSCCode.Rows[0]["BANK_IFSC_CODE"].ToString();
                txtSwift.Text = dtIFSCCode.Rows[0]["BANK_SWIFT_CODE"].ToString();
            }
            else
            {
                txtifsc.Text = "";
                txtSwift.Text = "";
            }

        }

        protected void ddlCustomersite_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLBranchNM();
        }

        private void FILLBranchNM()
        {
            DataTable dtBranchNmae = new DataTable();
            dtBranchNmae = DBMethod.ExecuteQuery(FIN.DAL.AR.CustomerBankDetails_DAL.GetBranchName(ddlCustomersite.SelectedValue)).Tables[0];
            txtName.Text = dtBranchNmae.Rows[0]["VENDOR_BRANCH_CODE"].ToString();
        }


    }
}