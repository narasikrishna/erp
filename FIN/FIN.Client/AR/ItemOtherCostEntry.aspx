﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ItemOtherCostEntry.aspx.cs" Inherits="FIN.Client.AR.ItemOtherCostEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCNumber">
                Delivery Voucher Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlDCNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="1" AutoPostBack="True" 
                    onselectedindexchanged="ddlDCNumber_SelectedIndexChanged1">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCDate">
                Delivery Voucher Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtDCDate" CssClass="validate[required] txtBox" runat="server" 
                    Enabled="true" TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDCDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDCDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblLineNumber">
                Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlLineNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="3" AutoPostBack="True" 
                    onselectedindexchanged="ddlLineNumber_SelectedIndexChanged1">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderNumber">
                Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtOrderNumber" CssClass="validate[required] txtBox" runat="server" MaxLength="50"
                    Enabled="true" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderLineNumber">
                Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtOrderLineNumber" CssClass="validate[required] txtBox_N" runat="server" MaxLength="13"
                    Enabled="true" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblItemDescription">
                Item Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtItemDescription" CssClass="validate[required] txtBox" runat="server" MaxLength="50"
                    Enabled="true" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    <div class="divRowContainer LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" 
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="OM_OTHER_COST_DTL_ID,LOOKUP_ID,DELETED">
                <Columns>
                    <asp:TemplateField HeaderText="Cost Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCostType" TabIndex="7" runat="server" CssClass="RequiredField ddlStype" Width="270px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCostType" TabIndex="7" runat="server" CssClass="RequiredField ddlStype"  Width="270px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCostType" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'
                                 Width="270px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cost">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCost" TabIndex="8"  
                                runat="server" Text='<%# Eval("OM_OTHER_COST_AMT") %>' CssClass="EntryFont RequiredField txtBox_N" MaxLength="13"
                                Width="270px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtCost" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCost" TabIndex="8" MaxLength="13"
                                runat="server" CssClass="EntryFont RequiredField txtBox_N"   Width="270px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtCost" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCost" runat="server" Text='<%# Eval("OM_OTHER_COST_AMT") %>'  Width="270px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" ToolTip="Edit"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" ToolTip="Delete"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" ToolTip="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" ToolTip="Cancel"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" ToolTip="Add"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
         <asp:HiddenField runat="server" ID="hforderNo" />
    <asp:HiddenField runat="server" ID="hfitem" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" 
                            CssClass="btn"/>
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                             />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                             />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                             />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
