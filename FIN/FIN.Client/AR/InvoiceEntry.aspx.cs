﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AR;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.AR
{
    public partial class InvoiceEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        OM_CUST_INVOICE_HDR oM_CUST_INVOICE_HDR = new OM_CUST_INVOICE_HDR();
        OM_CUST_INVOICE_DTL oM_CUST_INVOICE_DTL = new OM_CUST_INVOICE_DTL();
        DataTable dtGridData = new DataTable();
        FIN.BLL.AP.Terms_BLL Terms_BLL = new FIN.BLL.AP.Terms_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        decimal dblRunningTotal = 0;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session["InvTaxDet"] = null;
                    AssignToControl();

                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetSupplierName(ref ddlSupplierName);
            FIN.BLL.AP.Supplier_BLL.GetSupplierName_ForArInvoice(ref ddlSupplierNumber);
            Lookup_BLL.GetLookUpValues(ref ddlInvoiceType, "SO_INV_TY");
            Lookup_BLL.GetLookUpValues(ref ddlPaymentType, "PAYMENT_MODE");
            FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref ddlInvoiceCurrency);
            FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref ddlPaymentCurrency);
            FIN.BLL.AP.Terms_BLL.getTermName(ref ddlTerm);
            FIN.BLL.AP.InvoiceBatch_BLL.fn_getIvoiceBatch(ref ddlBatch);

            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);
            Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");


        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                ddlInvoiceCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
                ddlPaymentCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
                txtInvoiceDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = false;


              
                btnPrint.Visible = false;



                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnCancel.Visible = false;
                imgBtnCancelJV.Visible = false;
                imgbtninvoicecancel.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    ddlInvoiceType.Enabled = false;
                    imgbtninvoicecancel.Visible = true;
                    oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = oM_CUST_INVOICE_HDR;
                    btnPrint.Visible = true;
                    txtInvoiceNumber.Text = oM_CUST_INVOICE_HDR.OM_INV_NUM;
                    txtInvoiceDate.Text = DBMethod.ConvertDateToString(oM_CUST_INVOICE_HDR.OM_INV_DATE.ToString());

                    ddlSupplierNumber.SelectedValue = oM_CUST_INVOICE_HDR.OM_VENDOR_ID;
                    BindSupplierName();
                    ddlSupplierName.SelectedValue = oM_CUST_INVOICE_HDR.OM_VENDOR_ID;
                    ddlInvoiceType.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_TYPE;
                    ddlPaymentType.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_PAYMENT_METHOD;

                    ddlInvoiceCurrency.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_CURR_CODE;
                    ddlPaymentCurrency.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_RECEIPT_CURR_CODE;
                    txtReason.Text = oM_CUST_INVOICE_HDR.OM_INV_REMARKS;
                    txtRejectedReason.Text = oM_CUST_INVOICE_HDR.OM_INV_REJECT_REASON;
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_CUST_INVOICE_HDR.OM_INV_AMT.ToString());
                    txtTax.Text = oM_CUST_INVOICE_HDR.OM_INV_TAX_AMT.ToString();
                    ddlTerm.SelectedValue = oM_CUST_INVOICE_HDR.OM_TERM_ID;
                    ddlBatch.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_BATCH_ID;


                    if (oM_CUST_INVOICE_HDR.OM_VENDOR_LOC_ID != null)
                        ddlSupplierSite.SelectedValue = oM_CUST_INVOICE_HDR.OM_VENDOR_LOC_ID;
                    if (oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE_TYPE != null)
                        ddlExchangeRateType.SelectedValue = oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE_TYPE;
                    txtExchangeRate.Text = oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE.ToString();


                    if (oM_CUST_INVOICE_HDR.OM_INV_DUE_DATE != null)
                    {
                        txtInvDueDate.Text = DBMethod.ConvertDateToString(oM_CUST_INVOICE_HDR.OM_INV_DUE_DATE.ToString());
                    }

                    if (oM_CUST_INVOICE_HDR.GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = oM_CUST_INVOICE_HDR.GLOBAL_SEGMENT_ID;
                    }
                    if (oM_CUST_INVOICE_HDR.OM_INV_RETENTION_AMT != null)
                    {
                        txtRetAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_CUST_INVOICE_HDR.OM_INV_RETENTION_AMT.ToString());
                    }
                    if (oM_CUST_INVOICE_HDR.OM_INV_PREPAY_BALANCE != null)
                    {
                        txtPrePaymentBal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_CUST_INVOICE_HDR.OM_INV_PREPAY_BALANCE.ToString());
                    }


                    if (oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                    }
                    if (oM_CUST_INVOICE_HDR.POSTED_FLAG != null)
                    {
                        if (oM_CUST_INVOICE_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;

                            imgBtnPost.Visible = false;
                            lblPosted.Visible = true;

                            imgBtnJVPrint.Visible = true;

                            imgBtnCancel.Visible = true;
                            lblCancelled.Visible = false;


                            pnlgridview.Enabled = false;
                            pnltdHeader.Enabled = false;
                            pnlReason.Enabled = false;
                            pnlRejReason.Enabled = false;

                            imgbtninvoicecancel.Visible = false;

                        }
                    }
                    if (oM_CUST_INVOICE_HDR.REV_FLAG != null)
                    {
                        if (oM_CUST_INVOICE_HDR.REV_FLAG.Trim() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;
                            imgBtnCancel.Visible = false;
                            lblCancelled.Visible = true;
                            imgbtninvoicecancel.Visible = false;
                           
                        }
                    }
                    if (oM_CUST_INVOICE_HDR.REV_JE_HDR_ID != null)
                    {
                        if (oM_CUST_INVOICE_HDR.REV_JE_HDR_ID.ToString().Length >0)
                        {
                            imgBtnCancelJV.Visible = true;
                        }
                    }
                    if (FINSP.Is_workflow_defined("AP_021"))
                    {
                        if (oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS == "0")
                        {
                            lblWaitApprove.Visible = true;
                        }
                        else
                        {
                            lblWaitApprove.Visible = false;
                        }
                    }
                    else
                    {
                        lblWaitApprove.Visible = false;
                    }

                }

                dtGridData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    // dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_ITEM_QTY", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("OM_ITEM_QTY"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_ITEM_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_ITEM_PRICE"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_ITEM_COST", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_ITEM_COST"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                txtInvoiceAmount.Text = CommonUtils.CalculateTotalAmount(dtData, "OM_ITEM_COST");
                Session["InvAmt"] = txtInvoiceAmount.Text;
                txtTax.Text = CommonUtils.CalculateTotalAmount(dtData, "OM_INV_LINE_TAX_AMT");
                hf_TotTaxAmt.Value = "";
                //  txtTotalQty.Text = CommonUtils.CalculateTotalAmount(dtData, "OM_ITEM_QTY");
                SetColVis4InvoiceType();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSupplierName();
        }
        private void BindSupplierName()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName_ForArInvoice(ddlSupplierNumber.SelectedValue.ToString())).Tables[0];

                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        ddlSupplierName.SelectedItem.Text = dtData.Rows[0][FINColumnConstants.VENDOR_NAME].ToString();
                    }
                }
                FIN.BLL.AP.SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref  ddlSupplierSite, ddlSupplierNumber.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnOkay_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlSegment1;
                slControls[1] = ddlSegment2;
                slControls[2] = ddlSegment3;
                slControls[3] = ddlSegment4;
                slControls[4] = ddlSegment5;
                slControls[5] = ddlSegment6;

                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

                string strMessage = "Segment1 ~ Segment2 ~ Segment3~ Segment4 ~ Segment5 ~ Segment6";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}
                //else
                //{
                ModalPopupExtender2.Hide();
                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void ddlGSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGSegmentValues(sender);
        }
        private void LoadGSegmentValues(object sender)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Seg = (DropDownList)sender;
            int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlGSegment", ""));
            DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (int_ddlNo + 1));
            if (ddlGSegment != null && int_ddlNo < dtdataAc.Rows.Count)
            {
                if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                }
                else
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                }

            }
        }

        private void BindGSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccCode");

            if (ddl_AccountCodes.SelectedValue != null && ddlGlobalSegment.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty && ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {

                        if (iLoop == 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlGSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            ddlGSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["OM_INV_SEGMENT_ID" + (iLoop + 1)].ToString();

                        }
                    }
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
                DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
                DropDownList ddlAccCode = tmpgvr.FindControl("ddlAccCode") as DropDownList;
                DropDownList dd_lItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
                // PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddl_ReceiptNo);
                //  Lookup_BLL.GetLookUpValues(ref ddl_LineType, "LINETYPE");


                ddl_LineType.Items.Clear();
                ddl_LineType.Items.Add(new ListItem("---Select---", ""));
                if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                {
                    ddl_LineType.Items.Add(new ListItem("Item", "Item"));
                    ddl_LineType.Items.Add(new ListItem("Other Charges", "Other_Charges"));
                    ddl_LineType.Items.Add(new ListItem("Service", "Service"));
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "SALES ORDER INVOICE")
                {
                    ddl_LineType.Items.Add(new ListItem("Sales Order Item", "Sales Order Item"));
                    ddl_LineType.Items.Add(new ListItem("Other Charges", "Other_Charges"));
                    ddl_LineType.Items.Add(new ListItem("Service", "Service"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                    ddl_LineType.Items.Add(new ListItem("Sales Order", "Sales Order"));
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "CREDIT MEMO")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "DEBIT MEMO")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                }


                else if (ddlInvoiceType.SelectedValue.ToString() == "4")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "9")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "1")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "3")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "5")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "16")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "17")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString() == "2")
                {
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }


                AccountCodes_BLL.fn_getAccount(ref ddlAccCode);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_LineType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.OM_INV_LINE_TYPE].ToString();
                    ddlAccCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CODE_ID"].ToString();
                    FillEntityNumber(tmpgvr);
                    ddl_ReceiptNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.OM_DC_ID].ToString();
                    FillItemNo(tmpgvr);

                    dd_lItemNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ITEM_ID"].ToString();




                    BindGSegment(tmpgvr);
                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID1"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID1"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment1 = tmpgvr.FindControl("ddlGSegment1") as DropDownList;
                        ddl_GSegment1.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID1"].ToString();
                        LoadGSegmentValues(ddl_GSegment1);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID2"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID2"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment2 = tmpgvr.FindControl("ddlGSegment2") as DropDownList;
                        ddl_GSegment2.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID2"].ToString();
                        LoadGSegmentValues(ddl_GSegment2);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID3"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID3"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment3 = tmpgvr.FindControl("ddlGSegment3") as DropDownList;
                        ddl_GSegment3.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID3"].ToString();
                        LoadGSegmentValues(ddl_GSegment3);
                    }


                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID4"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID4"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment4 = tmpgvr.FindControl("ddlGSegment4") as DropDownList;
                        ddl_GSegment4.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID4"].ToString();
                        LoadGSegmentValues(ddl_GSegment4);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID5"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID5"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment5 = tmpgvr.FindControl("ddlGSegment5") as DropDownList;
                        ddl_GSegment5.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID5"].ToString();
                        LoadGSegmentValues(ddl_GSegment5);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID6"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID6"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment6 = tmpgvr.FindControl("ddlGSegment6") as DropDownList;
                        ddl_GSegment6.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["OM_INV_SEGMENT_ID6"].ToString();
                        LoadGSegmentValues(ddl_GSegment6);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                BindGSegment(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txtLineNo = new TextBox(); ;
            DropDownList ddl_ReceiptNo = gvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = gvr.FindControl("ddlItemNo") as DropDownList;
            TextBox txt_Quantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txt_UnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            DropDownList ddl_LineType = gvr.FindControl("ddlLineType") as DropDownList;
            TextBox txtMisNo = gvr.FindControl("txtMisNo") as TextBox;
            DropDownList ddl_AccCode = gvr.FindControl("ddlAccCode") as DropDownList;


            DropDownList ddl_GSegment1 = gvr.FindControl("ddlGSegment1") as DropDownList;
            DropDownList ddl_GSegment2 = gvr.FindControl("ddlGSegment2") as DropDownList;
            DropDownList ddl_GSegment3 = gvr.FindControl("ddlGSegment3") as DropDownList;
            DropDownList ddl_GSegment4 = gvr.FindControl("ddlGSegment4") as DropDownList;
            DropDownList ddl_GSegment5 = gvr.FindControl("ddlGSegment5") as DropDownList;
            DropDownList ddl_GSegment6 = gvr.FindControl("ddlGSegment6") as DropDownList;



            ErrorCollection.Clear();
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["OM_CUST_INV_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();


            if (ddl_LineType.SelectedValue == FINAppConstants.intialRowValueField && ddl_LineType.SelectedItem.Text == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ddlLineType", "Line Type cannot be empty");
                return drList;
            }
            else if (ddl_LineType.SelectedItem.Text == "Miscellaneous")
            {
                slControls[0] = txtMisNo;
                slControls[1] = txt_UnitPrice;
                slControls[2] = ddl_AccCode;
                slControls[3] = txt_Amount;
                slControls[4] = ddl_GSegment1;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + "~" + " Segment1 ";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            }
            else
            {
                slControls[0] = txtLineNo;
                slControls[1] = ddl_ReceiptNo;
                slControls[2] = ddl_ItemNo;
                slControls[3] = txt_Quantity;
                slControls[4] = txt_UnitPrice;
                slControls[5] = ddl_AccCode;
                slControls[6] = txt_Amount;
                slControls[7] = ddl_LineType;
                slControls[8] = ddl_GSegment1;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Line_Number_P"] + " ~ " + Prop_File_Data["Receipt_Number_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + " ~ " + Prop_File_Data["Line_Type_P"] + "~" + " Segment1 ";
                // string strMessage = "Line No ~ Receipt No ~ Item Name ~ Quentity ~ Unit Price ~ Account Code ~ Amount ~ Line Type";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            }

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "ITEM_ID='" + ddl_ItemNo.SelectedValue.ToString() + "'";
            string strMessage1 = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage1);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            ErrorCollection.Clear();
            DataTable dtSaleorderQty = new DataTable();
            DataTable dtQtyfrominvoice = new DataTable();

            dtSaleorderQty = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.GetSaleOrderQty(ddl_ReceiptNo.SelectedValue, ddl_ItemNo.SelectedValue).ToString()).Tables[0];
            if (dtSaleorderQty.Rows.Count > 0)
            {
                hfSalorderqty.Value = dtSaleorderQty.Rows[0]["om_order_qty"].ToString();
                if (decimal.Parse(txt_Quantity.Text) > decimal.Parse(hfSalorderqty.Value))
                {
                    ErrorCollection.Add("invqty", "Quantity should not exceed order quantity.");
                    return drList;
                }

                //if (txtTotalQty.Text.Length > 0)
                //{
                //    Qtyadd.Value = (decimal.Parse(txtTotalQty.Text) + decimal.Parse(txt_Quantity.Text)).ToString();

                //    if (decimal.Parse(Qtyadd.Value) > decimal.Parse(hfSalorderqty.Value))
                //    {
                //        ErrorCollection.Add("invqty", "Quantity should not exceed order quantity.");
                //        return drList;
                //    }

                //}


                dtQtyfrominvoice = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.GetitemQty_fromINVOICE(ddl_ReceiptNo.SelectedValue, ddl_ItemNo.SelectedValue).ToString()).Tables[0];
                if (dtQtyfrominvoice.Rows.Count > 0)
                {
                    HFQTYFROMINV.Value = dtQtyfrominvoice.Rows[0]["om_item_qty"].ToString();
                    Qtyadd.Value = (decimal.Parse(HFQTYFROMINV.Value) + decimal.Parse(txt_Quantity.Text)).ToString();
                    if (decimal.Parse(Qtyadd.Value) > decimal.Parse(hfSalorderqty.Value))
                    {
                        ErrorCollection.Add("invqty1", "Quantity should not exceed order quantity.");
                        return drList;
                    }
                }

            }
            ErrorCollection.Clear();



            if (ddl_ReceiptNo.SelectedValue != null && ddl_ReceiptNo.SelectedItem != null)
            {
                if (ddl_ReceiptNo.SelectedValue.ToString() != FINAppConstants.intialRowValueField && ddl_ReceiptNo.SelectedItem.Text.ToString() != FINAppConstants.intialRowTextField)
                {
                    //drList[FINColumnConstants.RECEIPT_ID] = ddl_ReceiptNo.SelectedValue.ToString();
                    drList[FINColumnConstants.GRN_NUM] = ddl_ReceiptNo.SelectedValue == null ? string.Empty : ddl_ReceiptNo.SelectedItem.Text;
                    drList[FINColumnConstants.OM_DC_ID] = ddl_ReceiptNo.SelectedValue.ToString();
                }
            }


            if (ddl_ItemNo.SelectedValue != null && ddl_ItemNo.SelectedItem != null)
            {
                if (ddl_ItemNo.SelectedValue.ToString() != FINAppConstants.intialRowValueField && ddl_ItemNo.SelectedItem.Text.ToString() != FINAppConstants.intialRowTextField)
                {
                    drList[FINColumnConstants.ITEM_ID] = ddl_ItemNo.SelectedValue == null ? string.Empty : ddl_ItemNo.SelectedValue;
                    drList[FINColumnConstants.ITEM_NAME] = ddl_ItemNo.SelectedItem.Text;
                }
            }


            drList[FINColumnConstants.OM_INV_LINE_NUM] = txtLineNo.Text;
            drList["OM_INV_LINE_TYPE"] = ddl_LineType.SelectedValue.ToString();
            drList["OM_INV_LINE_TYPE_DESC"] = ddl_LineType.SelectedItem.Text.ToString();

            //drList[FINColumnConstants.GRN_NUM] = ddl_ReceiptNo.SelectedItem.Text.ToString();

            drList["MIS_NO"] = txtMisNo.Text;
            // drList[FINColumnConstants.ITEM_ID] = ddl_ItemNo.SelectedValue.ToString();
            // drList[FINColumnConstants.ITEM_NAME] = ddl_ItemNo.SelectedItem.Text.ToString();
            if (txt_Quantity.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_ITEM_QTY] = txt_Quantity.Text;
            }
            else
            {
                drList[FINColumnConstants.OM_ITEM_QTY] = null;
            }
            if (txt_UnitPrice.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_ITEM_PRICE] = txt_UnitPrice.Text;
            }
            else
            {
                drList[FINColumnConstants.OM_ITEM_PRICE] = null;
            }


            drList["OM_ITEM_COST"] = txt_Amount.Text;

            drList[FINColumnConstants.CODE_ID] = ddl_AccCode.SelectedValue.ToString();
            drList[FINColumnConstants.CODE_NAME] = ddl_AccCode.SelectedItem.Text.ToString();

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;
            if (hf_TotTaxAmt.Value.ToString().Length > 0)
            {
                drList["OM_INV_LINE_TAX_AMT"] = hf_TotTaxAmt.Value.ToString();
            }

            if (ddl_GSegment1.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID1] = ddl_GSegment1.SelectedValue.ToString();
                drList["SEGMENT_1_TEXT"] = ddl_GSegment1.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID1"] = null;
                drList["SEGMENT_1_TEXT"] = null;
            }
            if (ddl_GSegment2.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID2] = ddl_GSegment2.SelectedValue.ToString();
                drList["SEGMENT_2_TEXT"] = ddl_GSegment2.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID2"] = null;
                drList["SEGMENT_2_TEXT"] = null;
            }
            if (ddl_GSegment3.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID3] = ddl_GSegment3.SelectedValue.ToString();
                drList["SEGMENT_3_TEXT"] = ddl_GSegment3.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID3"] = null;
                drList["SEGMENT_3_TEXT"] = null;
            }
            if (ddl_GSegment4.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID4] = ddl_GSegment4.SelectedValue.ToString();
                drList["SEGMENT_4_TEXT"] = ddl_GSegment4.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID4"] = null;
                drList["SEGMENT_4_TEXT"] = null;
            } if (ddl_GSegment5.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID5] = ddl_GSegment5.SelectedValue.ToString();
                drList["SEGMENT_5_TEXT"] = ddl_GSegment5.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID5"] = null;
                drList["SEGMENT_5_TEXT"] = null;
            }
            if (ddl_GSegment6.SelectedValue.ToString().Length > 0)
            {
                drList[FINColumnConstants.OM_INV_SEGMENT_ID6] = ddl_GSegment6.SelectedValue.ToString();
                drList["SEGMENT_6_TEXT"] = ddl_GSegment6.SelectedItem.Text.ToString();
            }
            else
            {
                drList["OM_INV_SEGMENT_ID6"] = null;
                drList["SEGMENT_6_TEXT"] = null;
            }
            ddlSegment1.SelectedIndex = 0;
            ddlSegment2.SelectedIndex = 0;
            ddlSegment3.SelectedIndex = 0;
            ddlSegment4.SelectedIndex = 0;
            ddlSegment5.SelectedIndex = 0;
            ddlSegment6.SelectedIndex = 0;




            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
                //if (e.Row.RowType == DataControlRowType.Footer)
                //{
                //    //calculate colspan. we want to display totals in the last column
                //    //and label in the last but one column. since we dont want to deal
                //    //with the borders and cell widths we simply merge remaining columns
                //    int labelTextCellColsSpan = e.Row.Cells.Count - 1;

                //    //create a new row. this is our footer row
                //    GridViewRow runningTotalRow = new GridViewRow(0, 0,
                //        DataControlRowType.Footer, DataControlRowState.Normal);

                //    //create running Total label text
                //    TableCell cell = new TableCell();
                //    cell.Text = "Invoice Amount: ";
                //    cell.HorizontalAlign = HorizontalAlign.Right;
                //    cell.ColumnSpan = labelTextCellColsSpan;

                //    //add the label text cell to the row
                //    runningTotalRow.Cells.Add(cell);

                //    //create running total amount cell
                //    cell = new TableCell();
                //    if (Session["InvAmt"].ToString() != null && Session["InvAmt"].ToString().Length > 0)
                //    {
                //        cell.Text = Session["InvAmt"].ToString();// dblRunningTotal.ToString("C");
                //    }
                //    else
                //    {
                //        cell.Text = "0";
                //    }
                //    cell.HorizontalAlign = HorizontalAlign.Right;

                //    //add the amount cell to the row
                //    runningTotalRow.Cells.Add(cell);

                //    //add the row to the gridview
                //    gvData.Controls[0].Controls.Add(runningTotalRow);

                //    //we only want two cells, hide the rest of them                
                //    for (int colIndex = 0; colIndex <= labelTextCellColsSpan; colIndex++)
                //        e.Row.Cells[colIndex].Visible = false;

                //    ////set the grant total text
                //    //e.Row.Cells[0].ColumnSpan = labelTextCellColsSpan;
                //    //e.Row.Cells[0].Text = "Grand Total : ";
                //    //e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;

                //    ////set the grant total amount value
                //    //e.Row.Cells[1].Text = dblTotalAmount.ToString("C");
                //    //e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion


        private void PaymentCurrecnyChecking()
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
            if ((ddlPaymentCurrency.SelectedValue == ddlInvoiceCurrency.SelectedValue) || (ddlPaymentCurrency.SelectedValue == Session[FINSessionConstants.ORGCurrency].ToString()))
            {
            }
            else
            {
                ErrorCollection.Add("InvalidPayCurrecny", Prop_File_Data["InvalidPayCurrecny_P"]);

            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Master.Mode == FINAppConstants.WF)
                {
                    return;
                }
                System.Collections.SortedList slControls = new System.Collections.SortedList();

              //  slControls[0] = txtInvoiceNumber;
                slControls[0] = ddlSupplierNumber;
                slControls[1] = ddlExchangeRateType;

                string strCtrlTypes = "DropDownList~DropDownList";

                string strMessage = "Customer Number ~ Exchange Rate Type";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                PaymentCurrecnyChecking();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Remove("exerror");

                if (txtExchangeRate.Text == "0")
                {
                    ErrorCollection.Add("exerror", "Exchange Rate cannot be zero");
                    return;
                }
                ErrorCollection.Clear();
                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_CUST_INVOICE_HDR = (OM_CUST_INVOICE_HDR)EntityData;
                }

                oM_CUST_INVOICE_HDR.OM_INV_NUM = txtInvoiceNumber.Text;

                if (txtInvoiceDate.Text != string.Empty)
                {
                    oM_CUST_INVOICE_HDR.OM_INV_DATE = DBMethod.ConvertStringToDate(txtInvoiceDate.Text.ToString());
                }
                oM_CUST_INVOICE_HDR.OM_VENDOR_ID = ddlSupplierNumber.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.OM_INV_TYPE = (ddlInvoiceType.SelectedValue.ToString());
                oM_CUST_INVOICE_HDR.OM_INV_PAYMENT_METHOD = ddlPaymentType.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.OM_INV_CURR_CODE = ddlInvoiceCurrency.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.OM_INV_RECEIPT_CURR_CODE = ddlPaymentCurrency.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.OM_INV_REMARKS = txtReason.Text;
                oM_CUST_INVOICE_HDR.OM_INV_REJECT_REASON = txtRejectedReason.Text;
                oM_CUST_INVOICE_HDR.OM_INV_AMT = CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text);
                oM_CUST_INVOICE_HDR.OM_INV_TAX_AMT = CommonUtils.ConvertStringToDecimal(txtTax.Text);
                oM_CUST_INVOICE_HDR.OM_TERM_ID = ddlTerm.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.OM_INV_BATCH_ID = ddlBatch.SelectedValue.ToString();
                oM_CUST_INVOICE_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;


                oM_CUST_INVOICE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                // oM_CUST_INVOICE_HDR .WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                oM_CUST_INVOICE_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (txtInvDueDate.Text.ToString().Trim().Length > 0)
                {
                    oM_CUST_INVOICE_HDR.OM_INV_DUE_DATE = DBMethod.ConvertStringToDate(txtInvDueDate.Text);
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_DUE_DATE = null;
                }
                if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                {
                    oM_CUST_INVOICE_HDR.OM_VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_VENDOR_LOC_ID = null;
                }

                if (ddlExchangeRateType.SelectedValue.ToString().Length > 0)
                {
                    oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE_TYPE = ddlExchangeRateType.SelectedValue;
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE_TYPE = null;
                }
                if (txtExchangeRate.Text.ToString().Length > 0)
                {

                    oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE = decimal.Parse(txtExchangeRate.Text);
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_EXCHANGE_RATE = null;
                }
                if (txtRetAmount.Text.ToString().Length > 0)
                {
                    oM_CUST_INVOICE_HDR.OM_INV_RETENTION_AMT = CommonUtils.ConvertStringToDecimal(txtRetAmount.Text);
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_RETENTION_AMT = null;
                }
                if (txtPrePaymentBal.Text.ToString().Length > 0)
                {
                    oM_CUST_INVOICE_HDR.OM_INV_PREPAY_BALANCE = CommonUtils.ConvertStringToDecimal(txtPrePaymentBal.Text);
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_PREPAY_BALANCE = null;
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_CUST_INVOICE_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_CUST_INVOICE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_CUST_INVOICE_HDR.OM_INV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_021_M.ToString(), false, true);

                    oM_CUST_INVOICE_HDR.OM_INV_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_021_IN.ToString(), false, true);
                    oM_CUST_INVOICE_HDR.CREATED_BY = this.LoggedUserName;
                    oM_CUST_INVOICE_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS = "0";


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS
                //string ProReturn = FIN.DAL.HR.Employee_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, oM_CUST_INVOICE_HDR .INV_NUM, oM_CUST_INVOICE_HDR .INV_ID);

                // if (ProReturn != string.Empty)
                // {
                //     if (ProReturn != "0")
                //     {
                //         ErrorCollection.Add("InvoiceNumDuplication", ProReturn);
                //         if (ErrorCollection.Count > 0)
                //         {
                //             return;
                //         }
                //     }
                // }

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Invoice ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                OM_CUST_INVOICE_DTL oM_CUST_INVOICE_DTL = new OM_CUST_INVOICE_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    oM_CUST_INVOICE_DTL = new OM_CUST_INVOICE_DTL();
                    if (dtGridData.Rows[iLoop]["OM_CUST_INV_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_CUST_INV_DTL_ID"].ToString() != string.Empty)
                    {
                        oM_CUST_INVOICE_DTL = Invoice_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["OM_CUST_INV_DTL_ID"].ToString());
                    }
                    oM_CUST_INVOICE_DTL.OM_INV_LINE_TYPE = dtGridData.Rows[iLoop]["OM_INV_LINE_TYPE"].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_LINE_NUM = iLoop;
                    oM_CUST_INVOICE_DTL.OM_ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    oM_CUST_INVOICE_DTL.OM_ITEM_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ITEM_QTY].ToString());
                    oM_CUST_INVOICE_DTL.OM_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ITEM_PRICE].ToString());
                    oM_CUST_INVOICE_DTL.OM_ITEM_COST = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_ITEM_COST"].ToString());
                    oM_CUST_INVOICE_DTL.OM_ITEM_ACCT_CODE_ID = dtGridData.Rows[iLoop][FINColumnConstants.CODE_ID].ToString();
                    oM_CUST_INVOICE_DTL.OM_DC_ID = dtGridData.Rows[iLoop][FINColumnConstants.OM_DC_ID].ToString();
                    if (dtGridData.Rows[iLoop]["OM_INV_LINE_TAX_AMT"] != null && dtGridData.Rows[iLoop]["OM_INV_LINE_TAX_AMT"].ToString().Length > 0)
                    {
                        oM_CUST_INVOICE_DTL.OM_INV_LINE_TAX_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_INV_LINE_TAX_AMT"].ToString());
                    }
                    else
                    {
                        oM_CUST_INVOICE_DTL.OM_INV_LINE_TAX_AMT = null;
                    }
                    // oM_CUST_INVOICE_DTL .WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    oM_CUST_INVOICE_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    oM_CUST_INVOICE_DTL.ATTRIBUTE1 = dtGridData.Rows[iLoop]["MIS_NO"].ToString();


                    oM_CUST_INVOICE_DTL.OM_INV_ID = oM_CUST_INVOICE_HDR.OM_INV_ID;

                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID1 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID1].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID2 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID2].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID3 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID3].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID4 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID4].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID5 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID5].ToString();
                    oM_CUST_INVOICE_DTL.OM_INV_SEGMENT_ID6 = dtGridData.Rows[iLoop][FINColumnConstants.OM_INV_SEGMENT_ID6].ToString();



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(oM_CUST_INVOICE_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["OM_CUST_INV_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_CUST_INV_DTL_ID"].ToString() != string.Empty)
                        {
                            oM_CUST_INVOICE_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_CUST_INVOICE_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            oM_CUST_INVOICE_DTL.OM_CUST_INV_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_021_D.ToString(), false, true);
                            oM_CUST_INVOICE_DTL.CREATED_BY = this.LoggedUserName;
                            oM_CUST_INVOICE_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_CUST_INVOICE_DTL, FINAppConstants.Add));
                        }
                    }
                    oM_CUST_INVOICE_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                }

                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(oM_CUST_INVOICE_HDR.OM_INV_ID, txtInvoiceDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_CUST_INVOICE_HDR, OM_CUST_INVOICE_DTL>(oM_CUST_INVOICE_HDR, tmpChildEntity, oM_CUST_INVOICE_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<OM_CUST_INVOICE_HDR, OM_CUST_INVOICE_DTL>(oM_CUST_INVOICE_HDR, tmpChildEntity, oM_CUST_INVOICE_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_CUST_INVOICE_HDR.OM_INV_ID);

                DBMethod.SaveEntity<OM_CUST_INVOICE_HDR>(oM_CUST_INVOICE_HDR, true);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    // FINSP.GetSP_GL_Posting(oM_CUST_INVOICE_HDR.OM_INV_ID, "AR_010");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (Session["InvTaxDet"] != null)
                {
                    DataTable dt_invTax = (DataTable)Session["InvTaxDet"];
                    if (dt_invTax.Rows.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < dt_invTax.Rows.Count; iLoop++)
                        {
                            INV_TAX_DTLS iNV_TAX_DTLS = new INV_TAX_DTLS();
                            double dbl_TaxAmount = 0;
                            if (dt_invTax.Rows[iLoop]["INV_TAX_AMT"] != null)
                            {
                                if (dt_invTax.Rows[iLoop]["INV_TAX_AMT"].ToString().Length > 0)
                                {
                                    dbl_TaxAmount = double.Parse(dt_invTax.Rows[iLoop]["INV_TAX_AMT"].ToString());
                                }
                                else
                                {
                                    dbl_TaxAmount = 0;
                                }
                            }
                            else
                            {
                                dbl_TaxAmount = 0;
                            }

                            if (dt_invTax.Rows[iLoop]["INV_TAX_ID"].ToString() != "0")
                            {

                                using (IRepository<INV_TAX_DTLS> userCtx = new DataRepository<INV_TAX_DTLS>())
                                {
                                    iNV_TAX_DTLS = userCtx.Find(r =>
                                        (r.INV_TAX_ID == dt_invTax.Rows[iLoop]["INV_TAX_ID"].ToString())
                                        ).SingleOrDefault();
                                }
                                if (dbl_TaxAmount > 0)
                                {
                                    DBMethod.DeleteEntity<INV_TAX_DTLS>(iNV_TAX_DTLS);
                                }
                                else
                                {
                                    iNV_TAX_DTLS.MODIFIED_BY = this.LoggedUserName;
                                    iNV_TAX_DTLS.MODIFIED_DATE = DateTime.Now;
                                    iNV_TAX_DTLS.INV_TAX_AMT = decimal.Parse(dbl_TaxAmount.ToString());
                                    DBMethod.SaveEntity<INV_TAX_DTLS>(iNV_TAX_DTLS, true);
                                }
                            }
                            else
                            {
                                OM_CUST_INVOICE_DTL tmp_oM_CUST_INVOICE_DTL = new OM_CUST_INVOICE_DTL();
                                using (IRepository<OM_CUST_INVOICE_DTL> userCtx = new DataRepository<OM_CUST_INVOICE_DTL>())
                                {
                                    tmp_oM_CUST_INVOICE_DTL = userCtx.Find(r =>
                                        (r.OM_INV_ID == oM_CUST_INVOICE_HDR.OM_INV_ID && r.OM_DC_ID == dt_invTax.Rows[iLoop]["EntityNo"].ToString() && r.OM_ITEM_ID == dt_invTax.Rows[iLoop]["ItemNo"].ToString())
                                        ).SingleOrDefault();
                                }

                                if (dbl_TaxAmount > 0)
                                {
                                    iNV_TAX_DTLS.INV_ID = oM_CUST_INVOICE_HDR.OM_INV_ID;
                                    iNV_TAX_DTLS.INV_LINE_ID = tmp_oM_CUST_INVOICE_DTL.OM_CUST_INV_DTL_ID;
                                    iNV_TAX_DTLS.CREATED_BY = this.LoggedUserName;
                                    iNV_TAX_DTLS.CREATED_DATE = DateTime.Now;
                                    iNV_TAX_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                                    iNV_TAX_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                                    iNV_TAX_DTLS.TAX_ID = gvTaxDet.DataKeys[iLoop].Values["TAX_ID"].ToString();
                                    iNV_TAX_DTLS.INV_TAX_AMT = decimal.Parse(dbl_TaxAmount.ToString());
                                    iNV_TAX_DTLS.INV_TAX_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_T.ToString(), false, true);
                                    DBMethod.SaveEntity<INV_TAX_DTLS>(iNV_TAX_DTLS);
                                }
                            }
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {


                string str_sp_output = FINSP.SP_POSTING_ACCTCODE_VALIDATION("AR_009", ddlSupplierNumber.SelectedValue, Master.StrRecordId);

                if (str_sp_output != "SUCCESS")
                {
                    ErrorCollection.Add("PostingAcctErr", str_sp_output);
                    return;
                }

                if (EntityData != null)
                {
                    oM_CUST_INVOICE_HDR = (OM_CUST_INVOICE_HDR)EntityData;
                }
                if (oM_CUST_INVOICE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(oM_CUST_INVOICE_HDR.OM_INV_ID, "AR_009");

                }

                oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
                oM_CUST_INVOICE_HDR.POSTED_FLAG = FINAppConstants.Y;
                oM_CUST_INVOICE_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<OM_CUST_INVOICE_HDR>(oM_CUST_INVOICE_HDR, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


                imgBtnPost.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;
                pnlReason.Enabled = false;
                pnlRejReason.Enabled = false;

                imgbtninvoicecancel.Visible = false;
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", oM_CUST_INVOICE_HDR.JE_HDR_ID));

                if (VMVServices.Web.Utils.LanguageCode == string.Empty)
                {
                    Session["ProgramName"] = "Journal Voucher";
                }
                else
                {
                    Session["ProgramName"] = "ملكة جمال لكمة";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

            if (oM_CUST_INVOICE_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", oM_CUST_INVOICE_HDR.JE_HDR_ID));

                if (VMVServices.Web.Utils.LanguageCode == string.Empty)
                {
                    Session["ProgramName"] = "Journal Voucher";
                }
                else
                {
                    Session["ProgramName"] = "ملكة جمال لكمة";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

                    ErrorCollection.Remove("Paythere");
                    if (FIN.DAL.AR.Invoice_DAL.IsPaymentRecordFound(oM_CUST_INVOICE_HDR.OM_INV_ID))
                    {
                        ErrorCollection.Add("Paythere", "Receipt is already exists for this invoice.Please stop the Receipt, before cancel the invoice");
                        return;
                    }


                    if (oM_CUST_INVOICE_HDR.POSTED_FLAG != null)
                    {
                        if (oM_CUST_INVOICE_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(oM_CUST_INVOICE_HDR.JE_HDR_ID, "AR_009_R", this.LoggedUserName);
                        }
                    }

                    imgBtnPost.Visible = false;
                    lblCancelled.Visible = true;
                    imgBtnCancelJV.Visible = true;
                    imgBtnCancel.Visible = false;

                    lblPosted.Visible = true;

                    pnlgridview.Enabled = false;
                    pnltdHeader.Enabled = false;
                    pnlReason.Enabled = false;
                    pnlRejReason.Enabled = false;

                    btnSave.Visible = false;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                    oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
                }
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";



                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", oM_CUST_INVOICE_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnCancelJV_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

                if (oM_CUST_INVOICE_HDR != null)
                {

                    Hashtable htParameters = new Hashtable();
                    Hashtable htHeadingParameters = new Hashtable();
                    Hashtable htFilterParameter = new Hashtable();

                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", oM_CUST_INVOICE_HDR.REV_JE_HDR_ID));


                    if (VMVServices.Web.Utils.LanguageCode == string.Empty)
                    {
                        Session["ProgramName"] = "Journal Voucher";
                    }
                    else
                    {
                        Session["ProgramName"] = "ملكة جمال لكمة";
                    }
                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        
        protected void btnSegments_Click(object sender, EventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_AccCode = (DropDownList)gvr.FindControl("ddlAccCode");
            if (ddl_AccCode.SelectedValue.ToString().Trim().Length > 0)
            {
                //FIN.BLL.GL.AccountCodes_BLL.getSegmentValues(ref ddlSegment1, ref ddlSegment2, ref ddlSegment3, ref ddlSegment4, ref ddlSegment5, ref ddlSegment6, ddl_AccCode.SelectedValue.ToString());
                //if (gvData.EditIndex > 0)
                //{
                //    ddlSegment1.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID1].ToString();
                //    ddlSegment2.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID2].ToString();
                //    ddlSegment3.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID3].ToString();
                //    ddlSegment4.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID4].ToString();
                //    ddlSegment5.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID5].ToString();
                //    ddlSegment6.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID6].ToString();
                //}



                BindSegmentValues(gvr);
                ModalPopupExtender2.Show();
            }
            else
            {

                ErrorCollection.Add("SelectAccCode", "Please Select Account Code ");
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            }

            // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            //ImageButton btndetails = sender as ImageButton;
            //GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            //lblID.Text = gvData.DataKeys[gvrow.RowIndex].Value.ToString();



        }


        private void BindSegmentValues(GridViewRow gvr)
        {

            BindSegment(gvr);
            if (gvr.RowType != DataControlRowType.Footer)
            {
                int iLoop = gvr.RowIndex;
                string seg1 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_1].ToString();
                string seg2 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_2].ToString();
                string seg3 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_3].ToString();
                string seg4 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_4].ToString();
                string seg5 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_5].ToString();
                string seg6 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_6].ToString();

                ddlSegment1.SelectedValue = seg1;
                ddlSegment2.SelectedValue = seg2;
                ddlSegment3.SelectedValue = seg3;
                ddlSegment4.SelectedValue = seg4;
                ddlSegment5.SelectedValue = seg5;
                ddlSegment6.SelectedValue = seg6;
            }

        }


        private void BindSegment(GridViewRow gvr)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                DropDownList ddlAccountCodes = new DropDownList();
                ddlAccountCodes.ID = "ddlAccountCodes";


                ddlAccountCodes = (DropDownList)gvr.FindControl("ddlAccountCodes");

                if (ddlAccountCodes != null)
                {
                    if (ddlAccountCodes.SelectedValue != null)
                    {
                        dtdataAc = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentBasedAccCode(ddlAccountCodes.SelectedValue.ToString())).Tables[0];
                        if (dtdataAc != null)
                        {
                            if (dtdataAc.Rows.Count > 0)
                            {
                                int j = dtdataAc.Rows.Count;

                                for (int i = 0; i < dtdataAc.Rows.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        //segment1
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }

                                    }
                                    if (i == 1)
                                    {
                                        //segment2
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 2)
                                    {
                                        //Segments3
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 3)
                                    {
                                        //segment4
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 4)
                                    {
                                        //segment5
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 5)
                                    {
                                        //segment6
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                }
                                if (j == 1)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 2)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 3)
                                {
                                    // Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, VMVServices.Web.Utils.OrganizationID);

                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 4)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 5)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
                //throw ex;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<oM_CUST_INVOICE_HDR >(oM_CUST_INVOICE_HDR );
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        protected void ddlItemNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_ReceiptNo = gvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = gvr.FindControl("ddlItemNo") as DropDownList;
            TextBox txtItemName = gvr.FindControl("txtItemName") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;

            DataTable dtData = new DataTable();
            DataTable dtqtyunitprice = new DataTable();


            dtqtyunitprice = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.GetSaleOrderQty(ddl_ReceiptNo.SelectedValue, ddl_ItemNo.SelectedValue).ToString()).Tables[0];
            if (dtqtyunitprice.Rows.Count > 0)
            {
                txtQuantity.Text = dtqtyunitprice.Rows[0]["om_order_qty"].ToString();
                txtUnitPrice.Text = dtqtyunitprice.Rows[0]["om_order_price"].ToString();

                fn_UnitPricetxtChanged();
            }
            // dtData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLineDetails(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
            dtData = FIN.BLL.AP.PurchaseItemReceipt_BLL.fn_getGRNItemRecDet(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString());
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtItemName.Visible = true;
                    txtItemName.Text = dtData.Rows[0][FINColumnConstants.item_service].ToString();
                    txtQuantity.Text = dtData.Rows[0][FINColumnConstants.QTY_RECEIVED].ToString();
                    txtUnitPrice.Text = dtData.Rows[0][FINColumnConstants.PO_UNIT_PRICE].ToString();
                    txtAmount.Text = dtData.Rows[0][FINColumnConstants.PO_Amount].ToString();
                    Session["Item_Id"] = dtData.Rows[0][FINColumnConstants.ITEM_ID].ToString();
                }
            }
        }

        protected void ddlLineType_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            FillEntityNumber(gvr);
        }
        protected void ddlInvoiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceDetails(Master.StrRecordId)).Tables[0];
            BindGrid(dtGridData);
        }
        private void SetColVis4InvoiceType()
        {
            gvData.Columns[4].Visible = true;
            gvData.Columns[5].Visible = true;
            gvData.Columns[6].Visible = true;

            if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
            {
                gvData.Columns[4].Visible = false;
            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
            {

                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;
                gvData.Columns[6].Visible = false;

            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "CREDIT_MEMO")
            {

                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;
                gvData.Columns[6].Visible = false;
            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "DEBIT_MEMO")
            {

                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;
                gvData.Columns[6].Visible = false;
            }
        }
        private void FillEntityNumber(GridViewRow tmpgvr)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
                DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
                DropDownList ddl_ItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
                TextBox txtMisNo = tmpgvr.FindControl("txtMisNo") as TextBox;
                TextBox txt_Quantity = tmpgvr.FindControl("txtQuantity") as TextBox;


                gvData.Columns[4].Visible = true;
                gvData.Columns[5].Visible = true;
                gvData.Columns[6].Visible = true;

                txtMisNo.Visible = false;
                ddl_ReceiptNo.Visible = true;

                ddl_ItemNo.Visible = false;

                if (ddl_LineType.SelectedValue.ToString().ToUpper() == "RECEIPT")
                {
                    if (ddlInvoiceType.SelectedValue == "Standard")
                    {
                        FIN.BLL.AP.PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddl_ReceiptNo);
                        ddl_ItemNo.Visible = true;
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection",
                            "Invalid Line Type Selection");
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "ITEM")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Item");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Item");
                        gvData.Columns[4].Visible = false;
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection", Prop_File_Data["InvalidSelection_P"]);

                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "MISCELLANEOUS")
                {

                    txtMisNo.Visible = true;
                    ddl_ReceiptNo.Visible = false;
                    txt_Quantity.Text = "1";
                    gvData.Columns[4].Visible = false;

                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "SALES ORDER ITEM")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "SALES ORDER INVOICE")
                    {
                        if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                        {
                            FIN.BLL.AR.SalesOrder_BLL.fn_getOrderNo(ref ddl_ReceiptNo, ddlSupplierSite.SelectedValue);
                            ddl_ItemNo.Visible = true;
                        }
                        else
                        {
                            ddl_LineType.SelectedIndex = 0;
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Customer Site");

                        }
                    }
                }


                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "SERVICE")
                {
                    if (ddlInvoiceType.SelectedValue == "Standard")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Service");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Service");
                        gvData.Columns[4].Visible = false;
                    }
                    else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "SALES ORDER INVOICE")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Service");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Service");
                        gvData.Columns[4].Visible = false;

                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection", Prop_File_Data["InvalidSelection_P"]);
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "OTHER_CHARGES")
                {
                    if (ddlInvoiceType.SelectedValue == "Standard")
                    {
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ReceiptNo, "OC");
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ItemNo, "OC");
                        gvData.Columns[4].Visible = false;
                    }
                    else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "SALES ORDER INVOICE")
                    {
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ReceiptNo, "OC");
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ItemNo, "OC");
                        gvData.Columns[4].Visible = false;
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection", Prop_File_Data["InvalidSelection_P"]);
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "APPLY PREPAYMENT")
                {
                    if (ddlInvoiceType.SelectedValue == "Prepayment")
                    {
                        FIN.BLL.AP.Invoice_BLL.fn_getInvoice4Perpayment(ref ddl_ReceiptNo);
                        FIN.BLL.AP.Invoice_BLL.fn_getInvoice4Perpayment(ref ddl_ItemNo);
                        gvData.Columns[4].Visible = false;
                        gvData.Columns[5].Visible = false;
                        gvData.Columns[6].Visible = false;
                       // txt_Amount.Enabled = true;
                        txt_Quantity.Text = "1";
                       // txt_UnitPrice.Text = "1";
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection", Prop_File_Data["InvalidSelection_P"]);
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "INVOICE")
                {
                    if (ddlInvoiceType.SelectedValue == "PO Invoice" || ddlInvoiceType.SelectedValue == "Prepayment PO" || ddlInvoiceType.SelectedValue == "Credit Memo" || ddlInvoiceType.SelectedValue == "Debit Memo")
                    {
                        FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber(ref ddl_ReceiptNo);
                        FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber(ref ddl_ItemNo);
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidSelection", Prop_File_Data["InvalidSelection_P"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillEntity", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void ddlReceiptNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            FillItemNo(gvr);
        }

        private void FillItemNo(GridViewRow tmpgvr)
        {
            DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
            DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
            if (ddl_LineType.SelectedValue.ToString().ToUpper() == "RECEIPT")
            {
                FIN.BLL.AP.PurchaseItemReceipt_BLL.fn_GetPOItemLine(ref ddl_ItemNo, ddl_ReceiptNo.SelectedValue.ToString());
            }
            else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "SALES ORDER ITEM")
            {
                FIN.BLL.AP.Item_BLL.getItemNameDetails_frARDeleiverychallan(ref ddl_ItemNo, ddl_ReceiptNo.SelectedValue);
            }
            else
            {
                ddl_ItemNo.SelectedValue = ddl_ReceiptNo.SelectedValue;
            }
        }

        protected void btnTax_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_ReceiptNo = (DropDownList)gvr.FindControl("ddlReceiptNo");
            DropDownList ddl_ItemNo = (DropDownList)gvr.FindControl("ddlItemNo");

            if (ddl_ReceiptNo != null && ddl_ItemNo != null)
            {
                if (ddl_ReceiptNo.SelectedValue.ToString().Length > 0 && ddl_ItemNo.SelectedValue.ToString().Length > 0)
                {
                    hf_ItemNo.Value = ddl_ItemNo.SelectedValue.ToString();
                    hf_ReceitpNo.Value = ddl_ReceiptNo.SelectedValue.ToString();
                    DataTable dt_Tax = new DataTable();
                    DataTable dt_InvTax = new DataTable();
                    if (gvr.RowType == DataControlRowType.Footer)
                    {
                        dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails("0", ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                    }
                    else
                    {
                        DataRow[] dr = null;
                        if (Session["InvTaxDet"] != null)
                        {
                            dt_InvTax = (DataTable)Session["InvTaxDet"];
                            if (dt_InvTax != null)
                            {
                                if (dt_InvTax.Rows.Count > 0)
                                    dr = dt_InvTax.Select("EntityNo='" + ddl_ReceiptNo.SelectedValue.ToString() + "' and ItemNo='" + ddl_ItemNo.SelectedValue.ToString() + "'");
                            }
                        }
                        if (dr == null)
                        {
                            dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails(gvData.DataKeys[gvr.RowIndex].Values["INV_LINE_ID"].ToString(), ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                        }
                        else if (dr.Length == 0)
                        {
                            dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails(gvData.DataKeys[gvr.RowIndex].Values["INV_LINE_ID"].ToString(), ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                        }
                    }
                    if (Session["InvTaxDet"] == null)
                    {
                        Session["InvTaxDet"] = dt_Tax;
                        dt_InvTax = dt_Tax;
                    }
                    else
                    {
                        dt_InvTax = (DataTable)Session["InvTaxDet"];
                        if (dt_Tax.Rows.Count > 0)
                        {
                            dt_InvTax.Merge(dt_Tax);
                        }
                    }
                    gvTaxDet.DataSource = dt_InvTax;
                    gvTaxDet.DataBind();
                    mptTaxDet.Show();
                }
            }

        }

        protected void ddlSegment1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment2.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment3.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment4.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment5.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment5_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment6.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void btnTaxOk_Click(object sender, EventArgs e)
        {
            double dbl_TaxAmt = 0;
            DataTable dt_InvTax = (DataTable)Session["InvTaxDet"];
            for (int iLoop = 0; iLoop < gvTaxDet.Rows.Count; iLoop++)
            {
                TextBox txt_TaxAmount = (TextBox)gvTaxDet.Rows[iLoop].FindControl("txtTaxAmount");
                TextBox txt_TaxPercentage = (TextBox)gvTaxDet.Rows[iLoop].FindControl("txtTaxPercentage");
                DataRow[] dr = dt_InvTax.Select("EntityNo='" + hf_ReceitpNo.Value + "' and ItemNo='" + hf_ItemNo.Value + "' and tax_id='" + gvTaxDet.DataKeys[iLoop].Values["TAX_ID"].ToString() + "'");
                if (txt_TaxAmount.Text.ToString().Length > 0)
                {

                    dr[0]["INV_TAX_AMT"] = txt_TaxAmount.Text;
                    dbl_TaxAmt = dbl_TaxAmt + Convert.ToDouble(txt_TaxAmount.Text);
                    if (txt_TaxPercentage.Text.ToString().Length > 0)
                        dr[0]["INV_TAX_PER"] = txt_TaxPercentage.Text;
                }
                else
                {
                    dr[0]["INV_TAX_AMT"] = DBNull.Value;
                    dr[0]["INV_TAX_PER"] = DBNull.Value;
                }
            }
            Session["InvTaxDet"] = dt_InvTax;
            hf_TotTaxAmt.Value = dbl_TaxAmt.ToString();
            mptTaxDet.Hide();
        }

        protected void ddlInvoiceCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillExchangeRate();
        }
        private void FillExchangeRate()
        {
            txtExchangeRate.Text = "";

            if (ddlInvoiceCurrency.SelectedValue != string.Empty && ddlPaymentCurrency.SelectedValue != string.Empty)
            {
                if (ddlInvoiceCurrency.SelectedValue.ToString() == ddlPaymentCurrency.SelectedValue.ToString())
                {
                    txtExchangeRate.Text = "1";
                    txtExchangeRate.Enabled = false;
                }
                else
                {
                    txtExchangeRate.Text = string.Empty;
                    txtExchangeRate.Enabled = true;
                }
            }

            if (ddlInvoiceCurrency.SelectedValue.ToString() == Session[FINSessionConstants.ORGCurrency].ToString())
            {
                txtExchangeRate.Text = "1";
            }
            else if ((txtInvoiceDate.Text.ToString().Length > 0) && (ddlInvoiceCurrency.SelectedValue.ToString().Length > 0))
            {
                DataTable dt_exValue = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.GetExchangeRate(txtInvoiceDate.Text, ddlInvoiceCurrency.SelectedValue)).Tables[0];
                if (dt_exValue.Rows.Count > 0)
                {
                    txtExchangeRate.Text = dt_exValue.Rows[0]["CURRENCY_BUY_RATE"].ToString();
                }
            }

        }

        protected void txtInvoiceDate_TextChanged(object sender, EventArgs e)
        {
            FillExchangeRate();
        }

        protected void gvTaxDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView tmp_erv = (DataRowView)e.Row.DataItem;
                    if ((tmp_erv.Row["ItemNo"].ToString() != hf_ItemNo.Value.ToString()) && (tmp_erv.Row["EntityNo"].ToString() != hf_ReceitpNo.Value.ToString()))
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtInvoiceNumber_TextChanged(object sender, EventArgs e)
        {

        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtAmount.ID = "txtAmount";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.FooterRow.FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text.ToString()) > 0 && CommonUtils.ConvertStringToDecimal(txtQuantity.Text.ToString()) > 0)
                        {
                            txtAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();

                            txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                            txtUnitPrice.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtUnitPrice.Text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtRetAmount_TextChanged(object sender, EventArgs e)
        {

        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                //if (txtInvoiceDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Date", txtInvoiceDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}
                htFilterParameter.Add("INV_ID", Master.StrRecordId);

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

               
                ReportData = FIN.BLL.AR.Invoice_BLL.GetInvoiceListReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                if (VMVServices.Web.Utils.LanguageCode == string.Empty)
                {
                    Session["ProgramName"] = "Invoice";
                }
                else
                {
                    Session["ProgramName"] = "فاتورة";
                }
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgbtninvoicecancel_Click(object sender, ImageClickEventArgs e)
        {
            oM_CUST_INVOICE_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
            oM_CUST_INVOICE_HDR.POSTED_FLAG = FINAppConstants.CANCELED;
            oM_CUST_INVOICE_HDR.REV_FLAG = FINAppConstants.Y;
            oM_CUST_INVOICE_HDR.REV_DATE = DateTime.Today;
            DBMethod.SaveEntity<OM_CUST_INVOICE_HDR>(oM_CUST_INVOICE_HDR, true);
            ErrorCollection.Add("Invoice Canceled", "Invoice Cancelled Successfully");
            imgBtnPost.Visible = false;
            imgbtninvoicecancel.Visible = false;
             lblCancelled.Visible = true;
            btnSave.Visible = false;
            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        }

    }
}