﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.Client.AR
{
    public partial class ItemOtherCostEntry : PageBase
    {

      
        OM_DC_OTHER_COST_HDR oM_DC_OTHER_COST_HDR = new OM_DC_OTHER_COST_HDR();
        OM_DC_OTHER_COST_DTL oM_DC_OTHER_COST_DTL = new OM_DC_OTHER_COST_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {

            DeliveryChallan_BLL.GetDCNo(ref ddlDCNumber);


        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AR.ItemOtherCost_DAL.getItemothercostDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_DC_OTHER_COST_HDR = FIN.BLL.AR.ItemOtherCost_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = oM_DC_OTHER_COST_HDR;

                    
                    ddlDCNumber.SelectedValue = oM_DC_OTHER_COST_HDR.OM_DC_ID.ToString();
                    Filldcdate();
                    ddlLineNumber.SelectedValue = oM_DC_OTHER_COST_HDR.OM_DC_LINE_NUM.ToString();
                    Filldcdtls();


                    if (oM_DC_OTHER_COST_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        btnSave.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_OTHER_COST_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_OTHER_COST_AMT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

               
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOCEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCostType = tmpgvr.FindControl("ddlCostType") as DropDownList;


                Lookup_BLL.GetLookUpValues(ref ddlCostType, "COST_TYPE");


                if (gvData.EditIndex >= 0)
                {
                    ddlCostType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                                        
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtCost = gvr.FindControl("txtCost") as TextBox;
            DropDownList ddlCostType = gvr.FindControl("ddlCostType") as DropDownList;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["OM_OTHER_COST_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }



            slControls[0] = ddlCostType;
            slControls[1] = txtCost;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Cost_Type_P"] + " ~ " + Prop_File_Data["Cost_P"] + "";
            //string strMessage = "Cost Type ~ Cost";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "LOOKUP_ID='" + ddlCostType.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }




            drList["LOOKUP_ID"] = ddlCostType.SelectedValue.ToString();
            drList["LOOKUP_NAME"] = ddlCostType.SelectedItem.Text.ToString();

            drList["OM_OTHER_COST_AMT"] = txtCost.Text;



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOCEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_RD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_RE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_RC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_DB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_BS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_DC_OTHER_COST_HDR = (OM_DC_OTHER_COST_HDR)EntityData;
                }

                //PO_HEADERS.PO_HEADER_ID = txtPONumber.Text;


                //  oM_DC_OTHER_COST_HDR.QUOTE_DATE = DBMethod.ConvertStringToDate(txtQuoteDate.Text.ToString());
                oM_DC_OTHER_COST_HDR.OM_DC_ID = ddlDCNumber.SelectedValue;

                oM_DC_OTHER_COST_HDR.OM_DC_LINE_NUM = decimal.Parse(ddlLineNumber.SelectedValue.ToString());
                oM_DC_OTHER_COST_HDR.OM_DC_ORDER_ID = hforderNo.Value;
                oM_DC_OTHER_COST_HDR.OM_DC_ITEM_ID = hfitem.Value;
                oM_DC_OTHER_COST_HDR.OM_DC_ORDER_LINE_NUM = decimal.Parse(txtOrderLineNumber.Text.ToString());
                oM_DC_OTHER_COST_HDR.OM_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                               


                oM_DC_OTHER_COST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_DC_OTHER_COST_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_DC_OTHER_COST_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_DC_OTHER_COST_HDR.OM_OTHER_COST_ID = FINSP.GetSPFOR_SEQCode("AR_008_M".ToString(), false, true);

                    oM_DC_OTHER_COST_HDR.CREATED_BY = this.LoggedUserName;
                    oM_DC_OTHER_COST_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_DC_OTHER_COST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_DC_OTHER_COST_HDR.OM_OTHER_COST_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Item Other Cost");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                OM_DC_OTHER_COST_DTL oM_DC_OTHER_COST_DTL = new OM_DC_OTHER_COST_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    oM_DC_OTHER_COST_DTL = new OM_DC_OTHER_COST_DTL();

                    if (dtGridData.Rows[iLoop]["OM_OTHER_COST_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_OTHER_COST_DTL_ID"].ToString() != string.Empty)
                    {
                        oM_DC_OTHER_COST_DTL = FIN.BLL.AR.ItemOtherCost_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["OM_OTHER_COST_DTL_ID"].ToString());
                    }

                    oM_DC_OTHER_COST_DTL.OM_OTHER_COST_TYPE = (dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString());

                    oM_DC_OTHER_COST_DTL.OM_OTHER_COST_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["OM_OTHER_COST_AMT"].ToString());

                    oM_DC_OTHER_COST_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    oM_DC_OTHER_COST_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    oM_DC_OTHER_COST_DTL.OM_OTHER_COST_ID = oM_DC_OTHER_COST_HDR.OM_OTHER_COST_ID;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(oM_DC_OTHER_COST_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["OM_OTHER_COST_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["OM_OTHER_COST_DTL_ID"].ToString() != string.Empty)
                        {
                            oM_DC_OTHER_COST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_DC_OTHER_COST_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            oM_DC_OTHER_COST_DTL.OM_OTHER_COST_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_008_D".ToString(), false, true);
                            oM_DC_OTHER_COST_DTL.CREATED_BY = this.LoggedUserName;
                            oM_DC_OTHER_COST_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_DC_OTHER_COST_DTL, FINAppConstants.Add));
                        }
                    }
                }
                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(oM_DC_OTHER_COST_HDR.OM_OTHER_COST_ID,txtDCDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_DC_OTHER_COST_HDR, OM_DC_OTHER_COST_DTL>(oM_DC_OTHER_COST_HDR, tmpChildEntity, oM_DC_OTHER_COST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<OM_DC_OTHER_COST_HDR, OM_DC_OTHER_COST_DTL>(oM_DC_OTHER_COST_HDR, tmpChildEntity, oM_DC_OTHER_COST_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (oM_DC_OTHER_COST_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(oM_DC_OTHER_COST_HDR.OM_OTHER_COST_ID, "AR_008");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IOC_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BY_IOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }





        protected void ddlLotNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlLotNumber = gvr.FindControl("ddlLotNumber") as DropDownList;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            DataTable dtQTY = new DataTable();
            dtQTY = DBMethod.ExecuteQuery(PickRelease_DAL.getLotqty(ddlLotNumber.SelectedValue)).Tables[0];
            txtQuantity.Text = dtQTY.Rows[0]["LOT_QTY"].ToString();

        }

      
        private void Filldcdate()
        {
            DeliveryChallan_BLL.fn_GetDCLineNo(ref ddlLineNumber, ddlDCNumber.SelectedValue);

            DataTable dtdate = new DataTable();
            dtdate = DBMethod.ExecuteQuery(PickRelease_DAL.getDcdate(ddlDCNumber.SelectedValue)).Tables[0];
            txtDCDate.Text = DBMethod.ConvertDateToString(dtdate.Rows[0]["OM_DC_DATE"].ToString());
        }

        #endregion

       

        private void Filldcdtls()
        {

            DataTable dtdcdtl = new DataTable();
            dtdcdtl = DBMethod.ExecuteQuery(PickRelease_DAL.getDcdtl(int.Parse(ddlLineNumber.SelectedValue.ToString()),ddlDCNumber.SelectedValue)).Tables[0];
            txtOrderNumber.Text = dtdcdtl.Rows[0]["OM_ORDER_NUM"].ToString();
            txtItemDescription.Text = dtdcdtl.Rows[0]["ITEM_NAME"].ToString();
            txtOrderLineNumber.Text = dtdcdtl.Rows[0]["OM_ORDER_LINE_NUM"].ToString();
            hforderNo.Value = dtdcdtl.Rows[0]["OM_ORDER_ID"].ToString();
            hfitem.Value = dtdcdtl.Rows[0]["ITEM_ID"].ToString();
        }

        protected void ddlDCNumber_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Filldcdate();
        }

        protected void ddlLineNumber_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Filldcdtls();
        }


    }
}