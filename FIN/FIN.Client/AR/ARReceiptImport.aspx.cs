﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace FIN.Client.AR
{
    public partial class ARReceiptImport : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
        string connectionStringOracle = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringOracle"].ToString();
        bool postedMsg = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CollectionHeaderATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["amount"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("amount"))));
                    }

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindFilteredGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;


                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    }
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtVendorNum = gvr.FindControl("txtVendorNum") as TextBox;
                TextBox txtVendorName = gvr.FindControl("txtVendorName") as TextBox;
                TextBox txtRecNumber = gvr.FindControl("txtRecNumber") as TextBox;
                TextBox txtInvoiceNumber = gvr.FindControl("txtInvoiceNumber") as TextBox;
           
                TextBox txtAMOUNT = gvr.FindControl("txtAMOUNT") as TextBox;

                if (txtVendorNum.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtRecNumber.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                  
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtVendorName.Text.Length > 0)
                {
                    txtVendorNum.Text = string.Empty;
                    txtRecNumber.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtRecNumber.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtInvoiceNumber.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtRecNumber.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
             
                    txtAMOUNT.Text = string.Empty;
                }
                
                else if (txtAMOUNT.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtRecNumber.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;                
                }
                DataTable dt = new DataTable();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt = (DataTable)Session[FINSessionConstants.GridData];
                    if (dt.Rows.Count > 0)
                    {
                        var var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString() != string.Empty);

                        if (txtVendorNum.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["vendornumber"].ToString().ToUpper().StartsWith(txtVendorNum.Text.ToUpper()));
                        }
                        else if (txtVendorName.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["name_en"].ToString().ToUpper().StartsWith(txtVendorName.Text.ToUpper()));
                        }
                        else if (txtRecNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["ReceiptNumber"].ToString().ToUpper().StartsWith(txtRecNumber.Text.ToUpper()));
                        }
                        else if (txtInvoiceNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString().ToUpper().StartsWith(txtInvoiceNumber.Text.ToUpper()));
                        }
                      
                        else if (txtAMOUNT.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["AMOUNT"].ToString().StartsWith(txtAMOUNT.Text));
                        }
                        if (var_List.Any())
                        {
                            dt = System.Data.DataTableExtensions.CopyToDataTable(var_List);
                            BindFilteredGrid(dt);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);
            }
        }
        #endregion


        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CollectionHeaderPOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion



        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        FINSP.GetSP_GL_Posting(gvData.DataKeys[iLoop].Values["CollectionId"].ToString(), "SSM_032");
                        postedMsg = true;
                    }
                }

                if (postedMsg)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

                DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesRegister_DAL.getCollectionHdrDtlData()).Tables[0];
                BindGrid(dtData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CollectionHdrPOST", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string maxIdCH = "";
            string maxChildId = "";
            try
            {
                //AccountReceivable
                //Get the MAX Id
                using (OracleConnection connMax = new OracleConnection())
                {
                    connMax.ConnectionString = connectionStringOracle;
                    connMax.Open();
                    OracleCommand cmdMax = connMax.CreateCommand();
                    string sql = "";

                    sql = "Select max(ch.CollectionId) maxValueCH from CollectionHdr ch where ch.PaymentDate <=to_date('" + txtFromDate.Text + "','dd/MM/yyyy')";
                    DataTable dtDataMax = new DataTable();
                    OracleDataAdapter oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[0]["maxValueCH"].ToString().Length > 0)
                        {
                            maxIdCH = dtDataMax.Rows[0]["maxValueCH"].ToString();
                        }
                    }


                    sql = "Select max(ch.CollectionDetailId) maxValueCH from collectiondetail ch,collectionhdr c where c.collectionid=ch.collectionid and c.PaymentDate <=to_date('" + txtFromDate.Text + "','dd/MM/yyyy')";
                  
                    oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[1]["maxValueCH"].ToString().Length > 0)
                        {
                            maxChildId = dtDataMax.Rows[1]["maxValueCH"].ToString();
                        }
                    }

                    connMax.Close();
                }

                //Create Connection - AccountReceivable  -- SQL
                SqlConnection con = new SqlConnection(connectionStringSQL);

                string sqlQuery = " select CollectionId, CollectionNumber, Amount, convert(nvarchar,IssueDate,103) as IssueDate, CustomerId, CurrencyId, Remarks_En, Remarks_Ar,";
                sqlQuery += " Status, convert(nvarchar,StatusDate,103) as StatusDate, PaymentMethod, PaymentNumber, BankId, BankAccountId,"; 
                sqlQuery += " convert(nvarchar,PaymentDate,103) as PaymentDate, CreditCardNumber, convert(nvarchar,ExpiryDate,103) as ExpiryDate, OverCollectionARId, CUserId,";
                sqlQuery += " MDate, convert(nvarchar,CDate,103) as CDate, DocumentId, OverCollectionDetailId, PostBatchNumber, UnPostBatchNumber, CostCenterId,"; 
                sqlQuery += " SettlementOverCollectionAPId, SettlementOverCollectionARId, CheckId, convert(nvarchar,UnpostDate,103) as UnpostDate, TreasuryId, RentNoticeId, CustomerInvoiceId";
                sqlQuery += " from Collection where status=4 and paymentdate>=convert(datetime,'01/01/2015',103)";

                if (maxIdCH.ToString().Length > 0)
                {
                    sqlQuery += " and CollectionId > '" + maxIdCH + "'";
                }
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                DataTable dtData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dtData);

                //Create Connection -- Oracle
                //Account Receivables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into CollectionHdr (CollectionId,CollectionNumber,Amount, IssueDate, CustomerId, CurrencyId, Remarks_En,Remarks_Ar, Status, StatusDate, PaymentMethod, PaymentNumber, BankId, ";
                            sql += " BankAccountId, PaymentDate, CreditCardNumber, ExpiryDate, OverCollectionARId, CUserId, MDate, CDate, DocumentId, OverCollectionDetailId, PostBatchNumber, UnPostBatchNumber,";
                            sql += " CostCenterId, SettlementOverCollectionAPId,SettlementOverCollectionARId,CheckId,UnpostDate,TreasuryId,RentNoticeId,CustomerInvoiceId) Values ('";
                            sql += dtData.Rows[iLoop]["CollectionId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CollectionNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Amount"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["IssueDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["CustomerId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CurrencyId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Remarks_En"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Remarks_Ar"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["Status"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["StatusDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["PaymentMethod"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PaymentNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["BankId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["BankAccountId"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["PaymentDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["CreditCardNumber"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["ExpiryDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["OverCollectionARId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CUserId"].ToString() + "',";
                            sql += "NULL,";
                            sql += "to_date('" + dtData.Rows[iLoop]["CDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["DocumentId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["OverCollectionDetailId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PostBatchNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["UnPostBatchNumber"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CostCenterId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["SettlementOverCollectionAPId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["SettlementOverCollectionARId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CheckId"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["UnpostDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["TreasuryId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["RentNoticeId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CustomerInvoiceId"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }




                sqlQuery = " select cd.CollectionDetailId,  cd.CollectionId,  cd.AccountReceivableId ,  cd.Amount ,cd.MDate ,   cd.Received,cd.InProcess  , cd.Receivable ,cd.Discount,cd.Status     ";
                sqlQuery += " from collectiondetail cd,collection c  where c.collectionid=cd.collectionid and c.status=4 and c.paymentdate>=convert(datetime,'01/01/2015',103) ";

                if (maxIdCH.ToString().Length > 0)
                {
                    sqlQuery += " and CollectionDetailId > '" + maxChildId + "'";
                }
                cmd = new SqlCommand(sqlQuery, con);
                da = new SqlDataAdapter(cmd);
                dtData = new DataTable();
                da.Fill(dtData);

                //Create Connection -- Oracle
                //Account Receivables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into collectionDetail (CollectionDetailId,CollectionId,AccountReceivableId,Amount, MDate, Received, InProcess, Receivable,Discount, Status)";                            
                            sql += "  Values (";
                            sql += "'" + dtData.Rows[iLoop]["CollectionDetailId"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["CollectionId"].ToString() + "',";
                            sql +="'"+ dtData.Rows[iLoop]["AccountReceivableId"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["Amount"].ToString() + "',";
                            sql += "NULL" + ",";
                            sql += "'" + dtData.Rows[iLoop]["Received"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["InProcess"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["Receivable"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["Discount"].ToString() + "',";
                            sql += "'" + dtData.Rows[iLoop]["Status"].ToString() + "'";                          
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                DataTable dtDataView = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesRegister_DAL.getCollectionHdrDtlData()).Tables[0];
                BindGrid(dtDataView);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATAIMPORT);

                //Close Connection  
                con.Close();
                da.Dispose();
                //close

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CollectionHdrImport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gvData.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gvData.Rows[rloop].FindControl("chkSelect");
                chk_tmp.Checked = chk_SAA.Checked;
            }
        }

    }
}