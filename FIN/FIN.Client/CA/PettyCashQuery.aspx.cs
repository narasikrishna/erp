﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class PettyCashQuery : PageBase
    {
      
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PettyCashQ", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountCodes_BLL.fn_getUserdtls(ref ddlUserCodes);
        }
        private void Startup()
        {

            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PettyCashQuery", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        #endregion

        protected void ddlUserCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtUserDtls = new DataTable();

            dtUserDtls = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getUserdtls()).Tables[0];
            if (dtUserDtls.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop <= dtUserDtls.Rows.Count; iLoop++)
                {
                    if (dtUserDtls.Rows[iLoop]["USER_CODE"].ToString() == ddlUserCodes.SelectedItem.Text.ToString())
                    {
                        txtUserName.Text = dtUserDtls.Rows[iLoop]["USER_NAMES"].ToString();
                        break;
                    }
                }
            }
        }
        
        protected void btnQuery_Click(object sender, EventArgs e)
        {
            DataTable dtGridData = new DataTable();
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetPettyCashDtls(txtFromDate.Text.ToString(), txtEndDate.Text.ToString())).Tables[0];

            if (dtGridData.Rows.Count > 0)
            {
                gvData.DataSource = dtGridData;
                gvData.DataBind();
            }
        }
    }
}