﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PettyCashExpenditureEntry.aspx.cs" Inherits="FIN.Client.CA.PettyCashExpenditureEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 940px" id="divMainContainer">
        <table width="100%">
            <tr>
                <td>
                    <div class="divRowContainer" id="div_ddl_CC" runat="server" visible="false">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="lblGlobalSegment">
                            Global Segment
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 517px">
                            <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                TabIndex="14">
                                <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="lblUser">
                            Employee
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 517px">
                            <asp:DropDownList ID="ddlUser" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                                runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace  LNOrient" >
                            &nbsp</div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="lblExpenditureDate">
                            Expenditure Date
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtExpenditureDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                                runat="server" TabIndex="2"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtExpenditureDate"
                                OnClientDateSelectionChanged="checkDate">
                            </cc2:CalendarExtender>
                            <%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />--%>
                        </div>
                        <div class="colspace  LNOrient" >
                            &nbsp</div>
                        <div class="lblBox  LNOrient" style=" width: 200px" id="lblBalanceAmount">
                            Allocated Amount
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 147px">
                            <asp:TextBox ID="txtBalAmount" CssClass="EntryFont txtBox_N" MaxLength="20" runat="server"
                                Enabled="false" TabIndex="9"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtBalAmount" />
                            <%-- <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,custom"
                ValidChars=".," TargetControlID="ddlBalanceAmount" />--%>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div id="Div1" class="divRowContainer" runat="server">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                            Balance Amount
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtTotAmt" CssClass="EntryFont txtBox_N" MaxLength="20" runat="server"
                                Enabled="false" TabIndex="8"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtTotAmt" />
                        </div>
                        <div class="colspace  LNOrient" >
                            &nbsp;</div>
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                            &nbsp;
                        </div>
                    </div>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Visible="false"
                                    OnClick="imgBtnPost_Click" Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Visible="false" Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPrint" runat="server" ImageUrl="~/Images/Print.png" Visible="false"
                                    OnClick="imgBtnPrint_Click" Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient " align="left" >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="ACCT_CODE_ID,ACCT_CODE,GL_EXP_AMOUNT,GL_PCE_REMARKS,GL_PCE_POSTED,DELETED,GL_PCE_DTL_ID,JE_SEGMENT_ID_1,JE_SEGMENT_ID_2,JE_SEGMENT_ID_3,JE_SEGMENT_ID_4,JE_SEGMENT_ID_5,JE_SEGMENT_ID_6"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="15" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Expenditure Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtExpenditureAmount" Width="180px" MaxLength="250" runat="server"
                                CssClass="RequiredField   txtBox_N" Text='<%# Eval("GL_EXP_AMOUNT") %>' TabIndex="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtExpenditureAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtExpenditureAmount" Width="180px" MaxLength="250" runat="server"
                                CssClass="RequiredField   txtBox_N" TabIndex="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtExpenditureAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName2" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("GL_EXP_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" Width="180px" MaxLength="250" runat="server" CssClass="RequiredField   txtBox"
                                Text='<%# Eval("GL_PCE_REMARKS") %>' TabIndex="6"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" Width="180px" MaxLength="250" runat="server" CssClass="RequiredField   txtBox"
                                TabIndex="6"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName1" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("GL_PCE_REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Posted" Visible="false">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPosted" runat="server" TabIndex="7" Width="30px" Checked='<%# Convert.ToBoolean(Eval("GL_PCE_POSTED")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkPosted" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPosted" runat="server" Width="30px" TabIndex="7" Checked='<%# Convert.ToBoolean(Eval("GL_PCE_POSTED")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" Width="30px" TabIndex="8" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" Width="30px" TabIndex="8" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Detail">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountDetail" TabIndex="4" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="180px" AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountDetail" TabIndex="4" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="180px" AutoPostBack="true" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ACCT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 1">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment1" Width="180px" runat="server" Text='<%# Eval("SEGMENT_1_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 2">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment2" Width="180px" runat="server" Text='<%# Eval("SEGMENT_2_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 3">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment3" Width="180px" runat="server" Text='<%# Eval("SEGMENT_3_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 4">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment4" Width="180px" runat="server" Text='<%# Eval("SEGMENT_4_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 5">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment5" Width="180px" runat="server" Text='<%# Eval("SEGMENT_5_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 6">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment6" Width="180px" runat="server" Text='<%# Eval("SEGMENT_6_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnPosting" runat="server" CssClass="btn" Text="Post" CommandName="btnPop"
                                TabIndex="8" OnClick="btnPosting_Click" Enabled='<%# Convert.ToBoolean(Eval("POSTED_FLAG")) %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnPosting" runat="server" CssClass="btn" Text="Post" CommandName="btnPop"
                                TabIndex="8" OnClick="btnPosting_Click" Enabled='<%# Convert.ToBoolean(Eval("POSTED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnPosting" runat="server" CssClass="btn" Text="Post" CommandName="btnPop"
                                TabIndex="8" Enabled="false" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer">
            <asp:HiddenField ID="hf_Posted" runat="server" />
            <asp:HiddenField ID="hf_sameUser" runat="server" />
            <asp:HiddenField ID="hf_GL_PCA_DTL_ID" runat="server" />
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
