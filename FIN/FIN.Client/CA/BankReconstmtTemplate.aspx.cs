﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class BankReconstmtTemplate : PageBase
    {

        GL_BANK_RECON_STMT_TEMPL gL_BANK_RECON_STMT_TEMPL = new GL_BANK_RECON_STMT_TEMPL();
        string ProReturn = null;
        bool bol_dataSave = false;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    DataTable dt_tmp_det = DBMethod.ExecuteQuery(FIN.DAL.CA.BankStatementTemplate_DAL.getBankTemplateDetails4Name(Master.StrRecordId)).Tables[0];
                    txtTemplateName.Text = dt_tmp_det.Rows[0]["BR_TMPL_NAME"].ToString();
                    txtAcctNoRow.Text = dt_tmp_det.Rows[0]["BR_ROW_NUMBER"].ToString(); txtAcctNoCol.Text = dt_tmp_det.Rows[0]["BR_COL_NUMBER"].ToString();
                    txtStmtNumRow.Text = dt_tmp_det.Rows[1]["BR_ROW_NUMBER"].ToString(); txtStmtNumCol.Text = dt_tmp_det.Rows[1]["BR_COL_NUMBER"].ToString();
                    txtStmtDateRow.Text = dt_tmp_det.Rows[2]["BR_ROW_NUMBER"].ToString(); txtStmtDateCol.Text = dt_tmp_det.Rows[2]["BR_COL_NUMBER"].ToString();
                    txtTotalAmountRow.Text = dt_tmp_det.Rows[3]["BR_ROW_NUMBER"].ToString(); txtTotalAmountCol.Text = dt_tmp_det.Rows[3]["BR_COL_NUMBER"].ToString();
                    txtSerialNoCol.Text = dt_tmp_det.Rows[4]["BR_COL_NUMBER"].ToString();
                    txtTransDateCol.Text = dt_tmp_det.Rows[5]["BR_COL_NUMBER"].ToString();
                    txtRemarksCol.Text = dt_tmp_det.Rows[6]["BR_COL_NUMBER"].ToString();
                    txtChequNoCol.Text = dt_tmp_det.Rows[7]["BR_COL_NUMBER"].ToString();
                    txtCrDrCol.Text = dt_tmp_det.Rows[8]["BR_COL_NUMBER"].ToString();
                    txtAmountCol.Text = dt_tmp_det.Rows[9]["BR_COL_NUMBER"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            // ComboFilling.fn_getCategoryParent(ref ddlCategoryParent);
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                SaveData("ACCOUNTNUMBER", txtAcctNoRow.Text, txtAcctNoCol.Text);
                SaveData("STATEMENTNUMBER", txtStmtNumRow.Text, txtStmtNumCol.Text);
                SaveData("STATEMENTDATE", txtStmtDateRow.Text, txtStmtDateCol.Text);
                SaveData("TOTALAMOUNT", txtTotalAmountRow.Text, txtTotalAmountCol.Text);
                SaveData("SLNO", "", txtSerialNoCol.Text);
                SaveData("TRANSACTIONDATE", "", txtTransDateCol.Text);
                SaveData("REMARKS", "", txtRemarksCol.Text);
                SaveData("CHEQUENUMBER", "", txtChequNoCol.Text);
                SaveData("CRDR", "", txtCrDrCol.Text);
                SaveData("AMOUNT", "", txtAmountCol.Text);
                bol_dataSave = true;
               
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void SaveData(string str_Colname, string str_rowValue, string str_colValue)
        {

            gL_BANK_RECON_STMT_TEMPL = new GL_BANK_RECON_STMT_TEMPL();
            bool bol_Save = true;
            using (IRepository<GL_BANK_RECON_STMT_TEMPL> userCtx = new DataRepository<GL_BANK_RECON_STMT_TEMPL>())
            {
                gL_BANK_RECON_STMT_TEMPL = userCtx.Find(r =>
                    (r.BR_TMPL_NAME == txtTemplateName.Text && r.BR_COL_NAME == str_Colname)
                    ).SingleOrDefault();
            }

            if (gL_BANK_RECON_STMT_TEMPL == null)
            {
                gL_BANK_RECON_STMT_TEMPL = new GL_BANK_RECON_STMT_TEMPL();
            }
            else
            {
                bol_Save = false;
            }
            gL_BANK_RECON_STMT_TEMPL.BR_TMPL_NAME = txtTemplateName.Text;
            gL_BANK_RECON_STMT_TEMPL.BR_COL_NAME = str_Colname;// "";
            if (str_rowValue.Length > 0)
            {
                gL_BANK_RECON_STMT_TEMPL.BR_ROW_NUMBER = decimal.Parse(str_rowValue);
            }
            gL_BANK_RECON_STMT_TEMPL.BR_COL_NUMBER = str_colValue;
            gL_BANK_RECON_STMT_TEMPL.ENABLED_FLAG = FINAppConstants.Y;
            gL_BANK_RECON_STMT_TEMPL.BR_ORG_ID = VMVServices.Web.Utils.OrganizationID;

            if (bol_Save)
            {
                gL_BANK_RECON_STMT_TEMPL.BR_TMPL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0004.ToString(), false, true);
                gL_BANK_RECON_STMT_TEMPL.CREATED_BY = this.LoggedUserName;
                gL_BANK_RECON_STMT_TEMPL.CREATED_DATE = DateTime.Today;
                gL_BANK_RECON_STMT_TEMPL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                DBMethod.SaveEntity<GL_BANK_RECON_STMT_TEMPL>(gL_BANK_RECON_STMT_TEMPL);
            }
            else
            {
                gL_BANK_RECON_STMT_TEMPL.MODIFIED_BY = this.LoggedUserName;
                gL_BANK_RECON_STMT_TEMPL.MODIFIED_DATE = DateTime.Today;
                DBMethod.SaveEntity<GL_BANK_RECON_STMT_TEMPL>(gL_BANK_RECON_STMT_TEMPL, true);
            }
            VMVServices.Web.Utils.SavedRecordId = gL_BANK_RECON_STMT_TEMPL.BR_TMPL_ID.ToString();
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (bol_dataSave)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<GL_BANK_RECON_STMT_TEMPL>(gL_BANK_RECON_STMT_TEMPL);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }






    }
}