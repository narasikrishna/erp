﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.DAL.CA;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.CA
{
    public partial class PettyCashAllocationEntry : PageBase
    {
        GL_PETTY_CASH_ALLOCATE_HDR gL_PETTY_CASH_ALLOCATE_HDR = new GL_PETTY_CASH_ALLOCATE_HDR();
        GL_PETTY_CASH_ALLOCATE_DTL gL_PETTY_CASH_ALLOCATE_DTL = new GL_PETTY_CASH_ALLOCATE_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                //  Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.CA.Pettycashallocation_DAL.GetPettycashallocationDetails(Master.StrRecordId)).Tables[0];
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.CA.Cheque_DAL.GetChequeCreationDetails()).Tables[0];

                btnChequePrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnPrintReport.Visible = true;

                    // gL_PETTY_CASH_ALLOCATE_HDR = FIN.BLL.CA.PettyCashAllocation_BLL.getClassEntity(Master.StrRecordId);
                    using (IRepository<GL_PETTY_CASH_ALLOCATE_HDR> userCtx = new DataRepository<GL_PETTY_CASH_ALLOCATE_HDR>())
                    {
                        gL_PETTY_CASH_ALLOCATE_HDR = userCtx.Find(r =>
                            (r.GL_PCA_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    EntityData = gL_PETTY_CASH_ALLOCATE_HDR;

                    ddlBankName.SelectedValue = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BANK_ID.ToString();
                    FillBankDetails();

                    ddlBranchName.SelectedValue = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BRANCH_ID.ToString();
                    FillBranchDetails();
                    ddlAccountNumber.SelectedValue = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_ACCT_NUMBER.ToString();
                    fillchkhdr();
                    ddlChequeDtl.SelectedValue = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_CHECK_DTL_ID.ToString();

                    txtWithdrawlTyp.Text = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_WITHDRAWL_TYPE;
                    txtBalAmount.Text = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BALANCE_AMOUNT.ToString();
                    txtWithdrawalAmount.Text = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_AMOUNT.ToString();

                    txtBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalAmount.Text);
                    txtWithdrawalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtWithdrawalAmount.Text);

                    if (gL_PETTY_CASH_ALLOCATE_HDR.GL_WITHDRAW_DATE != null)
                        txtWithdrawalDate.Text = DBMethod.ConvertDateToString(gL_PETTY_CASH_ALLOCATE_HDR.GL_WITHDRAW_DATE.ToString());
                    //ddlChequeHeader.SelectedValue = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_CHECK_HDR_ID.ToString();
                    //fn_fill_cheque_dtl();

                    //nCreateCheques.Enabled = false;
                    btnChequePrint.Visible = true;

                    if (gL_PETTY_CASH_ALLOCATE_HDR.WORKFLOW_COMPLETION_STATUS == FINAppConstants.Y)
                    {
                        imgBtnPost.Visible = true;
                    }

                    if (gL_PETTY_CASH_ALLOCATE_HDR.POSTED_FLAG == FINAppConstants.Y)
                    {
                        btnSave.Enabled = false;
                        imgBtnPost.Visible = false;                        
                        imgBtnJVPrint.Visible = true;
                    }
                }

                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {

            if (EntityData != null)
            {
                gL_PETTY_CASH_ALLOCATE_HDR = (GL_PETTY_CASH_ALLOCATE_HDR)EntityData;
            }

            if (gL_PETTY_CASH_ALLOCATE_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PETTY_CASH_ALLOCATE_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" +  VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgBtnPost.Visible = false;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

               

                AssignToBE();
                if (savedBool)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {

                        if (EntityData != null)
                        {

                            gL_PETTY_CASH_ALLOCATE_HDR = (GL_PETTY_CASH_ALLOCATE_HDR)EntityData;

                            if (gL_PETTY_CASH_ALLOCATE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                FINSP.GetSP_GL_Posting(Master.StrRecordId, "CA_009");

                                DBMethod.ExecuteNonQuery("UPDATE GL_PETTY_CASH_ALLOCATE_HDR set  POSTED_FLAG='" + FINAppConstants.Y + "',POSTED_DATE=TO_DATE('" + DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString()) + "','dd/MM/yyyy') where GL_PCA_HDR_ID='" + Master.StrRecordId + "'");
                                imgBtnJVPrint.Visible = true;

                                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                            }
                            else
                            {
                                imgBtnPost.Visible = true;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_PETTY_CASH_ALLOCATE_HDR = (GL_PETTY_CASH_ALLOCATE_HDR)EntityData;
                }

                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BANK_ID = ddlBankName.SelectedValue.ToString();
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BRANCH_ID = ddlBranchName.SelectedValue.ToString();
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_ACCT_NUMBER = ddlAccountNumber.SelectedValue.ToString();
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_WITHDRAWL_TYPE = txtWithdrawlTyp.Text;
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_CHECK_HDR_ID = ddlChequeHeader.SelectedValue.ToString();
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_CHECK_DTL_ID = ddlChequeDtl.SelectedValue.ToString();
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_AMOUNT = CommonUtils.ConvertStringToDecimal(txtWithdrawalAmount.Text.ToString());
                gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_BALANCE_AMOUNT = CommonUtils.ConvertStringToDecimal(txtBalAmount.Text.ToString());
                gL_PETTY_CASH_ALLOCATE_HDR.GL_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (txtWithdrawalDate.Text.ToString().Trim().Length > 0)
                {
                    gL_PETTY_CASH_ALLOCATE_HDR.GL_WITHDRAW_DATE = DBMethod.ConvertStringToDate(txtWithdrawalDate.Text);
                }
                else
                {
                    gL_PETTY_CASH_ALLOCATE_HDR.GL_WITHDRAW_DATE = null;
                }
                gL_PETTY_CASH_ALLOCATE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_PETTY_CASH_ALLOCATE_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_PETTY_CASH_ALLOCATE_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_009_M.ToString(), false, true);
                    gL_PETTY_CASH_ALLOCATE_HDR.CREATED_BY = this.LoggedUserName;
                    gL_PETTY_CASH_ALLOCATE_HDR.CREATED_DATE = DateTime.Today;

                }
                gL_PETTY_CASH_ALLOCATE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_HDR_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Petty Cash Allocation");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_PETTY_CASH_ALLOCATE_DTL = new GL_PETTY_CASH_ALLOCATE_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop]["GL_PCA_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<GL_PETTY_CASH_ALLOCATE_DTL> userCtx = new DataRepository<GL_PETTY_CASH_ALLOCATE_DTL>())
                        {
                            gL_PETTY_CASH_ALLOCATE_DTL = userCtx.Find(r =>
                                (r.GL_PCA_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString() != "0")
                    //{
                    //    Cheque_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString());
                    //}
                    //gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString());


                    if (dtGridData.Rows[iLoop][FINColumnConstants.USER_CODE] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_USER_ID = dtGridData.Rows[iLoop][FINColumnConstants.USER_CODE].ToString();
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DATE] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_DATE = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DATE].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_AMOUNT] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_AMOUNT].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_BALANCE_AMOUNT] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_BALANCE_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_BALANCE_AMOUNT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.previous_bal_amount] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.PREVIOUS_BAL_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.previous_bal_amount].ToString());
                    }
                    
                    if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_REMARKS] != DBNull.Value)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_REMARKS = dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_REMARKS].ToString();
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    }
                    else
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                    }



                    // cA_CHECK_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    gL_PETTY_CASH_ALLOCATE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_HDR_ID = gL_PETTY_CASH_ALLOCATE_HDR.GL_PCA_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(gL_PETTY_CASH_ALLOCATE_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString() != string.Empty)
                        {
                            gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.GL_PCA_DTL_ID].ToString();
                            gL_PETTY_CASH_ALLOCATE_DTL.MODIFIED_DATE = DateTime.Today;
                            gL_PETTY_CASH_ALLOCATE_DTL.MODIFIED_BY = this.LoggedUserName;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_PETTY_CASH_ALLOCATE_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            gL_PETTY_CASH_ALLOCATE_DTL.GL_PCA_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_009_D.ToString(), false, true);
                            gL_PETTY_CASH_ALLOCATE_DTL.CREATED_BY = this.LoggedUserName;
                            gL_PETTY_CASH_ALLOCATE_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_PETTY_CASH_ALLOCATE_DTL, FINAppConstants.Add));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_PETTY_CASH_ALLOCATE_HDR, GL_PETTY_CASH_ALLOCATE_DTL>(gL_PETTY_CASH_ALLOCATE_HDR, tmpChildEntity, gL_PETTY_CASH_ALLOCATE_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<GL_PETTY_CASH_ALLOCATE_HDR, GL_PETTY_CASH_ALLOCATE_DTL>(gL_PETTY_CASH_ALLOCATE_HDR, tmpChildEntity, gL_PETTY_CASH_ALLOCATE_DTL, true);
                            savedBool = true;
                            break;
                        }
                }


                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChequeDtl.SelectedValue.ToString())
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {

                    cA_CHECK_DTL.CHECK_PAY_TO = VMVServices.Web.Utils.OrganizationName;

                    if (txtWithdrawalDate.Text.ToString().Length > 0)
                    {
                        cA_CHECK_DTL.CHECK_DT = DBMethod.ConvertStringToDate(txtWithdrawalDate.Text);
                    }
                    else
                    {
                        cA_CHECK_DTL.CHECK_DT = DateTime.Now;
                    }
                    cA_CHECK_DTL.CHECK_AMT = decimal.Parse(txtWithdrawalAmount.Text);
                    cA_CHECK_DTL.CHECK_STATUS = "USED";
                    cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                    cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                    cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            //PettyCashAllocation_BLL.fn_getChequeNum(ref ddlChequeHeader);

        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBankDetails();


                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }
        private void FillBankDetails()
        {

            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);



        }

        protected void ddlBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();
                    FillBranchDetails();

                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }

        private void FillBranchDetails()
        {
            Cheque_BLL.fn_getAccountNumber(ref ddlAccountNumber, ddlBankName.SelectedValue, ddlBranchName.SelectedValue);
        }

        private void FillChequeDetails()
        {

            Cheque_BLL.fn_getAccountNumber(ref ddlAccountNumber, ddlBankName.SelectedValue, ddlBranchName.SelectedValue);
        }

        protected void ddlChequeHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_cheque_dtl();
        }
        private void fn_fill_cheque_dtl()
        {
            PettyCashAllocation_BLL.fn_getChequeNum_dtl(ref ddlChequeDtl, ddlChequeHeader.SelectedValue.ToString());
            ddlChequeDtl.SelectedIndex = 1;
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlUserId = tmpgvr.FindControl("ddlUserId") as DropDownList;
                PettyCashExpenditure_BLL.GetUserEmpDtl(ref ddlUserId);



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    gvData.Columns[6].Visible = true;

                }
                else
                {
                    gvData.Columns[6].Visible = false;
                }

                if (gvData.EditIndex >= 0)
                {
                    ddlUserId.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.USER_CODE].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    txtTotAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "GL_PCA_AMOUNT");
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("GL_PCA_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("GL_PCA_AMOUNT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("GL_PCA_BALANCE_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("GL_PCA_BALANCE_AMOUNT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["POSTED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
              
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
              
                txtTotAmt.Text = CommonUtils.CalculateTotalAmount(dtData, FINColumnConstants.GL_PCA_AMOUNT);
                if (txtTotAmt.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtTotAmt.Text) > CommonUtils.ConvertStringToDecimal(txtWithdrawalAmount.Text))
                    {
                        ErrorCollection.Add("validAmt", "Sum of allocated amount should not exceed withdrawl amount.");
                        return;
                    }
                   
                    txtTotAmt.Text = (CommonUtils.ConvertStringToDecimal(txtWithdrawalAmount.Text) - CommonUtils.ConvertStringToDecimal(txtTotAmt.Text)).ToString();

                    txtTotAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotAmt.Text);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;

                    if (gvr == null)
                    {
                        return;
                    }
                    //gvData.Columns[6].Visible = false;
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlUserId = gvr.FindControl("ddlUserId") as DropDownList;
            TextBox dtpAllocDate = gvr.FindControl("dtpAllocDate") as TextBox;
            TextBox txtAllocAmount = gvr.FindControl("txtAllocAmount") as TextBox;
            TextBox txtBalAmount = gvr.FindControl("txtBalAmount") as TextBox;
            TextBox txtPreviousBalAmount = gvr.FindControl("txtPreviousBalAmount") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.GL_PCA_DTL_ID] = "0";
                drList["POSTED_FLAG"] = "FALSE";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            if (txtWithdrawalAmount.Text.ToString().Trim().Length == 0)
            {
                ErrorCollection.Add("withdrawamt", "Please Enter WithDraw Amount");
                return drList;
            }

            slControls[0] = ddlUserId;
            slControls[1] = dtpAllocDate;
            slControls[2] = txtAllocAmount;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Employee_P"] + " ~ " + Prop_File_Data["Allocation_Date_P"] + " ~ " + Prop_File_Data["Allocated_Amount_P"] + "";
            //string strMessage = "User Id ~ Allocation Date ~ Allocated Amount";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            ErrorCollection.Remove("ChequeDetails");

            if (ddlChequeDtl.SelectedValue == FINAppConstants.intialRowValueField && ddlChequeDtl.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ChequeDetails", "Please select the Cheque Detail.");
                return drList;
            }


            //string strCondition = "USER_CODE='" + ddlUserId.SelectedValue.ToString() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}
            if (decimal.Parse(txtAllocAmount.Text) > decimal.Parse(txtWithdrawalAmount.Text))
            {
                ErrorCollection.Add("validAmt", "Allocated amount should not exceed withdrawl amount.");
                return drList;
            }




            drList[FINColumnConstants.USER_CODE] = ddlUserId.SelectedValue;
            drList[FINColumnConstants.USER_NAME] = ddlUserId.SelectedItem.Text;

            if (dtpAllocDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.GL_PCA_DATE] = DBMethod.ConvertStringToDate(dtpAllocDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.GL_PCA_DATE] = DBNull.Value;
            }
            if (txtAllocAmount.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.GL_PCA_AMOUNT] = txtAllocAmount.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.GL_PCA_AMOUNT] = DBNull.Value;
            }

            if (txtBalAmount.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.GL_PCA_BALANCE_AMOUNT] = txtBalAmount.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.GL_PCA_BALANCE_AMOUNT] = DBNull.Value;
            }
            if (txtPreviousBalAmount.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.previous_bal_amount] = txtPreviousBalAmount.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.previous_bal_amount] = DBNull.Value;
            }
            
            if (txtRemarks.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.GL_PCA_REMARKS] = txtRemarks.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.GL_PCA_REMARKS] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BalAmount(object sender)
        {
            DataTable dtAlloc = new DataTable();

            GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlUserId = (DropDownList)gvr.FindControl("ddlUserId");
            TextBox txtBalAmount = (TextBox)gvr.FindControl("txtBalAmount");
            TextBox txtPreviousBalAmount = (TextBox)gvr.FindControl("txtPreviousBalAmount");
            TextBox txtAllocAmount = (TextBox)gvr.FindControl("txtAllocAmount");

            ErrorCollection.Remove("alwiamt");

            if (CommonUtils.ConvertStringToDecimal(txtAllocAmount.Text) > CommonUtils.ConvertStringToDecimal(txtWithdrawalAmount.Text))
            {
                ErrorCollection.Add("alwiamt", "Allocation amount should be less than the Withdrwal amount");
                return;
            }

            //dtAlloc = DBMethod.ExecuteQuery(PettyCashExpenditure_DAL.GetPettyBalAmount(ddlUserId.SelectedValue)).Tables[0];
            DataTable dtbalAmt = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetBalAmt(ddlUserId.SelectedValue)).Tables[0];
            if (dtbalAmt != null)
            {
                if (dtbalAmt.Rows.Count > 0)
                {

                    txtPreviousBalAmount.Text = (CommonUtils.ConvertStringToDecimal(dtbalAmt.Rows[0]["GL_PCA_BALANCE_AMOUNT"].ToString()) - CommonUtils.ConvertStringToDecimal(dtbalAmt.Rows[0]["Exp_Amount"].ToString())).ToString();
                
                   // txtPreviousBalAmount.Text = CommonUtils.ConvertStringToDecimal(dtAlloc.Rows[0]["gl_exp_amount"].ToString()).ToString();
                    txtBalAmount.Text = (CommonUtils.ConvertStringToDecimal(txtAllocAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPreviousBalAmount.Text)).ToString();

                    txtPreviousBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPreviousBalAmount.Text);
                    txtBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalAmount.Text);
                }
                else
                {
                    txtBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAllocAmount.Text);
                    txtPreviousBalAmount.Text = "0";

                }
            }
        }
        protected void ddlUserId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BalAmount(sender);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        protected void btnPosting_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;


                if (gvData.DataKeys[gvr.RowIndex].Values["GL_PCA_DTL_ID"].ToString() != string.Empty)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        if (EntityData != null)
                        {
                            gL_PETTY_CASH_ALLOCATE_HDR = (GL_PETTY_CASH_ALLOCATE_HDR)EntityData;

                            if (gL_PETTY_CASH_ALLOCATE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                FINSP.GetSP_GL_Posting(gvData.DataKeys[gvr.RowIndex].Values["GL_PCA_DTL_ID"].ToString(), "CA_009");

                                DBMethod.ExecuteNonQuery("UPDATE GL_PETTY_CASH_ALLOCATE_DTL set POSTED_FLAG='" + FINAppConstants.Y + "',POSTED_DATE=TO_DATE('" + DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString()) + "','dd/MM/yyyy') where GL_PCA_DTL_ID='" + gvData.DataKeys[gvr.RowIndex].Values["GL_PCA_DTL_ID"].ToString() + "'");
                                Button btn_tmp_post = (Button)sender;
                                btn_tmp_post.Enabled = false;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<GL_PETTY_CASH_ALLOCATE_DTL>(gL_PETTY_CASH_ALLOCATE_DTL);
                }

                DBMethod.DeleteEntity<GL_PETTY_CASH_ALLOCATE_HDR>(gL_PETTY_CASH_ALLOCATE_HDR);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (txtTotAmt.Text != string.Empty)
                {
                    if (decimal.Parse(txtTotAmt.Text) > decimal.Parse(txtWithdrawalAmount.Text))
                    {
                        ErrorCollection.Add("validAmt", "Sum of allocated amount should not exceed withdrawl amount.");
                        return;
                    }
                }


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Petty Cash Allocation");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();
                decimal dbl_Amount=0;
                for (int kLoop = 0; kLoop < dtGridData.Rows.Count; kLoop++)
                {
                    dbl_Amount += CommonUtils.ConvertStringToDecimal(dtGridData.Rows[kLoop][FINColumnConstants.GL_PCA_AMOUNT].ToString());
                }

                if (CommonUtils.ConvertStringToDecimal(txtWithdrawalAmount.Text) != dbl_Amount)
                {

                    ErrorCollection.Add("invalidwitdrawamt", "With draw amount and allocated amount must be same");
                    return;
                }
                AssignToBE();



                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtWithdrawlTyp_TextChanged(object sender, EventArgs e)
        {

        }
        protected void txtAllocAmount_TextChanged(object sender, EventArgs e)
        {
            BalAmount(sender);
        }
        protected void ddlAccountNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillchkhdr();
        }

        private void fillchkhdr()
        {
            Cheque_BLL.GetUnusedCheckNumberDetails(ref ddlChequeDtl, ddlBankName.SelectedValue, ddlBranchName.SelectedValue, ddlAccountNumber.SelectedValue,Master.Mode);
            if (ddlChequeDtl.Items.Count > 1)
            {
                ddlChequeDtl.SelectedIndex = 1;
                //ddlChequeDtl.Enabled = false;
                DataTable dtWithdrawlamt = new DataTable();

                dtWithdrawlamt = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeAmount(ddlChequeDtl.SelectedValue)).Tables[0];
                if (dtWithdrawlamt.Rows.Count > 0)
                {
                    txtWithdrawalAmount.Text = dtWithdrawlamt.Rows[0]["CHECK_AMT"].ToString();
                }
            }
            //  PettyCashAllocation_BLL.fn_getChequeNum(ref ddlChequeHeader,ddlBankName.SelectedValue,ddlBranchName.SelectedValue,ddlAccountNumber.SelectedValue);
        }

        protected void ddlChequeDtl_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtWithdrawlamt = new DataTable();
            dtWithdrawlamt = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeAmount(ddlChequeDtl.SelectedValue)).Tables[0];
            if (dtWithdrawlamt.Rows.Count > 0)
            {
                txtWithdrawalAmount.Text = dtWithdrawlamt.Rows[0]["CHECK_AMT"].ToString();
                if (txtWithdrawalAmount.Text != string.Empty)
                {
                    txtWithdrawalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtWithdrawalAmount.Text);
                }
            }
        }

        protected void imgBtnChequePrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Hashtable htFilterParameter = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                ErrorCollection.Clear();
                ReportFile = "AP_REPORTS/RPTChequePrintout_B.rpt";

                if (ddlChequeDtl.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("CHEQUEID", ddlChequeDtl.SelectedValue.ToString());

                    NumberToWord.ToWord toword = new NumberToWord.ToWord(decimal.Parse(txtWithdrawalAmount.Text));
                    htFilterParameter.Add("AMTINWORD", toword.ConvertToEnglish());

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.CA.Cheque_BLL.GetChequesReportData();

                    htHeadingParameters.Add("ReportName", "Cheque");

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPrintReport_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("ALLOCATE_ID", Master.StrRecordId.ToString());
                }
             
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.CA.PettyCashAllocation_BLL.GetPettyCashAllocationDetData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


    }
        #endregion


}