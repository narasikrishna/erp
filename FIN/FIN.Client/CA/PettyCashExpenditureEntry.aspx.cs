﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using VMVServices.Services.Data;

namespace FIN.Client.CA
{
    public partial class PettyCashExpenditureEntry : PageBase
    {
        GL_PETTY_CASH_EXPENDITURE_HDR gL_PETTY_CASH_EXPENDITURE_HDR = new GL_PETTY_CASH_EXPENDITURE_HDR();
        GL_PETTY_CASH_EXPENDITURE_DTL gL_PETTY_CASH_EXPENDITURE_DTL = new GL_PETTY_CASH_EXPENDITURE_DTL();
        DataTable dtGridData = new DataTable();
        string career_path_type;
        Boolean bol_rowVisiable;
        Boolean savedBool;
       

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.CA.PettyCashExpenditure_BLL.GetUserEmpDtl(ref ddlUser);
            // FIN.BLL.CA.PettyCashExpenditure_BLL.GetBalanceAmt(ref ddlBalanceAmount);
            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = FIN.BLL.CA.PettyCashExpenditure_BLL.getChildEntityDet(Master.StrRecordId);
                imgBtnPrint.Visible = false;
                div_ddl_CC.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<GL_PETTY_CASH_EXPENDITURE_HDR> userCtx = new DataRepository<GL_PETTY_CASH_EXPENDITURE_HDR>())
                    {
                        gL_PETTY_CASH_EXPENDITURE_HDR = userCtx.Find(r =>
                            (r.GL_PCE_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    ddlUser.Enabled = false;
                    EntityData = gL_PETTY_CASH_EXPENDITURE_HDR;

                    if (gL_PETTY_CASH_EXPENDITURE_HDR.GL_USER_ID != null)
                    {
                        ddlUser.SelectedValue = gL_PETTY_CASH_EXPENDITURE_HDR.GL_USER_ID;
                    }
                    if (gL_PETTY_CASH_EXPENDITURE_HDR.GL_EXPD_DATE != null)
                    {
                        txtExpenditureDate.Text = DBMethod.ConvertDateToString(gL_PETTY_CASH_EXPENDITURE_HDR.GL_EXPD_DATE.ToString());
                    }

                    if (gL_PETTY_CASH_EXPENDITURE_HDR.GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = gL_PETTY_CASH_EXPENDITURE_HDR.GLOBAL_SEGMENT_ID;
                    }
                    txtBalAmount.Text = gL_PETTY_CASH_EXPENDITURE_HDR.GL_BALANCE_AMOUNT.ToString();
                    txtBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalAmount.Text);
                    hf_Posted.Value = FINAppConstants.N;
                    if (gL_PETTY_CASH_EXPENDITURE_HDR.CREATED_BY == this.LoggedUserName)
                    {
                        hf_sameUser.Value  = "Y";
                    }
                    else
                    {
                        div_ddl_CC.Visible = true;
                        hf_sameUser.Value  = "N";
                        imgBtnPost.Visible = true;
                        imgBtnPrint.Visible = true;
                    }
                    if (gL_PETTY_CASH_EXPENDITURE_HDR.POSTED_FLAG == FINAppConstants.Y)
                    {
                        
                        imgBtnPost.Visible = false;
                        hf_Posted.Value = FINAppConstants.Y;
                        imgBtnJVPrint.Visible = true;
                    }
                }

                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {

            if (EntityData != null)
            {
                gL_PETTY_CASH_EXPENDITURE_HDR = (GL_PETTY_CASH_EXPENDITURE_HDR)EntityData;
            }

            if (gL_PETTY_CASH_EXPENDITURE_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PETTY_CASH_EXPENDITURE_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" +  VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    txtTotAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "GL_EXP_AMOUNT");
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("GL_EXP_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("GL_EXP_AMOUNT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = false;
                    dr["GL_PCE_POSTED"] = false;
                    dr["POSTED_FLAG"] = false;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                txtTotAmt.Text = (CommonUtils.CalculateTotalAmount(dtData, "GL_EXP_AMOUNT"));

                if (txtTotAmt.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtTotAmt.Text) > CommonUtils.ConvertStringToDecimal(txtBalAmount.Text))
                    {
                        ErrorCollection.Add("validAmt", "Sum of Expenditure amount should not be exceed the Allocated amount.");
                        return;
                    }
                    txtTotAmt.Text = (CommonUtils.ConvertStringToDecimal(txtBalAmount.Text) - CommonUtils.ConvertStringToDecimal(txtTotAmt.Text)).ToString();
                }

                if (txtTotAmt.Text != string.Empty)
                {
                    txtTotAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotAmt.Text);
                }
                if (hf_Posted.Value == FINAppConstants.Y)
                {
                    gvData.FooterRow.Visible = false;
                    gvData.Columns[0].Visible = false;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlAccountDetail = tmpgvr.FindControl("ddlAccountDetail") as DropDownList;
                TextBox txtExpenditureAmount = tmpgvr.FindControl("txtExpenditureAmount") as TextBox;
                FIN.BLL.CA.PettyCashExpenditure_BLL.GetAccountdtls(ref ddlAccountDetail);

                gvData.Columns[3].Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (hf_sameUser.Value == "Y")
                    {
                        txtExpenditureAmount.Enabled = true;                       
                        gvData.Columns[5].Visible = false;
                        gvData.Columns[6].Visible = false;
                        gvData.Columns[7].Visible = false;
                        gvData.Columns[8].Visible = false;
                        gvData.Columns[9].Visible = false;
                        gvData.Columns[10].Visible = false;
                        gvData.Columns[11].Visible = false;
                        imgBtnPost.Visible = false;
                    }
                    else
                    {
                        txtExpenditureAmount.Enabled = false;
                      
                        gvData.Columns[5].Visible = true ;
                        gvData.Columns[6].Visible = true;
                        gvData.Columns[7].Visible = true;
                        gvData.Columns[8].Visible = true;
                        gvData.Columns[9].Visible = true;
                        gvData.Columns[10].Visible = true;
                        gvData.Columns[11].Visible = true ;
                        //imgBtnPost.Visible = true;
                    }
                }
                else
                {
                    gvData.Columns[5].Visible = false;
                    gvData.Columns[6].Visible = false;
                    gvData.Columns[7].Visible = false;
                    gvData.Columns[8].Visible = false;
                    gvData.Columns[9].Visible = false;
                    gvData.Columns[10].Visible = false;
                    gvData.Columns[11].Visible = false;
                    imgBtnPost.Visible = false;
                }

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccountDetail.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ACCT_CODE_ID"].ToString();

                    if (gvData.DataKeys[gvData.EditIndex].Values["GL_PCE_DTL_ID"].ToString() != "0")
                    {
                        txtExpenditureAmount.Enabled = false;
                    }

                    BindGSegment(tmpgvr);
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment1 = tmpgvr.FindControl("ddlGSegment1") as DropDownList;
                        ddl_GSegment1.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString();
                        LoadGSegmentValues(ddl_GSegment1);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment2 = tmpgvr.FindControl("ddlGSegment2") as DropDownList;
                        ddl_GSegment2.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString();
                        LoadGSegmentValues(ddl_GSegment2);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment3 = tmpgvr.FindControl("ddlGSegment3") as DropDownList;
                        ddl_GSegment3.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString();
                        LoadGSegmentValues(ddl_GSegment3);
                    }


                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment4 = tmpgvr.FindControl("ddlGSegment4") as DropDownList;
                        ddl_GSegment4.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString();
                        LoadGSegmentValues(ddl_GSegment4);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment5 = tmpgvr.FindControl("ddlGSegment5") as DropDownList;
                        ddl_GSegment5.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString();
                        LoadGSegmentValues(ddl_GSegment5);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment6 = tmpgvr.FindControl("ddlGSegment6") as DropDownList;
                        ddl_GSegment6.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString();
                        LoadGSegmentValues(ddl_GSegment6);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlUser;
                //slControls[1] = ddlPricelistType;
                //slControls[2] = ddlCareerPathName;


                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
                string strMessage = " User ";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                if (txtTotAmt.Text != string.Empty)
                {
                    if (decimal.Parse(txtTotAmt.Text) > decimal.Parse(txtBalAmount.Text))
                    {
                        ErrorCollection.Add("validAmt", "Sum of Expenditure amount should not exceed Balance amount.");
                        return;
                    }
                }

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Pettycash ExpenditureList");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    //txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_PETTY_CASH_EXPENDITURE_HDR = (GL_PETTY_CASH_EXPENDITURE_HDR)EntityData;
                }

                gL_PETTY_CASH_EXPENDITURE_HDR.GL_USER_ID = ddlUser.SelectedValue.ToString();
                gL_PETTY_CASH_EXPENDITURE_HDR.ALLOC_DTL_ID = hf_GL_PCA_DTL_ID.Value;
                if (txtExpenditureDate.Text != string.Empty)
                {
                    gL_PETTY_CASH_EXPENDITURE_HDR.GL_EXPD_DATE = DBMethod.ConvertStringToDate(txtExpenditureDate.Text.ToString());
                }
                gL_PETTY_CASH_EXPENDITURE_HDR.GL_BALANCE_AMOUNT = CommonUtils.ConvertStringToDecimal(txtBalAmount.Text);




                gL_PETTY_CASH_EXPENDITURE_HDR.GL_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                gL_PETTY_CASH_EXPENDITURE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                gL_PETTY_CASH_EXPENDITURE_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_PETTY_CASH_EXPENDITURE_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_PETTY_CASH_EXPENDITURE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    gL_PETTY_CASH_EXPENDITURE_HDR.GL_PCE_HDR_ID = FINSP.GetSPFOR_SEQCode("AP_023_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    gL_PETTY_CASH_EXPENDITURE_HDR.CREATED_BY = this.LoggedUserName;
                    gL_PETTY_CASH_EXPENDITURE_HDR.CREATED_DATE = DateTime.Today;
                }

                gL_PETTY_CASH_EXPENDITURE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_PETTY_CASH_EXPENDITURE_HDR.GL_PCE_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_PETTY_CASH_EXPENDITURE_DTL = new GL_PETTY_CASH_EXPENDITURE_DTL();
                    if (dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<GL_PETTY_CASH_EXPENDITURE_DTL> userCtx = new DataRepository<GL_PETTY_CASH_EXPENDITURE_DTL>())
                        {
                            gL_PETTY_CASH_EXPENDITURE_DTL = userCtx.Find(r =>
                                (r.GL_PCE_DTL_ID == dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //iNV_PRICE_LIST_DTL.PRICE_HDR_ID = dtGridData.Rows[iLoop]["PRICE_HDR_ID"].ToString();
                    gL_PETTY_CASH_EXPENDITURE_DTL.GL_ACCT_CODE_ID = dtGridData.Rows[iLoop]["GL_ACCT_CODE_ID"].ToString();
                    gL_PETTY_CASH_EXPENDITURE_DTL.GL_EXP_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["GL_EXP_AMOUNT"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_REMARKS = dtGridData.Rows[iLoop]["GL_PCE_REMARKS"].ToString();
                    //hR_CAREER_PLAN_DTL.PLAN_PROFILE_REQUIRED_VALUE = short.Parse(dtGridData.Rows[iLoop]["PLAN_PROFILE_REQUIRED_VALUE"].ToString());

                    gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_HDR_ID = gL_PETTY_CASH_EXPENDITURE_HDR.GL_PCE_HDR_ID;
                    gL_PETTY_CASH_EXPENDITURE_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGridData.Rows[iLoop]["GL_PCE_POSTED"].ToString() == "TRUE")
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_POSTED = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_POSTED = FINAppConstants.N;
                    }
                    // gL_PETTY_CASH_EXPENDITURE_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == "TRUE")
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }
                    gL_PETTY_CASH_EXPENDITURE_DTL.EXPEND_DATE = gL_PETTY_CASH_EXPENDITURE_HDR.GL_EXPD_DATE;


                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_1 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_1"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_2 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_2"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_3 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_3"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_4 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_4"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_5 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_5"].ToString());
                    gL_PETTY_CASH_EXPENDITURE_DTL.JE_SEGMENT_ID_6 = (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_6"].ToString());



                    if (dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString() != string.Empty)
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_DTL_ID = dtGridData.Rows[iLoop]["GL_PCE_DTL_ID"].ToString();
                        gL_PETTY_CASH_EXPENDITURE_DTL.MODIFIED_BY = this.LoggedUserName;
                        gL_PETTY_CASH_EXPENDITURE_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(gL_PETTY_CASH_EXPENDITURE_DTL, "U"));
                    }
                    else
                    {
                        gL_PETTY_CASH_EXPENDITURE_DTL.GL_PCE_DTL_ID = FINSP.GetSPFOR_SEQCode("AP_023_D".ToString(), false, true);
                        gL_PETTY_CASH_EXPENDITURE_DTL.CREATED_BY = this.LoggedUserName;
                        gL_PETTY_CASH_EXPENDITURE_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(gL_PETTY_CASH_EXPENDITURE_DTL, "A"));
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<GL_PETTY_CASH_EXPENDITURE_HDR, GL_PETTY_CASH_EXPENDITURE_DTL>(gL_PETTY_CASH_EXPENDITURE_HDR, tmpChildEntity, gL_PETTY_CASH_EXPENDITURE_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<GL_PETTY_CASH_EXPENDITURE_HDR, GL_PETTY_CASH_EXPENDITURE_DTL>(gL_PETTY_CASH_EXPENDITURE_HDR, tmpChildEntity, gL_PETTY_CASH_EXPENDITURE_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }




        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<GL_PETTY_CASH_EXPENDITURE_DTL>(gL_PETTY_CASH_EXPENDITURE_DTL);
                }
                DBMethod.DeleteEntity<GL_PETTY_CASH_EXPENDITURE_HDR>(gL_PETTY_CASH_EXPENDITURE_HDR);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlAccountDetail = gvr.FindControl("ddlAccountDetail") as DropDownList;

            TextBox txtExpenditureAmount = gvr.FindControl("txtExpenditureAmount") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            CheckBox chkPosted = gvr.FindControl("chkPosted") as CheckBox;
            CheckBox chkActive = gvr.FindControl("chkActive") as CheckBox;


            DropDownList ddl_GSegment1 = gvr.FindControl("ddlGSegment1") as DropDownList;
            DropDownList ddl_GSegment2 = gvr.FindControl("ddlGSegment2") as DropDownList;
            DropDownList ddl_GSegment3 = gvr.FindControl("ddlGSegment3") as DropDownList;
            DropDownList ddl_GSegment4 = gvr.FindControl("ddlGSegment4") as DropDownList;
            DropDownList ddl_GSegment5 = gvr.FindControl("ddlGSegment5") as DropDownList;
            DropDownList ddl_GSegment6 = gvr.FindControl("ddlGSegment6") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["GL_PCE_DTL_ID"] = "0";
                drList["POSTED_FLAG"] = "FALSE";

                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            // slControls[0] = ddlAccountDetail;
            slControls[0] = txtExpenditureAmount;
            slControls[1] = txtRemarks;
            
            if (gvData.Columns[5].Visible)
            {
                slControls[2] = ddlAccountDetail;
                slControls[3] = ddl_GSegment1;
            }

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

            string strMessage = Prop_File_Data["Expenditure_Amount_P"] + " ~ " + Prop_File_Data["Remarks_P"] + "";
            if (gvData.Columns[5].Visible)
            {
                strCtrlTypes += "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                strMessage += "~" + "Account Code " + "~" + "Segment 1 ";
            }
            //string strMessage = "  Account Details ~ Expenditure Amount ~ Remarks ";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }






            drList["GL_ACCT_CODE_ID"] = ddlAccountDetail.SelectedValue;
            drList["ACCT_CODE"] = ddlAccountDetail.SelectedItem;
            drList["GL_EXP_AMOUNT"] = txtExpenditureAmount.Text;
            drList["GL_PCE_REMARKS"] = txtRemarks.Text;
            //drList["GL_PCE_POSTED"] = chkPosted.Checked;
            if (chkPosted.Checked)
            {
                drList["GL_PCE_POSTED"] = "TRUE";
            }
            else
            {
                drList["GL_PCE_POSTED"] = "FALSE";
            }
            if (chkActive.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            if (ddl_GSegment1.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_1"] = ddl_GSegment1.SelectedValue.ToString();
                drList["SEGMENT_1_TEXT"] = ddl_GSegment1.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_1"] = null;
                drList["SEGMENT_1_TEXT"] = null;
            }

            if (ddl_GSegment2.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_2"] = ddl_GSegment2.SelectedValue.ToString();
                drList["SEGMENT_2_TEXT"] = ddl_GSegment2.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_2"] = null;
                drList["SEGMENT_2_TEXT"] = null;
            }
            if (ddl_GSegment3.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_3"] = ddl_GSegment3.SelectedValue.ToString();
                drList["SEGMENT_3_TEXT"] = ddl_GSegment3.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_3"] = null;
                drList["SEGMENT_3_TEXT"] = null;
            }
            if (ddl_GSegment4.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_4"] = ddl_GSegment4.SelectedValue.ToString();
                drList["SEGMENT_4_TEXT"] = ddl_GSegment4.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_4"] = null;
                drList["SEGMENT_4_TEXT"] = null;
            }
            if (ddl_GSegment5.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_5"] = ddl_GSegment5.SelectedValue.ToString();
                drList["SEGMENT_5_TEXT"] = ddl_GSegment5.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_5"] = null;
                drList["SEGMENT_5_TEXT"] = null;
            }
            if (ddl_GSegment6.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_6"] = ddl_GSegment6.SelectedValue.ToString();
                drList["SEGMENT_6_TEXT"] = ddl_GSegment6.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_6"] = null;
                drList["SEGMENT_6_TEXT"] = null;
            }


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                        // e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }





        protected void ddlItemCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.FooterRow;

                DropDownList ddlItemCode = (DropDownList)gvr.FindControl("ddlItemCode");
                TextBox txtItemName = (TextBox)gvr.FindControl("txtItemName");
                TextBox txtItemUOM = (TextBox)gvr.FindControl("txtItemUOM");
                TextBox txtItemCost = (TextBox)gvr.FindControl("txtItemCost");

                INV_ITEM_MASTER iNV_ITEM_MASTER = new INV_ITEM_MASTER();

                using (IRepository<INV_ITEM_MASTER> ssm = new DataRepository<INV_ITEM_MASTER>())
                {
                    iNV_ITEM_MASTER = ssm.Find(x => (x.ITEM_ID == ddlItemCode.SelectedValue.ToString())).SingleOrDefault();
                    if (iNV_ITEM_MASTER != null)
                    {
                        Session["Item Code"] = iNV_ITEM_MASTER.ITEM_CODE;
                    }
                }
                DataTable dt_std_rate = new DataTable();
                dt_std_rate = FIN.BLL.AP.InventoryPriceList_BLL.getItemDtls(ddlItemCode.SelectedValue);
                if (dt_std_rate != null)
                {
                    if (dt_std_rate.Rows.Count > 0)
                    {
                        txtItemName.Text = dt_std_rate.Rows[0]["Item_Name"].ToString();
                        txtItemUOM.Text = dt_std_rate.Rows[0]["ITEM_UOM"].ToString();
                        txtItemCost.Text = dt_std_rate.Rows[0]["ITEM_UNIT_PRICE"].ToString();
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtbalAmt = new DataTable();
            hf_GL_PCA_DTL_ID.Value = "";
            dtbalAmt = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetBalAmt(ddlUser.SelectedValue)).Tables[0];

            

            
            if (dtbalAmt.Rows.Count > 0)
            {
                txtBalAmount.Text = (CommonUtils.ConvertStringToDecimal(dtbalAmt.Rows[0]["GL_PCA_BALANCE_AMOUNT"].ToString()) - CommonUtils.ConvertStringToDecimal(dtbalAmt.Rows[0]["Exp_Amount"].ToString())).ToString();
                hf_GL_PCA_DTL_ID.Value = dtbalAmt.Rows[0]["GL_PCA_DTL_ID"].ToString();
                if (txtBalAmount.Text != string.Empty)
                {
                    txtBalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalAmount.Text);
                }
            }
        }

        protected void btnPosting_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;


                if (gvData.DataKeys[gvr.RowIndex].Values["GL_PCE_DTL_ID"].ToString() != string.Empty)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        if (EntityData != null)
                        {

                            gL_PETTY_CASH_EXPENDITURE_HDR = (GL_PETTY_CASH_EXPENDITURE_HDR)EntityData;

                            if (gL_PETTY_CASH_EXPENDITURE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                FINSP.GetSP_GL_Posting(gvData.DataKeys[gvr.RowIndex].Values["GL_PCE_DTL_ID"].ToString(), "CA_008");

                                DBMethod.ExecuteNonQuery("UPDATE GL_PETTY_CASH_EXPENDITURE_DTL set GL_PCE_POSTED='" + FINAppConstants.Y + "', POSTED_FLAG='" + FINAppConstants.Y + "',POSTED_DATE=TO_DATE('" + DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString()) + "','dd/MM/yyyy') where GL_PCE_DTL_ID='" + gvData.DataKeys[gvr.RowIndex].Values["GL_PCE_DTL_ID"].ToString() + "'");
                                Button btn_tmp_post = (Button)sender;
                                btn_tmp_post.Enabled = false;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgBtnPost.Visible = false;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    if (dtGridData.Rows[iLoop]["GL_ACCT_CODE_ID"].ToString().Trim().Length == 0)
                    {
                        ErrorCollection.Add("AddAccountCode", "Account Code Can't Empty");
                        return;
                    }
                }

                AssignToBE();
                if (savedBool)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {

                        if (EntityData != null)
                        {

                            gL_PETTY_CASH_EXPENDITURE_HDR = (GL_PETTY_CASH_EXPENDITURE_HDR)EntityData;

                            if (gL_PETTY_CASH_EXPENDITURE_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                FINSP.GetSP_GL_Posting(Master.StrRecordId, "CA_008");

                                DBMethod.ExecuteNonQuery("UPDATE gL_PETTY_CASH_EXPENDITURE_HDR set  POSTED_FLAG='" + FINAppConstants.Y + "',POSTED_DATE=TO_DATE('" + DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString()) + "','dd/MM/yyyy') where GL_PCE_HDR_ID='" + Master.StrRecordId + "'");
                                imgBtnJVPrint.Visible = true;

                              Master.ShowMessage( ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                            }
                            else
                            {
                                imgBtnPost.Visible = true;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                BindGSegment(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccountDetail");

            if (ddl_AccountCodes.SelectedValue != null && ddlGlobalSegment.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty && ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {
                        //Label lblSegment = (Label)gvr.FindControl("lblSegment" + (iLoop + 1));
                        //lblSegment.Text = dtdataAc.Rows[iLoop][FINColumnConstants.SEGMENT_NAME].ToString();
                        if (iLoop == 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlGSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            ddlGSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["JE_SEGMENT_ID_" + (iLoop + 1)].ToString();

                        }
                    }
                }
            }
        }

        protected void ddlGSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGSegmentValues(sender);


        }
        private void LoadGSegmentValues(object sender)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Seg = (DropDownList)sender;
            int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlGSegment", ""));
            DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (int_ddlNo + 1));
            if (ddlGSegment != null && int_ddlNo < dtdataAc.Rows.Count)
            {
                if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                }
                else
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                }

            }
        }

        protected void imgBtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = Master.ReportName;
                //ReportFile = "GL_REPORTS//RPTPettyCashExpenditure.rpt";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    htFilterParameter.Add("GL_PCE_HDR_ID", Master.StrRecordId.ToString());
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetPettyCashExpenditureReportDtls(Master.StrRecordId.ToString()));

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PettyCashExpenditureReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}