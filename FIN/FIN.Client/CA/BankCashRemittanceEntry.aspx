﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankCashRemittanceEntry.aspx.cs" Inherits="FIN.Client.CA.BankCashRemittanceEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankShortName">
                Bank Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBankShortName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged"
                    Width="150px" CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBranchShortName" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlAccountNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            DataKeyNames="REMIT_DTL_ID,LOOKUP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
            EmptyDataText="No Record Found" OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated"
            OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing"
            OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
            <Columns>
                <%-- <asp:TemplateField HeaderText="Transaction Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTransType" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>' CssClass="EntryFont txtBox_N"
                                Width="30px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtTransType" runat="server" CssClass="EntryFont txtBox_N" Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTransType" runat="server" Text='<%# Eval("CHECK_LINE_NUM") %>' Width="95%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Transaction Type">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlTranstyp" runat="server" CssClass="RequiredField ddlStype" Width="100%">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlTranstyp" runat="server" CssClass="RequiredField ddlStype" Width="100%">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblTranstyp" runat="server" Text='<%# Eval("LOOKUP_NAME") %>' Width="100%"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Number">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNumber" runat="server" Text='<%# Eval("REMIT_TRANS_ID") %>' CssClass="EntryFont RequiredField txtBox_N"
                            Width="150px"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtNumber" runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblNumber" runat="server" Text='<%# Eval("REMIT_TRANS_ID") %>' Width="150px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Transaction Date">
                    <EditItemTemplate>
                        <asp:TextBox ID="dtpTransDate" runat="server" CssClass="EntryFont RequiredField  txtBox" Width="100%"
                            Text='<%#  Eval("REMIT_TRANS_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtenderd8" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="dtpTransDate">
                        </cc2:CalendarExtender>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dtpTransDate" runat="server" CssClass="EntryFont RequiredField txtBox" Width="100%"
                            Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                        <cc2:CalendarExtender ID="CalendarExtendedr9" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="dtpTransDate">
                        </cc2:CalendarExtender>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblTransDate" runat="server" Width="100%" Text='<%# Eval("REMIT_TRANS_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Amount">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("REMIT_TRANS_AMOUNT") %>'
                            CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,custom"
                            ValidChars=".," TargetControlID="txtAmount" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtAmount" runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,custom"
                            ValidChars=".," TargetControlID="txtAmount" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("REMIT_TRANS_AMOUNT") %>'
                            Width="150px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remarks">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("ATTRIBUTE10") %>' CssClass="EntryFont txtBox"
                            Width="150px"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="EntryFont txtBox" Width="150px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("ATTRIBUTE10") %>' Width="150px"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                            ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                            ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                            ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                            ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                            ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_btnCreateCheques").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
