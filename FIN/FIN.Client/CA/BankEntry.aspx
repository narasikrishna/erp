﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankEntry.aspx.cs" Inherits="FIN.Client.CA.BankEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblShortName">
                Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtShortName" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="1" OnTextChanged="txtShortName_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankNameEn">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 535px">
                <asp:TextBox ID="txtBankNameEn" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="200" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankNameAr">
                Bank Name(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 535px">
                <asp:TextBox ID="txtBankNameAr" CssClass="txtBox_ol" MaxLength="240" runat="server"
                    TabIndex="3" OnTextChanged="txtBankNameAr_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAddress1" MaxLength="100" CssClass="validate[]  txtBox" runat="server"
                    TabIndex="4"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    runat="server" TabIndex="5" CssClass="validate[]   ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAddress2" MaxLength="100" CssClass="validate txtBox" runat="server"
                    TabIndex="6"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblState">
                State
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    TabIndex="7" CssClass="validate[]   ddlStype" MaxLength="200">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAddress3" MaxLength="100" CssClass="validate txtBox" runat="server"
                    TabIndex="8"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCity">
                City
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="9" CssClass="validate[] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblPostalCode">
                Postal Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPostalCode" MaxLength="50" CssClass="validate[] txtBox" runat="server"
                    TabIndex="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblPhone">
                Phone
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPhone" CssClass="validate[] txtBox" runat="server" MaxLength="20"
                    TabIndex="11"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtPhone" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" TabIndex="12" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
