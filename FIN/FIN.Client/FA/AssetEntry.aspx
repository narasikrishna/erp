﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetEntry.aspx.cs" Inherits="FIN.Client.FA.AssetEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblTaxName">
                Asset Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetNumber" MaxLength="15" CssClass="validate[required] RequiredField   txtBox"
                    TabIndex="1" runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblTaxRate">
                Manufacture Detail
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtManufactureDtl" MaxLength="100" CssClass="validate[required] RequiredField   txtBox"
                    TabIndex="2" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Asset Model
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetModel" MaxLength="50" CssClass="validate[required] RequiredField   txtBox_en"
                    TabIndex="3" runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Asset Model(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetModelAr" MaxLength="50" CssClass="validate[required] RequiredField   txtBox_ol"
                    TabIndex="3" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Asset Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetName" MaxLength="50" CssClass="validate[required] RequiredField   txtBox_en"
                    TabIndex="4" runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Asset Name(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetNameAr" MaxLength="50" CssClass="validate[required] RequiredField   txtBox_ol"
                    TabIndex="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 670px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Original Cost
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtOriginalCost" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    TabIndex="6" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="txtOriginalCost" ValidChars="." />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div19">
                Asset Cost
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtAssetCost" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    TabIndex="7" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="txtAssetCost" ValidChars="." />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlCurrency" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="8" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblEffectiveStartDate">
                Exchange Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpExchangeDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox" Enabled="false"
                    runat="server" TabIndex="9" AutoPostBack="True" OnTextChanged="dtpExchangeDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="dtpExchangeDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div5" runat="server" visible="false">
                Exchange Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px" id="divx22" runat="server"
                visible="false">
                <asp:DropDownList ID="ddlExchangeType" CssClass=" ddlStype" TabIndex="10" runat="server">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 145px" id="Div7" runat="server" visible="false">
                Base Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px" id="Div8" runat="server"
                visible="false">
                <asp:DropDownList ID="ddlBaseCurrecny" CssClass=" ddlStype" TabIndex="11" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Exchange Rate
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtExchangeRate" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
                    TabIndex="12" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="txtExchangeRate" ValidChars="." />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive">
                Depreciable
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:CheckBox ID="chkDepreciable" runat="server" TabIndex="13" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div86">
                Date In Service
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpDateInService" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                    runat="server" TabIndex="14"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender5" TargetControlID="dtpDateInService"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                Purchase Order Reference
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtPOReference" MaxLength="50" CssClass="  txtBox" TabIndex="15"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div10">
                Asset In Use
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:CheckBox ID="chkAssetInUse" runat="server" TabIndex="16" Checked="true" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div11">
                Warranty Reference
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtWarrantyReference" MaxLength="150" CssClass="  txtBox" TabIndex="17"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div12">
                Warranty Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpWarrantyDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  RequiredField  txtBox"
                    runat="server" TabIndex="18"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender6" TargetControlID="dtpWarrantyDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div13">
                Major Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlMajorCategory" CssClass="validate[required]  RequiredField ddlStype"
                    TabIndex="19" OnSelectedIndexChanged="ddlMajorCategory_SelectedIndexChanged"
                    AutoPostBack="true" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div14">
                Minor Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlMinorCategory" CssClass="validate[required]  RequiredField ddlStype"
                    TabIndex="20" runat="server">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div15">
                Location
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlLocation" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="21" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div17">
                Debit Asset Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlDrAsAcct" CssClass="validate[required]  RequiredField ddlStype"
                    TabIndex="22" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDrAsAcct_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div18">
                Accumulated Depreciation Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlApLibAcct" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="23" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div16">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" TabIndex="24" Checked="true" />
            </div>
            <%-- <div class="lblBox  LNOrient" style=" width: 250px" id="Div19">
                &nbsp;
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:Button ID="btnSegval" runat="server" Text="Segment Value" CssClass="btn" 
                    TabIndex="20" onclick="btnSegval_Click" />
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="right">
            <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="btnPrint_Click"
                Visible="false" Style="border: 0px" />
            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Style="border: 0px"
                Visible="false" OnClick="imgBtnPost_Click" />
            <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                Visible="false" Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnSeg" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnSeg"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="70%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" Width="250px" CssClass="ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment1_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" Width="250px" CssClass="ddlStype"
                                    Enabled="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment2_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" Width="250px" CssClass="ddlStype"
                                    Enabled="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment3_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" Width="250px" CssClass="ddlStype"
                                    Enabled="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment4_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" Width="250px" CssClass="ddlStype"
                                    Enabled="false" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment5_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" CssClass="lblBox  LNOrient"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" Width="250px" CssClass="ddlStype"
                                    Enabled="false" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" OnClick="btnOK_Click" CssClass="button" ID="btnOkay" Text="Ok"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="20" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="21" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="22" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="23" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete1">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
