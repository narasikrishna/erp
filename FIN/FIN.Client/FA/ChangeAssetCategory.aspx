﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ChangeAssetCategory.aspx.cs" Inherits="FIN.Client.FA.ChangeAssetCategory" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1300px;" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblTaxName">
                Asset Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlAssetName" CssClass="RequiredField ddlStype" runat="server"
                    OnSelectedIndexChanged="ddlAssetName_SelectedIndexChanged" AutoPostBack="true"
                    >
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblTaxRate">
                Current Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:Label ID="lblCurrentCategory" CssClass="Label_Style" runat="server"></asp:Label>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblEffectiveStartDate">
                Major Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlMajorCategory" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="1" OnSelectedIndexChanged="ddlMajorCategory_SelectedIndexChanged" AutoPostBack="true"
                    >
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient"  >&nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="Div1">
                Minor Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlMinorCategory" CssClass="RequiredField ddlStype" runat="server"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                Effective From
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="dtpEffectivedate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="3" ></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="dtpEffectivedate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

                $(document).ready(function () {
                    fn_changeLng('<%= Session["Sel_Lng"] %>');
                });
    </script>
</asp:Content>
