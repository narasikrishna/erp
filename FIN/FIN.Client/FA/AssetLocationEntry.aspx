﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetLocationEntry.aspx.cs" Inherits="FIN.Client.FA.AssetLocationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="ASSET_LOCATION_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="ASSET_LOCATION_ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblASSETLOCATIONID" Text='<%# Eval("ASSET_LOCATION_ID") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblASSETLOCATIONID" Text='<%# Eval("ASSET_LOCATION_ID") %>' runat="server"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblASSETLOCATIONID" Text='' runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle CssClass="ColumnHide" />
                        <HeaderStyle CssClass="ColumnHide" />
                        <FooterStyle CssClass="ColumnHide" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Location Name">
                        <ItemTemplate>
                            <asp:Label ID="lblLocationName" Text='<%# Eval("LOCATION_NAME") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLocationName" MaxLength="100" CssClass="RequiredField   txtBox"  TabIndex="1"
                                Text='<%# Eval("LOCATION_NAME") %>' Width="310px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLocationName" MaxLength="100" CssClass="RequiredField   txtBox"  TabIndex="1"
                                Text='' runat="server" Width="310px"></asp:TextBox>
                        </FooterTemplate>
                      
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Asset Location Name(Arabic)">
                        <ItemTemplate>
                            <asp:Label ID="lblLocationNameAr" Text='<%# Eval("LOCATION_NAME_OL") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLocationNameAr" MaxLength="100" CssClass="RequiredField   txtBox_ol"  TabIndex="1"
                                Text='<%# Eval("LOCATION_NAME_OL") %>' Width="310px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLocationNameAr" MaxLength="100" CssClass="RequiredField   txtBox_ol"  TabIndex="1"
                                Text='' runat="server" Width="310px"></asp:TextBox>
                        </FooterTemplate>
                      
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location Description">
                        <ItemTemplate>
                            <asp:Label ID="lblLocationDescription" CssClass="Label_Style" Text='<%# Eval("LOCATION_DESC") %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate> 
                            <asp:TextBox ID="txtLocationDescription" MaxLength="100" CssClass="RequiredField   txtBox"  TabIndex="2"
                                Text='<%# Eval("LOCATION_DESC") %>' runat="server" Width="300px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLocationDescription" MaxLength="100" CssClass="RequiredField   txtBox"  TabIndex="2"
                                Width="300px" Text='' runat="server"></asp:TextBox>
                        </FooterTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"  TabIndex="3"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"  TabIndex="4"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"  TabIndex="5"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"  TabIndex="6"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="7" AlternateText="Add"  
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
