﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DashBoard_FA.aspx.cs" Inherits="FIN.Client.FA.DashBoard_FA" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server" Visible="false">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div style="width: 1100px">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 120px" id="lblGlobalSegment">
                    Global Segment
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 250px">
                    <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="3" Width="150px"
                        CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
                </div>
                <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 100px" id="Div1">
                    Group Name
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 250px">
                    <asp:DropDownList ID="ddlGroupName" runat="server" TabIndex="3" Width="150px" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
                </div>
                <div class="divRowContainer">
                <div class="lblBox  LNOrient" style=" width: 80px" id="lblDate">
                    Date
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 250px">
                    <asp:TextBox ID="txtDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        Width="150px"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            
            </div>
            <div class="divClear_10">
            </div>
            
            <div class="divRowContainer" style="float: right; width: 210px">
                <asp:Button ID="btnGenChart" runat="server" Text="Generate Chart" OnClick="btnGenerateChart_Click"
                    CssClass="btn" TabIndex="4" />
            </div>
            <div class="divClear_10">
            </div>
            <asp:Chart ID="ChartGL" runat="server" Width="500" Height="450">
                <Series>
                    <asp:Series Name="GLCreditSeries">
                    </asp:Series>
                    <asp:Series Name="GLDebitSeries">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend Name="Default">
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaGL">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Chart ID="ChartGroupGL" runat="server" Width="500" Height="450">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                    <asp:Series Name="Series2">
                    </asp:Series>
                    <asp:Series Name="Series3">
                    </asp:Series>
                    <asp:Series Name="Series4">
                    </asp:Series>
                    <asp:Series Name="Series5">
                    </asp:Series>
                    <asp:Series Name="Series6">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend Name="DefaultGroup">
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaGroupGL">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PayrollFinalrunDetails.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
