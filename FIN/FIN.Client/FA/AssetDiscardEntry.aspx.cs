﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using FIN.BLL.GL;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;
using VMVServices.Services.Data;
using VMVServices.Web;


namespace FIN.Client.FA
{
    public partial class AssetDiscardEntry : PageBase
    {
        string screenMode = string.Empty;
        DataSet dsData = new DataSet();
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;
        AST_ASSET_MST AST_ASSET_MST = new AST_ASSET_MST();
        AST_ASSET_DISCARD_DTL AST_ASSET_DISCARD_DTL = new AST_ASSET_DISCARD_DTL();

        DataTable dtCategoryData = new DataTable();
        DataTable dtLocationData = new DataTable();
        DataTable dtAssetData = new DataTable();
        DataTable dtData = new DataTable();

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
               // pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
               // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                //btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;


                BindCurrency();
                BindBaseCurrency();
                BindExchangeType();
                BindAsset();
                BindDiscardType();

                btnPrint.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnPost.Visible = false;

                if (Master.Mode != null)
                {

                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        using (IRepository<AST_ASSET_DISCARD_DTL> userCtx = new DataRepository<AST_ASSET_DISCARD_DTL>())
                        {
                            AST_ASSET_DISCARD_DTL = userCtx.Find(r =>
                                (r.ASSET_DISCARD_ID == Master.RecordID)
                                ).SingleOrDefault();
                        }


                        EntityData = AST_ASSET_DISCARD_DTL;
                        ddlAssetName.SelectedValue = AST_ASSET_DISCARD_DTL.ASSET_MST_ID.ToString();
                        ddlAssetName.Enabled = false;
                        ddlDiscardType.SelectedValue = AST_ASSET_DISCARD_DTL.DISCARD_TYPE_ID.ToString();
                        if (ddlDiscardType.SelectedValue == "Scrap")
                        {
                            btnPrint.Visible = true;
                            imgBtnPost.Visible = true;
                            if (AST_ASSET_DISCARD_DTL.POSTED_FLAG == "1")
                            {
                                imgBtnPost.Visible = false;
                                btnSave.Visible = false;
                                imgBtnJVPrint.Visible = true;
                            }
                        }
                        else
                        {
                            btnPrint.Visible = false;
                            imgBtnJVPrint.Visible = false;
                            imgBtnPost.Visible = false;
                        }

                        if (AST_ASSET_DISCARD_DTL.DISCARD_DATE != null)
                        {
                            dtpDiscardDate.Text = DBMethod.ConvertDateToString(AST_ASSET_DISCARD_DTL.DISCARD_DATE.ToString());
                        }
                        txtSellPrice.Text = AST_ASSET_DISCARD_DTL.SELL_PRICE.ToString();
                        ddlCurrency.SelectedValue = AST_ASSET_DISCARD_DTL.CURRENCY_ID.ToString();
                        ddlExchangeType.SelectedValue = AST_ASSET_DISCARD_DTL.EXCHANGE_TYPE_ID.ToString();
                        if (AST_ASSET_DISCARD_DTL.EXCHANGE_RATE_DATE != null)
                        {
                            dtpExchangeDate.Text = DBMethod.ConvertDateToString(AST_ASSET_DISCARD_DTL.EXCHANGE_RATE_DATE.ToString());
                        }
                        txtExchangeRate.Text = AST_ASSET_DISCARD_DTL.EXCHANGE_RATE.ToString();
                        ddlBaseCurrency.SelectedValue = AST_ASSET_DISCARD_DTL.BASE_CURRENCY_ID.ToString();
                        txtCustomerReference.Text = AST_ASSET_DISCARD_DTL.CUSTOMER_REFERENCE.ToString();

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetInwardsValidation");
                ErrorCollection.Add("AssetInwardsValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #region Fill Dropdownlist
        private void BindAsset()
        {
            //dtData.Clear();
            //using (IRepository<AST_ASSET_MST> userCtx = new DataRepository<AST_ASSET_MST>())
            //{
            //    dtData = DataSetLinqOperators.CopyToDataTable<AST_ASSET_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1") && (r.ORGANIZATION_MASTER_ID == VMVServices.Web.Utils.OrganizationID)));

            //}
            //if (dtData != null)
            //{
            //    if (dtData.Rows.Count > 0)
            //    {
            //        CommonUtils.LoadDropDownList(ddlAssetName, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
            //    }
            //}


            DataTable dtData = new DataTable();
            dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDetails(Master.Mode)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlAssetName, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
                }
            }
          

            //dtData.Clear();
            //using (IRepository<AST_ASSET_MST> userCtx = new DataRepository<AST_ASSET_MST>())
            //{
            //    dtData = DataSetLinqOperators.CopyToDataTable<AST_ASSET_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1") && (r.ORGANIZATION_MASTER_ID == VMVServices.Web.Utils.OrganizationID)));
            //}
            //CommonUtils.LoadDropDownList(ddlAssetName, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);

        }
        private void BindCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlCurrency);
        }
        private void BindDiscardType()
        {
            //dtData.Clear();

            //dtData = DBMethod.ExecuteQuery(FA_SQL.GetLookupData(FINMessageConstatns.Discard_Type)).Tables[0];

            //if (dtData != null)
            //{
            //    if (dtData.Rows.Count > 0)
            //    {
            //        CommonUtils.LoadDropDownList(ddlDiscardType, FINColumnConstants.VALUE_NAME, FINColumnConstants.VALUE_KEY_ID, dtData, true, false);
            //    }
            //}

            Lookup_BLL.GetLookUpValues(ref ddlDiscardType, "DST");
        }
        private void BindExchangeType()
        {
            Lookup_BLL.GetLookUpValues(ref ddlExchangeType, "ET");
        }
        private void BindBaseCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlBaseCurrency);
        }

        #endregion


        #region Insert ,Update and Delete the records
        private void EmptyValidation()
        {
            try
            {


                slControls[0] = ddlAssetName;
                slControls[1] = ddlDiscardType;
                slControls[2] = dtpDiscardDate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));

                strCtrlTypes = "DropDownList~DropDownList~DropDownList";
                string strMessage = Prop_File_Data["Asset_Name_P"] + " ~ " + Prop_File_Data["Discard_Type_P"] + " ~ " + Prop_File_Data["Discard_Date_P"] + "";
                //strMessage = "Asset Name ~ Discard Type ~Discard Date";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetDiscadrValidation");
                ErrorCollection.Add("AssetDiscardValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                //Assign to Asset Master BE
                if (EntityData != null)
                {
                    AST_ASSET_DISCARD_DTL = (AST_ASSET_DISCARD_DTL)EntityData;
                }

                AST_ASSET_DISCARD_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();              

                AST_ASSET_DISCARD_DTL.ASSET_MST_ID = int.Parse(ddlAssetName.SelectedValue == string.Empty ? "0" : ddlAssetName.SelectedValue.ToString());
                AST_ASSET_DISCARD_DTL.SELL_PRICE = CommonUtils.ConvertStringToDecimal(txtSellPrice.Text == string.Empty ? "0" : txtSellPrice.Text.ToString());
                AST_ASSET_DISCARD_DTL.CUSTOMER_REFERENCE = txtCustomerReference.Text;
                AST_ASSET_DISCARD_DTL.DISCARD_TYPE_ID = (ddlDiscardType.SelectedValue == string.Empty ? "0" : ddlDiscardType.SelectedValue.ToString());
                AST_ASSET_DISCARD_DTL.CURRENCY_ID = (ddlCurrency.SelectedValue == string.Empty ? "0" : ddlCurrency.SelectedValue.ToString());
                AST_ASSET_DISCARD_DTL.EXCHANGE_TYPE_ID = (ddlExchangeType.SelectedValue == string.Empty ? "0" : ddlExchangeType.SelectedValue.ToString());
                AST_ASSET_DISCARD_DTL.BASE_CURRENCY_ID = (ddlBaseCurrency.SelectedValue == string.Empty ? "0" : ddlBaseCurrency.SelectedValue.ToString());
             
                if (dtpExchangeDate.Text != null && dtpExchangeDate.Text != string.Empty)
                {
                    AST_ASSET_DISCARD_DTL.EXCHANGE_RATE_DATE = DBMethod.ConvertStringToDate(dtpExchangeDate.Text);
                }
             
                AST_ASSET_DISCARD_DTL.EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());
            
                if (dtpDiscardDate.Text != null && dtpDiscardDate.Text != string.Empty)
                {
                    AST_ASSET_DISCARD_DTL.DISCARD_DATE = DBMethod.ConvertStringToDate(dtpDiscardDate.Text);
                }

                AST_ASSET_DISCARD_DTL.ENABLED_FLAG = "1";

                if (Master.Mode != null)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        AST_ASSET_DISCARD_DTL.MODIFIED_BY = this.LoggedUserName;
                        AST_ASSET_DISCARD_DTL.MODIFIED_DATE = DateTime.Today;
                    }
                    else
                    {
                        AST_ASSET_DISCARD_DTL.CREATE_BY = this.LoggedUserName;
                        AST_ASSET_DISCARD_DTL.CREATED_DATE = DateTime.Today;
                        AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_ASSET_MST_SEQ);
                       
                    }

                    AST_ASSET_DISCARD_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID.ToString());

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetBE");
                ErrorCollection.Add("AssetBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                EmptyValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<AST_ASSET_DISCARD_DTL>(AST_ASSET_DISCARD_DTL);
                            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.SaveEntity<AST_ASSET_DISCARD_DTL>(AST_ASSET_DISCARD_DTL, true);                        
                            break;
                        }
                  
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove(FINMessageConstatns.AssetDiscard);
                ErrorCollection.Add(FINMessageConstatns.AssetDiscard, ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Confirm button for delete the particular records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_DISCARD_DTL>(AST_ASSET_DISCARD_DTL);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove(FINMessageConstatns.AssetDiscard);
                ErrorCollection.Add("AssetDiscardEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetDiscardList.aspx", false);
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Currency_BLL.IsBaseCurrency(ddlCurrency.SelectedValue.ToString()))
                {
                    ddlExchangeType.Enabled = false;
                    txtExchangeRate.Enabled = false;
                    dtpExchangeDate.Enabled = false;
                //    ddlBaseCurrecny.SelectedValue = ddlCurrency.SelectedValue;
                    //ddlBaseCurrecny.Enabled = false;
                    txtExchangeRate.Text = "1";
                }
                else
                {
                    ddlExchangeType.Enabled = true;
                    txtExchangeRate.Enabled = true;
                    dtpExchangeDate.Enabled = true;
                    // ddlBaseCurrecny.Enabled = true;
                    txtExchangeRate.Text = string.Empty;

                    GetExchangeRate();
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void GetExchangeRate()
        {
            DataTable dtData = new DataTable();

            if (ddlCurrency.SelectedValue != string.Empty & dtpExchangeDate.Text != string.Empty)
            {
                dtData = ExchangeRate_BLL.getStandardRate(ddlCurrency.SelectedValue, (dtpExchangeDate.Text));
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtExchangeRate.Text = dtData.Rows[0]["currency_std_rate"].ToString();
                    }
                }
            }
        }
        protected void dtpExchangeDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GetExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgBtnPost.Visible = false;
                if (EntityData != null)
                {
                    AST_ASSET_DISCARD_DTL = (AST_ASSET_DISCARD_DTL)EntityData;
                }
                if (AST_ASSET_DISCARD_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(AST_ASSET_DISCARD_DTL.ASSET_MST_ID.ToString(), "FA_011");
                }
                imgBtnPost.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                imgBtnJVPrint.Visible = true;
                Print_JV();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FAAssetDiscard_Posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            Print_JV();
        }

        private void Print_JV()
        {
            AST_ASSET_DISCARD_DTL = AssetDiscardBLL.getClassEntity(Master.StrRecordId);

            if (AST_ASSET_DISCARD_DTL != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", AST_ASSET_DISCARD_DTL.JE_HDR_ID));

                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                AST_ASSET_DISCARD_DTL = AssetDiscardBLL.getClassEntity(Master.StrRecordId);

                if (AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID.ToString() != string.Empty)
                {
                    htFilterParameter.Add("ASSET_DISCARD_ID", AST_ASSET_DISCARD_DTL.ASSET_DISCARD_ID.ToString());
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.FA.AssetDiscardBLL.GetAssetDiscardReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FAAssetReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}