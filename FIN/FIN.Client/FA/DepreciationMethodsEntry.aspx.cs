﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class DepreciationMethodsEntry : PageBase
    {
        AST_DEPRECIATION_METHOD_MST aST_DEPRECIATION_METHOD_MST = new AST_DEPRECIATION_METHOD_MST();
        string screenMode = string.Empty;
        DataTable dtGridData = new DataTable();
        DepreciationMethodsBLL depreciationMethodsBLL = new DepreciationMethodsBLL();
        SortedList slControls = new SortedList();
        Boolean bol_rowVisiable;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //    btnSave.Visible = false;
                //    btnDelete.Visible = true;
                //    pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnDelete.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                if (Master.Mode != null)
                {
                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetDepreciationMethodList(0)).Tables[0];

                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetDepreciationMethodList(Master.RecordID)).Tables[0];
                    }

                    BindGrid(dtGridData);
                }
                else
                {
                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetDepreciationMethodList(0)).Tables[0];
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        public DataRow AssignGridControlToBE(DataRow drList, GridViewRow gvr, DataTable dtGridData)
        {


            TextBox txt_Name = new TextBox();
            txt_Name = gvr.FindControl("txtName") as TextBox;

            TextBox txt_NameAr = new TextBox();
            txt_NameAr = gvr.FindControl("txtNameAr") as TextBox;

            TextBox txt_Description = new TextBox();
            txt_Description = gvr.FindControl("txtDescription") as TextBox;

            DropDownList ddl_Method = new DropDownList();
            ddl_Method = gvr.FindControl("ddlMethod") as DropDownList;

            TextBox txt_Rate = new TextBox();
            txt_Rate = gvr.FindControl("txtRate") as TextBox;

            DropDownList ddl_DurationType = new DropDownList();
            ddl_DurationType = gvr.FindControl("ddlDurationType") as DropDownList;

            TextBox ntxt_Duration = new TextBox();
            ntxt_Duration = gvr.FindControl("ntxtDuration") as TextBox;

            ErrorCollection.Clear();

            //Validation
            slControls[0] = txt_Name;
            slControls[1] = ddl_Method;
            slControls[2] = txt_Rate;
            slControls[3] = ddl_DurationType;
            slControls[4] = ntxt_Duration;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "TextBox~DropDownList~TextBox~DropDownList~TextBox";
            string strMessage = Prop_File_Data["Name_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Rate_P"] + " ~ " + Prop_File_Data["Duration_Type_P"] + " ~ " + Prop_File_Data["Duration_P"] + "";
            //string strMessage = "Depreciation Name ~ Depreciation Method ~ Rate ~ Duration Type ~ Duration";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (EmptyErrorCollection.Count > 0)
            {
                return drList;

            }

            if (Master.Mode == FINAppConstants.Add)
            {
                string strCondition = "DPRN_NAME='" + txt_Name.Text + "'";
                strMessage = FINMessageConstatns.RecordAlreadyExists;
                ErrorCollection = UserUtility_BLL.DataDuplication(dtGridData, strCondition, strMessage);
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
            }



            aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = int.Parse(drList[FINColumnConstants.DPRN_METHOD_MST_ID].ToString());
            aST_DEPRECIATION_METHOD_MST.DPRN_NAME = txt_Name.Text;
           aST_DEPRECIATION_METHOD_MST.DPRN_NAME_OL = txt_NameAr.Text;
            depreciationMethodsBLL.aST_DEPRECIATION_METHOD_MST = aST_DEPRECIATION_METHOD_MST;
            depreciationMethodsBLL.ValidateGrid(dtGridData, Master.Mode, gvr.RowIndex);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.DPRN_NAME] = txt_Name.Text;
            drList["DPRN_NAME_OL"] = txt_NameAr.Text;
            drList[FINColumnConstants.DPRN_DESC] = txt_Description.Text;

            drList[FINColumnConstants.DPRN_METHOD_ID] = ddl_Method.SelectedValue;

            // drList[FINColumnConstants.DPRN_METHOD_NAME] = ddl_Method.SelectedItem.Text;

            drList[FINColumnConstants.DPRN_RATE] = txt_Rate.Text;

            drList[FINColumnConstants.DPRN_TYPE_DURATION_ID] = ddl_DurationType.SelectedValue;

            //drList[FINColumnConstants.DPRN_TYPE_DURATION_NAME] = ddl_DurationType.SelectedItem.Text;

            drList[FINColumnConstants.DPRN_TYPE_DURATION] = ntxt_Duration.Text;

            if (int.Parse(Master.RecordID.ToString()) > 0)
            {
                aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = int.Parse(Master.RecordID.ToString());
            }
            else
            {
                aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = 0;
            }

            aST_DEPRECIATION_METHOD_MST.DPRN_NAME = txt_Name.Text;

            return drList;

        }

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;
            BindGrid(dtGridData);
            SetVisibleDeleteAction();
        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drGridList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert"))
                {
                    gvr = gvData.Controls[0].Controls[1] as GridViewRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }
                else if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {

                    drGridList = dtGridData.NewRow();

                    drGridList[FINColumnConstants.DPRN_METHOD_MST_ID] = 0;

                    AssignGridControlToBE(drGridList, gvr, dtGridData);

                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }

                    if (EmptyErrorCollection.Count > 0)
                    {
                        ErrorCollection = EmptyErrorCollection;
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    else if (depreciationMethodsBLL.ErrorCollection.Count > 0)
                    {
                        ErrorCollection = depreciationMethodsBLL.ErrorCollection;
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }

                    else
                    {
                        dtGridData.Rows.Add(drGridList);
                        BindGrid(dtGridData);
                        SetVisibleDeleteAction();
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

                //gvData.DataSource = dtData;
                //gvData.DataBind();

                if (Master.Mode == FINAppConstants.Add)
                {
                    gvData.FooterRow.Visible = true;
                }
                else
                {
                    gvData.FooterRow.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            dtGridData.Rows[e.RowIndex].Delete();
            dtGridData.AcceptChanges();
            BindGrid(dtGridData);
            SetVisibleDeleteAction();
        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = e.NewEditIndex;
            BindGrid(dtGridData);
            SetVisibleDeleteAction();
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drGridList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drGridList = dtGridData.Rows[e.RowIndex];

                AssignGridControlToBE(drGridList, gvr, dtGridData);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                else if (depreciationMethodsBLL.ErrorCollection.Count > 0)
                {
                    ErrorCollection = depreciationMethodsBLL.ErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                    SetVisibleDeleteAction();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            {
                GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                gvData.Controls[0].Controls.AddAt(0, gvr);
            }

        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_Method = new DropDownList();
                ddl_Method = e.Row.FindControl("ddlMethod") as DropDownList;

                DropDownList ddl_DurationType = new DropDownList();
                ddl_DurationType = e.Row.FindControl("ddlDurationType") as DropDownList;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (ddl_Method != null)
                    {
                        ddl_Method.Text = gvData.DataKeys[e.Row.RowIndex].Values["DPRN_METHOD_ID"].ToString();
                    }
                    if (ddl_DurationType != null)
                    {
                        ddl_DurationType.Text = gvData.DataKeys[e.Row.RowIndex].Values["DPRN_TYPE_DURATION_ID"].ToString();
                    }
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                }

                if (e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.EmptyDataRow || e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (ddl_Method != null)
                    {
                        Lookup_BLL.GetLookUpValues(ref ddl_Method, "DM");
                    }
                    if (ddl_DurationType != null)
                    {
                        Lookup_BLL.GetLookUpValues(ref ddl_DurationType, "DT");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindDepreciationMaster()
        {
            DropDownList ddl_Method = new DropDownList();
            DropDownList ddl_DurationType = new DropDownList();

            ddl_Method.ID = "ddlMethod";
            ddl_DurationType.ID = "ddlDurationType";

            if (gvData.FooterRow != null)
            {
                if (gvData.EditIndex < 0)
                {
                    ddl_Method = gvData.FooterRow.FindControl(ddl_Method.ID) as DropDownList;
                    ddl_DurationType = gvData.FooterRow.FindControl(ddl_DurationType.ID) as DropDownList;

                }
                else
                {
                    ddl_Method = gvData.Rows[gvData.EditIndex].FindControl(ddl_Method.ID) as DropDownList;
                    ddl_DurationType = gvData.Rows[gvData.EditIndex].FindControl(ddl_DurationType.ID) as DropDownList;
                }
            }
            else
            {
                ddl_Method = gvData.Controls[0].Controls[1].FindControl(ddl_Method.ID) as DropDownList;
                ddl_DurationType = gvData.Controls[0].Controls[1].FindControl(ddl_DurationType.ID) as DropDownList;
            }
            if (ddl_Method != null)
            {
                Lookup_BLL.GetLookUpValues(ref ddl_Method, "DM");
                Lookup_BLL.GetLookUpValues(ref ddl_DurationType, "DT");

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Depreciation Method ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    AST_DEPRECIATION_METHOD_MST aST_DEPRECIATION_METHOD_MST = new AST_DEPRECIATION_METHOD_MST();

                    if (int.Parse(dtGridData.Rows[iLoop]["DPRN_METHOD_MST_ID"].ToString()) > 0)
                    {
                        using (IRepository<AST_DEPRECIATION_METHOD_MST> userCtx = new DataRepository<AST_DEPRECIATION_METHOD_MST>())
                        {
                            aST_DEPRECIATION_METHOD_MST = userCtx.Find(r =>
                                (r.DPRN_METHOD_MST_ID == int.Parse(dtGridData.Rows[iLoop]["DPRN_METHOD_MST_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    aST_DEPRECIATION_METHOD_MST.DPRN_NAME = dtGridData.Rows[iLoop]["DPRN_NAME"].ToString();
                    aST_DEPRECIATION_METHOD_MST.DPRN_NAME_OL = dtGridData.Rows[iLoop]["DPRN_NAME_OL"].ToString();
                    aST_DEPRECIATION_METHOD_MST.DPRN_DESC = dtGridData.Rows[iLoop]["DPRN_DESC"].ToString();
                    aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_ID = (dtGridData.Rows[iLoop]["DPRN_METHOD_ID"].ToString());
                    aST_DEPRECIATION_METHOD_MST.DPRN_RATE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["DPRN_RATE"].ToString());
                    aST_DEPRECIATION_METHOD_MST.DPRN_TYPE_DURATION_ID = (dtGridData.Rows[iLoop]["DPRN_TYPE_DURATION_ID"].ToString());
                    aST_DEPRECIATION_METHOD_MST.DPRN_TYPE_DURATION = (dtGridData.Rows[iLoop]["DPRN_TYPE_DURATION"].ToString());
                    aST_DEPRECIATION_METHOD_MST.ENABLED_FLAG = "1";
                    //aST_DEPRECIATION_METHOD_MST.WORKFLOW_COMPLETION_STATUS = "1";

                    aST_DEPRECIATION_METHOD_MST.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                    // aST_DEPRECIATION_METHOD_MST.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

                    if (int.Parse(dtGridData.Rows[iLoop]["DPRN_METHOD_MST_ID"].ToString()) > 0)
                    {
                        aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = int.Parse(dtGridData.Rows[iLoop]["DPRN_METHOD_MST_ID"].ToString());

                        aST_DEPRECIATION_METHOD_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID.ToString());

                        aST_DEPRECIATION_METHOD_MST.MODIFIED_BY = this.LoggedUserName;
                        aST_DEPRECIATION_METHOD_MST.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<AST_DEPRECIATION_METHOD_MST>(aST_DEPRECIATION_METHOD_MST, true);

                    }
                    else
                    {
                        aST_DEPRECIATION_METHOD_MST.CREATE_BY = this.LoggedUserName;
                        aST_DEPRECIATION_METHOD_MST.CREATED_DATE = DateTime.Today;

                        aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.aST_DEPRECIATION_METHOD_SEQ);

                        aST_DEPRECIATION_METHOD_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID.ToString());

                        DBMethod.SaveEntity<AST_DEPRECIATION_METHOD_MST>(aST_DEPRECIATION_METHOD_MST);

                    }

                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DepreciationMethodsEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("DepreciationMethodList.aspx", false);
        }

        protected void ibtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ModalPopupExtender1.Show();
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            AST_DEPRECIATION_METHOD_MST aST_DEPRECIATION_METHOD_MST = new AST_DEPRECIATION_METHOD_MST();
            try
            {
                ErrorCollection.Clear();
                aST_DEPRECIATION_METHOD_MST.DPRN_METHOD_MST_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_DEPRECIATION_METHOD_MST>(aST_DEPRECIATION_METHOD_MST);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DepreciationMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void SetVisibleDeleteAction()
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    ImageButton ibtnGridDelete = gvData.Rows[iLoop].FindControl("ibtnDelete") as ImageButton;
                    if (ibtnGridDelete != null)
                    {
                        if (((Label)gvData.Rows[iLoop].FindControl("lblDPRNMETHODMSTID")).Text != null)
                        {
                            if (((Label)gvData.Rows[iLoop].FindControl("lblDPRNMETHODMSTID")).Text != string.Empty)
                            {

                                if (int.Parse(((Label)gvData.Rows[iLoop].FindControl("lblDPRNMETHODMSTID")).Text) > 0)
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = false;
                                }
                                else
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}