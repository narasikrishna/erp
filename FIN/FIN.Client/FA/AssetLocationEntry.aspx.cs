﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class AssetLocationEntry : PageBase
    {
        AST_LOCATION_MST aST_LOCATION_MST = new AST_LOCATION_MST();
        string screenMode = string.Empty;
        AssetLocationBLL assetLocationBLL = new AssetLocationBLL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        SortedList slControls = new SortedList();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //    btnSave.Visible = false;
                //    btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {

                Startup();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocation(0)).Tables[0];
                if (Master.Mode != null)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocation(Master.RecordID)).Tables[0];
                    }
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        public DataRow AssignGridControlToBE(DataRow drList, GridViewRow gvr)
        {
            TextBox txt_Location_Name = new TextBox();
            TextBox txt_Location_NameAr = new TextBox();
            txt_Location_Name = gvr.FindControl("txtLocationName") as TextBox;
            txt_Location_NameAr = gvr.FindControl("txtLocationNameAr") as TextBox;

            TextBox txt_Location_Description = new TextBox();
            txt_Location_Description = gvr.FindControl("txtLocationDescription") as TextBox;

            //if (txt_Location_Name.Text.ToString().Length == 0)
            //{
            //    ScriptManager.RegisterStartupScript(ibtnSave, typeof(ImageButton), "Location", "alert('Please enter the Location Name');", true);
            //    return;
            //}

            ErrorCollection.Clear();

            slControls[0] = txt_Location_Name;
            slControls[1] = txt_Location_Name;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "TextBox~TextBox";
            string strMessage = Prop_File_Data["Asset_Location_Name_P"] + " ~ " + Prop_File_Data["Location_Description_P"] + "";
            //string strMessage = "Asset Location Name~Location Description";
           
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                return drList;
            }
            aST_LOCATION_MST.ASSET_LOCATION_ID = int.Parse(drList[FINColumnConstants.ASSET_LOCATION_ID].ToString());
            aST_LOCATION_MST.LOCATION_NAME = txt_Location_Name.Text;
            aST_LOCATION_MST.LOCATION_NAME_OL = txt_Location_NameAr.Text;
            assetLocationBLL.aST_LOCATION_MST = aST_LOCATION_MST;
            assetLocationBLL.ValidateGrid(dtGridData, Master.Mode);



            drList[FINColumnConstants.LOCATION_NAME] = txt_Location_Name.Text;
            drList["LOCATION_NAME_OL"] = txt_Location_NameAr.Text;
            drList[FINColumnConstants.LOCATION_DESC] = txt_Location_Description.Text;

            if (Master.RecordID != null)
            {
                if (int.Parse(Master.RecordID.ToString()) > 0)
                {
                    aST_LOCATION_MST.ASSET_LOCATION_ID = int.Parse(Master.RecordID.ToString());
                }
                else
                {
                    aST_LOCATION_MST.ASSET_LOCATION_ID = 0;
                }
            }
            else
            {
                aST_LOCATION_MST.ASSET_LOCATION_ID = 0;
            }


            return drList;
        }

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;
            BindGrid(dtGridData);
            SetVisibleDeleteAction();

        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
            DataRow drGridList = null;
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }

            if (e.CommandName.Equals("EmptyDataTemplateInsert"))
            {
                gvr = gvData.Controls[0].Controls[1] as GridViewRow;
                if (gvr == null)
                {
                    return;
                }
            }
            else if (e.CommandName.Equals("FooterInsert"))
            {
                gvr = gvData.FooterRow;
                if (gvr == null)
                {
                    return;
                }
            }


            if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
            {

                drGridList = dtGridData.NewRow();

                drGridList[FINColumnConstants.ASSET_LOCATION_ID] = 0;

                AssignGridControlToBE(drGridList, gvr);
                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                else if (assetLocationBLL.ErrorCollection.Count > 0)
                {
                    ErrorCollection = assetLocationBLL.ErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                else
                {
                    dtGridData.Rows.Add(drGridList);
                    BindGrid(dtGridData);
                    SetVisibleDeleteAction();
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();

            //gvData.DataSource = dtData;
            //gvData.DataBind();

            if (Master.Mode == FINAppConstants.Add)
            {
                gvData.FooterRow.Visible = true;
            }
            else
            {
                gvData.FooterRow.Visible = false;
            }

        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            dtGridData.Rows[e.RowIndex].Delete();
            dtGridData.AcceptChanges();
            BindGrid(dtGridData);
            SetVisibleDeleteAction();

        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = e.NewEditIndex;
            BindGrid(dtGridData);
            SetVisibleDeleteAction();
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
            DataRow drGridList = null;

            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            if (gvr == null)
            {
                return;
            }

            drGridList = dtGridData.Rows[e.RowIndex];
            AssignGridControlToBE(drGridList, gvr);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                return;
            }
            else if (assetLocationBLL.ErrorCollection.Count > 0)
            {
                ErrorCollection = assetLocationBLL.ErrorCollection;
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                return;
            }
            else
            {
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
                SetVisibleDeleteAction();
            }

        }

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            {
                GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                gvData.Controls[0].Controls.AddAt(0, gvr);
            }

        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (dtGridData.Rows.Count == 0)
                {
                    //ScriptManager.RegisterStartupScript(btnSave, typeof(ImageButton), "Asset Location", "alert('Please add a row to get data saved');", true);
                    ErrorCollection.Add("savemsg", "Location Details Cannot be empty");
                    return;
                }
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    AST_LOCATION_MST aST_LOCATION_MST = new AST_LOCATION_MST();

                    if (int.Parse(dtGridData.Rows[iLoop]["ASSET_LOCATION_ID"].ToString()) > 0)
                    {
                        using (IRepository<AST_LOCATION_MST> userCtx = new DataRepository<AST_LOCATION_MST>())
                        {
                            aST_LOCATION_MST = userCtx.Find(r =>
                                (r.ASSET_LOCATION_ID == int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.ASSET_LOCATION_ID].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    aST_LOCATION_MST.LOCATION_NAME = dtGridData.Rows[iLoop][FINColumnConstants.LOCATION_NAME].ToString();
                    aST_LOCATION_MST.LOCATION_NAME_OL = dtGridData.Rows[iLoop]["LOCATION_NAME_OL"].ToString();
                    aST_LOCATION_MST.LOCATION_DESC = dtGridData.Rows[iLoop][FINColumnConstants.LOCATION_DESC].ToString();
                    aST_LOCATION_MST.ENABLED_FLAG = "1";
                    //Organization and calendar id
                    aST_LOCATION_MST.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                    //  aST_LOCATION_MST.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;


                    if (int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.ASSET_LOCATION_ID].ToString()) > 0)
                    {
                        aST_LOCATION_MST.ASSET_LOCATION_ID = int.Parse(dtGridData.Rows[iLoop]["ASSET_LOCATION_ID"].ToString());
                        aST_LOCATION_MST.MODIFIED_BY = this.LoggedUserName;
                        aST_LOCATION_MST.MODIFIED_DATE = DateTime.Today;
                        aST_LOCATION_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_LOCATION_MST.ASSET_LOCATION_ID.ToString());

                        DBMethod.SaveEntity<AST_LOCATION_MST>(aST_LOCATION_MST, true);
                    }
                    else
                    {
                        aST_LOCATION_MST.CREATE_BY = this.LoggedUserName;
                        aST_LOCATION_MST.CREATED_DATE = DateTime.Today;
                        aST_LOCATION_MST.ASSET_LOCATION_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.aST_LOCATION_MSt_seq);
                        aST_LOCATION_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_LOCATION_MST.ASSET_LOCATION_ID.ToString());

                        DBMethod.SaveEntity<AST_LOCATION_MST>(aST_LOCATION_MST);
                    }

                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetLocationEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetLocationList.aspx", false);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            AST_LOCATION_MST aST_LOCATION_MST = new AST_LOCATION_MST();
            try
            {
                ErrorCollection.Clear();
                aST_LOCATION_MST.ASSET_LOCATION_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_LOCATION_MST>(aST_LOCATION_MST);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetLocationEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void SetVisibleDeleteAction()
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    ImageButton ibtnGridDelete = gvData.Rows[iLoop].FindControl("ibtnGridDelete") as ImageButton;
                    if (ibtnGridDelete != null)
                    {
                        if (((Label)gvData.Rows[iLoop].FindControl("lblASSETLOCATIONID")).Text != null)
                        {
                            if (((Label)gvData.Rows[iLoop].FindControl("lblASSETLOCATIONID")).Text != string.Empty)
                            {

                                if (int.Parse(((Label)gvData.Rows[iLoop].FindControl("lblASSETLOCATIONID")).Text) > 0)
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = false;
                                }
                                else
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}