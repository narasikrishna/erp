﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;
using FIN.BLL.GL;
using VMVServices.Web;
using VMVServices.Services.Data;


namespace FIN.Client.FA
{
    public partial class AssetEntry : PageBase
    {
        string screenMode = string.Empty;
        DataSet dsData = new DataSet();

        AST_ASSET_LOCATION_DTL AST_ASSET_LOCATION_DTL = new AST_ASSET_LOCATION_DTL();
        AST_ASSET_MST AST_ASSET_MST = new AST_ASSET_MST();
        AST_ASSET_CATEGORY_DTL AST_ASSET_CATEGORY_DTL = new AST_ASSET_CATEGORY_DTL();
        DataTable dtCategoryData = new DataTable();
        DataTable dtLocationData = new DataTable();
        DataTable dtAssetData = new DataTable();
        DataTable dtData = new DataTable();
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;
        AssetBLL assetBLL = new AssetBLL();
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            //Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                // pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                Session["SEG1"] = null;
                Session["SEG2"] = null;
                Session["SEG3"] = null;
                Session["SEG4"] = null;
                Session["SEG5"] = null;
                Session["SEG6"] = null;


                BindCurrency();
                BindBaseCurrency();
                BindExchangeType();
                BindLocation();
                BindMajorCategory();
                BindAPLiabilityAcct();
                BindDebitAssetAcct();

                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);


                DataTable dtLocation = new DataTable();
                btnPrint.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnPost.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    btnPrint.Visible = true;
                    ddlLocation.Enabled = false;
                    ddlMajorCategory.Enabled = false;
                    ddlMinorCategory.Enabled = false;


                    dtAssetData = DBMethod.ExecuteQuery(FA_SQL.GetAssetCategoriesWithData(Master.RecordID)).Tables[0];


                    using (IRepository<AST_ASSET_MST> userCtx = new DataRepository<AST_ASSET_MST>())
                    {
                        AST_ASSET_MST = userCtx.Find(r =>
                            (r.ASSET_MST_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }
                   

                    //using (IRepository<AST_ASSET_CATEGORY_DTL> ctx = new DataRepository<AST_ASSET_CATEGORY_DTL>())
                    //{
                    //    AST_ASSET_CATEGORY_DTL = ctx.Find(rn =>
                    //        (rn.ASSET_MST_ID == Master.RecordID)
                    //        ).SingleOrDefault();
                    //}
                    //using (IRepository<AST_ASSET_LOCATION_DTL> userCtxa = new DataRepository<AST_ASSET_LOCATION_DTL>())
                    //{
                    //    AST_ASSET_LOCATION_DTL = userCtxa.Find(m =>
                    //        (m.ASSET_MST_ID == Master.RecordID)
                    //        ).SingleOrDefault();
                    //}

                    if (AST_ASSET_MST != null)
                    {
                        EntityData = AST_ASSET_MST;

                        txtAssetNumber.Text = AST_ASSET_MST.ASSET_NUMBER;
                        txtManufactureDtl.Text = AST_ASSET_MST.MANUFACTURE_DTL;
                        txtAssetModel.Text = AST_ASSET_MST.ASSET_MODEL;
                        txtAssetModelAr.Text = AST_ASSET_MST.ASSET_MODEL_OL;
                        txtAssetName.Text = AST_ASSET_MST.ASSET_NAME;
                        txtAssetNameAr.Text = AST_ASSET_MST.ASSET_NAME_OL;
                        txtOriginalCost.Text = AST_ASSET_MST.ORGINAL_COST.ToString();
                        txtAssetCost.Text = AST_ASSET_MST.ASSET_COST.ToString();
                        if (AST_ASSET_MST.CURRENCY_ID != null)
                        {
                            ddlCurrency.SelectedValue = AST_ASSET_MST.CURRENCY_ID.ToString();
                        }
                        if (AST_ASSET_MST.EXCHANGE_TYPE_ID != null)
                        {
                            ddlExchangeType.SelectedValue = AST_ASSET_MST.EXCHANGE_TYPE_ID.ToString();

                        }
                        if (AST_ASSET_MST.EXCHANGE_RATE_DATE != null)
                        {
                            dtpExchangeDate.Text = DBMethod.ConvertDateToString(AST_ASSET_MST.EXCHANGE_RATE_DATE.ToString());
                        }
                        txtExchangeRate.Text = AST_ASSET_MST.EXCHANGE_RATE.ToString();
                        if (AST_ASSET_MST.BASE_CURRENCY_ID != null)
                        {
                            ddlBaseCurrecny.SelectedValue = AST_ASSET_MST.BASE_CURRENCY_ID.ToString();
                        }
                        chkDepreciable.Checked = AST_ASSET_MST.DEPRECIABLE.ToString() == "1" ? true : false;
                        if (AST_ASSET_MST.DATE_IN_SERVICE != null)
                        {
                            dtpDateInService.Text = DBMethod.ConvertDateToString(AST_ASSET_MST.DATE_IN_SERVICE.ToString());
                        }

                        txtPOReference.Text = AST_ASSET_MST.PO_REFERENCE;

                        chkAssetInUse.Checked = AST_ASSET_MST.ASSET_IN_USE.ToString() == "1" ? true : false;
                        txtWarrantyReference.Text = AST_ASSET_MST.WARRANTY_REFERENCE;
                        if (AST_ASSET_MST.WARRANTY_END_DATE != null)
                        {
                            dtpWarrantyDate.Text = DBMethod.ConvertDateToString(AST_ASSET_MST.WARRANTY_END_DATE.ToString());
                        }
                        chkActive.Checked = AST_ASSET_MST.ENABLED_FLAG.ToString() == "1" ? true : false;
                        if (AST_ASSET_MST.GLOBAL_SEGMENT_ID != null)
                        {
                            ddlGlobalSegment.SelectedValue = AST_ASSET_MST.GLOBAL_SEGMENT_ID.ToString();
                        }

                        if ( AST_ASSET_MST.EXCHANGE_RATE_DATE != null  )
                        {
                            dtpExchangeDate.Text = DBMethod.ConvertDateToString(AST_ASSET_MST.EXCHANGE_RATE_DATE.ToString());
                        }
                        ddlDrAsAcct.SelectedValue = AST_ASSET_MST.DEBIT_ASSET_ACC_NO;
                        ddlApLibAcct.SelectedValue = AST_ASSET_MST.AP_LIABILITY_ACC_NO;

                        if (AST_ASSET_MST.WORKFLOW_COMPLETION_STATUS == "1")
                        {
                            imgBtnPost.Visible = true;
                        }

                        if (AST_ASSET_MST.POSTED_FLAG == FINAppConstants.Y)
                        {
                            imgBtnPost.Visible = false;
                            btnSave.Visible = false;
                            imgBtnJVPrint.Visible = true;
                        }

                    }

                    dtCategoryData = DBMethod.ExecuteQuery(FA_SQL.BindCategoryDetail(Master.RecordID)).Tables[0];
                    if (dtCategoryData != null)
                    {
                        if (dtCategoryData.Rows.Count > 0)
                        {
                            ddlMajorCategory.SelectedValue = dtCategoryData.Rows[0]["MAJORE_CATEGORY_ID"].ToString();
                            BindMinorCategory();
                            ddlMinorCategory.SelectedValue = dtCategoryData.Rows[0][FINColumnConstants.MINOR_CATEGORY_ID].ToString();
                        }
                    }

                    dtLocation = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocationBasedAsset(Master.RecordID)).Tables[0];

                    if (dtLocation != null)
                    {
                        if (dtLocation.Rows.Count > 0)
                        {
                            BindLocation();
                            ddlLocation.SelectedValue = dtLocation.Rows[0]["CURRENT_LOCATION_ID"].ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #region Fill Dropdownlist
        private void BindCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlCurrency);
        }
        private void BindExchangeType()
        {
            Lookup_BLL.GetLookUpValues(ref ddlExchangeType, "ET");
        }
        private void BindBaseCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlBaseCurrecny);
        }
        private void BindAPLiabilityAcct()
        {
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlApLibAcct);
        }
        private void BindDebitAssetAcct()
        {
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlDrAsAcct);
        }



        private void BindMajorCategory()
        {
            dtData.Clear();
            using (IRepository<AST_MAJOR_CATEGORY_MST> userCtx = new DataRepository<AST_MAJOR_CATEGORY_MST>())
            {
                dtData = DataSetLinqOperators.CopyToDataTable<AST_MAJOR_CATEGORY_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1")));
            }
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlMajorCategory, FINColumnConstants.CATEGORY_NAME, FINColumnConstants.MAJOR_CATEGORY_ID, dtData, true, false);
                }
            }
        }
        private void BindMinorCategory()
        {
            dtData.Clear();
            ddlMinorCategory.Items.Clear();

            if (ddlMajorCategory != null)
            {
                if (ddlMajorCategory.SelectedValue != null && ddlMajorCategory.SelectedValue != string.Empty)
                {
                    using (IRepository<AST_MINOR_CATEGORY_DTL> userCtx = new DataRepository<AST_MINOR_CATEGORY_DTL>())
                    {
                        dtData = DataSetLinqOperators.CopyToDataTable<AST_MINOR_CATEGORY_DTL>(userCtx.Find(r => (r.MAJOR_CATEGORY_ID == int.Parse(ddlMajorCategory.SelectedValue.ToString()))));
                    }
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            CommonUtils.LoadDropDownList(ddlMinorCategory, FINColumnConstants.MINOR_CATEGORY_NAME, FINColumnConstants.MINOR_CATEGORY_ID, dtData, true, false);
                        }
                    }
                }

            }
        }
        protected void ddlMajorCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMinorCategory();
        }
        private void BindLocation()
        {
            dtData.Clear();
            using (IRepository<AST_LOCATION_MST> userCtx = new DataRepository<AST_LOCATION_MST>())
            {
                dtData = DataSetLinqOperators.CopyToDataTable<AST_LOCATION_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1")));
            }
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlLocation, FINColumnConstants.LOCATION_NAME, FINColumnConstants.ASSET_LOCATION_ID, dtData, true, false);
                }
            }
        }

        #endregion


        #region Insert ,Update and Delete the records

        private void AssignToBE()
        {
            try
            {

                //Assign to Asset Master BE
                if (EntityData != null)
                {
                    AST_ASSET_MST = (AST_ASSET_MST)EntityData;
                }



                AST_ASSET_MST.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                //AST_ASSET_MST.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;


                AST_ASSET_MST.ASSET_NUMBER = txtAssetNumber.Text;
                AST_ASSET_MST.MANUFACTURE_DTL = txtManufactureDtl.Text;
                AST_ASSET_MST.ASSET_MODEL = txtAssetModel.Text;
                AST_ASSET_MST.ASSET_MODEL_OL = txtAssetModelAr.Text;
                AST_ASSET_MST.ASSET_NAME = txtAssetName.Text;
                AST_ASSET_MST.ASSET_NAME_OL = txtAssetModelAr.Text;



                //save cost with precision based on the currency 
                AST_ASSET_MST.ORGINAL_COST = Math.Round(CommonUtils.ConvertStringToDecimal(txtOriginalCost.Text), Currency_BLL.GetPrecision(ddlCurrency.SelectedValue));
                AST_ASSET_MST.ASSET_COST = Math.Round(CommonUtils.ConvertStringToDecimal(txtAssetCost.Text), Currency_BLL.GetPrecision(ddlCurrency.SelectedValue));

                AST_ASSET_MST.CURRENCY_ID = (ddlCurrency.SelectedValue == string.Empty ? "0" : ddlCurrency.SelectedValue.ToString());
                AST_ASSET_MST.EXCHANGE_TYPE_ID = (ddlExchangeType.SelectedValue == string.Empty ? "0" : ddlExchangeType.SelectedValue.ToString());
                AST_ASSET_MST.BASE_CURRENCY_ID = (ddlBaseCurrecny.SelectedValue == string.Empty ? "0" : ddlBaseCurrecny.SelectedValue.ToString());

                if (dtpExchangeDate.Text != null && dtpExchangeDate.Text != string.Empty)
                {
                    AST_ASSET_MST.EXCHANGE_RATE_DATE = DBMethod.ConvertStringToDate(dtpExchangeDate.Text);
                }

                AST_ASSET_MST.EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());
                AST_ASSET_MST.DEPRECIABLE = chkDepreciable.Checked == true ? "1" : "0";

                if (dtpDateInService.Text != null && dtpDateInService.Text != string.Empty)
                {
                    AST_ASSET_MST.DATE_IN_SERVICE = DBMethod.ConvertStringToDate(dtpDateInService.Text);
                }
                AST_ASSET_MST.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;
                AST_ASSET_MST.PO_REFERENCE = txtPOReference.Text;
                AST_ASSET_MST.ASSET_IN_USE = chkAssetInUse.Checked == true ? "1" : "0";
                AST_ASSET_MST.WARRANTY_REFERENCE = txtWarrantyReference.Text;

                AST_ASSET_MST.DEBIT_ASSET_ACC_NO = ddlDrAsAcct.SelectedValue;
                AST_ASSET_MST.AP_LIABILITY_ACC_NO = ddlApLibAcct.SelectedValue;

                if (dtpWarrantyDate.Text != null && dtpWarrantyDate.Text != string.Empty)
                {
                    AST_ASSET_MST.WARRANTY_END_DATE = DBMethod.ConvertStringToDate(dtpWarrantyDate.Text);
                }

                AST_ASSET_MST.ENABLED_FLAG = chkActive.Checked == true ? "1" : "0";

                if (Session["SEG1"] != string.Empty && Session["SEG1"] !=null)
                {
                    AST_ASSET_MST.SEGMENT_ID_1 = Session["SEG1"].ToString();
                }
                if (Session["SEG2"] != string.Empty && Session["SEG2"] != null)
                {
                    AST_ASSET_MST.SEGMENT_ID_2 = Session["SEG2"].ToString();
                }
                if (Session["SEG3"] != string.Empty && Session["SEG3"] != null)
                {
                    AST_ASSET_MST.SEGMENT_ID_3 = Session["SEG3"].ToString();
                }
                if (Session["SEG4"] != string.Empty && Session["SEG4"] != null)
                {
                    AST_ASSET_MST.SEGMENT_ID_4 = Session["SEG4"].ToString();
                }
                if (Session["SEG5"] != string.Empty && Session["SEG5"] != null)
                {
                    AST_ASSET_MST.SEGMENT_ID_5 = Session["SEG5"].ToString();
                }
                if (Session["SEG6"] != string.Empty && Session["SEG6"] != null)
                {
                    AST_ASSET_MST.SEGMENT_ID_6 = Session["SEG6"].ToString();
                }            



                if (Master.Mode != null)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        AST_ASSET_MST.MODIFIED_BY = this.LoggedUserName;
                        AST_ASSET_MST.MODIFIED_DATE = DateTime.Today;

                        AST_ASSET_CATEGORY_DTL.MODIFIED_BY = this.LoggedUserName;
                        AST_ASSET_CATEGORY_DTL.MODIFIED_DATE = DateTime.Today;

                        AST_ASSET_LOCATION_DTL.MODIFIED_BY = this.LoggedUserName;
                        AST_ASSET_LOCATION_DTL.MODIFIED_DATE = DateTime.Today;

                        if (int.Parse(Master.RecordID.ToString()) > 0)
                        {
                            AST_ASSET_MST.ASSET_MST_ID = int.Parse(Master.RecordID.ToString());
                        }
                    }
                    else
                    {
                        AST_ASSET_MST.CREATE_BY = this.LoggedUserName;
                        AST_ASSET_MST.CREATED_DATE = DateTime.Today;
                        AST_ASSET_MST.ASSET_MST_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_ASSET_MST_SEQ);



                        AST_ASSET_CATEGORY_DTL.CREATE_BY = this.LoggedUserName;
                        AST_ASSET_CATEGORY_DTL.CREATED_DATE = DateTime.Today;
                        AST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_ASSET_CATEGORY_DTL_SEQ);

                        AST_ASSET_LOCATION_DTL.CREATE_BY = this.LoggedUserName;
                        AST_ASSET_LOCATION_DTL.CREATED_DATE = DateTime.Today;
                        AST_ASSET_LOCATION_DTL.ASSET_LOCATION_DTL_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_ASSET_LOCATION_DTL_SEQ);
                    }

                    AST_ASSET_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_ASSET_MST.ASSET_MST_ID.ToString());

                }

                if (AST_ASSET_MST.BASE_CURRENCY_ID == AST_ASSET_MST.CURRENCY_ID)
                {
                    AST_ASSET_MST.CONVERTED_COST = AST_ASSET_MST.ORGINAL_COST;
                }


                //Assign to Asset Category BE
                AST_ASSET_CATEGORY_DTL.MAJORE_CATEGORY_ID = int.Parse(ddlMajorCategory.SelectedValue == string.Empty ? "0" : ddlMajorCategory.SelectedValue.ToString());
                AST_ASSET_CATEGORY_DTL.MINOR_CATEGORY_ID = int.Parse(ddlMinorCategory.SelectedValue == string.Empty ? "0" : ddlMinorCategory.SelectedValue.ToString());
                AST_ASSET_CATEGORY_DTL.EFFECT_FROM = DateTime.Today;
                AST_ASSET_CATEGORY_DTL.ENABLED_FLAG = "1";


                //Assign to Asset Location BE
                AST_ASSET_LOCATION_DTL.PREVIOUS_LOCATION_ID = int.Parse(ddlLocation.SelectedValue == string.Empty ? "0" : ddlLocation.SelectedValue.ToString());
                AST_ASSET_LOCATION_DTL.EFFECT_FROM = DateTime.Today;
                AST_ASSET_LOCATION_DTL.ENABLED_FLAG = "1";
                AST_ASSET_LOCATION_DTL.CURRENT_LOCATION_ID = int.Parse(ddlLocation.SelectedValue == string.Empty ? "0" : ddlLocation.SelectedValue.ToString());

                assetBLL.aST_ASSET_MST = AST_ASSET_MST;
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetBE");
                ErrorCollection.Add("AssetBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EmptyValidation()
        {
            try
            {


                slControls[0] = txtAssetName;
                slControls[1] = txtOriginalCost;
                slControls[2] = ddlCurrency;
                //slControls[3] = ddlExchangeType;
                slControls[3] = dtpExchangeDate;
                slControls[4] = txtExchangeRate;
                //slControls[6] = ddlBaseCurrecny;
                slControls[5] = ddlMajorCategory;
                slControls[6] = ddlMinorCategory;
                slControls[7] = ddlLocation;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = "TextBox~TextBox~DropDownList~TextBox~TextBox~DropDownList~DropDownList~DropDownList";
                string strMessage = Prop_File_Data["Asset_Name_P"] + " ~ " + Prop_File_Data["Original_Cost_P"] + " ~ " + Prop_File_Data["Currency_P"] + " ~ " + Prop_File_Data["Exchange_Date_P"] + " ~ " + Prop_File_Data["Exchange_Rate_P"] + " ~ " + Prop_File_Data["Base_Currency_P"] + " ~ " + Prop_File_Data["Major_Category_P"] + " ~ " + Prop_File_Data["Minor_Category_P"] + "";
                // strMessage = "Asset Name ~ Original Cost ~Currency ~Exchange Date ~Exchange Rate ~Base Currency ~Major Category ~Minor Category";

                //strCtrlTypes = "TextBox~TextBox~DropDownList~DropDownList~DropDownList~DropDownList";
                //strMessage = "Asset Name ~ Original Cost ~Currency ~Major Category ~Minor Category~Location";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetValidation");
                ErrorCollection.Add("AssetValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                EmptyValidation();

                AssignToBE();

                assetBLL.Validate();

                if (assetBLL.ErrorCollection.Count > 0)
                {
                    ErrorCollection = assetBLL.ErrorCollection;
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            //Insert Asset
                            DBMethod.SaveEntity<AST_ASSET_MST>(AST_ASSET_MST);

                            //Insert Asset Category
                            AST_ASSET_CATEGORY_DTL.ASSET_MST_ID = AST_ASSET_MST.ASSET_MST_ID;
                            DBMethod.SaveEntity<AST_ASSET_CATEGORY_DTL>(AST_ASSET_CATEGORY_DTL);

                            //Insert Asset Location
                            AST_ASSET_LOCATION_DTL.ASSET_MST_ID = AST_ASSET_MST.ASSET_MST_ID;
                            DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL);

                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            //Update Asset
                            DBMethod.SaveEntity<AST_ASSET_MST>(AST_ASSET_MST, true);

                            ////Update Asset Category

                            //AST_ASSET_CATEGORY_DTL.ASSET_MST_ID = AST_ASSET_MST.ASSET_MST_ID;
                            //DBMethod.SaveEntity<AST_ASSET_CATEGORY_DTL>(AST_ASSET_CATEGORY_DTL, true);

                            ////Update Asset Location
                            //AST_ASSET_LOCATION_DTL.ASSET_MST_ID = AST_ASSET_MST.ASSET_MST_ID;
                            //DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL, true);

                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add(FINMessageConstatns.Asset, ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Confirm button for delete the particular records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //Delete Asset Category
                AST_ASSET_CATEGORY_DTL.ASSET_MST_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_CATEGORY_DTL>(AST_ASSET_CATEGORY_DTL);

                //Delete Asset Location
                AST_ASSET_LOCATION_DTL.ASSET_MST_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL);

                //Delete Asset
                AST_ASSET_MST.ASSET_MST_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_MST>(AST_ASSET_MST);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetList.aspx", false);
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Currency_BLL.IsBaseCurrency(ddlCurrency.SelectedValue.ToString()))
                {
                    ddlExchangeType.Enabled = false;
                    txtExchangeRate.Enabled = false;
                    dtpExchangeDate.Enabled = false;
                    ddlBaseCurrecny.SelectedValue = ddlCurrency.SelectedValue;
                    //ddlBaseCurrecny.Enabled = false;
                    txtExchangeRate.Text = "1";
                }
                else
                {
                    ddlExchangeType.Enabled = true;
                    txtExchangeRate.Enabled = true;
                    dtpExchangeDate.Enabled = true;
                    // ddlBaseCurrecny.Enabled = true;
                    txtExchangeRate.Text = string.Empty;

                    GetExchangeRate();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void GetExchangeRate()
        {
            DataTable dtData = new DataTable();

            if (ddlCurrency.SelectedValue != string.Empty & dtpExchangeDate.Text != string.Empty)
            {
                dtData = ExchangeRate_BLL.getStandardRate(ddlCurrency.SelectedValue, (dtpExchangeDate.Text));
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtExchangeRate.Text = dtData.Rows[0]["currency_std_rate"].ToString();
                    }
                }
            }
        }
        protected void dtpExchangeDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GetExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        //protected void btnSegval_Click(object sender, EventArgs e)
        //{
            
        //}


        private void BindSegment()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();


                if (ddlDrAsAcct != null)
                {
                    if (ddlDrAsAcct.SelectedValue != null)
                    {
                        dtdataAc = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentBasedAccCode(ddlDrAsAcct.SelectedValue.ToString())).Tables[0];
                        if (dtdataAc != null)
                        {
                            if (dtdataAc.Rows.Count > 0)
                            {
                                int j = dtdataAc.Rows.Count;

                                for (int i = 0; i < dtdataAc.Rows.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        //segment1
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }

                                    }
                                    if (i == 1)
                                    {
                                        //segment2
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 2)
                                    {
                                        //Segments3
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 3)
                                    {
                                        //segment4
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 4)
                                    {
                                        //segment5
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 5)
                                    {
                                        //segment6
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                        }
                                    }
                                }
                                if (j == 1)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                }
                                else if (j == 2)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                }
                                else if (j == 3)
                                {
                                    // Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, VMVServices.Web.Utils.OrganizationID);

                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                }
                                else if (j == 4)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                }
                                else if (j == 5)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlDrAsAcct.SelectedValue.ToString(), "0");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
                //throw ex;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        //protected void ddlSegment_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}


        protected void ddlSegment1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment2.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment3.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment4.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment5.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment5_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment6.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Session["SEG1"] = ddlSegment1.SelectedValue;
                Session["SEG2"] = ddlSegment2.SelectedValue;
                Session["SEG3"] = ddlSegment3.SelectedValue;
                Session["SEG4"] = ddlSegment4.SelectedValue;
                Session["SEG5"] = ddlSegment5.SelectedValue;
                Session["SEG6"] = ddlSegment6.SelectedValue;
                
                ModalPopupExtender2.Hide();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlDrAsAcct_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSegment();
            ModalPopupExtender2.Show();

           
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            { 
                imgBtnPost.Visible = false;
                if (EntityData != null)
                {
                    AST_ASSET_MST = (AST_ASSET_MST)EntityData;
                }
                if (AST_ASSET_MST.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(AST_ASSET_MST.ASSET_MST_ID.ToString(), "FA_005");
                }
                imgBtnPost.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                imgBtnJVPrint.Visible = true;
                Print_JV();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FA_Posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            Print_JV();
        }

        private void Print_JV()
        {
            AST_ASSET_MST = AssetBLL.getClassEntity(Master.RecordID.ToString());

            if (AST_ASSET_MST != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", AST_ASSET_MST.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (Master.RecordID.ToString() != string.Empty)
                {
                    htFilterParameter.Add("ASSET_MST_ID", Master.RecordID.ToString());
                }

               

                ReportData = FIN.BLL.FA.AssetBLL.GetAssetReportData();
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());             


                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;              

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FAAssetReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



    }
}