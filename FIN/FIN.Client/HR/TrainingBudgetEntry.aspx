﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingBudgetEntry.aspx.cs" Inherits="FIN.Client.HR.TrainingBudgetEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:DropDownList ID="ddlDepartment" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="1" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Program Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:DropDownList ID="ddlProgramName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="2" AutoPostBack="True">
                    <%-- <asp:ListItem Text="Select Department" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblshortName">
                Short Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShortName" CssClass="validate[required] RequiredField txtBox" runat="server" TabIndex="3"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblPreparedBy">
                Prepared By
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtPreparedBy" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFiscalYear">
                Fiscal Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlFiscalYear" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="5" AutoPostBack="True" OnSelectedIndexChanged="ddlFiscalYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="TrainingDate">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtFromDate" Enabled="false" CssClass="validate[custom[ReqDateDDMMYYY]]  txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtFromDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtToDate" Enabled="false" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="7"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtToDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblCurrency">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCurrency" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="8" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAmount" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="9"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:TextBox ID="txtRemarks" MaxLength="250" Height="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="10" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEnabledFlag">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="ChkEnabledFlag" runat="server" Checked="True" TabIndex="11" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="12"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="14"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
