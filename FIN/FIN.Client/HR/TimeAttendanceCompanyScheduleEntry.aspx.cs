﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TimeAttendanceCompanyScheduleEntry : PageBase
    {
        TM_COMP_SCH tM_COMP_SCH = new TM_COMP_SCH();
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<TM_COMP_SCH> userCtx = new DataRepository<TM_COMP_SCH>())
                    {
                        tM_COMP_SCH = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = tM_COMP_SCH;

                    if (tM_COMP_SCH.TM_START_DATE != null)
                    {
                        txtStartDate.Text = DBMethod.ConvertDateToString(tM_COMP_SCH.TM_START_DATE.ToString());
                    }
                    if (tM_COMP_SCH.TM_END_DATE != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(tM_COMP_SCH.TM_END_DATE.ToString());
                    }

                    //assign hour
                    if (tM_COMP_SCH.TM_IN_TIME != null)
                    {
                        ddl_FHour.SelectedValue = tM_COMP_SCH.TM_IN_TIME.Substring(tM_COMP_SCH.TM_IN_TIME.Length - 11, 2).Replace(" ", "0");
                    }
                    if (tM_COMP_SCH.TM_OUT_TIME != null)
                    {
                        ddl_THour.SelectedValue = tM_COMP_SCH.TM_OUT_TIME.Substring(tM_COMP_SCH.TM_OUT_TIME.Length - 11, 2).Replace(" ", "0");
                    }
                  
                    if (tM_COMP_SCH.TM_BREAK_1_START != null)
                    {
                        ddlStartTime1Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_1_START.Substring(tM_COMP_SCH.TM_BREAK_1_START.Length - 11, 2).Replace(" ", "0");
                    }
                    if (tM_COMP_SCH.TM_BREAK_1_ENDS != null)
                    {
                        ddlEndTime1Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_1_ENDS.Substring(tM_COMP_SCH.TM_BREAK_1_ENDS.Length - 11, 2).Replace(" ", "0");
                    }

                    if (tM_COMP_SCH.TM_BREAK_2_START != null)
                    {
                        ddlStartTime2Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_2_START.Substring(tM_COMP_SCH.TM_BREAK_2_START.Length - 11, 2).Replace(" ", "0");
                    }
                    if (tM_COMP_SCH.TM_BREAK_2_ENDS != null)
                    {
                        ddlEndTime2Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_2_ENDS.Substring(tM_COMP_SCH.TM_BREAK_2_ENDS.Length - 11, 2).Replace(" ", "0");
                    }

                    if (tM_COMP_SCH.TM_BREAK_3_START != null)
                    {
                        ddlStartTime3Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_3_START.Substring(tM_COMP_SCH.TM_BREAK_3_START.Length - 11, 2).Replace(" ", "0");
                    }
                    if (tM_COMP_SCH.TM_BREAK_3_ENDS != null)
                    {
                        ddlEndTime3Hr.SelectedValue = tM_COMP_SCH.TM_BREAK_3_ENDS.Substring(tM_COMP_SCH.TM_BREAK_3_ENDS.Length - 11, 2).Replace(" ", "0");
                    }


                    ///assign Minute
                    if (tM_COMP_SCH.TM_IN_TIME != null)
                    {
                        ddl_FMin.SelectedValue = tM_COMP_SCH.TM_IN_TIME.Substring(tM_COMP_SCH.TM_IN_TIME.Length -8, 2);
                    }
                    if (tM_COMP_SCH.TM_OUT_TIME != null)
                    {
                        ddl_TMin.SelectedValue = tM_COMP_SCH.TM_OUT_TIME.Substring(tM_COMP_SCH.TM_OUT_TIME.Length - 8, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_1_START != null)
                    {
                        ddlStartTime1Min.SelectedValue = tM_COMP_SCH.TM_BREAK_1_START.Substring(tM_COMP_SCH.TM_BREAK_1_START.Length - 8, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_1_ENDS != null)
                    {
                        ddlEndTime1Min.SelectedValue = tM_COMP_SCH.TM_BREAK_1_ENDS.Substring(tM_COMP_SCH.TM_BREAK_1_ENDS.Length - 8, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_2_START != null)
                    {
                        ddlStartTime2Min.SelectedValue = tM_COMP_SCH.TM_BREAK_2_START.Substring(tM_COMP_SCH.TM_BREAK_2_START.Length - 8, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_2_ENDS != null)
                    {
                        ddlEndTime2Min.SelectedValue = tM_COMP_SCH.TM_BREAK_2_ENDS.Substring(tM_COMP_SCH.TM_BREAK_2_ENDS.Length - 8, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_3_START != null)
                    {
                        ddlStartTime3Min.SelectedValue = tM_COMP_SCH.TM_BREAK_3_START.Substring(tM_COMP_SCH.TM_BREAK_3_START.Length - 8, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_3_ENDS != null)
                    {
                        ddlEndTime3Min.SelectedValue = tM_COMP_SCH.TM_BREAK_3_ENDS.Substring(tM_COMP_SCH.TM_BREAK_3_ENDS.Length - 5, 2);
                    }


                    ///assign AM or PM
                    if (tM_COMP_SCH.TM_IN_TIME != null)
                    {
                        ddl_FNoon.SelectedValue = tM_COMP_SCH.TM_IN_TIME.Substring(tM_COMP_SCH.TM_IN_TIME.Length - 2, 2);
                    }
                    if (tM_COMP_SCH.TM_OUT_TIME != null)
                    {
                        ddl_TNoon.SelectedValue = tM_COMP_SCH.TM_OUT_TIME.Substring(tM_COMP_SCH.TM_OUT_TIME.Length - 2, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_1_START != null)
                    {
                        ddlStartTime1AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_1_START.Substring(tM_COMP_SCH.TM_BREAK_1_START.Length - 2, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_1_ENDS != null)
                    {
                        ddlEndTime1AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_1_ENDS.Substring(tM_COMP_SCH.TM_BREAK_1_ENDS.Length - 2, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_2_START != null)
                    {
                        ddlStartTime2AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_1_START.Substring(tM_COMP_SCH.TM_BREAK_2_START.Length - 2, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_2_ENDS != null)
                    {
                        ddlEndTime2AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_2_ENDS.Substring(tM_COMP_SCH.TM_BREAK_2_ENDS.Length - 2, 2);
                    }

                    if (tM_COMP_SCH.TM_BREAK_3_START != null)
                    {
                        ddlStartTime3AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_3_START.Substring(tM_COMP_SCH.TM_BREAK_3_START.Length - 2, 2);
                    }
                    if (tM_COMP_SCH.TM_BREAK_3_ENDS != null)
                    {
                        ddlEndTime3AMPM.SelectedValue = tM_COMP_SCH.TM_BREAK_3_ENDS.Substring(tM_COMP_SCH.TM_BREAK_3_ENDS.Length - 2, 2);
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            //  FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlWarehouseType, "20");

        }

        private void AssignToBE(DateTime dt_from, DateTime dt_trom, DateTime dtStartTime1, DateTime dtEndTime1, DateTime dtStartTime2, DateTime dtEndTime2, DateTime dtStartTime3, DateTime dtEndTime3)
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tM_COMP_SCH = (TM_COMP_SCH)EntityData;
                }
                else
                {
                    tM_COMP_SCH = new TM_COMP_SCH();
                }

                if (txtStartDate.Text != string.Empty)
                {
                    tM_COMP_SCH.TM_START_DATE = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                }
                if (txtEndDate.Text.ToString().Length > 0)
                {
                    tM_COMP_SCH.TM_END_DATE = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                if (dt_from.ToString() != string.Empty && dt_from != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_IN_TIME = dt_from.ToString();
                }
                if (dt_trom.ToString() != string.Empty && dt_trom != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_OUT_TIME = dt_trom.ToString();
                }

                if (dtStartTime1.ToString() != string.Empty && dtStartTime1 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_1_START = dtStartTime1.ToString();
                }
                if (dtEndTime1.ToString() != string.Empty && dtEndTime1 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_1_ENDS = dtEndTime1.ToString();
                }

                if (dtStartTime2.ToString() != string.Empty && dtStartTime2 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_2_START = dtStartTime2.ToString();
                }
                if (dtEndTime2.ToString() != string.Empty && dtEndTime2 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_2_ENDS = dtEndTime2.ToString();
                }

                if (dtStartTime3.ToString() != string.Empty && dtStartTime3 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_3_START = dtStartTime3.ToString();
                }
                if (dtEndTime3.ToString() != string.Empty && dtEndTime3 != DateTime.MinValue)
                {
                    tM_COMP_SCH.TM_BREAK_3_ENDS = dtEndTime3.ToString();
                }


                //  tM_COMP_SCH.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                tM_COMP_SCH.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                tM_COMP_SCH.TM_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    tM_COMP_SCH.MODIFIED_BY = this.LoggedUserName;
                    tM_COMP_SCH.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    tM_COMP_SCH.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TM_COMP_SCH_SEQ);

                    tM_COMP_SCH.CREATED_BY = this.LoggedUserName;
                    tM_COMP_SCH.CREATED_DATE = DateTime.Today;

                }
                tM_COMP_SCH.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_COMP_SCH.PK_ID.ToString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtStartDate;
                slControls[1] = ddl_FHour;
                //slControls[2] = ddlStartTime1Hr;
                //slControls[3] = ddlEndTime1Hr;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();

                string strCtrlTypes = FINAppConstants.TEXT_BOX + '~' + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["In_Time_P"];


                //string strCtrlTypes = FINAppConstants.TEXT_BOX + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST;
                //string strMessage = Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["In_Time_P"] + " ~ " + Prop_File_Data["Start_Time1_P"] + " ~ " + Prop_File_Data["End_Time1_P"] + "";
                //string strMessage = "Start Date ~ In Time ~Start Time1~End Time1";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                if (DBMethod.ConvertStringToDate(txtStartDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()))
                {
                    ErrorCollection.Add("daterange", Prop_File_Data["daterangeTime_P"]);
                    return;
                }


                int int_fhour = Convert.ToInt16(ddl_FHour.SelectedValue.ToString());

                if (ddl_FNoon.SelectedValue == "PM" && int_fhour != 12)
                    int_fhour = int_fhour + 12;

                DateTime dt_from = DateTime.MinValue;
                DateTime dt_trom = DateTime.MinValue;
                if (ddl_THour.SelectedValue != string.Empty)
                {
                    int int_thour = Convert.ToInt16(ddl_THour.SelectedValue.ToString());
                    if (ddl_TNoon.SelectedValue == "PM" && int_thour != 12)
                        int_thour = int_thour + 12;

                    dt_from = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, int_fhour, Convert.ToInt16(ddl_FMin.SelectedValue), 0);
                    dt_trom = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, int_thour, Convert.ToInt16(ddl_TMin.SelectedValue), 0);

                    if ((dt_trom - dt_from).TotalMinutes <= 0)
                    {
                        ErrorCollection.Remove("frtime");
                        ErrorCollection.Add("frtime", "In Time must be less than the Out Time");
                        return;
                    }
                }

                int startHr1 = 0;
                if (ddlStartTime1Hr.SelectedValue != string.Empty)
                {
                    startHr1 = Convert.ToInt16(ddlStartTime1Hr.SelectedValue.ToString());
                    if (ddlStartTime1AMPM.SelectedValue == "PM" && startHr1 != 12)
                        startHr1 = startHr1 + 12;
                }

                DateTime dtStartTime1 = DateTime.MinValue;
                DateTime dtEndTime1 = DateTime.MinValue;
                if (ddlEndTime1Hr.SelectedValue != string.Empty && ddlStartTime1Hr.SelectedValue != string.Empty)
                {
                    int EndHr1 = Convert.ToInt16(ddlEndTime1Hr.SelectedValue.ToString());

                    if (ddlEndTime1AMPM.SelectedValue == "PM" && EndHr1 != 12)
                        EndHr1 = EndHr1 + 12;

                    dtStartTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr1, Convert.ToInt16(ddlStartTime1Min.SelectedValue), 0);
                    dtEndTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, EndHr1, Convert.ToInt16(ddlEndTime1Min.SelectedValue), 0);
                    //if ((dtEndTime1 - dtStartTime1).TotalMinutes <= 0)
                    //{
                    //    ErrorCollection.Remove("starttime1");
                    //    ErrorCollection.Add("starttime1", "Break1 Start must be less than the Break1 End");
                    //    return;
                    //}
                    if (ddlStartTime1Min.SelectedValue == ddlEndTime1Min.SelectedValue && ddlStartTime1Hr.SelectedValue == ddlEndTime1Hr.SelectedValue && ddlStartTime1AMPM.SelectedValue == ddlEndTime1AMPM.SelectedValue)
                    {
                        ErrorCollection.Remove("starttime1");
                        ErrorCollection.Add("starttime1", "Break1 Start should not be equal to Break1 End");
                        return;
                    }
                }


                int startHr2 = 0;
                if (ddlStartTime2Hr.SelectedValue != string.Empty)
                {
                    startHr2 = Convert.ToInt16(ddlStartTime2Hr.SelectedValue.ToString());
                    if (ddlStartTime2AMPM.SelectedValue == "PM" && startHr2 != 12)
                        startHr2 = startHr2 + 12;
                }

                DateTime dtStartTime2 = DateTime.MinValue;
                DateTime dtEndTime2 = DateTime.MinValue;
                if (ddlEndTime2Hr.SelectedValue != string.Empty && ddlStartTime2Hr.SelectedValue != string.Empty)
                {
                    int EndHr2 = Convert.ToInt16(ddlEndTime2Hr.SelectedValue.ToString());
                    if (ddlEndTime2AMPM.SelectedValue == "PM" && EndHr2 != 12)
                        EndHr2 = EndHr2 + 12;

                    dtStartTime2 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr2, Convert.ToInt16(ddlStartTime2Min.SelectedValue), 0);
                    dtEndTime2 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, EndHr2, Convert.ToInt16(ddlEndTime2Min.SelectedValue), 0);
                    //if ((dtEndTime2 - dtStartTime2).TotalMinutes <= 0)
                    //{
                    //    ErrorCollection.Remove("starttime2");
                    //    ErrorCollection.Add("starttime2", "Break2 Start must be less than the Break2 End");
                    //    return;
                    //}
                    if (ddlStartTime2Min.SelectedValue == ddlEndTime2Min.SelectedValue && ddlStartTime2Hr.SelectedValue == ddlEndTime2Hr.SelectedValue && ddlStartTime2AMPM.SelectedValue == ddlEndTime2AMPM.SelectedValue)
                    {
                        ErrorCollection.Remove("starttime2");
                        ErrorCollection.Add("starttime2", "Break2 Start should not be equal to Break2 End");
                        return;
                    }
                }

                int startHr3 = 0;
                if (ddlStartTime3Hr.SelectedValue != string.Empty)
                {
                    startHr3 = Convert.ToInt16(ddlStartTime3Hr.SelectedValue.ToString());
                    if (ddlStartTime3AMPM.SelectedValue == "PM" && startHr3 != 12)
                        startHr3 = startHr3 + 12;
                }

                DateTime dtStartTime3 = DateTime.MinValue;
                DateTime dtEndTime3 = DateTime.MinValue;
                if (ddlEndTime3Hr.SelectedValue != string.Empty && ddlStartTime3Hr.SelectedValue != string.Empty)
                {
                    int EndHr3 = Convert.ToInt16(ddlEndTime3Hr.SelectedValue.ToString());
                    if (ddlEndTime3AMPM.SelectedValue == "PM" && EndHr3 != 12)
                        EndHr3 = EndHr3 + 12;

                    dtStartTime3 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr3, Convert.ToInt16(ddlStartTime3Min.SelectedValue), 0);
                    dtEndTime3 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, EndHr3, Convert.ToInt16(ddlEndTime3Min.SelectedValue), 0);
                    //if ((dtEndTime3 - dtStartTime3).TotalMinutes <= 0)
                    //{
                    //    ErrorCollection.Remove("starttime3");
                    //    ErrorCollection.Add("starttime3", "Break3 Start must be less than the Break3 End");
                    //    return;
                    //}
                    if (ddlStartTime3Min.SelectedValue == ddlEndTime3Min.SelectedValue && ddlStartTime3Hr.SelectedValue == ddlEndTime3Hr.SelectedValue && ddlStartTime3AMPM.SelectedValue == ddlEndTime3AMPM.SelectedValue)
                    {
                        ErrorCollection.Remove("starttime3");
                        ErrorCollection.Add("starttime3", "Break3 Start should not be equal to Break3 End");
                        return;
                    }
                }


                //TimeSpan timeDiff = Convert.ToDateTime(txtOutTime.Text.ToString()) - Convert.ToDateTime(txtInTime.Text.ToString());
                //if (timeDiff.Ticks < 0)
                //{
                //    ErrorCollection.Add("timerange", Prop_File_Data["timerangeTime_P"]);

                //    return;
                //}

                //TimeSpan timeDiff1 = Convert.ToDateTime(txtEndTime1.Text.ToString()) - Convert.ToDateTime(txtStartTime1.Text.ToString());
                //TimeSpan timeDiff2 = Convert.ToDateTime(txtEndTime2.Text.ToString()) - Convert.ToDateTime(txtStartTime2.Text.ToString());
                //TimeSpan timeDiff3 = Convert.ToDateTime(txtEndTime3.Text.ToString()) - Convert.ToDateTime(txtStartTime3.Text.ToString());

                //if (timeDiff1.Ticks < 0 || timeDiff2.Ticks < 0 || timeDiff3.Ticks < 0)
                //{
                //    ErrorCollection.Add("timerange", Prop_File_Data["timerangeEndTime_P"]);

                //    return;
                //}

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                AssignToBE(dt_from, dt_trom, dtStartTime1, dtEndTime1, dtStartTime2, dtEndTime2, dtStartTime3, dtEndTime3);

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<TM_COMP_SCH>(tM_COMP_SCH);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<TM_COMP_SCH>(tM_COMP_SCH, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TM_COMP_SCH>(tM_COMP_SCH);

                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtInTime_TextChanged(object sender, EventArgs e)
        {

        }



    }
}