﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LocationEntry : PageBase
    {
        HR_LOCATIONS hR_LOCATIONS = new HR_LOCATIONS();
        HR_LOCATION_DAY_SCH hR_LOCATION_DAY_SCH = new HR_LOCATION_DAY_SCH();
        HR_LOC_WORKING_SATURDAYS hR_LOC_WORKING_SATURDAYS = new HR_LOC_WORKING_SATURDAYS();
        DataTable dtGridData = new DataTable();

        DataTable dtGridData1 = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();
                Session[FINSessionConstants.GridData] = null;
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbSiteOffice.Items.Add(new ListItem(Prop_File_Data["Site_P"], "SI"));
                    rbSiteOffice.Items.Add(new ListItem(Prop_File_Data["Office_P"], "OI"));
                }
                else
                {
                    rbSiteOffice.Items.Add(new ListItem("Site", "SI"));
                    rbSiteOffice.Items.Add(new ListItem("Office", "OI"));
                }
                rbSiteOffice.SelectedIndex = 0;

                EntityData = null;
                dtGridData = FIN.BLL.HR.Location_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);
                dtGridData1 = DBMethod.ExecuteQuery(FIN.DAL.HR.Location_DAL.fn_GetWorkingSaturdayDtl(Master.StrRecordId)).Tables[0];
                BindGrid1(dtGridData1);
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_LOCATIONS> userCtx = new DataRepository<HR_LOCATIONS>())
                    {
                        hR_LOCATIONS = userCtx.Find(r =>
                            (r.LOC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LOCATIONS;
                    txtDescription.Text = hR_LOCATIONS.LOC_DESC;
                    ddlRegion.SelectedValue = hR_LOCATIONS.LOC_REGION;
                    ddlCountry.SelectedValue = hR_LOCATIONS.LOC_COUNTRY;
                    FILLstate();
                    ddlState.SelectedValue = hR_LOCATIONS.LOC_STATE;
                    fillcity();
                    ddlCity.SelectedValue = hR_LOCATIONS.LOC_CITY;
                    txtAddress1.Text = hR_LOCATIONS.LOC_ADDRESS1;
                    txtAddress2.Text = hR_LOCATIONS.LOC_ADDRESS2;
                    txtAddress3.Text = hR_LOCATIONS.LOC_ADDRESS3;
                    //txtAddident1.Text = hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER1;
                    //txtAddident2.Text = hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER2;
                    //txtAddIdent3.Text = hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER3;
                    //txtAddIdent4.Text = hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER4;
                    //txtAddIdent5.Text = hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER5;
                    txtZipCode.Text = hR_LOCATIONS.LOC_ZIP_CODE;
                    ChkEnabledFlag.Checked = hR_LOCATIONS.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;

                    if (hR_LOCATIONS.SITE_HEAD_OFFICE != null)
                    {
                        if (hR_LOCATIONS.SITE_HEAD_OFFICE.ToString().Trim() == "SI")
                        {
                            rbSiteOffice.Items.FindByText("Site").Selected = true;
                        }
                        else
                        {
                            rbSiteOffice.Items.FindByText("Office").Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid1(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session["GridData1"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvEPA.DataSource = dt_tmp;
                gvEPA.DataBind();
                GridViewRow gvr = gvEPA.FooterRow;
                FillFooterGridCombo1(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_BG1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlDayName = tmpgvr.FindControl("ddlDayName") as DropDownList;
                DropDownList ddlSchedule = tmpgvr.FindControl("ddlSchedule") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddlDayName, "DAY_NAME");
                FIN.BLL.HR.Location_BLL.GetSchedule(ref ddlSchedule);
                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlDayName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOOKUP_ID"].ToString();
                    ddlSchedule.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["loc_sch_code"].ToString();
                }

                //CheckBox chkact = tmpgvr.FindControl("chkact") as CheckBox;
                //DataTable dtchilcat = new DataTable();
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    hR_CATEGORIES.CATEGORY_ID = dtGridData.Rows[iLoop][FINColumnConstants.CATEGORY_ID].ToString();
                //}
                //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Category_DAL.Get_Childdataof_Category(hR_CATEGORIES.CATEGORY_ID)).Tables[0];
                //if (dtchilcat.Rows.Count > 0)
                //{
                //    chkact.Enabled = false;
                //}


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo1(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlWkSchedules = tmpgvr.FindControl("ddlWkSchedules") as DropDownList;
                FIN.BLL.HR.Location_BLL.GetSchedule(ref ddlWkSchedules);

                if (gvEPA.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlWkSchedules.SelectedValue = gvEPA.DataKeys[gvEPA.EditIndex].Values["LOC_SCH_CODE"].ToString();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_FillFootGrid1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlRegion, "Loc_Region");
            Supplier_BLL.fn_getCountry(ref ddlCountry);
            //ComboFilling.fn_getDepartment(ref ddlDepartment);

            //Lookup_BLL.GetLookUpValues(ref ddlType, "VAC_TYPE");


        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLstate();
            fillcity();
        }

        private void FILLstate()
        {
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillcity();
        }
        private void fillcity()
        {
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (EntityData != null)
                {
                    hR_LOCATIONS = (HR_LOCATIONS)EntityData;
                }
                hR_LOCATIONS.LOC_DESC = txtDescription.Text;
                hR_LOCATIONS.LOC_REGION = ddlRegion.SelectedValue;
                hR_LOCATIONS.LOC_COUNTRY = ddlCountry.SelectedValue;
                hR_LOCATIONS.LOC_STATE = ddlState.SelectedValue;
                hR_LOCATIONS.LOC_CITY = ddlCity.SelectedValue;
                hR_LOCATIONS.LOC_ADDRESS1 = txtAddress1.Text;
                hR_LOCATIONS.LOC_ADDRESS2 = txtAddress2.Text;
                hR_LOCATIONS.LOC_ADDRESS3 = txtAddress3.Text;
                //hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER1 = txtAddident1.Text;
                //hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER2 = txtAddident2.Text;
                //hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER3 = txtAddIdent3.Text;
                //hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER4 = txtAddIdent4.Text;
                //hR_LOCATIONS.LOC_ADDITIONAL_IDENTIFIER5 = txtAddIdent5.Text;
                hR_LOCATIONS.LOC_ZIP_CODE = txtZipCode.Text;
                hR_LOCATIONS.ENABLED_FLAG = FINAppConstants.Y;
                hR_LOCATIONS.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_LOCATIONS.LOC_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_LOCATIONS.SITE_HEAD_OFFICE = rbSiteOffice.SelectedValue.ToString().Trim();

                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LOCATIONS.MODIFIED_BY = this.LoggedUserName;
                    hR_LOCATIONS.MODIFIED_DATE = DateTime.Today;
                    hR_LOCATIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LOCATIONS.LOC_ID);
                    DBMethod.SaveEntity<HR_LOCATIONS>(hR_LOCATIONS, true);
                }
                else
                {
                    hR_LOCATIONS.LOC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_047.ToString(), false, true);
                    hR_LOCATIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LOCATIONS.LOC_ID);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_LOCATIONS.CREATED_BY = this.LoggedUserName;
                    hR_LOCATIONS.CREATED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<HR_LOCATIONS>(hR_LOCATIONS);

                }


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_LOCATION_DAY_SCH = new HR_LOCATION_DAY_SCH();
                    if (dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_LOCATION_DAY_SCH> userCtx = new DataRepository<HR_LOCATION_DAY_SCH>())
                        {
                            hR_LOCATION_DAY_SCH = userCtx.Find(r =>
                                (r.LOC_DAY_ID == dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != "0")
                    //{
                    //    FIN.BLL.HR.Induction_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString());
                    //}


                    hR_LOCATION_DAY_SCH.LOC_DAY_NAME = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                    hR_LOCATION_DAY_SCH.LOC_SCH_CODE = dtGridData.Rows[iLoop]["LOC_SCH_CODE"].ToString();
                    if (dtGridData.Rows[iLoop]["FROM_DATE"] != DBNull.Value)
                    {
                        hR_LOCATION_DAY_SCH.FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["FROM_DATE"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["TO_DATE"] != DBNull.Value)
                    {
                        hR_LOCATION_DAY_SCH.TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["TO_DATE"].ToString());
                    }
                    else
                    {
                        hR_LOCATION_DAY_SCH.TO_DATE = null;
                    }
                    hR_LOCATION_DAY_SCH.ENABLED_FLAG = FINAppConstants.Y;

                    hR_LOCATION_DAY_SCH.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_LOCATION_DAY_SCH.LOC_ID = hR_LOCATIONS.LOC_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_LOCATION_DAY_SCH.LOC_DAY_ID = dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_LOCATION_DAY_SCH, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString() != string.Empty)
                        {
                            hR_LOCATION_DAY_SCH.LOC_DAY_ID = dtGridData.Rows[iLoop]["LOC_DAY_ID"].ToString();
                            hR_LOCATION_DAY_SCH.MODIFIED_BY = this.LoggedUserName;
                            hR_LOCATION_DAY_SCH.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_LOCATION_DAY_SCH, FINAppConstants.Update));
                            DBMethod.SaveEntity<HR_LOCATION_DAY_SCH>(hR_LOCATION_DAY_SCH, true);
                            savedBool = true;

                        }
                        else
                        {
                            hR_LOCATION_DAY_SCH.LOC_DAY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_071_S.ToString(), false, true);
                            hR_LOCATION_DAY_SCH.CREATED_BY = this.LoggedUserName;
                            hR_LOCATION_DAY_SCH.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_LOCATION_DAY_SCH, FINAppConstants.Add));
                            DBMethod.SaveEntity<HR_LOCATION_DAY_SCH>(hR_LOCATION_DAY_SCH);
                            savedBool = true;
                        }
                    }
                    if (Session["GridData1"] != null)
                    {
                        dtGridData1 = (DataTable)Session["GridData1"];
                    }

                    var tmpChildEntity1 = new List<Tuple<object, string>>();

                    for (int jLoop = 0; jLoop < dtGridData1.Rows.Count; jLoop++)
                    {
                        hR_LOC_WORKING_SATURDAYS = new HR_LOC_WORKING_SATURDAYS();
                        if (dtGridData1.Rows[jLoop]["LOC_SAT_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_LOC_WORKING_SATURDAYS> userCtx = new DataRepository<HR_LOC_WORKING_SATURDAYS>())
                            {
                                hR_LOC_WORKING_SATURDAYS = userCtx.Find(r =>
                                    (r.LOC_SAT_ID == dtGridData1.Rows[jLoop]["LOC_SAT_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }
                        if (dtGridData1.Rows[jLoop]["LOC_SAT_DT"] != DBNull.Value)
                        {
                            hR_LOC_WORKING_SATURDAYS.LOC_SAT_DT = DateTime.Parse(dtGridData1.Rows[iLoop]["LOC_SAT_DT"].ToString());
                        }

                        if (dtGridData1.Rows[jLoop]["LOC_SCH_CODE"] != DBNull.Value)
                        {
                            hR_LOC_WORKING_SATURDAYS.LOC_SCH_CODE = dtGridData1.Rows[jLoop]["LOC_SCH_CODE"].ToString();
                        }


                        if (dtGridData1.Rows[jLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                        {
                            hR_LOC_WORKING_SATURDAYS.ENABLED_FLAG = FINAppConstants.Y;
                        }
                        else
                        {
                            hR_LOC_WORKING_SATURDAYS.ENABLED_FLAG = FINAppConstants.N;
                        }


                        hR_LOC_WORKING_SATURDAYS.LOC_ID = hR_LOCATIONS.LOC_ID;

                        if (dtGridData1.Rows[jLoop]["LOC_SAT_ID"].ToString() != "0")
                        {
                            hR_LOC_WORKING_SATURDAYS.LOC_SAT_ID = dtGridData1.Rows[jLoop]["LOC_SAT_ID"].ToString();
                            hR_LOC_WORKING_SATURDAYS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LOC_WORKING_SATURDAYS.LOC_SAT_ID);
                            hR_LOC_WORKING_SATURDAYS.MODIFIED_BY = this.LoggedUserName;
                            hR_LOC_WORKING_SATURDAYS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_LOC_WORKING_SATURDAYS>(hR_LOC_WORKING_SATURDAYS, true);
                            savedBool = true;
                        }
                        else
                        {
                            hR_LOC_WORKING_SATURDAYS.LOC_SAT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_071_WS.ToString(), false, true);
                            hR_LOC_WORKING_SATURDAYS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LOC_WORKING_SATURDAYS.LOC_SAT_ID);
                            hR_LOC_WORKING_SATURDAYS.CREATED_BY = this.LoggedUserName;
                            hR_LOC_WORKING_SATURDAYS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_LOC_WORKING_SATURDAYS>(hR_LOC_WORKING_SATURDAYS);
                            savedBool = true;
                        }

                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {

                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {


                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
                //}
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        //protected void btnYes_Click(object sender, EventArgs e)
        //{


        //    try
        //    {
        //        ErrorCollection.Clear();
        //        DBMethod.DeleteEntity<HR_LOCATIONS>(hR_LOCATIONS);
        //        DisplayDeleteCompleteMessage(Master.ListPageToOpen);

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("BYC", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }

        //}




        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEPA_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                gvEPA.EditIndex = -1;
                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CNCL_EDT1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEPA_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvEPA.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl1(gvr, dtGridData1, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData1.Rows.Add(drList);
                    BindGrid1(dtGridData1);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CMD1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlDayName = gvr.FindControl("ddlDayName") as DropDownList;
            DropDownList ddlSchedule = gvr.FindControl("ddlSchedule") as DropDownList;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                //if (Master.Mode == FINAppConstants.Add)
                //{
                drList["LOC_DAY_ID"] = "0";
                // }
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }
            slControls[0] = ddlDayName;
            slControls[1] = ddlSchedule;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpEndDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Day_Name_P"] + " ~ " + Prop_File_Data["Schedule_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Start Date ~ Start Date ~ End Date ~ Probation Status";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            //string strCondition = "PROB_STATUS='" + ddlProbStatus.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}

            //if (Master.Mode == FINAppConstants.Add)
            //{
            //    ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["PROB_ID"].ToString(), ddlEmpNumber.SelectedValue);
            //    if (ProReturn != string.Empty)
            //    {
            //        if (ProReturn != "0")
            //        {
            //            ErrorCollection.Add("EMPLOYEEPROBATION", ProReturn);
            //            if (ErrorCollection.Count > 0)
            //            {
            //                return drList;
            //            }
            //        }
            //    }
            //}

            // DataTable dtchilcat = new DataTable();

            //if (gvData.EditIndex >= 0)
            //{
            //    hR_EMP_PROBATION.PROB_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PROB_ID].ToString();

            //    dtchilcat = null;
            //    //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeProbation_DAL.Get_Childdataof_Category(hR_EMP_PROBATION.PROB_ID)).Tables[0];
            //    //if (dtchilcat.Rows.Count > 0)
            //    //{
            //    //    if (chkact.Checked)
            //    //    {

            //    //    }
            //    //    else
            //    //    {
            //    //        ErrorCollection.Add("chkflg", Prop_File_Data["chkflg_P"]);
            //    //        return drList;
            //    //    }
            //    //}
            //}





            if (ddlDayName.SelectedIndex.ToString() != "0")
            {
                drList["LOOKUP_NAME"] = ddlDayName.SelectedItem.Text;
                drList["LOOKUP_ID"] = ddlDayName.SelectedItem.Value;
            }
            if (ddlSchedule.SelectedIndex.ToString() != "0")
            {
                drList["SCHEDULE_TIME"] = ddlSchedule.SelectedItem.Text;
                drList["loc_sch_code"] = ddlSchedule.SelectedItem.Value;
            }

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["FROM_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {
                drList["TO_DATE"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["TO_DATE"] = DBNull.Value;
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;
            return drList;
        }

        private DataRow AssignToGridControl1(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpwksaturdayDate = gvr.FindControl("dtpwksaturdayDate") as TextBox;
            DropDownList ddlWkSchedules = gvr.FindControl("ddlWkSchedules") as DropDownList;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData1.NewRow();
                drList["LOC_SAT_ID"] = "0";

            }
            else
            {
                drList = dtGridData1.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = dtpwksaturdayDate;
            slControls[1] = ddlWkSchedules;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~DropDownList";
            string strMessage = Prop_File_Data["Working_Saturday_P"] + " ~ " + Prop_File_Data["Schedule_P"];

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string dtSaturdayDate;
            dtSaturdayDate = DBMethod.ConvertStringToDate(dtpwksaturdayDate.Text).DayOfWeek.ToString();
            if (dtSaturdayDate != DayOfWeek.Saturday.ToString())
            {
                ErrorCollection.Add("Saturady", "Please Enter only Saturday Date");
                return drList;
            }

            if (ddlWkSchedules.SelectedIndex.ToString() != "0")
            {
                drList["SCHEDULE_TIME"] = ddlWkSchedules.SelectedItem.Text;
                drList["LOC_SCH_CODE"] = ddlWkSchedules.SelectedValue;
            }

            if (dtpwksaturdayDate.Text.ToString().Length > 0)
            {
                drList["LOC_SAT_DT"] = DBMethod.ConvertStringToDate(dtpwksaturdayDate.Text.ToString());
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            return drList;
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        protected void gvEPA_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                gvEPA.EditIndex = e.NewEditIndex;
                BindGrid1(dtGridData1);
                GridViewRow gvr = gvEPA.Rows[e.NewEditIndex];
                FillFooterGridCombo1(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_EDT1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEPA_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvEPA.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RowCreated1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvEPA_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvEPA.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl1(gvr, dtGridData1, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvEPA.EditIndex = -1;
                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_UP1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEPA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RDB1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEPA_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                DataRow drList = null;
                drList = dtGridData1.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_DEL1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_LOCATION_DAY_SCH.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_LOCATION_DAY_SCH>(hR_LOCATION_DAY_SCH);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void dtpwksaturdayDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox dtpwksaturdayDate = gvr.FindControl("dtpwksaturdayDate") as TextBox;
                string dtSaturdayDate;
                dtSaturdayDate = DBMethod.ConvertStringToDate(dtpwksaturdayDate.Text).DayOfWeek.ToString();
                if (dtSaturdayDate != DayOfWeek.Saturday.ToString())
                {
                    ErrorCollection.Add("Saturady", "Please Enter only Saturday Date");
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }











        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
