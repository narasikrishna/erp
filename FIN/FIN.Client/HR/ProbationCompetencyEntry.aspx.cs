﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ProbationCompetencyEntry : PageBase
    {
        HR_PROBATION_COMP_HDR hR_PROBATION_COMP_HDR = new HR_PROBATION_COMP_HDR();
        HR_PROBATION_COMP_DTL hR_PROBATION_COMP_DTL = new HR_PROBATION_COMP_DTL();
        DataTable dtGridData = new DataTable();

        ProbationCompetencyBLL probationCompetencyBLL = new ProbationCompetencyBLL();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                //   txtGradeName.Enabled = false;
               // FillComboBox();
                Session[FINSessionConstants.GridData] = null;

                EntityData = null;

                dtGridData = FIN.BLL.HR.ProbationCompetencyBLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_PROBATION_COMP_HDR> userCtx = new DataRepository<HR_PROBATION_COMP_HDR>())
                    {
                        hR_PROBATION_COMP_HDR = userCtx.Find(r =>
                            (r.COM_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    
                    EntityData = hR_PROBATION_COMP_HDR;

                    txtCompEntry.Text = hR_PROBATION_COMP_HDR.COM_DESC.ToString();
                  
                    if (hR_PROBATION_COMP_HDR.COM_EFFECTIVE_FROM_DT != null)
                    {
                        dtpFromDate.Text = DBMethod.ConvertDateToString(hR_PROBATION_COMP_HDR.COM_EFFECTIVE_FROM_DT.ToString());
                    }
                    
                    if (hR_PROBATION_COMP_HDR.COM_EFFECTIVE_TO_DT != null)
                    {
                        dtpToDate.Text = DBMethod.ConvertDateToString(hR_PROBATION_COMP_HDR.COM_EFFECTIVE_TO_DT.ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_PROBATION_COMP_HDR = (HR_PROBATION_COMP_HDR)EntityData;
                }

                hR_PROBATION_COMP_HDR.COM_DESC = txtCompEntry.Text.ToString();
                if (dtpFromDate.Text != string.Empty)
                {
                    hR_PROBATION_COMP_HDR.COM_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(dtpFromDate.Text.ToString());
                }


                if (dtpToDate.Text != string.Empty)
                {
                    hR_PROBATION_COMP_HDR.COM_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(dtpToDate.Text.ToString());
                }
                else
                {
                    hR_PROBATION_COMP_HDR.COM_EFFECTIVE_TO_DT = null;
                }

                hR_PROBATION_COMP_HDR.COM_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_PROBATION_COMP_HDR.ENABLED_FLAG = FINAppConstants.Y;
                //  hR_PER_APP_REVIEW_ASGNMNT_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_PROBATION_COMP_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_PROBATION_COMP_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    hR_PROBATION_COMP_HDR.CREATED_BY = this.LoggedUserName;
                    hR_PROBATION_COMP_HDR.CREATED_DATE = DateTime.Today;
                    hR_PROBATION_COMP_HDR.COM_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_091_M.ToString(), false, true);

                }

                hR_PROBATION_COMP_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PROBATION_COMP_HDR.COM_HDR_ID);
                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_PROBATION_COMP_DTL = new HR_PROBATION_COMP_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString() != "0")
                    {
                        using (IRepository<HR_PROBATION_COMP_DTL> userCtx = new DataRepository<HR_PROBATION_COMP_DTL>())
                        {
                            hR_PROBATION_COMP_DTL = userCtx.Find(r =>
                                (r.COM_LINE_ID == dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString() != "0")
                    {
                        FIN.BLL.HR.ProbationCompetencyBLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString());
                    }


                    hR_PROBATION_COMP_DTL.COM_LINE_NUM = (iLoop + 1);
                    hR_PROBATION_COMP_DTL.COM_LEVEL_DESC = dtGridData.Rows[iLoop][FINColumnConstants.COM_LEVEL_DESC].ToString();
                 //   hR_PROBATION_COMP_DTL.COM_RATING = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.COM_RATING].ToString());
                    hR_PROBATION_COMP_DTL.COM_HDR_ID = hR_PROBATION_COMP_HDR.COM_HDR_ID;

                    hR_PROBATION_COMP_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    hR_PROBATION_COMP_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_PROBATION_COMP_DTL.COM_LINE_ID = dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_COMP_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString() != string.Empty)
                        {
                            hR_PROBATION_COMP_DTL.COM_LINE_ID = dtGridData.Rows[iLoop][FINColumnConstants.COM_LINE_ID].ToString();
                            hR_PROBATION_COMP_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_PROBATION_COMP_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_COMP_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_PROBATION_COMP_DTL.COM_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_091_D.ToString(), false, true);
                            hR_PROBATION_COMP_DTL.CREATED_BY = this.LoggedUserName;
                            hR_PROBATION_COMP_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_COMP_DTL, FINAppConstants.Add));
                        }
                    }

                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_PROBATION_COMP_HDR, HR_PROBATION_COMP_DTL>(hR_PROBATION_COMP_HDR, tmpChildEntity, hR_PROBATION_COMP_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_PROBATION_COMP_HDR, HR_PROBATION_COMP_DTL>(hR_PROBATION_COMP_HDR, tmpChildEntity, hR_PROBATION_COMP_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

               // DropDownList ddlRating = tmpgvr.FindControl("ddlRating") as DropDownList;
              //  Lookup_BLL.GetLookUpValues(ref ddlRating, "RATING");
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                if (gvData.EditIndex >= 0)
                {
                   // ddlRating.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.COM_RATING].ToString();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Probation Competency ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PROBATION_COMP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNum = new TextBox(); 
            //TextBox txtLineNum = gvr.FindControl("txtLineNum") as TextBox;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;
            //DropDownList ddlRating = gvr.FindControl("ddlRating") as DropDownList;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["COM_LINE_ID"] = "0";
                txtLineNum.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
                txtLineNum.Text = (rowindex + 1).ToString();

            }


           
            slControls[0] = txtdesc;
            //slControls[1] = ddlRating;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox";
            string strMessage = Prop_File_Data["Level_Description_P"] + "";
           // string strMessage = "Level Description ~ Rating ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "COM_LEVEL_DESC='" + txtdesc.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.LevelDesc;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }
            //Duplicate Validation Through Backend Package PKG_VALIDATIONS

            //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["JOB_ID"].ToString(), ddlCategory.SelectedValue, txtcode.Text);
            //if (ProReturn != string.Empty)
            //{
            //    if (ProReturn != "0")
            //    {
            //        ErrorCollection.Add("GRADE", ProReturn);
            //        if (ErrorCollection.Count > 0)
            //        {
            //            return drList;
            //        }
            //    }
            //}



            drList["COM_LEVEL_DESC"] = txtdesc.Text;
            drList["COM_LINE_NUM"] = txtLineNum.Text;
            //drList["COM_RATING"] = ddlRating.SelectedValue;
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_PROBATION_COMP_DTL>(hR_PROBATION_COMP_DTL);
                }

                DBMethod.DeleteEntity<HR_PROBATION_COMP_HDR>(hR_PROBATION_COMP_HDR);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
    }
}