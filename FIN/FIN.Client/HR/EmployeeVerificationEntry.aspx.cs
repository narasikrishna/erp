﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeVerificationEntry : PageBase
    {
        HR_EMP_VERIFICATION_HDR hR_EMP_VERIFICATION_HDR = new HR_EMP_VERIFICATION_HDR();
        HR_EMP_VERIFICATION_DTL hR_EMP_VERIFICATION_DTL = new HR_EMP_VERIFICATION_DTL();
        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            //AccountingCalendar_BLL.GetFinancialYear(ref ddlFinancialYear);
            if (Master.Mode == FINAppConstants.Add)
            {
                EmployeeVerification_BLL.GetApplnId(ref ddlApplnId);
            }
            else
            {
                EmployeeVerification_BLL.GetApplnId_Edit(ref ddlApplnId);
            }
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "STAT");
            //  Employee_BLL.GetEmployeeName(ref ddlStaffName);
        }


        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

               

                dtGridData = DBMethod.ExecuteQuery(EmployeeVerification_DAL.GetEmployeeVerfnDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);
                hR_EMP_VERIFICATION_HDR = EmployeeVerification_BLL.getClassEntity(Master.StrRecordId);
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    EntityData = hR_EMP_VERIFICATION_HDR;

                    ddlApplnId.SelectedValue = hR_EMP_VERIFICATION_HDR.APP_ID.ToString();
                    fn_fill_applnName();
                    ddlStatus.SelectedValue = hR_EMP_VERIFICATION_HDR.VERIFY_STATUS.ToString();
                    txtRemarks.Text = hR_EMP_VERIFICATION_HDR.VERIFY_REMARKS;
                    ddlApplnId.Enabled = false;

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCategory = tmpgvr.FindControl("ddlCategory") as DropDownList;
                DropDownList ddlType = tmpgvr.FindControl("ddlType") as DropDownList;
                DropDownList ddlStatus_g = tmpgvr.FindControl("ddlStatus_g") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddlCategory, "VER_CTG");
                Lookup_BLL.GetLookUpValues(ref ddlType, "VER_TYP");
                Lookup_BLL.GetLookUpValues(ref ddlStatus_g, "STAT");

                if (gvData.EditIndex >= 0)
                {
                    ddlCategory.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VERIFICATION_CATEGORY].ToString();
                    ddlType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VERIFICATION_TYPE].ToString();
                    ddlStatus_g.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VERIFICATION_STATUS].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlApplnId;
                slControls[1] = ddlStatus;
                //slControls[3] = txtAttendanceDate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["ApplicantId_P"] + " ~ " + Prop_File_Data["Status_P"] +  "";
                //string strMessage = "ApplicantId ~ Status";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

              


                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee Verification ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_VERIFICATION_HDR = (HR_EMP_VERIFICATION_HDR)EntityData;
                }

                hR_EMP_VERIFICATION_HDR.APP_ID = ddlApplnId.SelectedValue.ToString();
                hR_EMP_VERIFICATION_HDR.VERIFY_STATUS = ddlStatus.SelectedValue.ToString();
                hR_EMP_VERIFICATION_HDR.VERIFY_REMARKS = txtRemarks.Text;



                hR_EMP_VERIFICATION_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_EMP_VERIFICATION_HDR.VERIFY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                // hR_TRM_COURSE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_VERIFICATION_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_VERIFICATION_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_VERIFICATION_HDR.VERIFY_ID = FINSP.GetSPFOR_SEQCode("HR_072_M".ToString(), false, true);
                    //hR_TRM_COURSE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_COURSE_SEQ);
                    hR_EMP_VERIFICATION_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_VERIFICATION_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_EMP_VERIFICATION_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_VERIFICATION_HDR.VERIFY_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_VERIFICATION_DTL = new HR_EMP_VERIFICATION_DTL();
                    if (dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_EMP_VERIFICATION_DTL> userCtx = new DataRepository<HR_EMP_VERIFICATION_DTL>())
                        {
                            hR_EMP_VERIFICATION_DTL = userCtx.Find(r =>
                                (r.VERIFY_DTL_ID == dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_TRM_SUBJECT.COM_LINE_NUM = (iLoop + 1);
                    hR_EMP_VERIFICATION_DTL.VERIFICATION_CATEGORY = dtGridData.Rows[iLoop]["VERIFICATION_CATEGORY"].ToString();
                    hR_EMP_VERIFICATION_DTL.VERIFICATION_TYPE = dtGridData.Rows[iLoop]["VERIFICATION_TYPE"].ToString();
                    hR_EMP_VERIFICATION_DTL.VERIFICATION_AGENCY_EMP = dtGridData.Rows[iLoop]["VERIFICATION_AGENCY_EMP"].ToString();
                    hR_EMP_VERIFICATION_DTL.VERIFICATION_STATUS = dtGridData.Rows[iLoop]["VERIFICATION_STATUS"].ToString();
                    hR_EMP_VERIFICATION_DTL.VERIFY_ID = hR_EMP_VERIFICATION_HDR.VERIFY_ID;
                    hR_EMP_VERIFICATION_DTL.ENABLED_FLAG = "1";

                    hR_EMP_VERIFICATION_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_VERIFICATION_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_TRM_SUBJECT.COM_LEVEL_DESC, hR_TRM_SUBJECT.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_EMP_VERIFICATION_DTL.VERIFY_DTL_ID = dtGridData.Rows[iLoop]["VERIFY_DTL_ID"].ToString();
                            hR_EMP_VERIFICATION_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_VERIFICATION_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_VERIFICATION_DTL, "U"));

                        }
                        else
                        {

                            hR_EMP_VERIFICATION_DTL.VERIFY_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_072_D".ToString(), false, true);
                            hR_EMP_VERIFICATION_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_VERIFICATION_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_VERIFICATION_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_VERIFICATION_HDR, HR_EMP_VERIFICATION_DTL>(hR_EMP_VERIFICATION_HDR, tmpChildEntity, hR_EMP_VERIFICATION_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_VERIFICATION_HDR, HR_EMP_VERIFICATION_DTL>(hR_EMP_VERIFICATION_HDR, tmpChildEntity, hR_EMP_VERIFICATION_DTL, true);
                            savedBool = true;
                            break;

                        }
                }

                if (ddlStatus.SelectedValue == "Completed")
                {
                    DBMethod.ExecuteNonQuery("Update HR_APPLICANTS SET STATUS='Verified' WHERE APP_ID='" + ddlApplnId.SelectedValue+ "'");
                }

                if (hR_EMP_VERIFICATION_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.APPLICANT_VERIFICATION_ALERT);
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COurs_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_EMP_VERIFICATION_DTL>(hR_EMP_VERIFICATION_DTL);
                }

                DBMethod.DeleteEntity<HR_EMP_VERIFICATION_HDR>(hR_EMP_VERIFICATION_HDR);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        protected void ddlApplnId_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_applnName();
        }

        private void fn_fill_applnName()
        {
            DataTable dt_appln_name = new DataTable();
            dt_appln_name = DBMethod.ExecuteQuery(EmployeeVerification_DAL.GetApplicantName(ddlApplnId.SelectedValue.ToString())).Tables[0];
            if (dt_appln_name != null)
            {
                if (dt_appln_name.Rows.Count > 0)
                {
                    txtApplicantName.Text = dt_appln_name.Rows[0][0].ToString();
                }

            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlCategory = gvr.FindControl("ddlCategory") as DropDownList;
            DropDownList ddlType = gvr.FindControl("ddlType") as DropDownList;
            TextBox txtAgency_EmployeeName = gvr.FindControl("txtAgency_EmployeeName") as TextBox;
            DropDownList ddlStatus_g = gvr.FindControl("ddlStatus_g") as DropDownList;
            

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["VERIFY_DTL_ID"] = "0";
                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlCategory;
            slControls[1] = ddlType;
            slControls[2] = txtAgency_EmployeeName;
            slControls[3] = ddlStatus_g;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~TextBox~DropDownList";
            string strMessage = Prop_File_Data["Verification_Category_P"] + " ~ " + Prop_File_Data["Verification_Type_P"] + " ~ " + Prop_File_Data["Agency/Employee_Name_P"] + " ~ " + Prop_File_Data["Status_P"] + "";
            //string strMessage = " Verification Category ~ Verification Type ~ Agency/Employee Name ~ Status";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "VERIFICATION_CATEGORY_ID='" + ddlCategory.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }



            drList["VERIFICATION_CATEGORY_ID"] = ddlCategory.SelectedValue;
            drList["VERIFICATION_CATEGORY"] = ddlCategory.SelectedItem.Text;
            drList["VERIFICATION_TYPE_ID"] = ddlType.SelectedValue;
            drList["VERIFICATION_TYPE"] = ddlType.SelectedItem.Text;
            drList["VERIFICATION_STATUS_ID"] = ddlStatus_g.SelectedValue;
            drList["VERIFICATION_STATUS"] = ddlStatus_g.SelectedItem.Text;
            drList["VERIFICATION_AGENCY_EMP"] = txtAgency_EmployeeName.Text;



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}