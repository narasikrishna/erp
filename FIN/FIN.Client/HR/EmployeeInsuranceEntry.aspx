﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeInsuranceEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeInsuranceEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 950px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblTermName">
                Financial Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlfinyear" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2" runat="server">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddldept" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" TabIndex="2" runat="server" OnSelectedIndexChanged="ddldept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                DataKeyNames="PLAN_ENTRY_DTL_ID,ELIGIBLE_PLAN_ID,SELECTED_PLAN_ID,DELETED,EMP_ID"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating">
                <Columns>
                    <asp:BoundField DataField="EMP_NO" HeaderText="Employee No" ItemStyle-Width="150px">
                        <ItemStyle Width="150px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="EMP_NAME" HeaderText="Employee Name" ItemStyle-Width="150px">
                        <ItemStyle Width="150px"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Eligible Plan">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddleligplan" Width="100%" runat="server" CssClass="RequiredField ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddleligplan_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Selected Plan">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlselplan" Width="100%" runat="server" CssClass="RequiredField ddlStype"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlselplan_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Cost">
                        <ItemTemplate>
                            <asp:TextBox ID="txttotalcost" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("TOTAL_COST") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txttotalcost" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Company Cost">
                        <ItemTemplate>
                            <asp:TextBox ID="txtcomcost" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("COMPANY_COST") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txtcomcost" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Cost">
                        <ItemTemplate>
                            <asp:TextBox ID="txtempcost" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("EMP_COST") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txtempcost" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Family">
                        <ItemTemplate>
                            <asp:TextBox ID="txtfamily" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("FAMILY_MEMBER") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txtfamily" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Amt">
                        <ItemTemplate>
                            <asp:TextBox ID="txttotamt" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("TOTAL_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txttotamt" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="6" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
