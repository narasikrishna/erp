﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingInfoDetailEntry.aspx.cs" Inherits="FIN.Client.HR.TrainingInfoDetailEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFromDate">
                Id
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtId" TabIndex="2" Width="250px" MaxLength="50" runat="server"
                    CssClass="RequiredField txtBox"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblFinancialYear">
                Program Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
             <asp:DropDownList ID="ddlProgram" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="250px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="TextBox1" TabIndex="2" Width="250px" MaxLength="50" runat="server"
                    CssClass="RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartment">
                Start Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtStartDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender3"
                        runat="server" Format="dd/MM/yyyy" TargetControlID="txtAttendanceDate">
                    </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtAttendanceDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblAttendanceDate">
                End Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtAttendanceDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="4"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender2"
                        runat="server" Format="dd/MM/yyyy" TargetControlID="txtAttendanceDate">
                    </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtAttendanceDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="STATT_ID,LEAVE_REQ_ID,EMP_ID,ATTENDANCE_TYPE" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Registration Id">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlReg" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlReg" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStaffName" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStaff" OnSelectedIndexChanged="ddlRequest_SelectedIndexChanged" Width="150px"
                                AutoPostBack="true" TabIndex="6" runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlStaff" OnSelectedIndexChanged="ddlRequest_SelectedIndexChanged" Width="150px"
                                AutoPostBack="true" TabIndex="6" runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSAttendanceType" Width="130px" runat="server" Text='<%# Eval("ATTENDANCE_TYPE_name") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReason" TabIndex="7" Width="130px" MaxLength="500" runat="server"
                                CssClass="RequiredField txtBox" Text='<%# Eval("REASON") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtReason" TabIndex="7" Width="130px" MaxLength="500" runat="server"
                                CssClass=" RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReason" Width="130px" runat="server" Text='<%# Eval("REASON") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Registration Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRegisterDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                runat="server" TabIndex="8"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender2"
                                    runat="server" Format="dd/MM/yyyy" TargetControlID="txtAttendanceDate">
                                </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtRegisterDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRegisterDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                                runat="server" TabIndex="8"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender2"
                                    runat="server" Format="dd/MM/yyyy" TargetControlID="txtRegisterDate">
                                </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtRegisterDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReason" Width="130px" runat="server" Text='<%# Eval("REASON") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="11" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="12" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/StaffAttendance.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
