﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class PayrollDistributionEntry : PageBase
    {

        HR_EMP_PAY_DISTRIBUTION_HDR hR_EMP_PAY_DISTRIBUTION_HDR = new HR_EMP_PAY_DISTRIBUTION_HDR();
        HR_EMP_PAY_DISTRIBUTION_DTL hR_EMP_PAY_DISTRIBUTION_DTL = new HR_EMP_PAY_DISTRIBUTION_DTL();
        string ProReturn = null;
        int totalPercentage = 0;
        Competency_BLL Competency_BLL = new Competency_BLL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeId(ref ddlEmpName);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.Competency_BLL.getpayrollDistributionDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_PAY_DISTRIBUTION_HDR> userCtx = new DataRepository<HR_EMP_PAY_DISTRIBUTION_HDR>())
                    {
                        hR_EMP_PAY_DISTRIBUTION_HDR = userCtx.Find(r =>
                            (r.DIS_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_PAY_DISTRIBUTION_HDR;

                    ddlEmpName.SelectedValue = hR_EMP_PAY_DISTRIBUTION_HDR.EMP_ID;

                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(hR_EMP_PAY_DISTRIBUTION_HDR.EFFECTIVE_FROM_DT.ToString());
                    if (hR_EMP_PAY_DISTRIBUTION_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(hR_EMP_PAY_DISTRIBUTION_HDR.EFFECTIVE_TO_DT.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_PAY_DISTRIBUTION_HDR = (HR_EMP_PAY_DISTRIBUTION_HDR)EntityData;
                }


                hR_EMP_PAY_DISTRIBUTION_HDR.EMP_ID = ddlEmpName.SelectedValue;
                hR_EMP_PAY_DISTRIBUTION_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text != string.Empty)
                {
                    hR_EMP_PAY_DISTRIBUTION_HDR.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                hR_EMP_PAY_DISTRIBUTION_HDR.ENABLED_FLAG = chKEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_EMP_PAY_DISTRIBUTION_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                // hR_COMPETENCY_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_PAY_DISTRIBUTION_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_PAY_DISTRIBUTION_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_PAY_DISTRIBUTION_HDR.DIS_ID = FINSP.GetSPFOR_SEQCode("PR_024".ToString(), false, true);
                    //hR_EMP_PAY_DISTRIBUTION_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.pay_distribution_hdr_seq);
                    hR_EMP_PAY_DISTRIBUTION_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_PAY_DISTRIBUTION_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_EMP_PAY_DISTRIBUTION_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PAY_DISTRIBUTION_HDR.DIS_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_PAY_DISTRIBUTION_DTL = new HR_EMP_PAY_DISTRIBUTION_DTL();
                    if (dtGridData.Rows[iLoop]["DIS_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["DIS_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_EMP_PAY_DISTRIBUTION_DTL> userCtx = new DataRepository<HR_EMP_PAY_DISTRIBUTION_DTL>())
                        {
                            hR_EMP_PAY_DISTRIBUTION_DTL = userCtx.Find(r =>
                                (r.DIS_DTL_ID == dtGridData.Rows[iLoop]["DIS_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    // hR_COMPETENCY_DTL.COM_LINE_NUM = int.Parse(dtGridData.Rows[iLoop]["COM_LINE_NUM"].ToString());
                    //hR_EMP_PAY_DISTRIBUTION_DTL.DIS_DTL_ID = (iLoop + 1);
                    hR_EMP_PAY_DISTRIBUTION_DTL.SEG_ID = dtGridData.Rows[iLoop]["segment_value_id"].ToString();
                    hR_EMP_PAY_DISTRIBUTION_DTL.PERCENTAGE = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["PERCENTAGE"].ToString());

                    hR_EMP_PAY_DISTRIBUTION_DTL.DIS_ID = hR_EMP_PAY_DISTRIBUTION_HDR.DIS_ID;
                    //hR_EMP_PAY_DISTRIBUTION_DTL.CHILD_ID = hR_EMP_PAY_DISTRIBUTION_HDR.PK_ID;
                    hR_EMP_PAY_DISTRIBUTION_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_PAY_DISTRIBUTION_DTL.ENABLED_FLAG = FINAppConstants.Y;
                   

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_PAY_DISTRIBUTION_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_EMP_PAY_DISTRIBUTION_DTL.COM_LEVEL_DESC, hR_COMPETENCY_DTL.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["DIS_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["DIS_DTL_ID"].ToString() != string.Empty)
                        {
                            // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            hR_EMP_PAY_DISTRIBUTION_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_PAY_DISTRIBUTION_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_PAY_DISTRIBUTION_DTL, "U"));

                        }
                        else
                        {

                            hR_EMP_PAY_DISTRIBUTION_DTL.DIS_DTL_ID = FINSP.GetSPFOR_SEQCode("PR_024_D".ToString(), false, true);
                            hR_EMP_PAY_DISTRIBUTION_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_PAY_DISTRIBUTION_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_PAY_DISTRIBUTION_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_PAY_DISTRIBUTION_HDR, HR_EMP_PAY_DISTRIBUTION_DTL>(hR_EMP_PAY_DISTRIBUTION_HDR, tmpChildEntity, hR_EMP_PAY_DISTRIBUTION_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_PAY_DISTRIBUTION_HDR, HR_EMP_PAY_DISTRIBUTION_DTL>(hR_EMP_PAY_DISTRIBUTION_HDR, tmpChildEntity, hR_EMP_PAY_DISTRIBUTION_DTL, true);
                            saveBool = true;
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlPropertyName = tmpgvr.FindControl("ddlPropertyName") as DropDownList;
                FIN.BLL.GL.Segments_BLL.GetGlobalCostCentreSegmentvalues(ref ddlPropertyName);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlPropertyName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["segment_value_id"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (total.Value != "100")
                {
                    ErrorCollection.Add("Percent", "Total Percentage should be hundred");
                    return;
                }
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Distribution ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                totalPercentage = CommonUtils.ConvertStringToInt(CalculateTotalAmount(dtData, "PERCENTAGE"));
                total.Value = totalPercentage.ToString();
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("COM_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();



            DropDownList ddlPropertyName = gvr.FindControl("ddlPropertyName") as DropDownList;
            TextBox txtPercentage = gvr.FindControl("txtPercentage") as TextBox;
           

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["DIS_DTL_ID"] = "0";
                
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlPropertyName;
            slControls[1] = txtPercentage;
           

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox";

            string strMessage = "Property Name~ Percentage";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}




            drList["segment_value_id"] = ddlPropertyName.SelectedValue;
            drList["SEGMENT_VALUE"] = ddlPropertyName.SelectedItem.Text;
            drList["PERCENTAGE"] = txtPercentage.Text;
           



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        public string CalculateTotalAmount(DataTable dtGridData, string ColumnName, string extraCondition = "")
        {
            //dtGridData.Compute("sum(ColumnName)", "DELETED='N'").ToString()
            string deletedValue = "N";
            string str_Condition = "0";
            double totInvItemAmt = 0;

            if (dtGridData.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    totInvItemAmt += CommonUtils.ConvertStringToDouble(dtGridData.Rows[iLoop]["PERCENTAGE"].ToString());

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == "1")
                    {
                        totInvItemAmt -= CommonUtils.ConvertStringToDouble(dtGridData.Rows[iLoop]["PERCENTAGE"].ToString());
                    }

                    //if (dtGridData.Rows[iLoop]["INV_LINE_ID"].ToString() == "" || dtGridData.Rows[iLoop]["INV_LINE_ID"].ToString() == "0")
                    //{
                    //    deletedValue = dtGridData.Rows[iLoop]["DELETED"].ToString();
                    //}
                }
                str_Condition = "DELETED='" + deletedValue + "'";
            }
            if (extraCondition.Length > 0)
            {
                str_Condition += " and " + extraCondition;
            }

            return totInvItemAmt.ToString();

            //return dtGridData.Compute("sum(" + ColumnName + ")", str_Condition).ToString();
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_EMP_PAY_DISTRIBUTION_HDR>(hR_EMP_PAY_DISTRIBUTION_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("com_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }









    }
}