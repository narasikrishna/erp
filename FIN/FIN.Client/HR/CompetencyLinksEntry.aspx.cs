﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class CompetencyLinksEntry : PageBase
    {

        HR_COMPETANCY_LINKS_HDR hR_COMPETANCY_LINKS_HDR = new HR_COMPETANCY_LINKS_HDR();
        HR_COMPETANCY_LINKS_DTL hR_COMPETANCY_LINKS_DTL = new HR_COMPETANCY_LINKS_DTL();

        CompetencyLinks_BLL CompetencyLinks_BLL = new CompetencyLinks_BLL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            CompetencyLinks_BLL.fn_GetLookUpValues(ref ddlEntityType, "ETY");
            CompetencyLinks_BLL.fn_getcompetencyname(ref ddlCompetencyName);


        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                txtCompetencyType.Enabled = false;

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.CompetencyLinks_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_COMPETANCY_LINKS_HDR> userCtx = new DataRepository<HR_COMPETANCY_LINKS_HDR>())
                    {
                        hR_COMPETANCY_LINKS_HDR = userCtx.Find(r =>
                            (r.COM_LINK_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_COMPETANCY_LINKS_HDR;

                    ddlEntityType.SelectedValue = hR_COMPETANCY_LINKS_HDR.COM_ENTITY_TYPE;
                    FillEntityName();
                    ddlentityname.SelectedValue = hR_COMPETANCY_LINKS_HDR.COM_ENTITY_ID;
                    //   txtCompetencyType.Text = hR_COMPETANCY_LINKS_HDR.COM_ENTITY_ID;
                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_FROM_DT.ToString());
                    if (hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_TO_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_TO_DT.ToString());
                    }
                    ddlCompetencyName.SelectedValue = hR_COMPETANCY_LINKS_HDR.COM_HDR_ID;
                    fillcom_type();
                    //FillCompetency();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_COMPETANCY_LINKS_HDR = (HR_COMPETANCY_LINKS_HDR)EntityData;
                }


                hR_COMPETANCY_LINKS_HDR.COM_ENTITY_TYPE = ddlEntityType.SelectedValue;
                hR_COMPETANCY_LINKS_HDR.COM_ENTITY_ID = ddlentityname.SelectedValue;
                hR_COMPETANCY_LINKS_HDR.COM_HDR_ID = ddlCompetencyName.SelectedValue;
                hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text != string.Empty)
                {
                    hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    hR_COMPETANCY_LINKS_HDR.COM_EFFECTIVE_TO_DT = null;
                }
                hR_COMPETANCY_LINKS_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_COMPETANCY_LINKS_HDR.ENABLED_FLAG = FINAppConstants.Y;


                // hR_COMPETANCY_LINKS_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_COMPETANCY_LINKS_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_COMPETANCY_LINKS_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_COMPETANCY_LINKS_HDR.COM_LINK_ID = FINSP.GetSPFOR_SEQCode("HR_007_M".ToString(), false, true);
                    hR_COMPETANCY_LINKS_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_COMPETANCY_LINKS_HDR_SEQ);
                    hR_COMPETANCY_LINKS_HDR.CREATED_BY = this.LoggedUserName;
                    hR_COMPETANCY_LINKS_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_COMPETANCY_LINKS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_COMPETANCY_LINKS_HDR.COM_LINK_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count == 0)
                {
                    ErrorCollection.Add("levelerror", "Level details cannot be empty");
                    return;
                }

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_COMPETANCY_LINKS_DTL hR_COMPETANCY_LINKS_DTL = new HR_COMPETANCY_LINKS_DTL();

                        if (gvData.DataKeys[iLoop].Values["COM_LINK_DTL_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_COMPETANCY_LINKS_DTL> userCtx = new DataRepository<HR_COMPETANCY_LINKS_DTL>())
                            {
                                hR_COMPETANCY_LINKS_DTL = userCtx.Find(r =>
                                    (r.COM_LINK_DTL_ID == gvData.DataKeys[iLoop].Values["COM_LINK_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtremarks = (TextBox)gvData.Rows[iLoop].FindControl("txtremarks");
                        TextBox txtrate = (TextBox)gvData.Rows[iLoop].FindControl("txtrate");
                        CheckBox chkact = (CheckBox)gvData.Rows[iLoop].FindControl("chkact");

                        hR_COMPETANCY_LINKS_DTL.COM_LINE_ID = gvData.DataKeys[iLoop].Values["COM_LINE_ID"].ToString();
                        hR_COMPETANCY_LINKS_DTL.COM_REMARKS = txtremarks.Text;
                        hR_COMPETANCY_LINKS_DTL.COM_RATE = int.Parse(txtrate.Text);
                        hR_COMPETANCY_LINKS_DTL.COM_LINK_ID = hR_COMPETANCY_LINKS_HDR.COM_LINK_ID;
                        hR_COMPETANCY_LINKS_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                        if (chkact.Checked == true)
                        {
                            hR_COMPETANCY_LINKS_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        }
                        else
                        {
                            hR_COMPETANCY_LINKS_DTL.ENABLED_FLAG = FINAppConstants.N;
                        }

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(hR_COMPETANCY_LINKS_DTL, "D"));
                        }
                        else
                        {
                            if (dtGridData.Rows[iLoop]["COM_LINK_DTL_ID"].ToString() != "0")
                            {
                                hR_COMPETANCY_LINKS_DTL.COM_LINK_DTL_ID = dtGridData.Rows[iLoop]["COM_LINK_DTL_ID"].ToString();
                                hR_COMPETANCY_LINKS_DTL.MODIFIED_BY = this.LoggedUserName;
                                hR_COMPETANCY_LINKS_DTL.MODIFIED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_COMPETANCY_LINKS_DTL, "U"));

                            }
                            else
                            {

                                hR_COMPETANCY_LINKS_DTL.COM_LINK_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_007_D".ToString(), false, true);
                                hR_COMPETANCY_LINKS_DTL.CREATED_BY = this.LoggedUserName;
                                hR_COMPETANCY_LINKS_DTL.CREATED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_COMPETANCY_LINKS_DTL, "A"));
                            }
                        }

                    }


                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_COMPETANCY_LINKS_HDR, HR_COMPETANCY_LINKS_DTL>(hR_COMPETANCY_LINKS_HDR, tmpChildEntity, hR_COMPETANCY_LINKS_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_COMPETANCY_LINKS_HDR, HR_COMPETANCY_LINKS_DTL>(hR_COMPETANCY_LINKS_HDR, tmpChildEntity, hR_COMPETANCY_LINKS_DTL, true);
                            saveBool = true;
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtremarks = gvr.FindControl("txtremarks") as TextBox;
            TextBox txtrate = gvr.FindControl("txtrate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["COM_LINK_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtremarks;
            slControls[1] = txtrate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox";
            string strMessage = Prop_File_Data["Remark_P"] + " ~ " + Prop_File_Data["Rate_P"] + "";
            //string strMessage = "Remark ~ Rate";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}



            drList["COM_REMARKS"] = txtremarks.Text;

            drList["COM_RATE"] = decimal.Parse(txtrate.Text);

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }






        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_COMPETANCY_LINKS_HDR>(hR_COMPETANCY_LINKS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("cML_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlName_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillcom_type();

            FillCompetency();

        }

        private void fillcom_type()
        {
            DataTable dtcompetencytyp = new DataTable();
            dtcompetencytyp = DBMethod.ExecuteQuery(FIN.DAL.HR.CompetencyLinks_DAL.GetCompetencytyp(ddlCompetencyName.SelectedValue)).Tables[0];
            txtCompetencyType.Text = dtcompetencytyp.Rows[0]["COM_TYPE"].ToString();
        }


        private void FillCompetency()
        {



            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.CompetencyLinks_DAL.GetCompetency(ddlCompetencyName.SelectedValue)).Tables[0];
            dtGridData.Columns.Add("DELETED");
            BindGrid(dtGridData);


        }

        protected void ddlEntityType_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillEntityName();

        }

        private void FillEntityName()
        {
            if (ddlEntityType.SelectedItem.Value == "Department")
            {
                CompetencyLinks_BLL.fn_GetDepartmentDetails(ref ddlentityname);
            }
            if (ddlEntityType.SelectedItem.Value == "Jobs")
            {
                CompetencyLinks_BLL.fn_GetJobname(ref ddlentityname);
            }
            if (ddlEntityType.SelectedItem.Value == "Position")
            {
                CompetencyLinks_BLL.fn_GetPositionName(ref ddlentityname, Master.Mode);
            }
        }

        protected void txtEffectiveDate_TextChanged(object sender, EventArgs e)
        {

        }

    }
}