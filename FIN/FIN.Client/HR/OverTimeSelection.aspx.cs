﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.HR
{
    public partial class OverTimeSelection : PageBase
    {
        TM_EMP_OVERTIME tM_EMP_OVERTIME = new TM_EMP_OVERTIME();
        //TMP_TM_EMP_OVERTIME tMP_TM_EMP_OVERTIME = new TMP_TM_EMP_OVERTIME();


        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            //Interview_BLL.fn_GetVacancyNM(ref ddlVacancy);

            //dtData = DBMethod.ExecuteQuery(Supplier_DAL.getCountry()).Tables[0];
            //chkDropNation.DataSource = dtData;
            //chkDropNation.DataTextField = "NATIONALITY";
            //chkDropNation.DataValueField = "COUNTRY_CODE";
            //chkDropNation.DataBind();
        }


        private void FillLeaveRequest(GridViewRow gvr)
        {
            // GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;

            if (ddlStaff.SelectedValue != "")
            {
                LeaveApplication_BLL.GetLeaveApplication(ref ddlRequest, ddlStaff.SelectedValue.ToString());


            }

            if (ddlAttendanceType.SelectedItem.Text == "Leave")
            {
                ddlRequest.Enabled = true;
            }
            else
            {
                ddlRequest.Enabled = false;
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session["ParentGrid"] = null;
                btnSave.Visible = false;

                FillComboBox();
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    //tMP_TM_EMP_OVERTIME = OverTimeSelection_BLL.getClassEntity(Master.StrRecordId);

                    //EntityData = tMP_TM_EMP_OVERTIME;
                    //ddlPayrollNumber.SelectedValue = pAY_DEDUCTION_DTLS.
                    //ddlDeductionPartyNumber.SelectedValue = pAY_DEDUCTION_DTLS.DED_PARTY_CODE.ToString();
                    //ddlEmployeeNo.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_EMP_ID.ToString();
                    //fn_fill_employeeName();
                    //ddlGroupCode.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_GROUP_ID.ToString();
                    //fn_fillGroupName();


                }


                //dtGridData = DBMethod.ExecuteQuery(ShortList_DAL.GetShortListData(Master.StrRecordId)).Tables[0];
                //BindGrid(dtGridData);


                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    //tM_EMP_OVERTIME = ShortList_BLL.getClassEntity(Master.StrRecordId);

                //    //EntityData = tM_EMP_OVERTIME;
                //    btnget.Enabled = false;

                //    //ddlVacancy.SelectedValue = HR_SHORT_LIST_HDR.VACANCY_ID.ToString();
                //    //if (tM_EMP_OVERTIME.OVERTIME_DATE != null)
                //    //{
                //    //    txtFromDate.Text = DBMethod.ConvertDateToString(tM_EMP_OVERTIME.OVERTIME_DATE.ToString());
                //    //}

                //    //if (HR_SHORT_LIST_HDR.NATIONALITY_ID != null)
                //    //{
                //    //    chkDropNation.SelectedValue = HR_SHORT_LIST_HDR.NATIONALITY_ID;
                //    //}

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlStaff = tmpgvr.FindControl("ddlStaff") as DropDownList;
                //DropDownList ddlAttendanceType = tmpgvr.FindControl("ddlAttendanceType") as DropDownList;
                //DropDownList ddlRequest = tmpgvr.FindControl("ddlRequest") as DropDownList;

                //Employee_BLL.GetEmplName(ref ddlStaff,ddlDepartment.SelectedValue);
                //Lookup_BLL.GetLookUpValues(ref ddlAttendanceType, "ATY");

                //if (gvData.EditIndex >= 0)
                //{
                //    ddlStaff.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.EMP_ID].ToString();
                //    ddlAttendanceType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ATTENDANCE_TYPE].ToString();

                //    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //    {
                //        FillLeaveRequest(tmpgvr);
                //    }
                //    ddlRequest.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_REQ_ID].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete

        //protected void btnProcess_Click(object sender, EventArgs e)
        //{

        //}
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();



                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        protected void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            // GridView gvr = (GridView)sender;
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            CheckBox chkSelection = ((CheckBox)gvr.FindControl("chkSelection"));
            TextBox txtPaidAmount = ((TextBox)gvr.FindControl("txtPaidAmount"));

            if (chkSelection.Checked == true)
            {
                txtPaidAmount.Enabled = false;

            }
            else
            {
                txtPaidAmount.Enabled = true;
                txtPaidAmount.Text = string.Empty;

            }
        }
        protected void btnDetails_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Show();
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            string empId = gvParentData.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString();
            string groupCode = gvParentData.DataKeys[gvrP.RowIndex].Values["OVERTIME_EMP_GROUP"].ToString();


            DataTable dtTemp = new DataTable();
            try
            {
                ErrorCollection.Clear();
                DataTable dt_Overtime = new DataTable();
                DataTable dtEmp = new DataTable();
                DataRow drRow = null;

                DateTime dtFrom = DBMethod.ConvertStringToDate(txtFromDate.Text);
                DateTime dtTo = DBMethod.ConvertStringToDate(txtToDate.Text);
                DateTime dtCurrent = dtFrom;

                dt_Overtime = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimedtls("0", txtFromDate.Text, txtToDate.Text)).Tables[0];
                dtTemp = dt_Overtime.Clone();

                for (int iLoop = 0; iLoop <= (dtTo - dtFrom).Days; iLoop++)
                {
                    dt_Overtime = FIN.DAL.HR.OverTimeSelection_DAL.GetSP_OverTime(groupCode, empId, dtCurrent.ToShortDateString(), FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimedtls(empId, dtCurrent.ToShortDateString(), txtToDate.Text)).Tables[0];
                    if (dt_Overtime.Rows.Count > 0)
                    {
                        dt_Overtime.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OVERTIME_PAID_AMOUNT", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("OVERTIME_PAID_AMOUNT"))));
                        dt_Overtime.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OVERTIME_AMOUNT", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("OVERTIME_AMOUNT"))));
                        dt_Overtime.AcceptChanges();

                        drRow = dt_Overtime.Rows[0];
                        dtTemp.Rows.Add(drRow.ItemArray);
                        dtTemp.AcceptChanges();
                    }

                    dtCurrent = dtCurrent.AddDays(1);
                }


                Session[FINSessionConstants.GridData] = dtTemp;
                gvDataChild.DataSource = dtTemp;
                gvDataChild.DataBind();


                if (dtTemp.Rows.Count == 0)
                {
                    lblErrorMsg.Visible = true;

                }
                else
                {
                    lblErrorMsg.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    tM_EMP_OVERTIME = (TM_EMP_OVERTIME)EntityData;
                }


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                int selectedCount = 0;

                //if (gvData.Rows.Count > 0)
                //{
                //    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                //    {
                //        tM_EMP_OVERTIME = new TM_EMP_OVERTIME();

                //        if (gvData.DataKeys[iLoop].Values["overtime_id"].ToString() != string.Empty && gvData.DataKeys[iLoop].Values["overtime_id"].ToString() != "0")
                //        {
                //            using (IRepository<TM_EMP_OVERTIME> userCtx = new DataRepository<TM_EMP_OVERTIME>())
                //            {
                //                tM_EMP_OVERTIME = userCtx.Find(r =>
                //                    (r.OVERTIME_ID == gvData.DataKeys[iLoop].Values["overtime_id"].ToString())
                //                    ).SingleOrDefault();

                //            }
                //        }
                //        if (tM_EMP_OVERTIME != null)
                //        {

                //            selectedCount = selectedCount + 1;

                //            tM_EMP_OVERTIME.OVERTIME_DEPT_ID = gvData.DataKeys[iLoop].Values["DEPT_ID"].ToString();
                //            tM_EMP_OVERTIME.OVERTIME_EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                //            tM_EMP_OVERTIME.OVERTIME_DATE = DateTime.Parse(gvData.DataKeys[iLoop].Values["OVERTIME_DATE"].ToString());
                //            tM_EMP_OVERTIME.OVERTIME_HOURS = CommonUtils.ConvertStringToDecimal(gvData.DataKeys[iLoop].Values["OVERTIME_HOURS"].ToString());

                //            TextBox txtAmount = gvData.Rows[iLoop].FindControl("txtAmount") as TextBox;
                //            tM_EMP_OVERTIME.OVERTIME_AMOUNT = CommonUtils.ConvertStringToDecimal(txtAmount.Text);

                //            TextBox txtPaidAmount = gvData.Rows[iLoop].FindControl("txtPaidAmount") as TextBox;
                //            tM_EMP_OVERTIME.OVERTIME_PAID_AMOUNT = CommonUtils.ConvertStringToDecimal(txtPaidAmount.Text);


                //            //tM_EMP_OVERTIME.OVERTIME_EMP_GROUP = "NULL";
                //            //tM_EMP_OVERTIME.OVERTIME_SCHEDULE_CODE = "NULL";
                //            tM_EMP_OVERTIME.OVERTIME_PAID = ((CheckBox)gvData.Rows[iLoop].FindControl("chkSelection")).Checked == true ? "1" : "0";

                //            tM_EMP_OVERTIME.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                //            if (gvData.DataKeys[iLoop].Values["overtime_id"].ToString() != string.Empty && gvData.DataKeys[iLoop].Values["overtime_id"].ToString() != "0")
                //            {
                //                tM_EMP_OVERTIME.MODIFIED_BY = this.LoggedUserName;
                //                tM_EMP_OVERTIME.MODIFIED_DATE = DateTime.Today;
                //                tM_EMP_OVERTIME.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_OVERTIME.OVERTIME_ID);
                //                DBMethod.SaveEntity<TM_EMP_OVERTIME>(tM_EMP_OVERTIME, true);
                //                savedBool = true;

                //            }
                //            else
                //            {
                //                tM_EMP_OVERTIME.CREATED_BY = this.LoggedUserName;
                //                tM_EMP_OVERTIME.CREATED_DATE = DateTime.Today;

                //                tM_EMP_OVERTIME.OVERTIME_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_104.ToString(), false, true);
                //                tM_EMP_OVERTIME.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_OVERTIME.OVERTIME_ID);
                //                DBMethod.SaveEntity<TM_EMP_OVERTIME>(tM_EMP_OVERTIME);
                //                savedBool = true;
                //            }

                //        }
                //    }
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvParentData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //if (bol_rowVisiable)
                    //    e.Row.Visible = false;

                    string empId = gvParentData.DataKeys[e.Row.RowIndex].Values["EMP_ID"].ToString();
                    string groupCode = gvParentData.DataKeys[e.Row.RowIndex].Values["OVERTIME_EMP_GROUP"].ToString();

                    GridView gvData = (GridView)e.Row.FindControl("gvData");

                    BindChildGrid(gvData, empId, groupCode);

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //if (bol_rowVisiable)
                    //    e.Row.Visible = false;

                    CheckBox chkSelection = ((CheckBox)e.Row.FindControl("chkSelection"));
                    TextBox txtPaidAmount = ((TextBox)e.Row.FindControl("txtPaidAmount"));
                    if (chkSelection != null && txtPaidAmount != null)
                    {
                        if (chkSelection.Checked == true)
                        {
                            txtPaidAmount.Enabled = false;
                            chkSelection.Enabled = false;
                        }
                        else
                        {
                            txtPaidAmount.Enabled = true;
                            chkSelection.Enabled = true;
                            txtPaidAmount.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("sCATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion
        protected void btnOk_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Hide();
        }
        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                // GridView gvr = (GridView)sender;
                CheckBox chkSelection = (CheckBox)gvr.FindControl("chkSelection");
                TextBox txtPaidAmount = (TextBox)gvr.FindControl("txtPaidAmount");
                Label lblId = (Label)gvr.FindControl("lblId");

                if (lblId.Text != string.Empty && lblId.Text!= "0")
                {
                    using (IRepository<TM_EMP_OVERTIME> userCtx = new DataRepository<TM_EMP_OVERTIME>())
                    {
                        tM_EMP_OVERTIME = userCtx.Find(r =>
                            (r.OVERTIME_ID == lblId.Text)
                            ).SingleOrDefault();
                    }
                }
                if (tM_EMP_OVERTIME != null)
                {
                 
                    tM_EMP_OVERTIME.OVERTIME_PAID_AMOUNT = CommonUtils.ConvertStringToDecimal(txtPaidAmount.Text);
                    tM_EMP_OVERTIME.OVERTIME_PAID = chkSelection.Checked == true ? "1" : "0";
                    tM_EMP_OVERTIME.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    if (lblId.Text != string.Empty && lblId.Text != "0")
                    {
                        tM_EMP_OVERTIME.MODIFIED_BY = this.LoggedUserName;
                        tM_EMP_OVERTIME.MODIFIED_DATE = DateTime.Today;
                        tM_EMP_OVERTIME.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_OVERTIME.OVERTIME_ID);
                        DBMethod.SaveEntity<TM_EMP_OVERTIME>(tM_EMP_OVERTIME, true);
                        savedBool = true;
                    }                    
                }
                //if (txtAmount.Text.Length > 0)
                //{
                //    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                //}
                //if (txtPaidAmount.Text.Length > 0)
                //{
                //    txtPaidAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPaidAmount.Text);
                //}

              
                ModalPopupExtender4.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindChildGrid(GridView gvData, string empId, string groupCode)
        {
            DataTable dtTemp = new DataTable();
            try
            {
                ErrorCollection.Clear();
                DataTable dt_Overtime = new DataTable();
                DataTable dtEmp = new DataTable();
                DataRow drRow = null;


                //  dtEmp = DBMethod.ExecuteQuery(OverTimeSelection_DAL.GetEMPGroupCode()).Tables[0];


                //if (dtEmp.Rows.Count > 0)
                //{
                DateTime dtFrom = DBMethod.ConvertStringToDate(txtFromDate.Text);
                DateTime dtTo = DBMethod.ConvertStringToDate(txtToDate.Text);
                DateTime dtCurrent = dtFrom;

                dt_Overtime = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimedtls("0", txtFromDate.Text, txtToDate.Text)).Tables[0];
                dtTemp = dt_Overtime.Clone();

                for (int iLoop = 0; iLoop <= (dtTo - dtFrom).Days; iLoop++)
                {

                    //for (int i = 0; i < dtEmp.Rows.Count; i++)
                    //{
                    dt_Overtime = FIN.DAL.HR.OverTimeSelection_DAL.GetSP_OverTime(groupCode, empId, dtCurrent.ToShortDateString(), FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimedtls(empId, dtCurrent.ToShortDateString(), txtToDate.Text)).Tables[0];
                    if (dt_Overtime.Rows.Count > 0)
                    {
                        //dt_Overtime.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OVERTIME_PAID_AMOUNT", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("OVERTIME_PAID_AMOUNT"))));
                        //dt_Overtime.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OVERTIME_AMOUNT", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("OVERTIME_AMOUNT"))));
                        //dt_Overtime.AcceptChanges();

                        drRow = dt_Overtime.Rows[0];
                        dtTemp.Rows.Add(drRow.ItemArray);
                        dtTemp.AcceptChanges();


                        // dtTemp = dt_Overtime.Copy();
                        //drRow = dtTemp.NewRow();                              
                        //var desRow = dt_Overtime.NewRow();
                        //var sourceRow = dt_Overtime.Rows[0];
                        //desRow.ItemArray = sourceRow.ItemArray.Clone() as object[];                                
                    }
                    // }
                    dtCurrent = dtCurrent.AddDays(1);
                }


                Session[FINSessionConstants.GridData] = dtTemp;
                //DataTable dt_tmp = dtTemp.Copy();
                //if (dt_tmp.Rows.Count == 0)
                //{
                //    DataRow dr = dt_tmp.NewRow();
                //    dr[0] = "0";
                //    dr["OVERTIME_PAID"] = "FALSE";
                //    dt_tmp.Rows.Add(dr);
                //    bol_rowVisiable = true;
                //}
                gvData.DataSource = dtTemp;
                gvData.DataBind();
                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindParentGrid()
        {
            DataTable dtTemp = new DataTable();
            try
            {
                ErrorCollection.Clear();
                DataTable dt_Overtime = new DataTable();
                DataTable dtEmp = new DataTable(); DataRow drRow = null;

                dtEmp = DBMethod.ExecuteQuery(OverTimeSelection_DAL.GetEMPGroupCode()).Tables[0];

                if (dtEmp.Rows.Count > 0)
                {
                    DateTime dtFrom = DBMethod.ConvertStringToDate(txtFromDate.Text);
                    DateTime dtTo = DBMethod.ConvertStringToDate(txtToDate.Text);
                    DateTime dtCurrent = dtFrom;

                    dt_Overtime = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimeParentdtls("0", txtFromDate.Text, txtToDate.Text)).Tables[0];
                    dtTemp = dt_Overtime.Clone();

                    for (int iLoop = 0; iLoop <= (dtTo - dtFrom).Days; iLoop++)
                    {
                        for (int i = 0; i < dtEmp.Rows.Count; i++)
                        {
                            dt_Overtime = FIN.DAL.HR.OverTimeSelection_DAL.GetSP_OverTime(dtEmp.Rows[i]["tm_grp_code"].ToString(), dtEmp.Rows[i]["EMP_ID"].ToString(), dtCurrent.ToShortDateString(), FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimeParentdtls(dtEmp.Rows[i]["EMP_ID"].ToString(), dtCurrent.ToShortDateString(), txtToDate.Text)).Tables[0];
                            if (dt_Overtime.Rows.Count > 0)
                            {
                                //drRow = dt_Overtime.Rows[0];
                                //dtTemp.Rows.Add(drRow.ItemArray);
                                //dtTemp.AcceptChanges();
                            }
                        }
                        dtCurrent = dtCurrent.AddDays(1);
                    }
                    dtTemp = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.GettmpOverTimeParentdtls("", txtFromDate.Text, txtToDate.Text)).Tables[0];
                    Session["ParentGrid"] = dtTemp;
                    gvParentData.DataSource = dtTemp;
                    gvParentData.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FidllFootGrid123", ex.Message);
            }
            finally
            {
                BindGrid(dtTemp);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnget_Click(object sender, EventArgs e)
        {
            // BindChildGrid();
            BindParentGrid();
        }


    }
}