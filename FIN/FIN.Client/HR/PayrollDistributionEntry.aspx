﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollDistributionEntry.aspx.cs" Inherits="FIN.Client.HR.PayrollDistributionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEmpName">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px">
                <asp:DropDownList ID="ddlEmpName" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEffectiveDate">
                Effective From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtEffectiveDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEffectiveDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffectiveDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEndDate">
                Effective To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]]    txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
           
            <div class="lblBox LNOrient" style="  width: 200px" id="lblActiveFlag">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="  width: 50px">
                <asp:CheckBox ID="chKEnabledFlag" runat="server" Checked="True" TabIndex="10" Text=" " />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
             <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/btnCopyFromPreviousMonth.png" TabIndex = "4"
                    OnClick="btnSave_Click" Style="border: 0px;" />
            <%--<asp:Button ID="Button1" runat="server" Text="Copy From Previous Month" OnClick="btnSave_Click" CssClass="btn"
                TabIndex="4" />--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DIS_DTL_ID,DELETED,segment_value_id" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True" Width="550px">
                <Columns>
                    <asp:TemplateField HeaderText="Property Name" ControlStyle-Width="200px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlPropertyName" runat="server" CssClass="ddlStype" Width="200px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlPropertyName" runat="server" CssClass="ddlStype" Width="200px"
                                TabIndex="4">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("SEGMENT_VALUE") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Percentage" ControlStyle-Width="150px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPercentage" TabIndex="7" MaxLength="3" runat="server" CssClass="RequiredField txtBox_N"
                                Width="150px" Text='<%# Eval("PERCENTAGE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtPercentage" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPercentage" MaxLength="3" TabIndex="5" Wrap="true" runat="server"
                                CssClass="RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtPercentage" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblleveldesc" Enabled="false" Width="350px" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("PERCENTAGE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <asp:HiddenField ID="total" runat="server" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
