﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class SettlementRulesEntry : PageBase
    {
        HR_STAFF_ATTENDANCE HR_STAFF_ATTENDANCE = new HR_STAFF_ATTENDANCE();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            //AccountingCalendar_BLL.GetFinancialYear(ref ddlFinancialYear);
            //Department_BLL.GetDepartmentName(ref ddlDepartment);
            //  Employee_BLL.GetEmployeeName(ref ddlStaffName);
        }
        private void FillFromToDate()
        {
            //if (ddlFinancialYear.SelectedValue != null)
            //{
            //    dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetFinancialYear((ddlFinancialYear.SelectedValue.ToString()))).Tables[0];
            //    if (dtData != null)
            //    {
            //        if (dtData.Rows.Count > 0)
            //        {
            //            if (dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT] != string.Empty)
            //            {
            //                txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //            if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT] != string.Empty)
            //            {
            //                txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //        }
            //    }
            //}
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillFromToDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillLeaveRequest(GridViewRow gvr)
        {
            // GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;

            if (ddlStaff.SelectedValue != "")
            {
                LeaveApplication_BLL.GetLeaveApplication(ref ddlRequest, ddlStaff.SelectedValue.ToString());

                if (gvData.EditIndex >= 0)
                {
                    ddlRequest.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_REQ_ID].ToString();
                }
            }

            if (ddlAttendanceType.SelectedItem.Text == "Leave")
            {
                ddlRequest.Enabled = true;
            }
            else
            {
                ddlRequest.Enabled = false;
            }
        }
        protected void ddlRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                FillLeaveRequest(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();

                dtGridData = DBMethod.ExecuteQuery(StaffAttendance_DAL.GetStaffAttedDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    HR_STAFF_ATTENDANCE = StaffAttendance_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = HR_STAFF_ATTENDANCE;

                    // ddlFinancialYear.SelectedValue = HR_STAFF_ATTENDANCE.FISCAL_YEAR.ToString();
                    FillFromToDate();

                    //HR_EMP_WORK_DTLS HR_EMP_WORK_DTLS = new HR_EMP_WORK_DTLS();
                    //HR_EMP_WORK_DTLS = Workdetails_BLL.getEmpWorkDetails(HR_STAFF_ATTENDANCE.STAFF_ID);
                    //if (HR_EMP_WORK_DTLS != null)
                    //{
                    //    ddlDepartment.SelectedValue = HR_EMP_WORK_DTLS.EMP_DEPT_ID;
                    //}

                    //  ddlDepartment.SelectedValue = HR_STAFF_ATTENDANCE.ATTRIBUTE1;
                    //if (HR_STAFF_ATTENDANCE.ATTENDANCE_DATE != null)
                    //{
                        
                    //    txtDate.Text = DBMethod.ConvertDateToString(HR_STAFF_ATTENDANCE.ATTENDANCE_DATE.ToString());
                    //}
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlStaff = tmpgvr.FindControl("ddlStaff") as DropDownList;
                //DropDownList ddlAttendanceType = tmpgvr.FindControl("ddlAttendanceType") as DropDownList;

                //Employee_BLL.GetEmployeeName(ref ddlStaff);
                //Lookup_BLL.GetLookUpValues(ref ddlAttendanceType, "ATY");

                //if (gvData.EditIndex >= 0)
                //{
                //    ddlStaff.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.EMP_ID].ToString();
                //    ddlAttendanceType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ATTENDANCE_TYPE].ToString();
                //    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //    {
                //        FillLeaveRequest(tmpgvr);
                //    }

                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlFinancialYear;
                //slControls[1] = txtFromDate;
                //slControls[2] = ddlDepartment;
                //slControls[3] = txtAttendanceDate;

                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
                string strMessage = "Financial Year ~ From Date~Department ~ Attendance Date";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                //if (DBMethod.ConvertStringToDate(txtFromDate.Text) <= DBMethod.ConvertStringToDate(txtAttendanceDate.Text) && DBMethod.ConvertStringToDate(txtToDate.Text) >= DBMethod.ConvertStringToDate(txtAttendanceDate.Text))
                //{

                //}
                //else
                //{
                //    ErrorCollection.Add("invalidAttendance", "Attendance Date should be between From and To Date");
                //    return;
                //}

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Staff Leave ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    HR_STAFF_ATTENDANCE = new HR_STAFF_ATTENDANCE();

                    if ((dtGridData.Rows[iLoop][FINColumnConstants.STATT_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.STATT_ID].ToString() != string.Empty)
                    {
                        HR_STAFF_ATTENDANCE = StaffAttendance_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.STATT_ID].ToString());

                    }
                    HR_STAFF_ATTENDANCE.STAFF_ID = dtGridData.Rows[iLoop][FINColumnConstants.EMP_ID].ToString();
                    HR_STAFF_ATTENDANCE.ATTENDANCE_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.ATTENDANCE_TYPE].ToString();
                    HR_STAFF_ATTENDANCE.LEAVE_REQ_ID = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_REQ_ID].ToString();
                    HR_STAFF_ATTENDANCE.REASON = dtGridData.Rows[iLoop][FINColumnConstants.REASON].ToString();
                    //if (txtDate.Text.Trim() != string.Empty)
                    //{
                    //    HR_STAFF_ATTENDANCE.ATTENDANCE_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                    //}
                    //HR_STAFF_ATTENDANCE.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());
                    //HR_STAFF_ATTENDANCE.ATTRIBUTE1 = ddlDepartment.SelectedValue.ToString();
                    HR_STAFF_ATTENDANCE.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    HR_STAFF_ATTENDANCE.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    HR_STAFF_ATTENDANCE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_STAFF_ATTENDANCE.STATT_ID);

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(HR_STAFF_ATTENDANCE, "D"));
                    }
                    else
                    {
                        if ((dtGridData.Rows[iLoop][FINColumnConstants.STATT_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.STATT_ID].ToString() != string.Empty)
                        {
                            HR_STAFF_ATTENDANCE.STATT_ID = dtGridData.Rows[iLoop]["STATT_ID"].ToString();
                            HR_STAFF_ATTENDANCE.MODIFIED_BY = this.LoggedUserName;
                            HR_STAFF_ATTENDANCE.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_STAFF_ATTENDANCE, FINAppConstants.Update));
                        }
                        else
                        {
                            //HR_STAFF_ATTENDANCE.STATT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_022);
                            HR_STAFF_ATTENDANCE.STATT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_022.ToString(), false, true);
                            HR_STAFF_ATTENDANCE.CREATED_BY = this.LoggedUserName;
                            HR_STAFF_ATTENDANCE.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_STAFF_ATTENDANCE, FINAppConstants.Add));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveSingleEntity<HR_STAFF_ATTENDANCE>(tmpChildEntity);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveSingleEntity<HR_STAFF_ATTENDANCE>(tmpChildEntity, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_STAFF_ATTENDANCE>(HR_STAFF_ATTENDANCE);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;
            TextBox txtReason = gvr.FindControl("txtReason") as TextBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.STATT_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            if (ddlRequest.SelectedItem.Text != FINMessageConstatns.AttendanceType_Present.ToString())
            {
                slControls[0] = ddlStaff;
                slControls[1] = ddlAttendanceType;

                strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                strMessage = "Staff Name ~ Attendance type ";
            }
            else if (ddlRequest.SelectedItem.Text == FINMessageConstatns.AttendanceType_Present.ToString())
            {
                slControls[0] = ddlStaff;
                slControls[1] = ddlAttendanceType;
                slControls[3] = ddlRequest;

                strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                strMessage = "Staff Name ~ Attendance type ~ Leave Request";
            }

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "EMP_ID='" + ddlStaff.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.EMP_ID] = ddlStaff.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlStaff.SelectedItem.Text;
            drList[FINColumnConstants.ATTENDANCE_TYPE] = ddlAttendanceType.SelectedValue;
            drList["ATTENDANCE_TYPE_name"] = ddlAttendanceType.SelectedItem.Text;
            drList[FINColumnConstants.LEAVE_NAME] = ddlRequest.SelectedItem.Text == FINAppConstants.intialRowTextField ? string.Empty : ddlRequest.SelectedItem.Text;
            drList[FINColumnConstants.LEAVE_REQ_ID] = ddlRequest.SelectedValue == null ? string.Empty : ddlRequest.SelectedValue.ToString();
            drList[FINColumnConstants.REASON] = txtReason.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion
    }
}