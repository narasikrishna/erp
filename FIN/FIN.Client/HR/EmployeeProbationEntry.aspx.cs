﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeProbationEntry : PageBase
    {

        HR_EMP_PROBATION hR_EMP_PROBATION = new HR_EMP_PROBATION();
        HR_EMP_PROB_APPRAISER hR_EMP_PROB_APPRAISER = new HR_EMP_PROB_APPRAISER();
        DataTable dtGridData = new DataTable();
        Employee_BLL employeeBLL = new Employee_BLL();
        DataTable dtGridData1 = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMP_PB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>


        private void FillComboBox()
        {
            FIN.BLL.HR.EmployeeProbation_BLL.GetEmployeeWithoutProbation(ref ddlEmpNumber);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();

                Session[FINSessionConstants.GridData] = null;

                EntityData = null;


                dtGridData = FIN.BLL.HR.EmployeeProbation_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                dtGridData1 = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeProbation_DAL.fn_GetEmpProbationAppraiserDtl(Master.StrRecordId)).Tables[0];
                BindGrid1(dtGridData1);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_PROBATION> userCtx = new DataRepository<HR_EMP_PROBATION>())
                    {
                        hR_EMP_PROBATION = userCtx.Find(r =>
                            (r.PROB_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_PROBATION;
                    ddlEmpNumber.SelectedValue = hR_EMP_PROBATION.PROB_EMP_ID;

                    AssignEmpName();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_PROBATION = new HR_EMP_PROBATION();
                    if (dtGridData.Rows[iLoop]["PROB_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_EMP_PROBATION> userCtx = new DataRepository<HR_EMP_PROBATION>())
                        {
                            hR_EMP_PROBATION = userCtx.Find(r =>
                                (r.PROB_ID == dtGridData.Rows[iLoop]["PROB_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    hR_EMP_PROBATION.PROB_EMP_ID = ddlEmpNumber.SelectedValue; //dtGridData.Rows[iLoop]["EMP_ID"].ToString();

                    if (dtGridData.Rows[iLoop]["PROB_STATUS"] != DBNull.Value)
                    {
                        hR_EMP_PROBATION.PROB_STATUS = dtGridData.Rows[iLoop]["PROB_STATUS"].ToString();
                    }

                    if (dtGridData.Rows[iLoop]["PROB_FROM_DT"] != DBNull.Value)
                    {
                        hR_EMP_PROBATION.PROB_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["PROB_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["PROB_TO_DT"] != DBNull.Value)
                    {
                        hR_EMP_PROBATION.PROB_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["PROB_TO_DT"].ToString());
                    }
                    else
                    {
                        hR_EMP_PROBATION.PROB_TO_DT = null;
                    }

                    hR_EMP_PROBATION.PROBATION_PERIOD = dtGridData.Rows[iLoop]["PROBATION_PERIOD"].ToString();


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_EMP_PROBATION.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_EMP_PROBATION.ENABLED_FLAG = FINAppConstants.N;
                    }

                    hR_EMP_PROBATION.PROB_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_EMP_PROBATION.PROB_ID, hR_EMP_PROBATION.PROB_EMP_ID);
                    //if (ProReturn != string.Empty)
                    //{
                    //    if (ProReturn != "0")
                    //    {
                    //        ErrorCollection.Add("EMPLOYEEPROBATION", ProReturn);
                    //        if (ErrorCollection.Count > 0)
                    //        {
                    //            return;
                    //        }
                    //    }
                    //}

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_PROBATION, "D"));
                    //}
                    //else
                    //{
                    // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                    //ProReturn = FIN.DAL.HR.EmployeeProbation_DAL.GetSPFOR_ERR_MGR_CATEGORY(hR_EMP_PROBATION.PROB_EMP_ID, hR_EMP_PROBATION.PROB_ID);

                    //if (ProReturn != string.Empty)
                    //{
                    //    if (ProReturn != "0")
                    //    {
                    //        ErrorCollection.Add("EMPPB", ProReturn);
                    //        if (ErrorCollection.Count > 0)
                    //        {
                    //            return;
                    //        }
                    //    }
                    //}

                    if (dtGridData.Rows[iLoop]["PROB_ID"].ToString() != "0")
                    {
                        hR_EMP_PROBATION.PROB_ID = dtGridData.Rows[iLoop][FINColumnConstants.PROB_ID].ToString();
                        hR_EMP_PROBATION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PROBATION.PROB_ID);
                        hR_EMP_PROBATION.MODIFIED_BY = this.LoggedUserName;
                        hR_EMP_PROBATION.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_EMP_PROBATION>(hR_EMP_PROBATION, true);
                        savedBool = true;
                    }
                    else
                    {
                        hR_EMP_PROBATION.PROB_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_077.ToString(), false, true);
                        hR_EMP_PROBATION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PROBATION.PROB_ID);
                        hR_EMP_PROBATION.CREATED_BY = this.LoggedUserName;
                        hR_EMP_PROBATION.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_EMP_PROBATION>(hR_EMP_PROBATION);
                        savedBool = true;
                    }
                    //}
                }



                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }

                var tmpChildEntity1 = new List<Tuple<object, string>>();

                for (int jLoop = 0; jLoop < dtGridData1.Rows.Count; jLoop++)
                {
                    hR_EMP_PROB_APPRAISER = new HR_EMP_PROB_APPRAISER();
                    if (dtGridData1.Rows[jLoop]["PROB_APPR_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_EMP_PROB_APPRAISER> userCtx = new DataRepository<HR_EMP_PROB_APPRAISER>())
                        {
                            hR_EMP_PROB_APPRAISER = userCtx.Find(r =>
                                (r.PROB_APPR_ID == dtGridData1.Rows[jLoop]["PROB_APPR_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_EMP_PROB_APPRAISER.LINE_NO = (jLoop + 1);
                    if (dtGridData1.Rows[jLoop]["EMP_ID"] != DBNull.Value)
                    {
                        hR_EMP_PROB_APPRAISER.EMP_ID = dtGridData1.Rows[jLoop]["EMP_ID"].ToString();
                    }

                    hR_EMP_PROB_APPRAISER.ENABLED_FLAG = FINAppConstants.Y;

                    hR_EMP_PROB_APPRAISER.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    hR_EMP_PROB_APPRAISER.PROB_ID = hR_EMP_PROBATION.PROB_ID;

                    if (dtGridData1.Rows[jLoop]["PROB_APPR_ID"].ToString() != "0")
                    {
                        hR_EMP_PROB_APPRAISER.PROB_APPR_ID = dtGridData1.Rows[jLoop]["PROB_APPR_ID"].ToString();
                        hR_EMP_PROB_APPRAISER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PROB_APPRAISER.PROB_APPR_ID);
                        hR_EMP_PROB_APPRAISER.MODIFIED_BY = this.LoggedUserName;
                        hR_EMP_PROB_APPRAISER.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_EMP_PROB_APPRAISER>(hR_EMP_PROB_APPRAISER, true);
                        savedBool = true;
                    }
                    else
                    {
                        hR_EMP_PROB_APPRAISER.PROB_APPR_ID = FINSP.GetSPFOR_SEQCode("HR_077_PA".ToString(), false, true);
                        hR_EMP_PROB_APPRAISER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PROB_APPRAISER.PROB_APPR_ID);
                        hR_EMP_PROB_APPRAISER.CREATED_BY = this.LoggedUserName;
                        hR_EMP_PROB_APPRAISER.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_EMP_PROB_APPRAISER>(hR_EMP_PROB_APPRAISER);
                        savedBool = true;
                    }

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlProbStatus = tmpgvr.FindControl("ddlProbStatus") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlProbStatus, "PRB");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlProbStatus.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PROB_STATUS].ToString();
                }

                //CheckBox chkact = tmpgvr.FindControl("chkact") as CheckBox;
                //DataTable dtchilcat = new DataTable();
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    hR_CATEGORIES.CATEGORY_ID = dtGridData.Rows[iLoop][FINColumnConstants.CATEGORY_ID].ToString();
                //}
                //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Category_DAL.Get_Childdataof_Category(hR_CATEGORIES.CATEGORY_ID)).Tables[0];
                //if (dtchilcat.Rows.Count > 0)
                //{
                //    chkact.Enabled = false;
                //}


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void FillFooterGridCombo1(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlEmp = tmpgvr.FindControl("ddlEmp") as DropDownList;
                Employee_BLL.GetEmployeeNameWithCode(ref ddlEmp);

                if (gvEPA.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlEmp.SelectedValue = gvEPA.DataKeys[gvEPA.EditIndex].Values[FINColumnConstants.EMP_ID].ToString();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_FillFootGrid1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee Probation");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session["GridData1"], "Probation Appraiser");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void BindGrid1(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session["GridData1"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvEPA.DataSource = dt_tmp;
                gvEPA.DataBind();
                GridViewRow gvr = gvEPA.FooterRow;
                FillFooterGridCombo1(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_BG1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEPA_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                gvEPA.EditIndex = -1;
                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CNCL_EDT1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEPA_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvEPA.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl1(gvr, dtGridData1, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData1.Rows.Add(drList);
                    BindGrid1(dtGridData1);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_CMD1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            DropDownList ddlProbStatus = gvr.FindControl("ddlProbStatus") as DropDownList;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            TextBox txtPeriod = gvr.FindControl("txtPeriod") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                //if (Master.Mode == FINAppConstants.Add)
                //{
                    drList["PROB_ID"] = "0";
               // }
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = dtpStartDate;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpEndDate;
            slControls[3] = ddlProbStatus;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~DateTime~DateRangeValidate~DropDownList";
            string strMessage = Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + " ~ " + Prop_File_Data["Probation_Status_P"] + "";
            //string strMessage = "Start Date ~ Start Date ~ End Date ~ Probation Status";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "PROB_STATUS='" + ddlProbStatus.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (Master.Mode == FINAppConstants.Add)
            {
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["PROB_ID"].ToString(), ddlEmpNumber.SelectedValue);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("EMPLOYEEPROBATION", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return drList;
                        }
                    }
                }
            }

            // DataTable dtchilcat = new DataTable();

            //if (gvData.EditIndex >= 0)
            //{
            //    hR_EMP_PROBATION.PROB_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PROB_ID].ToString();

            //    dtchilcat = null;
            //    //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeProbation_DAL.Get_Childdataof_Category(hR_EMP_PROBATION.PROB_ID)).Tables[0];
            //    //if (dtchilcat.Rows.Count > 0)
            //    //{
            //    //    if (chkact.Checked)
            //    //    {

            //    //    }
            //    //    else
            //    //    {
            //    //        ErrorCollection.Add("chkflg", Prop_File_Data["chkflg_P"]);
            //    //        return drList;
            //    //    }
            //    //}
            //}


            //ValidatePeriod(dtpStartDate.Text, dtpEndDate.Text, txtPeriod.Text);
            dtpEndDate.Text = DBMethod.ConvertDateToString(DBMethod.ConvertStringToDate(dtpStartDate.Text).AddDays(CommonUtils.ConvertStringToInt(txtPeriod.Text)).ToString()).ToString();
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList["PROBATION_PERIOD"] = txtPeriod.Text;

            if (ddlProbStatus.SelectedIndex.ToString() != "0")
            {
                drList["PROB_STATUS"] = ddlProbStatus.SelectedItem.Text;
            }

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["PROB_FROM_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {
                drList["PROB_TO_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["PROB_TO_DT"] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            //drList[FINColumnConstants.DELETED] = FINAppConstants.N;
            return drList;
        }


        private DataRow AssignToGridControl1(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlEmp = gvr.FindControl("ddlEmp") as DropDownList;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData1.NewRow();
                drList["PROB_APPR_ID"] = "0";

            }
            else
            {
                drList = dtGridData1.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddlEmp;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList";
            string strMessage = Prop_File_Data["Employee_P"];

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            if (ddlEmp.SelectedIndex.ToString() != "0")
            {
                drList["EMP_NAME"] = ddlEmp.SelectedItem.Text;
                drList["EMP_ID"] = ddlEmp.SelectedValue;
            }


            return drList;
        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEPA_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvEPA.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl1(gvr, dtGridData1, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvEPA.EditIndex = -1;
                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_UP1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEPA_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                DataRow drList = null;
                drList = dtGridData1.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid1(dtGridData1);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_DEL1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEPA_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData1"] != null)
                {
                    dtGridData1 = (DataTable)Session["GridData1"];
                }
                gvEPA.EditIndex = e.NewEditIndex;
                BindGrid1(dtGridData1);
                GridViewRow gvr = gvEPA.Rows[e.NewEditIndex];
                FillFooterGridCombo1(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_ROW_EDT1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEPA_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvEPA.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RowCreated1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void gvEPA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPB_RDB1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_PROBATION.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_EMP_PROBATION>(hR_EMP_PROBATION);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void AssignEmpName()
        {
            DataTable dtEmpName = new DataTable();
            dtEmpName = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeName(ddlEmpNumber.SelectedValue.ToString())).Tables[0];
            if (dtEmpName.Rows.Count > 0)
            {
                txtEmpName.Text = dtEmpName.Rows[0]["EMP_NAME"].ToString();
            }
        }

        protected void ddlEmpNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            AssignEmpName();
        }

        private void ValidatePeriod(string stDate, string enDate, string period)
        {
            try
            {
                ErrorCollection.Clear();

                double diffDays = 0;
                double yrDays = 0;

                if (stDate != string.Empty && enDate != string.Empty && period != string.Empty)
                {
                    diffDays = (DBMethod.ConvertStringToDate(enDate) - DBMethod.ConvertStringToDate(stDate)).TotalDays;
                    // yrs = DBMethod.ConvertStringToDate(dtpEndDate.Text).Year - DBMethod.ConvertStringToDate(dtpStartDate.Text).Year;

                    yrDays = CommonUtils.ConvertStringToDouble(period);
                    if (diffDays != yrDays)
                    {
                        ErrorCollection.Remove("STEN");
                        ErrorCollection.Add("STEN", "Start date & End date period is not matching with the probation period");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtPeriod_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
                TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
                TextBox txtPeriod = gvr.FindControl("txtPeriod") as TextBox;
                int period = 0;
                period = CommonUtils.ConvertStringToInt(txtPeriod.Text);

                dtpEndDate.Text = DBMethod.ConvertDateToString(DBMethod.ConvertStringToDate(dtpStartDate.Text).AddDays(period).ToString());

                // ValidatePeriod(dtpStartDate.Text, dtpEndDate.Text, txtPeriod.Text);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}