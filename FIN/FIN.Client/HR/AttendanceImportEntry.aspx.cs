﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using Excel = Microsoft.Office.Interop.Excel;

namespace FIN.Client.HR
{
    public partial class AttendanceImportEntry : PageBase
    {

        TM_EMP_TRANS tM_EMP_TRANS = new TM_EMP_TRANS();
        Boolean saveBool = false;
        Boolean bol_rowVisiable;
        DataTable dtgriddata = new DataTable();
        Dictionary<string, string> Prop_File_Data;

        protected void Page_Load(object sender, EventArgs e)
        {
            hf_Sel_Lng.Value = Session["Sel_Lng"].ToString();
            if (!IsPostBack)
            {
                Session["ProgramID"] = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
                //PopulateMonth();
                //PopulateYear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    lblId.Text = Prop_File_Data["Date_P"];
                    dataImport.InnerHtml = Prop_File_Data["Data_Imported_Successfully_P"];
                    noDataImport.InnerHtml = Prop_File_Data["No_Data_to_Import_P"];
                    changeLangGrid();
                }
            }
           
            //else
            //{
            //    lblId.Text = "Date";

            //}
        }



        //private void PopulateMonth()
        //{
        //    ddlMonth.Items.Clear();
        //    ListItem lt = new ListItem();
        //    //  lt.Text = "MM";
        //    // lt.Value = "0";

        //    // ddlMonth.Items.Add(lt);
        //    for (int i = 1; i <= 12; i++)
        //    {
        //        lt = new ListItem();
        //        lt.Text = Convert.ToDateTime(i.ToString() + "/" + i.ToString() + "/1900").ToString("MMMM");
        //        // lt.Text = i.ToString();
        //        lt.Value = i.ToString();
        //        ddlMonth.Items.Add(lt);
        //    }
        //    //  ddlMonth.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        //}

        //private void PopulateYear()
        //{
        //    ddlYear.Items.Clear();
        //    ListItem lt = new ListItem();
        //    // lt.Text = "YYYY";
        //    // lt.Value = "0";
        //    // ddlYear.Items.Add(lt);
        //    for (int i = DateTime.Now.Year; i >= 1950; i--)
        //    {
        //        lt = new ListItem();
        //        lt.Text = i.ToString();
        //        lt.Value = i.ToString();
        //        ddlYear.Items.Add(lt);
        //    }
        //    // ddlYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        //}

        private void changeLangGrid()
        {
            if (gvData.HeaderRow != null)
            {
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    gvData.HeaderRow.Cells[0].Text = Prop_File_Data["Employee_Number_P"];
                    gvData.HeaderRow.Cells[1].Text = Prop_File_Data["Employee_Name_P"];
                    gvData.HeaderRow.Cells[2].Text = Prop_File_Data["Date_P"];
                    gvData.HeaderRow.Cells[3].Text = Prop_File_Data["Late_P"];
                    gvData.HeaderRow.Cells[4].Text = Prop_File_Data["Late_In(In_Minutes)_P"];
                    gvData.HeaderRow.Cells[5].Text = Prop_File_Data["Early_Out(In_Minutes)_P"];
                    gvData.HeaderRow.Cells[6].Text = Prop_File_Data["Time_In_P"];
                    gvData.HeaderRow.Cells[7].Text = Prop_File_Data["Time_Out_P"];
                    gvData.HeaderRow.Cells[8].Text = Prop_File_Data["Permission_From_Time_P"];
                    gvData.HeaderRow.Cells[9].Text = Prop_File_Data["Permission_To_Time_P"];
                    gvData.HeaderRow.Cells[10].Text = Prop_File_Data["NO_IN_TIME_P"];
                    gvData.HeaderRow.Cells[11].Text = Prop_File_Data["NO_OUT_TIME_P"];
                }
            }
        }

        protected void btnimport_Click(object sender, EventArgs e)
        {

            try
            {
                //Excel.Application xlApp;
                //Excel.Workbook xlWorkBook;
                //Excel.Worksheet xlWorkSheet;
                //Excel.Range range;

                //string str;
                //int rCnt = 0;
                //int cCnt = 0;

                //xlApp = new Excel.Application();

                ////HERE Fileupload1 IS BROWSE ID WHEN CLICK IMPORT BTN PATH WILL DIRECTLY TAKE FROM HERE..

                //string filename = Path.GetFileNameWithoutExtension(Fileupload1.FileName);
                //string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");

                //// Here "UploadFile"  is a server path in your system imported excel first save at here then it will ready for export..

                //Fileupload1.SaveAs(Server.MapPath("~/UploadFile/") + filename + formatted + ".xlsx");

                //string existingFile = Server.MapPath("~/UploadFile/") + filename + formatted + ".xlsx";


                //xlWorkBook = xlApp.Workbooks.Open(existingFile, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                //range = xlWorkSheet.UsedRange;
                //string trnsserial;
                //string trdate;
                //string emp_No;
                //string tm_time;

                //// DateTime dtdttm = new DateTime(;
                //string tm_mode;
                //// string trtime;
                //int count = 0;

                //// variables trnsemp,trdate are columns in excel it will insert at table like vertically..

                //for (rCnt = 2; rCnt <= range.Rows.Count; rCnt++)
                //{

                //    //here rcnt, 1 & rcnt,6 are 1st & 6th columns in excel

                //    DataTable dttransserl = new DataTable();
                //    DataTable dtEmpID = new DataTable();

                //    trnsserial = Convert.ToString((range.Cells[rCnt, 1] as Excel.Range).Value2);
                //    trdate = Convert.ToString((range.Cells[rCnt, 6] as Excel.Range).Value2);
                //    tm_time = Convert.ToString((range.Cells[rCnt, 7] as Excel.Range).Value2);
                //    tm_mode = Convert.ToString((range.Cells[rCnt, 25] as Excel.Range).Value2);
                //    emp_No = Convert.ToString((range.Cells[rCnt, 3] as Excel.Range).Value2);

                //    if (trnsserial != null)
                //    {

                //        // here we r using "double"  "datetime" for while date comes like number it will convert to date..

                //        double tmdat = double.Parse(trdate);

                //        int mode = int.Parse(tm_mode);

                //        DateTime dat = DateTime.FromOADate(tmdat);
                //        DateTime tm_dttm = DateTime.Parse(tm_time);



                //        dttransserl = DBMethod.ExecuteQuery(AttendanceImport_DAL.GetTransserialNo(trnsserial)).Tables[0];
                //        dtEmpID = DBMethod.ExecuteQuery(AttendanceImport_DAL.GetEmpID(emp_No)).Tables[0];

                //        if (dttransserl.Rows.Count <= 0)
                //        {
                //            tM_EMP_TRANS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TM_EMP_TRANS_SEQ);
                //            tM_EMP_TRANS.TM_EMP_ID = dtEmpID.Rows[0]["EMP_ID"].ToString();
                //            tM_EMP_TRANS.ATTRIBUTE1 = trnsserial;
                //            tM_EMP_TRANS.TM_DATE = dat;
                //            tM_EMP_TRANS.TM_TIME = tm_dttm;
                //            if (mode == 1)
                //            {
                //                tM_EMP_TRANS.TM_MODE = "I";
                //            }
                //            else
                //            {
                //                tM_EMP_TRANS.TM_MODE = "O";
                //            }

                //            tM_EMP_TRANS.ENABLED_FLAG = FINAppConstants.Y;
                //            tM_EMP_TRANS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                //            tM_EMP_TRANS.CREATED_BY = this.LoggedUserName;
                //            tM_EMP_TRANS.CREATED_DATE = DateTime.Today;
                //            DBMethod.SaveEntity<TM_EMP_TRANS>(tM_EMP_TRANS);
                //            saveBool = true;
                //        }
                //    }
                //    else
                //    {
                //        count = count + 1;
                //        if (count > 10)
                //        {
                //            break;
                //        }
                //    }



                //}

                //xlWorkBook.Close(true, null, null);
                //xlApp.Quit();

                //if (saveBool)
                //{
                //    IDdataImport.Visible = true;
                //}
                ErrorCollection.Clear();
                if (txtDate.Text != null)
                {
                    string p_out;
                    int p_out1 = 0;
                    int rwcnt_bfinsert;
                    int rwcnt_afinsert;

                    dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetDummyEmpTransDtl(DBMethod.ConvertStringToDate(txtDate.Text))).Tables[0];
                    if (dtgriddata.Rows.Count == 0)
                    {
                        rwcnt_bfinsert = dtgriddata.Rows.Count;
                        if (txtDate.Text.Length > 0)
                        {
                            FIN.DAL.FINSP.GetProc_For_Import_EmTrans(DBMethod.ConvertStringToDate(txtDate.Text));
                            dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetDummyEmpTransDtl(DBMethod.ConvertStringToDate(txtDate.Text))).Tables[0];


                            if (dtgriddata.Rows.Count > 0)
                            {
                                rwcnt_afinsert = dtgriddata.Rows.Count;

                                if (rwcnt_bfinsert != rwcnt_afinsert)
                                {
                                    IDdataImport.Visible = true;
                                    IDNodata.Visible = false;
                                    BindGrid(dtgriddata);
                                    gvData.Visible = true;

                                }
                                else
                                {
                                    IDNodata.Visible = true;
                                    IDdataImport.Visible = false;
                                    BindGrid(dtgriddata);
                                    gvData.Visible = true;
                                }
                            }
                            else
                            {
                                IDNodata.Visible = true;
                                IDdataImport.Visible = false;
                                BindGrid(dtgriddata);
                                gvData.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        IDNodata.Visible = true;
                        IDdataImport.Visible = false;
                        BindGrid(dtgriddata);
                        gvData.Visible = true;
                    }
                    changeLangGrid();
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
                ErrorCollection.Add("err", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Add(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void btnshow_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                if (txtDate.Text.Length > 0)
                {
                    dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetDummyEmpTransDtl(DBMethod.ConvertStringToDate(txtDate.Text))).Tables[0];
                    if (dtgriddata.Rows.Count > 0)
                    {
                        BindGrid(dtgriddata);
                        gvData.Visible = true;
                    }
                    else
                    {
                        BindGrid(dtgriddata);
                        gvData.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
                ErrorCollection.Add("err2", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Add(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void BindGrid(DataTable dtData)
        {

            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();


        }


        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}


                    string lateflg = e.Row.Cells[3].Text;

                    string noIntime = string.Empty;
                    //if (e.Row.Cells[6].Text.Length > 0)
                    //{
                    //    noIntime = e.Row.Cells[6].Text;
                    //}
                    if (gvData.DataKeys[e.Row.RowIndex].Values["NO_IN_TIME"].ToString() != null)
                    {
                        if (gvData.DataKeys[e.Row.RowIndex].Values["NO_IN_TIME"].ToString() != string.Empty)
                        {
                            if (gvData.DataKeys[e.Row.RowIndex].Values["NO_IN_TIME"].ToString().Length > 0)
                            {
                                noIntime = gvData.DataKeys[e.Row.RowIndex].Values["NO_IN_TIME"].ToString();// e.Row.Cells[7].Text;
                            }
                        }
                    }
                    //else
                    //{
                    //    noIntime = "1";
                    //}

                    string noOutTime = string.Empty;
                    if (gvData.DataKeys[e.Row.RowIndex].Values["NO_OUT_TIME"].ToString() != string.Empty)
                    {
                        if (gvData.DataKeys[e.Row.RowIndex].Values["NO_OUT_TIME"].ToString() != string.Empty)
                        {
                            if (gvData.DataKeys[e.Row.RowIndex].Values["NO_OUT_TIME"].ToString().Length > 0)
                            {
                                noOutTime = gvData.DataKeys[e.Row.RowIndex].Values["NO_OUT_TIME"].ToString();// e.Row.Cells[7].Text;
                            }
                        }
                    }
                    //else
                    //{
                    //    noOutTime = "1";
                    //}

                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if (lateflg == "YES")
                        {
                            cell.BackColor = System.Drawing.Color.Yellow;
                        }
                        if (noIntime != string.Empty)
                        {
                            if (noIntime == "NO_IN_TIME")
                            {
                                //if (noIntime == "1")
                                //{
                                cell.BackColor = System.Drawing.Color.Orange;
                                e.Row.Cells[6].BackColor = System.Drawing.Color.Orange;
                            }
                        }
                        if (noOutTime != string.Empty)
                        {
                            if (noOutTime == "NO_OUT_TIME")
                            {
                                //if (noOutTime == "1")
                                //{
                                cell.BackColor = System.Drawing.Color.Orange;
                                e.Row.Cells[7].BackColor = System.Drawing.Color.Orange;
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("atnimp_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Add(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


    }
}
