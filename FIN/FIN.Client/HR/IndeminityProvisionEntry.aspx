﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="IndeminityProvisionEntry.aspx.cs" Inherits="FIN.Client.HR.IndeminityProvisionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalendarName">
                Payroll Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlPayrollPeriod" Width="300px" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="1" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px">
                 <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png"
                    OnClick="btnView_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn" OnClick="btnView_Click" />--%>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px">
                 <asp:ImageButton ID="btnPosting" runat="server" ImageUrl="~/Images/post.png"
                    OnClick="btnPosting_Click" Style="border: 0px;" />
              <%--  <asp:Button ID="btnPosting" runat="server" Text="Post" CssClass="btn" OnClick="btnPosting_Click" />--%>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px">
                 <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/print.png" Visible="False"
                    OnClick="btnPrint_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" OnClick="btnPrint_Click"
                    Visible="False" />--%>
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField runat="server" ID="hdDate" />
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                ShowFooter="false" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="EMP_NO" HeaderText="Employee No" />
                    <asp:BoundField DataField="EMPLOYEE_NAME" HeaderText="Employee Name" />
                    <asp:BoundField DataField="PERVIOUS_BALANCE" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Previous Month Balances">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CURRENT_ACCUMULATION" HeaderText="Current Accumulation">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CURRENT_BALANCE" HeaderText="Current Balances">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
