﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TrainingEnrollment : PageBase
    {
        HR_TRM_ENROLL_HDR HR_TRM_ENROLL_HDR = new HR_TRM_ENROLL_HDR();
        HR_TRM_ENROLL_DTL HR_TRM_ENROLL_DTL = new HR_TRM_ENROLL_DTL();
        string ProReturn = null;

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool = false;
        string TrngDtlID = null;

        # region Page Load

        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            // FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlCompetencyType, "COMT");
            FIN.BLL.HR.TrainingAttendance_BLL.fn_GetSchedule(ref ddlScheduleName);

           // Trainer_BLL.GetTrainer(ref ddlTrainer);
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                dtpTrnDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRM_ENROLL_HDR> userCtx = new DataRepository<HR_TRM_ENROLL_HDR>())
                    {
                        HR_TRM_ENROLL_HDR = userCtx.Find(r =>
                            (r.ENRL_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = HR_TRM_ENROLL_HDR;

                    txtCriteria.Text = HR_TRM_ENROLL_HDR.ENRL_HDR_ID;
                    if (HR_TRM_ENROLL_HDR.ENRL_DATE != null)
                    {
                        dtpTrnDate.Text = DBMethod.ConvertDateToString(HR_TRM_ENROLL_HDR.ENRL_DATE.ToString());
                    }
                    ddlScheduleName.SelectedValue = HR_TRM_ENROLL_HDR.TRN_SCH_HDR_ID;
                    Program4Shedule();
                    ddlProgram.SelectedValue = HR_TRM_ENROLL_HDR.PROG_ID;
                    Course4Shedule();
                    ddlCourse.SelectedValue = HR_TRM_ENROLL_HDR.PROG_COURSE_ID;
                    Subject4Shedule();
                    ddlSubject.SelectedValue = HR_TRM_ENROLL_HDR.SUBJECT_ID;
                    FillSession();
                    ddlSession.SelectedValue = HR_TRM_ENROLL_HDR.SESSION_ID;
                    fillTrainer();
                    ddlTrainer.SelectedValue = HR_TRM_ENROLL_HDR.TRNR_ID;
                    getTrngSchDtlID();
                    getTrngReqDeptEmpDet();

                    ddlScheduleName.Enabled = false;
                    ddlProgram.Enabled = false;
                    ddlCourse.Enabled = false;
                    ddlSubject.Enabled = false;
                    ddlSession.Enabled = false;


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOteC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Save,Update and Delete
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_TRM_ENROLL_HDR = (HR_TRM_ENROLL_HDR)EntityData;
                }

                if (dtpTrnDate.Text != string.Empty)
                {
                    HR_TRM_ENROLL_HDR.ENRL_DATE = DBMethod.ConvertStringToDate(dtpTrnDate.Text.ToString());
                }


                HR_TRM_ENROLL_HDR.TRN_SCH_HDR_ID = ddlScheduleName.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.PROG_ID = ddlProgram.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.PROG_COURSE_ID = ddlCourse.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.SUBJECT_ID = ddlSubject.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.SESSION_ID = ddlSession.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.TRNR_ID = ddlTrainer.SelectedValue.ToString();
                HR_TRM_ENROLL_HDR.TRN_SCH_DTL_ID = hf_Trreqdtlid.Value.ToString();
                HR_TRM_ENROLL_HDR.ENRL_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                HR_TRM_ENROLL_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_TRM_ENROLL_HDR.MODIFIED_BY = this.LoggedUserName;
                    HR_TRM_ENROLL_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    HR_TRM_ENROLL_HDR.ENRL_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_059_M".ToString(), false, true);
                    HR_TRM_ENROLL_HDR.CREATED_BY = this.LoggedUserName;
                    HR_TRM_ENROLL_HDR.CREATED_DATE = DateTime.Today;
                }

                HR_TRM_ENROLL_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_TRM_ENROLL_HDR.ENRL_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    HR_TRM_ENROLL_DTL = new HR_TRM_ENROLL_DTL();
                    if (gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString().ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_ENROLL_DTL> userCtx = new DataRepository<HR_TRM_ENROLL_DTL>())
                        {
                            HR_TRM_ENROLL_DTL = userCtx.Find(r =>
                                (r.ENRL_DTL_ID == gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    CheckBox chk_select = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chk_select.Checked)
                    {
                        HR_TRM_ENROLL_DTL.TRN_SCH_HDR_ID = ddlScheduleName.SelectedValue.ToString();
                        HR_TRM_ENROLL_DTL.PROG_COURSE_ID = ddlCourse.SelectedValue.ToString();
                        HR_TRM_ENROLL_DTL.PROG_ID = ddlProgram.SelectedValue.ToString();
                        HR_TRM_ENROLL_DTL.SUBJECT_ID = ddlSubject.SelectedValue.ToString();
                        HR_TRM_ENROLL_DTL.TRNR_ID = ddlTrainer.SelectedValue.ToString();
                        HR_TRM_ENROLL_DTL.TRN_SCH_DTL_ID = hf_Trreqdtlid.Value.ToString();
                        HR_TRM_ENROLL_DTL.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();

                        HR_TRM_ENROLL_DTL.ENRL_HDR_ID = HR_TRM_ENROLL_HDR.ENRL_HDR_ID;
                        HR_TRM_ENROLL_DTL.ENABLED_FLAG = FINAppConstants.Y;

                        HR_TRM_ENROLL_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        if (gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString() != string.Empty)
                        {
                            HR_TRM_ENROLL_DTL.ENRL_DTL_ID = gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString();
                            HR_TRM_ENROLL_DTL.MODIFIED_BY = this.LoggedUserName;
                            HR_TRM_ENROLL_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_TRM_ENROLL_DTL, "U"));
                        }
                        else
                        {

                            HR_TRM_ENROLL_DTL.ENRL_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_059_D".ToString(), false, true);
                            HR_TRM_ENROLL_DTL.CREATED_BY = this.LoggedUserName;
                            HR_TRM_ENROLL_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_TRM_ENROLL_DTL, "A"));
                        }
                       // HR_TRM_ENROLL_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_TRM_ENROLL_DTL.ENRL_DTL_ID);

                        
                    }

                    else
                    {
                        if (gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ENRL_DTL_ID"].ToString() != string.Empty)
                        {
                            DBMethod.DeleteEntity<HR_TRM_ENROLL_DTL>(HR_TRM_ENROLL_DTL);
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_ENROLL_HDR, HR_TRM_ENROLL_DTL>(HR_TRM_ENROLL_HDR, tmpChildEntity, HR_TRM_ENROLL_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_ENROLL_HDR, HR_TRM_ENROLL_DTL>(HR_TRM_ENROLL_HDR, tmpChildEntity, HR_TRM_ENROLL_DTL, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlProg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow tmpgvr = gvData.FooterRow;

                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
                DropDownList ddlProg = tmpgvr.FindControl("ddlProg") as DropDownList;

                FIN.BLL.HR.Course_BLL.GetCourseBasedPgm(ref ddlCourse, ddlProg.SelectedValue);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COURSE_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        //protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();

        //        GridViewRow tmpgvr = gvData.FooterRow;

        //        DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
        //        DropDownList ddlSubject = tmpgvr.FindControl("ddlSubject") as DropDownList;

        //        FIN.BLL.HR.Course_BLL.GetSubjectBasedCourse(ref ddlSubject, ddlCourse.SelectedValue);

        //        if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
        //        {
        //            ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["SUBJECT_ID"].ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("TS_ATO45B", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
                DropDownList ddlProgram = tmpgvr.FindControl("ddlProgram") as DropDownList;
                DropDownList ddlSubject = tmpgvr.FindControl("ddlSubject") as DropDownList;
                DropDownList ddlSchedule = tmpgvr.FindControl("ddlSchedule") as DropDownList;
                DropDownList ddlStaff = tmpgvr.FindControl("ddlStaff") as DropDownList;


                FIN.BLL.HR.TrainingSchedule_BLL.GetScheduleData(ref ddlSchedule);
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse(ref ddlCourse);
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram(ref ddlProgram);
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject(ref ddlSubject);              

                Trainer_BLL.GetTrainer(ref ddlStaff);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlSchedule.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["TRN_SCH_HDR_ID"].ToString();
                    // FillProgram4Schdule(tmpgvr);
                    ddlProgram.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PROG_ID"].ToString();
                    // FillCourse4SchduleProgram(tmpgvr);
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COURSE_ID"].ToString();
                    // FillSubject4SchduleProgramCourse(tmpgvr);
                    ddlSubject.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["SUBJECT_ID"].ToString();

                    ddlStaff.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["EMP_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Enrollment ");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                AssignToBE();
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        protected void Program4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram4Shedule(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
            ddlCourse.Items.Clear();
            ddlSubject.Items.Clear();
            ddlSession.Items.Clear();
            gvData.DataSource = new DataTable();
            gvData.DataBind();

        }
        protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetProgram(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram4Shedule(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
                Program4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_SCHEDULE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void Course4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse4SheduleProg(ref ddlCourse, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString());
            ddlSubject.Items.Clear();
            ddlSession.Items.Clear();
            gvData.DataSource = new DataTable();
            gvData.DataBind();
        }
        protected void ddlProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetCourse(ref ddlCourse, ddlProgram.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse4SheduleProg(ref ddlCourse, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString());
                Course4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_PROGRAM", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void Subject4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject4ScheduleprogCourse(ref ddlSubject, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString());
            
            ddlSession.Items.Clear();
            gvData.DataSource = new DataTable();
            gvData.DataBind();
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetSubject(ref ddlSubject, ddlCourse.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject4ScheduleprogCourse(ref ddlSubject, ddlScheduleName.SelectedValue.ToString(),ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString());
                Subject4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_COURSE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            getTrngSchDtlID();
            getHeaderData();
            getTrngReqDeptEmpDet();
            fillTrainer();

        }

        private void fillTrainer()
        {
            Trainer_BLL.GetTrainer_ForTrainingEnrolment(ref ddlTrainer, ddlSubject.SelectedValue, ddlSession.SelectedValue);
        }

        private void getHeaderData()
        {
            using (IRepository<HR_TRM_ENROLL_HDR> userCtx = new DataRepository<HR_TRM_ENROLL_HDR>())
            {
                HR_TRM_ENROLL_HDR = userCtx.Find(r =>
                    (r.TRN_SCH_DTL_ID ==hf_Trreqdtlid.Value)
                    ).SingleOrDefault();
            }
            if (HR_TRM_ENROLL_HDR != null)
            {
                EntityData = HR_TRM_ENROLL_HDR;
                Master.StrRecordId = HR_TRM_ENROLL_HDR.ENRL_HDR_ID;
                Master.Mode = FINAppConstants.Update;
            }
        }
        protected void getTrngSchDtlID()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtTrngDtlID = new DataTable();
                dtTrngDtlID = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingSchedule_DAL.GetTrngSchDtlID4ScheduleprogCourseSubjSession(ddlScheduleName.SelectedValue.ToString(),
                              ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString(), ddlSubject.SelectedValue.ToString(), ddlSession.SelectedItem.Text.ToString())).Tables[0];

                if (dtTrngDtlID.Rows.Count > 0)
                {
                    hf_Trreqdtlid.Value = dtTrngDtlID.Rows[0]["TRN_SCH_DTL_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_SESSION", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void getTrngReqDeptEmpDet()
        {
            DataTable dt_data = FIN.BLL.HR.TrainingEnrollment_BLL.getTrngReqDeptEmpDet(hf_Trreqdtlid.Value.ToString());
            gvData.DataSource = dt_data;
            gvData.DataBind();
        }
        protected void ddlSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSession();
        }
        private void FillSession()
        {
            Lookup_BLL.GetLookUpValues(ref ddlSession, "Session");
        }
    }
}