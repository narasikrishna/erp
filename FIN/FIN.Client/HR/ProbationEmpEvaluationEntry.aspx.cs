﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ProbationEmpEvaluationEntry : PageBase
    {
        HR_PROBATION_EMP_DEPT_HDR hR_PROBATION_EMP_DEPT_HDR = new HR_PROBATION_EMP_DEPT_HDR();
        HR_PROBATION_EMP_DEPT_DTL hR_PROBATION_EMP_DEPT_DTL = new HR_PROBATION_EMP_DEPT_DTL();
        HR_PROBATION_EMP_DEPT_COMMENTS hR_PROBATION_EMP_DEPT_COMMENTS = new HR_PROBATION_EMP_DEPT_COMMENTS();
        HR_PROBATION_EMP_IMP hR_PROBATION_EMP_IMP = new HR_PROBATION_EMP_IMP();


        DataTable dtGridData = new DataTable();
        DataTable dtGridData2 = new DataTable();
        DataTable dtGridData3 = new DataTable();

        Department_BLL Department_BLL = new Department_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                chkLastLevel.Checked = false;
                Startup();
                FillComboBox();
                EntityData = null;


                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpEvalDAL.GetProbationEmpDeptdtls(Master.StrRecordId)).Tables[0];
                dtGridData2 = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpEvalDAL.GetProbationEmpDeptCommentsdtls(Master.StrRecordId)).Tables[0];
                dtGridData3 = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpEvalDAL.GetProbationEmpImpdtls(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_PROBATION_EMP_DEPT_HDR> userCtx = new DataRepository<HR_PROBATION_EMP_DEPT_HDR>())
                    {
                        hR_PROBATION_EMP_DEPT_HDR = userCtx.Find(r =>
                            (r.PROB_LINK_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_PROBATION_EMP_DEPT_HDR;


                    ddlDepartment.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.PROB_DEPT_ID;
                    fillEmp();
                    ddlEmployee.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.PROB_EMP_ID;
                    fillProbCompetency();
                    ddlProbCompt.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.PROB_ID;
                    fillAppraiserName();
                    ddlAppraiser.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.PROB_APPR_ID;


                    ddlLineStatus.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.PROB_LINE_STATUS;

                    if (hR_PROBATION_EMP_DEPT_HDR.LAST_LEVEL == "1")
                    {
                        chkLastLevel.Checked = true;
                        ddlRecomendation.SelectedValue = hR_PROBATION_EMP_DEPT_HDR.ATTRIBUTE1;
                        RecomChange();
                        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
                        using (IRepository<HR_EMPLOYEES> userCtx = new DataRepository<HR_EMPLOYEES>())
                        {
                            hR_EMPLOYEES = userCtx.Find(r =>
                                (r.EMP_ID == ddlEmployee.SelectedValue)
                                ).SingleOrDefault();
                        }
                        if (hR_EMPLOYEES != null)
                        {
                            txtConfDate.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.CONFIRMATION_DATE.ToString());
                        }
                        ddlRecomendation.Enabled = true;
                        gvEmpImp.Enabled = true;
                    }
                    else
                    {
                        chkLastLevel.Checked = false;
                        ddlRecomendation.Enabled = false;
                        gvEmpImp.Enabled = false;
                    }


                }

                BindGrid(dtGridData);
                BindGrid2(dtGridData2);
                BindGrid3(dtGridData3);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpEval", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_PROBATION_EMP_DEPT_HDR = (HR_PROBATION_EMP_DEPT_HDR)EntityData;
                }



                hR_PROBATION_EMP_DEPT_HDR.PROB_DEPT_ID = ddlDepartment.SelectedValue;
                hR_PROBATION_EMP_DEPT_HDR.PROB_EMP_ID = ddlEmployee.SelectedValue;
                hR_PROBATION_EMP_DEPT_HDR.PROB_ID = ddlProbCompt.SelectedValue;
                hR_PROBATION_EMP_DEPT_HDR.PROB_APPR_ID = ddlAppraiser.SelectedValue;

                hR_PROBATION_EMP_DEPT_HDR.PROB_LINE_STATUS = ddlLineStatus.SelectedValue;
                if (chkLastLevel.Checked == true)
                {
                    hR_PROBATION_EMP_DEPT_HDR.LAST_LEVEL = FINAppConstants.Y;
                    hR_PROBATION_EMP_DEPT_HDR.ATTRIBUTE1 = ddlRecomendation.SelectedValue;
                }
                else
                {
                    hR_PROBATION_EMP_DEPT_HDR.LAST_LEVEL = FINAppConstants.N;
                }
                hR_PROBATION_EMP_DEPT_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_PROBATION_EMP_DEPT_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_PROBATION_EMP_DEPT_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    hR_PROBATION_EMP_DEPT_HDR.CREATED_BY = this.LoggedUserName;
                    hR_PROBATION_EMP_DEPT_HDR.CREATED_DATE = DateTime.Today;
                    hR_PROBATION_EMP_DEPT_HDR.PROB_LINK_ID = FINSP.GetSPFOR_SEQCode("HR_099".ToString(), false, true);

                }

                hR_PROBATION_EMP_DEPT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PROBATION_EMP_DEPT_HDR.PROB_LINK_ID);
                //Save Detail Table


                var tmpChildEntity = new List<Tuple<object, string>>();
                var tmpChildEntity2 = new List<Tuple<object, string>>();
                var tmpChildEntity3 = new List<Tuple<object, string>>();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }

                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_PROBATION_EMP_DEPT_DTL = new HR_PROBATION_EMP_DEPT_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_PROBATION_EMP_DEPT_DTL> userCtx = new DataRepository<HR_PROBATION_EMP_DEPT_DTL>())
                        {
                            hR_PROBATION_EMP_DEPT_DTL = userCtx.Find(r =>
                                (r.PROB_LINK_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }



                    hR_PROBATION_EMP_DEPT_DTL.ATTRIBUTE1 = (iLoop + 1).ToString();	
                    hR_PROBATION_EMP_DEPT_DTL.PROB_DTL_ID = dtGridData.Rows[iLoop]["PROB_DTL_ID"].ToString();
                    hR_PROBATION_EMP_DEPT_DTL.PROB_EVAL_RATING = int.Parse(dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString());

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_PROBATION_EMP_DEPT_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_PROBATION_EMP_DEPT_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    hR_PROBATION_EMP_DEPT_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_PROBATION_EMP_DEPT_DTL.PROB_LINK_ID = hR_PROBATION_EMP_DEPT_HDR.PROB_LINK_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_PROBATION_EMP_DEPT_DTL.PROB_LINK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString() != string.Empty)
                        {
                            hR_PROBATION_EMP_DEPT_DTL.PROB_LINK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.PROB_LINK_DTL_ID].ToString();
                            hR_PROBATION_EMP_DEPT_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_DEPT_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_PROBATION_EMP_DEPT_DTL.PROB_LINK_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_099_D".ToString(), false, true);
                            hR_PROBATION_EMP_DEPT_DTL.CREATED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_DEPT_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_DTL, FINAppConstants.Add));
                        }
                    }

                }

                for (int jLoop = 0; jLoop < dtGridData2.Rows.Count; jLoop++)
                {
                    hR_PROBATION_EMP_DEPT_COMMENTS = new HR_PROBATION_EMP_DEPT_COMMENTS();
                    if (dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_PROBATION_EMP_DEPT_COMMENTS> userCtx = new DataRepository<HR_PROBATION_EMP_DEPT_COMMENTS>())
                        {
                            hR_PROBATION_EMP_DEPT_COMMENTS = userCtx.Find(r =>
                                (r.PROB_COMMENT_ID == dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }



                    //hR_PROBATION_EMP_DEPT_COMMENTS.ATTRIBUTE1 = dtGridData2.Rows[jLoop]["LINE_NUM_2"].ToString();
                    hR_PROBATION_EMP_DEPT_COMMENTS.ATTRIBUTE1 = (jLoop + 1).ToString();
                    hR_PROBATION_EMP_DEPT_COMMENTS.PROB_COMMENT = dtGridData2.Rows[jLoop]["PROB_COMMENT"].ToString();



                    hR_PROBATION_EMP_DEPT_COMMENTS.ENABLED_FLAG = FINAppConstants.Y;


                    hR_PROBATION_EMP_DEPT_COMMENTS.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_PROBATION_EMP_DEPT_COMMENTS.PROB_LINK_ID = hR_PROBATION_EMP_DEPT_HDR.PROB_LINK_ID;

                    if (dtGridData2.Rows[jLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_PROBATION_EMP_DEPT_COMMENTS.PROB_COMMENT_ID = dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString();
                        tmpChildEntity2.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_COMMENTS, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString() != "0" && dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString() != string.Empty)
                        {
                            hR_PROBATION_EMP_DEPT_COMMENTS.PROB_COMMENT_ID = dtGridData2.Rows[jLoop]["PROB_COMMENT_ID"].ToString();
                            hR_PROBATION_EMP_DEPT_COMMENTS.MODIFIED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_DEPT_COMMENTS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity2.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_COMMENTS, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_PROBATION_EMP_DEPT_COMMENTS.PROB_COMMENT_ID = FINSP.GetSPFOR_SEQCode("HR_099_C".ToString(), false, true);
                            hR_PROBATION_EMP_DEPT_COMMENTS.CREATED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_DEPT_COMMENTS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity2.Add(new Tuple<object, string>(hR_PROBATION_EMP_DEPT_COMMENTS, FINAppConstants.Add));
                        }
                    }

                }

                for (int kLoop = 0; kLoop < dtGridData3.Rows.Count; kLoop++)
                {
                    hR_PROBATION_EMP_IMP = new HR_PROBATION_EMP_IMP();
                    if (dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_PROBATION_EMP_IMP> userCtx = new DataRepository<HR_PROBATION_EMP_IMP>())
                        {
                            hR_PROBATION_EMP_IMP = userCtx.Find(r =>
                                (r.PROB_IMP_ID == dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }




                    hR_PROBATION_EMP_IMP.PROB_IMP_COMMENTS = dtGridData3.Rows[kLoop]["PROB_IMP_COMMENTS"].ToString();



                    hR_PROBATION_EMP_IMP.ENABLED_FLAG = FINAppConstants.Y;


                    hR_PROBATION_EMP_IMP.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_PROBATION_EMP_IMP.ATTRIBUTE1 = hR_PROBATION_EMP_DEPT_HDR.PROB_LINK_ID;

                    if (dtGridData3.Rows[kLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_PROBATION_EMP_IMP.PROB_IMP_ID = dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString();
                        tmpChildEntity3.Add(new Tuple<object, string>(hR_PROBATION_EMP_IMP, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString() != "0" && dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString() != string.Empty)
                        {
                            hR_PROBATION_EMP_IMP.PROB_IMP_ID = dtGridData3.Rows[kLoop]["PROB_IMP_ID"].ToString();
                            hR_PROBATION_EMP_IMP.MODIFIED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_IMP.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity3.Add(new Tuple<object, string>(hR_PROBATION_EMP_IMP, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_PROBATION_EMP_IMP.PROB_IMP_ID = FINSP.GetSPFOR_SEQCode("HR_099_I".ToString(), false, true);
                            hR_PROBATION_EMP_IMP.CREATED_BY = this.LoggedUserName;
                            hR_PROBATION_EMP_IMP.CREATED_DATE = DateTime.Today;
                            tmpChildEntity3.Add(new Tuple<object, string>(hR_PROBATION_EMP_IMP, FINAppConstants.Add));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveFourEntity<HR_PROBATION_EMP_DEPT_HDR, HR_PROBATION_EMP_DEPT_DTL, HR_PROBATION_EMP_DEPT_COMMENTS, HR_PROBATION_EMP_IMP>(hR_PROBATION_EMP_DEPT_HDR, tmpChildEntity, hR_PROBATION_EMP_DEPT_DTL, tmpChildEntity2, hR_PROBATION_EMP_DEPT_COMMENTS, tmpChildEntity3, hR_PROBATION_EMP_IMP);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveFourEntity<HR_PROBATION_EMP_DEPT_HDR, HR_PROBATION_EMP_DEPT_DTL, HR_PROBATION_EMP_DEPT_COMMENTS, HR_PROBATION_EMP_IMP>(hR_PROBATION_EMP_DEPT_HDR, tmpChildEntity, hR_PROBATION_EMP_DEPT_DTL, tmpChildEntity2, hR_PROBATION_EMP_DEPT_COMMENTS, tmpChildEntity3, hR_PROBATION_EMP_IMP, true);
                            savedBool = true;
                            break;
                        }
                }

                if (txtConfDate.Text.ToString().Length > 0)
                {
                    DBMethod.ExecuteNonQuery("UPDATE HR_EMPLOYEES SET CONFIRMATION_DATE=TO_DATE('" + txtConfDate.Text + "','DD/MM/YYYY') WHERE EMP_ID='" + ddlEmployee.SelectedValue + "'");

                }
                            


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_OD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {
           
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
            
            
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlLineStatus, "STAT");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlRecomendation, "RECOMMENDATION");
            

        }

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid2(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData2"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                   
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvEmpDeptCmnts.DataSource = dt_tmp;
                gvEmpDeptCmnts.DataBind();
                GridViewRow gvr = gvEmpDeptCmnts.FooterRow;
               // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpDeptCmts", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid3(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData3"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvEmpImp.DataSource = dt_tmp;
                gvEmpImp.DataBind();
                GridViewRow gvr = gvEmpImp.FooterRow;
                // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpImp", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEmpDeptCmnts_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }
                gvEmpDeptCmnts.EditIndex = -1;

                BindGrid2(dtGridData2);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpDeptCmts", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEmpImp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }
                gvEmpImp.EditIndex = -1;

                BindGrid3(dtGridData3);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpImp", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmpDeptCmnts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvEmpDeptCmnts.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl2(gvr, dtGridData2, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData2.Rows.Add(drList);
                        BindGrid2(dtGridData2);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmpImp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvEmpImp.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl3(gvr, dtGridData3, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData3.Rows.Add(drList);
                        BindGrid3(dtGridData3);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlCompTencyName = gvr.FindControl("ddlCompTencyName") as DropDownList;
            TextBox txtLineNum = gvr.FindControl("txtLineNum") as TextBox;
           // TextBox txtRating = gvr.FindControl("txtRating") as TextBox;
            DropDownList ddlRating = gvr.FindControl("ddlRating") as DropDownList;
            TextBox txtLevelDesc = gvr.FindControl("txtLevelDesc") as TextBox;
            
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PROB_LINK_DTL_ID] = "0";
                txtLineNum.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlCompTencyName;
            slControls[1] = ddlRating;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Competency_Name_P"] + " ~ " + Prop_File_Data["Rating_P"] + "";
            
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            //string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementName.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            if (ddlCompTencyName.SelectedItem != null)
            {
                drList[FINColumnConstants.PROB_DTL_ID] = ddlCompTencyName.SelectedItem.Value;
                drList["COM_DESC"] = ddlCompTencyName.SelectedItem.Text;
            }
            if (ddlRating.SelectedItem != null)
            {
                drList[FINColumnConstants.LOOKUP_ID] = ddlRating.SelectedItem.Value;
                drList[FINColumnConstants.LOOKUP_NAME] = ddlRating.SelectedItem.Text;
            }
            if (txtLevelDesc.Text != null)
            {
                drList["COM_LEVEL_DESC"] = txtLevelDesc.Text;
            }

            //drList["PROB_EVAL_RATING"] = txtRating.Text;
            drList["LINE_NUM_1"] = txtLineNum.Text;

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }


        private DataRow AssignToGridControl2(GridViewRow gvr, DataTable tmpdtGridData2, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            
            TextBox txtLineNum2 = gvr.FindControl("txtLineNum2") as TextBox;
            TextBox txtComments = gvr.FindControl("txtComments") as TextBox;
                       

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData2.Copy();
            if (GMode == "A")
            {
                drList = dtGridData2.NewRow();
                drList["PROB_COMMENT_ID"] = "0";
                txtLineNum2.Text = (tmpdtGridData2.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData2.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            drList["PROB_COMMENT"] = txtComments.Text;
            drList["LINE_NUM_2"] = txtLineNum2.Text;

            

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        private DataRow AssignToGridControl3(GridViewRow gvr, DataTable tmpdtGridData3, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            
            TextBox txtComments3 = gvr.FindControl("txtComments3") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData3.Copy();
            if (GMode == "A")
            {
                drList = dtGridData3.NewRow();
                drList["PROB_IMP_ID"] = "0";
                
            }
            else
            {
                drList = dtGridData3.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            drList["PROB_IMP_COMMENTS"] = txtComments3.Text;
            


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEmpDeptCmnts_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvEmpDeptCmnts.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl2(gvr, dtGridData2, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvEmpDeptCmnts.EditIndex = -1;
                    BindGrid2(dtGridData2);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEmpImp_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvEmpImp.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl3(gvr, dtGridData3, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvEmpImp.EditIndex = -1;
                    BindGrid3(dtGridData3);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void gvEmpDeptCmnts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }
                dtGridData2.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid2(dtGridData2);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmpImp_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }
                dtGridData3.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid3(dtGridData3);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvEmpDeptCmnts_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData2"] != null)
                {
                    dtGridData2 = (DataTable)Session["GridData2"];
                }
                gvEmpDeptCmnts.EditIndex = e.NewEditIndex;
                BindGrid2(dtGridData2);
                GridViewRow gvr = gvEmpDeptCmnts.Rows[e.NewEditIndex];
               // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvEmpImp_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData3"] != null)
                {
                    dtGridData3 = (DataTable)Session["GridData3"];
                }
                gvEmpImp.EditIndex = e.NewEditIndex;
                BindGrid3(dtGridData3);
                GridViewRow gvr = gvEmpImp.Rows[e.NewEditIndex];
                // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmpDeptCmnts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvEmpDeptCmnts.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpEval", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmpImp_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvEmpImp.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ProbEmpEval", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                               
                
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCompTencyName = tmpgvr.FindControl("ddlCompTencyName") as DropDownList;
                DropDownList ddlRating = tmpgvr.FindControl("ddlRating") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlRating, "RATING");

                FIN.BLL.HR.ProbEmpCompetencyBLL.GetProbation_Competency_Name(ref ddlCompTencyName, ddlEmployee.SelectedValue);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCompTencyName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PROB_DTL_ID].ToString();
                    ddlRating.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvEmpDeptCmnts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvEmpImp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee Evaluation");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                
                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);                
            }
                
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillEmp();
        }

        private void fillEmp()
        {
            FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmployee, ddlDepartment.SelectedValue);
        }

        protected void chkLastLevel_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLastLevel.Checked == true)
            {
                ddlRecomendation.Enabled = true;
                gvEmpImp.Enabled = true;
            }
            else
            {
                ddlRecomendation.Enabled = false;
                gvEmpImp.Enabled = false;
            }
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gvData.FooterRow;
            fillcompetency(gvr);
            fillProbCompetency();
            fillAppraiserName();
        }

        private void fillAppraiserName()
        {
            FIN.BLL.HR.ProbEmpEvalBLL.fn_ProbAppraiser4Emp(ref ddlAppraiser, ddlEmployee.SelectedValue);
            
        }
        private void fillcompetency(GridViewRow gvr)
        {
            DataTable dtCompetencyDtl = new DataTable();
            DropDownList ddlCompTencyName = gvr.FindControl("ddlCompTencyName") as DropDownList;

            FIN.BLL.HR.ProbEmpCompetencyBLL.GetProbation_Competency_Name(ref ddlCompTencyName,ddlEmployee.SelectedValue);
            dtCompetencyDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpEvalDAL.GetCompetencyDtl(ddlEmployee.SelectedValue)).Tables[0];
            BindGrid(dtCompetencyDtl);

        }
        private void fillProbCompetency()
        {
            FIN.BLL.HR.ProbEmpCompetencyBLL.GetProbation_Emp_Name(ref ddlProbCompt,ddlEmployee.SelectedValue);
        }

        protected void ddlCompTencyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLeveldesc = new DataTable();
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
              DropDownList ddlCompTencyName = gvr.FindControl("ddlCompTencyName") as DropDownList;
            TextBox txtLevelDesc = gvr.FindControl("txtLevelDesc") as TextBox;

            dtLeveldesc = DBMethod.ExecuteQuery(FIN.DAL.HR.ProbEmpEvalDAL.GetLeveldesc(ddlCompTencyName.SelectedValue)).Tables[0];
              if (dtLeveldesc.Rows.Count > 0)
              {
                  txtLevelDesc.Text = dtLeveldesc.Rows[0]["COM_LEVEL_DESC"].ToString();
              }
        }

        protected void ddlAppraiser_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (ddlAppraiser.SelectedIndex == ddlAppraiser.Items.Count)
            {
                chkLastLevel.Checked = true;
                chkLastLevel_CheckedChanged(chkLastLevel, e);
            }
        }

        protected void ddlRecomendation_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecomChange();
        }

        private void RecomChange()
        {
            if (ddlRecomendation.SelectedIndex == 1 && chkLastLevel.Checked)
            {
                divConfDate.Visible = true;
                divtxtConfDate.Visible = true;
                txtConfDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
            }
            else
            {
                divConfDate.Visible = false;
                divtxtConfDate.Visible = false;
                txtConfDate.Text = "";
            }
        }
    }
}