﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveReturnEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveReturnEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 170px" id="Div1">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
                
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 170px" id="lblBankName">
                Leave Request Identifier
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlLevEncash" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlLevEncash_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 170px" id="lblBankShortName">
                Return Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 130px">
                <asp:TextBox ID="txtDate" CssClass="validate[required, custom[ReqDateDDMMYYY]]  RequiredField txtBox" runat="server"
                    TabIndex="3" ></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="lblShortName">
                No of Days
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txtNoofdys" CssClass="  txtBox_N" Enabled="false" runat="server"
                    TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtNoofdys" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 170px" id="lblNoofDays">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 130px">
                <asp:TextBox ID="txtFromdat" CssClass="  txtBox" Enabled="false" runat="server"
                    TabIndex="5"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="Div7">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txttodate" CssClass=" txtBox" Enabled="false" runat="server"
                    TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div runat="server" id="IdPayPrd">
            <div class="lblBox LNOrient" style="width: 170px" id="Div9">
                Leave Adjustment
            </div>
            <div class="divtxtBox  LNOrient" style="width: 130px">
                <asp:TextBox ID="txtlevbaldys" CssClass=" txtBox_N" MaxLength="3" runat="server"
                    TabIndex="7"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtlevbaldys" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <asp:HiddenField ID="hfEligamt" runat="server" />
        <asp:HiddenField ID="hf_AvailableLeave" runat="server" />
        <asp:HiddenField ID="hf_editMode_leave" runat="server" />
        <asp:HiddenField ID="hf_finYear" runat="server" />
        <asp:HiddenField ID="hf_leave_id" runat="server" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
   <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
