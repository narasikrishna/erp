﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class InterviewResultsEntry : PageBase
    {
        HR_INTERVIEWS_RESULTS hR_INTERVIEWS_RESULTS = new HR_INTERVIEWS_RESULTS();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        HR_STAFF_ATTENDANCE HR_STAFF_ATTENDANCE = new HR_STAFF_ATTENDANCE();
        DataTable dtData = new DataTable();
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL_IR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }



            UserRightsChecking();



        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                //gvData.DataSource = dt_tmp;
                //gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;


                dtGridData = DBMethod.ExecuteQuery(StaffAttendance_DAL.GetStaffAttedDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    hR_INTERVIEWS_RESULTS = FIN.BLL.HR.InterviewResult_BLL.getClassEntity(Master.StrRecordId);



                    EntityData = hR_INTERVIEWS_RESULTS;

                    ddlvacancy.SelectedValue = hR_INTERVIEWS_RESULTS.VAC_ID;
                    fillapplicant();
                    ddlApplicant.SelectedValue = hR_INTERVIEWS_RESULTS.CANDIDATE_NAME;
                    BindInterviewStatus();
                    txtComments.Text = hR_INTERVIEWS_RESULTS.INT_COMMENTS;
                    chkActive.Checked = false;
                    ddlResult.SelectedValue = hR_INTERVIEWS_RESULTS.INT_RESULT;
                    if (hR_INTERVIEWS_RESULTS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IR_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void FillComboBox()
        {
            //FIN.BLL.HR.Interview_BLL.fn_GetInterview(ref ddlInterviewName); 
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlResult, "INT_RESULT");
            Interview_BLL.fn_GetVacancyNM(ref ddlvacancy);
        }

        private void GetInterviewDetails()
        {
            //DataTable dt_INT_Det = new DataTable();
            //dt_INT_Det =FIN.BLL.HR.Interview_BLL.getInterview4InterveiwId(ddlInterviewName.SelectedValue.ToString());
            //if (dt_INT_Det.Rows.Count > 0)
            //{
            //    txtVacancyname.Text = dt_INT_Det.Rows[0]["VAC_NAME"].ToString();
            //    hf_VAC_ID.Value = dt_INT_Det.Rows[0]["VAC_ID"].ToString();
            //    txtCandidate.Text = dt_INT_Det.Rows[0]["APP_FIRST_NAME"].ToString();
            //    txtContact.Text = dt_INT_Det.Rows[0]["APP_CONTACT_NO"].ToString();
            //}
        }

        protected void ddlInterviewName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GetInterviewDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IntChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_INTERVIEWS_RESULTS = (HR_INTERVIEWS_RESULTS)EntityData;
                }
                hR_INTERVIEWS_RESULTS.INT_ID = hf_INVID.Value;
                hR_INTERVIEWS_RESULTS.VAC_ID = ddlvacancy.SelectedValue.ToString();
                hR_INTERVIEWS_RESULTS.CANDIDATE_NAME = ddlApplicant.SelectedValue;
                hR_INTERVIEWS_RESULTS.CANDIDATE_CONTACT_NO = "";
                hR_INTERVIEWS_RESULTS.INT_RESULT = ddlResult.SelectedValue.ToString();
                hR_INTERVIEWS_RESULTS.INT_COMMENTS = txtComments.Text;
                hR_INTERVIEWS_RESULTS.INT_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_INTERVIEWS_RESULTS.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                if (chkActive.Checked)
                {
                    hR_INTERVIEWS_RESULTS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_INTERVIEWS_RESULTS.MODIFIED_BY = this.LoggedUserName;
                    hR_INTERVIEWS_RESULTS.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_INTERVIEWS_RESULTS.RESULT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_046.ToString(), false, true);
                    hR_INTERVIEWS_RESULTS.CREATED_BY = this.LoggedUserName;
                    hR_INTERVIEWS_RESULTS.CREATED_DATE = DateTime.Today;
                }
                hR_INTERVIEWS_RESULTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INTERVIEWS_RESULTS.RESULT_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IR_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                LeaveApplication_BLL.DeleteEntity(Master.StrRecordId);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_BTNDEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();
                slControls[0] = ddlvacancy;
                slControls[1] = ddlApplicant;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));



                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList";
                string strMessage = Prop_File_Data["Interview_Name_P"] + " ~ " + Prop_File_Data["Result_P"] + "";
                //string strMessage = "Interview Name ~ Result";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                if (ErrorCollection.Count > 0)
                    return;

                ErrorCollection.Clear();

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                string str_Error = FIN.BLL.HR.InterviewResult_BLL.ValidateEntity(hR_INTERVIEWS_RESULTS);
                if (str_Error.Length > 0)
                {
                    ErrorCollection.Add("EntityValidation_AG", str_Error);
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                // ProReturn = FIN.DAL.HR.InterviewResult_DAL.GetSPFOR_DUPLICATE_CHECK(hR_INTERVIEWS_RESULTS.RESULT_ID, ddlInterviewName.SelectedValue.ToString());

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("LEAVAPP", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                FIN.BLL.HR.InterviewResult_BLL.SaveEntity(hR_INTERVIEWS_RESULTS, Master.Mode, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_IR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlvacancy_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillapplicant();
        }

        private void fillapplicant()
        {
            Interview_BLL.fn_GetApplicantNM_basedon_vacancy(ref ddlApplicant, ddlvacancy.SelectedValue);
        }

        protected void ddlApplicant_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindInterviewStatus();
        }
        private void BindInterviewStatus()
        {
            DataTable dt_intDet = DBMethod.ExecuteQuery(InterviewResult_DAL.getCriteraEvalDet(ddlApplicant.SelectedValue)).Tables[0];
            if (dt_intDet.Rows.Count > 0)
            {
                hf_INVID.Value = dt_intDet.Rows[0]["INT_ID"].ToString();
            }
            gvData.DataSource = dt_intDet;
            gvData.DataBind();

            DataTable dt_status = DBMethod.ExecuteQuery(InterviewResult_DAL.GetInterviewStatus(ddlApplicant.SelectedValue)).Tables[0];
            if (dt_status.Rows.Count > 0)
            {

            }
            gvstatus.DataSource = dt_status;
            gvstatus.DataBind();
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = DBMethod.ExecuteQuery("select * from dual where " + gvData.DataKeys[e.Row.RowIndex].Values["CONDITIONS"].ToString()).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    e.Row.Cells[5].Text = "Success";
                }
                else
                {
                    e.Row.Cells[5].Text = "Fail";
                }
            }
        }

    }
}
