﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TrainingAgreementEntry : PageBase
    {
        HR_TRM_AGR_HDR hR_TRM_AGR_HDR = new HR_TRM_AGR_HDR();
        HR_TRM_AGR_DTL hR_TRM_AGR_DTL = new HR_TRM_AGR_DTL();


        DataTable dtGridData = new DataTable();
        DataTable dtTrmAGRdTLGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session["AgrDtlGridData"] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        #endregion Pageload
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void FillComboBox()
        {

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session["GridData"] = null;
                Session["rowindex"] = null;



                dtGridData = DBMethod.ExecuteQuery(TrainingAgreement_DAL.GetTrainingAgreementHdrDtl(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session["GridData"] != null)
            {
                dtGridData = (DataTable)Session["GridData"];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            System.Collections.SortedList slControls_n = new System.Collections.SortedList();

            TextBox txtLowlimit = gvr.FindControl("txtLowlimit") as TextBox;
            TextBox txtHighlimit = gvr.FindControl("txtHighlimit") as TextBox;
            TextBox dtpFromdt = gvr.FindControl("dtpFromdt") as TextBox;
            TextBox dtpToDt = gvr.FindControl("dtpToDt") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["AGR_HDR_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }


            slControls[0] = txtLowlimit;
            slControls[1] = txtHighlimit;
            slControls[2] = dtpFromdt;
            slControls[3] = dtpFromdt;
            slControls[4] = dtpToDt;


            ErrorCollection.Clear();

            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;

            string strMessage = "Low Limit ~ High Limit ~ From Date ~ From Date ~ To Date";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            drList["AGR_LOW_LIMIT"] = txtLowlimit.Text;
            drList["AGR_HIGH_LIMIT"] = txtHighlimit.Text;

            if (dtpFromdt.Text.ToString().Length > 0)
            {
                drList["AGR_EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtpFromdt.Text.ToString());
            }

            if (dtpToDt.Text.ToString().Length > 0)
            {

                drList["AGR_EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtpToDt.Text.ToString());
            }
            else
            {
                drList["AGR_EFFECTIVE_TO_DT"] = DBNull.Value;
            }

            drList[FINColumnConstants.ENABLED_FLAG] = chkact.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList["DELETED"] = FINAppConstants.N;

            return drList;
        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList["DELETED"] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        #endregion

        //private void fn_POData()
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();

        //        DropDownList ddlItemName = new DropDownList();

        //        ddlItemName.ID = "ddlItemName";

        //        if (gvData.FooterRow != null)
        //        {
        //            if (gvData.EditIndex < 0)
        //            {
        //                ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
        //            }
        //            else
        //            {
        //                ddlItemName = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlItemName");
        //            }
        //        }
        //        else
        //        {
        //            ddlItemName = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlItemName");
        //        }
        //        DataTable dtData = new DataTable();
        //        if (ddlItemName.SelectedValue != string.Empty && ddlItemName.SelectedValue != "0")
        //        {
        //            dtData = DBMethod.ExecuteQuery(PurchaseItemReceiptDAL.GetPOData(ddlPoNumber.SelectedValue.ToString())).Tables[0];
        //            if (dtData != null)
        //            {
        //                if (dtData.Rows.Count > 0)
        //                {
        //                    ddlItemName.SelectedValue = dtData.Rows[0]["PO_ITEM_ID"].ToString();
        //                    hdItemId.Value = dtData.Rows[0]["PO_ITEM_ID"].ToString();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("MIE", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
        //        }
        //    }
        //}

        //private void BindLotGrid(DataTable dtData)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();


        //        bol_rowVisiable = false;
        //        Session[FINSessionConstants.LotGridData] = dtData;
        //        DataTable dt_tmp = dtData.Copy();
        //        if (dt_tmp.Rows.Count == 0)
        //        {
        //            DataRow dr = dt_tmp.NewRow();
        //            dr[0] = "0";
        //            dt_tmp.Rows.Add(dr);
        //            bol_rowVisiable = true;
        //        }
        //        gvLot.DataSource = dt_tmp;
        //        gvLot.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RLD_BG", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
        //        }
        //    }
        //}




        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                var tmpSecondChildEntity = new List<Tuple<object, string>>();
                if (EntityData != null)
                {
                    hR_TRM_AGR_HDR = (HR_TRM_AGR_HDR)EntityData;
                }



                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRM_AGR_HDR = new HR_TRM_AGR_HDR();

                    if (dtGridData.Rows[iLoop]["AGR_HDR_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["AGR_HDR_ID"].ToString() != string.Empty)
                    {

                        using (IRepository<HR_TRM_AGR_HDR> userCtx = new DataRepository<HR_TRM_AGR_HDR>())
                        {
                            hR_TRM_AGR_HDR = userCtx.Find(r =>
                                (r.AGR_HDR_ID == dtGridData.Rows[iLoop]["AGR_HDR_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_TRM_AGR_HDR.AGR_LOW_LIMIT = decimal.Parse(dtGridData.Rows[iLoop]["AGR_LOW_LIMIT"].ToString());
                    hR_TRM_AGR_HDR.AGR_HIGH_LIMIT = decimal.Parse(dtGridData.Rows[iLoop]["AGR_HIGH_LIMIT"].ToString());
                    if (dtGridData.Rows[iLoop]["AGR_EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        hR_TRM_AGR_HDR.AGR_EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["AGR_EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["AGR_EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        hR_TRM_AGR_HDR.AGR_EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["AGR_EFFECTIVE_TO_DT"].ToString());
                    }

                    hR_TRM_AGR_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    hR_TRM_AGR_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_HDR, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["AGR_HDR_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["AGR_HDR_ID"].ToString() != string.Empty)
                        {
                            hR_TRM_AGR_HDR.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_HDR, FINAppConstants.Update));
                        }
                        else
                        {
                            hR_TRM_AGR_HDR.AGR_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_106_M".ToString(), false, true);
                            hR_TRM_AGR_HDR.CREATED_BY = this.LoggedUserName;
                            hR_TRM_AGR_HDR.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_HDR, FINAppConstants.Add));
                        }
                    }



                    //Warehouse Entry
                    if (Session["AgrDtlGridData"] != null)
                    {
                        dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
                        if (dtTrmAGRdTLGridData.Rows.Count > 0)
                        {
                            for (int jLoop = 0; jLoop < dtTrmAGRdTLGridData.Rows.Count; jLoop++)
                            {

                                hR_TRM_AGR_DTL = new HR_TRM_AGR_DTL();

                                if (dtTrmAGRdTLGridData.Rows[iLoop]["AGR_DTL_ID"].ToString() != "0" && dtTrmAGRdTLGridData.Rows[iLoop]["AGR_DTL_ID"].ToString() != string.Empty)
                                {

                                    using (IRepository<HR_TRM_AGR_DTL> userCtx = new DataRepository<HR_TRM_AGR_DTL>())
                                    {
                                        hR_TRM_AGR_DTL = userCtx.Find(r =>
                                            (r.AGR_DTL_ID == dtTrmAGRdTLGridData.Rows[iLoop]["AGR_DTL_ID"].ToString())
                                            ).SingleOrDefault();
                                    }
                                }


                                hR_TRM_AGR_DTL.AGR_CLAUSE = dtTrmAGRdTLGridData.Rows[iLoop]["AGR_CLAUSE"].ToString(); //iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID;
                                hR_TRM_AGR_DTL.AGR_HDR_ID = hR_TRM_AGR_HDR.AGR_HDR_ID;
                                hR_TRM_AGR_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                                hR_TRM_AGR_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                                if (dtTrmAGRdTLGridData.Rows[jLoop]["DELETED"].ToString() == FINAppConstants.Y)
                                    {
                                        tmpSecondChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_DTL, FINAppConstants.Delete));
                                    }
                                    else
                                    {
                                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                        {
                                            if (dtTrmAGRdTLGridData.Rows[jLoop]["AGR_DTL_ID"].ToString() != "0" && dtTrmAGRdTLGridData.Rows[jLoop]["AGR_DTL_ID"].ToString() != string.Empty)
                                            {
                                                hR_TRM_AGR_DTL.MODIFIED_DATE = DateTime.Today;
                                                tmpSecondChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_DTL, FINAppConstants.Update));
                                            }
                                        }
                                        else
                                        {
                                            hR_TRM_AGR_DTL.AGR_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_106_D".ToString(), false, true);
                                            hR_TRM_AGR_DTL.CREATED_BY = Session["UserId"].ToString();
                                            hR_TRM_AGR_DTL.CREATED_DATE = DateTime.Today;
                                            tmpSecondChildEntity.Add(new Tuple<object, string>(hR_TRM_AGR_DTL, FINAppConstants.Add));
                                        }
                                    }
                                                                    
                                
                            }
                        }


                    }

                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveMultipleRowsEntity<HR_TRM_AGR_HDR, HR_TRM_AGR_DTL>(tmpChildEntity, hR_TRM_AGR_HDR, tmpSecondChildEntity, hR_TRM_AGR_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleRowsEntity<HR_TRM_AGR_HDR, HR_TRM_AGR_DTL>(tmpChildEntity, hR_TRM_AGR_HDR, tmpSecondChildEntity, hR_TRM_AGR_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                VMVServices.Web.Utils.SavedRecordId = hR_TRM_AGR_HDR.AGR_HDR_ID;


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //ErrorCollection.Clear();

                //AssignToBE();
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_MATERIAL_ISSUE_HDR.INDENT_ID, iNV_MATERIAL_ISSUE_HDR.ISSUING_EMP_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("INDENT", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Agreement");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                //if (savedBool)
                //{
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MISAVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }










        # region Grid Events

        private void BindAGRDtlGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();


                bol_rowVisiable = false;
                
                Session["AgrDtlGridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvAGRDtl.DataSource = dt_tmp;
                gvAGRDtl.DataBind();
                //GridViewRow gvr = gvAGRDtl.FooterRow;
                //FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterLotGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //TextBox txtClause = tmpgvr.FindControl("txtClause") as TextBox;
                //CheckBox chkAct = tmpgvr.FindControl("chkact2") as CheckBox;

                //if (gvData.EditIndex >= 0)
                //{
                //    txtClause.Text = gvData.DataKeys[gvData.EditIndex].Values["AGR_CLAUSE"].ToString();
                //    chkAct.Checked = Convert.ToBoolean(gvData.DataKeys[gvData.EditIndex].Values["ENABLED_FLAG"].ToString());
                //}
               
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootLOTGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvAGRDtl_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session["AgrDtlGridData"] != null)
            {
                dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
            }
            gvAGRDtl.EditIndex = -1;

            BindAGRDtlGrid(dtTrmAGRdTLGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvAGRDtl_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["AgrDtlGridData"] != null)
                {
                    dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
                }


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvAGRDtl.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }

                }
                if (Session["rowindex"] == null)
                {
                    Session["rowindex"] = 0;
                    hdRowIndex.Value = Session["rowindex"].ToString();
                }
                else
                {
                    hdRowIndex.Value = (int.Parse(Session["rowindex"].ToString()) + 1).ToString();
                }

               
                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControlgvAGRDtl(gvr, dtTrmAGRdTLGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtTrmAGRdTLGridData.Rows.Add(drList);
                    BindAGRDtlGrid(dtTrmAGRdTLGridData);
                    if (Session["AgrDtlGridData"] != null)
                    {
                        Session["AgrDtlGridData" + hdRowIndex.Value] = Session["AgrDtlGridData"];
                    }
                    

                }
                ModalPopupExtender4.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControlgvAGRDtl(GridViewRow gvr, DataTable tmpdtLotGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

           
            TextBox txtClause = gvr.FindControl("txtClause") as TextBox;
            CheckBox chkact2 = gvr.FindControl("chkact2") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtLotGridData.Copy();
            if (GMode == "A")
            {
                drList = dtTrmAGRdTLGridData.NewRow();
                drList["AGR_DTL_ID"] = "0";
            }
            else
            {
                drList = dtTrmAGRdTLGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            //slControls[0] = ddlWH;
            //slControls[1] = ddlLot;
            //slControls[2] = txtQty;

            //ErrorCollection.Clear();
            //string strCtrlTypes = "DropDownList ~ DropDownList ~ TextBox";

            //string strMessage = "Warehouse ~ Lot ~ Quantity";

            //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}

            string strCondition = string.Empty;

            drList["AGR_CLAUSE"] = txtClause.Text;
            drList[FINColumnConstants.ENABLED_FLAG] = chkact2.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAGRDtl_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvAGRDtl.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["AgrDtlGridData"] != null)
                {
                    dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
                   
                }
                if (gvr == null)
                {
                    return;
                }
                //int rowindex = Convert.ToInt32(e.CommandArgument);

                //hdRowIndex.Value = rowindex.ToString();

                drList = AssignToGridControlgvAGRDtl(gvr, dtTrmAGRdTLGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvAGRDtl.EditIndex = -1;
                BindAGRDtlGrid(dtTrmAGRdTLGridData);

               
                if (Session["AgrDtlGridData"] == null)
                {
                    Session["AgrDtlGridData" + hdRowIndex.Value] = Session["AgrDtlGridData"];
                }
                ModalPopupExtender4.Show();
             
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvAGRDtl_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AgrDtlGridData"] != null)
                {
                    dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];

                }
                DataRow drList = null;
                drList = dtTrmAGRdTLGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindAGRDtlGrid(dtTrmAGRdTLGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvAGRDtl_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AgrDtlGridData"] != null)
                {
                    dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];

                }
                gvAGRDtl.EditIndex = e.NewEditIndex;
                BindAGRDtlGrid(dtTrmAGRdTLGridData);
                GridViewRow gvr = gvAGRDtl.Rows[e.NewEditIndex];
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvAGRDtl_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvAGRDtl.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvAGRDtl_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        #endregion

        protected void btnDetails_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hdRowIndex.Value = gvr.RowIndex.ToString();
            if (Session["AgrDtlGridData"] == null)
            {
                dtTrmAGRdTLGridData = DBMethod.ExecuteQuery(TrainingAgreement_DAL.GetTrainingAgreementDtlDtl(Master.StrRecordId)).Tables[0];
            }
            else
            {
                dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
            }
           
            BindAGRDtlGrid(dtTrmAGRdTLGridData);
            ModalPopupExtender4.Show();

        }

        protected void btnCLosePopUp_Click(object sender, EventArgs e)
        {
            if (Session["AgrDtlGridData"] == null)
            {
                Session["AgrDtlGridData" + hdRowIndex.Value] = Session["AgrDtlGridData"];
            }
            else
            {
                dtTrmAGRdTLGridData = (DataTable)Session["AgrDtlGridData"];
            }
        }

       

       
        

    }
}