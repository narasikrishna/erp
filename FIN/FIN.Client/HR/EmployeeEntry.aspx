﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .PopUpHeader
        {
            background-color: RGB(102,186,232);
        }
        .divClear_10
        {
            width: 700px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1270px" id="divPersonlDet" runat="server"
        visible="true">
        <table style="display: none">
            <tr>
                <td>
                    <asp:Button ID="btnAddress" runat="server" Text="Address" CssClass="btn" OnClick="btnAddress_Click"
                        TabIndex="19" Visible="false" />
                </td>
                <td>
                    <asp:Button ID="btnWorkDetails" runat="server" Text="Work Details" OnClick="btnWorkDetails_Click"
                        TabIndex="19" Visible="false" />
                </td>
                <td>
                    <asp:Button ID="btnContDet" runat="server" Text="Contact Details" OnClick="btnContDet_Click"
                        TabIndex="20" Visible="false" />
                </td>
                <td>
                    <asp:Button ID="btnQualDet" runat="server" Text="Qualification Details" OnClick="btnQualDet_Click"
                        TabIndex="21" Visible="false" />
                </td>
                <td>
                    <asp:Button ID="btnSkillsDet" runat="server" Text="Skills Details" OnClick="btnSkillsDet_Click"
                        TabIndex="22" Visible="false" />
                </td>
                <td>
                    <asp:Button ID="btnBankDetails" runat="server" Text="Bank Details" OnClick="btnBankDetails_Click"
                        TabIndex="23" Visible="false" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div class="divClear_10">
        </div>
        <table width="100%" style="border: 0px solid">
            <tr>
                <td valign="top">
                    <div id="div_MasterData" runat="server" visible="true">
                        <div id="div_MHData" runat="server" visible="true">
                            <table width="100%">
                                <tr>
                                    <td style="width: 150px" valign="top">
                                        <div class="divRowContainer">
                                            <div id="div_FormHeader" runat="server" style="display: none" class="divtxtBox">
                                                <asp:ImageButton BorderColor="Navy" ImageUrl="~/Images/fotoshot.jpg" runat="server"
                                                    Width="40px" Height="37px" ID="btnFoto" OnClick="btnFoto_Click" ToolTip="Photo Capture" />
                                            </div>
                                            <div class="divtxtBox LNOrient" runat="server" id="imgCaptureImagediv" visible="true">
                                                <asp:ImageButton ID="imgPatientFoto" runat="server" Style="height: 130px; width: 130px"
                                                    AlternateText="PHOTO" ToolTip="Upload Photo" ImageUrl="~/Images/fotoshot.jpg"
                                                    OnClick="btnFoto_Click" />
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <asp:Label ID="lblFileName" runat="server" Text="File Name" Visible="false" CssClass="DisplayFont lblBox"> </asp:Label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="divFormcontainer">
                                            <div class="lblBox LNOrient" style="width: 120px" id="lblEmployeeNumber">
                                                Employee Number
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 198px">
                                                <asp:TextBox ID="txtEmpNo" CssClass="validate[required] RequiredField txtBox" runat="server"
                                                    TabIndex="1" MaxLength="50"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="FTEmNo" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                    TargetControlID="txtEmpNo" />
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 170px" id="Div9">
                                                Applicant ID
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:DropDownList ID="ddlApplicantID" runat="server" CssClass="ddlStype" TabIndex="2"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlApplicantID_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 80px" id="lblBloodGroup">
                                                Blood Group
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 150px">
                                                <asp:DropDownList ID="ddlBlodGroup" runat="server" CssClass=" RequiredField ddlStype"
                                                    TabIndex="2">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="divClear_10">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="width: 50px" id="Div6">
                                                Title
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 60px">
                                                <asp:DropDownList ID="ddlTitle" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                                    TabIndex="2">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 80px" id="lblFirstName">
                                                First Name
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:TextBox ID="txtFirstName" CssClass="validate[required] RequiredField txtBox_en"
                                                    runat="server" TabIndex="4" MaxLength="100"></asp:TextBox>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 80px" id="lblMiddleName">
                                                Middle Name
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:TextBox ID="txtMiddleName" CssClass="txtBox_en" runat="server" TabIndex="5"
                                                    MaxLength="100"></asp:TextBox>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 80px" id="lblLastName">
                                                Last Name
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 150px">
                                                <asp:TextBox ID="txtLastName" CssClass="txtBox_en" runat="server" TabIndex="6" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="divClear_10">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="width: 120px" id="lblFNOL">
                                                First Name(Arabic)
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:TextBox ID="txtFirstNameol" CssClass="txtBox_ol" runat="server" TabIndex="8"
                                                    MaxLength="240"></asp:TextBox>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 130px" id="lblMNOL">
                                                Middle Name(Arabic)
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:TextBox ID="txtMiddleNameol" CssClass="txtBox_ol" runat="server" TabIndex="9"
                                                    MaxLength="240"></asp:TextBox>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 120px" id="lblLNOL">
                                                Last Name(Arabic)
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 150px">
                                                <asp:TextBox ID="txtLastNameol" CssClass="txtBox_ol" runat="server" TabIndex="10"
                                                    MaxLength="240"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="divClear_10">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="width: 120px" id="lblDateofBirth">
                                                Date of Birth
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:TextBox ID="txtDateofBirth" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                                    runat="server" TabIndex="11"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDateofBirth"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc2:CalendarExtender>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 130px" id="lblMaritalStatus">
                                                Marital Status
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 200px">
                                                <asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                                    TabIndex="12">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="colspace LNOrient">
                                                &nbsp</div>
                                            <div class="lblBox LNOrient" style="width: 120px" id="Div5">
                                                Marriage Date
                                            </div>
                                            <div class="divtxtBox LNOrient" style="width: 150px">
                                                <asp:TextBox ID="txtMarriagedate" CssClass=" validate[custom[ReqDateDDMMYYY]]  txtBox"
                                                    runat="server" TabIndex="13"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtMarriagedate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc2:CalendarExtender>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                        </div>
                        <div id="div_MDData" runat="server" visible="true">
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 150px" id="lblEmploymentType">
                                    Employment Type</div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlEmploymentType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                        TabIndex="19" AutoPostBack="True" OnSelectedIndexChanged="ddlEmploymentType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                                    Company Name
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 580px">
                                    <asp:TextBox ID="txtCompanyname" CssClass="validate[required] SemiRequired txtBox"
                                        runat="server" TabIndex="15" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 150px" id="lbldoj">
                                    Date of Join
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtDoj" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                        runat="server" TabIndex="16"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDoj"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="lblReligion">
                                    Religion
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlReligion" runat="server" CssClass="ddlStype" TabIndex="17">
                                    </asp:DropDownList>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                                    Gender
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:RadioButtonList ID="RBG" runat="server" RepeatDirection="Horizontal" TabIndex="18">
                                        <asp:ListItem Selected="True" Value="M">Male</asp:ListItem>
                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 150px" id="lblInternal/Externale">
                                    Contract Type
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlInternalExternale" runat="server" CssClass="ddlStype" TabIndex="14"
                                        OnSelectedIndexChanged="ddlInternalExternale_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="ddlCompanyEMail">
                                    Company E-Mail
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtCompEmail" CssClass="validate[custom[email]] txtBox" runat="server"
                                        TabIndex="20" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                                    Place of Birth
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtPlaceofbirth" CssClass="txtBox" runat="server" TabIndex="21"
                                        MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                                    Number of Dependents
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtNoofDependents" CssClass="validate[custom[integer]] txtBox_N"
                                        runat="server" TabIndex="22" MaxLength="10"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                                        FilterType="Numbers,Custom" TargetControlID="txtNoofDependents" />
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="Div8">
                                    Nationality
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlNationality" runat="server" CssClass="ddlStype" TabIndex="23"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlNationality_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="divlblconfirdate">
                                    Confirmation Date
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConfDate" CssClass=" validate[custom[ReqDateDDMMYYY]]  txtBox"
                                        runat="server" TabIndex="24"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtConfDate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtConfDate" ValidChars="/" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 150px" id="Div11">
                                    Notice Period (days)
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtNoticePeriod" CssClass="validate[required,number] RequiredField txtBox_N"
                                        runat="server" TabIndex="25" MaxLength="3"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtNoticePeriod" ValidChars="" />
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="divNoticePeriodDate">
                                    Notice Period Date
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtNoticePeridDate" CssClass=" validate[custom[ReqDateDDMMYYY]]  txtBox"
                                        runat="server" TabIndex="26"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtNoticePeridDate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtNoticePeridDate" ValidChars="/" />
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div class="lblBox LNOrient" style="width: 150px" id="divResignationDate">
                                    EOS Date
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtResignationDate" CssClass="validate[custom[ReqDateDDMMYYY]]  txtBox"
                                        runat="server" TabIndex="27"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" TargetControlID="txtResignationDate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtResignationDate" ValidChars="/" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div style="float: left">
                                    <div class="lblBox LNOrient" style="width: 150px">
                                        Probation Flag
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 200px">
                                        <asp:CheckBox ID="chkProbation" TabIndex="27" runat="server" Text=" " Checked="True" />
                                    </div>
                                </div>
                                <div class="colspace LNOrient">
                                    &nbsp</div>
                                <div style="float: left" runat="server" id="divSSDFlag" visible="false">
                                    <div class="lblBox LNOrient" style="width: 150px" id="divssdFlag">
                                        SSD Flag
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 200px">
                                        <asp:CheckBox ID="chkSSDFlag" runat="server" />
                                    </div>
                                </div>
                                <div style="float: left" runat="server" id="divStopPayroll" visible="false">
                                    <div class="lblBox LNOrient" style="width: 150px" id="divlblStopPayroll">
                                        Stop Flag
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 200px">
                                        <asp:CheckBox ID="chkStopPayroll" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div style="float: left">
                                    <div class="lblBox LNOrient" style="width: 150px">
                                        Indemnity
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 200px">
                                        <asp:DropDownList ID="ddlIndemnity" TabIndex="27" runat="server" CssClass=" ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="colspace LNOrient">
                                        &nbsp</div>
                                    <div class="lblBox LNOrient" style="width: 150px; display: none" id="Div13">
                                        Account Code
                                    </div>
                                    <div class="divtxtBox LNOrient" style="width: 200px; display: none">
                                        <asp:DropDownList ID="ddlglAccCode" runat="server" CssClass="ddlStype">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divRowContainer">
                        <div align="right">
                            <asp:Button ID="btnSave" runat="server" Text="Add" CssClass="btn" OnClick="btnAdd_Click"
                                Style="display: none" TabIndex="28" />
                            <asp:HiddenField ID="hf_EMPID" runat="server" Value="" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hidAddress" runat="server" Value="Address" />
                    <%--<cc2:ModalPopupExtender ID="MPEAddress" runat="server" TargetControlID="hidAddress"
        PopupControlID="pnlAddress" CancelControlID="btnAddressClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlAddress" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" style="height: 25px;">
                                <div class="LNOrient" style="width: 25%">
                                    <asp:Label ID="lblAddSaved" runat="server" Text="Address" CssClass="lblBox" Style="color: White"></asp:Label>
                                </div>
                                <div class="LNOrient" style="width: 35%">
                                    <table width="100%">
                                        <tr align="center">
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgAddressFirst" runat="server" ImageUrl="~/Images/First.png"
                                                    Width="20px" Height="20px" OnClick="imgAddressFirst_Click" TabIndex="16" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgAddressPrevious" runat="server" ImageUrl="~/Images/previous.png"
                                                    Width="20px" Height="20px" OnClick="imgAddressPrevious_Click" TabIndex="17" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgAddressNext" runat="server" ImageUrl="~/Images/Next.png"
                                                    Width="20px" Height="20px" OnClick="imgAddressNext_Click" TabIndex="18" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgAddressLast" runat="server" ImageUrl="~/Images/Last.png"
                                                    Width="20px" Height="20px" OnClick="imgAddressLast_Click" TabIndex="19" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 40%; vertical-align: top" align="right">
                                    <table width="20%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAddAdd" runat="server" CssClass="btn" Text="Update Address" OnClick="btnAddAdd_Click"
                                                    TabIndex="14" Style="display: none" />
                                                <asp:ImageButton ID="imgbtnaddadd" runat="server" ImageUrl="~/Images/update-new.png"
                                                    OnClick="btnAddAdd_Click" Width="20px" Height="20px" ToolTip="Save / Update" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddClear" runat="server" CssClass="btn" Text="Add New" OnClick="btnAddClear_Click"
                                                    TabIndex="15" Style="display: none" />
                                                <asp:ImageButton ID="imgbtnAddClear" runat="server" ImageUrl="~/Images/add-new.png"
                                                    OnClick="btnAddClear_Click" Width="20px" Height="20px" ToolTip="Add New (Clear)" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="OppLNOrient" style="padding-right: 10px" align="right">
                                    <asp:ImageButton ID="btnAddressClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Visible="false" Width="20px" Height="20px" runat="server" TabIndex="30" />
                                    <asp:HiddenField ID="hfAddressID" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblAddressTYpe">
                                    Address Type
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlAddressType" runat="server" Style="width: 200px" CssClass="validate[required] RequiredField ddlStype"
                                        TabIndex="101">
                                    </asp:DropDownList>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblPermanentAddress">
                                    Permanent Address
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:CheckBox ID="PermanentAddress" runat="server" BorderStyle="NotSet" TabIndex="102" />
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="Div4">
                                    Active
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:CheckBox ID="chkAddActive" runat="server" Checked="true" TabIndex="103" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblAddress1">
                                    Address 1
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtAddress1" CssClass="validate[required] RequiredField txtBox"
                                        runat="server" OnTextChanged="txtAddress1_TextChanged" TabIndex="104" MaxLength="200"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblAddress2">
                                    Address 2
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtAddress2" CssClass=" txtBox" runat="server" TabIndex="105" MaxLength="200"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblAddress3">
                                    Address 3
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtAddress3" CssClass="txtBox" runat="server" TabIndex="106" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblCountry">
                                    Governorate
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtCountry" CssClass="txtBox" runat="server" TabIndex="107" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblState">
                                    Area
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtState" CssClass="txtBox" runat="server" TabIndex="108" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblCity">
                                    City
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtCity" CssClass="txtBox" runat="server" TabIndex="109" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblPostalCode">
                                    Postal Code
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtPostalCode" CssClass="txtBox" runat="server" TabIndex="110" MaxLength="50"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblPo.Box">
                                    PO Box
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtPoBox" CssClass=" txtBox" runat="server" TabIndex="110" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblPhone">
                                    Phone
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtPhone" CssClass="txtBox" runat="server" TabIndex="111" MaxLength="50"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="fteAddressPhone" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtPhone" ValidChars="+ -" />
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblMobile">
                                    Mobile1
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtMobile" CssClass="txtBox" runat="server" TabIndex="112" MaxLength="50"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="fteAddMobile" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtMobile" ValidChars="+ -" />
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="Div10">
                                    Mobile2
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtMobile2" CssClass="txtBox" runat="server" TabIndex="112" MaxLength="50"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtMobile" ValidChars="+ -" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="lblEffectiveDate">
                                    Effective Date
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtEffectiveDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                        runat="server" TabIndex="114" MaxLength="10"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CEAddressED" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEffectiveDate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblEndDate">
                                    End Date
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY]] txtBox" runat="server"
                                        TabIndex="115" MaxLength="10"></asp:TextBox>
                                    <cc2:CalendarExtender ID="CEAddressEndDate" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEndDate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc2:CalendarExtender>
                                </div>
                            </div>
                            <div class="divRowContainer" align="right">
                                <asp:HiddenField ID="hfAddRecId" runat="server" Value="0" />
                            </div>
                            <div class="divClear_10">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfWorkDet" runat="server" Value="WorkDet" />
                    <%-- <cc2:ModalPopupExtender ID="MPEWorkDet" runat="server" TargetControlID="hfWorkDet"
        PopupControlID="pnlWorkDet" CancelControlID="btnWorkClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlWorkDet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblWorDetHeader" runat="server" Text="Work Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="padding-right: 10px" align="right">
                                    <asp:ImageButton ID="btnWorkClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        runat="server" Width="20px" Height="20px" Visible="False" />
                                    <asp:HiddenField ID="hfWorkId" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvWorkDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvWorkDet_RowCancelingEdit" OnRowCreated="gvWorkDet_RowCreated"
                                    OnRowDataBound="gvWorkDet_RowDataBound" ShowFooter="True" OnRowCommand="gvWorkDet_RowCommand"
                                    OnRowUpdating="gvWorkDet_RowUpdating" OnRowEditing="gvWorkDet_RowEditing" DataKeyNames="EMP_WH_ID,EMP_DEPT_ID,EMP_DESIG_ID,EMP_CATEGORY,EMP_GRADE_ID,EMP_JOB_ID,EMP_POSITION_ID,REPORTINGTO_ID,CONTINGENCY_PLAN,SEC_ID,LOC_ID">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Department">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlDepartment" TabIndex="201" runat="server" Width="98%" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlDepartment" Width="98%" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                                    runat="server" CssClass="RequiredField ddlStype">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DEPT_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="130px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlDesignation" TabIndex="202" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="98%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="98%">
                                                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("DESIG_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlCategory" TabIndex="203" runat="server" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                                                    Width="98%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                                                    Width="98%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("CATEGORY_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Job">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlJob" TabIndex="204" runat="server" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" Width="98%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlJob" AutoPostBack="True" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged"
                                                    runat="server" CssClass="RequiredField ddlStype" Width="98%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblJob" runat="server" Text='<%# Eval("JOB_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Position">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlPosition" TabIndex="205" runat="server" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged"
                                                    Width="98%">
                                                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlPosition" runat="server" CssClass="RequiredField ddlStype"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged"
                                                    Width="98%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPosition" runat="server" Text='<%# Eval("POSITION_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Grade">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrade" TabIndex="206" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlGrade" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblGrade" runat="server" Text='<%# Eval("GRADE_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Section">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlSection" TabIndex="206" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlSection" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%--  <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSection" runat="server" Text='<%# Eval("SEC_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlLocation" TabIndex="206" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%--  <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LOC_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reporting To">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlReportingTo" TabIndex="206" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlReportingTo" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%--                                                    <asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblReportingTo" runat="server" Text='<%# Eval("REPORTINGTO_DESC") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contingency Plan">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlContPlan" TabIndex="206" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlContPlan" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="97%">
                                                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblContPlan" runat="server" Text='<%# Eval("CONTINGENCY_PLAN_DESC") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Position In Work Permit">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPosinworkpermit" TabIndex="207" runat="server" CssClass="EntryFont  txtBox"
                                                    MaxLength="100" Text='<%#  Eval("POS_IN_WORK_PERMIT","{0:dd/MM/yyyy}") %>' Width="100px"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPosinworkpermit" runat="server" CssClass="EntryFont  txtBox"
                                                    MaxLength="100" Width="100px"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPosinworkpermit" runat="server" Text='<%# Eval("POS_IN_WORK_PERMIT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Effective Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpFromDate" TabIndex="207" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    MaxLength="10" Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Width="100px"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    MaxLength="10" Width="100px"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpToDate" TabIndex="208" runat="server" CssClass="EntryFont  txtBox"
                                                    Width="100px" MaxLength="10" Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender11" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" TabIndex="209" runat="server" AlternateText="Edit"
                                                    CausesValidation="false" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" TabIndex="210" runat="server" AlternateText="Delete"
                                                    CausesValidation="false" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfEmpContDet" runat="server" Value="ContDet" />
                    <%--  <cc2:ModalPopupExtender ID="MPEContDet" runat="server" TargetControlID="hfEmpContDet"
        PopupControlID="pnlContDet" CancelControlID="btnContClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlContDet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%">
                            <div class="divRowContainer PopUpHeader" style="height: 25px">
                                <div class="LNOrient" style="width: 25%">
                                    <asp:Label ID="lblContSave" runat="server" Text="Contact Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="LNOrient" style="width: 35%">
                                    <table width="100%">
                                        <tr align="center">
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgbtncontFirst" runat="server" ImageUrl="~/Images/First.png"
                                                    Width="20px" Height="20px" OnClick="imgbtncontFirst_Click" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imtbtncontprevious" runat="server" ImageUrl="~/Images/previous.png"
                                                    Width="20px" Height="20px" OnClick="imtbtncontprevious_Click" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgbtncontnext" runat="server" ImageUrl="~/Images/Next.png"
                                                    Width="20px" Height="20px" OnClick="imgbtncontnext_Click" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:ImageButton ID="imgbtncontlast" runat="server" ImageUrl="~/Images/Last.png"
                                                    Width="20px" Height="20px" OnClick="imgbtncontlast_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 40%; vertical-align: top" align="right">
                                    <table width="20%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnContDetAdd" runat="server" Text="Update Contact" CssClass="btn"
                                                    OnClick="bntContDetAdd_Click" TabIndex="45" Style="display: none" />
                                                <asp:ImageButton ID="imgbtnContDetAdd" runat="server" ImageUrl="~/Images/update-new.png"
                                                    ToolTip="Save/Update" OnClick="bntContDetAdd_Click" Width="20px" Height="20px"
                                                    TabIndex="45" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnContDetClear" runat="server" Text="Add New" CssClass="btn" OnClick="btnContDetClear_Click"
                                                    TabIndex="46" Style="display: none" />
                                                <asp:ImageButton ID="imgbtnContDetClear" runat="server" ImageUrl="~/Images/add-new.png"
                                                    OnClick="btnContDetClear_Click" Width="20px" Height="20px" ToolTip="Add New (Clear)" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="OppLNOrient" style="padding-right: 10px" align="right">
                                    <asp:ImageButton ID="btnContClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" TabIndex="30" Visible="False" />
                                    <asp:HiddenField ID="hfContDetId" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerRelship">
                                    Relationship</div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:DropDownList ID="ddlConRelType" runat="server" CssClass=" ddlStype" TabIndex="301"
                                        Width="100%">
                                    </asp:DropDownList>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerActive">
                                    Active
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:CheckBox ID="chkContPerActive" runat="server" Text=" " TabIndex="302" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerPersonName">
                                    First Name
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConPerName" CssClass="validate[required] RequiredField txtBox"
                                        MaxLength="100" runat="server" TabIndex="303"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblConMiddleName">
                                    Middle Name
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConMiddleName" CssClass="txtBox" MaxLength="100" runat="server"
                                        TabIndex="304"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="lblConLastName">
                                    Last Name
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConLastName" CssClass="txtBox" MaxLength="100" runat="server"
                                        TabIndex="305"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerAdd1">
                                    Address 1
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConPerAdd1" CssClass="txtBox" runat="server" TabIndex="306" MaxLength="200"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerAdd2">
                                    Address 2
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConPerAdd2" CssClass="txtBox" runat="server" TabIndex="307" MaxLength="200"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divContPerAdd3">
                                    Address 3
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtConPerAdd3" CssClass=" txtBox" runat="server" TabIndex="308"
                                        MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblgovernorate_cont">
                                    Governorate
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtContCountry" CssClass="txtBox" runat="server" TabIndex="309"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblArea_cont">
                                    Area
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtContAread" CssClass="txtBox" runat="server" TabIndex="310" MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblCity_cont">
                                    City
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtContCity" CssClass="txtBox" runat="server" TabIndex="311" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblPhone_Cont">
                                    Phone
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtContPhone" CssClass="txtBox" runat="server" TabIndex="312" MaxLength="50"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtContPhone" ValidChars="+ -" />
                                </div>
                                <div class="lblBox LNOrient" style="width: 120px" id="divlblMobile_Cont">
                                    Mobile
                                </div>
                                <div class="divtxtBox LNOrient" style="width: 200px">
                                    <asp:TextBox ID="txtContMob" CssClass="txtBox" runat="server" TabIndex="313" MaxLength="50"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers,Custom"
                                        TargetControlID="txtContMob" ValidChars="+ -" />
                                </div>
                            </div>
                            <div class="divRowContainer" align="right">
                                <asp:HiddenField ID="hfContRecid" runat="server" Value="0" />
                            </div>
                            <div class="divClear_10">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfQualDet" runat="server" Value="QualificationDetails" />
                    <%-- <cc2:ModalPopupExtender ID="MPEQualDet" runat="server" TargetControlID="hfQualDet"
        PopupControlID="pnlQualDet" CancelControlID="btnQualDetClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlQualDet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblQualDet" runat="server" Text="Qualification Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="btnQualDetClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hfQualId" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvQualDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvQualDet_RowCancelingEdit" OnRowCreated="gvQualDet_RowCreated"
                                    OnRowDataBound="gvQualDet_RowDataBound" ShowFooter="True" OnRowCommand="gvQualDet_RowCommand"
                                    OnRowUpdating="gvQualDet_RowUpdating" OnRowEditing="gvQualDet_RowEditing" DataKeyNames="QUALI_ID,QUALI_TYPE,PRIMARY_FLAG">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Qualification Type">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlQualType" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlQualType" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQualType" runat="server" Text='<%# Eval("QUALI_TYPE_NAME") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qualification Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtQualName" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='<%# Eval("QUALI_NAME") %>' MaxLength="100" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtQualName" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="95%" MaxLength="100"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQualName" runat="server" Text='<%# Eval("QUALI_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDuration" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                    Text='<%# Eval("DURATION") %>' MaxLength="1" Width="95%"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="FTDur" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                    TargetControlID="txtDuration" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtDuration" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                                    Width="95%" MaxLength="1"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="FTDurft" runat="server" ValidChars="0" FilterType="Numbers,Custom"
                                                    TargetControlID="txtDuration" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDuration" runat="server" Text='<%# Eval("DURATION") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Primary">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkQualDetPrimay" runat="server" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkQualDetPrimay" runat="server" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkQualDetPrimay" runat="server" Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Effective Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDF1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDF2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDT1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDT2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfSkillsDet" runat="server" Value="SkillsDetails" />
                    <%--  <cc2:ModalPopupExtender ID="MPESkillsDet" runat="server" TargetControlID="hfSkillsDet"
        PopupControlID="pnlSkillsDet" CancelControlID="btnSkillsDetClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlSkillsDet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblSkillDet" runat="server" Text="Skill Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="btnSkillsDetClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hfSkillsId" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvSkillsDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvSkillsDet_RowCancelingEdit" OnRowCreated="gvSkillsDet_RowCreated"
                                    OnRowDataBound="gvSkillsDet_RowDataBound" ShowFooter="True" OnRowCommand="gvSkillsDet_RowCommand"
                                    OnRowUpdating="gvSkillsDet_RowUpdating" DataKeyNames="EMP_SKILL_ID,SKILL_CATEGORY,EMP_SKILL_TYPE"
                                    OnRowEditing="gvSkillsDet_RowEditing">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Skill Category">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlSkillCat" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlSkillCat" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSkillCat" runat="server" Text='<%# Eval("CAT_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Skill Type">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlSkillType" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlSkillType" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSkillType" runat="server" Text='<%# Eval("TYPE_DESC") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Skill Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtSkillName" runat="server" CssClass="RequiredField txtBox" Text='<%# Eval("EMP_SKILL_NAME") %>'
                                                    MaxLength="100" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtSkillName" runat="server" CssClass="RequiredField txtBox" Width="95%"
                                                    MaxLength="100"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSkillName" runat="server" Text='<%# Eval("EMP_SKILL_NAME") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Validity Details">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtValDet" runat="server" CssClass="RequiredField txtBox" Text='<%# Eval("EMP_VALIDITY_DTLS") %>'
                                                    MaxLength="50" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtValDet" runat="server" CssClass="RequiredField txtBox" Width="95%"
                                                    MaxLength="50"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblValDet" Style="word-wrap: break-word; white-space: pre-wrap;" runat="server"
                                                    Text='<%# Eval("EMP_VALIDITY_DTLS") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtBox" Text='<%# Eval("EMP_REMARKS") %>'
                                                    MaxLength="500" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtBox" Width="95%" MaxLength="500"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemarks" Style="word-wrap: break-word; white-space: pre-wrap;"
                                                    runat="server" Text='<%# Eval("EMP_REMARKS") %>' Width="180px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Effective Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='<%#  Eval("EFFECTIVE_FROM","{0:dd/MM/yyyy}") %>' Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDF1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDF2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfEmpBank" runat="server" Value="EmployeeBank" />
                    <%--   <cc2:ModalPopupExtender ID="MPEEmpBank" runat="server" TargetControlID="hfEmpBank"
        PopupControlID="pnlEmpBank" CancelControlID="btnEmpBankClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlEmpBank" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblEmpbankhe" runat="server" Text="Bank Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="btnEmpBankClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hfEmpBankID" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvEmpBank" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvEmpBank_RowCancelingEdit" OnRowCreated="gvEmpBank_RowCreated"
                                    OnRowDataBound="gvEmpBank_RowDataBound" ShowFooter="True" OnRowCommand="gvEmpBank_RowCommand"
                                    OnRowUpdating="gvEmpBank_RowUpdating" DataKeyNames="PK_ID,EMP_BANK_CODE,EMP_BANK_BRANCH_CODE,EMP_BANK_ACCT_CODE,ENABLED_FLAG"
                                    OnRowEditing="gvEmpBank_RowEditing">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Account Number">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAccNumber" runat="server" CssClass="RequiredField txtBox" Text='<%# Eval("EMP_BANK_ACCT_CODE") %>'
                                                    Width="95%" MaxLength="50"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAccNumber" runat="server" CssClass="RequiredField txtBox" Width="95%"
                                                    MaxLength="50"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccNumber" runat="server" Text='<%# Eval("EMP_BANK_ACCT_CODE") %>'
                                                    Style="word-wrap: break-word; white-space: pre-wrap;" Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bank Name">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" Width="95%"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" Width="95%"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBankName" runat="server" Text='<%# Eval("BANK_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Branch Name">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlBranchName" runat="server" CssClass="ddlStype" Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlBranchName" runat="server" CssClass="ddlStype" Width="95%">
                                                    <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("BANK_BRANCH_NAME") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkEmpBankActive" runat="server" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkEmpBankActive" runat="server" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEmpBankActive" Enabled="false" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="IBAN Number">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtIBANNumber" runat="server" CssClass="EntryFont txtBox" Text='<%# Eval("EMP_IBAN_NUM") %>'
                                                    Width="100px"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtIBANNumber" runat="server" CssClass="EntryFont txtBox" Width="100px"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblIBANNumber" runat="server" Text='<%# Eval("EMP_IBAN_NUM") %>' Width="130px"
                                                    Style="word-wrap: break-word; white-space: pre-wrap;"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Effective Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Width="100px"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEEBF1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="100px"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEEBF2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQEB1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQEB2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfPassPortDet" runat="server" Value="PassportDetails" />
                    <asp:HiddenField ID="hfPassTxnID" runat="server" Value="PassportDetails" />
                    <%--    <cc2:ModalPopupExtender ID="MPEPassPortDet" runat="server" TargetControlID="hfPPDet"
        PopupControlID="pnlPPDet" CancelControlID="btnPPClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlPPDet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 100%">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblPPdetHe" runat="server" Text="Document Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="btnPPClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hfPPDet" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvPPDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvPPDet_RowCancelingEdit" OnRowCreated="gvPPDet_RowCreated"
                                    OnRowDataBound="gvPPDet_RowDataBound" ShowFooter="True" OnRowCommand="gvPPDet_RowCommand"
                                    OnRowUpdating="gvPPDet_RowUpdating" OnRowEditing="gvPPDet_RowEditing" DataKeyNames="PASS_TXN_ID,LOOKUP_ID,PASSPORT_RELATIONSHIP,PASSPORT_NATIONALITY,ENABLED_FLAG">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID Type">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlIdType" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlIdType" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblIdType" runat="server" Text='<%# Eval("LOOKUP_NAME") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPPName" runat="server" CssClass="txtBox" Text='<%# Eval("NAME_IN_PASSPORT") %>'
                                                    MaxLength="200" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPPName" runat="server" CssClass="txtBox" Width="95%" MaxLength="200"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPName" runat="server" Text='<%# Eval("NAME_IN_PASSPORT") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Relationship">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlppRelShip" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlPPRelShip" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPRelShip" runat="server" Text='<%# Eval("PASSPORT_RELATIONSHIP_NAME") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID Number">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPPNumber" runat="server" CssClass="RequiredField txtBox" Text='<%# Eval("PASSPORT_NUMBER") %>'
                                                    MaxLength="50" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPPNumber" runat="server" CssClass="RequiredField txtBox" Width="95%"
                                                    MaxLength="50"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPNumber" runat="server" Text='<%# Eval("PASSPORT_NUMBER") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nationality">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlppNationality" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlPPNationality" runat="server" CssClass="RequiredField ddlStype"
                                                    Width="95%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPNationality" runat="server" Text='<%# Eval("PASSPORT_NATIONALITY_NAME") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Issue Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpPPIssueDate" runat="server" CssClass="EntryFont  txtBox" Text='<%#  Eval("PASSPORT_ISSUE_DATE","{0:dd/MM/yyyy}") %>'
                                                    Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEPP1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPPIssueDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpPPIssueDate" runat="server" CssClass="EntryFont txtBox" Width="100px"
                                                    MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEPP2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPPIssueDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPIssueDate" runat="server" Text='<%# Eval("PASSPORT_ISSUE_DATE","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpPPExpiryDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="100px" Text='<%#  Eval("PASSPORT_EXPIRY_DATE","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQPPE1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPPExpiryDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpPPExpiryDate" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                                    Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQPPE2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPPExpiryDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("PASSPORT_EXPIRY_DATE","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Issued Place">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPPIssuePlace" runat="server" CssClass="RequiredField txtBox"
                                                    Text='<%# Eval("PASSPORT_ISSUE_PLACE") %>' MaxLength="100" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPPIssuePlace" runat="server" CssClass="RequiredField txtBox"
                                                    Width="95%" MaxLength="100"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPIssuePlace" runat="server" Text='<%# Eval("PASSPORT_ISSUE_PLACE") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Issued By">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPPIssuedBy" runat="server" CssClass="RequiredField txtBox" Text='<%# Eval("PASSPORT_ISSUED_BY") %>'
                                                    MaxLength="100" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPPIssuedBy" runat="server" CssClass="RequiredField txtBox" Width="95%"
                                                    MaxLength="100"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPPIssuedBy" runat="server" Text='<%# Eval("PASSPORT_ISSUED_BY") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkPPActive" runat="server" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkPPActive" Checked="true" runat="server" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkppActive" runat="server" Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfPasswordDet" runat="server" Value="PasswordDetails" />
                    <%-- <cc2:ModalPopupExtender ID="MPEPasswordDet" runat="server" TargetControlID="pnlPassworddet"
        PopupControlID="pnlQualDet" CancelControlID="btnQualDetClose" BackgroundCssClass="ConfirmBackground">
    </cc2:ModalPopupExtender>--%>
                    <asp:Panel ID="pnlPassworddet" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblSelfServiceDtls" runat="server" Text="Self Service Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="imgPDClose" ToolTip="Close" AlternateText="Close" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hfPDId" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvPasswordDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    OnRowCancelingEdit="gvPasswordDet_RowCancelingEdit" OnRowCreated="gvPasswordDet_RowCreated"
                                    OnRowDataBound="gvPasswordDet_RowDataBound" ShowFooter="True" OnRowCommand="gvPasswordDet_RowCommand"
                                    OnRowUpdating="gvPasswordDet_RowUpdating" OnRowEditing="gvPasswordDet_RowEditing"
                                    DataKeyNames="EPD_ID,ENABLED_FLAG">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User Name" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtUserName" runat="server" CssClass="txtBox" Text='<%# Eval("USER_ID") %>'
                                                    MaxLength="100" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtUserName" runat="server" CssClass="txtBox" Width="95%" MaxLength="100"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_ID") %>' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Password">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPassword" runat="server" CssClass="txtBox" Text='<%# Eval("USER_PASSWORD") %>'
                                                    TextMode="Password" MaxLength="50" Width="95%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPassword" runat="server" CssClass="txtBox" Width="95%" MaxLength="50"
                                                    TextMode="Password"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPassword" runat="server" Text='******' Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkPasswordActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkPasswordActive" runat="server" Checked="true" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkPasswordActive" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <FooterStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtPPDFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='<%#  Eval("EFFECTIVE_FROM","{0:dd/MM/yyyy}") %>' Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEPDF1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPDFromDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpPDFromDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CPDF2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPDFromDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPDStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtpPDToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    Text='<%#  Eval("EFFECTIVE_TO","{0:dd/MM/yyyy}") %>' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDT1" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPDToDate">
                                                </cc2:CalendarExtender>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtpPDToDate" runat="server" CssClass="EntryFont  txtBox" Width="100px"
                                                    MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CEQDT2" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpPDToDate">
                                                </cc2:CalendarExtender>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPDToDate" runat="server" Text='<%# Eval("EFFECTIVE_TO","{0:dd/MM/yyyy}") %>'
                                                    Width="130px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlLeaveDetails" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblLeaveDtls" runat="server" Text="Leave Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient" style="margin-right: 20px">
                                    <asp:ImageButton ID="imgbtnLeavePost" runat="server" ImageUrl="~/Images/post-icon.png"
                                        Width="20px" Height="20px" OnClick="imgbtnPost_Click" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:GridView ID="gvLeaveDetails" runat="server" AutoGenerateColumns="False" CssClass="Grid">
                                    <Columns>
                                        <asp:BoundField DataField="CAL_ACCT_YEAR" HeaderText="Year" />
                                        <asp:BoundField DataField="LEAVE_ID" HeaderText="Leave Type" />
                                        <asp:BoundField DataField="NO_OF_DAYS" HeaderText="No. Of Days">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LEAVE_AVAILED" HeaderText="Leave Availed">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LEAVE_BALANCE" HeaderText="Leave Balance">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSalaryDetails" runat="server" Visible="false" Height="300px">
                        <div class="ConfirmForm" style="width: 98%;">
                            <div class="divRowContainer PopUpHeader" align="right">
                                <div class="LNOrient">
                                    <asp:Label ID="lblSalaryDtls" runat="server" Text="Salary Details" CssClass="lblBox"
                                        Style="color: White"></asp:Label>
                                </div>
                                <div class="OppLNOrient">
                                    <asp:ImageButton ID="imgbtnSalarySave" runat="server" ImageUrl="~/Images/update-new.png"
                                        Width="20px" Height="20px" ToolTip="Save / Update" OnClick="imgbtnSalarySave_Click" />
                                </div>
                            </div>
                            <div class="divClear_10">
                            </div>
                            <div class="divRowContainer">
                                <asp:HiddenField ID="hf_SalaryType" runat="server" />
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <div class="divFormcontainer" style="width: 800px;" id="divMainContainer">
                                                <div class="divRowContainer">
                                                    <div class="lblBox LNOrient" style="width: 150px" id="lblGroupCode">
                                                        Group Code
                                                    </div>
                                                    <div class="divtxtBox LNOrient" style="width: 155px">
                                                        <asp:DropDownList ID="ddlGroupCode" runat="server" AutoPostBack="True" CssClass="RequiredField EntryFont ddlStype"
                                                            OnSelectedIndexChanged="ddlGroupCode_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="colspace LNOrient">
                                                        &nbsp</div>
                                                    <div class="lblBox LNOrient" style="width: 145px" id="lblDescription">
                                                        Description
                                                    </div>
                                                    <div class="divtxtBox LNOrient" style="width: 150px; height: 18px;">
                                                        <asp:TextBox ID="txtDescription" Enabled="true" CssClass="validate[]  txtBox" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divClear_10">
                                                </div>
                                                <div class="divRowContainer">
                                                    <div class="lblBox LNOrient" style="width: 150px" id="lblElement">
                                                        Element
                                                    </div>
                                                    <div class="divtxtBox LNOrient" style="width: 150px; height: 18px;">
                                                        <asp:TextBox ID="txtElement" Enabled="true" CssClass="validate[]  txtBox" runat="server"
                                                            MaxLength="50"></asp:TextBox>
                                                    </div>
                                                    <div class="colspace LNOrient">
                                                        &nbsp</div>
                                                    <div class="lblBox LNOrient" style="width: 80px" id="lblMinLimit">
                                                        Min. Limit
                                                    </div>
                                                    <div class="divtxtBox LNOrient" style="width: 55px; height: 18px;">
                                                        <asp:TextBox ID="txtMinLimit" Enabled="true" CssClass="validate[]  txtBox" runat="server"
                                                            MaxLength="11"></asp:TextBox>
                                                        <cc2:FilteredTextBoxExtender ID="FTMinlimit" runat="server" ValidChars=".," FilterType="Numbers,Custom"
                                                            TargetControlID="txtMinLimit" />
                                                    </div>
                                                    <div class="colspace LNOrient">
                                                        &nbsp</div>
                                                    <div class="lblBox LNOrient" style="width: 80px" id="lblMaxLimit">
                                                        Max. Limit
                                                    </div>
                                                    <div class="divtxtBox LNOrient" style="width: 55px; height: 18px;">
                                                        <asp:TextBox ID="txtMaxLimit" Enabled="true" CssClass="validate[]  txtBox" runat="server"
                                                            MaxLength="11"></asp:TextBox>
                                                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                                                            FilterType="Numbers,Custom" TargetControlID="txtMaxLimit" />
                                                    </div>
                                                </div>
                                                <div class="divClear_10">
                                                </div>
                                                <div class="divClear_10">
                                                </div>
                                                <div class="divRowContainer" align="left">
                                                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                                        DataKeyNames="PAY_ELEMENT_ID,PAY_EMP_ELEMENT_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                                                        OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                                                        OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                                                        ShowFooter="True">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Element Code">
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlElementCode" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                                                        AutoPostBack="True" OnSelectedIndexChanged="ddlElementCode_SelectedIndexChanged"
                                                                        Width="100%">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="ddlElementCode" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                                                        AutoPostBack="True" OnSelectedIndexChanged="ddlElementCode_SelectedIndexChanged"
                                                                        Width="100%">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblElementCode" CssClass="adminFormFieldHeading" TabIndex="10" runat="server"
                                                                        Text='<%# Eval("PAY_ELEMENT_CODE") %>' Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Element Description">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtElementDescription" Enabled="true" Width="160px" MaxLength="500"
                                                                        runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="txtElementDescription" Enabled="true" Width="160px" MaxLength="500"
                                                                        runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblElementDescription" Width="160px" TabIndex="11" Enabled="true"
                                                                        runat="server" Text='<%# Eval("PAY_ELEMENT_DESC") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Pro Rate">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkprorate" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAY_ELEMENT_PRORATE")) %>' />
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:CheckBox ID="chkprorate" runat="server" />
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkprorate" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAY_ELEMENT_PRORATE")) %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Annual Leave">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkannualleave" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAID_FOR_ANNUAL_LEAVE")) %>' />
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:CheckBox ID="chkannualleave" runat="server" />
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkannualleave" runat="server" Checked='<%# Convert.ToBoolean(Eval("PAID_FOR_ANNUAL_LEAVE")) %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtAmount" Width="130px" MaxLength="13" runat="server" CssClass=" RequiredField  txtBox_N"
                                                                        Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="txtAmount" Width="130px" MaxLength="13" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtAmount" Width="130px" MaxLength="13" runat="server" CssClass=" RequiredField  txtBox_N"
                                                                        Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0.,"
                                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="From Date">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="dtpfromDate" Width="130px" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                                        Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                                                                    </cc2:CalendarExtender>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="dtpfromDate" Width="130px" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                                        Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                                                                    </cc2:CalendarExtender>
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="dtpfromDate" Width="130px" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                                        Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                                                                    </cc2:CalendarExtender>
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="To Date">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="dtpToDate" Width="130px" runat="server" CssClass="EntryFont  txtBox"
                                                                        Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                                                        TargetControlID="dtpToDate">
                                                                    </cc2:CalendarExtender>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="dtpToDate" Width="130px" runat="server" CssClass="EntryFont  txtBox"></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                                                        TargetControlID="dtpToDate">
                                                                    </cc2:CalendarExtender>
                                                                </FooterTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="dtpToDate" Width="130px" runat="server" CssClass="EntryFont  txtBox"
                                                                        Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                                                    <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                                                        TargetControlID="dtpToDate">
                                                                    </cc2:CalendarExtender>
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                                        CommandName="Edit" ImageUrl="~/Images/Edit.GIF" Visible="false" />
                                                                    <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                                        CommandName="Delete" ImageUrl="~/Images/Delete.JPG" Visible="false" />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                                        Height="20px" ImageUrl="~/Images/Update.png" />
                                                                    <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                                        CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                                        ImageUrl="~/Images/Add.jpg" />
                                                                </FooterTemplate>
                                                                <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td>
                                            <div class="divRowContainer PopUpHeader" align="left" style="display: none">
                                                Earnings
                                            </div>
                                        </td>
                                        <td>
                                            <div class="divRowContainer PopUpHeader" align="left" style="display: none">
                                                Deduction
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td valign="top">
                                            <asp:GridView ID="gvSalaryDetails" runat="server" AutoGenerateColumns="false" CssClass="Grid"
                                                OnRowDataBound="gvSalaryDetails_RowDataBound" Visible="false">
                                                <Columns>
                                                    <asp:BoundField DataField="PAY_ELEMENT_CODE" HeaderText="Element Code" />
                                                    <asp:BoundField DataField="PAY_ELEMENT_DESC" HeaderText="Description" />
                                                    <%--<asp:BoundField DataField="PAY_AMOUNT" HeaderText="Amount" />--%>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtAmount" MaxLength="10" Width="96%" runat="server" CssClass="txtBox_N"
                                                                Text='<%#  Eval("PAY_AMOUNT") %>'></asp:TextBox>
                                                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EFFECTIVE_FROM_DT" HeaderText="Start Date" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField DataField="EFFECTIVE_TO_DT" HeaderText="End Date" DataFormatString="{0:dd/MM/yyyy}" />
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GrdAltRow" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            <asp:GridView ID="gvSalaryDetails_Det" runat="server" AutoGenerateColumns="false"
                                                Visible="false" CssClass="Grid" OnRowDataBound="gvSalaryDetails_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="PAY_ELEMENT_CODE" HeaderText="Element Code" />
                                                    <asp:BoundField DataField="PAY_ELEMENT_DESC" HeaderText="Description" />
                                                    <asp:BoundField DataField="PAY_AMOUNT" HeaderText="Amount" />
                                                    <asp:BoundField DataField="EFFECTIVE_FROM_DT" HeaderText="Start Date" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField DataField="EFFECTIVE_TO_DT" HeaderText="End Date" DataFormatString="{0:dd/MM/yyyy}" />
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GrdAltRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlpopupupload" runat="server" Visible="true" Height="300px">
                        <div id="div12">
                            <asp:HiddenField ID="hifoto" runat="server" />
                            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="hifoto"
                                PopupControlID="pnlFoto" BackgroundCssClass="ConfirmBackground">
                            </cc2:ModalPopupExtender>
                            <asp:Panel ID="pnlFoto" runat="server" Width="800px">
                                <div class="ConfirmForm">
                                    <table>
                                        <tr class="ConfirmFotoHeading">
                                            <td>
                                                <iframe id="frmFoto" runat="server" src="../UpLoadFile/PhotoCapture.aspx" name="frmFoto"
                                                    frameborder="0" style="width: 750px; height: 280px;"></iframe>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button runat="server" CssClass="button" ID="btnNo" Text="Close" OnClick="btnCloseImage_Click"
                                                    Width="60px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </td>
                <td valign="top">
                    <div class="divRowContainer" align="center" id="divImgbtnEmpDet" runat="server" visible="false">
                        <table width="100%" align="center">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnPersonal" runat="server" ImageUrl="~/Images/personal-Details.png"
                                        AlternateText="Personal" ToolTip="Personal" TabIndex="28" OnClick="imgbtnPersonal_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnAddress" runat="server" ImageUrl="~/Images/add-det.png"
                                        AlternateText="Address" ToolTip="Address" OnClick="imgbtnAddress_Click" TabIndex="29" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgBtnWorkDetails" runat="server" ImageUrl="~/Images/work-det.png"
                                        AlternateText="Work Details" ToolTip="Work Details" OnClick="imgBtnWorkDetails_Click"
                                        TabIndex="30" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgBtnContDet" runat="server" ImageUrl="~/Images/contact-det.png"
                                        AlternateText="Contact Details" ToolTip="Contact Details" OnClick="imgBtnContDet_Click"
                                        TabIndex="31" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnQualDet" runat="server" ImageUrl="~/Images/qua-det.png"
                                        AlternateText="Qualification Details" ToolTip="Qualification Details" OnClick="imgbtnQualDet_Click"
                                        TabIndex="32" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnSkillDet" runat="server" ImageUrl="~/Images/skills-Details.png"
                                        AlternateText="Skill Details" ToolTip="Skill Details" OnClick="imgbtnSkillDet_Click"
                                        TabIndex="33" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnBankDetails" runat="server" ImageUrl="~/Images/bank-det.png"
                                        AlternateText="Bank Details" ToolTip="Bank Details" OnClick="imgbtnBankDetails_Click"
                                        TabIndex="34" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnPPDet" runat="server" ImageUrl="~/Images/Document-det.png"
                                        AlternateText="Passport" ToolTip="Passport" OnClick="imgbtnPPDet_Click" TabIndex="35" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnPasswordDet" runat="server" ImageUrl="~/Images/Self-Service.png"
                                        AlternateText="Password" ToolTip="Password" OnClick="imgbtnPasswrodDet_Click"
                                        Visible="false" TabIndex="36" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imbbtnLeaveDetails" runat="server" ImageUrl="~/Images/Leave-Det.png"
                                        AlternateText="Leave Details" ToolTip="Leave Details" OnClick="imbbtnLeaveDetails_Click"
                                        TabIndex="37" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnSalaryDetails" runat="server" ImageUrl="~/Images/Salary-Det.png"
                                        AlternateText="Salary Details" ToolTip="Salary Details" OnClick="imgbtnSalaryDetails_Click"
                                        TabIndex="38" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })

            $("#FINContent_btnAddress").click(function (e) {
                return $("#form1").validationEngine('validate')
            })

            $("#FINContent_btnContDetAdd").click(function (e) {
                return $("#form1").validationEngine('validate')
            })

        }

    </script>
</asp:Content>
