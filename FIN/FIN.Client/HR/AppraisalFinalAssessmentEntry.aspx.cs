﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AppraisalFinalAssessmentEntry : PageBase
    {
        HR_APPRAISAL_FINAL_ASSESSMENT hR_APPRAISAL_FINAL_ASSESSMENT = new HR_APPRAISAL_FINAL_ASSESSMENT();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_APPRAISAL_FINAL_ASSESSMENT> userCtx = new DataRepository<HR_APPRAISAL_FINAL_ASSESSMENT>())
                    {
                        hR_APPRAISAL_FINAL_ASSESSMENT = userCtx.Find(r =>
                            (r.HR_APP_FINAL_ASSES_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_APPRAISAL_FINAL_ASSESSMENT;

                    ddlEmp.SelectedValue = hR_APPRAISAL_FINAL_ASSESSMENT.EMP_ID;
                    fillDeptDesig();
                    txtRat.Text = hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_RATING.ToString();
                    txtHrRat.Text = hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_HR_CEO_RATING.ToString();
                    txtchairmanrat.Text = hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_CHAIRMAN_RATING.ToString();
                    filltotrat();
                    txtTotRat.Text = hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_TOTAL_RATING.ToString();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeId(ref ddlEmp);
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_APPRAISAL_FINAL_ASSESSMENT = (HR_APPRAISAL_FINAL_ASSESSMENT)EntityData;
                }



                hR_APPRAISAL_FINAL_ASSESSMENT.EMP_ID = ddlEmp.SelectedValue;
                hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_RATING = decimal.Parse(txtRat.Text.ToString());
                hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_HR_CEO_RATING = decimal.Parse(txtHrRat.Text.ToString());
                hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_CHAIRMAN_RATING = decimal.Parse(txtchairmanrat.Text.ToString());
                hR_APPRAISAL_FINAL_ASSESSMENT.FINAL_TOTAL_RATING = decimal.Parse(txtTotRat.Text.ToString());

                hR_APPRAISAL_FINAL_ASSESSMENT.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                hR_APPRAISAL_FINAL_ASSESSMENT.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_APPRAISAL_FINAL_ASSESSMENT.MODIFIED_BY = this.LoggedUserName;
                    hR_APPRAISAL_FINAL_ASSESSMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_APPRAISAL_FINAL_ASSESSMENT.HR_APP_FINAL_ASSES_ID = FINSP.GetSPFOR_SEQCode("HR_110".ToString(), false, true);
                    hR_APPRAISAL_FINAL_ASSESSMENT.CREATED_BY = this.LoggedUserName;
                    hR_APPRAISAL_FINAL_ASSESSMENT.CREATED_DATE = DateTime.Today;
                }
                hR_APPRAISAL_FINAL_ASSESSMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPRAISAL_FINAL_ASSESSMENT.HR_APP_FINAL_ASSES_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_APPRAISAL_FINAL_ASSESSMENT>(hR_APPRAISAL_FINAL_ASSESSMENT);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_APPRAISAL_FINAL_ASSESSMENT>(hR_APPRAISAL_FINAL_ASSESSMENT, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_APPRAISAL_FINAL_ASSESSMENT>(hR_APPRAISAL_FINAL_ASSESSMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDeptDesig();
        }

        private void fillDeptDesig()
        {
            DataTable dtdeptdesig = new DataTable();
            DataTable dtgetcountofappraiser = new DataTable();
            DataTable dtrating = new DataTable();

            dtdeptdesig = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.GetDeptDesig(ddlEmp.SelectedValue)).Tables[0];
            if (dtdeptdesig.Rows.Count > 0)
            {
                txtDept.Text = dtdeptdesig.Rows[0]["DEPT_NAME"].ToString();
                txtDesig.Text = dtdeptdesig.Rows[0]["DESIG_NAME"].ToString();
            }


            dtgetcountofappraiser = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.Getcountofappraiser(ddlEmp.SelectedValue)).Tables[0];
            if (dtgetcountofappraiser.Rows.Count > 0)
            {
                hfRating.Value = dtgetcountofappraiser.Rows[0]["count"].ToString();
                dtrating = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.GetRating(ddlEmp.SelectedValue, int.Parse(hfRating.Value))).Tables[0];
                txtRat.Text = dtrating.Rows[0]["RATING"].ToString();
               

            }


        }

        protected void txtchairmanrat_TextChanged(object sender, EventArgs e)
        {
            filltotrat();
        }
        private void filltotrat()
        {
            DataTable dtTotrat = new DataTable();
            dtTotrat = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.GetTotalRating(decimal.Parse(txtRat.Text), decimal.Parse(txtHrRat.Text), decimal.Parse(txtchairmanrat.Text))).Tables[0];

            txtTotRat.Text = dtTotrat.Rows[0]["TOT_RAT"].ToString();


        }
    

    }
}