﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class DepartmentGraceTime : PageBase
    {
        TM_DEPT_GRACE tM_DEPT_GRACE = new TM_DEPT_GRACE();
        Departmentgrace_BLL Departmentgrace_BLL = new Departmentgrace_BLL();
        Boolean saveBool;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeptGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// AssignToControl Function is used to assign the data from table to the master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    using (IRepository<TM_DEPT_GRACE> userCtx = new DataRepository<TM_DEPT_GRACE>())
                    {
                        tM_DEPT_GRACE = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = tM_DEPT_GRACE;

                    ddlDept.SelectedValue = tM_DEPT_GRACE.TM_DEPT_ID;
                    txtGraceInTime.Text = tM_DEPT_GRACE.TM_GRACE_IN.ToString();
                    txtGraceOutTime.Text = tM_DEPT_GRACE.TM_GRACE_OUT.ToString();
                    txtStartDate.Text = DBMethod.ConvertDateToString(tM_DEPT_GRACE.TM_START_DATE.ToString());
                    //txtEndDate.Text = DBMethod.ConvertDateToString(tM_DEPT_GRACE.END_DT.ToString());
                    if (tM_DEPT_GRACE.TM_END_DATE != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(tM_DEPT_GRACE.TM_END_DATE.ToString());
                    }

                    if (tM_DEPT_GRACE.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeptGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        private void FillComboBox()
        {
            Departmentgrace_BLL.GetDepartmentName(ref ddlDept);

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tM_DEPT_GRACE = (TM_DEPT_GRACE)EntityData;
                }

                tM_DEPT_GRACE.TM_DEPT_ID = ddlDept.SelectedValue;
                tM_DEPT_GRACE.TM_GRACE_IN = CommonUtils.ConvertStringToDecimal(txtGraceInTime.Text);
                tM_DEPT_GRACE.TM_GRACE_OUT = CommonUtils.ConvertStringToDecimal(txtGraceOutTime.Text);
                tM_DEPT_GRACE.TM_START_DATE = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                //tM_DEPT_GRACE.ENABLED_FLAG = FINAppConstants.Y;
                tM_DEPT_GRACE.TM_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtEndDate.Text != null && txtEndDate.Text != string.Empty && txtEndDate.Text.ToString().Trim().Length > 0)
                {
                    tM_DEPT_GRACE.TM_END_DATE = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                if (chkActive.Checked == true)
                {
                    tM_DEPT_GRACE.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    tM_DEPT_GRACE.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    tM_DEPT_GRACE.MODIFIED_BY = this.LoggedUserName;
                    tM_DEPT_GRACE.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    tM_DEPT_GRACE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.tM_DEPT_GRACE_SEQ);
                    tM_DEPT_GRACE.CREATED_BY = this.LoggedUserName;
                    tM_DEPT_GRACE.CREATED_DATE = DateTime.Today;
                }

                tM_DEPT_GRACE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_DEPT_GRACE.PK_ID.ToString());

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<TM_DEPT_GRACE>(tM_DEPT_GRACE);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<TM_DEPT_GRACE>(tM_DEPT_GRACE, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeptGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlDept;
                slControls[1] = txtStartDate;
                slControls[2] = txtEndDate;
                slControls[3] = txtGraceInTime;
                slControls[4] = txtGraceOutTime;

                ErrorCollection.Clear();
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = "DropDownList~TextBox~TextBox~TextBox~TextBox";
                string strMessage = Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + " ~ " + Prop_File_Data["Grace_In_Time_P"] + " ~ " + Prop_File_Data["Grace_Out_Time_P"] + "";
                //string strMessage = "Department ~ Start Date ~End Date~Grace InTime~Grace OutTime";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeptGraceTimeSave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TM_DEPT_GRACE>(tM_DEPT_GRACE);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DeptGraceTimeDelete", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



    }
}