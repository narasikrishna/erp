﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingRequestEntry.aspx.cs" Inherits="FIN.Client.HR.Training_RequestEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style="width: 150px" id="lblDate">
            Date
        </div>
        <div class="divtxtBox  LNOrient" style="width: 150px">
            <asp:TextBox ID="txtDate" CssClass="validate[required,custom[ReqDateDDMMYYY] ] RequiredField txtBox" runat="server" TabIndex="1"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDate"
                OnClientDateSelectionChanged="checkDate" />
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                FilterType="Numbers,Custom" TargetControlID="txtDate" />
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 150px" id="lblAudianceTybe">
            Audience Type
        </div>
        <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:DropDownList ID="ddlAudianceTybe" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 150px" id="lblStatus">
            Status
        </div>
        <div class="divtxtBox  LNOrient" style="width: 150px">
            <asp:DropDownList ID="ddlStatus" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                runat="server">
            </asp:DropDownList>
        </div>
        <div class="colspace" style="float: left;">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 150px" id="lblNoofMembers">
            Program Name
        </div>
        <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:DropDownList ID="ddlProgrameName" TabIndex="4" runat="server" CssClass="EntryFont RequiredField ddlStype"
                Width="100%">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 150px" id="lblComments">
            Comments
        </div>
        <div class="divtxtBox  LNOrient" style="width: 530px">
            <asp:TextBox ID="txtComments" MaxLength="50" Height="50px" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                runat="server" TabIndex="5"></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            Width="600px" DataKeyNames="REQ_DTL_ID,DEPT_ID,LOOKUP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
            OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
            OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
            ShowFooter="True">
            <Columns>
                <asp:TemplateField HeaderText="Department Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlDepartmentName" TabIndex="12" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            Width="100%">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlDepartmentName" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            Width="100%">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDepartmentName" CssClass="adminFormFieldHeading" runat="server"
                            Text='<%# Eval("DEPT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Group">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlEmployeeGroup" TabIndex="13" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            Width="100%">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlEmployeeGroup" TabIndex="7" runat="server" CssClass="EntryFont RequiredField ddlStype"
                            Width="100%">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblGroupName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="No of Members">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtnoofmem" MaxLength="7" CssClass="RequiredField txtBox_N"
                            Text='<%# Eval("NO_OF_MEMBERS") %>' runat="server" TabIndex="14"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                            FilterType="Numbers,Custom" TargetControlID="txtnoofmem" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtnoofmem" MaxLength="7" CssClass=" RequiredField txtBox_N"
                            runat="server" TabIndex="8"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                            FilterType="Numbers,Custom" TargetControlID="txtnoofmem" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblnoofmem" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("NO_OF_MEMBERS") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                            TabIndex="10" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                            TabIndex="11" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                            TabIndex="15" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                            TabIndex="16" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                            TabIndex="9" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete" style="display: none">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
