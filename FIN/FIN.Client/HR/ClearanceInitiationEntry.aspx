﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ClearanceInitiationEntry.aspx.cs" Inherits="FIN.Client.HR.ClearanceInitiationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblEmployee">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="divClear_10">
            </div>
            <div class="LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    DataKeyNames="CLEARANCE_INIT_ID,DEPT_ID,EMP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                    OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="True" Width="700px">
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlDept" Width="350px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlDept" Width="350px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDept" Width="350px" runat="server" Text='<%# Eval("DEPT_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlEmpName" Width="350px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlEmpName" Width="350px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpName" Width="350px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkact" TabIndex="5" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="chkact" runat="server" TabIndex="5" Checked="true" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                    Enabled="false" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="6" runat="server" AlternateText="Edit" CausesValidation="false"
                                    ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="7" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="8" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="9" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                                TabIndex="11" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
