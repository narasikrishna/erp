﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using System.Data;

namespace FIN.Client.HR
{
    public partial class FeedbackTemplateTypeEntry : PageBase
    {
        HR_FEEDBACK_TEMPLATE_TYPE hR_FEEDBACK_TEMPLATE_TYPE = new HR_FEEDBACK_TEMPLATE_TYPE();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ErrorCollection.Clear();

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryPgLoad", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FIN.BLL.FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FIN.BLL.FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlRatingType, "FEEDBACK_RATING_TYP");
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FIN.BLL.FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_FEEDBACK_TEMPLATE_TYPE> userCtx = new DataRepository<HR_FEEDBACK_TEMPLATE_TYPE>())
                    {
                        hR_FEEDBACK_TEMPLATE_TYPE = userCtx.Find(r =>
                            (r.FEEDBACK_TYPE_ID == Master.StrRecordId.ToString())
                            ).SingleOrDefault();
                    }

                    EntityData = hR_FEEDBACK_TEMPLATE_TYPE;

                    txtTypeName.Text = hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_NAME.ToString();
                    if (hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_NAME_OL != null )
                    {
                        txtTypeNameOL.Text = hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_NAME_OL.ToString();
                    }
                    ddlRatingType.SelectedValue = hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_RATING_TYPE.ToString();
                    if (hR_FEEDBACK_TEMPLATE_TYPE.ENABLED_FLAG == FINAppConstants.Y)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the exam master table entities
        /// Fetch the CalendarID and OrganizationID from common properties which was stored while the user has been logined.
        /// </summary>

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    hR_FEEDBACK_TEMPLATE_TYPE = (HR_FEEDBACK_TEMPLATE_TYPE)EntityData;
                }

                hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_NAME = txtTypeName.Text.ToString();
                hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_NAME_OL = txtTypeNameOL.Text.ToString();
                hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_RATING_TYPE = ddlRatingType.SelectedValue.ToString();

                if (chkActive.Checked)
                {
                    hR_FEEDBACK_TEMPLATE_TYPE.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    hR_FEEDBACK_TEMPLATE_TYPE.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_FEEDBACK_TEMPLATE_TYPE.MODIFIED_BY = this.LoggedUserName;
                    hR_FEEDBACK_TEMPLATE_TYPE.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_FEEDBACK_TEMPLATE_TYPE.CREATED_BY = this.LoggedUserName;
                    hR_FEEDBACK_TEMPLATE_TYPE.CREATED_DATE = DateTime.Today;
                    hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_ID = FINSP.GetSPFOR_SEQCode("HR_R_085".ToString(), false, true);
                }

                hR_FEEDBACK_TEMPLATE_TYPE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_FEEDBACK_TEMPLATE_TYPE.FEEDBACK_TYPE_ID);

                DataTable dtDataDup = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.getFeedbackTemplateTypes(txtTypeName.Text, Master.StrRecordId)).Tables[0];
                if (dtDataDup.Rows.Count > 0)
                {

                    ErrorCollection.Add("data duplication", "Feedback Type Name already exists");
                    return;

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_FEEDBACK_TEMPLATE_TYPE>(hR_FEEDBACK_TEMPLATE_TYPE);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<HR_FEEDBACK_TEMPLATE_TYPE>(hR_FEEDBACK_TEMPLATE_TYPE, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATOBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                DBMethod.DeleteEntity<HR_FEEDBACK_TEMPLATE_TYPE>(hR_FEEDBACK_TEMPLATE_TYPE);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryYES", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



    }
}