﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanRequestEntry.aspx.cs" Inherits="FIN.Client.HR.LoanRequestEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtRequestNumber" Enabled="false" MaxLength="15" CssClass="txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
          <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:TextBox runat="server" TabIndex="2" ID="txtRequestDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtRequestDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 670px">
                <asp:DropDownList ID="ddlEmployeeName" TabIndex="3" CssClass="validate[required]  RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 680px">
                <asp:TextBox ID="txtDept" Enabled="false" CssClass=" RequiredField txtBox" 
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 680px">
                <asp:TextBox ID="txtDesignation" Enabled="false"  CssClass=" RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divEligRemark" runat="server" visible="false">
            <div class="lblBox LNOrient" style="width: 150px" id="lblELigRemarks">
                Eligible Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 680px">
                <asp:TextBox ID="txtEligibleRemarks" Height="30px" TextMode="MultiLine" Enabled="true"
                    CssClass="validate[required]  RequiredField txtBox" TabIndex="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblStatus">
                Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlType" CssClass="validate[required]  ddlStype" runat="server"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
            <%-- <div class="lblBox" style="float: left; width: 200px" id="lblStatus">
                Status
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlStatus" CssClass="validate[required]  ddlStype" runat="server" TabIndex="7">
                </asp:DropDownList>
            </div>--%>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 80px" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 140px">
                <asp:TextBox ID="txtAmount" MaxLength="13" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtAmount" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 110px" id="lblNoofDays">
                Payment Option
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlPaymentOption" runat="server" TabIndex="7" CssClass="validate[required] ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPaymentOption_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_Payroll" runat="server" visible="false">
            <div class="lblBox LNOrient" style="width: 150px" id="Div9">
                Pay Period
            </div>
            <div class="divtxtBox  LNOrient" style="width: 670px">
                <asp:DropDownList ID="ddlPayperiod" TabIndex="7" runat="server"  CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_bank" runat="server" visible="false">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblPONumber">
                    Bank Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px; height: 22px;">
                    <asp:DropDownList ID="ddlBankName" TabIndex="7" runat="server"  CssClass="validate[required]   ddlStype"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="width: 145px" id="lblPOLineNumber">
                    Bank Branch
                </div>
                <div class="divtxtBox  LNOrient" style="width: 260px">
                    <asp:DropDownList ID="ddlBankBranch" TabIndex="7" runat="server"  CssClass="validate[required]   ddlStype"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblItemDescription">
                    Account Number
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlAccountnumber" TabIndex="7" runat="server"  AutoPostBack="True"
                        OnSelectedIndexChanged="ddlAccountnumber_SelectedIndexChanged" CssClass="validate[required]   ddlStype">
                    </asp:DropDownList>
                </div>
                <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="width: 145px">
                    Cheque Number
                </div>
                <div class="divtxtBox  LNOrient" style="width: 260px">
                    <asp:DropDownList ID="ddlChequeNumber" TabIndex="7" runat="server"  CssClass="validate[required]   ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 680px">
                <asp:TextBox ID="txtComments" Height="30px" CssClass=" txtBox" TextMode="MultiLine"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblNoOfInstallments">
                No. Of Installments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtNoOfInstallments" MaxLength="3" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" AutoPostBack="true" TabIndex="9" OnTextChanged="txtNoOfInstallments_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtNoOfInstallments" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblInstallmentAmount">
                Installment Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtInstallmentAmount" MaxLength="16" Enabled="true" CssClass="txtBox_N"
                    runat="server" TabIndex="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtInstallmentAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblPaymentReleaseDate">
                Payment Release Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" TabIndex="11" ID="txtPaymentReleaseDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtPaymentReleaseDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPaymentReleaseDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblLoanStartDate">
                Loan Start Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox TabIndex="12" runat="server" ID="txtLoanStartDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtLoanStartDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLoanStartDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="ChkActive" runat="server" Checked="True" TabIndex="13" />
            </div>
        </div>
        <asp:HiddenField ID="hfdempdays_fromjoining" runat="server" />
        <asp:HiddenField ID="hfloaneligibleindays" runat="server" />
        <asp:HiddenField ID="hflastlnreqdt" runat="server" />
        <asp:HiddenField ID="hflnreeligibledt" runat="server" />
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                             />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn"  />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDeleteas">
            <asp:HiddenField ID="hfallowloan" runat="server" />
            <cc2:ModalPopupExtender ID="mpAllowloan" runat="server" TargetControlID="hfallowloan"
                PopupControlID="pnlConfirm" CancelControlID="Button3" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" CssClass="lblBox" runat="server" Text="Selected employee has not complete the eligible criteria for loan are you sure to allow for furthur loan"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" OnClick="btNo_Click" />
                                <asp:Button runat="server" CssClass="button" ID="Button3" Text="No" Width="60px"
                                    OnClick="btNo_Click" Style="display: none" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divEligibleCond">
            <asp:HiddenField ID="hfEligibleAllow" runat="server" />
            <cc2:ModalPopupExtender ID="mpeEligible" runat="server" TargetControlID="hfEligibleAllow"
                PopupControlID="pnlEligible" CancelControlID="Button2" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlEligible" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="Label1" CssClass="lblBox" runat="server" Text="Selected employee Not Eligible for System condition. Do YouWant to Proceed"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="btn" ID="Button1" Text="Yes" Width="60px" OnClick="Button1_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="btn" ID="btnEligibleNO" Text="No" Width="60px"
                                    OnClick="btnEligibleNO_Click" />
                                <asp:Button runat="server" CssClass="btn" ID="Button2" Text="No" Style="display: none"
                                    Width="60px" OnClick="btnEligibleNO_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

       

    </script>
</asp:Content>
