﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GradesEntry.aspx.cs" Inherits="FIN.Client.HR.GradesEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1300px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblCategory">
                Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
         <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="lblJob">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddljob" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddljob_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblCategoryCode">
                Position
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:DropDownList ID="ddlPosition" CssClass="validate[required] RequiredField  ddlStype"
                    TabIndex="3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoryCode_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 148px; display: none" id="lblCategoryName">
                Position Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px; display: none">
                <asp:TextBox ID="txtCategoryName" CssClass="txtBox" runat="server" TabIndex="4" Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="GRADE_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Grade">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcode" Width="130px" TabIndex="15" MaxLength="10" runat="server"
                                CssClass=" RequiredField  txtBox_en" Text='<%# Eval("GRADE_CODE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="-"
                                FilterType="UppercaseLetters,LowercaseLetters,Numbers" TargetControlID="txtcode" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtcode" Width="130px" MaxLength="10" runat="server" CssClass="RequiredField   txtBox_en"
                                TabIndex="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="-"
                                FilterType="UppercaseLetters,LowercaseLetters,Numbers" TargetControlID="txtcode" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcode" Width="100px" runat="server" Text='<%# Eval("GRADE_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdesc" MaxLength="200" TabIndex="16" runat="server" CssClass=" RequiredField  txtBox_en"
                                Text='<%# Eval("GRADE_DESC") %>' Width="280px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdesc" MaxLength="200" runat="server" CssClass="RequiredField   txtBox_en"
                                TabIndex="6" Width="280px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldesc" runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap; " Text='<%# Eval("GRADE_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcodeAR" Width="130px" TabIndex="17" MaxLength="20" runat="server"
                                CssClass=" txtBox_ol" Text='<%# Eval("GRADE_CODE_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtcodeAR" Width="130px" MaxLength="20" runat="server" CssClass=" txtBox_ol"
                                TabIndex="7"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcodeAR" Width="100px" runat="server" Text='<%# Eval("GRADE_CODE_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdescAR" MaxLength="200" TabIndex="18" runat="server" CssClass="  txtBox_ol"
                                Text='<%# Eval("GRADE_DESC_OL") %>' Width="280px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdescAR" MaxLength="200" runat="server" CssClass=" txtBox_ol"
                                TabIndex="8" Width="280px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldescAR"  runat="server" Style="word-wrap: break-word;
                                white-space: pre-wrap; " Text='<%# Eval("GRADE_DESC_OL") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="19" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="130px" Text='<%#  Eval("EFFECTIVE_FROM_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                TabIndex="9" Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" Width="100px" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="20" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Width="130px" Text='<%#  Eval("EFFECTIVE_TO_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                TabIndex="10" Width="130px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" Width="100px" runat="server" Text='<%# Eval("EFFECTIVE_TO_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" TabIndex="21" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked="true" TabIndex="11" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="13" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="14" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="22" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="23" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="12" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
