﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using FIN.BLL.HR;
using FIN.DAL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LeaveApplicationEntry : PageBase
    {
        HR_LEAVE_APPLICATIONS hR_LEAVE_APPLICATIONS = new HR_LEAVE_APPLICATIONS();
        LeaveApplication_BLL LeaveApplication_BLL = new LeaveApplication_BLL();
        string ProReturn = null;
        DataTable Month_data = new DataTable();
        static int month;
        Boolean saveBool = false;
        Dictionary<string, string> Prop_File_Data;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL_LA", ex.Message);
            }
            finally
            {
                // ErrorCollection.Add("SS", "Success");
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }



            UserRightsChecking();



        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void ChangeLanguage()
        {
            if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
            {
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                lblApplicationStatus.InnerHtml = Prop_File_Data["Application_Status_P"];
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtNoOfDays.Enabled = false;
                FillComboBox();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                   
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    //lblAsOnDate.Text = Prop_File_Data["As_on_P"];
                   
                }
                else
                {
                    lblAsOnDate.Text = txtLeaveFrom.Text;
                    
                }
               
                EntityData = null;

                txtApplicationDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                //ddlActingEmp.Visible = false;
                chkPlanUnplan.Enabled = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    //divActingEmp.Visible = true;

                    hR_LEAVE_APPLICATIONS = LeaveApplication_BLL.getClassEntity(Master.StrRecordId);

                    //using (IRepository<HR_LEAVE_APPLICATIONS> userCtx = new DataRepository<HR_LEAVE_APPLICATIONS>())
                    //{
                    //    hR_LEAVE_APPLICATIONS = userCtx.Find(r =>
                    //        (r.PK_ID == Master.RecordID)
                    //        ).SingleOrDefault();
                    //}

                    EntityData = hR_LEAVE_APPLICATIONS;

                    ddlFinancialYear.SelectedValue = hR_LEAVE_APPLICATIONS.FISCAL_YEAR.ToString();
                    //filldept();
                    ddlDepartment.SelectedValue = hR_LEAVE_APPLICATIONS.DEPT_ID;
                    fill_Lvtyp_Staff();
                    ddlStaffName.SelectedValue = hR_LEAVE_APPLICATIONS.STAFF_ID;
                    fillLeaveTyp();
                    ddlLeaveType.SelectedValue = hR_LEAVE_APPLICATIONS.LEAVE_ID;
                    fillleavebal();
                    txtApplicationDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_APPLICATIONS.APPLICATION_DATE.ToString());
                    txtLeaveFrom.Text = DBMethod.ConvertDateToString(hR_LEAVE_APPLICATIONS.LEAVE_DATE_FROM.ToString());
                    txtLeaveTo.Text = DBMethod.ConvertDateToString(hR_LEAVE_APPLICATIONS.LEAVE_DATE_TO.ToString());
                    txtNoOfDays.Text = hR_LEAVE_APPLICATIONS.NO_OF_DAYS.ToString();
                    txtReason.Text = hR_LEAVE_APPLICATIONS.REASON;
                    ddlLevel.SelectedValue = hR_LEAVE_APPLICATIONS.ATTRIBUTE2;
                    ddlApplicationStatus.SelectedValue = hR_LEAVE_APPLICATIONS.APP_STATUS;
                    txtLeaveAddress.Text = hR_LEAVE_APPLICATIONS.LEAVE_ADD;
                    txtContactNumber.Text = hR_LEAVE_APPLICATIONS.LEAVE_CONTACT_NO;

                    if (hR_LEAVE_APPLICATIONS.HALF_DAY_FROM != null)
                    {
                        if (hR_LEAVE_APPLICATIONS.HALF_DAY_FROM.Trim() == FINAppConstants.Y)
                        {
                            HalfDayFrom.Checked = true;
                        }
                        else
                        {
                            HalfDayFrom.Checked = false;
                        }
                    }
                    if (hR_LEAVE_APPLICATIONS.HALF_DAY_TO != null)
                    {
                        if (hR_LEAVE_APPLICATIONS.HALF_DAY_TO.Trim() == FINAppConstants.Y)
                        {
                            HalfDayTo.Checked = true;
                        }
                        else
                        {
                            HalfDayTo.Checked = false;
                        }
                    }
                    if (hR_LEAVE_APPLICATIONS.PLANNED_UNPLANNED_LEAVE != null)
                    {
                        if (hR_LEAVE_APPLICATIONS.PLANNED_UNPLANNED_LEAVE.Trim() == FINAppConstants.Y)
                        {
                            chkPlanUnplan.Checked = true;
                        }
                        else
                        {
                            chkPlanUnplan.Checked = false;
                        }
                    }

                    if (hR_LEAVE_APPLICATIONS.IS_HANDOVER_REQUIRED != null)
                    {
                        if (hR_LEAVE_APPLICATIONS.IS_HANDOVER_REQUIRED.Trim() == FINAppConstants.Y)
                        {
                            chkHandOver.Checked = true;
                        }
                        else
                        {
                            chkHandOver.Checked = false;
                        }
                    }

                    if (hR_LEAVE_APPLICATIONS.SUBSTITUTE_EMP_ID != null)
                    {
                        ddlActingEmp.SelectedValue = hR_LEAVE_APPLICATIONS.SUBSTITUTE_EMP_ID;
                    }

                    ddlActingEmp.Items.Remove(new ListItem(ddlStaffName.SelectedItem.Text, ddlStaffName.SelectedValue.ToString()));
                    txtRemarks.Text = hR_LEAVE_APPLICATIONS.ATTRIBUTE1;

                    if (hR_LEAVE_APPLICATIONS.ATTRIBUTE2 != null)
                    {
                        txtAvailLeave.Text = hR_LEAVE_APPLICATIONS.ATTRIBUTE2;
                        if (txtAvailLeave.Text.Length > 0)
                        {
                            txtAvailLeave.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAvailLeave.Text);
                        }
                    }

                    divremarks.Visible = true;
                    lblApplicationStatus.Visible = true;
                    ddlApplicationStatus.Visible = true;
                    if (hR_LEAVE_APPLICATIONS.APP_STATUS != null)
                    {

                        if (hR_LEAVE_APPLICATIONS.APP_STATUS.ToUpper() == "APPROVED")
                        {
                            ddlFinancialYear.Enabled = false;
                            ddlDepartment.Enabled = false;
                            ddlActingEmp.Enabled = false;
                            ddlLeaveType.Enabled = false;
                            ddlStaffName.Enabled = false;
                            txtRemarks.Enabled = false;
                            txtReason.Enabled = false;
                            txtLeaveTo.Enabled = false;
                            txtLeaveFrom.Enabled = false;
                            txtLeaveAddress.Enabled = false;
                            txtContactNumber.Enabled = false;
                            btnSave.Visible = false;
                            ddlApplicationStatus.Enabled = false;
                            HalfDayFrom.Enabled = false;
                            HalfDayTo.Enabled = false;
                            chkPlanUnplan.Enabled = false;
                            chkHandOver.Enabled = false;

                        }
                        else
                        {
                            divActingEmp.Visible = true;
                        }

                    }
                    fn_ApplicationStatusChange();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            // LeaveApplication_BLL.GetDepartment(ref ddlDepartment);
            Lookup_BLL.GetLookUpValues(ref ddlApplicationStatus, "SS");
            Lookup_BLL.GetLookUpValues(ref ddlLevel, "LC_CONDITION");
            Employee_BLL.GetEmpName(ref ddlActingEmp);
            Employee_BLL.GetEmpName(ref ddlStaffName);

        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //filldept();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void filldept()
        {
            LeaveDepartment_BLL.GetDeptBasedFisYear(ref ddlDepartment, ddlFinancialYear.SelectedValue);
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fill_Lvtyp_Staff();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fill_Lvtyp_Staff()
        {


            Employee_BLL.GetEmplNameBothIntExt(ref ddlStaffName, ddlDepartment.SelectedValue);
            // Employee_BLL.GetEmplNameBothIntExt(ref ddlActingEmp, ddlDepartment.SelectedValue);
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_APPLICATIONS = (HR_LEAVE_APPLICATIONS)EntityData;
                }
                hR_LEAVE_APPLICATIONS.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());
                hR_LEAVE_APPLICATIONS.DEPT_ID = ddlDepartment.SelectedValue;
                hR_LEAVE_APPLICATIONS.LEAVE_ID = ddlLeaveType.SelectedValue;
                hR_LEAVE_APPLICATIONS.APPLICATION_DATE = DBMethod.ConvertStringToDate(txtApplicationDate.Text.ToString());
                hR_LEAVE_APPLICATIONS.STAFF_ID = ddlStaffName.SelectedValue;
                hR_LEAVE_APPLICATIONS.LEAVE_DATE_FROM = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                hR_LEAVE_APPLICATIONS.LEAVE_DATE_TO = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());
                hR_LEAVE_APPLICATIONS.NO_OF_DAYS = int.Parse(txtNoOfDays.Text.ToString());
                hR_LEAVE_APPLICATIONS.REASON = txtReason.Text;
                hR_LEAVE_APPLICATIONS.ATTRIBUTE2 = ddlLevel.SelectedValue;
                hR_LEAVE_APPLICATIONS.APP_STATUS = ddlApplicationStatus.SelectedValue;
                hR_LEAVE_APPLICATIONS.LEAVE_ADD = txtLeaveAddress.Text;
                hR_LEAVE_APPLICATIONS.LEAVE_CONTACT_NO = txtContactNumber.Text;
                hR_LEAVE_APPLICATIONS.ATTRIBUTE1 = txtRemarks.Text;
                hR_LEAVE_APPLICATIONS.ATTRIBUTE2 = txtAvailLeave.Text;
                hR_LEAVE_APPLICATIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (HalfDayFrom.Checked == true)
                {
                    hR_LEAVE_APPLICATIONS.HALF_DAY_FROM = FINAppConstants.Y;
                }
                else
                {
                    hR_LEAVE_APPLICATIONS.HALF_DAY_FROM = FINAppConstants.N;
                }
                if (HalfDayTo.Checked == true)
                {
                    hR_LEAVE_APPLICATIONS.HALF_DAY_TO = FINAppConstants.Y;
                }
                else
                {
                    hR_LEAVE_APPLICATIONS.HALF_DAY_TO = FINAppConstants.N;
                }

                if (chkPlanUnplan.Checked == true)
                {
                    hR_LEAVE_APPLICATIONS.PLANNED_UNPLANNED_LEAVE = FINAppConstants.Y;
                }
                else
                {
                    hR_LEAVE_APPLICATIONS.PLANNED_UNPLANNED_LEAVE = FINAppConstants.N;
                }
                if (chkHandOver.Checked == true)
                {
                    hR_LEAVE_APPLICATIONS.IS_HANDOVER_REQUIRED = FINAppConstants.Y;
                }
                else
                {
                    hR_LEAVE_APPLICATIONS.IS_HANDOVER_REQUIRED = FINAppConstants.N;
                }

                hR_LEAVE_APPLICATIONS.ENABLED_FLAG = FINAppConstants.Y;

                if (ddlActingEmp.SelectedValue.ToString().Length > 0)
                {
                    hR_LEAVE_APPLICATIONS.SUBSTITUTE_EMP_ID = ddlActingEmp.SelectedValue;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LEAVE_APPLICATIONS.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_APPLICATIONS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_023.ToString(), false, true);

                    hR_LEAVE_APPLICATIONS.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_APPLICATIONS.CREATED_DATE = DateTime.Today;


                }
                hR_LEAVE_APPLICATIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                LeaveApplication_BLL.DeleteEntity(Master.StrRecordId);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_BTNDEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();
                slControls[0] = ddlFinancialYear;
                slControls[1] = ddlDepartment;
                slControls[2] = txtApplicationDate;
                slControls[3] = ddlStaffName;
                slControls[4] = ddlLeaveType;
                //   slControls[5] = ddlApplicationStatus;
                slControls[5] = txtLeaveFrom;
                slControls[6] = txtLeaveFrom;
                slControls[7] = txtLeaveTo;
                slControls[8] = txtLeaveTo;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList~TextBox~DropDownList~DropDownList~TextBox~DateTime~TextBox~DateTime";
                string strMessage = Prop_File_Data["Financial_Year_P"] + " ~ " + Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Application_Date_P"] + " ~ " + Prop_File_Data["Staff_Name_P"] + " ~ " + Prop_File_Data["Leave_Type_P"] + " ~ " + Prop_File_Data["Leave_From_P"] + " ~ " + Prop_File_Data["Leave_To_P"] + " ~ " + Prop_File_Data["Leave_To_P"] + "";
                //string strMessage = "Financial Year ~ Department ~ Application Date ~ Staff Name ~ Leave Type ~ Application Status ~ Leave From ~ Leave From ~ Leave To ~ Leave To";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                if (ErrorCollection.Count > 0)
                    return;

                // validation not required by Natesh 18-12-14
                //if (DBMethod.ConvertStringToDate(txtApplicationDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString())) 
                //{
                //    ErrorCollection.Add("daterange", Prop_File_Data["daterange_P"]);

                //    return;

                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                if (DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString()) > DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString()))
                {
                    ErrorCollection.Add("daterange", Prop_File_Data["daterangeTo_P"]);

                    return;

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                string str_Error = LeaveApplication_BLL.ValidateEntity(hR_LEAVE_APPLICATIONS);
                if (str_Error.Length > 0)
                {
                    ErrorCollection.Add("EntityValidation_AG", str_Error);
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_DUPLICATE_CHECK(hR_LEAVE_APPLICATIONS.STAFF_ID, txtLeaveFrom.Text, txtLeaveTo.Text, hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("LEAVAPPexists", ProReturn);

                        // Check whether this leave date range cancelled or not...

                        ProReturn = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_LEAVE_CANCEL_OR_NOT(hR_LEAVE_APPLICATIONS.STAFF_ID, ddlFinancialYear.SelectedValue, txtLeaveFrom.Text, txtLeaveTo.Text, hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID);

                        if ((ProReturn != string.Empty && ProReturn != null) && (ProReturn != "Requested Leave Application Date is already in processing"))
                        {
                            ErrorCollection.Remove("LEAVAPPexists");
                            // ErrorCollection.Add("LEAVCanceled", ProReturn);
                        }
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DataTable dt_assignedSub = FIN.BLL.HR.LeaveApplication_BLL.get_Leave_Emp_Assign_Substitue_chk(hR_LEAVE_APPLICATIONS.STAFF_ID, txtLeaveFrom.Text, txtLeaveTo.Text);
                if (dt_assignedSub.Rows.Count > 0)
                {
                    ErrorCollection.Add("EmpAssigenedassubstit45", Prop_File_Data["Emp_Assign_as_Substitue_P"]);
                    return;
                }
                if (ddlActingEmp.Visible)
                {
                    if (ddlActingEmp.SelectedValue.ToString().Length > 0)
                    {
                        dt_assignedSub = FIN.BLL.HR.LeaveApplication_BLL.get_Substitue_emp_Leave_chk(hR_LEAVE_APPLICATIONS.SUBSTITUTE_EMP_ID, txtLeaveFrom.Text, txtLeaveTo.Text);
                        if (dt_assignedSub.Rows.Count > 0)
                        {
                            ErrorCollection.Add("EmpAssigenedassubstit", Prop_File_Data["Emp_Assign_as_Substitue_P"]);
                            //return;
                        }
                    }
                }

                LeaveApplication_BLL.SaveEntity(hR_LEAVE_APPLICATIONS, Master.Mode, true);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    saveBool = true;
                }
                Month_data = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetMonth(txtLeaveFrom.Text)).Tables[0];
                month = int.Parse(Month_data.Rows[0][0].ToString());
                DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (Master.StrRecordId != string.Empty && Master.Mode != FINAppConstants.Add)
                    {
                        if (hR_LEAVE_APPLICATIONS.APP_STATUS.ToUpper() == "APPROVED")
                        {

                            Alert_BLL.GenerateEmail(FINMessageConstatns.Leave_Acting_Emp_Alert);
                            Alert_BLL.GenerateEmail(FINMessageConstatns.Leave_Approved_Alert);

                            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && saveBool == true)
                            {
                                if (VMVServices.Web.Utils.IsAlert == "1")
                                {
                                    FINSQL.UpdateAlertUserLevel();
                                }
                            }

                            if (ErrorCollection.Count > 0)
                            {
                                return;
                            }
                            StaffLeaveDetails_DAL.GetSP_UPDATE_STAFF_ATTENDANCE(this.LoggedUserName, hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID.ToString(), hR_LEAVE_APPLICATIONS.REASON, txtLeaveFrom.Text.ToString(), CommonUtils.ConvertStringToDecimal(month.ToString()), ddlFinancialYear.SelectedValue.ToString(), ddlStaffName.SelectedValue.ToString(), hR_LEAVE_APPLICATIONS.ORG_ID.ToString(), hR_LEAVE_APPLICATIONS.LEAVE_ID.ToString(), ddlDepartment.SelectedValue.ToString(), CommonUtils.ConvertStringToDecimal(hR_LEAVE_APPLICATIONS.NO_OF_DAYS.ToString()));

                            StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("LTAKEN", hR_LEAVE_APPLICATIONS.LEAVE_REQ_ID.ToString(), "LRA", txtLeaveFrom.Text.ToString(), CommonUtils.ConvertStringToDecimal(month.ToString()), ddlFinancialYear.SelectedValue.ToString(), ddlStaffName.SelectedValue.ToString(), hR_LEAVE_APPLICATIONS.ORG_ID.ToString(), hR_LEAVE_APPLICATIONS.LEAVE_ID.ToString(), ddlDepartment.SelectedValue.ToString(), CommonUtils.ConvertStringToDecimal(hR_LEAVE_APPLICATIONS.NO_OF_DAYS.ToString()));
                            StaffLeaveDetails_DAL.UPDATE_EMP_LEAVE_BALANCE(ddlFinancialYear.SelectedValue.ToString(), ddlStaffName.SelectedValue.ToString(), hR_LEAVE_APPLICATIONS.ORG_ID.ToString(), hR_LEAVE_APPLICATIONS.LEAVE_ID.ToString(), ddlDepartment.SelectedValue.ToString(), decimal.Parse(hR_LEAVE_APPLICATIONS.NO_OF_DAYS.ToString()), "APPLICATION");

                        }
                    }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void txtLeaveFrom_TextChanged(object sender, EventArgs e)
        {
            DateChangedEvent();
            lblAsOnDate.Text = txtLeaveFrom.Text;
            txtAsOnDateLeaveBalance.Text = FIN.DAL.HR.LeaveApplication_DAL.getAsonDateLeaveBalance(ddlDepartment.SelectedValue, ddlStaffName.SelectedValue, ddlLeaveType.SelectedValue, txtLeaveFrom.Text);

        }
        private void DateChangedEvent()
        {
            try
            {
                ErrorCollection.Clear();
                ErrorCollection.Remove("Asondateleave");
                if (txtLeaveFrom.Text.Trim() != string.Empty && txtLeaveTo.Text.Trim() != string.Empty)
                {
                    //DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                    //DateTime ToDate = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());

                    //if (FrmDate == ToDate)
                    //{
                    //    txtNoOfDays.Text = "1";
                    //}
                    //else
                    //{
                    //    int count_friday = ClsGridBase.CountDays(DayOfWeek.Friday, FrmDate, ToDate);
                    //    txtNoOfDays.Text = (((ToDate - FrmDate).Days + 1) - count_friday).ToString();
                    //}


                    txtNoOfDays.Text = FIN.DAL.HR.LeaveApplication_DAL.getNoOf_days(txtLeaveFrom.Text, txtLeaveTo.Text);


                    if (LeaveApplication_DAL.IsPlannedLeave(txtLeaveFrom.Text, txtLeaveTo.Text, ddlStaffName.SelectedValue))
                    {
                        chkPlanUnplan.Checked = true;
                    }
                    else
                    {
                        chkPlanUnplan.Checked = false;
                    }

                    if (CommonUtils.ConvertStringToDecimal(txtNoOfDays.Text) > CommonUtils.ConvertStringToDecimal(txtAsOnDateLeaveBalance.Text))
                    {
                        ErrorCollection.Add("Asondateleave","No of days should not be greater than the Leave Balance");
                        return;
                    }
                }

                //if (txtLeaveFrom.Text.Trim() != string.Empty && txtLeaveTo.Text.Trim() != string.Empty)
                //{
                //    DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                //    DateTime ToDate = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());

                //    if (FrmDate == ToDate)
                //    {
                //        txtNoOfDays.Text = "1";
                //    }
                //    else
                //    {
                //        txtNoOfDays.Text = ((ToDate - FrmDate).Days + 1).ToString();
                //    }
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
              
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Sassvde_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtLeaveTo_TextChanged(object sender, EventArgs e)
        {
            DateChangedEvent();

        }

        protected void txtApplicationDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtNoOfDays_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillleavebal();
        }


        private void fillleavebal()
        {
            //DataTable dtleavebal = new DataTable();
            //dtleavebal = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveAvail(ddlFinancialYear.SelectedValue, ddlStaffName.SelectedValue, ddlLeaveType.SelectedValue)).Tables[0];
            //if (dtleavebal.Rows.Count > 0)
            //{
            //    txtAvailLeave.Text = dtleavebal.Rows[0]["LEAVE_AVAIL"].ToString();
            //}
            if (Master.Mode == FINAppConstants.Add)
            {
                //txtAvailLeave.Text = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_EMP_LEAVE_BAL(ddlFinancialYear.SelectedValue, ddlDepartment.SelectedValue, ddlStaffName.SelectedValue, ddlLeaveType.SelectedValue);
                txtAvailLeave.Text = DBMethod.GetDecimalValue(StaffLeaveDetails_DAL.getAccuredleavebalnce(ddlDepartment.SelectedValue, ddlStaffName.SelectedValue, ddlLeaveType.SelectedValue, txtApplicationDate.Text, ddlFinancialYear.SelectedValue)).ToString();
            }
            if (txtAvailLeave.Text.Length > 0)
            {
                txtAvailLeave.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAvailLeave.Text);
            }

            if (ddlLeaveType.SelectedValue == "CLD")
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }

        }

        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillLeaveTyp();
            //  Employee_BLL.GetEmployee_NotinAppliedemp(ref ddlActingEmp, ddlStaffName.SelectedValue);
        }

        private void fillLeaveTyp()
        {
            DataTable dt_empdept = Employee_BLL.getEmployeeCurrentDepartment(ddlStaffName.SelectedValue);
            if (dt_empdept.Rows.Count > 0)
            {
                ddlDepartment.SelectedValue = dt_empdept.Rows[0]["EMP_DEPT_ID"].ToString();
            }
            LeaveDefinition_BLL.GetLeaveBasedFiscalYearDept(ref ddlLeaveType, ddlFinancialYear.SelectedValue.ToString(), ddlDepartment.SelectedValue.ToString(), ddlStaffName.SelectedValue);
        }

        protected void ddlApplicationStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_ApplicationStatusChange();
        }
        private void fn_ApplicationStatusChange()
        {
            if (ddlApplicationStatus.SelectedValue.ToUpper().Trim() == "APPROVED")
            {
                divActingEmp.Visible = true;
            }
            else
            {
                divActingEmp.Visible = false;
            }
        }

    }
}