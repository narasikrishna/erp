﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpLateDeduction.aspx.cs" Inherits="FIN.Client.HR.EmpLateDeduction" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function shrinkandgrow(input) {
            var displayIcon = "img" + input;
            if ($("#" + displayIcon).attr("src") == "../Images/expand_blue.png") {
                $("#" + displayIcon).closest("tr")
			    .after("<tr><td></td><td colspan = '100%'>" + $("#" + input)
			    .html() + "</td></tr>");
                $("#" + displayIcon).attr("src", "../Images/collapse_blue.png");
            } else {
                $("#" + displayIcon).closest("tr").next().remove();
                $("#" + displayIcon).attr("src", "../Images/expand_blue.png");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFinancialYear">
                Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlYear" runat="server" onchange="PopulateDays()" Width="250px"
                    TabIndex="1" CssClass="validate[required] RequiredField ddlStype" />
            </div>
            <%-- <div class="lblBox" style="float: left; width: 150px" id="lblFromDate">
                Description
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:TextBox ID="txtDescription" TabIndex="2" Width="250px" MaxLength="50" runat="server"
                    CssClass="RequiredField txtBox"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAttendanceDate">
                Month
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlMonth" runat="server" onchange="PopulateDays()" Width="250px"
                    TabIndex="2" CssClass="validate[required] RequiredField ddlStype" />
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:ImageButton ID="btnimport" runat="server" ImageUrl="~/Images/btnShow.png" OnClick="btnimport_Click" TabIndex="3"
                    Style="border: 0px;" />
                <%--<asp:Button ID="btnimport" runat="server" Text="Show" CssClass="btn" OnClick="btnimport_Click"
                    TabIndex="3" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvParentData" runat="server" AutoGenerateColumns="False" Width="900px"
                CssClass="DisplayFont Grid" ShowFooter="false" DataKeyNames="emp_no,TM_SCH_CODE,tm_emp_group,EMP_ID">
                <Columns>
                  
                    <asp:BoundField DataField="EMP_no" HeaderText="Employee No" />
                    <asp:BoundField DataField="EMP_FIRST_NAME" HeaderText="Employee Name" />
                    <asp:BoundField DataField="TM_EMP_GROUP" HeaderText="Employee Group" />
                    <asp:BoundField DataField="TM_SCH_CODE" HeaderText="Schedule" />
                    <asp:BoundField DataField="TM_LATE_MIN" HeaderText="Total Late in Minutes">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                OnClick="btnDetails_Click" CommandName="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>

                      <asp:TemplateField ItemStyle-Width="40px" Visible="false">
                        <ItemTemplate>
                            <%--  <a href="JavaScript:shrinkandgrow('div<%# Eval("EMP_ID") %>');">
                                <img alt="Details" id="imgdiv<%# Eval("EMP_ID") %>" src="../Images/expand_blue.png" />
                            </a>--%>
                            <div id="div<%# Eval("EMP_ID") %>" style="display: none;">
                                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                    DataKeyNames="LATE_TM_ID,EMP_ID">
                                    <Columns>
                                        <asp:BoundField DataFormatString="{0:dd/MM/yyyy}" DataField="TM_DATE" HeaderText="Date" />
                                        <asp:BoundField DataField="TM_LATE_MIN" HeaderText="Late in Minutes" />
                                        <asp:BoundField DataField="TM_LATE_AMT" HeaderText="Deduction Amount" />
                                        <asp:TemplateField HeaderText="Deducted">
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" OnCheckedChanged="chkActive_CheckedChanged" AutoPostBack="true"
                                                    ID="chkSelection" Checked='<%# Convert.ToBoolean(Eval("attribute1")) %>' />
                                                <asp:Label ID="lblId" Visible="false" Width="150px" runat="server" Text='<%# Eval("LATE_TM_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 800px;">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="btnDetails1"
                PopupControlID="Panel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="500px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td>
                                <asp:Label runat="server" ID="lblErrorMsg" Visible="false" Text="No Record Found"
                                    Style="color: Red;"></asp:Label>
                            </td>
                        </tr>
                        <tr class="LNOrient">
                            <td colspan="4">
                                <div id="div_ABB">
                                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" DataKeyNames="LATE_TM_ID,EMP_ID" OnRowDataBound="gvData_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataFormatString="{0:dd/MM/yyyy}" DataField="TM_DATE" HeaderText="Date" />
                                            <asp:BoundField DataField="TM_LATE_MIN" HeaderText="Late in Minutes">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TM_LATE_AMT" HeaderText="Deduction Amount">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Deducted">
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" OnCheckedChanged="chkActive_CheckedChanged" AutoPostBack="true"
                                                        ID="chkSelection" Checked='<%# Convert.ToBoolean(Eval("attribute1")) %>' />
                                                    <asp:Label ID="lblId" Visible="false" Width="150px" runat="server" Text='<%# Eval("LATE_TM_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: left">
                                    <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Ok"
                                        OnClick="btnOk_Click" Width="60px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
