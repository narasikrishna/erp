﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ApplicantEntry.aspx.cs" Inherits="FIN.Client.HR.ApplicantEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblApplicantId">
                Applicant Id
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtApplicantId" CssClass="txtBox" MaxLength="10" runat="server"
                    TabIndex="1" Enabled="false"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div22">
                Upload Resume
            </div>
            <div class="divtxtBox  LNOrient" style="width: 45px; height: 33px;">
                <%--<asp:FileUpload ID="FileUpload1" runat="server" CssClass="EntryFont txtBox" />--%>
                <asp:ImageButton ID="img_Photo" ImageUrl="~/Images/UploadLogo.jpg" Width="60px" Height="40px"
                    AlternateText=" Upload Resume " runat="server" OnClick="img_Photo_Click" ToolTip="Click to Upload Resume"
                    TabIndex="2" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:Label ID="lblFileName" runat="server" Text="File Name" Visible="false" CssClass="DisplayFont lblBox"> </asp:Label>
                <br />
                <asp:HyperLink ID="hlresumdDownlaod" runat="server" Visible="false" Target="_Download">Download</asp:HyperLink>
            </div>
            <%--<div class="lblBox" runat = "server" Text="File Det" style=" width: 200px " Visible = "false" id="lblFileName">
               FileName
            </div>--%>
            <%-- <div id="div_FormHeader" runat="server" style="float:left" class= "divtxtBox">
            <table width="100%" >
                        <asp:ImageButton BorderColor="Navy" ImageUrl="~/Images/fotoshot.jpg" runat="server"
                            Width="40px" Height="37px" ID="btnFoto" OnClick="btnFoto_Click" 
                            ToolTip="Photo Capture" /> 
                           <asp:ImageButton BorderColor="Navy" ImageUrl="~/Images/imgIDCard.png" runat="server"
                            Width="50px" Height="50px" ID="btnID" OnClick="btnID_Click" ToolTip="Print ID Card" />
            </table>
        </div>--%>
            <%-- </div>
                <div class="divClear_10">
        </div>
        <div class="divRowContainer">
         <div class="lblBox" visible="false" style=" width: 150px" runat="server"
                id="lblCaptureImage">
                Photo
            </div>
            <div class="colspace" style="">
            </div>
            <div class="divtxtBox" style=" width: 223px" runat="server" id="imgCaptureImagediv"
                visible="false">
                <asp:Image ID="imgPatientFoto" runat="server" Style="height: 140px; width: 220px" />
            </div>
       </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer PopUpHeader" align="right">
            <div class="lblBox LNOrient" style="color: White">
                Personal Details
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="lblFirstName">
                First Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFirstName" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="50" runat="server" TabIndex="3" OnTextChanged="txtFirstName_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div12">
                Middle Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMName" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblLastName">
                Last Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLastName" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 320px">
                <asp:TextBox ID="txtAddress1" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="6"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 130px;" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 325px">
                <asp:TextBox ID="txtAddress2" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="lblCity">
                City
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCity" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="8"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblState">
                State
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtState" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="9"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblpincode">
                Pincode
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtpincode" CssClass="txtBox" MaxLength="20" runat="server" TabIndex="10"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="lblDob">
                DOB
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox runat="server" ID="txtDOB" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField txtBox"
                    TabIndex="11" MaxLength="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDOB" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDOB" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblEmail">
                E-Mail
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtEmail" CssClass="validate[custom[email]] txtBox" MaxLength="50"
                    runat="server" TabIndex="12"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblGender">
                Gender
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlGender" runat="server" TabIndex="13" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="lblNatinality">
                Nationality
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlNationality" runat="server" TabIndex="14" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <%-- <div class="colspace" style="">
                &nbsp</div>--%>
            <%-- <div class="lblBox" style=" width: 150px" id="Div13">
                E-Mail
            </div>
            <div class="divtxtBox" style=" width: 150px">
                <asp:TextBox ID="TextBox2" CssClass="validate[custom[email]] txtBox" MaxLength="50"
                    runat="server" TabIndex="15"></asp:TextBox>
            </div>--%>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblContactNo">
                Contact No
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtContactNo" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="50" runat="server" TabIndex="15"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                    TargetControlID="txtContactNo" ValidChars="+- " />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblMobile">
                Mobile
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMobile" CssClass="txtBox" MaxLength="20" runat="server" TabIndex="16"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="a3FilteredTextBoxExtender21" runat="server" FilterType="Numbers,custom"
                    TargetControlID="txtMobile" ValidChars="+- " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer PopUpHeader" align="right">
            <div class="lblBox LNOrient" style="color: White">
               Applicant Details
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblApplicantType">
                Applicant Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlApplicantType" runat="server" TabIndex="17" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblApplicatnJobType">
                Applicant Job Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlApplnJobType" runat="server" TabIndex="18" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div1">
                Last Contact Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLastContactDate" runat="server" TabIndex="19" CssClass="txtBox"
                    MaxLength="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtLastContactDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="Div4">
                Source Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlReferalType" runat="server" TabIndex="20" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblReferenceName">
                Source Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 475px">
                <asp:TextBox ID="txtReferenceName" CssClass="txtBox" MaxLength="50" runat="server"
                    TabIndex="21"></asp:TextBox>
            </div>
            <%-- <div class="lblBox" style=" width: 150px" id="Div14">
                Reference Name
            </div>
            <div class="divtxtBox" style=" width: 150px">
                <asp:TextBox ID="TextBox3" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="17"></asp:TextBox>
            </div>--%>
        </div>
        <%-- <div class="divClear_10">
        </div>
        <div class="divRowContainer">
              <div class="lblBox" style=" width: 200px" id="lblReferenceName">
                Reference Name
            </div>
            <div class="divtxtBox" style=" width: 150px">
                <asp:TextBox ID="txtReferenceName" CssClass="txtBox" MaxLength="50" runat="server"
                    TabIndex="18"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div15">
                Reference1</div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtRefName1" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="22"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div16">
                Company Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCompName1" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="23"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div17">
                Contact Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtContactNo1" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="24"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                    TargetControlID="txtContactNo1" ValidChars="+- " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div19">
                Reference2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtRefName2" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="25"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div20">
                Company Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCompName2" CssClass="txtBox" MaxLength="200" runat="server" TabIndex="26"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div21">
                Contact Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtContactNo2" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="27"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                    TargetControlID="txtContactNo2" ValidChars="+- " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer PopUpHeader" align="right">
            <div class="lblBox LNOrient" style="color: White">
              Work Details
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div9">
                Previous Company
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtPreviousCompany" CssClass="txtBox" MaxLength="100" runat="server"
                    TabIndex="27"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div5">
                Current/Last Position
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCurrentPosition" CssClass="txtBox" MaxLength="100" runat="server"
                    TabIndex="28"></asp:TextBox>
            </div>
        </div>
        <%-- <div class="colspace" style="">
                &nbsp</div>--%>
        <%--  <div class="lblBox" style=" width: 120px;" id="lblCurrentCTC">
                Current CTC
            </div>
            <div class="divtxtBox" style=" width: 130px">
                <asp:TextBox ID="txtCurrentCTC" CssClass="txtBox" MaxLength="100" runat="server"
                    TabIndex="29"></asp:TextBox>
            </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div3">
                Experience(In Years)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 130px">
                <asp:TextBox ID="txtExp" CssClass="validate[required] RequiredField  txtBox_N" MaxLength="2"
                    runat="server" TabIndex="30"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="Filt56eredTextffBoxExtende7r1" runat="server" FilterType="Numbers"
                    TargetControlID="txtExp" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblActive">
                Ex-Employee
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 130px">
                <asp:CheckBox ID="chkExEmp" runat="server" TabIndex="31" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer PopUpHeader" align="right">
            <div class="lblBox LNOrient" style="color: White">
                Skill Details 
            </div>
            <div style="float: right; padding-right: 10px" align="right">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblPrimarySkill">
                Primary Skill
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPrimarySkill" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="200" runat="server" TabIndex="32"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="lblSecondarySkill">
                Secondary Skill
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtSecondarySkill" CssClass="txtBox" MaxLength="200" runat="server"
                    TabIndex="33"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer PopUpHeader" align="right">
            <div class="lblBox LNOrient" style="color: White">
              Passport visa Details 
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="Div6">
                Passport Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPassportNum" CssClass="txtBox" MaxLength="50" runat="server"
                    TabIndex="34"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="Div7">
                Visa Detail
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtVisaDtl" CssClass="txtBox" MaxLength="100" runat="server" TabIndex="35"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div8">
                Visa Validity Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="dtpVisaVal" runat="server" TabIndex="36" CssClass="txtBox" MaxLength="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="dtpVisaVal"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px;" id="Div11">
                Status
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="37" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblRejectionReason">
                Rejection Reason
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtRejectionReason" CssClass="txtBox" MaxLength="500" TextMode="MultiLine"
                    runat="server" TabIndex="38"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtComments" CssClass="txtBox" MaxLength="500" TextMode="MultiLine"
                    runat="server" TabIndex="39"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divClear_10">
            </div>
            <div class="LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    DataKeyNames="APP_QUALI_ID,ENABLED_FLAG,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                    OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="True" TabIndex="40">
                    <Columns>
                        <asp:TemplateField HeaderText="Qualification Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtShortName" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox"
                                    Text='<%# Eval("APP_QUALI_SHORT_NAME") %>' TabIndex="48"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtShortName" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="41"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblShortName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("APP_QUALI_SHORT_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" Width="180px" MaxLength="200" runat="server" CssClass="RequiredField   txtBox"
                                    Text='<%# Eval("APP_QUALI_DESCRIPTION") %>' TabIndex="49"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtDescription" Width="180px" MaxLength="200" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="42"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("APP_QUALI_DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Issuing Authority">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtIssueAuth" Width="180px" MaxLength="200" runat="server" CssClass="RequiredField   txtBox"
                                    Text='<%# Eval("APP_ISSUING_AUTHORITY") %>' TabIndex="50"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtIssueAuth" Width="180px" MaxLength="200" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="43"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblIssuingAuth" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("APP_ISSUING_AUTHORITY") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Duration">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDuration" Width="180px" MaxLength="2" runat="server" CssClass="RequiredField   txtBox_N"
                                    Text='<%# Eval("APP_QUALI_DURATION") %>' TabIndex="51"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="Filt56eredTextffBoxExtende7r1" runat="server" FilterType="Numbers"
                                    TargetControlID="txtDuration" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtDuration" Width="180px" MaxLength="2" runat="server" CssClass="RequiredField   txtBox_N"
                                    TabIndex="44"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="Filt56eredTextffBoxExtende7r1" runat="server" FilterType="Numbers"
                                    TargetControlID="txtDuration" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDuration" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("APP_QUALI_DURATION") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="46" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="47" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="52" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="53" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="45" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer PopUpHeader" align="right">
                <div class="lblBox LNOrient" Style="color: White">
                    Previous Package
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="LNOrient">
                <asp:GridView ID="gvPrevPack" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    DataKeyNames="APP_PREV_CTC_ID,DELETED" OnRowCancelingEdit="gvPrevPack_RowCancelingEdit"
                    OnRowCommand="gvPrevPack_RowCommand" OnRowCreated="gvPrevPack_RowCreated" OnRowDataBound="gvPrevPack_RowDataBound"
                    OnRowDeleting="gvPrevPack_RowDeleting" OnRowEditing="gvPrevPack_RowEditing" OnRowUpdating="gvPrevPack_RowUpdating"
                    ShowFooter="True" TabIndex="40">
                    <Columns>
                        <asp:TemplateField HeaderText="Element Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEleName" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox"
                                    Text='<%# Eval("ELEMENT_NAME") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtEleName" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox"
                                    TabIndex="54"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblElementName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ELEMENT_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmt" Width="180px" MaxLength="13" runat="server" CssClass="RequiredField   txtBox_N "
                                    Text='<%# Eval("ELEMENT_AMOUNT") %>'></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers,custom"
                                    ValidChars=",." TargetControlID="txtAmt" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmt" Width="180px" MaxLength="13" runat="server" CssClass="RequiredField   txtBox_N"
                                    TabIndex="55"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterType="Numbers,custom"
                                    ValidChars=",." TargetControlID="txtAmt" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ELEMENT_AMOUNT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="57" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="58" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="59" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="60" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="56" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer " align="center">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="61"
                                OnClick="btnSave_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="62" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="63" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="64" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <asp:HiddenField ID="hfAttachments" runat="server" />
                <cc2:ModalPopupExtender ID="meUpload" runat="server" TargetControlID="hfAttachments"
                    BackgroundCssClass="ConfirmBackground" PopupControlID="pnlAttachments" CancelControlID="btnCloseUpload">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlAttachments" runat="server" Width="500px" Height="400px" BackColor="#99ccff"
                    HorizontalAlign="Left" BorderWidth="1">
                    <table width="500px">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnCloseUpload" runat="server" Text="Close" CssClass="DisplayFont button"
                                    OnClick="btnCloseUpload_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <iframe id="ifrmupload" style=" width: 486px;" name="uploadfrm" class="uploadfrm"
                                    frameborder="0" src="../UploadFile/PhotoUpload.aspx"></iframe>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div id="div2">
                <%-- <asp:HiddenField ID="hfPhoto" runat="server" />
                <cc2:ModalPopupExtender ID="photoupload" runat="server" TargetControlID="hfPhoto" 
                    BackgroundCssClass="ConfirmBackground" PopupControlID="pnlphoto" CancelControlID="btnClosePhoto">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlphoto" runat="server" Width="500px" Height="400px" BackColor="#99ccff" HorizontalAlign = "Left"
                    BorderWidth="1">
                    <table width="500px">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnClosePhoto" runat="server" Text="Close" CssClass="DisplayFont button"
                                    OnClick="btnCloseUpload_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <iframe id="ifrmphoto" style = "float:left; width: 486px;" name="photofrm" 
                                    class="uploadfrm"  frameborder="0" src="../UploadFile/PhotoUpload.aspx">
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>--%>
                <%--<asp:HiddenField ID="hifoto" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="hifoto"
                PopupControlID="pnlFoto" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlFoto" runat="server" Width="800px">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmFotoHeading">
                            <td>
                                <iframe id="frmFoto" runat="server" src="../PhotoCapturing.aspx" name="frmFoto" frameborder="0"
                                    style="width: 750px; height: 280px;"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnNo" Text="Close" OnClick="btnCloseImage_Click"
                                    Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
