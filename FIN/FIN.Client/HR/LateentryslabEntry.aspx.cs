﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LateentryslabEntry : PageBase
    {

        TM_EMP_LATE_ENTRY_SLAB tm_emp_late_entry_slab = new TM_EMP_LATE_ENTRY_SLAB();
        DataTable dtGridData = new DataTable();
        DepartmentDetails_BLL DepartmentDetails_BLL = new DepartmentDetails_BLL();

        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Clearance_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlEntryType, "ENTRY_TYPE");
            


        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                //   txtGradeName.Enabled = false;
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.Clearance_BLL.getLabEntrySlabDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    //ddlDepartment.Enabled = false;
                    using (IRepository<TM_EMP_LATE_ENTRY_SLAB> userCtx = new DataRepository<TM_EMP_LATE_ENTRY_SLAB>())
                    {
                        tm_emp_late_entry_slab = userCtx.Find(r =>
                            (r.TM_LATE_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    txtEntryName.Text = tm_emp_late_entry_slab.TM_LATE_DESC;
                    ddlEntryType.SelectedValue = tm_emp_late_entry_slab.TM_LATE_TYPE;
                    

                    // as per customer requirement job under category instead of grade


                    //   HR_GRADES  hR_GRADES = new HR_GRADES();
                    //using (IRepository<HR_GRADES> userCtx = new DataRepository<HR_GRADES>())
                    //{
                    //    hR_GRADES = userCtx.Find(r =>
                    //        (r.GRADE_ID == hR_JOBS.GRADE_ID.ToString())
                    //        ).SingleOrDefault();
                    //}
                    //EntityData = hR_JOBS;
                    //ddlCategory.SelectedValue = hR_GRADES.CATEGORY_ID;
                    //Fn_GradeName();
                    //ddlGradeCode.SelectedValue = hR_JOBS.GRADE_ID;
                    //Fillgradedesc();

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

               
                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    tm_emp_late_entry_slab = new TM_EMP_LATE_ENTRY_SLAB();
                    if (dtGridData.Rows[iLoop]["TM_LATE_ID"].ToString() != "0")
                    {
                        using (IRepository<TM_EMP_LATE_ENTRY_SLAB> userCtx = new DataRepository<TM_EMP_LATE_ENTRY_SLAB>())
                        {
                            tm_emp_late_entry_slab = userCtx.Find(r =>
                                (r.TM_LATE_ID == dtGridData.Rows[iLoop]["TM_LATE_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    tm_emp_late_entry_slab.TM_LATE_DESC = txtEntryName.Text;
                    tm_emp_late_entry_slab.TM_LATE_TYPE = ddlEntryType.SelectedValue;
                    tm_emp_late_entry_slab.TM_LATE_LOW_VALUE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["TM_LATE_LOW_VALUE"].ToString());
                    tm_emp_late_entry_slab.TM_LATE_HIGH_VALUE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["TM_LATE_HIGH_VALUE"].ToString());
                    tm_emp_late_entry_slab.TM_LATE_VALUE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["TM_LATE_VALUE"].ToString());
                    tm_emp_late_entry_slab.TM_LATE_REMARKS = dtGridData.Rows[iLoop]["TM_LATE_REMARKS"].ToString();
                    if (dtGridData.Rows[iLoop]["TM_EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        tm_emp_late_entry_slab.TM_EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["TM_EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["TM_EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        tm_emp_late_entry_slab.TM_EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["TM_EFFECTIVE_TO_DT"].ToString());
                    }
                    else
                    {
                        tm_emp_late_entry_slab.TM_EFFECTIVE_TO_DT = null;
                    }



                   // hR_CLEARANCE_HDR.DEPT_ID = ddlDepartment.SelectedValue;
                    //  as per customer requirement job under category instead of grade
                    //hR_JOBS.GRADE_ID = ddlGradeCode.SelectedValue;
                    tm_emp_late_entry_slab.TM_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        tm_emp_late_entry_slab.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        tm_emp_late_entry_slab.ENABLED_FLAG = FINAppConstants.N;
                    }




                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(tm_emp_late_entry_slab, "D"));
                    }
                    else
                    {





                        //ProReturn = FIN.DAL.HR.Jobs_DAL.GetSPFOR_ERR_MGR_JOBS(hR_JOBS.JOB_CODE, hR_JOBS.JOB_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("JOB", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["TM_LATE_ID"].ToString() != "0")
                        {
                            tm_emp_late_entry_slab.TM_LATE_ID = dtGridData.Rows[iLoop]["TM_LATE_ID"].ToString();
                            tm_emp_late_entry_slab.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tm_emp_late_entry_slab.TM_LATE_ID);

                            tm_emp_late_entry_slab.MODIFIED_BY = this.LoggedUserName;
                            tm_emp_late_entry_slab.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<TM_EMP_LATE_ENTRY_SLAB>(tm_emp_late_entry_slab, true);
                            savedBool = true;
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            tm_emp_late_entry_slab.TM_LATE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_128.ToString(), false, true);
                            tm_emp_late_entry_slab.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tm_emp_late_entry_slab.TM_LATE_ID);

                            //hR_CLEARANCE_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_CLEARANCE_SEQ);
                            tm_emp_late_entry_slab.CREATED_BY = this.LoggedUserName;
                            tm_emp_late_entry_slab.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<TM_EMP_LATE_ENTRY_SLAB>(tm_emp_late_entry_slab);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }

                }
                //  VMVServices.Web.Utils.SavedRecordId = "";

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ClearanceForm_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                //ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}

                //CheckBox chkact = tmpgvr.FindControl("chkact") as CheckBox;
                //DataTable dtchilcat = new DataTable();
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    hR_JOBS.JOB_ID = dtGridData.Rows[iLoop][FINColumnConstants.JOB_ID].ToString();
                //}
                //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Get_Childdataof_Job(hR_JOBS.JOB_ID)).Tables[0];
                //if (dtchilcat.Rows.Count > 0)
                //{
                //    chkact.Enabled = false;
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ClearanceForm_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Jobs ");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Late Entry Slab Details");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtLowValue = gvr.FindControl("txtLowValue") as TextBox;
            TextBox txtHighValue = gvr.FindControl("txtHighValue") as TextBox;
            TextBox txtValue = gvr.FindControl("txtValue") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["TM_LATE_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtLowValue;
            slControls[1] = txtHighValue;
            slControls[2] = txtValue;
            
            slControls[3] = dtpStartDate;
            slControls[4] = dtpStartDate;
            slControls[5] = dtpEndDate;
            

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Low_Value_P"] + " ~ " + Prop_File_Data["High_Value_P"] + " ~ " + Prop_File_Data["Value_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            // string strMessage = "Job Code ~ Description ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //string strCondition = "PARTICULAR='" + txtParticulars.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.Particulars;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}
            //Duplicate Validation Through Backend Package PKG_VALIDATIONS

            //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["CLEARANCE_ID"].ToString(), ddlDepartment.SelectedValue, txtParticulars.Text);
            //if (ProReturn != string.Empty)
            //{
            //    if (ProReturn != "0")
            //    {
            //        ErrorCollection.Add("CLEARANCE", ProReturn);
            //        if (ErrorCollection.Count > 0)
            //        {
            //            return drList;
            //        }
            //    }
            //}
            DataTable dtchildJob = new DataTable();
            dtchildJob = null;

            //if (gvData.EditIndex >= 0)
            //{
            //    hR_CLEARANCE_HDR.CLEARANCE_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CLEARANCE_ID].ToString();

            //    dtchildJob = null;
            //    dtchildJob = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Get_Childdataof_Job(hR_CLEARANCE_HDR.CLEARANCE_ID)).Tables[0];
            //    if (dtchildJob.Rows.Count > 0)
            //    {
            //        if (chkact.Checked)
            //        {


            //        }
            //        else
            //        {
            //            ErrorCollection.Add("chkflg", "Child record found please check the active flag");
            //            return drList;
            //        }
            //    }
            //}


            drList["TM_LATE_LOW_VALUE"] = txtLowValue.Text;
            drList["TM_LATE_HIGH_VALUE"] = txtHighValue.Text;
            drList["TM_LATE_VALUE"] = txtValue.Text;
            drList["TM_LATE_REMARKS"] = txtRemarks.Text;
            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["TM_EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["TM_EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["TM_EFFECTIVE_TO_DT"] = DBNull.Value;
            }


            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        //e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                        // e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    tm_emp_late_entry_slab.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<TM_EMP_LATE_ENTRY_SLAB>(tm_emp_late_entry_slab);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }



        private void Fillgradedesc()
        {
            //DataTable dtgradename = new DataTable();
            //dtgradename = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.GetGradename(ddlGradeCode.SelectedValue)).Tables[0];

            //if (dtgradename != null)
            //{
            //    if (dtgradename.Rows.Count > 0)
            //    {
            //        txtGradeName.Text = dtgradename.Rows[0]["GRADE_DESCRIPTION"].ToString();
            //    }
            //}
        }


        //private void LoadDeptParticular()
        //{
        //    dtGridData = FIN.BLL.HR.Clearance_BLL.getChildEntityDet4Dept(ddlDepartment.SelectedValue);
        //    BindGrid(dtGridData);
        //}

    }
}