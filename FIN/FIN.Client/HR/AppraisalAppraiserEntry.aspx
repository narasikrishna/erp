﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalAppraiserEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalAppraiserEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 550px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 254px">
                <asp:TextBox ID="txtDescription" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="250" runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 254px">
                <asp:DropDownList ID="ddlDepartment" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="2" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblJob">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 254px">
                <asp:DropDownList ID="ddlJob" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblAppraisalDefKPI">
                Appraisal Definition KPI
            </div>
            <div class="divtxtBox  LNOrient" style="width: 254px">
                <asp:DropDownList ID="ddlAppraisalDefKPI" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="4" OnSelectedIndexChanged="ddlAppraiserName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblAppraiserName">
                Appraiser Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 254px">
                <asp:DropDownList ID="ddlAppraiserName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="5" OnSelectedIndexChanged="ddlAppraiserName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px;" id="lblReviewerLvl">
                Reviewer Level
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlReviewerLvl" runat="server" TabIndex="6" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 220px" id="lblSelfAssesmentCompletionDate">
                Self Assesment Completion Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 153px">
                <asp:TextBox runat="server" ID="txtSelfAssesmentCompletionDate" TabIndex="6" CssClass="validate[custom[ReqDateDDMMYYY],,dateRange[g1]]  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtSelfAssesmentCompletionDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtSelfAssesmentCompletionDate" />
            </div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 220px" id="lblReviewCompletionDate">
            Review Completion Date
        </div>
        <div class="divtxtBox  LNOrient" style="width: 153px">
            <asp:TextBox runat="server" ID="txtReviewCompletionDate" TabIndex="7" CssClass="validate[custom[ReqDateDDMMYYY],,dateRange[g1]]  txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtReviewCompletionDate"
                OnClientDateSelectionChanged="checkDate" />
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                FilterType="Numbers,Custom" TargetControlID="txtReviewCompletionDate" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            Width="500px" DataKeyNames="RA_EMP_ID,EMP_NAME,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
            OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
            OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
            ShowFooter="True">
            <Columns>
                <asp:TemplateField HeaderText="Employee Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlEmployee" TabIndex="8" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlEmployee" TabIndex="8" runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblddlEmployeeName" CssClass="adminFormFieldHeading" runat="server"
                            Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                            ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" TabIndex="9" runat="server" AlternateText="Delete"
                            CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" TabIndex="9" runat="server" AlternateText="Update"
                            CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" TabIndex="9" runat="server" AlternateText="Cancel"
                            CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                            CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="10" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
