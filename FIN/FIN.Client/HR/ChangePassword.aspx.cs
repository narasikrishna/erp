﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL.AP;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.HR
{
    public partial class ChangePassword : PageBase
    {
        HR_EMP_PASSWORD_DTLS hR_EMP_PASSWORD_DTLS = new HR_EMP_PASSWORD_DTLS();
        SSM_USERS sSM_USERS = new SSM_USERS();
        string ProReturn = null;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    // EntryLoadHeader();
                    //  if (VMVServices.Web.Utils.Multilanguage)
                    //     AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                if (Request.QueryString.ToString().Contains("ReqType"))
                {
                    imgbtnSave.Visible = true;
                }
                //Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                //if (EntityData != null)
                //{
                //    hR_EMP_PASSWORD_DTLS = (HR_EMP_PASSWORD_DTLS)EntityData;
                //}

                DataTable dt_Det = DBMethod.ExecuteQuery("select user_code,user_password from ssm_users where user_code='" + Session[FINSessionConstants.UserName] + "'").Tables[0];

                string existingPassword = string.Empty;
                string Usr_cde = string.Empty;
                if (dt_Det.Rows.Count > 0)
                {
                    existingPassword = dt_Det.Rows[0]["user_password"].ToString();
                    Usr_cde = dt_Det.Rows[0]["user_code"].ToString();


                }

                hR_EMP_PASSWORD_DTLS = new HR_EMP_PASSWORD_DTLS();

                if (existingPassword.ToString() == EnCryptDecrypt.CryptorEngine.Encrypt(txtOldPswd.Text, true).ToString())
                {
                    if (txtNewPswd.Text.ToString() == txtConfirmPswd.Text.ToString())
                    {

                        if (dt_Det.Rows[0]["user_code"].ToString() != string.Empty)
                        {

                            using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                            {
                                sSM_USERS = userCtx.Find(r =>
                                    (r.USER_CODE == dt_Det.Rows[0]["user_code"].ToString())
                                    ).SingleOrDefault();
                            }

                            sSM_USERS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(txtConfirmPswd.Text, true);
                            sSM_USERS.MODIFIED_BY = this.LoggedUserName;
                            sSM_USERS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);

                        }
                        //hR_EMP_PASSWORD_DTLS.USER_PASSWORD = txtConfirmPswd.Text;
                        hR_EMP_PASSWORD_DTLS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(txtConfirmPswd.Text, true);
                        hR_EMP_PASSWORD_DTLS.USER_ID = this.LoggedUserName;

                        hR_EMP_PASSWORD_DTLS.EFFECTIVE_FROM = DateTime.Now;
                        hR_EMP_PASSWORD_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_EMP_PASSWORD_DTLS.EPD_ID = FINSP.GetSPFOR_SEQCode("HR_096".ToString(), false, true);

                        hR_EMP_PASSWORD_DTLS.CREATED_BY = this.LoggedUserName;
                        hR_EMP_PASSWORD_DTLS.CREATED_DATE = DateTime.Today;
                        //  hR_EMP_PASSWORD_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PASSWORD_DTLS.EPD_ID);
                        hR_EMP_PASSWORD_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    }

                }

                //else if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    sSM_USERS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(txtConfirmPswd.Text, true);
                //}


                ////if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                ////{
                ////    hR_EMP_PASSWORD_DTLS.MODIFIED_BY = this.LoggedUserName;
                ////    hR_EMP_PASSWORD_DTLS.MODIFIED_DATE = DateTime.Today;
                ////}
                ////else
                //{


                // }




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            //ComboFilling.fn_getUOMMaster(ref ddlUOMClass);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DBMethod.SaveEntity<HR_EMP_PASSWORD_DTLS>(hR_EMP_PASSWORD_DTLS);
                savedBool = true;

                if (savedBool)
                {
                    if (Request.QueryString.ToString().Contains("ReqType"))
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.SAVED);
                    }
                    else
                    {
                        Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
                    }
                    //ScriptManager.RegisterStartupScript(btnSave, typeof(Button), "closescreen", "window.open('','_parent',''); window.close();", true);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_PASSWORD_DTLS>(hR_EMP_PASSWORD_DTLS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void imgbtnSave_Click(object sender, ImageClickEventArgs e)
        {
            btnSave_Click(sender, e);
        }


    }
}