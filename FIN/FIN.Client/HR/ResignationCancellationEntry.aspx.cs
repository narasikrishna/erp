﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ResignationCancellationEntry : PageBase
    {
        HR_RESIG_CANCELLATION hR_RESIG_CANCELLATION = new HR_RESIG_CANCELLATION();
        ResignationCancellation_BLL ResignationCancellation_BLL = new ResignationCancellation_BLL();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                txtCancellationDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_RESIG_CANCELLATION> userCtx = new DataRepository<HR_RESIG_CANCELLATION>())
                    {
                        hR_RESIG_CANCELLATION = userCtx.Find(r =>
                            (r.RES_CANCEL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_RESIG_CANCELLATION;

                    txtCancellationNumber.Text = hR_RESIG_CANCELLATION.RES_CANCEL_ID;
                    ResignationCancellation_BLL.fn_GetResigReqNoCncel(ref ddlreqno);
                    ddlreqno.SelectedValue = hR_RESIG_CANCELLATION.RESIG_HDR_ID;
                    fillReqno();
                    //txtRequestNumber.text = hR_RESIG_CANCELLATION.RES_CANCEL_ID;  -- Column not available in table                  
                    //txtRequestDate.Text = DBMethod.ConvertDateToString(hR_RESIG_CANCELLATION.RES_CANCEL_DT.ToString());
                    //txtDepartmentName.Text = hR_RESIG_CANCELLATION.APP_FROM_DT;  -- Column not available in table
                    //txtEmployeeName.Text = hR_RESIG_CANCELLATION.; -- Column not available in table
                    //Lookup_BLL.GetLookUpValues(ref ddlReason, "RRR");
                    ddlReason.SelectedValue = hR_RESIG_CANCELLATION.RES_CANCEL_REASON;
                    txtRemarks.Text = hR_RESIG_CANCELLATION.RES_REMARKS;



                    if (hR_RESIG_CANCELLATION.RES_CANCEL_DT != null)
                    {
                        txtCancellationDate.Text = DBMethod.ConvertDateToString(hR_RESIG_CANCELLATION.RES_CANCEL_DT.ToString());
                    }

                    if (hR_RESIG_CANCELLATION.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                    //hR_RESIG_CANCELLATION.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    //hR_RESIG_CANCELLATION.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    //hR_RESIG_CANCELLATION.RESIG_ORG_ID = FINAppConstants.EnabledFlag;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_RESIG_CANCELLATION = (HR_RESIG_CANCELLATION)EntityData;
                }

                //ddlRequestNumber.SelectedValue = hR_RESIG_CANCELLATION.ATTRIBUTE1;  -- Column not available in table                  
                //txtRequestDate.Text = hR_RESIG_CANCELLATION.APP_DESC;  -- Column not available in table
                //txtDepartmentName.Text = hR_RESIG_CANCELLATION.APP_FROM_DT;  -- Column not available in table
                //txtEmployeeName.Text = hR_RESIG_CANCELLATION.; -- Column not available in table

                hR_RESIG_CANCELLATION.RES_CANCEL_ID = txtCancellationNumber.Text;
                hR_RESIG_CANCELLATION.RES_CANCEL_REASON = ddlReason.SelectedValue;
                hR_RESIG_CANCELLATION.RESIG_HDR_ID = ddlreqno.SelectedValue;
                hR_RESIG_CANCELLATION.RES_REMARKS = txtRemarks.Text;
                hR_RESIG_CANCELLATION.RESIG_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtRequestDate.Text != string.Empty)
                {
                    hR_RESIG_CANCELLATION.RES_CANCEL_DT = DBMethod.ConvertStringToDate(txtRequestDate.Text.ToString());
                }

                //hR_RESIG_CANCELLATION.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (chkActive.Checked == true)
                {
                    hR_RESIG_CANCELLATION.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_RESIG_CANCELLATION.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_RESIG_CANCELLATION.MODIFIED_BY = this.LoggedUserName;
                    hR_RESIG_CANCELLATION.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_RESIG_CANCELLATION.RES_CANCEL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_042.ToString(), false, true);
                    hR_RESIG_CANCELLATION.CREATED_BY = this.LoggedUserName;
                    hR_RESIG_CANCELLATION.CREATED_DATE = DateTime.Today;
                }
                hR_RESIG_CANCELLATION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_RESIG_CANCELLATION.RES_CANCEL_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            //Department_BLL.GetDepartmentName(ref ddlDepartment);
            //Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            Lookup_BLL.GetLookUpValues(ref ddlReason, "RC");
            ResignationCancellation_BLL.fn_GetResigReqNo(ref ddlreqno);

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.ResignationCancellation_DAL.GetSPFOR_DUPLICATE_CHECK(hR_RESIG_CANCELLATION.RESIG_HDR_ID, hR_RESIG_CANCELLATION.RES_CANCEL_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("RESIGNCAN", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_RESIG_CANCELLATION>(hR_RESIG_CANCELLATION);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_RESIG_CANCELLATION>(hR_RESIG_CANCELLATION, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_RESIG_CANCELLATION>(hR_RESIG_CANCELLATION);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtCancellationDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlreqno_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillReqno();
        }
        private void fillReqno()
        {
            DataTable dtreqdate = new DataTable();
            DataTable dtEmpData = new DataTable();

            dtreqdate = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationCancellation_DAL.GetReqDate(ddlreqno.SelectedValue)).Tables[0];
            dtEmpData = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationCancellation_DAL.GetEmpName(ddlreqno.SelectedValue)).Tables[0];
            if (dtreqdate != null)
            {
                txtRequestDate.Text = DBMethod.ConvertDateToString(dtreqdate.Rows[0]["RESIG_REQ_DT"].ToString());
            }
            if (dtEmpData != null)
            {
                if (dtEmpData.Rows.Count > 0)
                {
                    txtEmployeeName.Text = dtEmpData.Rows[0][0].ToString();
                }
            }
        }

    }
}