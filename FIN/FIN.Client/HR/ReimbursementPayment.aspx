﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ReimbursementPayment.aspx.cs" Inherits="FIN.Client.HR.ReimbursementPayment" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblDepartment">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
          <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 95px" id="lblProgram">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlProgram" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="true" TabIndex="2" 
                    onselectedindexchanged="ddlProgram_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblBudget">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlBudget" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlBudget_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 95px" id="lblAmount">
               
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAmount" Visible="false" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="CostDate">
            <div class="lblBox LNOrient" style="width: 100px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 96px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]]    txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="Div1">
                Total Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 130px">
                <asp:TextBox ID="txtInvoiceAmount" runat="server" TabIndex="26" MaxLength="13" 
                    CssClass="validate[required] RequiredField txtBox_N" Enabled="False"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
      <%--  <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 100px" id="Div2">
                Covered by KFAS
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtKFAS" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    runat="server" TabIndex="7"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtKFAS" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="COST_TYPE,COST_CURRENCY,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Reimbursement Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCostType" Width="150px" AutoPostBack="true" TabIndex="18"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCostType" Width="150px" AutoPostBack="true" TabIndex="7"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCostType" Width="130px" runat="server" Text='<%# Eval("COST_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
              <%--      <asp:TemplateField HeaderText="Item">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItem" TabIndex="19" Width="130px" MaxLength="500" runat="server"
                                CssClass="RequiredField txtBox" Text='<%# Eval("COST_ITEM_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItem" TabIndex="8" Width="130px" MaxLength="500" runat="server"
                                CssClass="RequiredField txtBox" Text='<%# Eval("COST_ITEM_DESC") %>'></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItem" Width="130px" runat="server" Text='<%# Eval("COST_ITEM_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" TabIndex="20" Width="130px" MaxLength="500" runat="server"
                                CssClass="RequiredField txtBox" Text='<%# Eval("COST_ITEM_REMARK") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" TabIndex="9" Width="130px" MaxLength="500" runat="server"
                                CssClass=" RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" Width="130px" runat="server" Text='<%# Eval("COST_ITEM_REMARK") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="21" Width="130px" MaxLength="10" runat="server"
                                AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("COST_QTY") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FiltedfsredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="10" Width="130px" MaxLength="10" runat="server"
                                AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("COST_QTY") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="qwetBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" Width="130px" runat="server" Text='<%# Eval("COST_QTY") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Price">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" TabIndex="22" Width="130px" MaxLength="13" runat="server"
                                AutoPostBack="True" CssClass="RequiredField txtBox_N" OnTextChanged="txtUnitPrice_TextChanged"
                                Text='<%# Eval("COST_UNIT_PRICE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="sewroxExtevbnder12" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtUnitPrice" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUnitPrice" TabIndex="11" Width="130px" MaxLength="13" runat="server"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("COST_UNIT_PRICE") %>' AutoPostBack="True"
                                OnTextChanged="txtUnitPrice_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="qwedTexggtBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtUnitPrice" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitPrice" Width="130px" Style="word-wrap: break-word;white-space: pre-wrap;"  runat="server" Text='<%# Eval("COST_UNIT_PRICE") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Currency">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCurrency" Width="150px" AutoPostBack="true" TabIndex="23"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCurrency" Width="150px" AutoPostBack="true" TabIndex="12"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCurrency" Width="130px" runat="server" Text='<%# Eval("CURRENCY_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField> --%>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="24" Width="130px" MaxLength="500" runat="server"
                                Enabled="false" CssClass="RequiredField txtBox_N" Text='<%# Eval("COST_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="13" Width="130px" MaxLength="500" runat="server"
                                Enabled="false" CssClass="RequiredField txtBox_N" Text='<%# Eval("COST_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" Style="word-wrap: break-word;white-space: pre-wrap;"  runat="server" Text='<%# Eval("COST_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="24" Width="130px" MaxLength="500" runat="server"
                                Enabled="false" CssClass="RequiredField txtBox_N" Text='<%# Eval("COST_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="13" Width="130px" MaxLength="500" runat="server"
                                Enabled="false" CssClass="RequiredField txtBox_N" Text='<%# Eval("COST_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" Style="word-wrap: break-word;white-space: pre-wrap;"  runat="server" Text='<%# Eval("COST_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                     <%-- <asp:TemplateField HeaderText="Covered by KFAS">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtKFAS" TabIndex="25" Width="130px" MaxLength="500" runat="server"
                                 CssClass="txtBox_N" Text='<%# Eval("COVERED_KFAS") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtKFAS" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtKFAS" TabIndex="14" Width="130px" MaxLength="500" runat="server"
                                 CssClass="txtBox_N" Text='<%# Eval("COVERED_KFAS") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtKFAS" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblKFAS" Width="130px" runat="server" Text='<%# Eval("COVERED_KFAS") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="16" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="17" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="26" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="27" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="15" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
          <div class="divClear_10">
        </div>
          <div class="divClear_10">
        </div>
      <%--  <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 810px; text-align: right" id="lblInvoiceAmount">
                Invoice Amount
            </div>
            <div class="divtxtBox" style="float: left; width: 130px">
                <asp:TextBox ID="txtInvoiceAmount" runat="server" TabIndex="26" MaxLength="13" 
                    CssClass="validate[required] RequiredField txtBox_N" Enabled="False"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <%--<div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
