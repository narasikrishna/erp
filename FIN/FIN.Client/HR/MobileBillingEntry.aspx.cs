﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class MobileBillingEntry : PageBase
    {
        HR_EMP_MOB_LIMIT_ENTRY_HDR hR_EMP_MOB_LIMIT_ENTRY_HDR = new HR_EMP_MOB_LIMIT_ENTRY_HDR();
        HR_EMP_MOB_LIMIT_ENTRY_DTL hR_EMP_MOB_LIMIT_ENTRY_DTL = new HR_EMP_MOB_LIMIT_ENTRY_DTL();



        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPeriod);
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);



        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetMobileBillingDtl(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_MOB_LIMIT_ENTRY_HDR> userCtx = new DataRepository<HR_EMP_MOB_LIMIT_ENTRY_HDR>())
                    {
                        hR_EMP_MOB_LIMIT_ENTRY_HDR = userCtx.Find(r =>
                            (r.MOB_LIMIT_ENTRY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_MOB_LIMIT_ENTRY_HDR;
                    ddlPeriod.SelectedValue = hR_EMP_MOB_LIMIT_ENTRY_HDR.PERIOD_ID;
                    ddlEmployeeName.SelectedValue = hR_EMP_MOB_LIMIT_ENTRY_HDR.EMP_ID;
                    FillDept();
                    //txtDepartment.Text = hR_EMP_MOB_LIMIT_ENTRY_HDR.DEPT_ID;
                    txtMobile.Text = hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_NUMBER;
                    txtLimit.Text = hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_ACTUAL_LIMIT.ToString();
                    txttotFacillimit.Text = hR_EMP_MOB_LIMIT_ENTRY_HDR.TOTAL_USAGE.ToString();
                    txttotRemainingAmt.Text = hR_EMP_MOB_LIMIT_ENTRY_HDR.ATTRIBUTE1;
                }

                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_MOB_LIMIT_ENTRY_HDR = (HR_EMP_MOB_LIMIT_ENTRY_HDR)EntityData;
                }
                hR_EMP_MOB_LIMIT_ENTRY_HDR.PERIOD_ID = ddlPeriod.SelectedValue;
                hR_EMP_MOB_LIMIT_ENTRY_HDR.EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                hR_EMP_MOB_LIMIT_ENTRY_HDR.DEPT_ID = hd_dept_id.Value;
                hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_NUMBER = hd_mob_no.Value;
                hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_ACTUAL_LIMIT = CommonUtils.ConvertStringToDecimal(hd_mob_limit.Value);
                hR_EMP_MOB_LIMIT_ENTRY_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_EMP_MOB_LIMIT_ENTRY_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_EMP_MOB_LIMIT_ENTRY_HDR.TOTAL_USAGE = CommonUtils.ConvertStringToDecimal(txttotFacillimit.Text);
                hR_EMP_MOB_LIMIT_ENTRY_HDR.ATTRIBUTE1 = txttotRemainingAmt.Text;
                //hR_EMP_MOB_LIMIT_ENTRY_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_MOB_LIMIT_ENTRY_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_MOB_LIMIT_ENTRY_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_LIMIT_ENTRY_ID = FINSP.GetSPFOR_SEQCode("PR_022_M".ToString(), false, true);

                    hR_EMP_MOB_LIMIT_ENTRY_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_MOB_LIMIT_ENTRY_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_EMP_MOB_LIMIT_ENTRY_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_LIMIT_ENTRY_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_EMP_MOB_LIMIT_ENTRY_DTL hR_EMP_MOB_LIMIT_ENTRY_DTL = new HR_EMP_MOB_LIMIT_ENTRY_DTL();

                        if (gvData.DataKeys[iLoop].Values["MOB_LIMIT_ENTRY_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["MOB_LIMIT_ENTRY_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_EMP_MOB_LIMIT_ENTRY_DTL> userCtx = new DataRepository<HR_EMP_MOB_LIMIT_ENTRY_DTL>())
                            {
                                hR_EMP_MOB_LIMIT_ENTRY_DTL = userCtx.Find(r =>
                                    (r.MOB_LIMIT_ENTRY_DTL_ID == gvData.DataKeys[iLoop].Values["MOB_LIMIT_ENTRY_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtFacilityLimit = (TextBox)gvData.Rows[iLoop].FindControl("txtFacilityLimit");
                        TextBox txtRequiredLimit = (TextBox)gvData.Rows[iLoop].FindControl("txtRequiredLimit");
                        CheckBox CheckBoxSelect = (CheckBox)gvData.Rows[iLoop].FindControl("CheckBoxSelect");
                        TextBox txtRemainingvalue = (TextBox)gvData.Rows[iLoop].FindControl("txtRemainingvalue");

                        hR_EMP_MOB_LIMIT_ENTRY_DTL.VALUE_KEY_ID = gvData.DataKeys[iLoop].Values["CODE"].ToString();
                        //hR_EMP_MOB_LIMIT_ENTRY_DTL.ELIGIBILITY_AMT = int.Parse(gvData.DataKeys[iLoop].Values["ELIGIBILITY_AMT"].ToString());
                        if (CheckBoxSelect.Checked == true)
                        {
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.ELIGIBILITY = FINAppConstants.Y;
                        }
                        else
                        {
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.ELIGIBILITY = FINAppConstants.N;
                        }
                        if (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text) < CommonUtils.ConvertStringToDecimal(txtRequiredLimit.Text))
                        {
                            ErrorCollection.Add("checkval", "Facility Limit must be greater than Required Limit");

                        }
                        hR_EMP_MOB_LIMIT_ENTRY_DTL.VALUE_AMOUNT = CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text);


                        hR_EMP_MOB_LIMIT_ENTRY_DTL.ELIGIBILITY_AMT = CommonUtils.ConvertStringToDecimal(txtRequiredLimit.Text);
                        hR_EMP_MOB_LIMIT_ENTRY_DTL.REMAINING_AMT = CommonUtils.ConvertStringToDecimal(txtRemainingvalue.Text);

                        hR_EMP_MOB_LIMIT_ENTRY_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_EMP_MOB_LIMIT_ENTRY_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        hR_EMP_MOB_LIMIT_ENTRY_DTL.MOB_LIMIT_ENTRY_ID = hR_EMP_MOB_LIMIT_ENTRY_HDR.MOB_LIMIT_ENTRY_ID;

                        //hR_EMP_MOB_LIMIT_ENTRY_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        //hR_EMP_MOB_LIMIT_ENTRY_DTL. = VMVServices.Web.Utils.OrganizationID;


                        if (gvData.DataKeys[iLoop].Values["MOB_LIMIT_ENTRY_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["MOB_LIMIT_ENTRY_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.MOB_LIMIT_ENTRY_DTL_ID = dtGridData.Rows[iLoop]["MOB_LIMIT_ENTRY_DTL_ID"].ToString();
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_MOB_LIMIT_ENTRY_DTL, "U"));

                        }
                        else
                        {

                            hR_EMP_MOB_LIMIT_ENTRY_DTL.MOB_LIMIT_ENTRY_DTL_ID = FINSP.GetSPFOR_SEQCode("PR_022_D".ToString(), false, true);
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_MOB_LIMIT_ENTRY_DTL.CREATED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_MOB_LIMIT_ENTRY_DTL, "A"));
                        }


                    }


                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_EMP_MOB_LIMIT_ENTRY_HDR, HR_EMP_MOB_LIMIT_ENTRY_DTL>(hR_EMP_MOB_LIMIT_ENTRY_HDR, tmpChildEntity, hR_EMP_MOB_LIMIT_ENTRY_DTL);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_EMP_MOB_LIMIT_ENTRY_HDR, HR_EMP_MOB_LIMIT_ENTRY_DTL>(hR_EMP_MOB_LIMIT_ENTRY_HDR, tmpChildEntity, hR_EMP_MOB_LIMIT_ENTRY_DTL, true);
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Competency Link");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["ELIGIBILITY"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_EMP_MOB_LIMIT_ENTRY_HDR>(hR_EMP_MOB_LIMIT_ENTRY_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("cML_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        private void filltotrat(GridViewRow gvr)
        {
            try
            {
                DataTable dtTotrat = new DataTable();

                // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtHrRat = gvr.FindControl("txtHrRat") as TextBox;
                TextBox txtchairmanrat = gvr.FindControl("txtchairmanrat") as TextBox;
                TextBox txtTotRat = gvr.FindControl("txtTotRat") as TextBox;

                dtTotrat = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.GetTotalRating(decimal.Parse(gvData.DataKeys[gvr.RowIndex].Values["FINAL_RATING"].ToString()), decimal.Parse(txtHrRat.Text), decimal.Parse(txtchairmanrat.Text))).Tables[0];

                txtTotRat.Text = dtTotrat.Rows[0]["TOT_RAT"].ToString();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("val_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtchairmanrat_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                // filltotrat(sender);
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                filltotrat(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
        }

        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDept();
            //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetFacilityDtls(ddlEmployeeName.SelectedValue)).Tables[0];
            //BindGrid(dtGridData);
            BindGrid();
        }

        private void FillDept()
        {

            DataTable dtdept = new DataTable();

            dtdept = DBMethod.ExecuteQuery(Employee_DAL.GetDepartmentName(ddlEmployeeName.SelectedValue)).Tables[0];
            if (dtdept.Rows.Count > 0)
            {
                txtDepartment.Text = dtdept.Rows[0]["DEPT_NAME"].ToString();
                hd_dept_id.Value = dtdept.Rows[0]["DEPT_ID"].ToString();
                txtLimit.Text = dtdept.Rows[0]["mob_limit"].ToString();
                hd_mob_limit.Value = dtdept.Rows[0]["mob_limit"].ToString();
                txtMobile.Text = dtdept.Rows[0]["mob_number"].ToString();
                hd_mob_no.Value = dtdept.Rows[0]["mob_number"].ToString();
            }

        }
        private void BindGrid()
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetFacilityDtls(ddlEmployeeName.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }
        protected void txtFacilityLimit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtFacilityLimit = gvr.FindControl("txtFacilityLimit") as TextBox;
                TextBox txtRequiredLimit = gvr.FindControl("txtRequiredLimit") as TextBox;
                TextBox txtRemainingvalue = gvr.FindControl("txtRemainingvalue") as TextBox;

                fn_FacilityLimittxtChanged(sender);
                fn_RemainingvaluetxtChanged(sender);
                if (txtFacilityLimit != null && txtRequiredLimit != null)
                {
                    txtRemainingvalue.Text = (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text) - CommonUtils.ConvertStringToDecimal(txtRequiredLimit.Text)).ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtRemainingvalue_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                fn_RemainingvaluetxtChanged(sender);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtRequiredLimit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtFacilityLimit = gvr.FindControl("txtFacilityLimit") as TextBox;
                TextBox txtRequiredLimit = gvr.FindControl("txtRequiredLimit") as TextBox;
                TextBox txtRemainingvalue = gvr.FindControl("txtRemainingvalue") as TextBox;
               

                if (txtFacilityLimit != null && txtRequiredLimit != null)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text) < CommonUtils.ConvertStringToDecimal(txtRequiredLimit.Text))
                    {
                        ErrorCollection.Add("checkval", "Facility Limit must be greater than Required Limit");
                        
                    }
                    txtRemainingvalue.Text = (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text) - CommonUtils.ConvertStringToDecimal(txtRequiredLimit.Text)).ToString();
                }

                fn_RemainingvaluetxtChanged(sender);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_RemainingvaluetxtChanged(object sender)
        {
            try
            {
                ErrorCollection.Clear();

                //TextBox txtFacilityLimit = new TextBox();
                //txtFacilityLimit.ID = "txtFacilityLimit";

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtRemainingvalue = gvr.FindControl("txtRemainingvalue") as TextBox;


                if (txtRemainingvalue != null)
                {
                    if (txtRemainingvalue.Text.Trim() != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToDecimal(txtRemainingvalue.Text.ToString()) > 0)
                        {
                            txttotRemainingAmt.Text = (CommonUtils.ConvertStringToDecimal(txtRemainingvalue.Text) + CommonUtils.ConvertStringToDecimal(txttotRemainingAmt.Text)).ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fn_FacilityLimittxtChanged(object sender)
        {
            try
            {
                ErrorCollection.Clear();

                //TextBox txtFacilityLimit = new TextBox();
                //txtFacilityLimit.ID = "txtFacilityLimit";

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtFacilityLimit = gvr.FindControl("txtFacilityLimit") as TextBox;


                if (txtFacilityLimit != null)
                {
                    if (txtFacilityLimit.Text.Trim() != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text.ToString()) > 0)
                        {
                            txttotFacillimit.Text = (CommonUtils.ConvertStringToDecimal(txtFacilityLimit.Text) + CommonUtils.ConvertStringToDecimal(txttotFacillimit.Text)).ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}