﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeAssignmentEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeAssignmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 145px" id="lblName">
                Employee Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="RequiredField EntryFont ddlStype"
                     AutoPostBack="True" TabIndex="1" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px ; display : none" id="lblEmp">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtEmpName" MaxLength="100" runat="server" Visible = "false" CssClass="RequiredField txtBox" Enabled="false"
                     TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="assignment_id,assignment_grade,assignment_job,assignment_position,assignment_reporting_to,DELETED,LOCATION_ID,CATEGORY_ID"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    
                    <asp:TemplateField HeaderText="Location">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLocation" Width="200px" TabIndex="3" runat="server"  CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLocation" Width="200px" TabIndex="3" runat="server"  CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" runat="server" TabIndex="10" Text='<%# Eval("LOCATION_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCategory" Width="200px" TabIndex="4" runat="server"  CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCategory" Width="200px" TabIndex="4" runat="server"  CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" TabIndex="4"  Text='<%# Eval("CATEGORY_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Job">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlJob" Width="200px" TabIndex="5" runat="server" OnSelectedIndexChanged="ddljob_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlJob" Width="200px" TabIndex="5" runat="server" OnSelectedIndexChanged="ddljob_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblJob" runat="server" TabIndex="5" Text='<%# Eval("Job_name") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Position">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlPosition" Width="200px" TabIndex="6" runat="server"  CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlPosition" Width="200px" TabIndex="6" runat="server"  CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPosition" runat="server" TabIndex="6" Text='<%# Eval("Position_name") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGrade" Width="200px" TabIndex="7" runat="server" 
                                AutoPostBack="true" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGrade" Width="200px" TabIndex="7" runat="server" 
                                AutoPostBack="true" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGrade" runat="server" TabIndex="7" Text='<%# Eval("Grade_name") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Reporting To">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlReporting" Width="200px" TabIndex="8" runat="server"  CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlReporting" Width="200px" TabIndex="8" runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReporting" runat="server" TabIndex="8" Text='<%# Eval("reporting_name") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkActive" TabIndex="9"  runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkActive" TabIndex="9" runat="server" Checked="true"  />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkActive" TabIndex="9" Enabled="false"  runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtFromDate" Text='<%# Eval("assignment_from_dt","{0:dd/MM/yyyy}") %>' Width="150px"
                                CssClass="validate[,custom[ReqDateDDMMYYY],,]  RequiredField txtBox"
                                TabIndex="10"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender24"
                                TargetControlID="txtFromDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender543" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  RequiredField txtBox"  Width="150px"
                                TabIndex="10"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtende4r4"
                                TargetControlID="txtFromDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtende4r54" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFromDate" runat="server" TabIndex="10" Text='<%# Eval("assignment_from_dt","{0:dd/MM/yyyy}") %>'  Width="150px"></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtToDate" Text='<%# Eval("assignment_to_dt","{0:dd/MM/yyyy}") %>'
                                CssClass="validate[,custom[ReqDateDDMMYYY],,]   txtBox"  Width="150px"
                                TabIndex="11"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtende6r4"
                                TargetControlID="txtToDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender554" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtToDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="txtToDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]   txtBox"  Width="150px"
                                TabIndex="11"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExten6der4"
                                TargetControlID="txtToDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtend6er54" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtToDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" TabIndex="11" Text='<%# Eval("assignment_to_dt","{0:dd/MM/yyyy}") %>'  Width="150px"></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="12" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="13"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="14" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="15"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="16" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
            <asp:HiddenField ID="hF_COM_LINE_ID" runat="server" />
            <asp:HiddenField ID="HF_COM_LINK_DTL_ID" runat="server" />
        </div>
       <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" 
                            CssClass="btn" TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                            TabIndex="5" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
