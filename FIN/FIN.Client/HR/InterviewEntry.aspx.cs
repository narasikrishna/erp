﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

using System.Data;
namespace FIN.Client.HR
{
    public partial class InterivewEntry : PageBase
    {

        HR_INTERVIEWS_HDR hR_INTERVIEWS_HDR = new HR_INTERVIEWS_HDR();
        HR_INTERVIEWS_DTL hR_INTERVIEWS_DTL = new HR_INTERVIEWS_DTL();
        string ProReturn = null;
        Boolean savedBool;


        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Interview_BLL.fn_GetVacancyNM(ref ddlVacancy);
            //Interview_BLL.fn_GetApplicantNM(ref ddlApplicant);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session["AppList"] = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.Interview_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_INTERVIEWS_HDR> userCtx = new DataRepository<HR_INTERVIEWS_HDR>())
                    {
                        hR_INTERVIEWS_HDR = userCtx.Find(r =>
                            (r.INT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_INTERVIEWS_HDR;


                    txtdesc.Text = hR_INTERVIEWS_HDR.INT_DESC;
                    ddlVacancy.SelectedValue = hR_INTERVIEWS_HDR.VAC_ID;
                    // ddlApplicant.SelectedValue = hR_INTERVIEWS_HDR.APP_ID;

                    if (hR_INTERVIEWS_HDR.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }


                    if (hR_INTERVIEWS_HDR.ATTRIBUTE1 == "1")
                    {
                        chkEmail.Checked = true;
                    }
                    else
                    {
                        chkEmail.Checked = false;
                    }

                    LoadApplicantList();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {

                savedBool = false;
                DataTable dt_AppList = (DataTable)Session["AppList"];
                for (int kLoop = 0; kLoop < dt_AppList.Rows.Count; kLoop++)
                {
                    ErrorCollection.Clear();
                    if (dt_AppList.Rows[kLoop]["SEL_APP"].ToString() == "TRUE")
                    {
                        HR_INTERVIEWS_HDR hR_INTERVIEWS_HDR = new HR_INTERVIEWS_HDR();
                        if (dt_AppList.Rows[kLoop]["INT_ID"].ToString() != "0")
                        {

                            using (IRepository<HR_INTERVIEWS_HDR> userCtx = new DataRepository<HR_INTERVIEWS_HDR>())
                            {
                                hR_INTERVIEWS_HDR = userCtx.Find(r =>
                                    (r.INT_ID == dt_AppList.Rows[kLoop]["INT_ID"].ToString())
                                    ).SingleOrDefault();
                            }

                        }
                        //ddlApplicant.SelectedValue = dt_AppList.Rows[kLoop]["APP_ID"].ToString();
                        hR_INTERVIEWS_HDR.INT_DESC = txtdesc.Text;
                        hR_INTERVIEWS_HDR.VAC_ID = ddlVacancy.SelectedValue;
                        hR_INTERVIEWS_HDR.APP_ID = dt_AppList.Rows[kLoop]["APP_ID"].ToString();

                        hR_INTERVIEWS_HDR.INT_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        if (chkActive.Checked == true)
                        {
                            hR_INTERVIEWS_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        }
                        else
                        {
                            hR_INTERVIEWS_HDR.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                        }

                        if (chkEmail.Checked == true)
                        {
                            hR_INTERVIEWS_HDR.ATTRIBUTE1 = FINAppConstants.EnabledFlag;
                        }
                        else
                        {
                            hR_INTERVIEWS_HDR.ATTRIBUTE1 = FINAppConstants.DisabledFlag;
                        }


                        if (dt_AppList.Rows[kLoop]["INT_ID"].ToString() != "0")
                        {
                            hR_INTERVIEWS_HDR.MODIFIED_BY = this.LoggedUserName;
                            hR_INTERVIEWS_HDR.MODIFIED_DATE = DateTime.Today;

                        }
                        else
                        {
                            hR_INTERVIEWS_HDR.INT_ID = FINSP.GetSPFOR_SEQCode("HR_049_M".ToString(), false, true);
                            //hR_INTERVIEWS_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_INTERVIEWS_HDR_SEQ);
                            hR_INTERVIEWS_HDR.CREATED_BY = this.LoggedUserName;
                            hR_INTERVIEWS_HDR.CREATED_DATE = DateTime.Today;

                        }
                        hR_INTERVIEWS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INTERVIEWS_HDR.INT_ID);

                        if (Session[FINSessionConstants.GridData] != null)
                        {
                            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                        }


                        var tmpChildEntity = new List<Tuple<object, string>>();

                        for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                        {
                            hR_INTERVIEWS_DTL = new HR_INTERVIEWS_DTL();
                            if (dt_AppList.Rows[kLoop]["INT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString() != string.Empty)
                            {
                                using (IRepository<HR_INTERVIEWS_DTL> userCtx = new DataRepository<HR_INTERVIEWS_DTL>())
                                {
                                    hR_INTERVIEWS_DTL = userCtx.Find(r =>
                                        (r.INT_DTL_ID == dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString())
                                        ).SingleOrDefault();
                                }
                            }

                            //hR_INTERVIEWS_DTL.COM_LINE_NUM = (iLoop + 1);
                            hR_INTERVIEWS_DTL.DEPT_ID = dtGridData.Rows[iLoop]["DEPT_ID"].ToString();
                            hR_INTERVIEWS_DTL.DEPT_DESIG_ID = dtGridData.Rows[iLoop]["DEPT_DESIG_ID"].ToString();
                            hR_INTERVIEWS_DTL.EMP_ID = dtGridData.Rows[iLoop]["EMP_ID"].ToString();
                            hR_INTERVIEWS_DTL.INT_LOCATION = dtGridData.Rows[iLoop]["LOC_ID"].ToString();
                            hR_INTERVIEWS_DTL.INT_MODE = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                            //hR_INTERVIEWS_DTL.VAC_CRIT_ID = dtGridData.Rows[iLoop]["VAC_CRIT_ID"].ToString();
                            //hR_INTERVIEWS_DTL.INT_LEVEL_SCORED = int.Parse(dtGridData.Rows[iLoop]["INT_LEVEL_SCORED"].ToString());
                            //hR_INTERVIEWS_DTL.INT_COMMENTS = dtGridData.Rows[iLoop]["INT_COMMENTS"].ToString();

                            hR_INTERVIEWS_DTL.ATTRIBUTE1 = dtGridData.Rows[iLoop]["time_hr"].ToString();
                            hR_INTERVIEWS_DTL.ATTRIBUTE2 = dtGridData.Rows[iLoop]["time_min"].ToString();
                            hR_INTERVIEWS_DTL.ATTRIBUTE3 = dtGridData.Rows[iLoop]["time_session"].ToString();


                            if (dtGridData.Rows[iLoop]["INT_DATE"] != DBNull.Value)
                            {
                                hR_INTERVIEWS_DTL.INT_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["INT_DATE"].ToString());
                            }

                            hR_INTERVIEWS_DTL.INT_ID = hR_INTERVIEWS_HDR.INT_ID;

                            hR_INTERVIEWS_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                            hR_INTERVIEWS_DTL.ENABLED_FLAG = FINAppConstants.Y;



                            if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                            {

                                tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL, "D"));
                            }
                            else
                            {

                                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                                //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_INTERVIEWS_DTL.COM_LEVEL_DESC, hR_INTERVIEWS_DTL.COM_LINE_ID);

                                //if (ProReturn != string.Empty)
                                //{
                                //    if (ProReturn != "0")
                                //    {
                                //        ErrorCollection.Add("COMPETENCY", ProReturn);
                                //        if (ErrorCollection.Count > 0)
                                //        {
                                //            return;
                                //        }
                                //    }
                                //}

                                if (dt_AppList.Rows[kLoop]["INT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString() != string.Empty)
                                {
                                    hR_INTERVIEWS_DTL.INT_DTL_ID = dtGridData.Rows[iLoop]["INT_DTL_ID"].ToString();
                                    hR_INTERVIEWS_DTL.MODIFIED_BY = this.LoggedUserName;
                                    hR_INTERVIEWS_DTL.MODIFIED_DATE = DateTime.Today;

                                    tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL, "U"));


                                }
                                else
                                {

                                    hR_INTERVIEWS_DTL.INT_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_049_D".ToString(), false, true);
                                    hR_INTERVIEWS_DTL.CREATED_BY = this.LoggedUserName;
                                    hR_INTERVIEWS_DTL.CREATED_DATE = DateTime.Today;
                                    //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                                    tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL, "A"));

                                }
                            }

                        }
                        //savedBool = true;

                        if (dt_AppList.Rows[kLoop]["INT_ID"].ToString() != "0")
                        {
                            FIN.BLL.HR.Interview_BLL.SavePCEntity<HR_INTERVIEWS_HDR, HR_INTERVIEWS_DTL>(hR_INTERVIEWS_HDR, tmpChildEntity, hR_INTERVIEWS_DTL, true);
                            savedBool = true;
                        }
                        else
                        {
                            FIN.BLL.HR.Interview_BLL.SavePCEntity<HR_INTERVIEWS_HDR, HR_INTERVIEWS_DTL>(hR_INTERVIEWS_HDR, tmpChildEntity, hR_INTERVIEWS_DTL);
                            savedBool = true;
                        }

                        HR_APPLICANTS hR_APPLICANTS = new HR_APPLICANTS();

                        using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
                        {
                            hR_APPLICANTS = userCtx.Find(r =>
                                (r.APP_ID == hR_INTERVIEWS_HDR.APP_ID)
                                ).SingleOrDefault();
                        }

                        hR_APPLICANTS.STATUS = "Scheduled";
                        DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS, true);

                        if (hR_INTERVIEWS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                        {
                            if (chkEmail.Checked == true)
                            {
                                FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.Interview_Schedule_Applicant_ALERT);
                            }
                            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                            {
                                if (VMVServices.Web.Utils.IsAlert == "1")
                                {
                                    FINSQL.UpdateAlertUserLevel();
                                }
                            }
                        }
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlDept = tmpgvr.FindControl("ddlDept") as DropDownList;
                DropDownList ddlDesig = tmpgvr.FindControl("ddlDesig") as DropDownList;
                DropDownList ddlEmployee = tmpgvr.FindControl("ddlEmployee") as DropDownList;
                DropDownList ddlLocation = tmpgvr.FindControl("ddlLocation") as DropDownList;
                DropDownList ddlMode = tmpgvr.FindControl("ddlMode") as DropDownList;

                DropDownList ddlStartTimeHr = tmpgvr.FindControl("ddlStartTimeHr") as DropDownList;
                DropDownList ddlStartTimeMin = tmpgvr.FindControl("ddlStartTimeMin") as DropDownList;
                DropDownList ddlStartTimeAMPM = tmpgvr.FindControl("ddlStartTimeAMPM") as DropDownList;
                //DropDownList ddlCreteria = tmpgvr.FindControl("ddlCreteria") as DropDownList;

                FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDept);
                FIN.BLL.HR.Interview_BLL.getLoc(ref ddlLocation);
                //FIN.BLL.HR.Interview_BLL.fn_GetVacCriteria(ref ddlCreteria);

                Lookup_BLL.GetLookUpValues(ref ddlMode, "INTV_MODE");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlDept.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["DEPT_ID"].ToString();
                    fillDesignation(tmpgvr);
                    ddlDesig.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["DEPT_DESIG_ID"].ToString();
                    fillEmp(tmpgvr);
                    ddlEmployee.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["EMP_ID"].ToString();
                    ddlLocation.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOC_ID"].ToString();
                    ddlMode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOOKUP_ID"].ToString();
                    //ddlCreteria.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["VAC_CRIT_ID"].ToString();
                    ddlStartTimeHr.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["time_hr"].ToString();
                    ddlStartTimeMin.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["time_min"].ToString();
                    ddlStartTimeAMPM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["time_session"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INT_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Interview Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            DropDownList ddlDept = gvr.FindControl("ddlDept") as DropDownList;
            DropDownList ddlDesig = gvr.FindControl("ddlDesig") as DropDownList;
            DropDownList ddlEmployee = gvr.FindControl("ddlEmployee") as DropDownList;
            DropDownList ddlLocation = gvr.FindControl("ddlLocation") as DropDownList;
            DropDownList ddlMode = gvr.FindControl("ddlMode") as DropDownList;
            //DropDownList ddlCreteria = gvr.FindControl("ddlCreteria") as DropDownList;
            //TextBox txtLevelscored = gvr.FindControl("txtLevelscored") as TextBox;
            //TextBox txtcomments = gvr.FindControl("txtcomments") as TextBox;


            DropDownList ddlStartTimeHr = gvr.FindControl("ddlStartTimeHr") as DropDownList;
            DropDownList ddlStartTimeMin = gvr.FindControl("ddlStartTimeMin") as DropDownList;
            DropDownList ddlStartTimeAMPM = gvr.FindControl("ddlStartTimeAMPM") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INT_DTL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlDept;
            slControls[1] = ddlDesig;
            slControls[2] = ddlEmployee;
            slControls[3] = ddlLocation;
            slControls[4] = ddlMode;
            slControls[5] = dtpStartDate;
            slControls[6] = ddlStartTimeHr;
            slControls[7] = ddlStartTimeMin;
            slControls[8] = ddlStartTimeAMPM;



            slControls[5] = dtpStartDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~DropDownList~DropDownList~TextBox~DropDownList~DropDownList~DropDownList";
            string strMessage = Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Designation_P"] + " ~ " + Prop_File_Data["Employee_P"] + " ~ " + Prop_File_Data["Location_P"] + " ~ " + Prop_File_Data["Mode_P"] + " ~ " + Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Time_Hr_P"] + " ~ " + Prop_File_Data["Min_P"] + " ~ " + Prop_File_Data["Session_P"] + "";
            //string strMessage = "Department ~ Designation ~ Employee ~ Location ~ Mode ~ Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            int startHr1 = 0;

            if (ddlStartTimeHr.SelectedValue != string.Empty)
            {
                startHr1 = Convert.ToInt16(ddlStartTimeHr.SelectedValue.ToString());
                if (ddlStartTimeAMPM.SelectedValue == "PM" && startHr1 != 12)
                    startHr1 = startHr1 + 12;
            }

            DateTime dtStartTime1 = DateTime.MinValue;
            DateTime dtEndTime1 = DateTime.MinValue;

            if (ddlStartTimeHr.SelectedValue != string.Empty)
            {
                if (ddlStartTimeMin.SelectedValue != string.Empty)
                {
                    dtStartTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr1, Convert.ToInt16(ddlStartTimeMin.SelectedValue), 0);

                }
            }


            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["INT_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }


            if (ddlDept.SelectedItem != null)
            {
                drList["DEPT_ID"] = ddlDept.SelectedItem.Value;
                drList["DEPT_NAME"] = ddlDept.SelectedItem.Text;
            }
            if (ddlDesig.SelectedItem != null)
            {
                drList["DEPT_DESIG_ID"] = ddlDesig.SelectedItem.Value;
                drList["DESIG_NAME"] = ddlDesig.SelectedItem.Text;
            }
            if (ddlEmployee.SelectedItem != null)
            {
                drList["EMP_ID"] = ddlEmployee.SelectedItem.Value;
                drList["EMP_FIRST_NAME"] = ddlEmployee.SelectedItem.Text;
            }
            if (ddlLocation.SelectedItem != null)
            {
                drList["LOC_ID"] = ddlLocation.SelectedItem.Value;
                drList["LOC_DESC"] = ddlLocation.SelectedItem.Text;
            }
            if (ddlMode.SelectedItem != null)
            {
                drList["LOOKUP_ID"] = ddlMode.SelectedItem.Value;
                drList["LOOKUP_NAME"] = ddlMode.SelectedItem.Text;
            }
            //if (ddlCreteria.SelectedItem != null)
            //{
            //    drList["VAC_CRIT_ID"] = ddlCreteria.SelectedItem.Value;
            //    drList["VAC_CRIT_NAME"] = ddlCreteria.SelectedItem.Text;
            //}

            if (ddlStartTimeHr.SelectedItem != null)
            {
                drList["time_hr"] = ddlStartTimeHr.SelectedValue;// startHr1.ToString().Substring(startHr1.ToString().Length - 11, 2).Replace(" ", "0");
            }
            if (ddlStartTimeMin.SelectedItem != null)
            {
                drList["time_min"] = ddlStartTimeMin.SelectedValue;
            }
            if (ddlStartTimeAMPM.SelectedItem != null)
            {
                drList["time_session"] = ddlStartTimeAMPM.SelectedValue;
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_INTERVIEWS_HDR>(hR_INTERVIEWS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillDesignation(gvr);

        }

        protected void ddlDesig_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillEmp(gvr);
        }

        private void fillDesignation(GridViewRow gvr)
        {
            DropDownList ddlDept = gvr.FindControl("ddlDept") as DropDownList;
            DropDownList ddlDesig = gvr.FindControl("ddlDesig") as DropDownList;

            FIN.BLL.HR.AssetIssue_BLL.GetDesignationName(ref ddlDesig, ddlDept.SelectedValue);
        }

        private void fillEmp(GridViewRow gvr)
        {
            DropDownList ddlDept = gvr.FindControl("ddlDept") as DropDownList;
            DropDownList ddlEmployee = gvr.FindControl("ddlEmployee") as DropDownList;
            DropDownList ddlDesig = gvr.FindControl("ddlDesig") as DropDownList;

            FIN.BLL.HR.Interview_BLL.GetEmployeeName(ref ddlEmployee, ddlDept.SelectedValue, ddlDesig.SelectedValue);
        }

        protected void ddlVacancy_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadApplicantList();
        }
        private void LoadApplicantList()
        {
            DataTable dt_Data = Interview_BLL.getShortListApp4vacancy(ddlVacancy.SelectedValue, Master.StrRecordId);
            BindAppGrid(dt_Data);
        }


        private void BindAppGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["AppList"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["SEL_APP"] = "TRUE";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvApp.DataSource = dt_tmp;
                gvApp.DataBind();
                GridViewRow gvr = gvApp.FooterRow;
                FillFooterAppGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvApp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session["AppList"];
            }
            gvApp.EditIndex = -1;

            BindAppGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvApp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["AppList"] != null)
                {
                    dtGridData = (DataTable)Session["AppList"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvApp.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToAPPGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindAppGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToAPPGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddl_AppName = gvr.FindControl("ddlAppName") as DropDownList;
            CheckBox chk_Selected = gvr.FindControl("chkSelected") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INT_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddl_AppName;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList";
            string strMessage = "Applicant";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "APP_ID='" + ddl_AppName.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }





            drList["APP_ID"] = ddl_AppName.SelectedValue.ToString();
            drList["APP_NAME"] = ddl_AppName.SelectedItem.Text.ToString();
            if (chk_Selected.Checked)
            {
                drList["SEL_APP"] = "TRUE";
            }
            else
            {
                drList["SEL_APP"] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvApp_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvApp.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["AppList"] != null)
                {
                    dtGridData = (DataTable)Session["AppList"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToAPPGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvApp.EditIndex = -1;
                BindAppGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvApp_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session["AppList"] != null)
                {
                    dtGridData = (DataTable)Session["AppList"];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindAppGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvApp_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AppList"] != null)
                {
                    dtGridData = (DataTable)Session["AppList"];
                }
                gvApp.EditIndex = e.NewEditIndex;
                BindAppGrid(dtGridData);
                GridViewRow gvr = gvApp.Rows[e.NewEditIndex];
                FillFooterAppGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvApp_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvApp.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvApp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillFooterAppGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_AppName = tmpgvr.FindControl("ddlAppName") as DropDownList;

                FIN.BLL.HR.Applicant_BLL.fn_GetApplicantNMEmployeeEntry(ref ddl_AppName, "'Verified'", Master.Mode);


                if (gvApp.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_AppName.SelectedValue = gvApp.DataKeys[gvApp.EditIndex].Values["APP_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INT_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}