﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InterviewEntry.aspx.cs" Inherits="FIN.Client.HR.InterivewEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <table width="100%">
            <tr>
                <td style="width: 450px" valign="top">
                    <div class="lblBox LNOrient" style="width: 100px" id="lblDesc">
                        Description
                    </div>
                    <div class="divtxtBox  LNOrient" style="width: 300px">
                        <asp:TextBox ID="txtdesc" CssClass="validate[required] RequiredField txtBox" runat="server"
                            TabIndex="1"></asp:TextBox>
                    </div>
                    <div style="clear: both">
                    </div>
                    <br />
                    <div style="clear: both">
                    </div>
                    <div class="lblBox LNOrient" style="width: 100px" id="lblNumber">
                        Vacancy
                    </div>
                    <div class="divtxtBox  LNOrient" style="width: 300px">
                        <asp:DropDownList ID="ddlVacancy" runat="server" CssClass="validate[required] RequiredField ddlStype"
                            TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlVacancy_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="clear: both">
                    </div>
                    <br />
                    <div style="clear: both">
                    </div>
                    <div class="lblBox LNOrient" style="width: 100px" id="lblRequisitionNumber">
                        Active</div>
                    <div class="divtxtBox  LNOrient" style="width: 300px">
                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="3" />
                    </div>
                    <div style="clear: both">
                    </div>
                    <br />
                    <div style="clear: both">
                    </div>
                    <div class="lblBox LNOrient" style="width: 100px" id="Div1">
                        Is Email Alert Required</div>
                    <div class="divtxtBox  LNOrient" style="width: 300px">
                        <asp:CheckBox ID="chkEmail" runat="server" Checked="true" TabIndex="4" />
                    </div>
                </td>
                <td style="width: 650px" valign="top">
                    <div style="height: 200px; overflow: auto">
                        <asp:GridView ID="gvApp" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            Width="600px" DataKeyNames="INT_ID,APP_ID,DELETED" OnRowCancelingEdit="gvApp_RowCancelingEdit"
                            OnRowCommand="gvApp_RowCommand" OnRowCreated="gvApp_RowCreated" OnRowDataBound="gvApp_RowDataBound"
                            OnRowDeleting="gvApp_RowDeleting" OnRowEditing="gvApp_RowEditing" OnRowUpdating="gvApp_RowUpdating"
                            ShowFooter="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Applicant">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlAppName" TabIndex="10" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                            Width="200px">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlAppName" TabIndex="5" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                            Width="200px">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppName" Width="200px" runat="server"  Text='<%# Eval("APP_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Selected">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkSelected" runat="server" TabIndex="11" Checked='<%# Convert.ToBoolean(Eval("SEL_APP")) %>' />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkSelected" TabIndex="6" runat="server" />
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelected" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("SEL_APP")) %>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" TabIndex="12"
                                            ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                        <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="13"
                                            ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" TabIndex="8"
                                            ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                        <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="9"
                                            ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" TabIndex="7"
                                            ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                                    </FooterTemplate>
                                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
        <div class="divRowContainer" align="left" id="divAppId" runat="server" visible="false">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
                Applicant
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlApplicant" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="100%" DataKeyNames="INT_DTL_ID,DEPT_ID,EMP_ID,DEPT_DESIG_ID,LOC_ID,LOOKUP_ID,DELETED,time_hr,time_min,time_session"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlDept" TabIndex="28" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlDept" TabIndex="14" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged" Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDept" Width="130px" runat="server" TabIndex="5" Text='<%# Eval("DEPT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlDesig" TabIndex="29" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="175px" AutoPostBack="True" OnSelectedIndexChanged="ddlDesig_SelectedIndexChanged">
                                <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlDesig" TabIndex="15" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="175px" AutoPostBack="True" OnSelectedIndexChanged="ddlDesig_SelectedIndexChanged">
                                <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDesig" Width="175" runat="server" Text='<%# Eval("DESIG_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmployee" TabIndex="30" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="200px">
                                <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEmployee" TabIndex="16" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="200px">
                                <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmployee" Width="200px" runat="server" Text='<%# Eval("EMP_FIRST_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLocation" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLocation" TabIndex="17" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" Width="130px" runat="server" Text='<%# Eval("LOC_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlMode" TabIndex="32" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlMode" TabIndex="18" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMode" Width="130px" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <EditItemTemplate>
                            <asp:TextBox Width="160px" ID="dtpStartDate" MaxLength="10" runat="server" CssClass="RequiredField txtBox" TabIndex="33"
                                Text='<%#  Eval("INT_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox Width="160px" ID="dtpStartDate" TabIndex="19" MaxLength="10" runat="server"
                                CssClass="RequiredField EntryFont  txtBox" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="160px" ID="lblStartDate" runat="server" Text='<%# Eval("INT_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time Hr" HeaderStyle-Width="100px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStartTimeHr" runat="server" CssClass="ddlStype" TabIndex="34"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="01">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlStartTimeHr" runat="server" CssClass="ddlStype" TabIndex="20"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="01">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="100px" ID="lblStartTimeHr" runat="server" Text='<%# Eval("time_hr") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Min" HeaderStyle-Width="100px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStartTimeMin" runat="server" CssClass="ddlStype" TabIndex="35"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="35">35</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="55">55</asp:ListItem>
                                <asp:ListItem Value="60">60</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlStartTimeMin" runat="server" CssClass="ddlStype" TabIndex="21"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="35">35</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="55">55</asp:ListItem>
                                <asp:ListItem Value="60">60</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="100px" ID="lblStartTimeMin" runat="server" Text='<%# Eval("time_min") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Session" HeaderStyle-Width="100px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStartTimeAMPM" runat="server" CssClass="ddlStype" TabIndex="36"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                <asp:ListItem Value="PM">PM</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlStartTimeAMPM" runat="server" CssClass="ddlStype" TabIndex="22"
                                Width="100px">
                                <asp:ListItem Value="" Text="-"></asp:ListItem>
                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                <asp:ListItem Value="PM">PM</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="100px" ID="lblStartTimeAMPM" runat="server" Text='<%# Eval("time_session") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level Scored" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLevelscored" TabIndex="37" Width="130px" MaxLength="500" runat="server"
                                CssClass="RequiredField txtBox_N" Text='<%# Eval("INT_LEVEL_SCORED") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLevelscored" TabIndex="23" Width="130px" MaxLength="500" runat="server"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLevelscored" Width="130px" runat="server" Text='<%# Eval("INT_LEVEL_SCORED") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcomments" TabIndex="38" Width="130px" MaxLength="500" runat="server"
                                CssClass="txtBox" Text='<%# Eval("INT_COMMENTS") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtcomments" TabIndex="24" Width="130px" MaxLength="500" runat="server"
                                CssClass=" txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblComments" Width="130px" runat="server" Text='<%# Eval("INT_COMMENTS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" TabIndex="39"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="40"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" TabIndex="26"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="27"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" TabIndex="25"
                                ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
