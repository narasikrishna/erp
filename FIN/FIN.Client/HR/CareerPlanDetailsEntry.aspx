﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CareerPlanDetailsEntry.aspx.cs" Inherits="FIN.Client.HR.CareerPlanDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDept">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlDept" runat="server" TabIndex="1"  CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div> 
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlDesignation" runat="server" TabIndex="2" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlDesignation_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmployee">
                Career Path Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:DropDownList ID="ddlCareerPathName" runat="server" TabIndex="3" 
                     CssClass="validate[required] RequiredField ddlStype" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlCareerPathName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtDesc" CssClass="validate[required] RequiredField txtBox" MaxLength="100"
                    TabIndex="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblStartDate">
                Start Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox runat="server" ID="txtStartDate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField  txtBox"
                    TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtStartDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtStartDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 130px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox runat="server" ID="txtEndDate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]]  txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEndDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="300px" DataKeyNames="PROF_ID,PROF_DTL_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Profile Header">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProfileId" Width="200px" AutoPostBack="true" TabIndex="7"
                                runat="server" CssClass="RequiredField EntryFont ddlStype"  OnSelectedIndexChanged="ddlProfileId_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlProfileId" Width="200px" AutoPostBack="true" TabIndex="7"
                                runat="server" CssClass="RequiredField EntryFont ddlStype"  OnSelectedIndexChanged="ddlProfileId_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProfileId" Width="200px" runat="server" Text='<%# Eval("PROF_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profile Detail">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProfiledtl" Width="200px" AutoPostBack="true" TabIndex="8"
                                runat="server"  CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlProfiledtl_SelectedIndexChanged" >
                                
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlProfiledtl" Width="200px" AutoPostBack="true" TabIndex="8"
                                runat="server" CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlProfiledtl_SelectedIndexChanged" >
                                 
                            </asp:DropDownList>
                            
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProfiledtl" Width="200px" runat="server" Text='<%# Eval("COM_LEVEL_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" Width="250px" MaxLength="250" Enabled="true" runat="server" Text='<%# Eval("COM_REMARKS") %>' CssClass="RequiredField   txtBox"
                                TabIndex="9"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" Width="250px" MaxLength="250" Enabled="true" runat="server"  CssClass="RequiredField   txtBox"
                                TabIndex="9"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" Width="250px" Style="word-wrap: break-word"  runat="server" Text='<%# Eval("COM_REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minimum Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMinValue" Width="70px" MaxLength="2" runat="server" Text='<%# Eval("PLAN_PROFILE_MIN_VALUE") %>' CssClass="RequiredField   txtBox_N "
                                TabIndex="10"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE1" runat="server" FilterType="Numbers"
                                TargetControlID="txtMinValue" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMinValue" Width="70px" MaxLength="2" runat="server" CssClass="RequiredField   txtBox_N"
                                TabIndex="10"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE2" runat="server" FilterType="Numbers"
                                TargetControlID="txtMinValue" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMinValue" Width="70px" runat="server" Text='<%# Eval("PLAN_PROFILE_MIN_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Maximum Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMaxValue" TabIndex="11" Width="70px" MaxLength="2" runat="server" Text='<%# Eval("PLAN_PROFILE_MAX_VALUE") %>'
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE3" runat="server" FilterType="Numbers"
                                TargetControlID="txtMaxValue" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMaxValue" TabIndex="11" Width="70px" MaxLength="2" runat="server"
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE4" runat="server" FilterType="Numbers"
                                TargetControlID="txtMaxValue" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMaxValue" Width="70px" runat="server" Text='<%# Eval("PLAN_PROFILE_MAX_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Required Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReqValue" TabIndex="12" Width="70px" MaxLength="2" runat="server" Text='<%# Eval("PLAN_PROFILE_REQUIRED_VALUE") %>'
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE5" runat="server" FilterType="Numbers"
                                TargetControlID="txtReqValue" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtReqValue" TabIndex="12" Width="70px" MaxLength="2" runat="server"
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FTE6" runat="server" FilterType="Numbers"
                                TargetControlID="txtReqValue" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReqValue" Width="70px" runat="server" Text='<%# Eval("PLAN_PROFILE_REQUIRED_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkActive" TabIndex="13" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkActive" TabIndex="13" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="14" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="15" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="16" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="15" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="16" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="btn" TabIndex="19" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" Text="Back" CssClass="btn" TabIndex="20" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
