﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using VMVServices.Services.Data;

namespace FIN.Client.HR
{
    public partial class IndemnityEncashmentEntry : PageBase
    {

        HR_INDEMNITY_ENCASHMENT hR_INDEMNITY_ENCASHMENT = new HR_INDEMNITY_ENCASHMENT();


        DataTable dtGridData = new DataTable();
        Boolean savedBool;
        Boolean bol_rowVisiable;

        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());


                EntityData = null;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_INDEMNITY_ENCASHMENT> userCtx = new DataRepository<HR_INDEMNITY_ENCASHMENT>())
                    {
                        hR_INDEMNITY_ENCASHMENT = userCtx.Find(r =>
                            (r.IND_EC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_INDEMNITY_ENCASHMENT;
                    if (hR_INDEMNITY_ENCASHMENT.IND_EC_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_INDEMNITY_ENCASHMENT.IND_EC_DATE.ToString());
                    }


                    ddlEmployee.SelectedValue = hR_INDEMNITY_ENCASHMENT.IND_EC_EMP_ID;

                    txtIndemnityamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_INDEMNITY_ENCASHMENT.INDM_AMOUNT.ToString());
                    txtElegamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_INDEMNITY_ENCASHMENT.ELIGIBLE_AMOUNT.ToString());
                    txtLoanamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_INDEMNITY_ENCASHMENT.LOAN_AMOUNT.ToString());
                    txtRemarks.Text = hR_INDEMNITY_ENCASHMENT.REMARKS;

                    ddlPaymentOption.SelectedValue = hR_INDEMNITY_ENCASHMENT.PAYMENT_OPTION;
                    if (hR_INDEMNITY_ENCASHMENT.PAYMENT_OPTION == "IN_PAYROLL")
                    {
                        IdPayPrd.Visible = true;
                        ddlPayperiod.SelectedValue = hR_INDEMNITY_ENCASHMENT.PAY_PERIOD_ID;

                    }
                    else
                    {
                        IdPayPrd.Visible = false;
                    }





                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {


            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
            Lookup_BLL.GetLookUpValues(ref ddlPaymentOption, "PAYMENT_OPTION");
            FIN.BLL.PER.PayrollPeriods_BLL.GetPayrollWithoutApprovPeriods(ref ddlPayperiod);



        }



        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_INDEMNITY_ENCASHMENT = (HR_INDEMNITY_ENCASHMENT)EntityData;
                }



                if (txtDate.Text != string.Empty)
                {
                    hR_INDEMNITY_ENCASHMENT.IND_EC_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }
                hR_INDEMNITY_ENCASHMENT.IND_EC_EMP_ID = ddlEmployee.SelectedValue;
                hR_INDEMNITY_ENCASHMENT.INDM_AMOUNT = CommonUtils.ConvertStringToDecimal(txtIndemnityamt.Text.ToString());
                hR_INDEMNITY_ENCASHMENT.ELIGIBLE_AMOUNT = CommonUtils.ConvertStringToDecimal(txtElegamt.Text.ToString());
                hR_INDEMNITY_ENCASHMENT.LOAN_AMOUNT = CommonUtils.ConvertStringToDecimal(txtLoanamt.Text.ToString());



                hR_INDEMNITY_ENCASHMENT.PAYMENT_OPTION = ddlPaymentOption.SelectedValue;

                if (ddlPaymentOption.SelectedValue == "IN_PAYROLL")
                {
                    hR_INDEMNITY_ENCASHMENT.PAY_PERIOD_ID = ddlPayperiod.SelectedValue;
                }

                hR_INDEMNITY_ENCASHMENT.REMARKS = txtRemarks.Text;

                hR_INDEMNITY_ENCASHMENT.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_INDEMNITY_ENCASHMENT.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_INDEMNITY_ENCASHMENT.MODIFIED_BY = this.LoggedUserName;
                    hR_INDEMNITY_ENCASHMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_INDEMNITY_ENCASHMENT.IND_EC_ID = FINSP.GetSPFOR_SEQCode("HR_114".ToString(), false, true);

                    hR_INDEMNITY_ENCASHMENT.CREATED_BY = this.LoggedUserName;
                    hR_INDEMNITY_ENCASHMENT.CREATED_DATE = DateTime.Today;

                }

                hR_INDEMNITY_ENCASHMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDEMNITY_ENCASHMENT.IND_EC_ID);




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                //AssignToBE();



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_INDEMNITY_ENCASHMENT>(hR_INDEMNITY_ENCASHMENT);

                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_INDEMNITY_ENCASHMENT>(hR_INDEMNITY_ENCASHMENT, true);

                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_INDEMNITY_ENCASHMENT>(hR_INDEMNITY_ENCASHMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void ddlPaymentOption_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlPaymentOption.SelectedValue == "IN_PAYROLL")
            {
                IdPayPrd.Visible = true;

            }
            else
            {
                IdPayPrd.Visible = false;

            }
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmployee.SelectedValue.ToString().Length > 0 && txtDate.Text.ToString().Length > 0)
            {
                txtIndemnityamt.Text = CommonUtils.ConvertStringToDecimal(IndemnityDAL.GetSP_Indemnity_Amount(ddlEmployee.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString())).ToString()).ToString();
                //if (CommonUtils.ConvertStringToDecimal(txtIndemnityamt.Text) > 0)
                //{
                //    txtIndemnityamt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtIndemnityamt.Text);
                //}
                DataTable dtEligAmt = new DataTable();
                dtEligAmt = DBMethod.ExecuteQuery(IndemnityEncashmentDAL.GetEligibleAmt(decimal.Parse(txtIndemnityamt.Text))).Tables[0];
                txtElegamt.Text = dtEligAmt.Rows[0]["ELIG_AMT"].ToString();
            }
            else
            {
                txtIndemnityamt.Text = "";
            }
        }

        protected void txtElegamt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEligAmt = new DataTable();
                dtEligAmt = DBMethod.ExecuteQuery(IndemnityEncashmentDAL.GetEligibleAmt(decimal.Parse(txtIndemnityamt.Text))).Tables[0];
                hfEligamt.Value = dtEligAmt.Rows[0]["ELIG_AMT"].ToString();
                if (decimal.Parse(txtElegamt.Text) > decimal.Parse(hfEligamt.Value))
                {
                    ErrorCollection.Add("valamt", "Eligible amount must be 10 to 15 percentage of Indemnity amount.");
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




    }
}