﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class CompanyGraceTime : PageBase
    {
        TM_COMP_GRACE tM_COMP_GRACE = new TM_COMP_GRACE();

        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// AssignToControl Function is used to assign the data from table to the master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    using (IRepository<TM_COMP_GRACE> userCtx = new DataRepository<TM_COMP_GRACE>())
                    {
                        tM_COMP_GRACE = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = tM_COMP_GRACE;
                    
                    txtStartDate.Enabled = false;

                    txtGraceInTime.Text = tM_COMP_GRACE.TM_GRACE_IN.ToString();
                    txtGraceOutTime.Text = tM_COMP_GRACE.TM_GRACE_OUT.ToString();
                    if (tM_COMP_GRACE.TM_START_DATE != null)
                    {
                        txtStartDate.Text = DBMethod.ConvertDateToString(tM_COMP_GRACE.TM_START_DATE.ToString());
                    }
                    //txtEndDate.Text = DBMethod.ConvertDateToString(tM_COMP_GRACE.TM_END_DATE.ToString());
                    if (tM_COMP_GRACE.TM_END_DATE != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(tM_COMP_GRACE.TM_END_DATE.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tM_COMP_GRACE = (TM_COMP_GRACE)EntityData;
                }
             

                tM_COMP_GRACE.TM_GRACE_IN = CommonUtils.ConvertStringToDecimal(txtGraceInTime.Text);
                tM_COMP_GRACE.TM_GRACE_OUT = CommonUtils.ConvertStringToDecimal(txtGraceOutTime.Text);
                tM_COMP_GRACE.TM_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtStartDate.Text != null && txtStartDate.Text != string.Empty && txtStartDate.Text.ToString().Trim().Length > 0)
                {
                    tM_COMP_GRACE.TM_START_DATE = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                }
             
                tM_COMP_GRACE.ENABLED_FLAG = FINAppConstants.Y;

                if (txtEndDate.Text != null && txtEndDate.Text != string.Empty && txtEndDate.Text.ToString().Trim().Length > 0)
                {
                    tM_COMP_GRACE.TM_END_DATE = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                if (Master.Mode == FINAppConstants.Add)
                {
                    if (CommonUtils.ConvertStringToInt(DBMethod.GetStringValue(FIN.DAL.HR.TimeAttendanceReason_DAL.IsValidGraceTime(txtStartDate.Text, tM_COMP_GRACE.PK_ID.ToString()))) > 0)
                    {
                        ErrorCollection.Remove("duplic");
                        ErrorCollection.Add("duplic", "Grace time already defined");
                        return;
                    }
                    if (CommonUtils.ConvertStringToInt(DBMethod.GetStringValue(FIN.DAL.HR.TimeAttendanceReason_DAL.GetDiffValidGraceTime(txtStartDate.Text, txtEndDate.Text))) > 0)
                    {
                        ErrorCollection.Remove("duplicrenage");
                        ErrorCollection.Add("duplicrenage", "Grace time already defined");
                        return;
                    }
                }
                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    tM_COMP_GRACE.MODIFIED_BY = this.LoggedUserName;
                    tM_COMP_GRACE.MODIFIED_DATE = DateTime.Today;

                    

                    tM_COMP_GRACE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_COMP_GRACE.PK_ID.ToString());

                    DBMethod.SaveEntity<TM_COMP_GRACE>(tM_COMP_GRACE, true);
                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                    savedBool = true;
                }
                else
                {
                    tM_COMP_GRACE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.tM_COMP_GRACE_SEQ);
                  
                  

                    tM_COMP_GRACE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_COMP_GRACE.PK_ID.ToString());

                    tM_COMP_GRACE.CREATED_BY = this.LoggedUserName;
                    tM_COMP_GRACE.CREATED_DATE = DateTime.Today;

                    DBMethod.SaveEntity<TM_COMP_GRACE>(tM_COMP_GRACE);
                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                    savedBool = true;
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {


                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {


                //            break;
                //        }
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTime", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtStartDate;
                //slControls[1] = txtEndDate;
                slControls[1] = txtGraceInTime;
                slControls[2] = txtGraceOutTime;

                ErrorCollection.Clear();
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = "TextBox~TextBox~TextBox";
                string strMessage = Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Grace_In_Time_P"] + " ~ " + Prop_File_Data["Grace_Out_Time_P"] + "";
                //string strMessage = "Start Date ~ End Date ~ Grace In Time ~ Grace Out Time";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtStartDate.Text.Length > 0 && txtEndDate.Text.Length > 0)
                {
                    if (Convert.ToDateTime(DBMethod.ConvertStringToDate(txtStartDate.Text)) >= Convert.ToDateTime(DBMethod.ConvertStringToDate(txtEndDate.Text)))
                    {
                        ErrorCollection.Remove("daterange");
                        ErrorCollection.Add("daterange","End Date Must Be Greater Than Start Date");
                        return;
                    }
                }
            
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTimeSave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TM_COMP_GRACE>(tM_COMP_GRACE);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTimeDelete", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtGraceInTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                if (int.Parse(txtGraceInTime.Text.ToString()) > 60)
                {
                    ErrorCollection.Add("grcprdval", Prop_File_Data["grcprdval_P"]);
                        
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTime_Grcprdval", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtGraceOutTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                if (int.Parse(txtGraceOutTime.Text.ToString()) > 60)
                {
                    ErrorCollection.Add("grcprdval", Prop_File_Data["grcproutdval_P"]);
                       
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CompanyGraceTime_Grcprdval2", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

    }
}