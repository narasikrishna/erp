﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR
{
    public partial class rptHRStaffAttedance : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlDepartment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.DEPT_NAME, ddlDepartment.SelectedValue);
                }
                if (ddlLeaveType.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.LEAVE_ID, ddlLeaveType.SelectedValue);
                }
                if (txtFirstname.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FIRSTNAME", txtFirstname.Text);
                }
                if (txtLastName.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("LASTNAME", txtLastName.Text);
                }
                if (chkShowOff.Checked)
                {
                    htFilterParameter.Add("SHOWOFF", chkShowOff.Checked);
                }
                if (ddlEmployeeName.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("EMPID", ddlEmployeeName.SelectedValue);
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;



                DateTime dt_FromDate = DBMethod.ConvertStringToDate(txtFromDate.Text);
                DateTime dt_ToDate = DBMethod.ConvertStringToDate(txtToDate.Text);
                TimeSpan ts = dt_ToDate - dt_FromDate;
                int int_NoOfMonth = int.Parse((ts.Days / 30).ToString());
                Table tbl = new Table();
                tbl.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                tbl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;

                tbl.Width = System.Web.UI.WebControls.Unit.Percentage(100);

                for (int kLoop = 0; kLoop <= int_NoOfMonth; kLoop++)
                {


                    if (dt_FromDate < dt_ToDate)
                    {

                        int int_StartCol = 1;
                        int int_EndCol = 0;
                        htFilterParameter.Remove("MONTH");
                        htFilterParameter.Add("MONTH", dt_FromDate.Month.ToString());

                        htFilterParameter.Remove("YEAR");
                        htFilterParameter.Add("YEAR", dt_FromDate.Year.ToString());

                        ReportData = FIN.BLL.HR.StaffAttendance_BLL.getAttendanceRep4Month();

                        GridView gv_AttCol = new GridView();
                        int int_NoOfDays4Month = DateTime.DaysInMonth(dt_FromDate.Year, dt_FromDate.Month);
                        DateTime dt_StartDate = new DateTime(dt_FromDate.Year, dt_FromDate.Month, 1);

                        string[] days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                        string[] Shortdays = { "M", "T", "W", "T", "F", "S", "S" };
                        int daysLoop = 0;
                        bool bol_WeekStart = false;
                        int int_DayCount = 1;
                        gv_AttCol.ID = "gv_" + dt_FromDate.Month.ToString() + "_" + dt_FromDate.Year.ToString();
                        gv_AttCol.CssClass = "DisplayFont Grid";
                        gv_AttCol.EmptyDataRowStyle.CssClass = "EmptyRowStyle";
                        gv_AttCol.HeaderStyle.CssClass = "GridHeader";
                        //gv_AttCol.AlternatingRowStyle.CssClass = "GrdAltRow";
                        gv_AttCol.AutoGenerateColumns = false;
                        gv_AttCol.ShowHeaderWhenEmpty = true;

                        gv_AttCol.RowDataBound += new GridViewRowEventHandler(gvAttCol_RowDataBound);

                        BoundField field = new BoundField();

                        field.HeaderText = "Employee Name";
                        field.DataField = "EMP_NAME";
                        gv_AttCol.Columns.Add(field);

                        //gv_AttCol.Columns[0].ControlStyle.Width = 300;
                        //gv_AttCol.Columns[0].ItemStyle.Wrap = true;

                        for (int iLoop = 1; iLoop <= 42; iLoop++)
                        {
                            field = new BoundField();

                            field.HeaderText = Shortdays[daysLoop];
                            if (dt_StartDate.DayOfWeek.ToString() == days[daysLoop])
                            {
                                bol_WeekStart = true;

                            }
                            if (bol_WeekStart)
                            {
                                if (int_DayCount > 0)
                                {
                                    int_EndCol = iLoop + 1;
                                    field.HeaderText = field.HeaderText;
                                    field.DataField = "D" + int_DayCount.ToString();
                                }
                                else
                                {
                                    field.DataField = "Empty_Col";
                                }
                                int_DayCount += 1;
                                if (int_DayCount > int_NoOfDays4Month)
                                {
                                    int_DayCount = -50;
                                }
                            }
                            else
                            {
                                field.DataField = "Empty_Col";
                                int_StartCol = iLoop + 1;
                            }

                            gv_AttCol.Columns.Add(field);
                            gv_AttCol.Columns[iLoop].ControlStyle.Width = 50;
                            gv_AttCol.Columns[iLoop].ItemStyle.Wrap = true;
                            daysLoop += 1;
                            if (daysLoop > 6)
                                daysLoop = 0;
                        }



                        gv_AttCol.DataSource = ReportData;
                        gv_AttCol.DataBind();
                        if (ddlEmployeeName.SelectedValue.Trim().Length > 0)
                        {
                            gv_AttCol.Columns[0].Visible = false;
                        }
                        else
                        {
                            gv_AttCol.Columns[0].Visible = true;
                        }
                        for (int rLoop = int_StartCol; rLoop < int_EndCol; rLoop++)
                        {

                            gv_AttCol.Rows[0].Cells[rLoop].Text = ((rLoop - int_StartCol) + 1).ToString();

                        }

                        TableRow TR = new TableRow();
                        TableCell TC = new TableCell();
                        TC.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        TC.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                        TC.CssClass = "lblBox";
                        TC.Text = dt_FromDate.ToString("MMMM");
                        TC.Width = System.Web.UI.WebControls.Unit.Percentage(10);
                        TR.Cells.Add(TC);
                        TC = new TableCell();
                        TC.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
                        TC.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                        TC.Width = System.Web.UI.WebControls.Unit.Percentage(90);
                        TC.Controls.Add(gv_AttCol);
                        TR.Cells.Add(TC);

                        dt_FromDate = dt_FromDate.AddMonths(1);
                        tbl.Rows.Add(TR);
                    }

                    div_AttCol.Controls.Add(tbl);

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        void gvAttCol_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Do whatever you want in here
            e.Row.Cells[0].Width = System.Web.UI.WebControls.Unit.Pixel(300);
            for (int gLoop = 1; gLoop < e.Row.Cells.Count; gLoop++)
            {
                if (e.Row.RowIndex > 0)
                {
                    e.Row.Cells[3].Width = System.Web.UI.WebControls.Unit.Pixel(30);
                    if (e.Row.Cells[gLoop].Text != "Present" && e.Row.Cells[gLoop].Text.ToString().Length > 0 && e.Row.Cells[gLoop].Text !="&nbsp;")
                    {
                        if (ddlLeaveType.Items.FindByText(e.Row.Cells[gLoop].Text) != null)
                        {
                            int int_value = int.Parse(ddlLeaveType.Items.FindByText(e.Row.Cells[gLoop].Text).Value.ToString());
                            e.Row.Cells[gLoop].BackColor = System.Drawing.Color.FromArgb(int_value, int_value, int_value);
                            e.Row.Cells[gLoop].ForeColor = System.Drawing.Color.White;
                        }// FromName();
                    }
                }
                else
                {
                    e.Row.Cells[gLoop].Width = System.Web.UI.WebControls.Unit.Pixel(30);

                }

            }
        }
        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            //LeaveDefinition_BLL.GetLeave(ref ddlLeaveType);

            DataTable dt_LeaveList = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeave()).Tables[0];
            
            int int_colornumber = 50;
            ddlLeaveType.Items.Clear();

            for (int iLoop = 0; iLoop < dt_LeaveList.Rows.Count; iLoop++)
            {
                ddlLeaveType.Items.Add(new ListItem(dt_LeaveList.Rows[iLoop]["LEAVE_ID"].ToString(), int_colornumber.ToString()));
                int_colornumber += 30;
            }

            ddlLeaveType.Items.Add(new ListItem("WEEKOFF", int_colornumber.ToString()));
            int_colornumber += 10;
            DataTable dt_WeekOffDay = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemParameters_DAL.GetWeekOffDay()).Tables[0];
            hf_WeekOffDays.Value = "";
            if (dt_WeekOffDay.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dt_WeekOffDay.Rows.Count; iLoop++)
                {
                    hf_WeekOffDays.Value += dt_WeekOffDay.Rows[iLoop]["param_value"].ToString() + " ";
                }

            }

            //DateTime tmp = new DateTime(2000, 1, 1);
            //ddlMonth.Items.Clear();
            //ddlYear.Items.Clear();
            //for (int iLoop = 0; iLoop < 12; iLoop++)
            //{
            //    ddlMonth.Items.Add(new ListItem(tmp.ToString("MMM"), iLoop.ToString()));
            //    tmp = tmp.AddMonths(1);
            //}
            //for (int kLoop = 2010; kLoop < 2030; kLoop++)
            //{
            //    ddlYear.Items.Add(new ListItem(kLoop.ToString(), kLoop.ToString()));
            //}
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            GridView gv = (GridView)div_AttCol.FindControl("gv_4_2014");
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee_BLL.GetEmplName(ref ddlEmployeeName, ddlDepartment.SelectedValue.ToString());

        }


    }
}