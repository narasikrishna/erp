﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.HR;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LeaveRegularisation : PageBase
    {
        HR_LEAVE_REGULARIZATION hR_LEAVE_REGULARIZATION = new HR_LEAVE_REGULARIZATION();
        Permission_BLL Permission_BLL = new Permission_BLL();
        DataTable dtData = new DataTable();
        string ProReturn = null;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRLeaveRegularization", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// AssignToControl Function is used to assign the data from table to the master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_LEAVE_REGULARIZATION> userCtx = new DataRepository<HR_LEAVE_REGULARIZATION>())
                    {
                        hR_LEAVE_REGULARIZATION = userCtx.Find(r =>
                            (r.LR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LEAVE_REGULARIZATION;

                    if (hR_LEAVE_REGULARIZATION.ATTRIBUTE1 != null)
                    {
                        ddlFinancialYear.SelectedValue = hR_LEAVE_REGULARIZATION.ATTRIBUTE1;

                    }
                    if (hR_LEAVE_REGULARIZATION.LR_TO_DT != null)
                    {
                        ddlDepartment.SelectedValue = hR_LEAVE_REGULARIZATION.ATTRIBUTE2;
                        fill_Lvtyp_Staff();
                    }
                    if (hR_LEAVE_REGULARIZATION.LR_EMP_ID != null)
                    {
                        ddlEmpName.SelectedValue = hR_LEAVE_REGULARIZATION.LR_EMP_ID.ToString();
                        fillLeaveTyp();
                    }

                    ddlCategory.SelectedValue = hR_LEAVE_REGULARIZATION.LR_CATEGORY.ToString();
                    ddlType.SelectedValue = hR_LEAVE_REGULARIZATION.LR_TYPE.ToString();
                    txtReason.Text = hR_LEAVE_REGULARIZATION.LR_REASON;
                    ddlStatus.SelectedValue = hR_LEAVE_REGULARIZATION.LR_STATUS;
                    txtFromDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_REGULARIZATION.LR_FROM_DT.ToString());
                    //txtToDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_REGULARIZATION.LR_TO_DT.ToString());
                    if (hR_LEAVE_REGULARIZATION.LR_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_REGULARIZATION.LR_TO_DT.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRLeaveRegularization", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlCategory, "CATEG");
            // Lookup_BLL.GetLookUpValues(ref ddlType, "LRTYP");
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "SS");
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
        }
        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillLeaveTyp();
        }
        private void fillLeaveTyp()
        {
            LeaveDefinition_BLL.GetLeaveBasedFiscalYearDept(ref ddlType, ddlFinancialYear.SelectedValue.ToString(), ddlDepartment.SelectedValue.ToString(), ddlEmpName.SelectedValue);
        }
        private void fill_Lvtyp_Staff()
        {
            Employee_BLL.GetEmplNameBothIntExt(ref ddlEmpName, ddlDepartment.SelectedValue);
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fill_Lvtyp_Staff();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        /// 

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_REGULARIZATION = (HR_LEAVE_REGULARIZATION)EntityData;
                }

                hR_LEAVE_REGULARIZATION.LR_EMP_ID = ddlEmpName.SelectedValue.ToString();
                hR_LEAVE_REGULARIZATION.LR_CATEGORY = ddlCategory.SelectedValue.ToString();
                hR_LEAVE_REGULARIZATION.LR_TYPE = ddlType.SelectedValue.ToString();
                hR_LEAVE_REGULARIZATION.LR_REASON = txtReason.Text;
                hR_LEAVE_REGULARIZATION.LR_STATUS = ddlStatus.SelectedValue.ToString();
                hR_LEAVE_REGULARIZATION.LR_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                hR_LEAVE_REGULARIZATION.ENABLED_FLAG = FINAppConstants.Y;
                hR_LEAVE_REGULARIZATION.LR_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_LEAVE_REGULARIZATION.ATTRIBUTE1 = ddlFinancialYear.SelectedValue;
                hR_LEAVE_REGULARIZATION.ATTRIBUTE2 = ddlDepartment.SelectedValue;


                if (txtToDate.Text != null && txtToDate.Text != string.Empty && txtToDate.Text.ToString().Trim().Length > 0)
                {
                    hR_LEAVE_REGULARIZATION.LR_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LEAVE_REGULARIZATION.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_REGULARIZATION.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_LEAVE_REGULARIZATION.LR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_LR_M.ToString(), false, true);
                    hR_LEAVE_REGULARIZATION.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_REGULARIZATION.CREATED_DATE = DateTime.Today;
                }
                hR_LEAVE_REGULARIZATION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_REGULARIZATION.LR_ID);



                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.LeaveRegularisation_DAL.GetSPFOR_DUPLICATE_CHECK(hR_LEAVE_REGULARIZATION.LR_EMP_ID, txtFromDate.Text.ToString(), txtFromDate.Text.ToString(), hR_LEAVE_REGULARIZATION.LR_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("LEAVAPPexists", ProReturn);

                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                DataTable leaveCancel = new DataTable();
                DataTable Month_data = new DataTable();
                string fiscalYr = string.Empty;
                string deptID = string.Empty;
                int month = 0;
                Month_data = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetMonth(txtFromDate.Text)).Tables[0];
                month = CommonUtils.ConvertStringToInt(Month_data.Rows[0][0].ToString());
                string leaveIdValue = string.Empty;

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_LEAVE_REGULARIZATION>(hR_LEAVE_REGULARIZATION);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<HR_LEAVE_REGULARIZATION>(hR_LEAVE_REGULARIZATION, true);

                            break;
                        }
                }


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (Master.StrRecordId != string.Empty && Master.Mode != FINAppConstants.Add)
                    {
                        if (hR_LEAVE_REGULARIZATION.LR_STATUS.ToUpper() == "APPROVED")
                        {
                            DateTime FrmDate = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                            leaveCancel = DBMethod.ExecuteQuery(LeaveReturn_DAL.GetLeaveRegDtls(hR_LEAVE_REGULARIZATION.LR_ID.ToString())).Tables[0];
                            if (leaveCancel.Rows.Count > 0)
                            {
                                fiscalYr = leaveCancel.Rows[0]["fiscal_year"].ToString();
                                deptID = leaveCancel.Rows[0]["dept_id"].ToString();
                                leaveIdValue = leaveCancel.Rows[0]["LEAVE_ID"].ToString();
                            }

                            DateTime dtfrmdt = new DateTime();
                            DateTime dtTodt = new DateTime();
                            dtfrmdt = Convert.ToDateTime(txtFromDate.Text);
                            dtTodt = Convert.ToDateTime(txtToDate.Text);
                            int noOfDays = 0;

                            int count_friday = ClsGridBase.CountDays(DayOfWeek.Friday, dtfrmdt, dtTodt);
                            noOfDays = (((dtTodt - FrmDate).Days + 1) - count_friday);

                            StaffLeaveDetails_DAL.UPDATE_EMP_LEAVE_BALANCE(fiscalYr, ddlEmpName.SelectedValue.ToString(), hR_LEAVE_REGULARIZATION.LR_ORG_ID.ToString(), leaveIdValue, deptID, noOfDays, "REGULAR");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRLeaveRegularization", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                if (DBMethod.ConvertStringToDate(txtFromDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtToDate.Text.ToString()))
                {
                    ErrorCollection.Add("daterange", Prop_File_Data["daterangeTo_P"]);

                    return;

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRLeaveRegularizationSave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        /// <summary>
        /// Used to delete the Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_LEAVE_REGULARIZATION>(hR_LEAVE_REGULARIZATION);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRLeaveRegularizationDelete", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}