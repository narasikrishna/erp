﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class ShortListEntry : PageBase
    {

        HR_SHORT_LIST_HDR HR_SHORT_LIST_HDR = new HR_SHORT_LIST_HDR();
        HR_SHORT_LIST_DTL HR_SHORT_LIST_DTL = new HR_SHORT_LIST_DTL();


        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        string websiteURL = "";
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            Interview_BLL.fn_GetVacancyNM(ref ddlVacancy);

            //dtData = DBMethod.ExecuteQuery(Supplier_DAL.getCountry()).Tables[0];
            //chkDropNation.DataSource = dtData;
            //chkDropNation.DataTextField = "NATIONALITY";
            //chkDropNation.DataValueField = "COUNTRY_CODE";
            //chkDropNation.DataBind();
            dtData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("NATIONALITY")).Tables[0];
            chkDropNation.DataSource = dtData;
            chkDropNation.DataTextField = FINColumnConstants.LOOKUP_NAME;
            chkDropNation.DataValueField = FINColumnConstants.LOOKUP_ID;
            chkDropNation.DataBind();
        }


        private void FillLeaveRequest(GridViewRow gvr)
        {
            // GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;

            if (ddlStaff.SelectedValue != "")
            {
                LeaveApplication_BLL.GetLeaveApplication(ref ddlRequest, ddlStaff.SelectedValue.ToString());

                if (gvData.EditIndex >= 0)
                {

                }
            }

            if (ddlAttendanceType.SelectedItem.Text == "Leave")
            {
                ddlRequest.Enabled = true;
            }
            else
            {
                ddlRequest.Enabled = false;
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();
                dtGridData = DBMethod.ExecuteQuery(ShortList_DAL.GetShortListData(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_SHORT_LIST_HDR = ShortList_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = HR_SHORT_LIST_HDR;
                    btnProcess.Enabled = false;

                    ddlVacancy.SelectedValue = HR_SHORT_LIST_HDR.VACANCY_ID.ToString();

                    if (HR_SHORT_LIST_HDR.REQUIRED_TECHNOLOGY != null)
                    {
                        txtReqTech.Text = HR_SHORT_LIST_HDR.REQUIRED_TECHNOLOGY;
                    }
                    if (HR_SHORT_LIST_HDR.EXP_MIN_VALUE != null)
                    {
                        txtExpMin.Text = HR_SHORT_LIST_HDR.EXP_MIN_VALUE;
                    }
                    if (HR_SHORT_LIST_HDR.EXP_MAX_VALUE != null)
                    {
                        txtExpMax.Text = HR_SHORT_LIST_HDR.EXP_MAX_VALUE;
                    }
                    if (HR_SHORT_LIST_HDR.AGE_MIN_VALUE != null)
                    {
                        txtAgeMin.Text = HR_SHORT_LIST_HDR.AGE_MIN_VALUE;
                    }
                    if (HR_SHORT_LIST_HDR.AGE_MAX_VALUE != null)
                    {
                        txtAgeMax.Text = HR_SHORT_LIST_HDR.AGE_MAX_VALUE;
                    }
                    if (HR_SHORT_LIST_HDR.QUALIFICATION_ID != null)
                    {
                        txtQualification.Text = HR_SHORT_LIST_HDR.QUALIFICATION_ID;
                    }
                    if (HR_SHORT_LIST_HDR.NATIONALITY_ID != null)
                    {
                        chkDropNation.SelectedValue = HR_SHORT_LIST_HDR.NATIONALITY_ID;
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                websiteURL = System.Web.Configuration.WebConfigurationManager.AppSettings["WBSITEURL"].ToString();

                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["IS_SELECT"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlStaff = tmpgvr.FindControl("ddlStaff") as DropDownList;
                //DropDownList ddlAttendanceType = tmpgvr.FindControl("ddlAttendanceType") as DropDownList;
                //DropDownList ddlRequest = tmpgvr.FindControl("ddlRequest") as DropDownList;

                //Employee_BLL.GetEmplName(ref ddlStaff,ddlDepartment.SelectedValue);
                //Lookup_BLL.GetLookUpValues(ref ddlAttendanceType, "ATY");

                //if (gvData.EditIndex >= 0)
                //{
                //    ddlStaff.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.EMP_ID].ToString();
                //    ddlAttendanceType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ATTENDANCE_TYPE].ToString();

                //    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //    {
                //        FillLeaveRequest(tmpgvr);
                //    }
                //    ddlRequest.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_REQ_ID].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                string selectedReq = string.Empty;

                if (chkDropNation.Items.Count > 0)
                {
                    for (int iLoop = 0; iLoop < chkDropNation.Items.Count; iLoop++)
                    {
                        if (chkDropNation.Items[iLoop].Selected == true)
                        {
                            selectedReq += "'" + chkDropNation.Items[iLoop].Value + "',";// "''" + chkReqNumber.Items[iLoop].Value + "'','";
                        }
                    }
                    if (selectedReq != string.Empty)
                    {
                        int index = selectedReq.LastIndexOf(',');
                        selectedReq = selectedReq.Remove(index, 1);
                    }
                }

                dtGridData = DBMethod.ExecuteQuery(ShortList_DAL.GetApplicantsData(ddlVacancy.SelectedValue.ToString(), txtReqTech.Text.Trim(), (txtExpMin.Text), (txtExpMax.Text), (txtAgeMin.Text), (txtAgeMax.Text), txtQualification.Text.ToUpper(), selectedReq.Trim())).Tables[0];
                //dtGridData = DBMethod.ExecuteQuery(ShortList_DAL.GetApplicantsData(txtReqTech.Text, CommonUtils.ConvertStringToDecimal(txtExpMin.Text), CommonUtils.ConvertStringToDecimal(txtExpMax.Text), CommonUtils.ConvertStringToDecimal(txtAgeMin.Text), CommonUtils.ConvertStringToDecimal(txtAgeMax.Text), txtQualification.Text, chkDropNation.SelectedValue.ToString())).Tables[0];
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlVacancy;

                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
                string strMessage = "Vacancy";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Short Listed Applicants ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (gvData.Rows.Count == 0)
                {
                    ErrorCollection.Remove("short");
                    ErrorCollection.Add("short", "Please select the Applicants");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GridViewRow gvr = gvData.FooterRow;
            //DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            //Employee_BLL.GetEmplName(ref ddlStaff, ddlDepartment.SelectedValue);
            //dtGridData = DBMethod.ExecuteQuery(StaffAttendance_DAL.GetStaffAttedDtls(Master.StrRecordId)).Tables[0];
            //BindGrid(dtGridData);
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_SHORT_LIST_HDR = (HR_SHORT_LIST_HDR)EntityData;
                }

                HR_SHORT_LIST_HDR.VACANCY_ID = ddlVacancy.SelectedValue.ToString();
                HR_SHORT_LIST_HDR.REQUIRED_TECHNOLOGY = txtReqTech.Text;
                HR_SHORT_LIST_HDR.EXP_MIN_VALUE = txtExpMin.Text;
                HR_SHORT_LIST_HDR.EXP_MAX_VALUE = txtExpMax.Text;

                HR_SHORT_LIST_HDR.AGE_MIN_VALUE = txtAgeMin.Text;
                HR_SHORT_LIST_HDR.AGE_MAX_VALUE = txtAgeMax.Text;

                HR_SHORT_LIST_HDR.QUALIFICATION_ID = txtQualification.Text;
                HR_SHORT_LIST_HDR.NATIONALITY_ID = chkDropNation.SelectedValue;
                HR_SHORT_LIST_HDR.ENABLED_FLAG = "1";
                HR_SHORT_LIST_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    HR_SHORT_LIST_HDR.MODIFIED_BY = this.LoggedUserName;
                    HR_SHORT_LIST_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    HR_SHORT_LIST_HDR.CREATED_BY = this.LoggedUserName;
                    HR_SHORT_LIST_HDR.CREATED_DATE = DateTime.Today;
                    HR_SHORT_LIST_HDR.SHORT_LIST_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_085_M.ToString(), false, true);
                }

                HR_SHORT_LIST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_SHORT_LIST_HDR.SHORT_LIST_HDR_ID);

                //detail section
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                int selectedCount = 0;

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    if (((CheckBox)gvData.Rows[iLoop].FindControl("chkSelection")).Checked == true)
                    {
                        selectedCount = selectedCount + 1;

                        HR_SHORT_LIST_DTL = new HR_SHORT_LIST_DTL();

                        if ((gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString()) != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString() != string.Empty)
                        {
                            HR_SHORT_LIST_DTL = ShortList_BLL.getDtlClassEntity(gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString());
                        }


                        HR_SHORT_LIST_DTL.APPLICANT_ID = ((Label)gvData.Rows[iLoop].FindControl("lblAppId")).Text;
                        HR_SHORT_LIST_DTL.IS_SELECT = "1";
                        HR_SHORT_LIST_DTL.SHORT_LIST_HDR_ID = HR_SHORT_LIST_HDR.SHORT_LIST_HDR_ID;

                        HR_SHORT_LIST_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        HR_SHORT_LIST_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                        if ((gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString()) != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString() != string.Empty)
                        {
                            HR_SHORT_LIST_DTL.SHORT_LIST_DTL_ID = gvData.DataKeys[iLoop].Values[FINColumnConstants.SHORT_LIST_DTL_ID].ToString();
                            HR_SHORT_LIST_DTL.MODIFIED_BY = this.LoggedUserName;
                            HR_SHORT_LIST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_SHORT_LIST_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            HR_SHORT_LIST_DTL.SHORT_LIST_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_085_D.ToString(), false, true);
                            HR_SHORT_LIST_DTL.CREATED_BY = this.LoggedUserName;
                            HR_SHORT_LIST_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_SHORT_LIST_DTL, FINAppConstants.Add));
                        }
                    }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (selectedCount == 0)
                {
                    ErrorCollection.Remove("short");
                    ErrorCollection.Add("short", "Please select the Applicants");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_SHORT_LIST_HDR, HR_SHORT_LIST_DTL>(HR_SHORT_LIST_HDR, tmpChildEntity, HR_SHORT_LIST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_SHORT_LIST_HDR, HR_SHORT_LIST_DTL>(HR_SHORT_LIST_HDR, tmpChildEntity, HR_SHORT_LIST_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;
            TextBox txtReason = gvr.FindControl("txtReason") as TextBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.STATT_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            if (ddlRequest.SelectedItem.Text != FINMessageConstatns.AttendanceType_Present.ToString())
            {
                slControls[0] = ddlStaff;
                slControls[1] = ddlAttendanceType;

                strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                strMessage = Prop_File_Data["Staff_Name_P"] + " ~ " + Prop_File_Data["Attendance_type_P"] + "";
                //strMessage = "Staff Name ~ Attendance type ";
            }
            else if (ddlRequest.SelectedItem.Text == FINMessageConstatns.AttendanceType_Present.ToString())
            {
                slControls[0] = ddlStaff;
                slControls[1] = ddlAttendanceType;
                slControls[3] = ddlRequest;

                strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                strMessage = Prop_File_Data["Staff_Name_P"] + " ~ " + Prop_File_Data["Attendance_type_P"] + " ~ " + Prop_File_Data["Leave_Request_P"] + "";
                strMessage = "Staff Name ~ Attendance type ~ Leave Request";
            }

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "EMP_ID='" + ddlStaff.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.EMP_ID] = ddlStaff.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlStaff.SelectedItem.Text;
            drList[FINColumnConstants.ATTENDANCE_TYPE] = ddlAttendanceType.SelectedValue;
            drList["ATTENDANCE_TYPE_name"] = ddlAttendanceType.SelectedItem.Text;
            drList[FINColumnConstants.LEAVE_NAME] = ddlRequest.SelectedItem.Text == FINAppConstants.intialRowTextField ? string.Empty : ddlRequest.SelectedItem.Text;
            drList[FINColumnConstants.LEAVE_REQ_ID] = ddlRequest.SelectedValue == null ? string.Empty : ddlRequest.SelectedValue.ToString();
            drList[FINColumnConstants.REASON] = txtReason.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                    HyperLink h1 = (HyperLink)e.Row.FindControl("hl_Resume");
                    if (gvData.DataKeys[e.Row.RowIndex].Values["file_path"].ToString().Length > 0)
                    {
                        h1.NavigateUrl = websiteURL + "/UploadFile/Resume/" + gvData.DataKeys[e.Row.RowIndex].Values["file_path"].ToString();
                    }
                    else
                    {
                        h1.Enabled = false;
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion


    }
}