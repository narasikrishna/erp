﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class SettlementEntry : PageBase
    {
        HR_SETTLEMENTS HR_SETTLEMENTS = new HR_SETTLEMENTS();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            Lookup_BLL.GetLookUpValues(ref ddlSettlementType, "LRT");
            Lookup_BLL.GetLookUpValues(ref ddlType, "SETLE_TYPE");
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_SETTLEMENTS> userCtx = new DataRepository<HR_SETTLEMENTS>())
                    {
                        HR_SETTLEMENTS = userCtx.Find(r =>
                            (r.SETTLEMENT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    EntityData = HR_SETTLEMENTS;

                    if (HR_SETTLEMENTS.SETTLEMENT_DATE != null)
                    {
                        txtSettlementDate.Text = DBMethod.ConvertDateToString(HR_SETTLEMENTS.SETTLEMENT_DATE.ToString());
                    }
                    if (HR_SETTLEMENTS.SETTLEMENT_DEPT_ID != null)
                    {
                        ddlDepartment.SelectedValue = HR_SETTLEMENTS.SETTLEMENT_DEPT_ID.ToString();
                        fill_Staff();
                    }
                    if (HR_SETTLEMENTS.SETTLEMENT_EMP_ID != null)
                    {
                        ddlEmployee.SelectedValue = HR_SETTLEMENTS.SETTLEMENT_EMP_ID.ToString();
                    }
                    if (HR_SETTLEMENTS.SETTLEMENT_ENTITY_TYPE != null)
                    {
                        ddlSettlementType.SelectedValue = HR_SETTLEMENTS.SETTLEMENT_ENTITY_TYPE.ToString();
                        LoanRequest_BLL.GetLoanDetails(ref ddlSettlementId, ddlSettlementType.SelectedValue,ddlEmployee.SelectedValue);
                    }
                    if (HR_SETTLEMENTS.SETTLEMENT_ENTITY_ID != null)
                    {
                        ddlSettlementId.SelectedValue = HR_SETTLEMENTS.SETTLEMENT_ENTITY_ID.ToString();
                    }
                    if (HR_SETTLEMENTS.SETTLEMENT_TYPE != null)
                    {
                        ddlType.SelectedValue = HR_SETTLEMENTS.SETTLEMENT_TYPE.ToString();
                        GetLoanAmt();
                    }

                    if (HR_SETTLEMENTS.SETTLEMENT_AMOUNT != null)
                    {
                        txtSettlementAmount.Text = CommonUtils.ConvertStringToDecimal(HR_SETTLEMENTS.SETTLEMENT_AMOUNT.ToString()).ToString();
                        txtSettlementAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtSettlementAmount.Text);
                    }
                    txtRemarks.Text = HR_SETTLEMENTS.SETTLEMENT_REMARKS;
                    txtAdditionalDetails1.Text = HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER1;
                    txtAdditionalDetails2.Text = HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER2;
                    txtAdditionalDetails3.Text = HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER3;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fill_Staff();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fill_Staff()
        {
            Employee_BLL.GetEmplName(ref ddlEmployee, ddlDepartment.SelectedValue);
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlDepartment;
                slControls[1] = ddlEmployee;
                slControls[2] = txtSettlementDate;
                slControls[3] = ddlSettlementType;
                slControls[4] = ddlSettlementId;

                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = "Department ~ Employee ~ Settlement Date ~ Settlement Type ~ Settlement Id";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                //if (DBMethod.ConvertStringToDate(txtFromDate.Text) <= DBMethod.ConvertStringToDate(txtAttendanceDate.Text) && DBMethod.ConvertStringToDate(txtToDate.Text) >= DBMethod.ConvertStringToDate(txtAttendanceDate.Text))
                //{

                //}
                //else
                //{
                //    ErrorCollection.Add("invalidAttendance", "Attendance Date should be between From and To Date");
                //    return;
                //}


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_SETTLEMENTS = (HR_SETTLEMENTS)EntityData;
                }
                if (txtSettlementDate.Text != string.Empty)
                {
                    HR_SETTLEMENTS.SETTLEMENT_DATE = DBMethod.ConvertStringToDate(txtSettlementDate.Text);
                }
                HR_SETTLEMENTS.SETTLEMENT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                HR_SETTLEMENTS.SETTLEMENT_DEPT_ID = ddlDepartment.SelectedValue;
                HR_SETTLEMENTS.SETTLEMENT_EMP_ID = ddlEmployee.SelectedValue;
                HR_SETTLEMENTS.SETTLEMENT_ENTITY_TYPE = ddlSettlementType.SelectedValue;
                HR_SETTLEMENTS.SETTLEMENT_ENTITY_ID = ddlSettlementId.SelectedValue;
                HR_SETTLEMENTS.SETTLEMENT_AMOUNT = CommonUtils.ConvertStringToDecimal(txtSettlementAmount.Text);
                HR_SETTLEMENTS.SETTLEMENT_REMARKS = txtRemarks.Text;
                HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER1 = txtAdditionalDetails1.Text;
                HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER2 = txtAdditionalDetails2.Text;
                HR_SETTLEMENTS.SEETLEMENT_ADD_IDENTIFIER3 = txtAdditionalDetails3.Text;
                HR_SETTLEMENTS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                HR_SETTLEMENTS.SETTLEMENT_TYPE = ddlType.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_SETTLEMENTS.MODIFIED_BY = this.LoggedUserName;
                    HR_SETTLEMENTS.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    HR_SETTLEMENTS.SETTLEMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_098.ToString(), false, true);
                    HR_SETTLEMENTS.CREATED_BY = this.LoggedUserName;
                    HR_SETTLEMENTS.CREATED_DATE = DateTime.Today;
                }

                HR_SETTLEMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_SETTLEMENTS.SETTLEMENT_ID);


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_SETTLEMENTS>(HR_SETTLEMENTS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<HR_SETTLEMENTS>(HR_SETTLEMENTS, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    if (ddlType.SelectedValue.ToString().ToUpper() == "FULL")
                    {
                        DBMethod.ExecuteNonQuery(LoanRequest_DAL.UpdateLoanRepay(txtSettlementDate.Text, ddlSettlementId.SelectedValue, "F"));
                    }
                    else if (ddlType.SelectedValue.ToString().ToUpper() == "PARTIAL")
                    {
                        DataTable dtData = new DataTable();
                        decimal totAmt = 0;
                        decimal diffAmt = 0;
                        decimal paidAmt = 0;
                        dtData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetRepayPlan(ddlSettlementId.SelectedValue)).Tables[0];

                        if (dtData.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtData.Rows.Count; i++)
                            {
                                if (totAmt < CommonUtils.ConvertStringToDecimal(txtSettlementAmount.Text))
                                {
                                    diffAmt = CommonUtils.ConvertStringToDecimal(txtSettlementAmount.Text) - totAmt;

                                    if (diffAmt < CommonUtils.ConvertStringToDecimal(dtData.Rows[i]["REPAY_AMT"].ToString()))
                                    {
                                        DBMethod.ExecuteNonQuery(LoanRequest_DAL.UpdateLoanRepay(txtSettlementDate.Text, ddlSettlementId.SelectedValue, "P", Math.Abs(diffAmt), (dtData.Rows[i]["REPAY_ID"].ToString())));
                                    }
                                    else if (diffAmt <= CommonUtils.ConvertStringToDecimal(txtSettlementAmount.Text))
                                    {
                                        DBMethod.ExecuteNonQuery(LoanRequest_DAL.UpdateLoanRepay(txtSettlementDate.Text, ddlSettlementId.SelectedValue, "P", CommonUtils.ConvertStringToDecimal(dtData.Rows[i]["REPAY_AMT"].ToString()), (dtData.Rows[i]["REPAY_ID"].ToString())));
                                        paidAmt = CommonUtils.ConvertStringToDecimal(txtSettlementAmount.Text) - CommonUtils.ConvertStringToDecimal(dtData.Rows[i]["REPAY_AMT"].ToString());
                                    }

                                    totAmt = decimal.Parse(dtData.Rows[i]["REPAY_AMT"].ToString()) + totAmt;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void ddlSettlementType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                LoanRequest_BLL.GetLoanDetails(ref ddlSettlementId, ddlSettlementType.SelectedValue,ddlEmployee.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue.ToString().ToUpper() == "FULL")
            {
                txtSettlementAmount.Enabled = false;
                txtSettlementAmount.Text = txtBalanceAmount.Text;
                txtSettlementAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtSettlementAmount.Text);
            }
            else
            {
                txtSettlementAmount.Text = string.Empty;
                txtSettlementAmount.Enabled = true;
            }
        }
        protected void ddlSettlementId_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLoanAmt();
        }
        private void GetLoanAmt()
        {
            if (ddlSettlementType.SelectedValue != string.Empty && ddlSettlementId.SelectedValue != string.Empty)
            {
                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoanDetail(ddlSettlementType.SelectedValue, ddlSettlementId.SelectedValue)).Tables[0];
             
                
                if (dtData.Rows.Count > 0)
                {
                    txtAmount.Text = CommonUtils.ConvertStringToDecimal(dtData.Rows[0]["req_amount"].ToString()).ToString();
                    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                }
                else
                {
                    txtAmount.Text = string.Empty;
                }

                DataTable dtData1 = new DataTable();
                dtData1 = DBMethod.ExecuteQuery(LoanRequest_DAL.GetBalanceAmt(ddlSettlementId.SelectedValue)).Tables[0];
              
                if (dtData1.Rows.Count > 0)
                {
                    txtBalanceAmount.Text = CommonUtils.ConvertStringToDecimal(dtData1.Rows[0]["repay_amt"].ToString()).ToString();
                    txtBalanceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtBalanceAmount.Text);
                }
                else
                {
                    txtBalanceAmount.Text = string.Empty;
                }
            }
        }
        #endregion
    }
}