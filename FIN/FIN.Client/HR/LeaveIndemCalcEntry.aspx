﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveIndemCalcEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveIndemCalcEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="Div1">
                Indem Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:TextBox ID="txtIndemName" MaxLength = "100" TabIndex = "1" runat="server" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblFinancialYear">
                Indem Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlIndemType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                   TabIndex="2" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="Div2">
                Provision
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:CheckBox ID="chkProvision" TabIndex = "3" runat="server" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="500px" DataKeyNames="INDEM_ID,LOOKUP_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <%--<asp:TemplateField Visible="false" HeaderText="Indem Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIndemType" MaxLength="10" Enabled="false" Width="95px" runat="server" CssClass="txtBox" TabIndex="2"
                                Text='<%# Eval("INDEM_TYPE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIndemType" TabIndex="2" Enabled="false" MaxLength="10" Width="95px" runat="server"
                                CssClass="EntryFont txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblIndemType" runat="server" Text='<%# Eval("INDEM_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Low Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIndemLowValue" MaxLength="12" Width="95px" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="4" Text='<%# Eval("INDEM_LOW_VALUE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FTLV" runat="server" ValidChars="" FilterType="Numbers,Custom"
                                TargetControlID="txtIndemLowValue" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIndemLowValue" TabIndex="4" MaxLength="10" Width="95px" runat="server"
                                CssClass="RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtIndemLowValue" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblIndemLowValue" runat="server" Text='<%# Eval("INDEM_LOW_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="High Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIndemHighValue" MaxLength="13" Width="95px" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="5" Text='<%# Eval("INDEM_HIGH_VALUE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FTHV" runat="server" ValidChars="" FilterType="Numbers,Custom"
                                TargetControlID="txtIndemHighValue" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIndemHighValue" TabIndex="5" MaxLength="10" Width="95px" runat="server"
                                CssClass="RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FTHV2" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="txtIndemHighValue" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblIndemHighValue" runat="server" Text='<%# Eval("INDEM_HIGH_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Days">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIndemVal" MaxLength="14" Width="95px" runat="server" CssClass="EntryFont txtBox_N"
                                TabIndex="6" Text='<%# Eval("INDEM_VALUE") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtIndemVal" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIndemVal" TabIndex="6" MaxLength="10" Width="95px" runat="server"
                                CssClass="EntryFont  txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtIndemVal" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblval" runat="server" Text='<%# Eval("INDEM_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtIndemRemarks" TabIndex="15" MaxLength="100" Width="95px" runat="server"
                                Text='<%# Eval("INDEM_REMARKS") %>' CssClass="EntryFont  txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtIndemRemarks" TabIndex="7" MaxLength="100" Width="95px" runat="server"
                                CssClass="EntryFont  txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblIndemRemarks" runat="server" Text='<%# Eval("INDEM_REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" Width="98%" Enabled = "true" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="16" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" Width="98%" Enabled = "true" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="10" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="11" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="17" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="18" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
<script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
