﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class LeaveIndemCalcEntry : PageBase
    {
        HR_INDEM_CALC_SLAB hR_INDEM_CALC_SLAB = new HR_INDEM_CALC_SLAB();
        DataTable dtGridData = new DataTable();

        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LICE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlIndemType, "INDEM_TYPE");
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveIndemCalc_DAL.GetIndemDetails(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_INDEM_CALC_SLAB> userCtx = new DataRepository<HR_INDEM_CALC_SLAB>())
                    {
                        hR_INDEM_CALC_SLAB = userCtx.Find(r =>
                            (r.INDEM_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_INDEM_CALC_SLAB;
                    ddlIndemType.SelectedValue = hR_INDEM_CALC_SLAB.INDEM_TYPE;
                    txtIndemName.Text = hR_INDEM_CALC_SLAB.INDEM_NAME;
                    chkProvision.Checked = false;
                    if (hR_INDEM_CALC_SLAB.ATTRIBUTE1 == FINAppConstants.Y)
                    {
                        chkProvision.Checked = true;
                    }
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LICE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_INDEM_CALC_SLAB = new HR_INDEM_CALC_SLAB();
                    if (dtGridData.Rows[iLoop]["INDEM_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_INDEM_CALC_SLAB> userCtx = new DataRepository<HR_INDEM_CALC_SLAB>())
                        {
                            hR_INDEM_CALC_SLAB = userCtx.Find(r =>
                                (r.INDEM_ID == dtGridData.Rows[iLoop]["INDEM_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_INDEM_CALC_SLAB.INDEM_NAME = txtIndemName.Text;
                    //hR_INDEM_CALC_SLAB.INDEM_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString();
                    hR_INDEM_CALC_SLAB.INDEM_TYPE = ddlIndemType.SelectedValue.ToString();

                    if (chkProvision.Checked)
                    {
                        hR_INDEM_CALC_SLAB.ATTRIBUTE1 = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_INDEM_CALC_SLAB.ATTRIBUTE1 = FINAppConstants.N;
                    }
                    if (dtGridData.Rows[iLoop]["INDEM_HIGH_VALUE"] != DBNull.Value)
                    {
                        hR_INDEM_CALC_SLAB.INDEM_HIGH_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["INDEM_HIGH_VALUE"].ToString());
                    }
                    else
                    {
                        hR_INDEM_CALC_SLAB.INDEM_HIGH_VALUE = null;
                    }
                    if (dtGridData.Rows[iLoop]["INDEM_LOW_VALUE"] != DBNull.Value)
                    {
                        hR_INDEM_CALC_SLAB.INDEM_LOW_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["INDEM_LOW_VALUE"].ToString());
                    }
                    else
                    {
                        hR_INDEM_CALC_SLAB.INDEM_LOW_VALUE = null;
                    }
                    if (dtGridData.Rows[iLoop]["INDEM_VALUE"] != DBNull.Value)
                    {
                        hR_INDEM_CALC_SLAB.INDEM_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["INDEM_VALUE"].ToString());
                    }
                    else
                    {
                        hR_INDEM_CALC_SLAB.INDEM_VALUE = null;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_INDEM_CALC_SLAB.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_INDEM_CALC_SLAB.ENABLED_FLAG = FINAppConstants.N;
                    }

                    hR_INDEM_CALC_SLAB.INDEM_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    hR_INDEM_CALC_SLAB.INDEM_REMARKS = dtGridData.Rows[iLoop]["INDEM_REMARKS"].ToString();

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(hR_INDEM_CALC_SLAB, "D"));
                    }
                    else
                    {
                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Category_DAL.GetSPFOR_ERR_MGR_CATEGORY(hR_INDEM_CALC_SLAB.CATEGORY_CODE, hR_INDEM_CALC_SLAB.CATEGORY_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("CATEGORY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["INDEM_ID"].ToString() != "0")
                        {
                            hR_INDEM_CALC_SLAB.INDEM_ID = dtGridData.Rows[iLoop][FINColumnConstants.INDEM_ID].ToString();
                            hR_INDEM_CALC_SLAB.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDEM_CALC_SLAB.INDEM_ID);
                            hR_INDEM_CALC_SLAB.MODIFIED_BY = this.LoggedUserName;
                            hR_INDEM_CALC_SLAB.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_INDEM_CALC_SLAB>(hR_INDEM_CALC_SLAB, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));
                            savedBool = true;

                        }
                        else
                        {

                            hR_INDEM_CALC_SLAB.INDEM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_100.ToString(), false, true);
                            hR_INDEM_CALC_SLAB.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDEM_CALC_SLAB.INDEM_ID);
                            hR_INDEM_CALC_SLAB.CREATED_BY = this.LoggedUserName;
                            hR_INDEM_CALC_SLAB.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_INDEM_CALC_SLAB>(hR_INDEM_CALC_SLAB);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LICE_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LICE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtIndemHighValue = gvr.FindControl("txtIndemHighValue") as TextBox;
            TextBox txtIndemLowValue = gvr.FindControl("txtIndemLowValue") as TextBox;
            TextBox txtIndemVal = gvr.FindControl("txtIndemVal") as TextBox;
            TextBox txtIndemRemarks = gvr.FindControl("txtIndemRemarks") as TextBox;

            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INDEM_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;
            if (txtIndemHighValue.Text.ToString().Trim().Length == 0)
            {
                txtIndemHighValue.Text = "999";
            }


            slControls[0] = txtIndemLowValue;
            slControls[1] = txtIndemVal;

            strCtrlTypes += FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            strMessage += " ~ " + Prop_File_Data["Low_Value_P"] + " ~ " + Prop_File_Data["Value_P"];



            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            if (decimal.Parse(txtIndemLowValue.Text) > decimal.Parse(txtIndemHighValue.Text))
            {
                ErrorCollection.Add("checkValue", "High Value must be greater than Low Value");
                return drList;
            }

            bool bol_ValuLimit = true;
            double dbl_StartValue = 0;
            double dbl_EndValue = 0;
            for (int gLoop = 0; gLoop < dt_tmp.Rows.Count; gLoop++)
            {
                dbl_StartValue = double.Parse(dt_tmp.Rows[gLoop]["INDEM_LOW_VALUE"].ToString());
                dbl_EndValue = double.Parse(dt_tmp.Rows[gLoop]["INDEM_HIGH_VALUE"].ToString());


                if (((double.Parse(txtIndemLowValue.Text) > dbl_StartValue) && (int.Parse(txtIndemLowValue.Text) < dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }
                if (((double.Parse(txtIndemHighValue.Text) > dbl_StartValue) && (int.Parse(txtIndemHighValue.Text) < dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }

                if (((double.Parse(txtIndemLowValue.Text) < dbl_StartValue) && (int.Parse(txtIndemHighValue.Text) > dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }
            }

            if (!bol_ValuLimit)
            {
                ErrorCollection.Add("InvalidValueLimit", "Invalid Value Limit ");
                return drList;
            }

            if (txtIndemHighValue.Text != "")
            {
                drList["INDEM_HIGH_VALUE"] = txtIndemHighValue.Text;
            }
            else
            {
                drList["INDEM_HIGH_VALUE"] = DBNull.Value;
            }
            if (txtIndemLowValue.Text != "")
            {
                drList["INDEM_LOW_VALUE"] = txtIndemLowValue.Text;
            }
            else
            {
                drList["INDEM_LOW_VALUE"] = DBNull.Value;
            }
            if (txtIndemVal.Text != "")
            {

                drList["INDEM_VALUE"] = txtIndemVal.Text;
            }
            else
            {
                drList["INDEM_VALUE"] = DBNull.Value;
            }

            drList["INDEM_REMARKS"] = txtIndemRemarks.Text;

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    ddlIndemType.SelectedValue = hR_INDEM_CALC_SLAB.INDEM_TYPE;
                //}
                //TextBox txtIndemType = tmpgvr.FindControl("txtIndemType") as TextBox;

                //if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                //{
                //    txtIndemType.Text = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlIndemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtIndemData = new DataTable();
            dtIndemData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveIndemCalc_DAL.GetIndemDetailsBasedOnType(ddlIndemType.SelectedValue.ToString())).Tables[0];
            BindGrid(dtIndemData);
        }

    }
}