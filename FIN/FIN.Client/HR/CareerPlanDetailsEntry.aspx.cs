﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class CareerPlanDetailsEntry : PageBase
    {
        HR_CAREER_PLAN_HDR hR_CAREER_PLAN_HDR = new HR_CAREER_PLAN_HDR();
        HR_CAREER_PLAN_DTL hR_CAREER_PLAN_DTL = new HR_CAREER_PLAN_DTL();
        DataTable dtGridData = new DataTable();
        string career_path_type;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            CareerPlanDtls_BLL.GetDepartmentName(ref ddlDept);
            
            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = FIN.BLL.HR.CareerPlanDtls_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_CAREER_PLAN_HDR> userCtx = new DataRepository<HR_CAREER_PLAN_HDR>())
                    {
                        hR_CAREER_PLAN_HDR = userCtx.Find(r =>
                            (r.PLAN_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_CAREER_PLAN_HDR;
                    ddlDept.SelectedValue = hR_CAREER_PLAN_HDR.PLAN_DEPT_ID.ToString();
                    fn_fill_Designation();
                    ddlDesignation.SelectedValue = hR_CAREER_PLAN_HDR.PLAN_DESIG_ID.ToString();
                    fn_fill_careerPath();
                    ddlCareerPathName.SelectedValue = hR_CAREER_PLAN_HDR.PATH_DTL_ID.ToString();
                    txtDesc.Text = hR_CAREER_PLAN_HDR.PLAN_DESC;
                    txtStartDate.Text = DBMethod.ConvertDateToString(hR_CAREER_PLAN_HDR.PLAN_EFFECTIVE_FROM_DT.ToString());

                    if (hR_CAREER_PLAN_HDR.PLAN_EFFECTIVE_TO_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(hR_CAREER_PLAN_HDR.PLAN_EFFECTIVE_TO_DT.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = false;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlProfileId = tmpgvr.FindControl("ddlProfileId") as DropDownList;
                DropDownList ddlProfiledtl = tmpgvr.FindControl("ddlProfiledtl") as DropDownList;
                FIN.BLL.HR.CareerPlanDtls_BLL.GetEmployeeProfile(ref ddlProfileId);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlProfileId.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PROF_ID"].ToString();
                    fillprofdtl(tmpgvr);
                    ddlProfiledtl.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PROF_DTL_ID"].ToString();
                    filldesigdtl(tmpgvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlDept;
                slControls[1] = ddlDesignation;
                slControls[2] = ddlCareerPathName;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Designation_P"] + " ~ " + Prop_File_Data["Career_Path_P"] +  "";
                //string strMessage = "Department ~ Designation ~ Career Path ";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Profile Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    //txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_CAREER_PLAN_HDR = (HR_CAREER_PLAN_HDR)EntityData;
                }

                hR_CAREER_PLAN_HDR.PLAN_DEPT_ID = ddlDept.SelectedValue.ToString();
                hR_CAREER_PLAN_HDR.PLAN_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                hR_CAREER_PLAN_HDR.PATH_DTL_ID = ddlCareerPathName.SelectedValue.ToString();
                
                fn_get_path_hdr();
                DataTable dt_career_path_type = new DataTable();
                dt_career_path_type = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetCareerPathType(ddlCareerPathName.SelectedValue.ToString())).Tables[0];
                if (dt_career_path_type != null)
                {
                    if (dt_career_path_type.Rows.Count > 0)
                    {
                        career_path_type = dt_career_path_type.Rows[0][0].ToString();
                    }
                }
                hR_CAREER_PLAN_HDR.PLAN_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_CAREER_PLAN_HDR.PLAN_TYPE = career_path_type;
                hR_CAREER_PLAN_HDR.PLAN_DESC = txtDesc.Text.ToString();
                hR_CAREER_PLAN_HDR.PLAN_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                if (txtEndDate.Text != string.Empty)
                {
                    hR_CAREER_PLAN_HDR.PLAN_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                hR_CAREER_PLAN_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_CAREER_PLAN_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_CAREER_PLAN_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_CAREER_PLAN_HDR.PLAN_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_074_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    hR_CAREER_PLAN_HDR.CREATED_BY = this.LoggedUserName;
                    hR_CAREER_PLAN_HDR.CREATED_DATE = DateTime.Today;
                }

                hR_CAREER_PLAN_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_CAREER_PLAN_HDR.PLAN_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_CAREER_PLAN_DTL = new HR_CAREER_PLAN_DTL();
                    if (dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_CAREER_PLAN_DTL> userCtx = new DataRepository<HR_CAREER_PLAN_DTL>())
                        {
                            hR_CAREER_PLAN_DTL = userCtx.Find(r =>
                                (r.PLAN_DTL_ID == dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_CAREER_PLAN_DTL.PLAN_PROFILE_HDR_ID = dtGridData.Rows[iLoop]["PROF_ID"].ToString();
                    hR_CAREER_PLAN_DTL.PLAN_PROFILE_DTL_ID = dtGridData.Rows[iLoop]["PROF_DTL_ID"].ToString();
                    hR_CAREER_PLAN_DTL.PLAN_PROFILE_MIN_VALUE = short.Parse(dtGridData.Rows[iLoop]["PLAN_PROFILE_MIN_VALUE"].ToString());
                    hR_CAREER_PLAN_DTL.PLAN_PROFILE_MAX_VALUE = short.Parse(dtGridData.Rows[iLoop]["PLAN_PROFILE_MAX_VALUE"].ToString());
                    hR_CAREER_PLAN_DTL.PLAN_PROFILE_REQUIRED_VALUE = short.Parse(dtGridData.Rows[iLoop]["PLAN_PROFILE_REQUIRED_VALUE"].ToString());

                    hR_CAREER_PLAN_DTL.PLAN_HDR_ID = hR_CAREER_PLAN_HDR.PLAN_HDR_ID;
                    hR_CAREER_PLAN_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == "TRUE")
                    {
                        hR_CAREER_PLAN_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_CAREER_PLAN_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "D"));
                    //}
                    //else
                    //{
                    if (dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString() != string.Empty)
                    {
                        hR_CAREER_PLAN_DTL.PLAN_DTL_ID = dtGridData.Rows[iLoop]["PLAN_DTL_ID"].ToString();
                        hR_CAREER_PLAN_DTL.MODIFIED_BY = this.LoggedUserName;
                        hR_CAREER_PLAN_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(hR_CAREER_PLAN_DTL, "U"));
                    }
                    else
                    {
                        hR_CAREER_PLAN_DTL.PLAN_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_074_D".ToString(), false, true);
                        hR_CAREER_PLAN_DTL.CREATED_BY = this.LoggedUserName;
                        hR_CAREER_PLAN_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(hR_CAREER_PLAN_DTL, "A"));
                    }
                    //}
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_CAREER_PLAN_HDR, HR_CAREER_PLAN_DTL>(hR_CAREER_PLAN_HDR, tmpChildEntity, hR_CAREER_PLAN_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_CAREER_PLAN_HDR, HR_CAREER_PLAN_DTL>(hR_CAREER_PLAN_HDR, tmpChildEntity, hR_CAREER_PLAN_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void fn_get_path_hdr()
        {
            DataTable dt_careerPth_hdr = new DataTable();
                dt_careerPth_hdr = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetCareerPathHdr(ddlCareerPathName.SelectedValue.ToString())).Tables[0];
                if (dt_careerPth_hdr != null)
                {
                    if (dt_careerPth_hdr.Rows.Count > 0)
                    {
                        hR_CAREER_PLAN_HDR.PATH_HDR_ID = dt_careerPth_hdr.Rows[0][0].ToString();
                    }
                }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Designation();
        }
        private void fn_fill_Designation()
        {
            AssetIssue_BLL.GetDesignationName(ref ddlDesignation, ddlDept.SelectedValue.ToString());
            
        }
        

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<HR_CAREER_PLAN_DTL>(hR_CAREER_PLAN_DTL);
                }
                DBMethod.DeleteEntity<HR_CAREER_PLAN_HDR>(hR_CAREER_PLAN_HDR);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlProfileId = gvr.FindControl("ddlProfileId") as DropDownList;
            DropDownList ddlProfiledtl = gvr.FindControl("ddlProfiledtl") as DropDownList;
            TextBox txtMinValue = gvr.FindControl("txtMinValue") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtMaxValue = gvr.FindControl("txtMaxValue") as TextBox;
            TextBox txtReqValue = gvr.FindControl("txtReqValue") as TextBox;
            CheckBox chkact = gvr.FindControl("chkActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PLAN_DTL_ID"] = "0";
                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlProfileId;
            slControls[1] = ddlProfiledtl;
            slControls[2] = txtMinValue;
            slControls[3] = txtMaxValue;
            slControls[4] = txtReqValue;
            
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~TextBox~TextBox~TextBox";
            string strMessage = Prop_File_Data["Profile_Id_P"] + " ~ " + "Profile Details " + "~" + Prop_File_Data["Minimum_Value_P"] + " ~ " + Prop_File_Data["Maximum_Value_P"] + " ~ " + Prop_File_Data["Required_Value_P"] + "";
           // string strMessage = " Profile Id ~ Minimum Value ~ Maximum Value ~ Required Value";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode,Master.StrRecordId, ddlDept.SelectedValue,ddlDesignation.SelectedValue,ddlCareerPathName.SelectedValue);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("CAREERPLAN", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }


            drList["PROF_ID"] = ddlProfileId.SelectedValue;
            drList["PROF_NAME"] = ddlProfileId.SelectedItem.Text;
            drList["PROF_DTL_ID"] = ddlProfiledtl.SelectedValue;
            drList["COM_LEVEL_DESC"] = ddlProfiledtl.SelectedItem.Text;
            drList["COM_REMARKS"] = txtDescription.Text;
            drList["PLAN_PROFILE_MIN_VALUE"] = txtMinValue.Text;
            drList["PLAN_PROFILE_MAX_VALUE"] = txtMaxValue.Text;
            drList["PLAN_PROFILE_REQUIRED_VALUE"] = txtReqValue.Text;
            if (chkact.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlProfileId_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillprofdtl(gvr);
        }

        private void fillprofdtl(GridViewRow gvr)
        {
            
            DropDownList ddlProfileId = gvr.FindControl("ddlProfileId") as DropDownList;
            DropDownList ddlProfiledtl = gvr.FindControl("ddlProfiledtl") as DropDownList;
            CareerPlanDtls_BLL.GetEmployeeProfile_Dtls(ref ddlProfiledtl, ddlProfileId.SelectedValue.ToString()); 
        }

        protected void ddlProfiledtl_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            filldesigdtl(gvr);
                //FIN.DAL.HR.CareerPlanDtls_DAL.GetDesignationName(ddlProfileId.SelectedValue.ToString());
            //fn_fill_designation_Name();
        }

        private void filldesigdtl(GridViewRow gvr)
        {
            DataTable dt_get_desig_name = new DataTable();
            
            DropDownList ddlProfiledtl = gvr.FindControl("ddlProfiledtl") as DropDownList;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            dt_get_desig_name = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetDesignationName(ddlProfiledtl.SelectedValue.ToString())).Tables[0];
            if (dt_get_desig_name != null)
            {
                if (dt_get_desig_name.Rows.Count > 0)
                {
                    txtDescription.Text = dt_get_desig_name.Rows[0][0].ToString();
                }
            }
        }

        protected void ddlCareerPathName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_careerPath();
        }



        private void fn_fill_careerPath()
        {
            CareerPlanDtls_BLL.GetCareerPath(ref ddlCareerPathName, ddlDept.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
        }
    }
}