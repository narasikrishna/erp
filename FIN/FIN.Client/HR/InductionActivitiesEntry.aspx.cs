﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class InductionActivitiesEntry : PageBase
    {

        HR_INDUCTION_ACTIVITIES hR_INDUCTION_ACTIVITIES = new HR_INDUCTION_ACTIVITIES();
        DataTable dtGridData = new DataTable();
        Induction_BLL Induction_BLL = new Induction_BLL();
        string ProReturn = null;

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>



        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.HR.Induction_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_CATEGORIES> userCtx = new DataRepository<HR_CATEGORIES>())
                //    {
                //        hR_CATEGORIES = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_CATEGORIES;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_INDUCTION_ACTIVITIES = new HR_INDUCTION_ACTIVITIES();
                    if (dtGridData.Rows[iLoop]["HR_IND_ACT_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_INDUCTION_ACTIVITIES> userCtx = new DataRepository<HR_INDUCTION_ACTIVITIES>())
                        {
                            hR_INDUCTION_ACTIVITIES = userCtx.Find(r =>
                                (r.HR_IND_ACT_ID == dtGridData.Rows[iLoop]["HR_IND_ACT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_INDUCTION_ACTIVITIES.HR_ACTIVITY_DESC = dtGridData.Rows[iLoop]["HR_ACTIVITY_DESC"].ToString();
                    if (dtGridData.Rows[iLoop]["HR_EFF_FROM_DT"] != DBNull.Value)
                    {
                        hR_INDUCTION_ACTIVITIES.HR_EFF_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["HR_EFF_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["HR_EFF_TO_DT"] != DBNull.Value)
                    {
                        hR_INDUCTION_ACTIVITIES.HR_EFF_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["HR_EFF_TO_DT"].ToString());
                    }
                   

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_INDUCTION_ACTIVITIES.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_INDUCTION_ACTIVITIES.ENABLED_FLAG = FINAppConstants.N;
                    }

                    //  hR_DEPARTMENTS.WORKFLOW_COMPLETION_STATUS = "1";

                    hR_INDUCTION_ACTIVITIES.HR_IND_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_INDUCTION_ACTIVITIES, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Department_DAL.GetSPFOR_DUPLICATE_CHECK(hR_DEPARTMENTS.DEPT_ID, hR_DEPARTMENTS.DEPT_TYPE, hR_DEPARTMENTS.DEPT_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("DEPARTMENT", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["HR_IND_ACT_ID"].ToString() != "0")
                        {
                            hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID = dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_ACT_ID].ToString();
                            hR_INDUCTION_ACTIVITIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID);
                            hR_INDUCTION_ACTIVITIES.MODIFIED_BY = this.LoggedUserName;
                            hR_INDUCTION_ACTIVITIES.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_INDUCTION_ACTIVITIES>(hR_INDUCTION_ACTIVITIES, true);
                            savedBool = true;
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID = FINSP.GetSPFOR_SEQCode("HR_116".ToString(), false, true);
                            hR_INDUCTION_ACTIVITIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID);
                            hR_INDUCTION_ACTIVITIES.CREATED_BY = this.LoggedUserName;
                            hR_INDUCTION_ACTIVITIES.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_INDUCTION_ACTIVITIES>(hR_INDUCTION_ACTIVITIES);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }

                }
                //VMVServices.Web.Utils.SavedRecordId = "";

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                //ErrorCollection.Clear();

                //DropDownList ddldeptTyp = tmpgvr.FindControl("ddldeptTyp") as DropDownList;
                //DropDownList ddlCostValue = tmpgvr.FindControl("ddlCostValue") as DropDownList;
                //DropDownList ddlCostSeg = tmpgvr.FindControl("ddlCostSeg") as DropDownList;

                //Lookup_BLL.GetLookUpValues(ref ddldeptTyp, "DEPT_TYP");
                //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentName(ref ddlCostSeg);
                ////  FIN.BLL.GL.Segments_BLL(ref ddlCostValue);

                //if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                //{
                //    ddldeptTyp.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VALUE_KEY_ID].ToString();
                //    ddlCostSeg.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SEGMENT_ID].ToString();
                //    fillsegmentvalue(tmpgvr);
                //    ddlCostValue.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SEGMENT_VALUE_ID].ToString();

                //}
                //    CheckBox chkact = tmpgvr.FindControl("chkact") as CheckBox;
                //    DataTable dtchilcat = new DataTable();
                //    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //    {
                //        hR_DEPARTMENTS.DEPT_ID = dtGridData.Rows[iLoop][FINColumnConstants.DEPT_ID].ToString();
                //    }
                //    dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.Get_Childdataof_Department(hR_DEPARTMENTS.DEPT_ID)).Tables[0];
                //    if (dtchilcat.Rows.Count > 0)
                //    {
                //        chkact.Enabled = false;
                //    }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Induction Activity");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Depat_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtActivity = gvr.FindControl("txtActivity") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            //TextBox txtdeptNameAR = gvr.FindControl("txtdeptNameAR") as TextBox;

            //CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["HR_IND_ACT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtActivity;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpEndDate;
            //slControls[1] = ddldeptTyp;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_TIME;
            string strMessage = Prop_File_Data["Activity_P"] + " ~ " + "Effective From Date" + " ~ " + " Effective From Date " + " ~ " + " Effective To Date " + "";
            //string strMessage = "Department Name ~ Department Type";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "HR_ACTIVITY_DESC='" + txtActivity.Text.Trim().ToUpper().Replace("'", "''") + "'";
            strMessage = FINMessageConstatns.DepartmentName;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            DataTable dtchildDept = new DataTable();
            dtchildDept = null;

            //if (gvData.EditIndex >= 0)
            //{
            //    hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.HR_IND_ACT_ID].ToString();

            //    dtchildDept = null;
            //    dtchildDept = DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.Get_Childdataof_Department(hR_DEPARTMENTS.DEPT_ID)).Tables[0];
            //    if (dtchildDept.Rows.Count > 0)
            //    {
            //        if (chkact.Checked)
            //        {


            //        }
            //        else
            //        {
            //            ErrorCollection.Add("chkflg", Prop_File_Data["chkflg_P"]);

            //            return drList;
            //        }
            //    }
            //}
            //hR_INDUCTION_ACTIVITIES.HR_EFF_TO_DT

            drList["HR_ACTIVITY_DESC"] = txtActivity.Text;
            //drList["DEPT_NAME_OL"] = txtdeptNameAR.Text;

           
            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["HR_EFF_FROM_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["HR_EFF_TO_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["HR_EFF_TO_DT"] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    //if (e.Row.RowType == DataControlRowType.DataRow)
                    //{
                    //    e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;

                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_INDUCTION_ACTIVITIES.HR_IND_ACT_ID = dtGridData.Rows[iLoop]["HR_IND_ACT_ID"].ToString();
                    DBMethod.DeleteEntity<HR_INDUCTION_ACTIVITIES>(hR_INDUCTION_ACTIVITIES);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DEPT_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

      


    }
}