﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using FIN.DAL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class RepayPlanEntry : PageBase
    {
        HR_ADVANCE_REQ hR_ADVANCE_REQ = new HR_ADVANCE_REQ();
        HR_ADVANCE_REPAY_PLAN hR_ADVANCE_REPAY_PLAN = new HR_ADVANCE_REPAY_PLAN();
        DataTable dtGridData = new DataTable();
        DataTable dt_dept_designData = new DataTable();
        DataTable dt_employee_name = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        RepayPlan_BLL RepayPlan_BLL = new RepayPlan_BLL();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;



                txtRequestDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_ADVANCE_REQ> userCtx = new DataRepository<HR_ADVANCE_REQ>())
                    {
                        hR_ADVANCE_REQ = userCtx.Find(r =>
                            (r.REQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //hR_ADVANCE_REQ = AccountingGroupLinks_BLL.getClassEntity(Master.StrRecordId);



                    EntityData = hR_ADVANCE_REQ;


                    ddlEmployeeName.SelectedValue = hR_ADVANCE_REQ.REQ_EMP_ID;
                    fillreqNo();
                    ddlReqNo.SelectedValue = hR_ADVANCE_REQ.REQ_ID;
                    FillLineNumberDetails();
                    txtRequestDate.Text = DBMethod.ConvertDateToString(hR_ADVANCE_REQ.REQ_DT.ToString());
                    // ddldept.Text = hR_ADVANCE_REQ.REQ_DEPT_ID;
                    //FILLDESIG();
                    //  txtype.Text = hR_ADVANCE_REQ.REQ_ADVANCE_TYPE;
                    //  //ddldesig.Text = hR_ADVANCE_REQ.REQ_DESIG_ID;
                    //  txtAmount.Text = hR_ADVANCE_REQ.REQ_AMOUNT.ToString();

                    dtGridData = FIN.BLL.HR.RepayPlan_BLL.getRepayPlanDetailsDetails_edit(Master.StrRecordId);
                    BindGrid(dtGridData);

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                // ErrorCollection.Clear();
                // if (EntityData != null)
                // {
                //     hR_ADVANCE_REQ = (HR_ADVANCE_REQ)EntityData;
                // }

                // hR_ADVANCE_REQ.REQ_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                //// hR_ADVANCE_REQ.REQ_ID = ddlRequestNumber.SelectedValue.ToString();
                // hR_ADVANCE_REQ.REQ_DT = DBMethod.ConvertStringToDate(txtRequestDate.Text.ToString());
                // hR_ADVANCE_REQ.REQ_DEPT_ID = ddldept.SelectedValue;
                // hR_ADVANCE_REQ.REQ_ADVANCE_TYPE = txtype.Text;
                // hR_ADVANCE_REQ.REQ_DESIG_ID = ddldesig.SelectedValue;
                // hR_ADVANCE_REQ.REQ_AMOUNT = int.Parse(txtAmount.Text.ToString());
                // hR_ADVANCE_REQ.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                // if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                // {
                //     hR_ADVANCE_REQ.MODIFIED_BY = this.LoggedUserName;
                //     hR_ADVANCE_REQ.MODIFIED_DATE = DateTime.Today;

                // }
                // else
                // {
                //     hR_ADVANCE_REQ.REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_005_M.ToString(), false, true);

                //     hR_ADVANCE_REQ.CREATED_BY = this.LoggedUserName;
                //     hR_ADVANCE_REQ.CREATED_DATE = DateTime.Today;

                // }
                // hR_ADVANCE_REQ.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_ADVANCE_REQ.REQ_ID);

                //Save Detail Table


                //if (Session[FINSessionConstants.GridData] != null)
                //{
                //    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                //}

                //ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Repay Plan");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    hR_ADVANCE_REPAY_PLAN = new HR_ADVANCE_REPAY_PLAN();
                    if (gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString() != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString() != String.Empty)
                    {
                        using (IRepository<HR_ADVANCE_REPAY_PLAN> userCtx = new DataRepository<HR_ADVANCE_REPAY_PLAN>())
                        {
                            hR_ADVANCE_REPAY_PLAN = userCtx.Find(r =>
                                (r.REPAY_ID == gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_ADVANCE_REPAY_PLAN.RES_ID = ddlReqNo.SelectedValue.ToString();
                    //(dtGridData.Rows[iLoop][FINColumnConstants.REQ_ID].ToString());
                    //hR_ADVANCE_REQ.REQ_ID;
                    TextBox txtRepayAmount = gvData.Rows[iLoop].FindControl("txtRepayAmount") as TextBox;
                    hR_ADVANCE_REPAY_PLAN.REPAY_AMT = CommonUtils.ConvertStringToDecimal(txtRepayAmount.Text.ToString());

                    hR_ADVANCE_REPAY_PLAN.INSTALLMENT_NO = Convert.ToInt32(gvData.DataKeys[iLoop].Values[FINColumnConstants.INSTALLMENT_NO].ToString());

                    TextBox txtComments = gvData.Rows[iLoop].FindControl("txtComments") as TextBox;
                    hR_ADVANCE_REPAY_PLAN.REPAY_COMMENTS = txtComments.Text.ToString();

                    //hR_ADVANCE_REPAY_PLAN.REPAY_ID = (dtGridData.Rows[iLoop][FINColumnConstants.REPAY_ID].ToString());


                    TextBox dtpRepayDate = gvData.Rows[iLoop].FindControl("dtpRepayDate") as TextBox;
                    if (dtpRepayDate.Text.ToString().Length > 0)
                    {

                        hR_ADVANCE_REPAY_PLAN.REPAY_DT = DBMethod.ConvertStringToDate (dtpRepayDate.Text.ToString());
                    }
                    else
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_DT = null;
                    }


                    TextBox dtpActualDate = gvData.Rows[iLoop].FindControl("dtpActualDate") as TextBox;
                    if (dtpActualDate.Text.ToString().Length > 0)
                    {

                        hR_ADVANCE_REPAY_PLAN.REPAY_ACTUAL_DT = DBMethod.ConvertStringToDate(dtpActualDate.Text.ToString());
                    }
                    else
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_ACTUAL_DT = null;
                    }

                    CheckBox chkPaid = gvData.Rows[iLoop].FindControl("chkPaid") as CheckBox;
                    if (chkPaid.Checked)
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_PAIDYN = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_PAIDYN = FINAppConstants.N;
                    }

                    hR_ADVANCE_REPAY_PLAN.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    hR_ADVANCE_REPAY_PLAN.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    if (gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString() != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString() != String.Empty)
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_ID = gvData.DataKeys[iLoop].Values[FINColumnConstants.REPAY_ID].ToString();
                        hR_ADVANCE_REPAY_PLAN.MODIFIED_DATE = DateTime.Today;
                        hR_ADVANCE_REPAY_PLAN.MODIFIED_BY = this.LoggedUserName;
                        DBMethod.SaveEntity<HR_ADVANCE_REPAY_PLAN>(hR_ADVANCE_REPAY_PLAN, true);
                        savedBool = true;
                    }
                    else
                    {
                        hR_ADVANCE_REPAY_PLAN.REPAY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_035_D.ToString(), false, true);
                        hR_ADVANCE_REPAY_PLAN.CREATED_BY = this.LoggedUserName;
                        hR_ADVANCE_REPAY_PLAN.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_ADVANCE_REPAY_PLAN>(hR_ADVANCE_REPAY_PLAN);
                        savedBool = true;
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    savedBool = false;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            Employee_BLL.GetEmpName(ref ddlEmployeeName);
            //  Department_BLL.GetDepartmentName(ref ddldept);
        }



        protected void ddlRequestNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLineNumberDetails();
        }


        private void FillLineNumberDetails()
        {

            //dtGridData = FIN.BLL.HR.RepayPlan_BLL.getRepayPlanDetailsDetails(ddlReqNo.SelectedValue.ToString());
            dtGridData = FIN.BLL.HR.RepayPlan_BLL.getRepayPlanDetailsDetails_edit(ddlReqNo.SelectedValue.ToString());
            BindGrid(dtGridData);

            dt_dept_designData = DBMethod.ExecuteQuery(RepayPlan_DAL.GetDept_design_amt(ddlReqNo.SelectedValue.ToString())).Tables[0];

            if (dt_dept_designData != null)
            {
                if (dt_dept_designData.Rows.Count > 0)
                {
                    txtDept.Text = dt_dept_designData.Rows[0]["DEPT_NAME"].ToString();
                    txtDesign.Text = dt_dept_designData.Rows[0]["DESIG_NAME"].ToString();
                    txtAmount.Text = dt_dept_designData.Rows[0]["REQ_AMOUNT"].ToString();
                    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                    txtype.Text = dt_dept_designData.Rows[0]["REQ_ADVANCE_TYPE"].ToString();
                }
            }
            //DataTable dtgrndata = new DataTable();

            //    dtgrndata =   DBMethod.ExecuteQuery(FIN.DAL.HR.RepayPlan_DAL.GetRepayDetailsBasedReq(ddlRequestNumber.SelectedValue.ToString())).Tables[0];
            //    txtRequestDate.Text = DateTime.Parse(dtgrndata.Rows[0]["REQ_DT"].ToString()).ToString("dd/MM/yyyy");
            //    txtDepartment.Text = dtgrndata.Rows[0]["REQ_DEPT_ID"].ToString();
            //    txtype.Text = dtgrndata.Rows[0]["REQ_ADVANCE_TYPE"].ToString();
            //    txtDesignation.Text = dtgrndata.Rows[0]["REQ_DESIG_ID"].ToString();
            //    txtAmount.Text = dtgrndata.Rows[0]["REQ_AMOUNT"].ToString();

        }



        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("REPAY_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("REPAY_AMT"))));   
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["REPAY_PAIDYN"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;

             
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Repay Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void gvAdditional_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        CheckBox chkPaid = e.Row.FindControl("chkPaid") as CheckBox;
                        if (chkPaid.Checked == true)
                        {
                            chkPaid.Enabled = false;
                        }
                        else
                        {
                            chkPaid.Enabled = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        //# region Grid Events
        ///// <summary>
        ///// The GridView control is entering Canceling mode
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        //protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    if (Session[FINSessionConstants.GridData] != null)
        //    {
        //        dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //    }
        //    gvData.EditIndex = -1;

        //    BindGrid(dtGridData);

        //}
        //#endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        //protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {

        //        ErrorCollection.Clear();
        //        GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
        //        DataRow drList = null;
        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }

        //        if (e.CommandName.Equals("FooterInsert"))
        //        {
        //            gvr = gvData.FooterRow;
        //            if (gvr == null)
        //            {
        //                return;
        //            }
        //        }


        //        if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
        //        {
        //            drList = AssignToGridControl(gvr, dtGridData, "A", 0);
        //            if (ErrorCollection.Count > 0)
        //            {
        //                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
        //                return;
        //            }
        //            dtGridData.Rows.Add(drList);
        //            BindGrid(dtGridData);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("CalendarEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        ///// <summary>
        ///// To assign the controls to grid view
        ///// </summary>
        ///// <param name="tmpdrlist">Datarow details</param>
        ///// <param name="tmpgvr">Grid view objects</param>
        ///// <returns></returns>

        //private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        //{


        //    System.Collections.SortedList slControls = new System.Collections.SortedList();
        //    TextBox txtNo = gvr.FindControl("txtNo") as TextBox;
        //    TextBox txtRepayAmount = gvr.FindControl("txtRepayAmount") as TextBox;
        //    TextBox dtpRepayDate = gvr.FindControl("dtpRepayDate") as TextBox;
        //    TextBox dtpActualDate = gvr.FindControl("dtpActualDate") as TextBox;
        //    CheckBox chkPaid = gvr.FindControl("chkPaid") as CheckBox;
        //    TextBox txtComments = gvr.FindControl("txtComments") as TextBox;

        //    DataRow drList;
        //    DataTable dt_tmp = tmpdtGridData.Copy();
        //    if (GMode == "A")
        //    {
        //        drList = dtGridData.NewRow();
        //        drList[FINColumnConstants.REPAY_ID] = "0";
        //    }
        //    else
        //    {
        //        drList = dtGridData.Rows[rowindex];
        //        dt_tmp.Rows.RemoveAt(rowindex);

        //    }

        //    slControls[0] = txtNo;
        //    slControls[1] = txtRepayAmount;
        //    slControls[2] = dtpRepayDate;
        //    slControls[3] = dtpActualDate;
        //    slControls[4] = chkPaid;
        //    slControls[5] = txtComments;


        //    ErrorCollection.Clear();
        //    string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_TIME + " ~ " + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

        //    string strMessage = " No ~ Repay Amount ~ repay Date ~ Actual Date ~ Paid ~ Comments ";

        //    EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

        //    if (EmptyErrorCollection.Count > 0)
        //    {
        //        ErrorCollection = EmptyErrorCollection;
        //        return drList;
        //    }


        //    //ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_StartDate.Text), DBMethod.ConvertStringToDate(dtp_EndDate.Text), Master.Mode);
        //    //if (ErrorCollection.Count > 0)
        //    //    return drList;


        //    string strCondition = "INSTALLMENT_NO='" + txtNo.Text + "'";
        //    strMessage = FINMessageConstatns.RecordAlreadyExists;
        //    ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
        //    if (ErrorCollection.Count > 0)
        //    {
        //        return drList;
        //    }

        //    //drList[FINColumnConstants.RES_ID] = hR_ADVANCE_REQ.REQ_ID;
        //    drList[FINColumnConstants.INSTALLMENT_NO] = txtNo.Text;
        //    drList[FINColumnConstants.REPAY_AMT] = txtRepayAmount.Text;
        //    drList[FINColumnConstants.REPAY_COMMENTS] = txtComments.Text;


        //    if (dtpRepayDate.Text.ToString().Length > 0)
        //    {
        //        drList[FINColumnConstants.REPAY_DT] = DBMethod.ConvertStringToDate(dtpRepayDate.Text.ToString());
        //    }
        //    else
        //    {
        //        drList[FINColumnConstants.REPAY_DT] = DBNull.Value;
        //    }



        //    if (dtpActualDate.Text.ToString().Length > 0)
        //    {

        //        drList[FINColumnConstants.REPAY_ACTUAL_DT] = DBMethod.ConvertStringToDate(dtpActualDate.Text.ToString());
        //    }
        //    else
        //    {
        //        drList[FINColumnConstants.REPAY_ACTUAL_DT] = DBNull.Value;
        //    }


        //    if (chkPaid.Checked)
        //    {
        //        drList[FINColumnConstants.REPAY_PAIDYN] = "TRUE";
        //    }
        //    else
        //    {
        //        drList[FINColumnConstants.REPAY_PAIDYN] = "FALSE";
        //    }

        //    return drList;

        //}

        ////public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        ////{
        ////    try
        ////    {
        ////        // SortedList ErrorCollection = new SortedList();
        ////        if (dtGridData.Select(strCondition).Length > 0)
        ////        {
        ////            ErrorCollection.Add(strMessage, strMessage);
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
        ////    }
        ////    finally
        ////    {
        ////        if (ErrorCollection.Count > 0)
        ////        {
        ////            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        ////            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        ////        }
        ////    }
        ////    //return ErrorCollection;
        ////}

        ///// <summary>
        /////  The GridView control is entering row updating mode  
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
        //        DataRow drList = null;

        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        if (gvr == null)
        //        {
        //            return;
        //        }

        //        drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
        //            return;
        //        }
        //        gvData.EditIndex = -1;
        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Repay Plan", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        //protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        DataRow drList = null;
        //        drList = dtGridData.Rows[e.RowIndex];
        //       // drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Repay Plan", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        ///// <summary>
        /////  The GridView control is entering edit mode
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        //protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        gvData.EditIndex = e.NewEditIndex;
        //        BindGrid(dtGridData);
        //        GridViewRow gvr = gvData.Rows[e.NewEditIndex];

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Repay Plan", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}

        ///// <summary>
        ///// The GridView control is entering row created mode
        ///// To identify rowtype and created a row in the grid view control       
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        //protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (e.Row.RowType == DataControlRowType.EmptyDataRow)
        //        {
        //            GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        //            gvData.Controls[0].Controls.AddAt(0, gvr);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Repay Plan", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }

        //}


        ///// <summary>
        ///// The GridView control is entering edit mode
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>

        //protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            if (bol_rowVisiable)
        //                e.Row.Visible = false;

        //            //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
        //            //{
        //            //    e.Row.Visible = false;
        //            //}

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Repay Plan", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }

        //}



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                DBMethod.DeleteEntity<HR_ADVANCE_REQ>(hR_ADVANCE_REQ);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Repay Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillreqNo();
            DataTable dtEmpDetails = new DataTable();
            dtEmpDetails = DBMethod.ExecuteQuery(Employee_DAL.getWorkDetails(ddlEmployeeName.SelectedValue.ToString())).Tables[0];
            if (dtEmpDetails != null)
            {
                if (dtEmpDetails.Rows.Count > 0)
                {
                    txtDept.Text = dtEmpDetails.Rows[0]["DEPT_NAME"].ToString();
                    txtDesign.Text = dtEmpDetails.Rows[0]["DESIG_NAME"].ToString();
                }
            }
        }

        private void fillreqNo()
        {
            RepayPlan_BLL.fn_LoanReqId(ref ddlReqNo, ddlEmployeeName.SelectedValue.ToString());
        }


        protected void ddlReqNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    //  dtGridData = DBMethod.ExecuteQuery(RepayPlan_DAL.GetRepayDetailsBasedReq(ddlRequestNumber.SelectedValue.ToString())).Tables[0];
                    FillLineNumberDetails();
                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }

            }

        }




    }
        #endregion
}