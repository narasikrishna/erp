﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeTrainingRequestEntry : PageBase
    {
        HR_EMP_TRN_REQ hR_EMP_TRN_REQ = new HR_EMP_TRN_REQ();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_TRN_REQ> userCtx = new DataRepository<HR_EMP_TRN_REQ>())
                    {
                        hR_EMP_TRN_REQ = userCtx.Find(r =>
                            (r.TRN_REQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_TRN_REQ;

                    ddlEmp.SelectedValue = hR_EMP_TRN_REQ.EMP_ID;

                    if (hR_EMP_TRN_REQ.TRN_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_EMP_TRN_REQ.TRN_FROM_DT.ToString());
                    }
                    if (hR_EMP_TRN_REQ.TRN_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_EMP_TRN_REQ.TRN_TO_DT.ToString());
                    }
                    if (hR_EMP_TRN_REQ.TRAINING_TYPE != null)
                    {
                        txtTrainingTyp.Text = hR_EMP_TRN_REQ.TRAINING_TYPE;
                    }

                    if (hR_EMP_TRN_REQ.ENABLED_FLAG == "1")
                    {
                        chkAct.Checked = true;
                    }
                    else
                    {
                        chkAct.Checked = false;
                    }



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeId(ref ddlEmp);
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_TRN_REQ = (HR_EMP_TRN_REQ)EntityData;
                }



                hR_EMP_TRN_REQ.EMP_ID = ddlEmp.SelectedValue;
                if (txtFromDate.Text != string.Empty)
                {
                    hR_EMP_TRN_REQ.TRN_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                else
                {
                    hR_EMP_TRN_REQ.TRN_FROM_DT = null;
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_EMP_TRN_REQ.TRN_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                else
                {
                    hR_EMP_TRN_REQ.TRN_TO_DT = null;
                }
                if (txtTrainingTyp.Text != string.Empty)
                {
                    hR_EMP_TRN_REQ.TRAINING_TYPE = txtTrainingTyp.Text;
                }

                if (chkAct.Checked == true)
                {
                    hR_EMP_TRN_REQ.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_EMP_TRN_REQ.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }


                hR_EMP_TRN_REQ.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_TRN_REQ.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_TRN_REQ.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_TRN_REQ.TRN_REQ_ID = FINSP.GetSPFOR_SEQCode("HR_113".ToString(), false, true);
                    hR_EMP_TRN_REQ.CREATED_BY = this.LoggedUserName;
                    hR_EMP_TRN_REQ.CREATED_DATE = DateTime.Today;
                }
                hR_EMP_TRN_REQ.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_TRN_REQ.TRN_REQ_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_TRN_REQ>(hR_EMP_TRN_REQ);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_TRN_REQ>(hR_EMP_TRN_REQ, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_TRN_REQ>(hR_EMP_TRN_REQ);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillTrainingSchDtl();
        }


        private void fillTrainingSchDtl()
        {
            DataTable dtTrnSchdtl = new DataTable();

            dtTrnSchdtl = DBMethod.ExecuteQuery(EmpTrainingReq_DAL.GetTrainingSchDtl(ddlEmp.SelectedValue)).Tables[0];
            if (dtTrnSchdtl.Rows.Count > 0)
            {
                txtFromDate.Text = DBMethod.ConvertDateToString(dtTrnSchdtl.Rows[0]["TRN_FROM_DT"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtTrnSchdtl.Rows[0]["TRN_TO_DT"].ToString());
                txtTrainingTyp.Text = dtTrnSchdtl.Rows[0]["TRAINING_TYPE"].ToString();

            }
            else
            {
                txtFromDate.Text = "";
                txtToDate.Text = "";
                txtTrainingTyp.Text = "";
            }

        }



    }
}