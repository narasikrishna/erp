﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
namespace FIN.Client.HR
{
    public partial class Dashboard_HR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_DEPT_GRAP"] = null;
                Session["EMP_DESIG_GRAP"] = null;
                DisplayEmployeeDetails();

                FillComboBox();

                getEmpCount4FinYear();

                getEmployeeAgeDetails();

                getHolidayDetails();

                getEmployeePermitDetails();

                getEmployeeDistrubutionList();

                getEmployeeleaveDetails();
                LoadGraph();
            }
            else
            {
                LoadGraph();
            }
        }

        private void LoadGraph()
        {
            //if (Session["EMP_COUNT"] != null)
            //{
            //    chrt_empCount.DataSource = (DataTable)Session["EMP_COUNT"];
            //    chrt_empCount.DataBind();
            //}
            //if (Session["EMP_DEPT_GRAP"] != null)
            //{

            //    chrt_Dept.DataSource = (DataTable)Session["EMP_DEPT_GRAP"];
            //    chrt_Dept.Series["S_Dept_Count"].XValueMember = "DEPT_NAME";
            //    chrt_Dept.Series["S_Dept_Count"].YValueMembers = "DEPT_COUNT";
            //    chrt_Dept.DataBind();
               

            //}
            //if (Session["EMP_DESIG_GRAP"] != null)
            //{
            //    chrt_Desig.Visible = true ;
            //    //chrt_Desig.DataSource = (DataTable)Session["EMP_DESIG_GRAP"];
            //    //chrt_Desig.DataBind();
            //    chrt_Dept.DataSource = (DataTable)Session["EMP_DESIG_GRAP"];
            //    chrt_Dept.Series["S_Dept_Count"].XValueMember = "DESIG_NAME";
            //    chrt_Dept.Series["S_Dept_Count"].YValueMembers = "DESIG_COUNT   ";
            //    chrt_Dept.DataBind();
               
            //}
            //else
            //{
            //    chrt_Desig.Visible = false;
            //}

            //if (Session["EMP_AGE"] != null)
            //{
            //    chrtEmpAge.DataSource = (DataTable)Session["EMP_AGE"];
            //    chrtEmpAge.DataBind();
            //}
            //if (Session["EMP_DIST_LIST"] != null)
            //{
            //    chrt_emp_expire.DataSource = (DataTable)Session["EMP_DIST_LIST"];
            //    chrt_emp_expire.DataBind();
            //}
            //if (Session["EMP_LD"] != null)
            //{
            //    chrt_LD.DataSource = (DataTable)Session["EMP_LD"];
            //    chrt_LD.DataBind();
            //}
        }

        private void getEmployeeleaveDetails()
        {
            DataTable dt_Emp_LD = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeLeaveDetails(ddlLAPeriod.SelectedValue)).Tables[0];
            Session["EMP_LD"] = dt_Emp_LD;
            LoadGraph();
            chrt_LD.DataSource = (DataTable)Session["EMP_LD"];
            chrt_LD.DataBind();
        }
        private void getEmployeeDistrubutionList()
        {
            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployDistributionList()).Tables[0];
            Session["EMP_DIST_LIST"] = dt_Emp_Dist_list;
            chrt_emp_expire.DataSource = (DataTable)Session["EMP_DIST_LIST"];
            chrt_emp_expire.DataBind();
        }

        private void getEmployeePermitDetails()
        {
            if (ddlPerFinPeriod.SelectedValue.ToString().Trim().Length > 0)
            {
                DataTable dt_PermitDet = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeePermitDetails_DAL.getEmployeePermitExpiry4Period(ddlPerFinPeriod.SelectedValue)).Tables[0];
                if (dt_PermitDet.Rows.Count > 0)
                {
                    gvPermitGrid.DataSource = dt_PermitDet;
                    gvPermitGrid.DataBind();
                }
            }
        }

        private void getHolidayDetails()
        {
            DataTable dt_Holiday = DBMethod.ExecuteQuery(FIN.DAL.HR.HolidayMaster_DAL.GetHolodayDetails_finyr(ddlHolFinYear.SelectedValue)).Tables[0];
            gvHolidayDet.DataSource = dt_Holiday;
            gvHolidayDet.DataBind();
        }
        private void getEmployeeAgeDetails()
        {
            DataTable dt_empAge = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployAgeDetails()).Tables[0];
            Session["EMP_AGE"] = dt_empAge;
            chrtEmpAge.DataSource = (DataTable)Session["EMP_AGE"];
            chrtEmpAge.DataBind();
            
        }
        private void getEmpCount4FinYear()
        {
            DataTable dt_emp_count = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmpCount4FinYear(ddlFinYear.SelectedValue)).Tables[0];
            Session["EMP_COUNT"] = dt_emp_count;
            LoadGraph();
            chrt_empCount.DataSource = (DataTable)Session["EMP_COUNT"];
            chrt_empCount.DataBind();
        }


        private void FillComboBox()
        {
            ddlPerFinYear.Visible = false;
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinYear);
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            if (ddlFinYear.Items.Count > 0)
            {
                //ddlFinYear.SelectedIndex = ddlFinYear.Items.Count - 1;
                ddlFinYear.SelectedValue = str_finyear;
            }

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlPerFinYear);
            if (ddlPerFinYear.Items.Count > 0)
            {
               // ddlPerFinYear.SelectedIndex = ddlPerFinYear.Items.Count - 1;
                ddlPerFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddlPerFinPeriod, ddlPerFinYear.SelectedValue);
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            if (dt_cur_period.Rows.Count > 0)
            {
                ddlPerFinPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlHolFinYear);
            if (ddlHolFinYear.Items.Count > 0)
            {
                //ddlHolFinYear.SelectedIndex = ddlHolFinYear.Items.Count - 1;
                ddlHolFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlLAFinYear);
            if (ddlLAFinYear.Items.Count > 0)
            {
                //ddlLAFinYear.SelectedIndex = ddlLAFinYear.Items.Count - 1;
                ddlLAFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlLAPeriod, ddlLAFinYear.SelectedValue);          
            if (dt_cur_period.Rows.Count > 0)
            {
                ddlLAPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

        }
        private void DisplayEmployeeDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeDetails4Dashbord()).Tables[0];
            Session["EMPDET"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "DEPT_ID", "DEPT_NAME");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("DEPT_ID");
                dt_Dept_List.Columns.Add("DEPT_NAME");
                dt_Dept_List.Columns.Add("DEPT_COUNT");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["DEPT_ID"] = dt_Dist_DEPT.Rows[iLoop]["DEPT_ID"].ToString();
                    dr["DEPT_NAME"] = dt_Dist_DEPT.Rows[iLoop]["DEPT_NAME"].ToString();
                    DataRow[] dr_count = dt_EmpDet.Select("DEPT_ID='" + dt_Dist_DEPT.Rows[iLoop]["DEPT_ID"].ToString() + "'");
                    dr["DEPT_COUNT"] = dr_count.Length;
                    dt_Dept_List.Rows.Add(dr);
                }
                gvDeptDet.DataSource = dt_Dept_List;
                gvDeptDet.DataBind();
                Session["EMP_DEPT_GRAP"] = dt_Dept_List;

                LoadGraph();


                chrt_Dept.DataSource = (DataTable)Session["EMP_DEPT_GRAP"];
                chrt_Dept.Series["S_Dept_Count"].XValueMember = "DEPT_NAME";
                chrt_Dept.Series["S_Dept_Count"].YValueMembers = "DEPT_COUNT";
                chrt_Dept.DataBind();

                // chrt_Dept.Series["S_Dept_Count"].XValueMember = "DEPT_COUNT";
                // chrt_Dept.Series["S_Dept_Count"].YValueMembers = "DEPT_COUNT";


                //var query = from row in dt_EmpDet.AsEnumerable()
                //            group row by row.Field<string>("DEPT_ID") into sales
                //            orderby sales.Key
                //            select new
                //            {
                //                Name = sales.Key,
                //                CountOfClients = sales.Count()
                //            };

                //List<DataTable> tables = dt_EmpDet.AsEnumerable()
                //           .GroupBy(row => new
                //           {
                //               Email = row.Field<string>("DEPT_ID"),
                //               Name = row.Field<string>("DEPT_NAME")
                //           }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();




            }

        }


        protected void lnk_Dept_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadDesigDet(gvr);
        }

        private void LoadDesigDet(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["EMPDET"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["DEPT_ID"].ToString().Contains(gvDeptDet.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString()))
                       .GroupBy(row => new
                       {
                           Email = row.Field<string>("DEPT_ID"),
                           Name = row.Field<string>("DEPT_NAME")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_Desig = tables[0].DefaultView.ToTable(true, "DEPT_DESIG_ID", "DESIG_NAME");

            DataTable dt_Desig_List = new DataTable();
            dt_Desig_List.Columns.Add("DEPT_ID");
            dt_Desig_List.Columns.Add("DEPT_DESIG_ID");
            dt_Desig_List.Columns.Add("DESIG_NAME");
            dt_Desig_List.Columns.Add("DESIG_COUNT");

            for (int iLoop = 0; iLoop < dt_Dist_Desig.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Desig_List.NewRow();
                dr["DEPT_ID"] = gvDeptDet.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString();
                dr["DEPT_DESIG_ID"] = dt_Dist_Desig.Rows[iLoop]["DEPT_DESIG_ID"].ToString();
                dr["DESIG_NAME"] = dt_Dist_Desig.Rows[iLoop]["DESIG_NAME"].ToString();
                DataRow[] dr_count = dt_EmpDet.Select("DEPT_DESIG_ID='" + dt_Dist_Desig.Rows[iLoop]["DEPT_DESIG_ID"].ToString() + "'");
                dr["DESIG_COUNT"] = dr_count.Length;
                dt_Desig_List.Rows.Add(dr);
            }

            gvDesigDet.DataSource = dt_Desig_List;
            gvDesigDet.DataBind();
           // divEmpDesig.Visible = true;
           // divEmpDept.Visible = false;
            Session["EMP_DESIG_GRAP"] = dt_Desig_List;

            LoadGraph();


            chrt_Dept.DataSource = (DataTable)Session["EMP_DESIG_GRAP"];
            chrt_Dept.Series["S_Dept_Count"].XValueMember = "DESIG_NAME";
            chrt_Dept.Series["S_Dept_Count"].YValueMembers = "DESIG_COUNT   ";
            chrt_Dept.DataBind();

        }





        protected void imgBack_Click(object sender, ImageClickEventArgs e)
        {
            divEmpDept.Visible = true;
            divEmpDesig.Visible = false;
            Session["EMP_DESIG_GRAP"] = null;
            LoadGraph();
        }

        protected void imgEmp_Click_Click(object sender, ImageClickEventArgs e)
        {
            divEmpDept.Visible = false;
            divEmpDesig.Visible = true;
            divEmpDet.Visible = false;

            LoadGraph();
        }


        protected void lnk_Desig_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadEMPDet(gvr);
        }

        private void LoadEMPDet(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["EMPDET"];
            var tmp_menulist = dt_EmpDet.AsEnumerable()
                       .Where(r => r["DEPT_ID"].ToString().Contains(gvDesigDet.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString()) && r["DEPT_DESIG_ID"].ToString() == gvDesigDet.DataKeys[gvr.RowIndex].Values["DEPT_DESIG_ID"].ToString())
                       ;
            DataTable dt_Employee_Det = new DataTable();
            if (tmp_menulist.Any())
            {
                dt_Employee_Det = System.Data.DataTableExtensions.CopyToDataTable(tmp_menulist);
            }

            gvEmpDet.DataSource = dt_Employee_Det;
            gvEmpDet.DataBind();
            divEmpDesig.Visible = false;
            divEmpDept.Visible = false;
            divEmpDet.Visible = true;


            LoadGraph();

        }

        protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmpCount4FinYear();

        }

        protected void ddlHolFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getHolidayDetails();

        }

        protected void ddlPerFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlPerFinPeriod, ddlPerFinYear.SelectedValue);
            LoadGraph();
        }

        protected void ddlPerFinPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeePermitDetails();
            LoadGraph();
        }

        protected void lnk_Permit_Dept_Name_Click(object sender, EventArgs e)
        {
             GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
             getPermitExpiryEmpDetails(gvr);
        }
        private void getPermitExpiryEmpDetails(GridViewRow gvr)
        {
            DataTable dt_empDet = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeePermitDetails_DAL.getEmployeePermitExpiry4PeriodDept(ddlPerFinPeriod.SelectedValue, gvPermitGrid.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString())).Tables[0];
            divempPermitGrid.Visible = true;
            divpermitGrid.Visible = false;
            gvEmpPermitDet.DataSource = dt_empDet;
            gvEmpPermitDet.DataBind();
        }

        protected void img_emp_Permit_Det_Click(object sender, ImageClickEventArgs e)
        {
            divempPermitGrid.Visible = false;
            divpermitGrid.Visible = true;
        }

        protected void ddlLAFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlLAPeriod, ddlPerFinYear.SelectedValue);
            LoadGraph();
           
        }

        protected void ddlLAPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeeleaveDetails();
        }
    }
}