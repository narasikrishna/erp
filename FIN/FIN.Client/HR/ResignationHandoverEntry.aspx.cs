﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class ResignationHandoverEntry : PageBase
    {

        HR_HANDOVER_HDR hR_HANDOVER_HDR = new HR_HANDOVER_HDR();
        HR_HANDOVER_DTL hR_HANDOVER_DTL = new HR_HANDOVER_DTL();

        ResignationHandover_BLL ResignationHandover_BLL = new ResignationHandover_BLL();
        string ProReturn = null;
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            ResignationHandover_BLL.fn_GetResigRequestdtls(ref ddlRequestNumber);
            Lookup_BLL.GetLookUpValues(ref ddlHOStatus, "HNDOVRSTS");
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtReason.Enabled = false;
                txtRemarks.Enabled = false;
                txtRequestDate.Enabled = false;
                txtDepartmentName.Enabled = false;
                txtEmployeeName.Enabled = false;

                FillComboBox();
                EntityData = null;

                txtRequestDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtHandoverDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.ResignationHandover_BLL.getChildEntityDet(Master.StrRecordId);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_HANDOVER_HDR> userCtx = new DataRepository<HR_HANDOVER_HDR>())
                    {
                        hR_HANDOVER_HDR = userCtx.Find(r =>
                            (r.HO_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_HANDOVER_HDR;
                    
                    ddlRequestNumber.SelectedValue = hR_HANDOVER_HDR.RESIG_HDR_ID;
                    FillRequsetdtls();
                    txtHandoverDate.Text = DBMethod.ConvertDateToString(hR_HANDOVER_HDR.HO_DATE.ToString());
                    ddlHOStatus.SelectedValue = hR_HANDOVER_HDR.HO_STATUS;
                    ddlEmployeeName.SelectedValue = hR_HANDOVER_HDR.ATTRIBUTE1;
                    //if (dtGridData != null)
                    //{
                    //    if (dtGridData.Rows.Count > 0)
                    //    {
                    //        ddlEmployeeName.SelectedValue = dtGridData.Rows[0]["HO_TO"].ToString();
                    //    }
                    //}

                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_HANDOVER_HDR = (HR_HANDOVER_HDR)EntityData;
                }

                hR_HANDOVER_HDR.RESIG_HDR_ID = ddlRequestNumber.SelectedValue;
                hR_HANDOVER_HDR.HO_DATE = DBMethod.ConvertStringToDate(txtHandoverDate.Text.ToString());
                hR_HANDOVER_HDR.HO_STATUS = ddlHOStatus.SelectedValue;
                hR_HANDOVER_HDR.HO_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_HANDOVER_HDR.ATTRIBUTE1 = ddlEmployeeName.SelectedValue;
                hR_HANDOVER_HDR.ENABLED_FLAG = FINAppConstants.Y;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_HANDOVER_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_HANDOVER_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_HANDOVER_HDR.HO_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_044_M".ToString(), false, true);

                    hR_HANDOVER_HDR.CREATED_BY = this.LoggedUserName;
                    hR_HANDOVER_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_HANDOVER_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_HANDOVER_HDR.HO_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (dtGridData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        hR_HANDOVER_DTL = new HR_HANDOVER_DTL();
                        if (gvData.DataKeys[iLoop].Values["HO_DTL_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_HANDOVER_DTL> userCtx = new DataRepository<HR_HANDOVER_DTL>())
                            {
                                hR_HANDOVER_DTL = userCtx.Find(r =>
                                    (r.HO_DTL_ID == gvData.DataKeys[iLoop].Values["HO_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtremarks = (TextBox)gvData.Rows[iLoop].FindControl("txtremarks");
                        DropDownList ddlHandoverto = (DropDownList)gvData.Rows[iLoop].FindControl("ddlHandoverto");
                        CheckBox chkAct = (CheckBox)gvData.Rows[iLoop].FindControl("chkAct");

                        hR_HANDOVER_DTL.HO_LINE_NUM = (iLoop + 1);// int.Parse(dtGridData.Rows[iLoop]["HO_LINE_NUM"].ToString());
                        hR_HANDOVER_DTL.HO_TYPE = gvData.DataKeys[iLoop].Values["HO_TYPE"].ToString();
                        //  hR_HANDOVER_DTL.HO_REMARKS = gvData.DataKeys[iLoop].Values["HO_REMARKS"].ToString();
                        if (txtremarks.Text != "")
                        {
                            hR_HANDOVER_DTL.HO_REMARKS = txtremarks.Text;
                        }
                        else
                        {
                            hR_HANDOVER_DTL.HO_REMARKS = null;
                        }

                        hR_HANDOVER_DTL.ASSET_DTL_ID = gvData.DataKeys[iLoop].Values["ASSET_DTL_ID"].ToString();

                        hR_HANDOVER_DTL.HO_HDR_ID = hR_HANDOVER_HDR.HO_HDR_ID;

                        hR_HANDOVER_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                        hR_HANDOVER_DTL.HO_TO = ddlHandoverto.SelectedValue.ToString();

                        // hR_HANDOVER_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        //  hR_HANDOVER_DTL.ENABLED_FLAG = chk.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                        //if (gvData.DataKeys[iLoop].Values["ENABLED_FLAG"].ToString().ToUpper() == "TRUE")
                        //{
                        //    hR_HANDOVER_DTL.ENABLED_FLAG = "1";
                        //}
                        //else
                        //{
                        //    hR_HANDOVER_DTL.ENABLED_FLAG = "0";
                        //}
                        if (chkAct.Checked == true)
                        {
                            hR_HANDOVER_DTL.ENABLED_FLAG = "1";
                        }
                        else
                        {
                            hR_HANDOVER_DTL.ENABLED_FLAG = "0";
                        }

                        if (gvData.DataKeys[iLoop].Values[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(hR_HANDOVER_DTL, "D"));
                        }
                        else
                        {

                            ProReturn = FIN.DAL.HR.ResignationHandover_DAL.GetSPFOR_DUPLICATE_CHECK(ddlRequestNumber.SelectedValue, hR_HANDOVER_HDR.HO_HDR_ID);

                            if (ProReturn != string.Empty)
                            {
                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("RESIGNHANDOVR", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }


                            if (gvData.DataKeys[iLoop].Values["HO_DTL_ID"].ToString() != "0")
                            {
                                // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                                hR_HANDOVER_DTL.MODIFIED_BY = this.LoggedUserName;
                                hR_HANDOVER_DTL.MODIFIED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_HANDOVER_DTL, "U"));

                            }
                            else
                            {

                                hR_HANDOVER_DTL.HO_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_044_D".ToString(), false, true);
                                hR_HANDOVER_DTL.CREATED_BY = this.LoggedUserName;
                                hR_HANDOVER_DTL.CREATED_DATE = DateTime.Today;
                                //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                                tmpChildEntity.Add(new Tuple<object, string>(hR_HANDOVER_DTL, "A"));
                            }
                        }

                    }

                    switch (Master.Mode)
                    {
                        case FINAppConstants.Add:
                            {
                                FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_HANDOVER_HDR, HR_HANDOVER_DTL>(hR_HANDOVER_HDR, tmpChildEntity, hR_HANDOVER_DTL);
                                saveBool = true;
                                break;
                            }
                        case FINAppConstants.Update:
                            {

                                FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_HANDOVER_HDR, HR_HANDOVER_DTL>(hR_HANDOVER_HDR, tmpChildEntity, hR_HANDOVER_DTL, true);
                                saveBool = true;
                                break;

                            }
                    }

                }
                else
                {


                    ProReturn = FIN.DAL.HR.ResignationHandover_DAL.GetSPFOR_DUPLICATE_CHECK(ddlRequestNumber.SelectedValue, hR_HANDOVER_HDR.HO_HDR_ID);

                    if (ProReturn != string.Empty)
                    {
                        if (ProReturn != "0")
                        {
                            ErrorCollection.Add("RESIGNHANDOVR", ProReturn);
                            if (ErrorCollection.Count > 0)
                            {
                                return;
                            }
                        }
                    }

                    switch (Master.Mode)
                    {
                        case FINAppConstants.Add:
                            {
                                DBMethod.SaveEntity<HR_HANDOVER_HDR>(hR_HANDOVER_HDR);
                                saveBool = true;
                                break;
                            }
                        case FINAppConstants.Update:
                            {

                                DBMethod.SaveEntity<HR_HANDOVER_HDR>(hR_HANDOVER_HDR, true);
                                saveBool = true;
                                break;

                            }
                    }
                }
                // Duplicate Validation Through Backend Package PKG_VALIDATIONS







                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (hR_HANDOVER_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.RESIGNATION_HANDOVER_HR_ALERT);
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.RESIGNATION_HANDOVER_EMP_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && saveBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Resignation Handover");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Rh_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        //protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    if (Session[FINSessionConstants.GridData] != null)
        //    {
        //        dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //    }
        //    gvData.EditIndex = -1;

        //    BindGrid(dtGridData);

        //}
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtlineno = gvr.FindControl("txtlineno") as TextBox;
            TextBox txttype = gvr.FindControl("txttype") as TextBox;
            TextBox txtremarks = gvr.FindControl("txtremarks") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["HO_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // slControls[0] = txtlineno;
            //slControls[0] = txttype;
            //slControls[1] = txtremarks;



            //ErrorCollection.Clear();
            //string strCtrlTypes = "TextBox~TextBox~TextBox";
            //string strMessage = Prop_File_Data["Line_Number_P"] + " ~ " + Prop_File_Data["Type_P"] + " ~ " + Prop_File_Data["Remarks_P"] +  "";
            ////string strMessage = "Line Number ~ Type ~ Remarks";

            //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}



            // drList["HO_LINE_NUM"] = decimal.Parse(txtlineno.Text);
            drList["HO_TYPE"] = txttype.Text;
            drList["HO_REMARKS"] = txtremarks.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
        //        DataRow drList = null;

        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        if (gvr == null)
        //        {
        //            return;
        //        }

        //        drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
        //            return;
        //        }
        //        gvData.EditIndex = -1;
        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RH_ROW_UP", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        //protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();


        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        DataRow drList = null;
        //        drList = dtGridData.Rows[e.RowIndex];
        //        drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RH_ROW_DEL", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        ///// <summary>
        /////  The GridView control is entering edit mode
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        //protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        gvData.EditIndex = e.NewEditIndex;
        //        BindGrid(dtGridData);
        //        GridViewRow gvr = gvData.Rows[e.NewEditIndex];
        //        FillFooterGridCombo(gvr);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RH_ROW_EDT", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}

        ///// <summary>
        ///// The GridView control is entering row created mode
        ///// To identify rowtype and created a row in the grid view control       
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        //protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (e.Row.RowType == DataControlRowType.EmptyDataRow)
        //        {
        //            GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        //            gvData.Controls[0].Controls.AddAt(0, gvr);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RH_RowCreated", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }

        //}


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    GridViewRow gvr = gvData.SelectedRow;// (GridViewRow)((Control)e.Row).Parent.Parent;
                    // FillFooterGridCombo(gvr);
                    //DropDownList ddlInterviewAnswer = gvr.FindControl("ddlInterviewAnswer") as DropDownList;
                    DropDownList ddlHandoverto = e.Row.FindControl("ddlHandoverto") as DropDownList;
                    Employee_BLL.GetEmployeeName(ref ddlHandoverto);
                    ddlHandoverto.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.EMP_ID].ToString();

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_HANDOVER_HDR>(hR_HANDOVER_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RH_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        protected void ddlRequestNumber_SelectedIndexChanged1(object sender, EventArgs e)
        {
            FillRequsetdtls();
            // DataTable dtassetdtl = new DataTable();
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationHandover_DAL.getAssetDtl(HfEmpid.Value)).Tables[0];

            BindGrid(dtGridData);
        }


        private void FillRequsetdtls()
        {
            DataTable dtreqdtl = new DataTable();
            dtreqdtl = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationHandover_DAL.getResigReqDtl(ddlRequestNumber.SelectedValue)).Tables[0];
            if (dtreqdtl.Rows.Count > 0)
            {
                txtReason.Text = dtreqdtl.Rows[0]["RESIG_REASON"].ToString();
                txtRemarks.Text = dtreqdtl.Rows[0]["RESIG_REMARKS"].ToString();
                txtRequestDate.Text = DBMethod.ConvertDateToString(dtreqdtl.Rows[0]["RESIG_REQ_DT"].ToString());
                txtDepartmentName.Text = dtreqdtl.Rows[0]["DEPT_NAME"].ToString();
                txtEmployeeName.Text = dtreqdtl.Rows[0]["EMP_FIRST_NAME"].ToString();
                HfEmpid.Value = dtreqdtl.Rows[0]["EMP_ID"].ToString();

            }



        }


    }
}