﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class CategoryEntryNew : PageBase
    {
        HR_CATEGORIES hR_CATEGORIES = new HR_CATEGORIES();
            
        Categories_BLL Categories_BLL = new Categories_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_CATEGORIES> userCtx = new DataRepository<HR_CATEGORIES>())
                    {
                        hR_CATEGORIES = userCtx.Find(r =>
                            (r.CATEGORY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_CATEGORIES;

                    txtcode.Text = hR_CATEGORIES.CATEGORY_CODE;
                    txtcodeAR.Text = hR_CATEGORIES.CATEGORY_CODE_OL;
                    txtdesc.Text = hR_CATEGORIES.CATEGORY_DESC;
                    txtdescAR.Text = hR_CATEGORIES.CATEGORY_DESC_OL;
                    if (hR_CATEGORIES.EFFECTIVE_FROM_DATE != null)
                    {
                        dtpStartDate.Text = DBMethod.ConvertDateToString(hR_CATEGORIES.EFFECTIVE_FROM_DATE.ToString());
                    }
                    if (hR_CATEGORIES.EFFECTIVE_TO_DATE != null)
                    {
                        dtpEndDate.Text = DBMethod.ConvertDateToString(hR_CATEGORIES.EFFECTIVE_TO_DATE.ToString());
                    }

                    if (hR_CATEGORIES.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
           

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_CATEGORIES = (HR_CATEGORIES)EntityData;
                }

                hR_CATEGORIES.CATEGORY_CODE = txtcode.Text;
                hR_CATEGORIES.CATEGORY_CODE_OL = txtcodeAR.Text;
                hR_CATEGORIES.CATEGORY_DESC = txtdesc.Text;
                hR_CATEGORIES.CATEGORY_DESC_OL = txtdescAR.Text;
                if (dtpStartDate.Text != string.Empty)
                {
                    hR_CATEGORIES.EFFECTIVE_FROM_DATE = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
                }

                if (dtpEndDate.Text != string.Empty)
                {
                    hR_CATEGORIES.EFFECTIVE_TO_DATE = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
                }
                else
                {
                    hR_CATEGORIES.EFFECTIVE_TO_DATE = null;
                }


                hR_CATEGORIES.CATEGORY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_CATEGORIES.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_CATEGORIES.MODIFIED_BY = this.LoggedUserName;
                    hR_CATEGORIES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_CATEGORIES.CATEGORY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_001.ToString(), false, true);
                    hR_CATEGORIES.CREATED_BY = this.LoggedUserName;
                    hR_CATEGORIES.CREATED_DATE = DateTime.Today;

                }

                hR_CATEGORIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_CATEGORIES.CATEGORY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


       

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = txtBankNameEn;
                //slControls[1] = txtShortName;


                //ErrorCollection.Clear();
                //string strCtrlTypes = "TextBox~TextBox";

                //string strMessage = "Bank Name ~ Short Name";

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}


                AssignToBE();

                string ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_CATEGORIES.CATEGORY_ID, hR_CATEGORIES.CATEGORY_CODE);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("Category", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_CATEGORIES>(hR_CATEGORIES);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_CATEGORIES>(hR_CATEGORIES, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_CATEGORIES>(hR_CATEGORIES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

      
    }
}