﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveEnhancementEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveEnhancementEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblBankName">
                Transaction Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtTransDate" CssClass="validate[required]  RequiredField txtBox"
                    runat="server" TabIndex="1" Width="144px"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTransDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtTransDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
           
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblBranchName">
                Employee
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
             <div class="lblBox LNOrient" style="  width: 200px" id="lblBankShortName">
                Department
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px">
                <asp:DropDownList ID="ddlDepartment" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Enabled="false" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div6">
                Leave Transaction Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlTransTyp" runat="server" TabIndex="9" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlTransTyp_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblShortName">
                Leave Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlLeave" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlLeave_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:Label ID="lblAvailableLeave" runat="server" Text=" "></asp:Label>
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hf_AvailableLeave" runat="server" />
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblSWIFTCode">
                Leave From
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtLeaveFrom" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtLeaveFrom"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLeaveFrom" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblIFSCCode">
                Leave To
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtLeaveTo" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField txtBox"
                    runat="server" TabIndex="6" OnTextChanged="txtLeaveTo_TextChanged" AutoPostBack="True"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtLeaveTo"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLeaveTo" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblNoofDays">
                Payment Option
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlPaymentOption" runat="server" TabIndex="9" CssClass="ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPaymentOption_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="Div7">
                Leave Transaction ID
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlTransID" runat="server" TabIndex="10" CssClass="ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlTransID_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div runat="server" id="IdPayPrd">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div9">
                Pay Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px">
                <asp:DropDownList ID="ddlPayperiod" runat="server" TabIndex="10" CssClass="validate[required] ddlStype"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div5">
                No of Days
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtNoofdays" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="7" OnTextChanged="txtNoofdays_TextChanged" AutoPostBack="True"
                    Width="144px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtNoofdays" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="Div1">
                Leave Salary
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:TextBox ID="txtLevSal" CssClass="validate[required]  RequiredField txtBox_N"
                    runat="server" TabIndex="8" Width="150px" MaxLength="13" AutoPostBack="true"
                    OnTextChanged="txtLevSal_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtLevSal" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="Div11">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div10">
                Balance Payable
            </div>
            <div class="divtxtBox LNOrient" style="  width: 100px">
                <asp:TextBox ID="txtbalpay" Enabled="false" runat="server" TabIndex="8" Width="150px"
                    MaxLength="13" CssClass="txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtbalpay" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="DivBank">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div8">
                Bank Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 520px">
                <asp:DropDownList ID="ddlBank" runat="server" TabIndex="9" CssClass="ddlStype" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div3">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="  width: 530px">
                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Height="50px" CssClass="validate[]  RequiredField txtBox"
                    runat="server" TabIndex="11"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                Air Ticket
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkAirticket" runat="server" TabIndex="12" Checked="false" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="Div4">
                Ticket Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtTicketAmount" CssClass="txtBox_N"
                    runat="server" TabIndex="13" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtTicketAmount" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="divtxtBox LNOrient" style="  width: 85px">
                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/print-icon.png" ToolTip="Print"
                    OnClick="btnPrint_Click" Width="35px" Height="25px" Style="border: 0px" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient" style="width:410px;">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="410px" DataKeyNames="LOOKUP_ID,DELETED,LEAVE_SAL_ELEMENT_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="true">
                <Columns>
                    <asp:TemplateField HeaderText="Element">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlelement" AutoPostBack="true" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="300px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlelement" AutoPostBack="true" TabIndex="2" runat="server"
                                CssClass="RequiredField EntryFont ddlStype" Width="300px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldeptTyp" CssClass="adminFormFieldHeading" Width="300px" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" Width="100px" MaxLength="100" runat="server" CssClass="  RequiredField txtBox_N"
                                Text='<%# Eval("PAY_AMOUNT") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" Width="100px" MaxLength="100" runat="server" CssClass="  RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="100px" runat="server" Text='<%# Eval("PAY_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="9" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="9" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="9" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width:220px; text-align: right" id="lblInvoiceAmount">
                Total Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 100px">
                <asp:TextBox ID="txtInvoiceAmount" runat="server" TabIndex="10" MaxLength="13" Enabled="false"
                    CssClass="txtBox_N" Width="100px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtInvoiceAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
<script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
