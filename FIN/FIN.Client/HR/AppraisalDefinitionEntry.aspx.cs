﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AppraisalDefinitionEntry : PageBase
    {
        HR_PER_APPRAISAL_DEF hR_PER_APPRAISAL_DEF = new HR_PER_APPRAISAL_DEF();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_PER_APPRAISAL_DEF> userCtx = new DataRepository<HR_PER_APPRAISAL_DEF>())
                    {
                        hR_PER_APPRAISAL_DEF = userCtx.Find(r =>
                            (r.APP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_PER_APPRAISAL_DEF;

                    txtAppraisalCode.Text = hR_PER_APPRAISAL_DEF.APP_CODE;
                    txtPeriod.Text = hR_PER_APPRAISAL_DEF.APP_PERIOD.ToString();
                    txtDescription.Text = hR_PER_APPRAISAL_DEF.APP_DESC;
                    txtDescriptionol.Text = hR_PER_APPRAISAL_DEF.APP_DESC_OL;
                    //txtDesignation.Text = hR_PER_APPRAISAL_DEF..ToString;
                    //txtAmount.Text = hR_PER_APPRAISAL_DEF.REQ_STATUS;


                    if (hR_PER_APPRAISAL_DEF.APP_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_PER_APPRAISAL_DEF.APP_FROM_DT.ToString());
                    }
                    if (hR_PER_APPRAISAL_DEF.APP_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_PER_APPRAISAL_DEF.APP_TO_DT.ToString());
                    }
                    //  hR_PER_APPRAISAL_DEF.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    // hR_PER_APPRAISAL_DEF.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    if (hR_PER_APPRAISAL_DEF.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                   

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_PER_APPRAISAL_DEF = (HR_PER_APPRAISAL_DEF)EntityData;
                }



                hR_PER_APPRAISAL_DEF.APP_CODE = txtAppraisalCode.Text;
                hR_PER_APPRAISAL_DEF.APP_DESC = txtDescription.Text;
                hR_PER_APPRAISAL_DEF.APP_DESC_OL = txtDescriptionol.Text;
                hR_PER_APPRAISAL_DEF.APP_PERIOD = txtPeriod.Text.ToString();
                //hR_PER_APPRAISAL_DEF.REQ_ID = txtDesignation.Text;
                //hR_PER_APPRAISAL_DEF.REQ_ID = txtAmount.Text;


                if (txtFromDate.Text != string.Empty)
                {
                    hR_PER_APPRAISAL_DEF.APP_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_PER_APPRAISAL_DEF.APP_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }

                if (chkActive.Checked == true)
                {
                    hR_PER_APPRAISAL_DEF.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_PER_APPRAISAL_DEF.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                hR_PER_APPRAISAL_DEF.APP_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_PER_APPRAISAL_DEF.MODIFIED_BY = this.LoggedUserName;
                    hR_PER_APPRAISAL_DEF.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_PER_APPRAISAL_DEF.APP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_041.ToString(), false, true);
                    hR_PER_APPRAISAL_DEF.CREATED_BY = this.LoggedUserName;
                    hR_PER_APPRAISAL_DEF.CREATED_DATE = DateTime.Today;
                }
                hR_PER_APPRAISAL_DEF.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PER_APPRAISAL_DEF.APP_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, hR_PER_APPRAISAL_DEF.APP_ID, hR_PER_APPRAISAL_DEF.APP_CODE, hR_PER_APPRAISAL_DEF.APP_PERIOD);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("APPRAISALDEFINITION", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //ProReturn = FIN.DAL.HR.AppraisalDefinition_DAL.GetSPFOR_DUPLICATE_CHECK(hR_PER_APPRAISAL_DEF.APP_CODE, hR_PER_APPRAISAL_DEF.APP_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("APPDEF", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_PER_APPRAISAL_DEF>(hR_PER_APPRAISAL_DEF);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_PER_APPRAISAL_DEF>(hR_PER_APPRAISAL_DEF, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_PER_APPRAISAL_DEF>(hR_PER_APPRAISAL_DEF);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



    }
}