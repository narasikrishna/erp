﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeePermitDetailsEntry : PageBase
    {
        HR_EMP_PERMIT_DTLS hR_EMP_PERMIT_DTLS = new HR_EMP_PERMIT_DTLS();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_PERMIT_DTLS> userCtx = new DataRepository<HR_EMP_PERMIT_DTLS>())
                    {
                        hR_EMP_PERMIT_DTLS = userCtx.Find(r =>
                            (r.PERMIT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_PERMIT_DTLS;

                    ddlEmpId.SelectedValue = hR_EMP_PERMIT_DTLS.PERMIT_EMP_ID.ToString();
                    fn_fillName();
                    ddlPermitType.SelectedValue = hR_EMP_PERMIT_DTLS.PERMIT_TYPE.ToString();
                    txtPermitNum.Text = hR_EMP_PERMIT_DTLS.PERMIT_NUMBER.ToString();
                    txtSponsor.Text = hR_EMP_PERMIT_DTLS.PERMIT_SPONSOR.ToString();
                    if (hR_EMP_PERMIT_DTLS.PERMIT_ISSUE_DATE != null)
                    {
                        txtIssueDate.Text = DBMethod.ConvertDateToString(hR_EMP_PERMIT_DTLS.PERMIT_ISSUE_DATE.ToString());
                    }
                    if (hR_EMP_PERMIT_DTLS.PERMIT_EXPIRY_DATE != null)
                    {
                        txtExpirydate.Text = DBMethod.ConvertDateToString(hR_EMP_PERMIT_DTLS.PERMIT_EXPIRY_DATE.ToString());
                    }

                    if (hR_EMP_PERMIT_DTLS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            EmployeePermitDetails_BLL.fn_GetEmployee(ref ddlEmpId);
            Lookup_BLL.GetLookUpValues(ref ddlPermitType, "PERMIT_TYPE");
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_PERMIT_DTLS = (HR_EMP_PERMIT_DTLS)EntityData;
                }

                hR_EMP_PERMIT_DTLS.PERMIT_EMP_ID = ddlEmpId.SelectedValue.ToString();
                hR_EMP_PERMIT_DTLS.PERMIT_TYPE = ddlPermitType.SelectedValue.ToString();
                hR_EMP_PERMIT_DTLS.PERMIT_SPONSOR = txtSponsor.Text;
                hR_EMP_PERMIT_DTLS.PERMIT_NUMBER = txtPermitNum.Text;
                
                if (txtIssueDate.Text != string.Empty)
                {
                    hR_EMP_PERMIT_DTLS.PERMIT_ISSUE_DATE = DBMethod.ConvertStringToDate(txtIssueDate.Text.ToString());
                }
                if (txtExpirydate.Text != string.Empty)
                {
                    hR_EMP_PERMIT_DTLS.PERMIT_EXPIRY_DATE = DBMethod.ConvertStringToDate(txtExpirydate.Text.ToString());
                }
                hR_EMP_PERMIT_DTLS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_EMP_PERMIT_DTLS.PERMIT_ORG_ID = VMVServices.Web.Utils.OrganizationID;



                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_PERMIT_DTLS.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_PERMIT_DTLS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_PERMIT_DTLS.PERMIT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_081.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_PERMIT_DTLS.CREATED_BY = this.LoggedUserName;
                    hR_EMP_PERMIT_DTLS.CREATED_DATE = DateTime.Today;

                }
                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_ITEM_MASTER.ITEM_ID, iNV_ITEM_MASTER.ITEM_NAME);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                hR_EMP_PERMIT_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PERMIT_DTLS.PERMIT_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, hR_EMP_PERMIT_DTLS.PERMIT_ID, hR_EMP_PERMIT_DTLS.PERMIT_EMP_ID, hR_EMP_PERMIT_DTLS.PERMIT_TYPE, hR_EMP_PERMIT_DTLS.PERMIT_NUMBER);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("EMPLOYEEPERMITDETAILS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

             //   emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS




                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //                if (ProReturn != string.Empty)
                //                {
                //                    if (ProReturn != "0")
                //                    {
                //                        ErrorCollection.Add("ITEMNAME", ProReturn);
                //                        if (ErrorCollection.Count > 0)
                //                        {
                //                            return;
                //                        }
                //                    }
                //                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_PERMIT_DTLS>(hR_EMP_PERMIT_DTLS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_PERMIT_DTLS>(hR_EMP_PERMIT_DTLS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_PERMIT_DTLS>(hR_EMP_PERMIT_DTLS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmpId_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillName();
         
        }
        private void fn_fillName()
        {
            DataTable dt_emp_name = new DataTable();
            dt_emp_name = DBMethod.ExecuteQuery(EmployeePermitDetails_DAL.GetEmployeeName(ddlEmpId.SelectedValue.ToString())).Tables[0];
            if (dt_emp_name != null)
            {
                if (dt_emp_name.Rows.Count > 0)
                {
                    //txtName.Text = dt_emp_name.Rows[0][0].ToString();
                }
            }
        }


    }
}