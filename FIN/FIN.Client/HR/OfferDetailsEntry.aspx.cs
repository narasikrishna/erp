﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class OfferDetailsEntry : PageBase
    {
        HR_OFFER_LETTER_HDR hR_OFFER_LETTER_HDR = new HR_OFFER_LETTER_HDR();
        HR_OFFER_LETTER_DTL hR_OFFER_LETTER_DTL = new HR_OFFER_LETTER_DTL();

        DataTable dtGridData = new DataTable();
        Department_BLL Department_BLL = new Department_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                txtOfferDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.OfferDetailsEntry_DAL.GetOfferEntryDetails(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_OFFER_LETTER_HDR> userCtx = new DataRepository<HR_OFFER_LETTER_HDR>())
                    {
                        hR_OFFER_LETTER_HDR = userCtx.Find(r =>
                            (r.HR_LTR_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_OFFER_LETTER_HDR;
                    txtOfferDate.Text = DBMethod.ConvertDateToString(hR_OFFER_LETTER_HDR.HR_LTR_DATE.ToShortDateString());
                    ddlApplicantID.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_APPLICANT_ID;
                    getApplicantHeaderDet();
                    txtRemarks.Text = hR_OFFER_LETTER_HDR.HR_LTR_REMARKS;
                    ddlCategory.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_CATEGORY_ID;
                    FillJobOffer();
                    ddlJobOffer.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_JOB_ID;
                    FillPosition();
                    ddlPosition.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_POSITION_ID;
                    FillGrade();
                    ddlGrade.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_GRADE_ID;

                    ddlDepartment.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_DEPT_ID;
                    FillDesignation();
                    ddlDesignation.SelectedValue = hR_OFFER_LETTER_HDR.HR_LTR_DESIG_ID;
                    if (hR_OFFER_LETTER_HDR.HR_LTR_DOJ != null)
                    {
                        txtDoj.Text = DBMethod.ConvertDateToString(hR_OFFER_LETTER_HDR.HR_LTR_DOJ.ToString());
                    }


                    chkOfferLetterIssued.Checked = false;
                    if (hR_OFFER_LETTER_HDR.HR_LTR_ISSUED == FINAppConstants.EnabledFlag)
                        chkOfferLetterIssued.Checked = true;
                    chkOfferAccepted.Checked = false;
                    if (hR_OFFER_LETTER_HDR.HR_LTR_ACCEPTED == FINAppConstants.EnabledFlag)
                        chkOfferAccepted.Checked = true;

                    txtCTC.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_OFFER_LETTER_HDR.HR_LTR_CTC.ToString());
                    chkActive.Checked = false;
                    if (hR_OFFER_LETTER_HDR.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                        chkActive.Checked = true;

                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalAppraiser", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_OFFER_LETTER_HDR = (HR_OFFER_LETTER_HDR)EntityData;
                }


                hR_OFFER_LETTER_HDR.HR_LTR_DATE = DBMethod.ConvertStringToDate(txtOfferDate.Text);
                hR_OFFER_LETTER_HDR.HR_LTR_APPLICANT_ID = ddlApplicantID.SelectedValue;
                hR_OFFER_LETTER_HDR.HR_LTR_REMARKS = txtRemarks.Text;
                hR_OFFER_LETTER_HDR.HR_LTR_CATEGORY_ID = ddlCategory.SelectedValue;

                hR_OFFER_LETTER_HDR.HR_LTR_GRADE_ID = ddlGrade.SelectedValue;

                hR_OFFER_LETTER_HDR.HR_LTR_DEPT_ID = ddlDepartment.SelectedValue;

                hR_OFFER_LETTER_HDR.HR_LTR_DESIG_ID = ddlDesignation.SelectedValue;
                hR_OFFER_LETTER_HDR.HR_LTR_JOB_ID = ddlJobOffer.SelectedValue;

                hR_OFFER_LETTER_HDR.HR_LTR_POSITION_ID = ddlPosition.SelectedValue;

                hR_OFFER_LETTER_HDR.HR_LTR_ISSUED = FINAppConstants.DisabledFlag;
                if (chkOfferLetterIssued.Checked)
                    hR_OFFER_LETTER_HDR.HR_LTR_ISSUED = FINAppConstants.EnabledFlag;
                hR_OFFER_LETTER_HDR.HR_LTR_ACCEPTED = FINAppConstants.DisabledFlag;
                if (chkOfferAccepted.Checked)
                    hR_OFFER_LETTER_HDR.HR_LTR_ACCEPTED = FINAppConstants.EnabledFlag;

                hR_OFFER_LETTER_HDR.HR_LTR_CTC = CommonUtils.ConvertStringToDecimal(txtCTC.Text);

                hR_OFFER_LETTER_HDR.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                if (chkActive.Checked)
                    hR_OFFER_LETTER_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                hR_OFFER_LETTER_HDR.HR_LTR_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_OFFER_LETTER_HDR.HR_LTR_DOJ = DBMethod.ConvertStringToDate(txtDoj.Text);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_OFFER_LETTER_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_OFFER_LETTER_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    hR_OFFER_LETTER_HDR.CREATED_BY = this.LoggedUserName;
                    hR_OFFER_LETTER_HDR.CREATED_DATE = DateTime.Today;
                    hR_OFFER_LETTER_HDR.HR_LTR_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_050_M.ToString(), false, true);

                }

                hR_OFFER_LETTER_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_OFFER_LETTER_HDR.HR_LTR_HDR_ID);
                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_OFFER_LETTER_DTL = new HR_OFFER_LETTER_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_OFFER_LETTER_DTL> userCtx = new DataRepository<HR_OFFER_LETTER_DTL>())
                        {
                            hR_OFFER_LETTER_DTL = userCtx.Find(r =>
                                (r.HR_LTR_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }



                    hR_OFFER_LETTER_DTL.HR_LTR_ELEMENT_ID = dtGridData.Rows[iLoop][FINColumnConstants.PAY_ELEMENT_ID].ToString();
                    hR_OFFER_LETTER_DTL.HR_LTR_ELEMENT_AMOUNT = decimal.Parse(dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_ELEMENT_AMOUNT].ToString());

                    hR_OFFER_LETTER_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    hR_OFFER_LETTER_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_OFFER_LETTER_DTL.HR_LTR_HDR_ID = hR_OFFER_LETTER_HDR.HR_LTR_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_OFFER_LETTER_DTL.HR_LTR_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_OFFER_LETTER_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString() != string.Empty)
                        {
                            hR_OFFER_LETTER_DTL.HR_LTR_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString();
                            hR_OFFER_LETTER_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_OFFER_LETTER_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_OFFER_LETTER_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_OFFER_LETTER_DTL.HR_LTR_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_050_D.ToString(), false, true);
                            hR_OFFER_LETTER_DTL.CREATED_BY = this.LoggedUserName;
                            hR_OFFER_LETTER_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_OFFER_LETTER_DTL, FINAppConstants.Add));
                        }
                    }

                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_OFFER_LETTER_HDR, HR_OFFER_LETTER_DTL>(hR_OFFER_LETTER_HDR, tmpChildEntity, hR_OFFER_LETTER_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_OFFER_LETTER_HDR, HR_OFFER_LETTER_DTL>(hR_OFFER_LETTER_HDR, tmpChildEntity, hR_OFFER_LETTER_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                // DBMethod.ExecuteNonQuery("Update HR_APPLICANTS SET STATUS='Offered' WHERE APP_ID='" + ddlApplicantID.SelectedValue +"'");

                HR_APPLICANTS hR_APPLICANTS = new HR_APPLICANTS();

                using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
                {
                    hR_APPLICANTS = userCtx.Find(r =>
                        (r.APP_ID == ddlApplicantID.SelectedValue)
                        ).SingleOrDefault();
                }
                hR_APPLICANTS.STATUS = "Offered";
                DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS, true);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_OD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.HR.Applicant_BLL.fn_GetApplicantNMEmployeeEntry(ref ddlApplicantID, "'Scheduled'", Master.Mode);
            FIN.BLL.HR.Categories_BLL.fn_getCategory(ref ddlCategory, Master.Mode);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);

        }

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("HR_LTR_ELEMENT_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("HR_LTR_ELEMENT_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_ElementName = gvr.FindControl("ddlElementName") as DropDownList;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.HR_LTR_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_ElementName;
            slControls[1] = txt_Amount;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Element_Name_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
            //string strMessage = " Element Name ~ Amount ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementName.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            if (ddl_ElementName.SelectedItem != null)
            {
                drList[FINColumnConstants.PAY_ELEMENT_ID] = ddl_ElementName.SelectedItem.Value;
                drList[FINColumnConstants.PAY_ELEMENT] = ddl_ElementName.SelectedItem.Text;
            }
            drList["HR_LTR_ELEMENT_AMOUNT"] = txt_Amount.Text;
            drList[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.EnabledFlag;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {


                    if (dtGridData.Rows[iLoop][FINColumnConstants.RA_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_OFFER_LETTER_DTL> userCtx = new DataRepository<HR_OFFER_LETTER_DTL>())
                        {
                            hR_OFFER_LETTER_DTL = userCtx.Find(r =>
                                (r.HR_LTR_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.HR_LTR_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }


                    hR_OFFER_LETTER_DTL.HR_LTR_DTL_ID = dtGridData.Rows[iLoop]["HR_LTR_DTL_ID"].ToString();
                    DBMethod.DeleteEntity<HR_OFFER_LETTER_DTL>(hR_OFFER_LETTER_DTL);
                }

                hR_OFFER_LETTER_HDR.HR_LTR_HDR_ID = Master.StrRecordId.ToString();
                DBMethod.DeleteEntity<HR_OFFER_LETTER_HDR>(hR_OFFER_LETTER_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_ElementName = tmpgvr.FindControl("ddlElementName") as DropDownList;

                FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddl_ElementName);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_ElementName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_ELEMENT_ID].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Offer");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DataTable dtgetNoofVacancy = new DataTable();
                DataTable dtgetApplicant = new DataTable();
                DataTable dtgetVacID = new DataTable();

                dtgetNoofVacancy = DBMethod.ExecuteQuery(FIN.DAL.HR.OfferDetailsEntry_DAL.GetNoofVacancy(ddlDepartment.SelectedValue, ddlDesignation.SelectedValue)).Tables[0];
                if (dtgetNoofVacancy.Rows.Count > 0)
                {
                    hfNoofVacancy.Value = dtgetNoofVacancy.Rows[0]["NO_OF_VAC"].ToString();
                }

                dtgetApplicant = DBMethod.ExecuteQuery(FIN.DAL.HR.OfferDetailsEntry_DAL.GetNoofApplicant_frVac(ddlDepartment.SelectedValue, ddlDesignation.SelectedValue)).Tables[0];
                if (dtgetApplicant.Rows.Count > 0)
                {
                    hfApplicant.Value = dtgetApplicant.Rows[0]["NO_OF_APPLCNT"].ToString();
                }

                dtgetVacID = DBMethod.ExecuteQuery(FIN.DAL.HR.OfferDetailsEntry_DAL.GetVac_id(ddlDepartment.SelectedValue, ddlDesignation.SelectedValue)).Tables[0];

                if (dtgetVacID.Rows.Count > 0)
                {
                    if (hfNoofVacancy.Value == hfApplicant.Value)
                    {

                        for (int lLoop = 0; lLoop < dtgetVacID.Rows.Count; lLoop++)
                        {
                            OfferDetailsEntry_DAL.UPDATE_vacancy_Completed(dtgetVacID.Rows[lLoop]["VAC_ID"].ToString());
                        }
                    }
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR, true);
                //            break;
                //        }
            }


            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlAppraiserName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillJobOffer();

        }
        private void FillGrade()
        {
            FIN.BLL.HR.Grades_BLL.fn_getGradeName4Position(ref ddlGrade, ddlPosition.SelectedValue);
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDesignation();
        }
        private void FillDesignation()
        {
            FIN.BLL.HR.Department_BLL.GetDesignationName(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }

        protected void ddlJobOffer_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPosition();
        }
        private void FillPosition()
        {
            FIN.BLL.HR.Position_BLL.fn_GetPositionNameBasedonJob(ref ddlPosition, ddlJobOffer.SelectedValue.ToString());
        }

        protected void ddlApplicantID_SelectedIndexChanged(object sender, EventArgs e)
        {
            getApplicantHeaderDet();

        }
        private void getApplicantHeaderDet()
        {
            HR_APPLICANTS hR_APPLICANTS = FIN.BLL.HR.Applicant_BLL.getClassEntity(ddlApplicantID.SelectedValue.ToString());
            if (hR_APPLICANTS != null)
            {
                txtname.Text = hR_APPLICANTS.APP_FIRST_NAME;
            }
        }

        protected void ddlGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillJobOffer();
        }
        private void FillJobOffer()
        {
            // FIN.BLL.HR.Jobs_BLL.fn_getJobName4Grade(ref ddlJobOffer, ddlGrade.SelectedValue.ToString());
            FIN.BLL.HR.Jobs_BLL.fn_getJob4Category(ref ddlJobOffer, ddlCategory.SelectedValue.ToString());
        }

        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGrade();
        }
    }
}