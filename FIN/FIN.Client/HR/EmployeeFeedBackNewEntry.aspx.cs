﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using System.Data;

namespace FIN.Client.HR
{
    public partial class EmployeeFeedBackNewEntry : PageBase
    {

        HR_EMP_FEEDBACK_HDR hR_EMP_FEEDBACK_HDR = new HR_EMP_FEEDBACK_HDR();
        HR_EMP_FEEDBACK_DTL hR_EMP_FEEDBACK_DTL = new HR_EMP_FEEDBACK_DTL();
        Boolean savedBool = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ErrorCollection.Clear();

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryPgLoad", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FIN.BLL.FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;

            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;

            }
            if (Master.Mode == FIN.BLL.FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            txtFromDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
            FIN.BLL.HR.Employee_BLL.GetEmployeeId(ref ddlEmpName);
            FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTemplateName(ref ddlFeedbackTempName);
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;



                if (Master.Mode != FIN.BLL.FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_FEEDBACK_HDR> userCtx = new DataRepository<HR_EMP_FEEDBACK_HDR>())
                    {
                        hR_EMP_FEEDBACK_HDR = userCtx.Find(r =>
                            (r.EMP_FEEDBACK_ID == Master.StrRecordId.ToString())
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_FEEDBACK_HDR;
                    ddlEmpName.SelectedValue = hR_EMP_FEEDBACK_HDR.EMP_ID.ToString();
                    ddlFeedbackTempName.SelectedValue = hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_TEMP_NAME.ToString();
                    if (hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT.ToString());
                    }
                    ddlEmpName.Enabled = false;
                    ddlFeedbackTempName.Enabled = false;
                    getTemplateQuestion();
                  
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void getTemplateQuestion()
        {
            ErrorCollection.Clear();
            if (ddlFeedbackTempName.SelectedValue.ToString().Length == 0)
            {
                ErrorCollection.Add("SelectTempl", "Please Select the Template Name");
                return;
            }
            div_TemplQues.Visible = true;

            lblTemplateName.Text = ddlFeedbackTempName.SelectedItem.Text.ToString();
            DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.GetFeedbackDtls4EMP(ddlFeedbackTempName.SelectedValue.ToString(), ddlEmpName.SelectedValue)).Tables[0];

            if (dt_data.Rows.Count > 0)
            {
                lblTemplDesc.Text = dt_data.Rows[0]["feedback_template_comments"].ToString();
            }

            DataTable dt_RFA = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("FEEDBACK_RATING_N")).Tables[0];
            Session["RFA_NUMBER"] = dt_RFA;
            string str_RFA = "";
            str_RFA = "<TABLE border='1' align='Center' style='font-weight:bold' >";

            str_RFA += "<TR colspan='4' style='font-weight:bold' >";
            str_RFA += " Ratings for Attributes ";
            str_RFA += "</TR>";

            str_RFA += "<TR align='center'>";
            for (int iLoop = 0; iLoop < dt_RFA.Rows.Count; iLoop++)
            {
                str_RFA += "<TD  align='center'  style='width:150px'>";
                str_RFA += dt_RFA.Rows[iLoop]["LOOKUP_ID"].ToString();
                str_RFA += "</TD>";
            }
            str_RFA += "</TR>";
            str_RFA += "<TR align='center'>";
            for (int iLoop = 0; iLoop < dt_RFA.Rows.Count; iLoop++)
            {
                str_RFA += "<TD  align='center'>";
                str_RFA += dt_RFA.Rows[iLoop]["LOOKUP_NAME"].ToString();
                str_RFA += "</TD>";
            }
            str_RFA += "</TR>";
            str_RFA += "</TABLE>";
            div_RFA.InnerHtml = str_RFA;


            Session["EMPFeedBackSurvey"] = dt_data;
            hf_QuestNum.Value = "0";
            ShowQuestion(hf_QuestNum.Value);
        }
        protected void imgBtnView_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                getTemplateQuestion();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private int ShowQuestion(string str_QuestNum)
        {
            int int_num = int.Parse(str_QuestNum);
            DataTable dt_QuesList = (DataTable)Session["EMPFeedBackSurvey"];
            lblHeading.Text = dt_QuesList.Rows[int_num]["code_name"].ToString();
            lblQuestion.Text = dt_QuesList.Rows[int_num]["feedback_desc"].ToString();
            rbAnswer.Visible = true;
            txtComments.Visible = false;
            gv_quesList.Visible = false;
            lblQuestion.Visible = true;
            int intRetNum = 0;
            txtComments.Text = "";
            hf_QuesType.Value = dt_QuesList.Rows[int_num]["feedback_rating_type"].ToString().ToUpper();


            if (hf_QuesType.Value == "NUMBERS")
            {
                if (dt_QuesList.Rows[int_num]["code_id"].ToString().Trim().Length > 0)
                {
                    var dt_OptData = dt_QuesList.AsEnumerable()
                           .Where(r => r["code_id"].ToString().Trim() == dt_QuesList.Rows[int_num]["code_id"].ToString().Trim())
                           .ToList();

                    DataTable dt_optData = new DataTable();
                    if (dt_OptData.Any())
                    {
                        dt_optData = System.Data.DataTableExtensions.CopyToDataTable(dt_OptData);
                    }

                    gv_quesList.DataSource = dt_optData;
                    gv_quesList.DataBind();
                    lblQuestion.Visible = false;
                    rbAnswer.Visible = false;
                    gv_quesList.Visible = true;
                    intRetNum = dt_optData.Rows.Count;
                }
                else
                {
                    rbAnswer.CellPadding = 20;
                    rbAnswer.RepeatDirection = RepeatDirection.Horizontal;
                    DataTable dt_NUMBER = (DataTable)Session["RFA_NUMBER"];
                    rbAnswer.DataSource = dt_NUMBER;
                    rbAnswer.DataTextField = "LOOKUP_ID";
                    rbAnswer.DataValueField = "LOOKUP_ID";
                    rbAnswer.DataBind();
                    intRetNum = 0;
                    if (dt_QuesList.Rows[int_num]["SEL_RATE"].ToString().Length > 0)
                    {
                        rbAnswer.SelectedValue = dt_QuesList.Rows[int_num]["SEL_RATE"].ToString();
                    }
                }
            }
            else if (hf_QuesType.Value == "OPTIONS")
            {
                lblQuestion.Text = "";
                var dt_OptData = dt_QuesList.AsEnumerable()
                            .Where(r => r["code_id"].ToString().Trim() == dt_QuesList.Rows[int_num]["code_id"].ToString().Trim())
                            .ToList();

                DataTable dt_optData = new DataTable();
                if (dt_OptData.Any())
                {
                    dt_optData = System.Data.DataTableExtensions.CopyToDataTable(dt_OptData);
                }


                rbAnswer.CellPadding = 5;
                rbAnswer.RepeatDirection = RepeatDirection.Vertical;
                rbAnswer.RepeatColumns = 0;
                rbAnswer.DataSource = dt_optData;
                rbAnswer.DataTextField = "feedback_desc";
                rbAnswer.DataValueField = "feedback_dtl_id";
                rbAnswer.DataBind();
                intRetNum = dt_optData.Rows.Count;
                if (dt_QuesList.Rows[int_num]["SEL_RATE"].ToString().Length > 0)
                {
                    rbAnswer.SelectedValue = dt_QuesList.Rows[int_num]["SEL_RATE"].ToString();
                }

                if (dt_optData.Rows[0]["SEL_RATE"].ToString().Length > 0)
                {
                    rbAnswer.SelectedValue = dt_QuesList.Rows[int_num]["SEL_RATE"].ToString();
                }
            }
            else if (hf_QuesType.Value == "PARAGRAPH")
            {

                rbAnswer.Visible = false;
                txtComments.Visible = true;
                intRetNum = 0;
                if (dt_QuesList.Rows[int_num]["TYPED_COMMENTS"].ToString().Length > 0)
                {
                    txtComments.Text = dt_QuesList.Rows[int_num]["TYPED_COMMENTS"].ToString();
                }
            }
            return intRetNum;
        }
        protected void ddlFeedbackTempName_SelectedIndexChanged(object sender, EventArgs e)
        {
            div_TemplQues.Visible = false;
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                bool bol_edit = false;

                using (IRepository<HR_EMP_FEEDBACK_HDR> userCtx = new DataRepository<HR_EMP_FEEDBACK_HDR>())
                {
                    hR_EMP_FEEDBACK_HDR = userCtx.Find(r =>
                        (r.EMP_FEEDBACK_TEMP_NAME == ddlFeedbackTempName.SelectedValue.ToString() && r.EMP_ID == ddlEmpName.SelectedValue.ToString())
                        ).SingleOrDefault();
                }

                if (hR_EMP_FEEDBACK_HDR == null)
                {
                    hR_EMP_FEEDBACK_HDR = new HR_EMP_FEEDBACK_HDR();
                    bol_edit = false;
                }
                else
                {
                    Master.Mode = FINAppConstants.Update;
                    bol_edit = true;
                }
                hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_TEMP_NAME = ddlFeedbackTempName.SelectedValue.ToString();
                hR_EMP_FEEDBACK_HDR.EMP_ID = ddlEmpName.SelectedValue.ToString();
                if (txtFromDate.Text != string.Empty)
                {
                    hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }

                hR_EMP_FEEDBACK_HDR.ENABLED_FLAG = FINAppConstants.Y;

                if (bol_edit)
                {
                    hR_EMP_FEEDBACK_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_FEEDBACK_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_EMP_FEEDBACK_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_FEEDBACK_HDR.CREATED_DATE = DateTime.Today;
                    hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID = FINSP.GetSPFOR_SEQCode("HR_R_083_M".ToString(), false, true);
                }

                hR_EMP_FEEDBACK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID);

                //Save Detail Table
                DataTable dtGridData = new DataTable();
                if (Session["EMPFeedBackSurvey"] != null)
                {
                    dtGridData = (DataTable)Session["EMPFeedBackSurvey"];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_FEEDBACK_DTL = new HR_EMP_FEEDBACK_DTL();
                    if (dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_EMP_FEEDBACK_DTL> userCtx = new DataRepository<HR_EMP_FEEDBACK_DTL>())
                        {
                            hR_EMP_FEEDBACK_DTL = userCtx.Find(r =>
                                (r.EMP_FEEDBACK_DTL_ID == dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }


                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DESC = dtGridData.Rows[iLoop]["feedback_dtl_id"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_TYPE = dtGridData.Rows[iLoop]["feedback_type"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_RATING = dtGridData.Rows[iLoop]["feedback_rating_type"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_COMMENTS = dtGridData.Rows[iLoop]["TYPED_COMMENTS"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_RATE = dtGridData.Rows[iLoop]["SEL_RATE"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_ID = hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID;

                    hR_EMP_FEEDBACK_DTL.ENABLED_FLAG = dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == FIN.BLL.FINAppConstants.TRUEFLAG ? FIN.BLL.FINAppConstants.EnabledFlag : FIN.BLL.FINAppConstants.DisabledFlag;
                    hR_EMP_FEEDBACK_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == FIN.BLL.FINAppConstants.EnabledFlag)
                    {
                        hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString() != "0")
                        {
                            hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString();
                            hR_EMP_FEEDBACK_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_FEEDBACK_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_R_083_D".ToString(), false, true);
                            hR_EMP_FEEDBACK_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_FEEDBACK_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Add));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_EMP_FEEDBACK_HDR, HR_EMP_FEEDBACK_DTL>(hR_EMP_FEEDBACK_HDR, tmpChildEntity, hR_EMP_FEEDBACK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_EMP_FEEDBACK_HDR, HR_EMP_FEEDBACK_DTL>(hR_EMP_FEEDBACK_HDR, tmpChildEntity, hR_EMP_FEEDBACK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATOBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session["EMPFeedBackSurvey"], "FEEDBACK DETAILS ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void imgNext_Click(object sender, ImageClickEventArgs e)
        {

            DataTable dt_QuesList = (DataTable)Session["EMPFeedBackSurvey"];
            if (hf_QuesType.Value == "PARAGRAPH")
            {
                dt_QuesList.Rows[int.Parse(hf_QuestNum.Value)]["TYPED_COMMENTS"] = txtComments.Text;
                dt_QuesList.AcceptChanges();
            }
            Session["EMPFeedBackSurvey"] = dt_QuesList;

            hf_QuestNum.Value = (int.Parse(hf_QuestNum.Value) + 1).ToString();
            if (int.Parse(hf_QuestNum.Value) >= dt_QuesList.Rows.Count)
            {
                hf_QuestNum.Value = (dt_QuesList.Rows.Count - 1).ToString();
            }
            int int_count = ShowQuestion(hf_QuestNum.Value);
            if (int_count > 0)
                hf_QuestNum.Value = (int.Parse(hf_QuestNum.Value) + int_count - 1).ToString();


        }

        protected void imgPrevious_Click(object sender, ImageClickEventArgs e)
        {

            DataTable dt_QuesList = (DataTable)Session["EMPFeedBackSurvey"];

            if (hf_QuesType.Value == "PARAGRAPH")
            {
                dt_QuesList.Rows[int.Parse(hf_QuestNum.Value)]["TYPED_COMMENTS"] = txtComments.Text;
                dt_QuesList.AcceptChanges();
            }
            Session["EMPFeedBackSurvey"] = dt_QuesList;

            hf_QuestNum.Value = (int.Parse(hf_QuestNum.Value) - 1).ToString();
            if (int.Parse(hf_QuestNum.Value) >= dt_QuesList.Rows.Count)
            {
                hf_QuestNum.Value = (dt_QuesList.Rows.Count - 1).ToString();
            }
            if (int.Parse(hf_QuestNum.Value) < 0)
            {
                hf_QuestNum.Value = "0";
            }
            int int_count = ShowQuestion(hf_QuestNum.Value);
            if (int_count > 0)
                hf_QuestNum.Value = (int.Parse(hf_QuestNum.Value) - int_count).ToString();


        }

        protected void rbAnswer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt_QuesList = (DataTable)Session["EMPFeedBackSurvey"];
            if (hf_QuesType.Value == "NUMBERS")
            {
                dt_QuesList.Rows[int.Parse(hf_QuestNum.Value)]["SEL_RATE"] = rbAnswer.SelectedValue.ToString();
                dt_QuesList.AcceptChanges();
            }
            else if (hf_QuesType.Value == "PARAGRAPH")
            {
                dt_QuesList.Rows[int.Parse(hf_QuestNum.Value)]["TYPED_COMMENTS"] = txtComments.Text;
                dt_QuesList.AcceptChanges();
            }
            else if (hf_QuesType.Value == "OPTIONS")
            {
                DataRow[] dr = dt_QuesList.Select("code_id='" + dt_QuesList.Rows[int.Parse(hf_QuestNum.Value)]["code_id"].ToString().Trim() + "'");
                for (int kLoop = 0; kLoop < dr.Length; kLoop++)
                {
                    dr[kLoop]["SEL_RATE"] = rbAnswer.SelectedValue.ToString();
                }

            }
            Session["EMPFeedBackSurvey"] = dt_QuesList;

        }

        protected void gv_quesList_DataBound(object sender, EventArgs e)
        {

        }

        protected void gv_quesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                RadioButtonList rb_tmp = (RadioButtonList)e.Row.FindControl("rb_G_Answer");
                if (gv_quesList.DataKeys[e.Row.RowIndex].Values["feedback_rating_type"].ToString().ToUpper() == "NUMBERS")
                {
                    rb_tmp.CellPadding = 20;
                    rb_tmp.RepeatDirection = RepeatDirection.Horizontal;
                    DataTable dt_NUMBER = (DataTable)Session["RFA_NUMBER"];
                    rb_tmp.DataSource = dt_NUMBER;
                    rb_tmp.DataTextField = "LOOKUP_ID";
                    rb_tmp.DataValueField = "LOOKUP_ID";
                    rb_tmp.DataBind();
                    if (gv_quesList.DataKeys[e.Row.RowIndex].Values["SEL_RATE"].ToString().Length > 0)
                    {
                        rb_tmp.SelectedValue = gv_quesList.DataKeys[e.Row.RowIndex].Values["SEL_RATE"].ToString();
                    }
                }
                else
                {
                    rb_tmp.Visible = false;
                    TextBox txt_tmp = (TextBox)e.Row.FindControl("txt_G_Comments");
                    txt_tmp.Visible = true;
                    if (gv_quesList.DataKeys[e.Row.RowIndex].Values["TYPED_COMMENTS"].ToString().Length > 0)
                    {
                        txt_tmp.Text = gv_quesList.DataKeys[e.Row.RowIndex].Values["TYPED_COMMENTS"].ToString();
                    }
                }

            }
        }

        protected void rb_G_Answer_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            RadioButtonList rb_tmp = (RadioButtonList)gvr.FindControl("rb_G_Answer");
            DataTable dt_QuesList = (DataTable)Session["EMPFeedBackSurvey"];
            DataRow[] dr = dt_QuesList.Select("feedback_dtl_id='" + gv_quesList.DataKeys[gvr.RowIndex].Values["feedback_dtl_id"].ToString() + "'");
            for (int kLoop = 0; kLoop < dr.Length; kLoop++)
            {
                dr[kLoop]["SEL_RATE"] = rb_tmp.SelectedValue.ToString();
            }

            Session["EMPFeedBackSurvey"] = dt_QuesList;


        }
    }
}