﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{

    public partial class Training_RequestEntry : PageBase
    {

        HR_TRM_REQUEST_HDR hR_TRM_REQUEST_HDR = new HR_TRM_REQUEST_HDR();
        HR_TRM_REQUEST_DTL hR_TRM_REQUEST_DTL = new HR_TRM_REQUEST_DTL();
        string ProReturn = null;


        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlAudianceTybe, "AUDINC_TYP");
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "TR_STATUS");
            FIN.BLL.HR.TrainingRequest_BLL.fn_GetProgram(ref ddlProgrameName);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.TrainingRequest_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_TRM_REQUEST_HDR> userCtx = new DataRepository<HR_TRM_REQUEST_HDR>())
                    {
                        hR_TRM_REQUEST_HDR = userCtx.Find(r =>
                            (r.REQ_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_REQUEST_HDR;

                    if (hR_TRM_REQUEST_HDR.REQ_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_TRM_REQUEST_HDR.REQ_DATE.ToString());
                    }
                    ddlAudianceTybe.SelectedValue = hR_TRM_REQUEST_HDR.REQ_AUDIANCE_TYPE;
                    ddlStatus.SelectedValue = hR_TRM_REQUEST_HDR.REQ_STATUS;
                    // txtnoofmem.Text = hR_TRM_REQUEST_HDR.NO_OF_MEMBERS.ToString();
                    txtComments.Text = hR_TRM_REQUEST_HDR.REQ_COMMENTS;
                    if (hR_TRM_REQUEST_HDR.REQ_PROG_ID != null)
                    {
                        ddlProgrameName.SelectedValue = hR_TRM_REQUEST_HDR.REQ_PROG_ID.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_REQUEST_HDR = (HR_TRM_REQUEST_HDR)EntityData;
                }

                if (txtDate.Text.Trim() != string.Empty)
                {
                    hR_TRM_REQUEST_HDR.REQ_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }
                hR_TRM_REQUEST_HDR.REQ_AUDIANCE_TYPE = ddlAudianceTybe.SelectedValue;
                hR_TRM_REQUEST_HDR.REQ_STATUS = ddlStatus.SelectedValue;
                // hR_TRM_REQUEST_HDR.NO_OF_MEMBERS = decimal.Parse(txtnoofmem.Text);
                hR_TRM_REQUEST_HDR.REQ_COMMENTS = txtComments.Text;
                hR_TRM_REQUEST_HDR.REQ_PROG_ID = ddlProgrameName.SelectedValue;

                hR_TRM_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                // hR_TRM_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                hR_TRM_REQUEST_HDR.REQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_REQUEST_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_REQUEST_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_TRM_REQUEST_HDR.REQ_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_056_M".ToString(), false, true);
                    //hR_TRM_REQUEST_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_REQUEST_HDR_SEQ);
                    hR_TRM_REQUEST_HDR.CREATED_BY = this.LoggedUserName;
                    hR_TRM_REQUEST_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_TRM_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_REQUEST_HDR.REQ_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRM_REQUEST_DTL = new HR_TRM_REQUEST_DTL();
                    if (dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_REQUEST_DTL> userCtx = new DataRepository<HR_TRM_REQUEST_DTL>())
                        {
                            hR_TRM_REQUEST_DTL = userCtx.Find(r =>
                                (r.REQ_DTL_ID == dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_TRM_REQUEST_DTL.COM_LINE_NUM = (iLoop + 1);
                    hR_TRM_REQUEST_DTL.REQ_DEPT_ID = dtGridData.Rows[iLoop]["dept_id"].ToString();
                    hR_TRM_REQUEST_DTL.REQ_EMP_GROUP = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();

                    hR_TRM_REQUEST_DTL.NO_OF_MEMBERS = decimal.Parse(dtGridData.Rows[iLoop]["NO_OF_MEMBERS"].ToString());
                    hR_TRM_REQUEST_DTL.REQ_HDR_ID = hR_TRM_REQUEST_HDR.REQ_HDR_ID;
                    hR_TRM_REQUEST_DTL.REQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    hR_TRM_REQUEST_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRM_REQUEST_DTL.ENABLED_FLAG = FINAppConstants.Y;



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_REQUEST_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_TRM_REQUEST_DTL.COM_LEVEL_DESC, hR_TRM_REQUEST_DTL.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_TRM_REQUEST_DTL.REQ_DTL_ID = dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString();
                            hR_TRM_REQUEST_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_TRM_REQUEST_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_REQUEST_DTL, "U"));

                        }
                        else
                        {

                            hR_TRM_REQUEST_DTL.REQ_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_056_D".ToString(), false, true);
                            hR_TRM_REQUEST_DTL.CREATED_BY = this.LoggedUserName;
                            hR_TRM_REQUEST_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_REQUEST_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_REQUEST_HDR, HR_TRM_REQUEST_DTL>(hR_TRM_REQUEST_HDR, tmpChildEntity, hR_TRM_REQUEST_DTL);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_REQUEST_HDR, HR_TRM_REQUEST_DTL>(hR_TRM_REQUEST_HDR, tmpChildEntity, hR_TRM_REQUEST_DTL, true);
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlDepartmentName = tmpgvr.FindControl("ddlDepartmentName") as DropDownList;
                DropDownList ddlEmployeeGroup = tmpgvr.FindControl("ddlEmployeeGroup") as DropDownList;
               // DropDownList ddlProgrameName = tmpgvr.FindControl("ddlProgrameName") as DropDownList;

                
                FIN.BLL.HR.TrainingRequest_BLL.fn_GetDeptNM(ref ddlDepartmentName);
               // Lookup_BLL.GetLookUpValues(ref ddlEmployeeGroup, "EMP_GROUP");
                TimeAttendanceGroup_BLL.GetTmAttndGrpdtls(ref ddlEmployeeGroup);
                //FIN.BLL.HR.TrainingRequest_BLL.fn_GetProgram(ref ddlProgrameName);
                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlDepartmentName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["DEPT_ID"].ToString();
                    ddlEmployeeGroup.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOOKUP_ID"].ToString();
                  //  ddlProgrameName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PROG_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Request Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();



            DropDownList ddlDepartmentName = gvr.FindControl("ddlDepartmentName") as DropDownList;
            DropDownList ddlEmployeeGroup = gvr.FindControl("ddlEmployeeGroup") as DropDownList;
            //  DropDownList ddlProgrameName = gvr.FindControl("ddlProgrameName") as DropDownList;
            TextBox txtnoofmem = gvr.FindControl("txtnoofmem") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["REQ_DTL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlDepartmentName;
            slControls[1] = ddlEmployeeGroup;
            slControls[2] = txtnoofmem;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
       
            string strMessage = Prop_File_Data["Department_Name_P"] + " ~ " + Prop_File_Data["Employee_Group_P"] + " ~ " + Prop_File_Data["No_of_Members_P"] + "";
            //string strMessage = " Department Name ~ Employee Group ~ No of Members";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            string strCondition = " dept_id='" + ddlDepartmentName.SelectedValue + "' AND LOOKUP_ID= '" + ddlEmployeeGroup.SelectedValue + "'";

            strMessage = FINMessageConstatns.TraingReq_DeptEmpGrp;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }		



            if (ddlDepartmentName.SelectedItem != null)
            {
                drList["dept_id"] = ddlDepartmentName.SelectedItem.Value;
                drList["DEPT_NAME"] = ddlDepartmentName.SelectedItem.Text;
            }

            if (ddlEmployeeGroup.SelectedItem != null)
            {
                drList["LOOKUP_ID"] = ddlEmployeeGroup.SelectedItem.Value;
                drList["LOOKUP_NAME"] = ddlEmployeeGroup.SelectedItem.Text;
            }
            //if (ddlProgrameName.SelectedItem != null)
            //{
            //    drList["PROG_ID"] = ddlProgrameName.SelectedItem.Value;
            //    drList["PROG_DESC"] = ddlProgrameName.SelectedItem.Text;
            //}
            drList["NO_OF_MEMBERS"] = txtnoofmem.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_TRM_REQUEST_HDR>(hR_TRM_REQUEST_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TR_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



    }
}