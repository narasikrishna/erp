﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InterviewResultsEntry.aspx.cs" Inherits="FIN.Client.HR.InterviewResultsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDesc">
                Vacancy
            </div>
            <div class="divtxtBox  LNOrient" style="width: 505px">
                <asp:DropDownList ID="ddlvacancy" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlvacancy_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
                Applicant
            </div>
            <div class="divtxtBox  LNOrient" style="width: 505px">
                <asp:DropDownList ID="ddlApplicant" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlApplicant_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <br />
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Recommondations
            </div>
            <div class="divtxtBox  LNOrient" style="width: 505px">
                <asp:GridView ID="gvstatus" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    TabIndex="3" DataKeyNames="INT_STATUS" EmptyDataText="No Record Found" ShowHeaderWhenEmpty="True"
                    Width="500px">
                    <Columns>
                        <asp:BoundField DataField="EMP_NAME" HeaderText="Employee Name" ControlStyle-Width="300px" />
                        <asp:BoundField DataField="INT_STATUS" HeaderText="Status" ControlStyle-Width="200px" />
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <br />
        <%--        <div class="divRowContainer" align="center">
                 <div class="lblBox" style="float: left; width: 140px" id="Div4">
                
            </div>
        <div class="lblBox" style="float: center; width: 200px " id="Div3" >
                EVALUATION RESULTS
            </div>
            </div>
                    <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Evaluation Results
            </div>
            <div class="divtxtBox  LNOrient" style="width: 505px">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    TabIndex="4" DataKeyNames="CONDITIONS" OnRowDataBound="gvData_RowDataBound" EmptyDataText="No Record Found"
                    ShowHeaderWhenEmpty="True" Width="500px">
                    <Columns>
                        <asp:BoundField DataField="EMP_FIRST_NAME" HeaderText="Employee Name" />
                        <asp:BoundField DataField="VAC_CRIT_NAME" HeaderText="Criteria" />
                        <asp:BoundField DataField="VAC_CRIT_VALUE" HeaderText="Criteria Value" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField DataField="INT_LEVEL_SCORED" HeaderText="LevelScored" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField DataField="VAC_CRIT_CONDITION" HeaderText="Condition" />
                        <asp:BoundField HeaderText="Status" />
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
                <asp:HiddenField ID="hf_INVID" runat="server" />
            </div>
        </div>
        <br />
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer" align="center">
                 <div class="lblBox" style="float: left; width: 140px" id="Div5">
                
            </div>
        <div class="lblBox" style="float: center; width: 200px" id="Div6">
                RECOMMONDATIONS
            </div>
            </div>
                    <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblResult">
                Result
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlResult" runat="server" TabIndex="5" CssClass="validate[required]  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 145px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="6" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 495px">
                <asp:TextBox ID="txtComments" TabIndex="7" MaxLength="500" Height="50px" TextMode="MultiLine"
                    CssClass=" txtBox" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="8" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:HiddenField ID="hf_VAC_ID" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
