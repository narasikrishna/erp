﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeMGRPEntry : PageBase
    {

        HR_EMP_MGRP_DTLS hR_EMP_MGRP_DTLS = new HR_EMP_MGRP_DTLS();
        DataTable dtGridData = new DataTable();
        DepartmentDetails_BLL DepartmentDetails_BLL = new DepartmentDetails_BLL();
        string ProReturn = null;

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {

            DepartmentDetails_BLL.fn_GetDepartmentDetails_All(ref ddlDepartment);
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeMGRP_DAL.GetEmpMGRPDtl(Master.StrRecordId)).Tables[0];


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_MGRP_DTLS> userCtx = new DataRepository<HR_EMP_MGRP_DTLS>())
                    {
                        hR_EMP_MGRP_DTLS = userCtx.Find(r =>
                            (r.EMP_MGRP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_MGRP_DTLS;

                    ddlFinancialYear.SelectedValue = hR_EMP_MGRP_DTLS.EMP_MGRP_YEAR;
                    ddlDepartment.SelectedValue = hR_EMP_MGRP_DTLS.ATTRIBUTE1;
                    fillDesignation();
                    ddlDesignation.SelectedValue = hR_EMP_MGRP_DTLS.ATTRIBUTE2;


                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_EMP_MGRP_DTLS hR_EMP_MGRP_DTLS = new HR_EMP_MGRP_DTLS();

                        if (gvData.DataKeys[iLoop].Values["EMP_MGRP_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_EMP_MGRP_DTLS> userCtx = new DataRepository<HR_EMP_MGRP_DTLS>())
                            {
                                hR_EMP_MGRP_DTLS = userCtx.Find(r =>
                                    (r.EMP_MGRP_ID == gvData.DataKeys[iLoop].Values["EMP_MGRP_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtAmt = (TextBox)gvData.Rows[iLoop].FindControl("txtAmt");


                        hR_EMP_MGRP_DTLS.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        if (txtAmt.Text != "")
                        {
                            hR_EMP_MGRP_DTLS.EMP_MGRP_AMT = decimal.Parse(txtAmt.Text);
                        }
                        else
                        {
                            hR_EMP_MGRP_DTLS.EMP_MGRP_AMT = null;
                        }
                        hR_EMP_MGRP_DTLS.EMP_MGRP_YEAR = ddlFinancialYear.SelectedValue;
                        hR_EMP_MGRP_DTLS.ATTRIBUTE1 = ddlDepartment.SelectedValue;
                        hR_EMP_MGRP_DTLS.ATTRIBUTE2 = ddlDesignation.SelectedValue;

                        hR_EMP_MGRP_DTLS.ENABLED_FLAG = FINAppConstants.Y;

                        hR_EMP_MGRP_DTLS.EMP_MGRP_ORG_ID = VMVServices.Web.Utils.OrganizationID;




                        //if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                        //{
                        //    hR_EMP_MGRP_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                        //}
                        //else
                        //{
                        //    hR_EMP_MGRP_DTLS.ENABLED_FLAG = FINAppConstants.N;
                        //}


                        if (gvData.DataKeys[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_MGRP_DTLS, "D"));
                        }
                        else
                        {


                            if (gvData.DataKeys[iLoop].Values["EMP_MGRP_ID"].ToString() != "0")
                            {
                                hR_EMP_MGRP_DTLS.EMP_MGRP_ID = gvData.DataKeys[iLoop].Values["EMP_MGRP_ID"].ToString();
                                hR_EMP_MGRP_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_MGRP_DTLS.EMP_MGRP_ID);
                                hR_EMP_MGRP_DTLS.MODIFIED_BY = this.LoggedUserName;
                                hR_EMP_MGRP_DTLS.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<HR_EMP_MGRP_DTLS>(hR_EMP_MGRP_DTLS, true);
                                savedBool = true;
                                //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                            }
                            else
                            {

                                hR_EMP_MGRP_DTLS.EMP_MGRP_ID = FINSP.GetSPFOR_SEQCode("HR_095".ToString(), false, true);

                                hR_EMP_MGRP_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_MGRP_DTLS.EMP_MGRP_ID);
                                hR_EMP_MGRP_DTLS.CREATED_BY = this.LoggedUserName;
                                hR_EMP_MGRP_DTLS.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<HR_EMP_MGRP_DTLS>(hR_EMP_MGRP_DTLS);
                                savedBool = true;
                                // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                            }
                        }

                    }
                    VMVServices.Web.Utils.SavedRecordId = "";



                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlParentname = tmpgvr.FindControl("ddlParentname") as DropDownList;
                //DepartmentDetails_BLL.fn_GetDesigName(ref ddlParentname);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlParentname.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.DEPT_DESIG_ID].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee MGRP Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }



                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    // dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtAmt = gvr.FindControl("txtAmt") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["EMP_MGRP_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtAmt;

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox";
            string strMessage = "Amount";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            drList["EMP_MGRP_AMT"] = txtAmt.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;


        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_EMP_MGRP_DTLS.EMP_MGRP_ID = dtGridData.Rows[iLoop]["EMP_MGRP_ID"].ToString();
                    DBMethod.DeleteEntity<HR_EMP_MGRP_DTLS>(hR_EMP_MGRP_DTLS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }









        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillDesignation();
        }

        private void fillDesignation()
        {
            Department_BLL.GetDesignationName_All(ref ddlDesignation, ddlDepartment.SelectedValue);
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeMGRP_DAL.GetEmpMGRPDtl_asper_FSYR_Dept_Desig(ddlFinancialYear.SelectedValue, ddlDepartment.SelectedValue, ddlDesignation.SelectedValue)).Tables[0];
            if (dtGridData.Rows.Count > 0)
            {
                BindGrid(dtGridData);
            }
            else
            {
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeMGRP_DAL.GetEmpMGRPDtl_asperDept_Desig(ddlDepartment.SelectedValue, ddlDesignation.SelectedValue)).Tables[0];
                BindGrid(dtGridData);
            }


        }


    }
}