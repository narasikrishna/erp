﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class InterviewNewEntry : PageBase
    {

        HR_INTERVIEWS_HDR hR_INTERVIEWS_HDR = new HR_INTERVIEWS_HDR();
        HR_INTERVIEWS_DTL hR_INTERVIEWS_DTL = new HR_INTERVIEWS_DTL();
        string ProReturn = null;
        Boolean savedBool;


        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Interview_BLL.fn_GetVacancyNM(ref ddlvacancy);
            FIN.BLL.HR.Interview_BLL.fn_GetVacCriteria(ref ddlcriteria);
            Lookup_BLL.GetLookUpValues(ref ddlstatus, "INTRV_STS");
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;
                //Session[FINSessionConstants.GridData] = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_INTERVIEWS_DTL> userCtx = new DataRepository<HR_INTERVIEWS_DTL>())
                    {
                        hR_INTERVIEWS_DTL = userCtx.Find(r =>
                            (r.INT_DTL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_INTERVIEWS_DTL;

                    if (hR_INTERVIEWS_DTL.INT_COMMENTS != null)
                    {
                        txtcomments.Text = hR_INTERVIEWS_DTL.INT_COMMENTS;
                    }

                    if (hR_INTERVIEWS_DTL.INT_DATE != null)
                    {
                        txtdate.Text = DBMethod.ConvertDateToString(hR_INTERVIEWS_DTL.INT_DATE.ToString());
                    }

                    if (hR_INTERVIEWS_DTL.ATTRIBUTE1 != null)
                    {
                        ddlvacancy.SelectedValue = hR_INTERVIEWS_DTL.ATTRIBUTE1;
                        fillapplicant();
                    }

                    if (hR_INTERVIEWS_DTL.ATTRIBUTE2 != null)
                    {
                        ddlApplicant.SelectedValue = hR_INTERVIEWS_DTL.ATTRIBUTE2;
                    }
                    fillemp();

                    if (hR_INTERVIEWS_DTL.EMP_ID != null)
                    {
                        ddlemployee.SelectedValue = hR_INTERVIEWS_DTL.INT_DTL_ID;
                        fillDeptDesgData();
                    }
                    if (hR_INTERVIEWS_DTL.INT_STATUS != null)
                    {
                        ddlstatus.SelectedValue = hR_INTERVIEWS_DTL.INT_STATUS;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                using (IRepository<HR_INTERVIEWS_DTL> userCtx = new DataRepository<HR_INTERVIEWS_DTL>())
                {
                    hR_INTERVIEWS_DTL = userCtx.Find(r =>
                        (r.INT_DTL_ID == ddlemployee.SelectedValue)
                        ).SingleOrDefault();
                }

                VMVServices.Web.Utils.SavedRecordId = hR_INTERVIEWS_DTL.INT_DTL_ID.ToString();
                hR_INTERVIEWS_DTL.VAC_CRIT_ID = ddlcriteria.SelectedValue;
                //hR_INTERVIEWS_DTL.INT_LEVEL_SCORED = int.Parse(txtlevelscored.Text);
                hR_INTERVIEWS_DTL.INT_COMMENTS = txtcomments.Text;
                hR_INTERVIEWS_DTL.INT_STATUS = ddlstatus.SelectedValue;
                hR_INTERVIEWS_DTL.MODIFIED_BY = this.LoggedUserName;
                hR_INTERVIEWS_DTL.MODIFIED_DATE = DateTime.Today;

                hR_INTERVIEWS_DTL.ATTRIBUTE1 = ddlvacancy.SelectedValue;
                hR_INTERVIEWS_DTL.ATTRIBUTE2 = ddlApplicant.SelectedValue;


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Evaluation Condition");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                HR_INTERVIEWS_DTL_CRIT hR_INTERVIEWS_DTL_CRIT = new HR_INTERVIEWS_DTL_CRIT();
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    hR_INTERVIEWS_DTL_CRIT = new HR_INTERVIEWS_DTL_CRIT();
                    if (dtGridData.Rows[iLoop]["INT_CRIT_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_INTERVIEWS_DTL_CRIT> userCtx = new DataRepository<HR_INTERVIEWS_DTL_CRIT>())
                        {
                            hR_INTERVIEWS_DTL_CRIT = userCtx.Find(r =>
                                (r.INT_CRIT_ID == gvData.DataKeys[iLoop].Values["INT_CRIT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_INTERVIEWS_DTL_CRIT.INT_DTL_ID = hR_INTERVIEWS_DTL.INT_DTL_ID;
                    hR_INTERVIEWS_DTL_CRIT.VAC_EVAL_ID = gvData.DataKeys[iLoop].Values["VAC_EVAL_ID"].ToString();
                    TextBox txt_level_scored = (TextBox)gvData.Rows[iLoop].FindControl("txtItemLevelScored");
                    if (txt_level_scored.Text.ToString().Length > 0)
                    {

                        hR_INTERVIEWS_DTL_CRIT.INT_LEVEL_SCORED = int.Parse(txt_level_scored.Text);// int.Parse(dtGridData.Rows[iLoop]["INT_LEVEL_SCORED"].ToString());


                        hR_INTERVIEWS_DTL_CRIT.ENABLED_FLAG = FINAppConstants.Y;

                        hR_INTERVIEWS_DTL_CRIT.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            hR_INTERVIEWS_DTL_CRIT.INT_CRIT_ID = gvData.DataKeys[iLoop].Values["INT_CRIT_ID"].ToString();
                            tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL_CRIT, FINAppConstants.Delete));
                        }
                        else
                        {
                            if (gvData.DataKeys[iLoop].Values["INT_CRIT_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["INT_CRIT_ID"].ToString() != string.Empty)
                            {
                                hR_INTERVIEWS_DTL_CRIT.INT_CRIT_ID = gvData.DataKeys[iLoop].Values["INT_CRIT_ID"].ToString();

                                hR_INTERVIEWS_DTL_CRIT.MODIFIED_DATE = DateTime.Today;
                                hR_INTERVIEWS_DTL_CRIT.MODIFIED_BY = this.LoggedUserName;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL_CRIT, FINAppConstants.Update));
                            }
                            else
                            {
                                hR_INTERVIEWS_DTL_CRIT.INT_CRIT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_005_D.ToString(), false, true);

                                hR_INTERVIEWS_DTL_CRIT.CREATED_BY = this.LoggedUserName;
                                hR_INTERVIEWS_DTL_CRIT.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_INTERVIEWS_DTL_CRIT, FINAppConstants.Add));
                            }
                        }
                    }
                }

                CommonUtils.SavePCEntity<HR_INTERVIEWS_DTL, HR_INTERVIEWS_DTL_CRIT>(hR_INTERVIEWS_DTL, tmpChildEntity, hR_INTERVIEWS_DTL_CRIT, true);
                savedBool = true;

                if (hR_INTERVIEWS_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.INTERVIEW_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion




        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_INTERVIEWS_HDR>(hR_INTERVIEWS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("INTV_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlvacancy_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillapplicant();
        }

        private void fillapplicant()
        {
            Interview_BLL.fn_GetApplicantNM_basedon_vacancy(ref ddlApplicant, ddlvacancy.SelectedValue);
        }

        protected void ddlApplicant_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillemp();
        }
        private void fillemp()
        {
            AssetIssue_BLL.GetEmployeeName_asper_Applicant(ref ddlemployee, ddlApplicant.SelectedValue);
        }
        private void fillDeptDesgData()
        {
            DataTable dtinterviewdtls = new DataTable();
            dtinterviewdtls = DBMethod.ExecuteQuery(Interview_DAL.GetInterviewdtls_basedon_emp(ddlemployee.SelectedValue)).Tables[0];
            if (dtinterviewdtls.Rows.Count > 0)
            {
                txtdate.Text = DBMethod.ConvertDateToString(dtinterviewdtls.Rows[0]["INT_DATE"].ToString());
                txtDept.Text = dtinterviewdtls.Rows[0]["DEPT_NAME"].ToString();
                txtdesig.Text = dtinterviewdtls.Rows[0]["DESIG_NAME"].ToString();
                txtLoc.Text = dtinterviewdtls.Rows[0]["LOC_DESC"].ToString();
                txtmode.Text = dtinterviewdtls.Rows[0]["CODE"].ToString();
                txtcomments.Text = dtinterviewdtls.Rows[0]["INT_COMMENTS"].ToString();
                if (dtinterviewdtls.Rows[0]["INT_STATUS"] != null)
                {
                    ddlstatus.SelectedValue = dtinterviewdtls.Rows[0]["INT_STATUS"].ToString();
                }
            }

            dtinterviewdtls = DBMethod.ExecuteQuery(Interview_DAL.getIntDtlCRIT4DTLID(ddlemployee.SelectedValue,ddlvacancy.SelectedValue)).Tables[0];
            BindGrid(dtinterviewdtls);
        }
        protected void ddlemployee_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillDeptDesgData();
        }



        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_EvalCrit = tmpgvr.FindControl("ddlEvalCrit") as DropDownList;
                VacancyCriteria_BLL.getEvalCondition(ref ddl_EvalCrit, ddlvacancy.SelectedValue);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_EvalCrit.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["VAC_EVAL_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_EvalCrit = gvr.FindControl("ddlEvalCrit") as DropDownList;
            TextBox txt_LevelScored = gvr.FindControl("txtLevelScored") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INT_CRIT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_EvalCrit;
            slControls[1] = txt_LevelScored;


            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Evaluation_Criteria_P"] + " ~ " + Prop_File_Data["Level_Scored_P"];
            //string strMessage = "Group Name ~ Effective Date ~ Effective Date ~ End Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }



            string strCondition = "VAC_EVAL_ID='" + ddl_EvalCrit.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            drList["VAC_EVAL_ID"] = ddl_EvalCrit.SelectedValue.ToString();
            drList["VAC_EVAL_NAME"] = ddl_EvalCrit.SelectedItem.Text;
            drList["INT_LEVEL_SCORED"] = int.Parse(txt_LevelScored.Text.ToString());

            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

    }
}