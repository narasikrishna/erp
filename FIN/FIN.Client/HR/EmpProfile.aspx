﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpProfile.aspx.cs" Inherits="FIN.Client.HR.EmpProfile" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblRatingName">
                Rating Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:TextBox ID="txtRatingName" MaxLength="100" runat="server" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblCategory">
                Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlcategory" runat="server" CssClass=" validate[required]  RequiredField EntryFont ddlStype"
                    Width="100%" AutoPostBack="True" TabIndex="2" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="Div5">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddljob" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype"
                   AutoPostBack="True" TabIndex="3" OnSelectedIndexChanged="ddljob_SelectedIndexChanged">
                   
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="Div6">
                Position
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlposition" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype"
                    TabIndex="4" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddlposition_SelectedIndexChanged">
                   
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="Div4">
                Grade
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlgrade" runat="server" CssClass="validate[required]  RequiredField EntryFont ddlStype"
                    TabIndex="5">
                    
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="Div1">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:TextBox ID="txtfromdate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField txtBox"
                    TabIndex="6" runat="server"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtfromdate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtfromdate" />
            </div>
              <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="Div3">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 200px">
                <asp:TextBox ID="txttodate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]]  txtBox"
                    TabIndex="7" runat="server"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txttodate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txttodate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="100%" DataKeyNames="PROF_DTL_ID,COM_HDR_ID,COM_LINE_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Competency">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlcompetency" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="8" AutoPostBack="True" OnSelectedIndexChanged="ddlcompetency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlcompetency" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="8" AutoPostBack="True" OnSelectedIndexChanged="ddlcompetency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcompetency" runat="server" TabIndex="8" Text='<%# Eval("COM_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Competency Level">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlcompetencyLevel" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="9" AutoPostBack="True" OnSelectedIndexChanged="ddlcompetencyLevel_SelectedIndexChanged">
                              
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlcompetencyLevel" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="9" AutoPostBack="True" OnSelectedIndexChanged="ddlcompetencyLevel_SelectedIndexChanged">
                              
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblcompetencyLevel" runat="server" TabIndex="9" Text='<%# Eval("COM_LEVEL_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Rating">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEmprate" Width="100px" MaxLength="5" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="10" Text='<%# Eval("EMP_PROF_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxfEsdxtender4" runat="server" FilterType="Numbers"
                                TargetControlID="txtEmprate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtEmprate" Width="100px" MaxLength="5" runat="server" TabIndex="10"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxfExtenderer4" runat="server" FilterType="Numbers"
                                TargetControlID="txtEmprate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmprate" runat="server" TabIndex="10" Text='<%# Eval("EMP_PROF_RATING") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="11" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="11" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
            <asp:HiddenField ID="hF_COM_LINE_ID" runat="server" />
            <asp:HiddenField ID="HF_COM_LINK_DTL_ID" runat="server" />
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
