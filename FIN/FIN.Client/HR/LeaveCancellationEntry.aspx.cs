﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using FIN.DAL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LeaveCancellationEntry : PageBase
    {
        HR_LEAVE_CANCELLATION_HDR hR_LEAVE_CANCELLATION_HDR = new HR_LEAVE_CANCELLATION_HDR();
        Boolean savedBool;
        string ProReturn = null;
        DataTable Month_data = new DataTable();
        static int month;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                txtCancelDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                IdPayPrd.Visible = false;
                lbllevsal.Visible = false;
                IDLevsal.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_LEAVE_CANCELLATION_HDR> userCtx = new DataRepository<HR_LEAVE_CANCELLATION_HDR>())
                    {
                        hR_LEAVE_CANCELLATION_HDR = userCtx.Find(r =>
                            (r.LC_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                   // txtCancelDate.Enabled = false;
                    EntityData = hR_LEAVE_CANCELLATION_HDR;
                    HR_LEAVE_APPLICATIONS obj_HR_LEAVE_APPLICATIONS = new HR_LEAVE_APPLICATIONS();
                    using (IRepository<HR_LEAVE_APPLICATIONS> userCtx = new DataRepository<HR_LEAVE_APPLICATIONS>())
                    {
                        obj_HR_LEAVE_APPLICATIONS = userCtx.Find(r =>
                            (r.LEAVE_REQ_ID == hR_LEAVE_CANCELLATION_HDR.LEAVE_REQ_ID.ToString())
                            ).SingleOrDefault();
                    }

                    ddlFinancialYear.SelectedValue = obj_HR_LEAVE_APPLICATIONS.FISCAL_YEAR.ToString();
                    ddlDept.SelectedValue = hR_LEAVE_CANCELLATION_HDR.DEPT_ID;
                    fillEmp();
                    ddlStaffName.SelectedValue = hR_LEAVE_CANCELLATION_HDR.EMP_ID.ToString();
                    fillleavereq();
                    ddlCancelReason.SelectedValue = hR_LEAVE_CANCELLATION_HDR.CANCEL_REASON.ToString();

                    ddlLeaveApplication.SelectedValue = hR_LEAVE_CANCELLATION_HDR.LEAVE_REQ_ID.ToString();
                    ddlPayperiod.SelectedValue = hR_LEAVE_CANCELLATION_HDR.ATTRIBUTE2;
                    fillPayperiod();
                    getlevSal();
                    txtCancelDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_CANCELLATION_HDR.CANCEL_DATE.ToString());
                    txtLeaveFrom.Text = DBMethod.ConvertDateToString(hR_LEAVE_CANCELLATION_HDR.LEAVE_DATE_FROM.ToString());
                    txtLeaveTo.Text = DBMethod.ConvertDateToString(hR_LEAVE_CANCELLATION_HDR.LEAVE_DATE_TO.ToString());
                    txtNoOfDays.Text = hR_LEAVE_CANCELLATION_HDR.LC_NO_OF_DAYS.ToString();
                    txtRemarks.Text = hR_LEAVE_CANCELLATION_HDR.ATTRIBUTE1;

                    chkHalfDayFrom.Checked = true;
                    if (hR_LEAVE_CANCELLATION_HDR.HALF_DAY_FROM == FINAppConstants.N)
                    {
                        chkHalfDayFrom.Checked = false;
                    }

                    chkHalfDayTo.Checked = true;
                    if (hR_LEAVE_CANCELLATION_HDR.HALF_DAY_TO == FINAppConstants.N)
                    {
                        chkHalfDayTo.Checked = false;
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            // FIN.BLL.HR.Employee_BLL.GetEmpName(ref ddlStaffName);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDept);
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlCancelReason, "LCR");
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            FIN.BLL.PER.PayrollPeriods_BLL.GetPayrollWithoutApprovPeriods(ref ddlPayperiod);
        }

        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillleavereq();

        }
        private void fillleavereq()
        {
            FIN.BLL.HR.LeaveApplication_BLL.GetLeaveApplication4YearStafId(ref ddlLeaveApplication, ddlStaffName.SelectedValue, ddlFinancialYear.SelectedValue, Master.Mode, Master.StrRecordId);
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_CANCELLATION_HDR = (HR_LEAVE_CANCELLATION_HDR)EntityData;
                }


                hR_LEAVE_CANCELLATION_HDR.EMP_ID = ddlStaffName.SelectedValue;
                hR_LEAVE_CANCELLATION_HDR.DEPT_ID = ddlDept.SelectedValue;
                hR_LEAVE_CANCELLATION_HDR.LEAVE_REQ_ID = ddlLeaveApplication.SelectedValue;
                hR_LEAVE_CANCELLATION_HDR.CANCEL_REASON = ddlCancelReason.SelectedValue;
                hR_LEAVE_CANCELLATION_HDR.ATTRIBUTE2 = ddlPayperiod.SelectedValue;
                hR_LEAVE_CANCELLATION_HDR.CANCEL_DATE = DBMethod.ConvertStringToDate(txtCancelDate.Text.ToString());
                hR_LEAVE_CANCELLATION_HDR.LEAVE_DATE_FROM = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                hR_LEAVE_CANCELLATION_HDR.LEAVE_DATE_TO = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());
                hR_LEAVE_CANCELLATION_HDR.LC_NO_OF_DAYS = CommonUtils.ConvertStringToInt(txtNoOfDays.Text.ToString());
                hR_LEAVE_CANCELLATION_HDR.HALF_DAY_FROM = chkHalfDayFrom.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_LEAVE_CANCELLATION_HDR.HALF_DAY_TO = chkHalfDayTo.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_LEAVE_CANCELLATION_HDR.ATTRIBUTE1 = txtRemarks.Text;



                hR_LEAVE_CANCELLATION_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID.ToString();

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LEAVE_CANCELLATION_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_CANCELLATION_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_024.ToString(), false, true);
                    hfLC_HDR_ID.Value = hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID;
                    hR_LEAVE_CANCELLATION_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    hR_LEAVE_CANCELLATION_HDR.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_CANCELLATION_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_LEAVE_CANCELLATION_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();

                if (DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString()) > DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString()))
                {
                    ErrorCollection.Add("daterange", Prop_File_Data["daterangeTo_P"]);

                    return;

                }

                if (DBMethod.ConvertStringToDate(txtCancelDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString()))
                {
                    ErrorCollection.Add("daterange", "Cancel date must be less then from date");

                    return;

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlStaffName;
                slControls[1] = ddlCancelReason;
                slControls[2] = ddlLeaveApplication;
                slControls[3] = txtCancelDate;
                slControls[4] = txtLeaveFrom;
                slControls[5] = txtLeaveTo;


                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList~DropDownList~TextBox~TextBox~TextBox";
                string strMessage = Prop_File_Data["Staff_Name_P"] + " ~ " + Prop_File_Data["Cancel_Reason_P"] + " ~ " + Prop_File_Data["Leave_Application_P"] + " ~ " + Prop_File_Data["Cancel_Date_P"] + " ~ " + Prop_File_Data["Leave_From_P"] + " ~ " + Prop_File_Data["Leave_To_P"] + "";
                //string strMessage = "StaffName ~ Cancel Reason ~ Leave Application ~ Cancel Date ~ Leave From ~ Leave To";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }


                AssignToBE();


                DataTable dtdat = new DataTable();
                dtdat = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplDat(ddlLeaveApplication.SelectedValue)).Tables[0];

                hfLevFrm.Value = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_FROM"].ToString());
                hfLevto.Value = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_TO"].ToString());

                if (hfLevFrm.Value != txtLeaveFrom.Text)
                {
                    ErrorCollection.Add("INVDATE", "Invalid Date Range");
                    return;
                }

                if (DBMethod.ConvertStringToDate(hfLevFrm.Value) > DBMethod.ConvertStringToDate(txtLeaveFrom.Text))
                {
                    ErrorCollection.Add("INVDATE", "Invalid Date Range");
                    return;
                }

                if (DBMethod.ConvertStringToDate(hfLevto.Value) < DBMethod.ConvertStringToDate(txtLeaveTo.Text))
                {
                    ErrorCollection.Add("INVDATE", "Invalid Date Range");
                    return;
                }

                ErrorCollection.Clear();
                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.LeaveCancellation_DAL.GetSPFOR_DUPLICATE_CHECK(hR_LEAVE_CANCELLATION_HDR.EMP_ID, txtLeaveFrom.Text, txtLeaveTo.Text, hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID, hR_LEAVE_CANCELLATION_HDR.LEAVE_REQ_ID);


                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("LEAVECANCEL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                DataTable leaveCancel = new DataTable();
                string fiscalYr = string.Empty;
                string deptID = string.Empty;
                Month_data = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetMonth(txtLeaveFrom.Text)).Tables[0];
                month = int.Parse(Month_data.Rows[0][0].ToString());



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_LEAVE_CANCELLATION_HDR>(hR_LEAVE_CANCELLATION_HDR);
                            savedBool = true;

                            DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                            leaveCancel = DBMethod.ExecuteQuery(LeaveCancellation_DAL.GetLeaveCancelDtls(hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID.ToString())).Tables[0];
                            if (leaveCancel.Rows.Count > 0)
                            {
                                fiscalYr = leaveCancel.Rows[0]["fiscal_year"].ToString();
                                deptID = leaveCancel.Rows[0]["dept_id"].ToString();
                            }

                            DataTable dtData = new DataTable();
                            string leaveIdValue = string.Empty;
                            // dtData = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplication4YearStaffId(ddlStaffName.SelectedValue.ToString(), fiscalYr, ddlLeaveApplication.SelectedValue.ToString())).Tables[0];
                            dtData = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveID(ddlStaffName.SelectedValue.ToString(), fiscalYr, ddlLeaveApplication.SelectedValue.ToString())).Tables[0];
                            if (dtData != null)
                            {
                                if (dtData.Rows.Count > 0)
                                {
                                    leaveIdValue = dtData.Rows[0]["LEAVE_ID"].ToString();
                                }
                            }

                            DateTime dtfrmdt = new DateTime();
                            DateTime dtTodt = new DateTime();
                            dtfrmdt = Convert.ToDateTime(txtLeaveFrom.Text);
                            dtTodt = Convert.ToDateTime(txtLeaveTo.Text);

                            StaffLeaveDetails_DAL.UPDATE_EMP_LEAVE_BALANCE(fiscalYr, ddlStaffName.SelectedValue.ToString(), hR_LEAVE_CANCELLATION_HDR.ORG_ID.ToString(), leaveIdValue, deptID, decimal.Parse(hR_LEAVE_CANCELLATION_HDR.LC_NO_OF_DAYS.ToString()), "CANCELLATION");

                            StaffLeaveDetails_DAL.UPDATE_Staff_Attendance(fiscalYr, ddlStaffName.SelectedValue.ToString(), hR_LEAVE_CANCELLATION_HDR.ORG_ID.ToString(), ddlLeaveApplication.SelectedValue, deptID, dtfrmdt, dtTodt, hfLC_HDR_ID.Value);

                            //StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("LCANCEL", hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID.ToString(), "LCL", FrmDate, month, fiscalYr, ddlStaffName.SelectedValue.ToString(), hR_LEAVE_CANCELLATION_HDR.ORG_ID.ToString(), ddlLeaveApplication.SelectedItem.Text.ToString(), deptID, int.Parse(hR_LEAVE_CANCELLATION_HDR.LC_NO_OF_DAYS.ToString()));
                            StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("LCANCEL", hR_LEAVE_CANCELLATION_HDR.LC_HDR_ID.ToString(), "LCL", txtLeaveFrom.Text.ToString(), CommonUtils.ConvertStringToDecimal(month.ToString()), fiscalYr, ddlStaffName.SelectedValue.ToString(), hR_LEAVE_CANCELLATION_HDR.ORG_ID.ToString(), leaveIdValue, deptID, CommonUtils.ConvertStringToDecimal(hR_LEAVE_CANCELLATION_HDR.LC_NO_OF_DAYS.ToString()));

                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_LEAVE_CANCELLATION_HDR>(hR_LEAVE_CANCELLATION_HDR, true);
                            savedBool = true;
                            break;
                        }
                }





                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }



            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_LEAVE_CANCELLATION_HDR>(hR_LEAVE_CANCELLATION_HDR);

                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {

        }

        private void CalculateNoOfDays()
        {
            try
            {

                if (txtLeaveFrom.Text.Trim() != string.Empty && txtLeaveTo.Text.Trim() != string.Empty)
                {
                    System.Collections.SortedList slControls = new System.Collections.SortedList();
                    slControls[0] = txtLeaveFrom;
                    slControls[1] = txtLeaveTo;

                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


                    ErrorCollection.Clear();
                    string strCtrlTypes = FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_TIME;
                    string strMessage = "From Date~To Date";

                    ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                    if (ErrorCollection.Count > 0)
                        return;

                    DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                    DateTime ToDate = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());

                   

                    if (FrmDate == ToDate)
                    {
                        txtNoOfDays.Text = "1";
                    }
                    else
                    {
                        int count_friday = ClsGridBase.CountDays(DayOfWeek.Friday, FrmDate, ToDate);
                        txtNoOfDays.Text = (((ToDate - FrmDate).Days + 1) - count_friday).ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtLeaveTo_TextChanged(object sender, EventArgs e)
        {
            CalculateNoOfDays();
        }

        protected void ddlLeaveApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtdat = new DataTable();
            dtdat = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplDat(ddlLeaveApplication.SelectedValue)).Tables[0];
            txtLeaveFrom.Text = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_FROM"].ToString());
            txtLeaveTo.Text = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_TO"].ToString());

            hfLevFrm.Value = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_FROM"].ToString());
            hfLevto.Value = DBMethod.ConvertDateToString(dtdat.Rows[0]["LEAVE_DATE_TO"].ToString());

            CalculateNoOfDays();
            fillPayperiod();
            //getlevSal();
        }

        private void fillPayperiod()
        {
            DataTable dtlevAL = new DataTable();
            dtlevAL = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeavID_AL(ddlLeaveApplication.SelectedValue)).Tables[0];
            if (dtlevAL.Rows.Count > 0)
            {
                if (dtlevAL.Rows[0]["LEAVE_ID"].ToString() == "AL")
                {
                    IdPayPrd.Visible = true;
                }
                else
                {
                    IdPayPrd.Visible = false;
                }
            }
        }

        private void getlevSal()
        {
            DataTable dtdat = new DataTable();
            DataTable dtlevAL = new DataTable();
            DateTime dtfrmdt = new DateTime();
            DateTime dtTodt = new DateTime();
            dtdat = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeaveApplDat(ddlLeaveApplication.SelectedValue)).Tables[0];
            if (dtdat.Rows.Count > 0)
            {
                dtfrmdt = Convert.ToDateTime(dtdat.Rows[0]["LEAVE_DATE_FROM"].ToString());
                dtTodt = Convert.ToDateTime(dtdat.Rows[0]["LEAVE_DATE_TO"].ToString());
                dtlevAL = DBMethod.ExecuteQuery(LeaveApplication_DAL.GetLeavID_AL(ddlLeaveApplication.SelectedValue)).Tables[0];
                if (dtlevAL.Rows.Count > 0)
                {
                    if (dtlevAL.Rows[0]["LEAVE_ID"].ToString() == "AL")
                    {
                        txtLevSal.Text = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_Leave_Sal_app(ddlPayperiod.SelectedValue, ddlDept.SelectedValue, ddlStaffName.SelectedValue, dtfrmdt, dtTodt);
                        lbllevsal.Visible = true;
                        IDLevsal.Visible = true;
                    }
                    else
                    {
                        lbllevsal.Visible = false;
                        IDLevsal.Visible = false;
                    }
                }
            }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillEmp();
        }

        private void fillEmp()
        {
            FIN.BLL.HR.Employee_BLL.GetEmp_whohaveinLevapp(ref ddlStaffName, ddlDept.SelectedValue);

        }

        protected void ddlPayperiod_SelectedIndexChanged(object sender, EventArgs e)
        {
            getlevSal();
        }

    }
}