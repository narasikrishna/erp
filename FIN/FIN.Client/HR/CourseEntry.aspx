﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CourseEntry.aspx.cs" Inherits="FIN.Client.HR.CourseEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 100px" id="lblFinancialYear">
                Course
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCourse" TabIndex="1" MaxLength="200" runat="server" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
            <%-- <div class="lblBox" style="float: left; width: 150px" id="lblFromDate">
                Description
            </div>
            <div class="divtxtBox" style="float: left; width: 250px">
                <asp:TextBox ID="txtDescription" TabIndex="2" Width="250px" MaxLength="50" runat="server"
                    CssClass="RequiredField txtBox"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <%--
            <div class="lblBox" style="float: left; width: 100px" id="lblDepartment">
                Group
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
               <asp:DropDownList ID="ddlGroup" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="250px">
                </asp:DropDownList>
                 <asp:TextBox ID="txtGroup" TabIndex="2"   MaxLength="50" runat="server"
                    CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
            --%>
            <div class="lblBox LNOrient" style=" width: 100px" id="lblAttendanceDate">
                Duration
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtDuration" TabIndex="2" MaxLength="2" runat="server" CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0"
                    FilterType="Numbers,Custom" TargetControlID="txtDuration" />
            </div>
        </div>
      
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style=" width: 100px" id="lblCourse">
                Course Details:
            </div>
            <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient" >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="SUBJECT_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Course Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSubCode" MaxLength="5" runat="server" CssClass="RequiredField txtBox"
                                TabIndex="8" Width="95%" Text='<%# Eval("SUB_CODE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSubCode" MaxLength="5" runat="server" CssClass="RequiredField txtBox"
                                TabIndex="3" Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSubCode" Width="95%" Enabled="false" runat="server" Text='<%# Eval("SUB_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesc" Width="270px" MaxLength="200" runat="server" TabIndex="9" TextMode="MultiLine" Wrap="true"
                                CssClass="RequiredField txtBox" Text='<%# Eval("SUBJECT_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDesc" Width="270px" TextMode="MultiLine" Wrap="true" MaxLength="200" runat="server" TabIndex="4"
                                CssClass="RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSAttendanceType" Width="270px" runat="server" style="word-wrap:break-word;white-space: pre-wrap; height:100%" 
                            Text='<%# Eval("SUBJECT_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Facility">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFaility"  runat="server" CssClass="EntryFont RequiredField ddlStype"  Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFaility"  runat="server" CssClass="EntryFont RequiredField ddlStype"  Width="150px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFaility" Width="130px" runat="server" Text='<%# Eval("FACILITY_DTL") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <%-- <asp:TemplateField HeaderText="Instructor">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkInstructor" runat="server"  Checked='<%# Convert.ToBoolean(Eval("INSTRUCTOR_FLAG")) %>'  />
                        </EditItemTemplate>
                        <FooterTemplate>
                         <asp:CheckBox ID="chkInstructor" runat="server" Checked="True"  />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkInstructor" runat="server"  Checked='<%# Convert.ToBoolean(Eval("INSTRUCTOR_FLAG")) %>'  />
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CBT">
                        <EditItemTemplate>
                          <asp:CheckBox ID="chkCBT" runat="server"   Checked='<%# Convert.ToBoolean(Eval("CBT_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                           <asp:CheckBox ID="chkCBT" runat="server" Checked="True"  />
                        </FooterTemplate>
                        <ItemTemplate>
                              <asp:CheckBox ID="chkCBT" runat="server"  Checked='<%# Convert.ToBoolean(Eval("CBT_FLAG")) %>' />
                        </ItemTemplate>
                      
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" TabIndex="6" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" TabIndex="7"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" TabIndex="10"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" TabIndex="11"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" TabIndex="5"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
