﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="OverTimeSelection.aspx.cs" Inherits="FIN.Client.HR.OverTimeSelection" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function shrinkandgrow(input) {
            var displayIcon = "img" + input;
            if ($("#" + displayIcon).attr("src") == "../Images/expand_blue.png") {
                $("#" + displayIcon).closest("tr")
			    .after("<tr><td></td><td colspan = '100%'>" + $("#" + input)
			    .html() + "</td></tr>");
                $("#" + displayIcon).attr("src", "../Images/collapse_blue.png");
            } else {
                $("#" + displayIcon).closest("tr").next().remove();
                $("#" + displayIcon).attr("src", "../Images/expand_blue.png");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender2" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                 <asp:ImageButton ID="btnget" runat="server" ImageUrl="~/Images/btnGet.png" OnClick="btnget_Click" TabIndex="3"
                    Style="border: 0px;" />
               <%-- <asp:Button ID="btnget" runat="server" Width="120px" Text="Get" CssClass="btn" OnClick="btnget_Click"
                    TabIndex="3" /></div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvParentData" runat="server" AutoGenerateColumns="False" Width="900px"
                CssClass="DisplayFont Grid" ShowFooter="false" DataKeyNames="emp_no,DEPT_ID,DEPT_DESIG_ID,EMP_ID,OVERTIME_HOURS,OVERTIME_EMP_GROUP">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="40px" Visible="false">
                        <ItemTemplate>
                            <%--<a href="JavaScript:shrinkandgrow('div<%# Eval("EMP_ID") %>');">
                                <img alt="Details" id="imgdiv<%# Eval("EMP_ID") %>" src="../Images/expand_blue.png" />
                            </a>--%>
                            <div id="div<%# Eval("EMP_ID") %>" style="display: none;">
                                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                    Width="800px" DataKeyNames="emp_no,OVERTIME_PAID,overtime_id,DEPT_ID,DEPT_DESIG_ID,EMP_ID,OVERTIME_DATE,OVERTIME_HOURS,OVERTIME_EMP_GROUP">
                                    <Columns>
                                        <asp:BoundField DataField="OVERTIME_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:TemplateField HeaderText="Over Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOverTime" Width="150px" runat="server" Text='<%# Eval("OVERTIME_HOURS") %>'></asp:Label>
                                                <asp:Label ID="lblId" Visible="false" Width="150px" runat="server" Text='<%# Eval("overtime_id") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Over Time Amount">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtAmount" Enabled="false" MaxLength="13" Width="150px"
                                                    CssClass="RequiredField txtBox_N" Text='<%# Eval("OVERTIME_AMOUNT") %>'></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxsExtender7" runat="server" ValidChars="."
                                                    FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Amount">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtPaidAmount" MaxLength="13" Width="150px" CssClass="RequiredField txtBox_N"
                                                    OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" Text='<%# Eval("OVERTIME_PAID_AMOUNT") %>'></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="FtilteredTextBoxsssExtender7" runat="server" ValidChars="."
                                                    FilterType="Numbers,Custom" TargetControlID="txtPaidAmount" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" OnCheckedChanged="chkActive_CheckedChanged" AutoPostBack="true"
                                                    ID="chkSelection" Checked='<%# Convert.ToBoolean(Eval("OVERTIME_PAID")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DEPT_NAME" HeaderText="Department Name" />
                    <asp:BoundField DataField="DESIG_NAME" HeaderText="Designation Name" />
                    <asp:BoundField DataField="OVERTIME_EMP_GROUP" HeaderText="Group"></asp:BoundField>
                    <asp:BoundField DataField="emp_no" HeaderText="Employee No"></asp:BoundField>
                    <asp:BoundField DataField="EMP_FIRST_NAME" HeaderText="Employee Name"></asp:BoundField>
                    <asp:BoundField DataField="OVERTIME_HOURS" HeaderText="Total Overtime Hours">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                OnClick="btnDetails_Click" CommandName="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 600px;">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="btnDetails1"
                PopupControlID="Panel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="500px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td>
                                <asp:Label runat="server" ID="lblErrorMsg" Visible="false" Text="No Record Found"
                                    Style="color: Red;"></asp:Label>
                            </td>
                        </tr>
                        <tr class="LNOrient">
                            <td colspan="4">
                                <div id="div_ABB">
                                    <asp:GridView ID="gvDataChild" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="800px" OnRowDataBound="gvData_RowDataBound" DataKeyNames="emp_no,OVERTIME_PAID,overtime_id,DEPT_ID,DEPT_DESIG_ID,EMP_ID,OVERTIME_DATE,OVERTIME_HOURS,OVERTIME_EMP_GROUP">
                                        <Columns>
                                            <asp:BoundField DataField="OVERTIME_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:TemplateField HeaderText="Over Time(In Minutes)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" Visible="false" Width="150px" runat="server" Text='<%# Eval("overtime_id") %>'></asp:Label>
                                                    <asp:Label ID="lblOverTime" Width="150px" runat="server" Text='<%# Eval("OVERTIME_HOURS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Over Time Amount">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txt1Amount" Enabled="false" MaxLength="13" Width="150px"
                                                        CssClass="RequiredField txtBox_N" Text='<%# Eval("OVERTIME_AMOUNT") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxsExtender7" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txt1Amount" />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Paid To Payroll">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true"
                                                        ID="txtPaidAmount" MaxLength="13" Width="150px" CssClass="RequiredField txtBox_N"
                                                        Text='<%# Eval("OVERTIME_PAID_AMOUNT") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FtilteredTextBoxsssExtender7" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtPaidAmount" />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Select">
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" OnCheckedChanged="chkActive_CheckedChanged" AutoPostBack="true"
                                                        ID="chkSelection" Checked='<%# Convert.ToBoolean(Eval("OVERTIME_PAID")) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: left">
                                    <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Ok"
                                        OnClick="btnOk_Click" Width="60px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
