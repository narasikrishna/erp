﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.DAL;
using FIN.BLL.SSM;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class TrainingCostEntry : PageBase
    {
        HR_TRAINING_COST_HDR hR_TRAINING_COST_HDR = new HR_TRAINING_COST_HDR();
        HR_TRAINING_COST_DTL hR_TRAINING_COST_DTL = new HR_TRAINING_COST_DTL();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(TrainingCost_DAL.GetTrainingCostDtls(Master.StrRecordId.ToString())).Tables[0];
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRAINING_COST_HDR> userCtx = new DataRepository<HR_TRAINING_COST_HDR>())
                    {
                        hR_TRAINING_COST_HDR = userCtx.Find(r =>
                            (r.COST_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRAINING_COST_HDR;
                    ddlDepartment.SelectedValue = hR_TRAINING_COST_HDR.COST_DEPT_ID;
                    ddlProgram.SelectedValue = hR_TRAINING_COST_HDR.COST_PROG_ID;
                    FILLBudget();
                    ddlBudget.SelectedValue = hR_TRAINING_COST_HDR.COST_BUDGET_ID;
                    txtInvoiceAmount.Text = hR_TRAINING_COST_HDR.ATTRIBUTE10;
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                    // txtKFAS.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_TRAINING_COST_HDR.COVERED_KFAS.ToString());
                    fillBudgetDtls();
                    if (hR_TRAINING_COST_HDR.COST_EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_TRAINING_COST_HDR.COST_EFFECTIVE_FROM_DT.ToString());
                    }
                    if (hR_TRAINING_COST_HDR.COST_EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_TRAINING_COST_HDR.COST_EFFECTIVE_TO_DT.ToString());
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            Program_BLL.fn_getProgrameName(ref ddlProgram);
            //  TrainingCost_BLL.GetBudgetName(ref ddlBudget);



        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRAINING_COST_HDR = (HR_TRAINING_COST_HDR)EntityData;
                }

                hR_TRAINING_COST_HDR.COST_DEPT_ID = ddlDepartment.SelectedValue;
                hR_TRAINING_COST_HDR.COST_PROG_ID = ddlProgram.SelectedValue;
                hR_TRAINING_COST_HDR.COST_BUDGET_ID = ddlBudget.SelectedValue;
                hR_TRAINING_COST_HDR.ATTRIBUTE10 = txtInvoiceAmount.Text;
                //   hR_TRAINING_COST_HDR.COVERED_KFAS = decimal.Parse(txtKFAS.Text.ToString());

                if (txtFromDate.Text != string.Empty)
                {
                    hR_TRAINING_COST_HDR.COST_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_TRAINING_COST_HDR.COST_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                else
                {
                    hR_TRAINING_COST_HDR.COST_EFFECTIVE_TO_DT = null;
                }
                hR_TRAINING_COST_HDR.COST_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                hR_TRAINING_COST_HDR.ENABLED_FLAG = FINAppConstants.Y;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRAINING_COST_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_TRAINING_COST_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_TRAINING_COST_HDR.COST_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_090_M.ToString(), false, true);
                    // hR_APPLICANTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_COMPETENCY_HDR_SEQ);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRAINING_COST_HDR.CREATED_BY = this.LoggedUserName;
                    hR_TRAINING_COST_HDR.CREATED_DATE = DateTime.Today;

                }

                hR_TRAINING_COST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRAINING_COST_HDR.COST_HDR_ID);
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRAINING_COST_DTL = new HR_TRAINING_COST_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_TRAINING_COST_DTL> userCtx = new DataRepository<HR_TRAINING_COST_DTL>())
                        {
                            hR_TRAINING_COST_DTL = userCtx.Find(r =>
                                (r.COST_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }



                    hR_TRAINING_COST_DTL.COST_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.COST_TYPE].ToString();

                    hR_TRAINING_COST_DTL.COST_ITEM_DESC = dtGridData.Rows[iLoop][FINColumnConstants.COST_ITEM_DESC].ToString();
                    hR_TRAINING_COST_DTL.COST_ITEM_REMARK = dtGridData.Rows[iLoop][FINColumnConstants.COST_ITEM_REMARK].ToString();
                    hR_TRAINING_COST_DTL.COST_QTY = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.COST_QTY].ToString());
                    hR_TRAINING_COST_DTL.COST_UNIT_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.COST_UNIT_PRICE].ToString());
                    hR_TRAINING_COST_DTL.COST_CURRENCY = dtGridData.Rows[iLoop][FINColumnConstants.COST_CURRENCY].ToString();
                    hR_TRAINING_COST_DTL.COST_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.COST_AMOUNT].ToString());
                    if (dtGridData.Rows[iLoop]["COVERED_KFAS"] != null)
                    {
                        hR_TRAINING_COST_DTL.COVERED_KFAS = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["COVERED_KFAS"].ToString());
                    }
                    else
                    {
                        hR_TRAINING_COST_DTL.COVERED_KFAS = null;
                    }
                    hR_TRAINING_COST_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    hR_TRAINING_COST_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRAINING_COST_DTL.COST_HDR_ID = hR_TRAINING_COST_HDR.COST_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_TRAINING_COST_DTL.COST_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRAINING_COST_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString() != string.Empty)
                        {
                            hR_TRAINING_COST_DTL.COST_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.COST_DTL_ID].ToString();
                            hR_TRAINING_COST_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_TRAINING_COST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRAINING_COST_DTL, "U"));

                        }
                        else
                        {
                            hR_TRAINING_COST_DTL.COST_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_090_D.ToString(), false, true);
                            hR_TRAINING_COST_DTL.CREATED_BY = this.LoggedUserName;
                            hR_TRAINING_COST_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRAINING_COST_DTL, "A"));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_TRAINING_COST_HDR, HR_TRAINING_COST_DTL>(hR_TRAINING_COST_HDR, tmpChildEntity, hR_TRAINING_COST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_TRAINING_COST_HDR, HR_TRAINING_COST_DTL>(hR_TRAINING_COST_HDR, tmpChildEntity, hR_TRAINING_COST_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_CostType = tmpgvr.FindControl("ddlCostType") as DropDownList;
                DropDownList ddl_Currency = tmpgvr.FindControl("ddlCurrency") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddl_CostType, "COST_TYPE");
                Country_BLL.GetCurrencyDetails(ref ddl_Currency);



                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_CostType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COST_TYPE"].ToString();
                    ddl_Currency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COST_CURRENCY"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Applicant");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_TRAINING_COST_HDR>(hR_TRAINING_COST_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }







        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_CostType = gvr.FindControl("ddlCostType") as DropDownList;
            TextBox txt_Item = gvr.FindControl("txtItem") as TextBox;
            TextBox txt_Remarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox txt_Quantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txt_UnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            DropDownList ddl_Currency = gvr.FindControl("ddlCurrency") as DropDownList;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            TextBox txtKFAS = gvr.FindControl("txtKFAS") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.COST_DTL_ID] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_CostType;
            slControls[1] = txt_Item;
            slControls[2] = txt_Remarks;
            slControls[3] = txt_Quantity;
            slControls[4] = txt_UnitPrice;
            slControls[5] = ddl_Currency;
            slControls[6] = txt_Amount;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Cost_Type_P"] + " ~ " + Prop_File_Data["Item_P"] + " ~ " + Prop_File_Data["Remarks_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Currency_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
            //string strMessage = " Cost Type ~ Item ~ Remarks  ~ Quantity ~ Unit Price ~ Currency ~ Amount ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            // string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementName.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            //if (ddl_ElementName.SelectedItem != null)
            //{
            //    drList[FINColumnConstants.PAY_ELEMENT_ID] = ddl_ElementName.SelectedItem.Value;
            //    drList[FINColumnConstants.PAY_ELEMENT] = ddl_ElementName.SelectedItem.Text;
            //}
            drList["COST_TYPE"] = ddl_CostType.SelectedValue.ToString();
            drList["CT_DESC"] = ddl_CostType.SelectedItem.Text.ToString();
            drList["COST_ITEM_DESC"] = txt_Item.Text;
            drList["COST_ITEM_REMARK"] = txt_Remarks.Text;
            drList["COST_QTY"] = txt_Quantity.Text;
            drList["COST_UNIT_PRICE"] = txt_UnitPrice.Text;
            drList["COST_CURRENCY"] = ddl_Currency.SelectedValue.ToString();
            drList["CURRENCY_DESC"] = ddl_Currency.SelectedItem.Text;
            drList["COST_AMOUNT"] = txt_Amount.Text;
            if (txtKFAS.Text.ToString().Length > 0)
            {
                drList["COVERED_KFAS"] = txtKFAS.Text;
            }
            else
            {
                drList["COVERED_KFAS"] = DBNull.Value;
            }
            drList[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.EnabledFlag;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void fillBudgetDtls()
        {
            CostDate.Visible = true;
            DataTable dtAmount = new DataTable();
            dtAmount = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingCost_DAL.getBudgetDtls(ddlBudget.SelectedValue)).Tables[0];
            txtAmount.Text = dtAmount.Rows[0]["TRN_BUDGET_AMOUNT"].ToString();
            txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
            txtFromDate.Text = DBMethod.ConvertDateToString(dtAmount.Rows[0]["TRN_EFFECTIVE_FROM_DT"].ToString());
            txtToDate.Text = "";
            if (dtAmount.Rows[0]["TRN_EFFECTIVE_TO_DT"] != null)
            {
                if (dtAmount.Rows[0]["TRN_EFFECTIVE_TO_DT"].ToString().Length > 0)
                {
                    txtToDate.Text = DBMethod.ConvertDateToString(dtAmount.Rows[0]["TRN_EFFECTIVE_TO_DT"].ToString());
                }
            }
            else
            {
                txtToDate.Text = "";
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    //dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("COST_QTY", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("COST_QTY"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("COST_UNIT_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("COST_UNIT_PRICE"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("COST_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("COST_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["enabled_flag"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                txtInvoiceAmount.Text = CommonUtils.CalculateTotalAmount(dtData, FINColumnConstants.COST_AMOUNT);

                if (txtInvoiceAmount.Text.Length > 0)
                {
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                }

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlBudget_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillBudgetDtls();
        }
        protected void txtUnitPrice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            CalSumofTotal(gvr);

        }
        private void CalSumofTotal(GridViewRow gvr)
        {
            TextBox txt_Quantity = (TextBox)gvr.FindControl("txtQuantity");
            TextBox txt_UnitPrice = (TextBox)gvr.FindControl("txtUnitPrice");
            TextBox txt_Amount = (TextBox)gvr.FindControl("txtAmount");
            if (txt_Quantity.Text.ToString().Length > 0 && txt_UnitPrice.Text.ToString().Length > 0)
            {
                txt_Amount.Text = (CommonUtils.ConvertStringToDecimal(txt_Quantity.Text.ToString()) * CommonUtils.ConvertStringToDecimal(txt_UnitPrice.Text.ToString())).ToString();
                txt_Amount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txt_Amount.Text);
            }

        }

        protected void ddlProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLBudget();
        }

        private void FILLBudget()
        {
            TrainingCost_BLL.GetBudgetName(ref ddlBudget, ddlProgram.SelectedValue);
        }


    }





}
