﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class TrainingScheduleEntry : PageBase
    {

        HR_TRM_SCHEDULE_HDR hR_TRM_SCHEDULE_HDR = new HR_TRM_SCHEDULE_HDR();
        HR_TRM_SCHEDULE_DTL hR_TRM_SCHEDULE_DTL = new HR_TRM_SCHEDULE_DTL();
        string ProReturn = null;
        Boolean saveBool = false;
        string TrnReqHdrID = null;
        string TrnReqDtlID = null;

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlTrainingType, "TRAIN_TYPE");
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.TrainingSchedule_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_TRM_SCHEDULE_HDR> userCtx = new DataRepository<HR_TRM_SCHEDULE_HDR>())
                    {
                        hR_TRM_SCHEDULE_HDR = userCtx.Find(r =>
                            (r.TRN_SCH_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_SCHEDULE_HDR;

                    //  txtScheduleID.Text = hR_TRM_SCHEDULE_HDR.TRN_SCH_HDR_ID;
                    txtdesc.Text = hR_TRM_SCHEDULE_HDR.TRN_DESC;
                    if (hR_TRM_SCHEDULE_HDR.TRN_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_TRM_SCHEDULE_HDR.TRN_FROM_DT.ToString());
                    }
                    if (hR_TRM_SCHEDULE_HDR.TRN_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_TRM_SCHEDULE_HDR.TRN_TO_DT.ToString());
                    }

                    ddlTrainingType.SelectedValue = hR_TRM_SCHEDULE_HDR.TRAINING_TYPE;
                    txtNoOfDays.Text = hR_TRM_SCHEDULE_HDR.NO_OF_DAYS.ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_SCHEDULE_HDR = (HR_TRM_SCHEDULE_HDR)EntityData;
                }


                hR_TRM_SCHEDULE_HDR.TRN_DESC = txtdesc.Text;
                if (txtFromDate.Text != string.Empty)
                {
                    hR_TRM_SCHEDULE_HDR.TRN_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_TRM_SCHEDULE_HDR.TRN_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                hR_TRM_SCHEDULE_HDR.TRAINING_TYPE = ddlTrainingType.SelectedValue;
                hR_TRM_SCHEDULE_HDR.NO_OF_DAYS = decimal.Parse(txtNoOfDays.Text.ToString());

                hR_TRM_SCHEDULE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_TRM_SCHEDULE_HDR.TRN_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_SCHEDULE_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_SCHEDULE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_TRM_SCHEDULE_HDR.TRN_SCH_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_058_M".ToString(), false, true);
                    //hR_TRM_SCHEDULE_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    hR_TRM_SCHEDULE_HDR.CREATED_BY = this.LoggedUserName;
                    hR_TRM_SCHEDULE_HDR.CREATED_DATE = DateTime.Today;
                }

                hR_TRM_SCHEDULE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_SCHEDULE_HDR.TRN_SCH_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRM_SCHEDULE_DTL = new HR_TRM_SCHEDULE_DTL();
                    if (dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_SCHEDULE_DTL> userCtx = new DataRepository<HR_TRM_SCHEDULE_DTL>())
                        {
                            hR_TRM_SCHEDULE_DTL = userCtx.Find(r =>
                                (r.TRN_SCH_DTL_ID == dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_TRM_SCHEDULE_DTL.COM_LINE_NUM = (iLoop + 1);
                    hR_TRM_SCHEDULE_DTL.PROG_COURSE_ID = dtGridData.Rows[iLoop]["COURSE_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.PROG_ID = dtGridData.Rows[iLoop]["PROG_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.SUBJECT_ID = dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.TRN_SESSION = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.REQ_HDR_ID = dtGridData.Rows[iLoop]["REQ_HDR_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.REQ_DTL_ID = dtGridData.Rows[iLoop]["REQ_DTL_ID"].ToString();
                    hR_TRM_SCHEDULE_DTL.TRNR_ID = dtGridData.Rows[iLoop]["EMP_ID"].ToString();

                    if (dtGridData.Rows[iLoop]["TRN_DATE"] != DBNull.Value)
                    {
                        hR_TRM_SCHEDULE_DTL.TRN_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["TRN_DATE"].ToString());
                    }

                    hR_TRM_SCHEDULE_DTL.TRN_SCH_HDR_ID = hR_TRM_SCHEDULE_HDR.TRN_SCH_HDR_ID;

                    hR_TRM_SCHEDULE_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRM_SCHEDULE_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    AssignReqHdrRegDtID();
                    //hR_TRM_SCHEDULE_DTL.REQ_HDR_ID = TrnReqHdrID;
                    //hR_TRM_SCHEDULE_DTL.REQ_DTL_ID = TrnReqDtlID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SCHEDULE_DTL, "D"));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_TRM_SCHEDULE_DTL.TRN_SCH_DTL_ID = dtGridData.Rows[iLoop]["TRN_SCH_DTL_ID"].ToString();
                            hR_TRM_SCHEDULE_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_TRM_SCHEDULE_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SCHEDULE_DTL, "U"));

                        }
                        else
                        {

                            hR_TRM_SCHEDULE_DTL.TRN_SCH_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_058_D".ToString(), false, true);
                            hR_TRM_SCHEDULE_DTL.CREATED_BY = this.LoggedUserName;
                            hR_TRM_SCHEDULE_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SCHEDULE_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_SCHEDULE_HDR, HR_TRM_SCHEDULE_DTL>(hR_TRM_SCHEDULE_HDR, tmpChildEntity, hR_TRM_SCHEDULE_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_SCHEDULE_HDR, HR_TRM_SCHEDULE_DTL>(hR_TRM_SCHEDULE_HDR, tmpChildEntity, hR_TRM_SCHEDULE_DTL, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void ddlProg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                fillCourse(gvr);
                fillreqtyp(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fillCourse(GridViewRow gvr)
        {
          //  GridViewRow tmpgvr = gvData.FooterRow;

            DropDownList ddlCourse = gvr.FindControl("ddlCourse") as DropDownList;
            DropDownList ddlProg = gvr.FindControl("ddlProg") as DropDownList;

            FIN.BLL.HR.Course_BLL.GetCourseBasedPgm(ref ddlCourse, ddlProg.SelectedValue);

        }

        private void fillreqtyp(GridViewRow gvr)
        {
            //GridViewRow tmpgvr = gvData.FooterRow;

            DropDownList ddlReqType = gvr.FindControl("ddlReqType") as DropDownList;
            DropDownList ddlProg = gvr.FindControl("ddlProg") as DropDownList;
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetRequestType(ref ddlReqType, ddlProg.SelectedValue);
        }

        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                fillsubject(gvr);
                
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fillsubject(GridViewRow gvr)
        {
            //GridViewRow tmpgvr = gvData.FooterRow;

            DropDownList ddlCourse = gvr.FindControl("ddlCourse") as DropDownList;
            DropDownList ddlSubject = gvr.FindControl("ddlSubject") as DropDownList;

            FIN.BLL.HR.Course_BLL.GetSubjectBasedCourse(ref ddlSubject, ddlCourse.SelectedValue);
        }

        protected void ddlReqType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                fillreqno(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void fillreqno(GridViewRow gvr)
        {
            //GridViewRow tmpgvr = gvData.FooterRow;

            DropDownList ddlReqType = gvr.FindControl("ddlReqType") as DropDownList;
            DropDownList ddlReqDtl = gvr.FindControl("ddlReqDtl") as DropDownList;

            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetRequestDtls(ref ddlReqDtl, ddlReqType.SelectedValue);
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
                DropDownList ddlProg = tmpgvr.FindControl("ddlProg") as DropDownList;
                DropDownList ddlSubject = tmpgvr.FindControl("ddlSubject") as DropDownList;
                DropDownList ddlSession = tmpgvr.FindControl("ddlSession") as DropDownList;
                DropDownList ddlReqType = tmpgvr.FindControl("ddlReqType") as DropDownList;
                DropDownList ddlReqDtl = tmpgvr.FindControl("ddlReqDtl") as DropDownList;
                DropDownList ddlTrainer = tmpgvr.FindControl("ddlTrainer") as DropDownList;

            //    FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse(ref ddlCourse);
               FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram(ref ddlProg);
              //  FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject(ref ddlSubject);
                Lookup_BLL.GetLookUpValues(ref ddlSession, "Session");
                Employee_BLL.GetEmployeeTrainerName(ref ddlTrainer);

               // FIN.BLL.HR.TrainingSchedule_BLL.fn_GetRequestType(ref ddlReqType);
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetRequestDtls(ref ddlReqDtl);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlProg.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PROG_ID"].ToString();
                    fillCourse(tmpgvr);
                    fillreqtyp(tmpgvr);
                    ddlReqType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["REQ_HDR_ID"].ToString();
                    fillreqno(tmpgvr);
                    ddlReqDtl.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["REQ_DTL_ID"].ToString();
                    ddlTrainer.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["EMP_ID"].ToString();
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COURSE_ID"].ToString();
                    fillsubject(tmpgvr);
                    ddlSubject.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["SUBJECT_ID"].ToString();
                    ddlSession.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOOKUP_ID"].ToString();
                  

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();


                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Schedule Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            DropDownList ddlCourse = gvr.FindControl("ddlCourse") as DropDownList;
            DropDownList ddlProg = gvr.FindControl("ddlProg") as DropDownList;
            DropDownList ddlSubject = gvr.FindControl("ddlSubject") as DropDownList;
            DropDownList ddlSession = gvr.FindControl("ddlSession") as DropDownList;
            DropDownList ddlReqType = gvr.FindControl("ddlReqType") as DropDownList;
            DropDownList ddlReqDtls = gvr.FindControl("ddlReqDtl") as DropDownList;
            DropDownList ddlTrainer = gvr.FindControl("ddlTrainer") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["TRN_SCH_DTL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = dtpStartDate;
            slControls[1] = ddlCourse;
            //slControls[2] = ddlProg;
            slControls[2] = ddlSubject;
            slControls[3] = ddlSession;
            slControls[4] = ddlReqType;
            slControls[5] = ddlReqDtls;
            slControls[6] = ddlTrainer;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~DropDownList~DropDownList~DropDownList~DropDownList~DropDownList~DropDownList";
            string strMessage = Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Course_P"] + " ~ " + Prop_File_Data["Subject_P"] + " ~ " + Prop_File_Data["Session_P"] + " ~ " + Prop_File_Data["Request_Type_P"] + "~" + "Request Id~Trainer";
            //string strMessage = "Date ~ Course  ~ Subject ~ Session ~ Request Type";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            ErrorCollection = TrainingSchedule_BLL.ValidateDateRange(dt_tmp, gvr.RowIndex, DBMethod.ConvertStringToDate(txtFromDate.Text.ToString()), DBMethod.ConvertStringToDate(txtToDate.Text.ToString()), DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString()), "TRN_DATE");

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            string strCondition = FINColumnConstants.SUBJECT_ID + "='" + ddlSubject.SelectedValue.ToString() + "'";
                //FINColumnConstants.PROG_ID + "='" + ddlProg.SelectedValue.ToString() + "'";
           // strCondition += " AND " + FINColumnConstants.SUBJECT_ID + "='" + ddlSubject.SelectedValue.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.COURSE_ID + "='" + ddlCourse.SelectedItem.Value.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.TRN_DATE + "='" + DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString()) + "'";
            strCondition += " AND " + FINColumnConstants.LOOKUP_ID + "='" + ddlSession.SelectedValue + "'";


            strMessage = FINMessageConstatns.RecordAlreadyExists;
            //  ErrorCollection = Organisation_BLL.DataDuplication(dt_tmp, strCondition, strMessage, DBMethod.ConvertStringToDate(dtpStartDate.Text), ddlProg.SelectedValue.ToString(), ddlSubject.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString());
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["TRN_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (ddlReqType.SelectedItem != null)
            {
                drList["REQ_HDR_ID"] = ddlReqType.SelectedItem.Value;
                drList["REQ_TYPE"] = ddlReqType.SelectedItem.Text;
            }
            if (ddlReqDtls.SelectedItem != null)
            {
                drList["REQ_DTL_ID"] = ddlReqDtls.SelectedItem.Value;
                drList["REQ_DTLS"] = ddlReqDtls.SelectedItem.Text;
            }

            if (ddlCourse.SelectedItem != null)
            {
                drList["COURSE_ID"] = ddlCourse.SelectedItem.Value;
                drList["COURSE_DESC"] = ddlCourse.SelectedItem.Text;
            }
            if (ddlProg.SelectedItem != null)
            {
                drList["PROG_ID"] = ddlProg.SelectedItem.Value;
                drList["PROG_DESC"] = ddlProg.SelectedItem.Text;
            }
            if (ddlSubject.SelectedItem != null)
            {
                drList["SUBJECT_ID"] = ddlSubject.SelectedItem.Value;
                drList["SUBJECT_DESC"] = ddlSubject.SelectedItem.Text;
            }
            if (ddlSession.SelectedItem != null)
            {
                drList["LOOKUP_ID"] = ddlSession.SelectedItem.Value;
                drList["LOOKUP_NAME"] = ddlSession.SelectedItem.Text;
            }
            if (ddlTrainer.SelectedItem != null)
            {
                drList["EMP_ID"] = ddlTrainer.SelectedItem.Value;
                drList["TRAINER"] = ddlTrainer.SelectedItem.Text;
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_TRM_SCHEDULE_HDR>(hR_TRM_SCHEDULE_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void AssignReqHdrRegDtID()
        {
            //DataTable dtTrngReqDts = new DataTable();
            //dtTrngReqDts = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingSchedule_DAL.GetTrngReqHdrDtlID4ProgDeptEmp(ddlEmpNumber.SelectedValue.ToString())).Tables[0];
            //TrnReqHdrID = dtTrngReqDts.Rows[0]["REQ_HDR_ID"].ToString();
            //TrnReqDtlID = dtTrngReqDts.Rows[0]["REQ_DTL_ID"].ToString();
        }

        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            if (txtFromDate.Text.Trim() != string.Empty && txtToDate.Text.Trim() != string.Empty)
            {
                DateTime FrmDate = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                DateTime ToDate = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());

                if (FrmDate == ToDate)
                {
                    txtNoOfDays.Text = "1";
                }
                else
                {
                    txtNoOfDays.Text = ((ToDate - FrmDate).Days + 1).ToString();
                }
            }
        }



    }

}