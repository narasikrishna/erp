﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using FIN.DAL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ResignationInterviewEntry : PageBase
    {

        HR_EXIT_INTERVIEW_HDR hR_EXIT_INTERVIEW_HDR = new HR_EXIT_INTERVIEW_HDR();
        HR_EXIT_INTERVIEW_DTL hR_EXIT_INTERVIEW_DTL = new HR_EXIT_INTERVIEW_DTL();
        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();

        ResignationInterview_BLL ResignationInterview_BLL = new ResignationInterview_BLL();
        ResignationInterview_DAL ResignationInterview_DAL = new ResignationInterview_DAL();

        DataTable dtGridData = new DataTable();
        DataTable dtResigreasons = new DataTable();

        Boolean bol_rowVisiable;
        Boolean saveBool;
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            ResignationInterview_BLL.fn_GetResigRequestdtls(ref ddlRequestNumber);
            ResignationInterview_BLL.fn_GetDepartmentDetails(ref ddldeptname);
            //ResignationInterview_BLL.fn_GetEmployeeDetails(ref ddlConductedBy);

            DataTable dtDropDownData = new DataTable();
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("RES_RESIG")).Tables[0];
            chkReasons.DataSource = dtDropDownData;
            chkReasons.DataTextField = "LOOKUP_NAME";
            chkReasons.DataValueField = "LOOKUP_ID";
            chkReasons.DataBind();

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                //txtReason.Enabled = false;
                txtRemarks.Enabled = false;
                txtRequestDate.Enabled = false;

                FillComboBox();
                EntityData = null;


                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationInterview_DAL.getIntervQusdtls()).Tables[0];
                BindGrid(dtGridData);



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    dtGridData = FIN.BLL.HR.ResignationInterview_BLL.getChildEntityDet(Master.StrRecordId);
                    BindGrid(dtGridData);
                    using (IRepository<HR_EXIT_INTERVIEW_HDR> userCtx = new DataRepository<HR_EXIT_INTERVIEW_HDR>())
                    {
                        hR_EXIT_INTERVIEW_HDR = userCtx.Find(r =>
                            (r.EX_INT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EXIT_INTERVIEW_HDR;

                    ddlRequestNumber.SelectedValue = hR_EXIT_INTERVIEW_HDR.RESIG_HDR_ID;
                    fillrequestdtls();
                    ddldeptname.SelectedValue = hR_EXIT_INTERVIEW_HDR.EX_DEPT_ID;
                    filldesignation();
                    ddldesigName.SelectedValue = hR_EXIT_INTERVIEW_HDR.EX_DESIGNATION;
                    ddlConductedBy.SelectedValue = hR_EXIT_INTERVIEW_HDR.EX_CONDUCTED_BY;
                    if (hR_EXIT_INTERVIEW_HDR.LAST_WORKING_DATE != null)
                    {
                        txtLastWorking.Text = DBMethod.ConvertDateToString(hR_EXIT_INTERVIEW_HDR.LAST_WORKING_DATE.ToString());
                    }
                    if (hR_EXIT_INTERVIEW_HDR.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                    dtResigreasons = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationInterview_DAL.getResigReasonsDtl(Master.StrRecordId)).Tables[0];

                    if (dtResigreasons.Rows.Count > 0)
                    {
                        for (int lLoop = 0; lLoop < dtResigreasons.Rows.Count; lLoop++)
                        {
                            if (chkReasons.Items.Count > 0)
                            {
                                for (int kLoop = 0; kLoop < chkReasons.Items.Count; kLoop++)
                                {
                                    string org = dtResigreasons.Rows[lLoop]["REASONS"].ToString();
                                    if (org == chkReasons.Items[kLoop].Value.ToString())
                                    {
                                        chkReasons.Items[kLoop].Selected = true;
                                    }

                                }

                            }

                        }

                    }




                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EXIT_INTERVIEW_HDR = (HR_EXIT_INTERVIEW_HDR)EntityData;
                }

                hR_EXIT_INTERVIEW_HDR.RESIG_HDR_ID = ddlRequestNumber.SelectedValue;
                hR_EXIT_INTERVIEW_HDR.EX_DEPT_ID = ddldeptname.SelectedValue;
                hR_EXIT_INTERVIEW_HDR.EX_DESIGNATION = ddldesigName.SelectedValue;
                hR_EXIT_INTERVIEW_HDR.EX_CONDUCTED_BY = ddlConductedBy.SelectedValue;

                if (txtLastWorking.Text != string.Empty)
                {
                    hR_EXIT_INTERVIEW_HDR.LAST_WORKING_DATE = DBMethod.ConvertStringToDate(txtLastWorking.Text.ToString());
                    //hR_EMPLOYEES.TERMINATION_DATE = DBMethod.ConvertStringToDate(txtLastWorking.Text.ToString());
                }

                DBMethod.ExecuteNonQuery(ResignationInterview_DAL.updateLastwrkingDate(ddlRequestNumber.SelectedValue, txtLastWorking.Text));

                //if (txtLastWorking.Text != string.Empty)
                //{
                //    hR_EMPLOYEES.TERMINATION_DATE = DBMethod.ConvertStringToDate(txtLastWorking.Text.ToString());
                //}
                if (chkActive.Checked == true)
                {
                    hR_EXIT_INTERVIEW_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_EXIT_INTERVIEW_HDR.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }
                //hR_EXIT_INTERVIEW_HDR.ENABLED_FLAG = FINAppConstants.Y;

                hR_EXIT_INTERVIEW_HDR.RESIG_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                // hR_EXIT_INTERVIEW_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EXIT_INTERVIEW_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EXIT_INTERVIEW_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EXIT_INTERVIEW_HDR.EX_INT_ID = FINSP.GetSPFOR_SEQCode("HR_043_M".ToString(), false, true);

                    hR_EXIT_INTERVIEW_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EXIT_INTERVIEW_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_EXIT_INTERVIEW_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EXIT_INTERVIEW_HDR.EX_INT_ID);


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        hR_EXIT_INTERVIEW_DTL = new HR_EXIT_INTERVIEW_DTL();
                        if (gvData.DataKeys[iLoop].Values["EX_INT_DTL_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_EXIT_INTERVIEW_DTL> userCtx = new DataRepository<HR_EXIT_INTERVIEW_DTL>())
                            {
                                hR_EXIT_INTERVIEW_DTL = userCtx.Find(r =>
                                    (r.EX_INT_DTL_ID == gvData.DataKeys[iLoop].Values["EX_INT_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtlineno = (TextBox)gvData.Rows[iLoop].FindControl("txtlineno");
                        TextBox txtResponse = (TextBox)gvData.Rows[iLoop].FindControl("txtResponse");
                        DropDownList ddlInterviewAnswer = (DropDownList)gvData.Rows[iLoop].FindControl("ddlInterviewAnswer");

                        hR_EXIT_INTERVIEW_DTL.EX_INT_LINE_NUM = decimal.Parse(txtlineno.Text);
                        hR_EXIT_INTERVIEW_DTL.EX_INT_QUESTION = gvData.DataKeys[iLoop].Values["EX_INT_QUESTION"].ToString();
                        hR_EXIT_INTERVIEW_DTL.ATTRIBUTE2 = gvData.DataKeys[iLoop].Values["QUESTION_GROUP"].ToString();
                        hR_EXIT_INTERVIEW_DTL.ATTRIBUTE1 = ddlInterviewAnswer.SelectedValue;
                        hR_EXIT_INTERVIEW_DTL.EX_INT_RESPONSE = txtResponse.Text;

                        hR_EXIT_INTERVIEW_DTL.EX_INT_ID = hR_EXIT_INTERVIEW_HDR.EX_INT_ID;

                        hR_EXIT_INTERVIEW_DTL.WORKFLOW_COMPLETION_STATUS = "1";


                        hR_EXIT_INTERVIEW_DTL.ENABLED_FLAG = FINAppConstants.Y;

                        if (gvData.DataKeys[iLoop].Values[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EXIT_INTERVIEW_DTL, "D"));
                        }
                        else
                        {
                            if (gvData.DataKeys[iLoop].Values["EX_INT_DTL_ID"].ToString() != "0")
                            {
                                // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                                hR_EXIT_INTERVIEW_DTL.MODIFIED_BY = this.LoggedUserName;
                                hR_EXIT_INTERVIEW_DTL.MODIFIED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_EXIT_INTERVIEW_DTL, "U"));

                            }
                            else
                            {

                                hR_EXIT_INTERVIEW_DTL.EX_INT_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_043_D".ToString(), false, true);
                                hR_EXIT_INTERVIEW_DTL.CREATED_BY = this.LoggedUserName;
                                hR_EXIT_INTERVIEW_DTL.CREATED_DATE = DateTime.Today;
                                //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                                tmpChildEntity.Add(new Tuple<object, string>(hR_EXIT_INTERVIEW_DTL, "A"));
                            }
                        }

                    }
                }

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.ResignationInterview_DAL.GetSPFOR_DUPLICATE_CHECK(hR_EXIT_INTERVIEW_HDR.RESIG_HDR_ID, hR_EXIT_INTERVIEW_HDR.EX_INT_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("DEPTSCHD", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EXIT_INTERVIEW_HDR, HR_EXIT_INTERVIEW_DTL>(hR_EXIT_INTERVIEW_HDR, tmpChildEntity, hR_EXIT_INTERVIEW_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EXIT_INTERVIEW_HDR, HR_EXIT_INTERVIEW_DTL>(hR_EXIT_INTERVIEW_HDR, tmpChildEntity, hR_EXIT_INTERVIEW_DTL, true);
                            saveBool = true;
                            break;
                        }
                }



                // Reason for resignation checklist
                if (chkReasons.Items.Count > 0)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        ResignationInterview_DAL.deleteResigres(Master.StrRecordId);
                    }

                    for (int jLoop = 0; jLoop < chkReasons.Items.Count; jLoop++)
                    {
                        HR_REASONS_FOR_RESIGNATION hR_REASONS_FOR_RESIGNATION = new HR_REASONS_FOR_RESIGNATION();
                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        {

                            //if (hR_REASONS_FOR_RESIGNATION == null)
                            //{
                            //    hR_REASONS_FOR_RESIGNATION = new HR_REASONS_FOR_RESIGNATION();
                            //    hR_REASONS_FOR_RESIGNATION.EX_INT_ID = hR_EXIT_INTERVIEW_HDR.EX_INT_ID;
                            //    DBMethod.DeleteEntity<HR_REASONS_FOR_RESIGNATION>(hR_REASONS_FOR_RESIGNATION);
                            //}
                           
                            if (hR_REASONS_FOR_RESIGNATION == null)
                            {
                                hR_REASONS_FOR_RESIGNATION = new HR_REASONS_FOR_RESIGNATION();
                            }

                        }
                        //DataTable dtresgrs = new DataTable();

                        //dtresgrs = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationInterview_DAL.getResigReasons2(hR_EXIT_INTERVIEW_HDR.EX_INT_ID, chkReasons.Items[jLoop].Value.ToString())).Tables[0];


                        if (chkReasons.Items[jLoop].Selected)
                        {


                            hR_REASONS_FOR_RESIGNATION.EX_INT_ID = hR_EXIT_INTERVIEW_HDR.EX_INT_ID;
                            hR_REASONS_FOR_RESIGNATION.REASONS = chkReasons.Items[jLoop].Value.ToString();
                            hR_REASONS_FOR_RESIGNATION.ENABLED_FLAG = FINAppConstants.Y;
                            hR_REASONS_FOR_RESIGNATION.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                            hR_REASONS_FOR_RESIGNATION.WORKFLOW_COMPLETION_STATUS = "1";

                            if (hR_REASONS_FOR_RESIGNATION.RFR_ID != "0" && hR_REASONS_FOR_RESIGNATION.RFR_ID != null)
                            {
                                //if (dtresgrs.Rows[0]["RFR_ID"].ToString() != "0" && dtresgrs.Rows[0]["RFR_ID"].ToString() != string.Empty)
                                //{
                                //    //uSER_ORGANIZATION_DTL.USER_ORG_DTL_ID = int.Parse(dtGridData.Rows[iLoop][iAcademeColumnConstants.USER_ORG_DTL_ID].ToString());

                                hR_REASONS_FOR_RESIGNATION.MODIFIED_BY = this.LoggedUserName;
                                hR_REASONS_FOR_RESIGNATION.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<HR_REASONS_FOR_RESIGNATION>(hR_REASONS_FOR_RESIGNATION, true);
                            }
                            else
                            {
                                hR_REASONS_FOR_RESIGNATION.RFR_ID = FINSP.GetSPFOR_SEQCode("RFR_ID".ToString(), false, true);
                                hR_REASONS_FOR_RESIGNATION.CREATED_BY = this.LoggedUserName;
                                hR_REASONS_FOR_RESIGNATION.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<HR_REASONS_FOR_RESIGNATION>(hR_REASONS_FOR_RESIGNATION);
                            }

                        }
                        else
                        {

                         

                        }
                    }

                }






            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                int diff = int.Parse((DateTime.Parse(txtLastWorking.Text) - DateTime.Parse(txtRequestDate.Text)).TotalDays.ToString());
                DataTable dtemp = new DataTable();
                dtemp = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeNoticePeriod(ddlConductedBy.SelectedValue)).Tables[0];

                if (dtemp.Rows.Count > 0)
                {
                    if (diff < int.Parse(dtemp.Rows[0][0].ToString()))
                    {
                        ErrorCollection.Add("NoticePeriod", "Notice Period is not fully completed");
                        return;
                    }
                }
                else
                {
                    ErrorCollection.Add("Notice Period", "Notice Period is not mention for the employee");
                    return;
                }


                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Resignation Interview");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtlineno = gvr.FindControl("txtlineno") as TextBox;
            TextBox txtquestion = gvr.FindControl("txtquestion") as TextBox;
            TextBox txtResponse = gvr.FindControl("txtResponse") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["EX_INT_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtlineno;
            slControls[1] = txtquestion;
            slControls[2] = txtResponse;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox";
            string strMessage = Prop_File_Data["Line_Number_P"] + " ~ " + Prop_File_Data["Question_P"] + " ~ " + Prop_File_Data["Response_P"] + "";
            //string strMessage = "Line Number ~ Question ~ Response";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "EX_INT_LINE_NUM='" + txtlineno.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }



            drList["EX_INT_LINE_NUM"] = decimal.Parse(txtlineno.Text);
            drList["EX_INT_QUESTION"] = txtquestion.Text;
            drList["EX_INT_RESPONSE"] = txtResponse.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;


                    GridViewRow gvr = gvData.SelectedRow;// (GridViewRow)((Control)e.Row).Parent.Parent;
                    // FillFooterGridCombo(gvr);
                    //DropDownList ddlInterviewAnswer = gvr.FindControl("ddlInterviewAnswer") as DropDownList;
                    DropDownList ddlInterviewAnswer = e.Row.FindControl("ddlInterviewAnswer") as DropDownList;
                    Lookup_BLL.GetLookUpValues(ref ddlInterviewAnswer, "INTV_ANSWR");
                    ddlInterviewAnswer.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();


                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_EXIT_INTERVIEW_HDR>(hR_EXIT_INTERVIEW_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddldeptname_SelectedIndexChanged(object sender, EventArgs e)
        {
            filldesignation();

        }

        private void filldesignation()
        {
            ResignationInterview_BLL.fn_GetDesignationName(ref ddldesigName, ddldeptname.SelectedValue);
        }

        protected void ddlRequestNumber_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillrequestdtls();
        }

        private void fillrequestdtls()
        {
            DataTable dtreqdtl = new DataTable();
            dtreqdtl = DBMethod.ExecuteQuery(FIN.DAL.HR.ResignationInterview_DAL.getResigReqDtl(ddlRequestNumber.SelectedValue)).Tables[0];
            //txtReason.Text = dtreqdtl.Rows[0]["RESIG_REASON"].ToString();
            txtRemarks.Text = dtreqdtl.Rows[0]["RESIG_REMARKS"].ToString();
            txtRequestDate.Text = DBMethod.ConvertDateToString(dtreqdtl.Rows[0]["RESIG_REQ_DT"].ToString());
        }

        protected void ddldesigName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillemployeedetails();
        }

        private void fillemployeedetails()
        {
            Employee_BLL.GetEmplDet(ref ddlConductedBy, ddldeptname.SelectedValue, ddldesigName.SelectedValue);
        }


    }
}