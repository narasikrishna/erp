﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class DepartmentDetailsEntry : PageBase
    {

        HR_DEPT_DESIGNATIONS hR_DEPT_DESIGNATIONS = new HR_DEPT_DESIGNATIONS();
        DataTable dtGridData = new DataTable();
        DepartmentDetails_BLL DepartmentDetails_BLL = new DepartmentDetails_BLL();
        string ProReturn = null;

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            DepartmentDetails_BLL.fn_GetDepartmentDetails(ref ddlDeptname);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtDepartmentType.Enabled = false;
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.DepartmentDetails_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_DEPT_DESIGNATIONS> userCtx = new DataRepository<HR_DEPT_DESIGNATIONS>())
                    {
                        hR_DEPT_DESIGNATIONS = userCtx.Find(r =>
                            (r.DEPT_DESIG_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_DEPT_DESIGNATIONS;

                    ddlDeptname.SelectedValue = hR_DEPT_DESIGNATIONS.DEPT_ID;
                    filldept_typ();
                    fillDeptdtls();

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_DEPT_DESIGNATIONS = new HR_DEPT_DESIGNATIONS();
                    if (dtGridData.Rows[iLoop]["DEPT_DESIG_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_DEPT_DESIGNATIONS> userCtx = new DataRepository<HR_DEPT_DESIGNATIONS>())
                        {
                            hR_DEPT_DESIGNATIONS = userCtx.Find(r =>
                                (r.DEPT_DESIG_ID == dtGridData.Rows[iLoop]["DEPT_DESIG_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    hR_DEPT_DESIGNATIONS.DESIG_NAME = dtGridData.Rows[iLoop]["DESIG_NAME"].ToString();
                    hR_DEPT_DESIGNATIONS.DESIG_SHORT_NAME = dtGridData.Rows[iLoop]["DESIG_SHORT_NAME"].ToString();
                    hR_DEPT_DESIGNATIONS.DESIG_NAME_OL = dtGridData.Rows[iLoop]["DESIG_NAME_OL"].ToString();
                    hR_DEPT_DESIGNATIONS.DESIG_SHORT_NAME_OL = dtGridData.Rows[iLoop]["DESIG_SHORT_NAME_OL"].ToString();

                    hR_DEPT_DESIGNATIONS.PARENT_DEPT_DESIG_ID = dtGridData.Rows[iLoop]["PARENT_DEPT_DESIG_ID"].ToString();
                    hR_DEPT_DESIGNATIONS.DEPT_ID = ddlDeptname.SelectedValue;
                    hR_DEPT_DESIGNATIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                    if (dtGridData.Rows[iLoop]["NO_OF_EMP"].ToString().Length > 0)
                    {
                        hR_DEPT_DESIGNATIONS.NO_OF_EMP = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["NO_OF_EMP"].ToString());
                    }
                    else
                    {
                        hR_DEPT_DESIGNATIONS.NO_OF_EMP = null;
                    }


                    //if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"] != DBNull.Value)
                    //{
                    //    hR_DEPT_DESIGNATIONS.EFFECTIVE_FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"].ToString());
                    //}

                    //if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"] != DBNull.Value)
                    //{
                    //    hR_DEPT_DESIGNATIONS.EFFECTIVE_TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"].ToString());
                    //}


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_DEPT_DESIGNATIONS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_DEPT_DESIGNATIONS.ENABLED_FLAG = FINAppConstants.N;
                    }

                   // hR_DEPT_DESIGNATIONS.WORKFLOW_COMPLETION_STATUS = "1";
                   



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_DEPT_DESIGNATIONS, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.DepartmentDetails_DAL.GetSPFOR_DUPLICATE_CHECK(hR_DEPT_DESIGNATIONS.DEPT_ID, hR_DEPT_DESIGNATIONS.DESIG_SHORT_NAME, hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("DEPTSCHD", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["DEPT_DESIG_ID"].ToString() != "0")
                        {
                            hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID = dtGridData.Rows[iLoop]["DEPT_DESIG_ID"].ToString();
                            hR_DEPT_DESIGNATIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID);
                            hR_DEPT_DESIGNATIONS.MODIFIED_BY = this.LoggedUserName;
                            hR_DEPT_DESIGNATIONS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_DEPT_DESIGNATIONS>(hR_DEPT_DESIGNATIONS,true);
                            savedBool = true;
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_008.ToString(), false, true);
                            hR_DEPT_DESIGNATIONS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_DEPT_DESIGNATIONS_SEQ);
                            hR_DEPT_DESIGNATIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_DEPT_DESIGNATIONS.DEPT_DESIG_ID);
                            hR_DEPT_DESIGNATIONS.CREATED_BY = this.LoggedUserName;
                            hR_DEPT_DESIGNATIONS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_DEPT_DESIGNATIONS>(hR_DEPT_DESIGNATIONS);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }

                }
                //VMVServices.Web.Utils.SavedRecordId = "";

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlParentname = tmpgvr.FindControl("ddlParentname") as DropDownList;
                DepartmentDetails_BLL.fn_getDesigName4DeptId(ref ddlParentname, ddlDeptname.SelectedValue);

                if (gvData.EditIndex >= 0)
                {
                    ddlParentname.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["PARENT_DEPT_DESIG_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Job Responsibility");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlParentname = gvr.FindControl("ddlParentname") as DropDownList;
            TextBox txtshortnm = gvr.FindControl("txtshortnm") as TextBox;
            TextBox txtDesigName = gvr.FindControl("txtDesigName") as TextBox;
            TextBox txtshortnmAR = gvr.FindControl("txtshortnmAR") as TextBox;
            TextBox txtDesigNameAR = gvr.FindControl("txtDesigNameAR") as TextBox;
            TextBox txtNoofEmp = gvr.FindControl("txtNoofEmp") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["DEPT_DESIG_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtshortnm;
            slControls[1] = txtDesigName;
          //  slControls[2] = ddlParentname;
           

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox";
            string strMessage = "Short Name ~ Designation Name";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "DESIG_NAME='" + txtDesigName.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.DesignationName;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            string strCondition1 = "DESIG_SHORT_NAME='" + txtshortnm.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.DesignationShortName;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition1, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["DEPT_DESIG_ID"].ToString(), ddlDeptname.SelectedValue, txtshortnm.Text);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("DESIGNATION", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            if (ddlParentname.SelectedValue.ToString().Trim().Length > 0)
            {
                drList["PARENT_DEPT_DESIG_ID"] = ddlParentname.SelectedItem.Value;
                drList["PARENT_DESIG_DESC"] = ddlParentname.SelectedItem.Text;
            }

            if (txtNoofEmp.Text.ToString().Length > 0)
            {
                drList["NO_OF_EMP"] = txtNoofEmp.Text;
            }
            else
            {
                drList["NO_OF_EMP"] = DBNull.Value;
            }

            drList["DESIG_NAME"] = txtDesigName.Text;
            drList["DESIG_SHORT_NAME"] = txtshortnm.Text;
            drList["DESIG_NAME_OL"] = txtDesigNameAR.Text;
            drList["DESIG_SHORT_NAME_OL"] = txtshortnmAR.Text;

          
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_DEPT_DESIGNATIONS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_DEPT_DESIGNATIONS>(hR_DEPT_DESIGNATIONS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DD_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




      
        protected void ddlDeptname_SelectedIndexChanged(object sender, EventArgs e)
        {
            filldept_typ();
            fillDeptdtls();
        }

        private void filldept_typ()
        {
            DataTable dtdepttyp = new DataTable();
            dtdepttyp = DBMethod.ExecuteQuery(FIN.DAL.HR.DepartmentDetails_DAL.GetDepttype(ddlDeptname.SelectedValue)).Tables[0];
            txtDepartmentType.Text = dtdepttyp.Rows[0]["DEPT_TYPE"].ToString();
        }


        private void fillDeptdtls()
        {

            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.DepartmentDetails_DAL.GetDeptDtls(ddlDeptname.SelectedValue)).Tables[0];
              
                if (dtGridData.Rows.Count > 0)
                {
                    //ddlDeptname.SelectedIndex = 0;
                    //txtDepartmentType.Text = "";
                    //gvData.Visible = false;
                    //ErrorCollection.Add("RecordAlreadyExists", Prop_File_Data["RecordAlreadyExists_P"]);
                    gvData.Visible = true;
                    dtGridData.Columns.Add(FINColumnConstants.DELETED);
                    BindGrid(dtGridData);
                        
                }
                else
                {
                    gvData.Visible = true;
                    dtGridData.Columns.Add(FINColumnConstants.DELETED);
                    BindGrid(dtGridData);
                }

                //dtGridData.Columns.Add(FINColumnConstants.DELETED);
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RecordAlreadyExists", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

            //System.Collections.SortedList slControls = new System.Collections.SortedList();
            //string strMessage = "Record Already exists. Please go to Edit Mode and Modify the details";
            //slControls[0] = " ";
            //Master.ShowMessage(slControls, strMessage);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }


    }
}