﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeFeedbackEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeFeedbackEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="Server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
         <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 180px" id="Div1">
                Employee Name
            </div>
            <div class="divtxtBox" style="float: left; width: 300px">
                <asp:DropDownList ID="ddlEmpName" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 180px" id="lblFeedBackTempName">
                Feedback Template Name
            </div>
            <div class="divtxtBox" style="float: left; width: 300px">
                <asp:DropDownList ID="ddlFeedbackTempName" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlFeedbackTempName_SelectedIndexChanged" AutoPostBack="true"
                    Width="250px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 180px" id="lblDate">
                Feedback Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    Enabled="false" OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox" style="float: left; width: 180px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                DataKeyNames="FEEDBACK_ID,FEEDBACK_DTL_ID,DELETED,feedback_id_type,feedback_id_desc,feedback_id_rating,rating_code_id" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True" Width="450px">
                <Columns>
                    <asp:TemplateField HeaderText="Feedback Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbacktyp" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="250px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbacktyp" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="250px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbacktyp" runat="server" Text='<%# Eval("feedback_type") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Description">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbackDesc" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="250px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbackDesc" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="250px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackDesc" runat="server" Text='<%# Eval("feedback_desc") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Rating Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbackRatingType" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlFeedbackRatingType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbackRatingType" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlFeedbackRatingType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackRatingType" runat="server" Text='<%# Eval("feedback_rating_type") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Rating">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbackRating" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbackRating" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackRating" runat="server" Text='<%# Eval("rating_code_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtComments" Enabled="true" Width="160px" MaxLength="500" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("emp_comments") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtComments" Enabled="true" Width="160px" MaxLength="500" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblComments" Width="160px" TabIndex="11" Enabled="true" runat="server"
                                Text='<%# Eval("emp_comments") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="true" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" Checked="true" Enabled="true" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add/Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" AlternateText="Edit" runat="server" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" Height="20px" runat="server" AlternateText="Update"
                                ImageUrl="~/Images/Update.png" CommandName="Update" />
                            <asp:ImageButton ID="ibtnCancel" ImageUrl="~/Images/Close.jpg" AlternateText="Cancel"
                                runat="server" CausesValidation="false" CommandName="Cancel" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <%--  <ItemStyle HorizontalAlign="Center" />--%>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>