﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using FIN.DAL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class IndeminityProcessEntry : PageBase
    {
        HR_INDEM_PAYMENT HR_INDEM_PAYMENT = new HR_INDEM_PAYMENT();
        string ProReturn = null;
        DataTable Month_data = new DataTable();
        static int month;
        Boolean savedBool = false;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL_LA", ex.Message);
            }
            finally
            {
                // ErrorCollection.Add("SS", "Success");
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }



            UserRightsChecking();



        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                ComboFilling.fn_getDepartment(ref ddlDepartment);
                FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                EntityData = null;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_INDEM_PAYMENT> userCtx = new DataRepository<HR_INDEM_PAYMENT>())
                    {
                        HR_INDEM_PAYMENT = userCtx.Find(r =>
                            (r.INDEM_PAYMENT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = HR_INDEM_PAYMENT;

                    btnSave.Visible = false;

                    ddlDepartment.SelectedValue = HR_INDEM_PAYMENT.DEPT_ID;
                    Employee_BLL.GetEmplName(ref ddlStaffName, ddlDepartment.SelectedValue);
                    ddlStaffName.SelectedValue = HR_INDEM_PAYMENT.EMP_ID;
                    ddlBankName.SelectedValue = HR_INDEM_PAYMENT.BANK_ID;

                    if (HR_INDEM_PAYMENT.INDEM_PAY_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(HR_INDEM_PAYMENT.INDEM_PAY_DATE.ToString());
                    }
                    if (HR_INDEM_PAYMENT.INDEM_CALC_AMT != null)
                    {
                        txtPayableAmt.Text = HR_INDEM_PAYMENT.INDEM_CALC_AMT.ToString();

                    }
                    txtRemarks.Text = HR_INDEM_PAYMENT.INDEM_PAY_REMARKS;
                    txtChequeNo.Text = HR_INDEM_PAYMENT.CHEQUE_NO;
                    if (HR_INDEM_PAYMENT.CHEQUE_AMT != null)
                    {

                        txtChequeAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(HR_INDEM_PAYMENT.CHEQUE_AMT.ToString());
                    }
                    if (HR_INDEM_PAYMENT.CHEQUE_DATE != null)
                    {
                        txtChequeDate.Text = DBMethod.ConvertDateToString(HR_INDEM_PAYMENT.CHEQUE_DATE.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Employee_BLL.GetEmplName(ref ddlStaffName, ddlDepartment.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                var tmpChildEntity = new List<Tuple<object, string>>();
                if (EntityData != null)
                {
                    HR_INDEM_PAYMENT = (HR_INDEM_PAYMENT)EntityData;
                }
                HR_INDEM_PAYMENT.EMP_ID = (ddlStaffName.SelectedValue.ToString());
                HR_INDEM_PAYMENT.DEPT_ID = ddlDepartment.SelectedValue;
                HR_INDEM_PAYMENT.BANK_ID = ddlBankName.SelectedValue;

                if (txtDate.Text.Length > 0)
                {
                    HR_INDEM_PAYMENT.INDEM_PAY_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }
                HR_INDEM_PAYMENT.INDEM_CALC_AMT = CommonUtils.ConvertStringToDecimal(txtPayableAmt.Text);
                HR_INDEM_PAYMENT.INDEM_PAY_REMARKS = txtRemarks.Text.Trim();
                HR_INDEM_PAYMENT.CHEQUE_NO = txtChequeNo.Text;
                HR_INDEM_PAYMENT.CHEQUE_AMT = CommonUtils.ConvertStringToDecimal(txtChequeAmt.Text);
                if (txtChequeDate.Text.Length > 0)
                {
                    HR_INDEM_PAYMENT.CHEQUE_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text.ToString());
                }
                HR_INDEM_PAYMENT.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                HR_INDEM_PAYMENT.ENABLED_FLAG = FINAppConstants.Y;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_INDEM_PAYMENT.MODIFIED_BY = this.LoggedUserName;
                    HR_INDEM_PAYMENT.MODIFIED_DATE = DateTime.Today;
                    tmpChildEntity.Add(new Tuple<object, string>(HR_INDEM_PAYMENT, FINAppConstants.Update));
                }
                else
                {
                    HR_INDEM_PAYMENT.INDEM_PAYMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_102.ToString(), false, true);
                    HR_INDEM_PAYMENT.CREATED_BY = this.LoggedUserName;
                    HR_INDEM_PAYMENT.CREATED_DATE = DateTime.Today;
                    tmpChildEntity.Add(new Tuple<object, string>(HR_INDEM_PAYMENT, FINAppConstants.Add));
                }
                HR_INDEM_PAYMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_INDEM_PAYMENT.INDEM_PAYMENT_ID);

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveSingleEntity<HR_INDEM_PAYMENT>(tmpChildEntity);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveSingleEntity<HR_INDEM_PAYMENT>(tmpChildEntity, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LA_BTNDEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnPosting_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FIN.DAL.HR.IndemnityLedger_DAL.GetSP_IndemnityPaymentPosting(txtDate.Text);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlDepartment;
                slControls[1] = ddlStaffName;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList";
                string strMessage = Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Staff_Name_P"];

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                if (ErrorCollection.Count > 0)
                    return;

                ErrorCollection.Clear();

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    if (savedBool)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                string counts = string.Empty;

                counts = DBMethod.GetStringValue(IndemnityDAL.ValidateExistingEmp(ddlDepartment.SelectedValue, ddlStaffName.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString())));

                if (counts == string.Empty || counts == "0")
                {
                    // IndemnityDAL.GetSP_Indemnity_Ledger(ddlDepartment.SelectedValue, ddlStaffName.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString()));
                    txtPayableAmt.Text = IndemnityDAL.GetSP_Indemnity_Amount(ddlStaffName.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString())).ToString();
                }
                else
                {
                    ErrorCollection.Remove("AlrdyExisting");
                    ErrorCollection.Add("AlrdyExisting", "Already paiyment completed for this Employee");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savindeme_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void btnPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savindeme_LA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

    }
}