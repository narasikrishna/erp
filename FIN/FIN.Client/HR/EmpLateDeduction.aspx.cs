﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmpLateDeduction : PageBase
    {

        TM_EMP_TRANS tM_EMP_TRANS = new TM_EMP_TRANS();
        Boolean saveBool = false;
        Boolean bol_rowVisiable;
        DataTable dtgriddata = new DataTable();
        TM_EMP_LATE_DEDUCTION tM_EMP_LATE_DEDUCTION = new TM_EMP_LATE_DEDUCTION();
        Boolean savedBool = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Startup();
                PopulateMonth();
                PopulateYear();
                EntryLoadHeader();
                ChangeLanguage();
                if (VMVServices.Web.Utils.Multilanguage)
                    AssignLanguage();
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    
                    Button btnDetails=new Button();
                    btnDetails.Text = Prop_File_Data["Details_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //   pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        private void PopulateMonth()
        {
            ddlMonth.Items.Clear();
            ListItem lt = new ListItem();
            //  lt.Text = "MM";
            // lt.Value = "0";

            // ddlMonth.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/" + i.ToString() + "/1900").ToString("MMMM");
                // lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlMonth.Items.Add(lt);
            }
            //  ddlMonth.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }
        private void PopulateYear()
        {
            ddlYear.Items.Clear();
            ListItem lt = new ListItem();
            // lt.Text = "YYYY";
            // lt.Value = "0";
            // ddlYear.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1950; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear.Items.Add(lt);
            }
            // ddlYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }
        protected void btnDetails_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Show();
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;


                string empId = gvParentData.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString();
                string groupCode = gvParentData.DataKeys[gvrP.RowIndex].Values["tm_emp_group"].ToString();



                DataTable dtEmp = new DataTable();
                DataTable dtDropDownData = new DataTable();

                dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetEmpLateDeduction(0, 0, empId)).Tables[0];


                if (ddlYear.SelectedValue != null)
                {
                    //dtEmp = DBMethod.ExecuteQuery(OverTimeSelection_DAL.GetEMPGroupDatefromDummy()).Tables[0];

                    //if (dtEmp.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtEmp.Rows.Count; i++)
                    //    {
                    //        dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("ENTRY_TYPE")).Tables[0];//get the late type

                    //        if (dtDropDownData.Rows.Count > 0)
                    //        {
                    //            for (int iL = 0; iL < dtDropDownData.Rows.Count; iL++)
                    //            {
                    //                FINSP.GetSPFOR_PROC_EMP_LOP_FOR_LATE(groupCode, empId, (dtEmp.Rows[i]["tm_date"].ToString()), dtEmp.Rows[i]["DEPT_ID"].ToString(), dtDropDownData.Rows[iL]["LOOKUP_NAME"].ToString());

                    dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetEmpLateDeduction(CommonUtils.ConvertStringToInt(ddlMonth.SelectedValue.ToString()), CommonUtils.ConvertStringToInt(ddlYear.SelectedValue.ToString()), empId)).Tables[0];

                    //            }
                    //        }
                    //    }
                    //}
                }

                Session[FINSessionConstants.GridData] = dtgriddata;

                gvData.DataSource = dtgriddata;
                gvData.DataBind();

                if (dtgriddata.Rows.Count == 0)
                {
                    lblErrorMsg.Visible = true;

                }
                else
                {
                    lblErrorMsg.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvParentData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //if (bol_rowVisiable)
                    //    e.Row.Visible = false;

                    string empId = gvParentData.DataKeys[e.Row.RowIndex].Values["EMP_ID"].ToString();
                    string groupCode = gvParentData.DataKeys[e.Row.RowIndex].Values["tm_emp_group"].ToString();

                    GridView gvData = (GridView)e.Row.FindControl("gvData");

                    DataTable dtEmp = new DataTable();
                    DataTable dtDropDownData = new DataTable();

                    if (ddlYear.SelectedValue != null)
                    {
                        dtEmp = DBMethod.ExecuteQuery(OverTimeSelection_DAL.GetEMPGroupDatefromDummy()).Tables[0];

                        if (dtEmp.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtEmp.Rows.Count; i++)
                            {
                                dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("ENTRY_TYPE")).Tables[0];//get the late type

                                if (dtDropDownData.Rows.Count > 0)
                                {
                                    for (int iL = 0; iL < dtDropDownData.Rows.Count; iL++)
                                    {
                                        FINSP.GetSPFOR_PROC_EMP_LOP_FOR_LATE(groupCode, empId, (dtEmp.Rows[i]["tm_date"].ToString()), dtEmp.Rows[i]["DEPT_ID"].ToString(), dtDropDownData.Rows[iL]["LOOKUP_NAME"].ToString());

                                        dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetEmpLateDeduction(CommonUtils.ConvertStringToInt(ddlMonth.SelectedValue.ToString()), CommonUtils.ConvertStringToInt(ddlYear.SelectedValue.ToString()), empId)).Tables[0];

                                        Session[FINSessionConstants.GridData] = dtgriddata;

                                        gvData.DataSource = dtgriddata;
                                        gvData.DataBind();
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnimport_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();

                DataTable dtEmp = new DataTable();
                DataTable dtDropDownData = new DataTable();
               
                if (ddlYear.SelectedValue != null)
                {
                    dtEmp = DBMethod.ExecuteQuery(OverTimeSelection_DAL.GetEMPGroupDatefromDummy()).Tables[0];

                    if (dtEmp.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmp.Rows.Count; i++)
                        {
                            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("ENTRY_TYPE")).Tables[0];

                            if (dtDropDownData.Rows.Count > 0)
                            {
                                for (int iL = 0; iL < dtDropDownData.Rows.Count; iL++)
                                {
                                    FINSP.GetSPFOR_PROC_EMP_LOP_FOR_LATE(dtEmp.Rows[i]["tm_grp_code"].ToString(), dtEmp.Rows[i]["EMP_ID"].ToString(), (dtEmp.Rows[i]["tm_date"].ToString()), dtEmp.Rows[i]["DEPT_ID"].ToString(), dtDropDownData.Rows[iL]["LOOKUP_NAME"].ToString());


                                }
                            }
                        }
                        dtgriddata = DBMethod.ExecuteQuery(FIN.DAL.HR.AttendanceImport_DAL.GetEmpLateTotalDeduction()).Tables[0];
                        gvParentData.DataSource = dtgriddata;
                        gvParentData.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
                ErrorCollection.Add("1emplat_RDB", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Add(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //if (bol_rowVisiable)
                    //    e.Row.Visible = false;

                    CheckBox chkSelection = ((CheckBox)e.Row.FindControl("chkSelection"));

                    if (chkSelection.Checked == true)
                    {
                        chkSelection.Enabled = false;
                    }
                    else
                    {
                        chkSelection.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Hide();
        }
        protected void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            // GridView gvr = (GridView)sender;
            ModalPopupExtender4.Show();

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            CheckBox chkSelection = ((CheckBox)gvr.FindControl("chkSelection"));
            Label lblId = (Label)gvr.FindControl("lblId");
            tM_EMP_LATE_DEDUCTION = new TM_EMP_LATE_DEDUCTION();

            if (lblId.Text != string.Empty && lblId.Text != "0")
            {
                using (IRepository<TM_EMP_LATE_DEDUCTION> userCtx = new DataRepository<TM_EMP_LATE_DEDUCTION>())
                {
                    tM_EMP_LATE_DEDUCTION = userCtx.Find(r =>
                        (r.LATE_TM_ID == lblId.Text)
                        ).SingleOrDefault();

                }
            }
            if (tM_EMP_LATE_DEDUCTION != null)
            {

                tM_EMP_LATE_DEDUCTION.ATTRIBUTE1 = chkSelection.Checked == true ? "1" : "0";
                tM_EMP_LATE_DEDUCTION.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (lblId.Text != string.Empty && lblId.Text != "0")
                {
                    tM_EMP_LATE_DEDUCTION.MODIFIED_BY = this.LoggedUserName;
                    tM_EMP_LATE_DEDUCTION.MODIFIED_DATE = DateTime.Today;
                    tM_EMP_LATE_DEDUCTION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_LATE_DEDUCTION.LATE_TM_ID);
                    DBMethod.SaveEntity<TM_EMP_LATE_DEDUCTION>(tM_EMP_LATE_DEDUCTION, true);
                    savedBool = true;
                }

            }
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                var tmpChildEntity = new List<Tuple<object, string>>();
                int selectedCount = 0;

                //if (gvData.Rows.Count > 0)
                //{
                //    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                //    {
                //        tM_EMP_LATE_DEDUCTION = new TM_EMP_LATE_DEDUCTION();

                //        if (gvData.DataKeys[iLoop].Values["LATE_TM_ID"].ToString() != string.Empty && gvData.DataKeys[iLoop].Values["LATE_TM_ID"].ToString() != "0")
                //        {
                //            using (IRepository<TM_EMP_LATE_DEDUCTION> userCtx = new DataRepository<TM_EMP_LATE_DEDUCTION>())
                //            {
                //                tM_EMP_LATE_DEDUCTION = userCtx.Find(r =>
                //                    (r.LATE_TM_ID == gvData.DataKeys[iLoop].Values["LATE_TM_ID"].ToString())
                //                    ).SingleOrDefault();

                //            }
                //        }
                //        if (tM_EMP_LATE_DEDUCTION != null)
                //        {
                //            selectedCount = selectedCount + 1;                         
                //            tM_EMP_LATE_DEDUCTION.ATTRIBUTE1 = ((CheckBox)gvData.Rows[iLoop].FindControl("chkSelection")).Checked == true ? "1" : "0";
                //            tM_EMP_LATE_DEDUCTION.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                //            if (gvData.DataKeys[iLoop].Values["LATE_TM_ID"].ToString() != string.Empty && gvData.DataKeys[iLoop].Values["LATE_TM_ID"].ToString() != "0")
                //            {
                //                tM_EMP_LATE_DEDUCTION.MODIFIED_BY = this.LoggedUserName;
                //                tM_EMP_LATE_DEDUCTION.MODIFIED_DATE = DateTime.Today;
                //                tM_EMP_LATE_DEDUCTION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_LATE_DEDUCTION.LATE_TM_ID);
                //                DBMethod.SaveEntity<TM_EMP_LATE_DEDUCTION>(tM_EMP_LATE_DEDUCTION, true);
                //                savedBool = true;
                //            }                            

                //        }
                //    }
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();



                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {




        }


        //protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            if (bol_rowVisiable)
        //                e.Row.Visible = false;

        //            //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
        //            //{
        //            //    e.Row.Visible = false;
        //            //}


        //            string lateflg = e.Row.Cells[2].Text;

        //            foreach (TableCell cell in e.Row.Cells)
        //            {
        //                if (lateflg == "YES")
        //                {
        //                    cell.BackColor = System.Drawing.Color.Blue;
        //                }

        //            }


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("emplat_RDB", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            ErrorCollection.Add(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }

        //}


    }
}