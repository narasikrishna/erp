﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="IndemnityLedgerEntry.aspx.cs" Inherits="FIN.Client.HR.IndemnityLedgerEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCalendarName">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 490px">
                <asp:DropDownList ID="ddlDept" Width="490px" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 490px">
                <asp:DropDownList ID="ddlEmp" runat="server" Width="490px" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAdjPeriod">
                Transaction Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" TabIndex="2" ID="txtTransDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtTransDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtTransDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblEffectiveDate">
                <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png"
                    OnClick="btnView_Click" ToolTip="View" Style="border: 0px;" />
                <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn" 
                    onclick="btnView_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="" ShowFooter="true" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="INDEM_AMT_BAL" HeaderText="Amount Balance" />
                    <asp:BoundField DataField="INDEM_TXN_DATE" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transaction Date" />
                    <asp:BoundField DataField="INDEM_SERVICE_YEARS" HeaderText="Service Years" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="INDEM_ELIGIBLE_DAYS" HeaderText="Eligible Days" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
