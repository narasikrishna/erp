﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{

    public partial class SuccessionPlanningEntry : PageBase
    {

        HR_SUCCESSION_PLAN_HDR HR_SUCCESSION_PLAN_HDR = new HR_SUCCESSION_PLAN_HDR();
        HR_SUCCESSION_PLAN_DTL HR_SUCCESSION_PLAN_DTL = new HR_SUCCESSION_PLAN_DTL();
        HR_SUCCESSION_PLAN_DTL_DTL HR_SUCCESSION_PLAN_DTL_DTL = new HR_SUCCESSION_PLAN_DTL_DTL();
        string ProReturn = null;
        DataTable dtProfileData = new DataTable();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool = false;

        # region Page Load

        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        private void FillComboBox()
        {
            CareerPlanDtls_BLL.GetDepartmentName(ref ddlDept);

        }
        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Designation();
        }
        private void fn_fill_Designation()
        {
            AssetIssue_BLL.GetDesignationName(ref ddlDesignation, ddlDept.SelectedValue.ToString());

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["enabled_flag"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindProfileGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["ProfileGridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["enabled_flag"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvProfile.DataSource = dt_tmp;
                gvProfile.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                txtProfileDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetSuccessPlanDtl(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                dtProfileData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetSucPlanDtldtlData(Master.StrRecordId)).Tables[0];
                BindProfileGrid(dtProfileData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_SUCCESSION_PLAN_HDR> userCtx = new DataRepository<HR_SUCCESSION_PLAN_HDR>())
                    {
                        HR_SUCCESSION_PLAN_HDR = userCtx.Find(r =>
                            (r.PROC_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = HR_SUCCESSION_PLAN_HDR;
                    if (HR_SUCCESSION_PLAN_HDR.PROC_REMARKS != string.Empty)
                    {
                        txtRemarks.Text = HR_SUCCESSION_PLAN_HDR.PROC_REMARKS;
                    }
                    if (HR_SUCCESSION_PLAN_HDR.PROC_DATE != null)
                    {
                        txtProfileDate.Text = DBMethod.ConvertDateToString(HR_SUCCESSION_PLAN_HDR.PROC_DATE.ToString());
                    }
                    ddlDept.SelectedValue = HR_SUCCESSION_PLAN_HDR.PLAN_DEPT_ID;
                    fn_fill_Designation();
                    ddlDesignation.SelectedValue = HR_SUCCESSION_PLAN_HDR.PLAN_DESIG_ID;
                    fn_fill_CareerPath();
                    ddlCareerPathName.SelectedValue = HR_SUCCESSION_PLAN_HDR.PATH_HDR_ID;
                    fill_path_des();

                    ddlCareerPlan.SelectedValue = HR_SUCCESSION_PLAN_HDR.PLAN_HDR_ID;

                    //   GetProcess();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOteC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        #endregion
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    btnProcess.Text = Prop_File_Data["Process_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        # region Save,Update and Delete
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();



                if (EntityData != null)
                {
                    HR_SUCCESSION_PLAN_HDR = (HR_SUCCESSION_PLAN_HDR)EntityData;
                }

                if (txtProfileDate.Text != string.Empty)
                {
                    HR_SUCCESSION_PLAN_HDR.PROC_DATE = DBMethod.ConvertStringToDate(txtProfileDate.Text.ToString());
                }
                HR_SUCCESSION_PLAN_HDR.PROC_REMARKS = txtRemarks.Text;
                HR_SUCCESSION_PLAN_HDR.PATH_HDR_ID = ddlCareerPathName.SelectedValue;
                HR_SUCCESSION_PLAN_HDR.PATH_DTL_ID=ddlCareerPathName.SelectedValue;
                HR_SUCCESSION_PLAN_HDR.PLAN_HDR_ID = ddlCareerPlan.SelectedValue;
                HR_SUCCESSION_PLAN_HDR.PLAN_DEPT_ID = ddlDept.SelectedValue;
                HR_SUCCESSION_PLAN_HDR.PLAN_DESIG_ID = ddlDesignation.SelectedValue;
                HR_SUCCESSION_PLAN_HDR.PROC_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                HR_SUCCESSION_PLAN_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_SUCCESSION_PLAN_HDR.MODIFIED_BY = this.LoggedUserName;
                    HR_SUCCESSION_PLAN_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    HR_SUCCESSION_PLAN_HDR.PROC_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_067_M".ToString(), false, true);
                    HR_SUCCESSION_PLAN_HDR.CREATED_BY = this.LoggedUserName;
                    HR_SUCCESSION_PLAN_HDR.CREATED_DATE = DateTime.Today;
                }

                HR_SUCCESSION_PLAN_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_SUCCESSION_PLAN_HDR.PROC_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_SUCCESSION_PLAN_DTL = new HR_SUCCESSION_PLAN_DTL();
                        if (gvData.DataKeys[iLoop].Values["PROC_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["PROC_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_SUCCESSION_PLAN_DTL> userCtx = new DataRepository<HR_SUCCESSION_PLAN_DTL>())
                            {
                                HR_SUCCESSION_PLAN_DTL = userCtx.Find(r =>
                                    (r.PROC_DTL_ID == dtGridData.Rows[iLoop]["PROC_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        HR_SUCCESSION_PLAN_DTL.PROC_HDR_ID = HR_SUCCESSION_PLAN_HDR.PROC_HDR_ID;
                        HR_SUCCESSION_PLAN_DTL.PLAN_PROFILE_HDR_ID = gvData.DataKeys[iLoop].Values["PROF_ID"].ToString();

                        CheckBox chkActive = (CheckBox)gvData.Rows[iLoop].FindControl("chkActive");

                        if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                        {
                            HR_SUCCESSION_PLAN_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        }
                        else
                        {
                            HR_SUCCESSION_PLAN_DTL.ENABLED_FLAG = FINAppConstants.N;
                        }

                        //  HR_SUCCESSION_PLAN_DTL.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.Y : FINAppConstants.N;


                        if (gvData.DataKeys[iLoop].Values["PROC_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["PROC_DTL_ID"].ToString() != string.Empty)
                        {
                            HR_SUCCESSION_PLAN_DTL.PROC_DTL_ID = gvData.DataKeys[iLoop].Values["PROC_DTL_ID"].ToString();
                            HR_SUCCESSION_PLAN_DTL.MODIFIED_BY = this.LoggedUserName;
                            HR_SUCCESSION_PLAN_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_SUCCESSION_PLAN_DTL, "U"));
                        }
                        else
                        {

                            HR_SUCCESSION_PLAN_DTL.PROC_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_067_D".ToString(), false, true);
                            HR_SUCCESSION_PLAN_DTL.CREATED_BY = this.LoggedUserName;
                            HR_SUCCESSION_PLAN_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(HR_SUCCESSION_PLAN_DTL, "A"));
                        }
                        HR_SUCCESSION_PLAN_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_SUCCESSION_PLAN_DTL.PROC_DTL_ID);

                    }
                }

                var tmpSecondChildEntity = new List<Tuple<object, string>>();


                if (gvProfile.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvProfile.Rows.Count; iLoop++)
                    {
                        HR_SUCCESSION_PLAN_DTL_DTL = new HR_SUCCESSION_PLAN_DTL_DTL();
                        if (gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString() != "0" && gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_SUCCESSION_PLAN_DTL_DTL> userCtx = new DataRepository<HR_SUCCESSION_PLAN_DTL_DTL>())
                            {
                                HR_SUCCESSION_PLAN_DTL_DTL = userCtx.Find(r =>
                                    (r.PROC_DTL_DTL_ID == gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        HR_SUCCESSION_PLAN_DTL_DTL.PROC_DTL_ID = HR_SUCCESSION_PLAN_DTL.PROC_DTL_ID;
                        HR_SUCCESSION_PLAN_DTL_DTL.PROC_EMP_ID = gvProfile.DataKeys[iLoop].Values["emp_id"].ToString();
                        HR_SUCCESSION_PLAN_DTL_DTL.PLAN_PROFILE_DTL_ID = gvProfile.DataKeys[iLoop].Values["plan_profile_dtl_id"].ToString();
                        HR_SUCCESSION_PLAN_DTL_DTL.PLAN_PROFILE_MIN_VALUE = short.Parse(gvProfile.DataKeys[iLoop].Values["Min_Value"].ToString());
                        HR_SUCCESSION_PLAN_DTL_DTL.PLAN_PROFILE_MAX_VALUE = short.Parse(gvProfile.DataKeys[iLoop].Values["Max_Value"].ToString());
                        HR_SUCCESSION_PLAN_DTL_DTL.PLAN_PROFILE_REQUIRED_VALUE = short.Parse(gvProfile.DataKeys[iLoop].Values["PLAN_PROFILE_REQUIRED_VALUE"].ToString());
                        HR_SUCCESSION_PLAN_DTL_DTL.PROC_EMP_RATING = short.Parse(gvProfile.DataKeys[iLoop].Values["employee_rating"].ToString());

                        CheckBox chkProfileActive = (CheckBox)gvProfile.Rows[iLoop].FindControl("chkProfileActive");
                        TextBox txtRecommendation = (TextBox)gvProfile.Rows[iLoop].FindControl("txtRecommendation");
                        TextBox txtProfRemarks = (TextBox)gvProfile.Rows[iLoop].FindControl("txtProfRemarks");

                        HR_SUCCESSION_PLAN_DTL_DTL.ENABLED_FLAG = chkProfileActive.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        HR_SUCCESSION_PLAN_DTL_DTL.PROC_RECOMMEND = txtRecommendation.Text;
                        HR_SUCCESSION_PLAN_DTL_DTL.PROC_REMARKS = txtProfRemarks.Text;


                        if (gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString() != "0" && gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString() != string.Empty)
                        {
                            HR_SUCCESSION_PLAN_DTL_DTL.PROC_DTL_DTL_ID = gvProfile.DataKeys[iLoop].Values["PROC_DTL_DTL_ID"].ToString();
                            HR_SUCCESSION_PLAN_DTL_DTL.MODIFIED_BY = this.LoggedUserName;
                            HR_SUCCESSION_PLAN_DTL_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpSecondChildEntity.Add(new Tuple<object, string>(HR_SUCCESSION_PLAN_DTL_DTL, "U"));
                        }
                        else
                        {

                            HR_SUCCESSION_PLAN_DTL_DTL.PROC_DTL_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_067_D_D".ToString(), false, true);
                            HR_SUCCESSION_PLAN_DTL_DTL.CREATED_BY = this.LoggedUserName;
                            HR_SUCCESSION_PLAN_DTL_DTL.CREATED_DATE = DateTime.Today;
                            tmpSecondChildEntity.Add(new Tuple<object, string>(HR_SUCCESSION_PLAN_DTL_DTL, "A"));
                        }
                        HR_SUCCESSION_PLAN_DTL_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_SUCCESSION_PLAN_DTL_DTL.PROC_DTL_DTL_ID);

                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveMultipleEntity<HR_SUCCESSION_PLAN_HDR, HR_SUCCESSION_PLAN_DTL, HR_SUCCESSION_PLAN_DTL_DTL>(HR_SUCCESSION_PLAN_HDR, tmpChildEntity, HR_SUCCESSION_PLAN_DTL, tmpSecondChildEntity, HR_SUCCESSION_PLAN_DTL_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleEntity<HR_SUCCESSION_PLAN_HDR, HR_SUCCESSION_PLAN_DTL, HR_SUCCESSION_PLAN_DTL_DTL>(HR_SUCCESSION_PLAN_HDR, tmpChildEntity, HR_SUCCESSION_PLAN_DTL, tmpSecondChildEntity, HR_SUCCESSION_PLAN_DTL_DTL, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("sTS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlProg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow tmpgvr = gvData.FooterRow;

                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
                DropDownList ddlProg = tmpgvr.FindControl("ddlProg") as DropDownList;

                FIN.BLL.HR.Course_BLL.GetCourseBasedPgm(ref ddlCourse, ddlProg.SelectedValue);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COURSE_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow tmpgvr = gvData.FooterRow;

                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourse") as DropDownList;
                DropDownList ddlSubject = tmpgvr.FindControl("ddlSubject") as DropDownList;

                FIN.BLL.HR.Course_BLL.GetSubjectBasedCourse(ref ddlSubject, ddlCourse.SelectedValue);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["SUBJECT_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlProfileId = tmpgvr.FindControl("ddlProfileId") as DropDownList;


                FIN.BLL.HR.SuccessionPlanning_BLL.GetEmpProfileName(ref ddlProfileId);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlProfileId.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["prof_id"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void GetProcess()
        {
            try
            {
                ErrorCollection.Clear();

                string profId = string.Empty;

                //GridViewRow gvr = gvProfile.SelectedRow;
                //CheckBox chkActive = gvr.FindControl("chkActive") as CheckBox;
                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {

                        CheckBox chkActive = (CheckBox)gvData.Rows[iLoop].FindControl("chkActive");
                        if (chkActive.Checked == true)
                        {
                            // profId +="'"+ gvData.DataKeys[iLoop].Values["prof_id"].ToString().Trim() + "',";
                            profId += "'" + gvData.DataKeys[iLoop].Values["PLAN_DTL_ID"].ToString().Trim() + "',";

                        }
                    }
                    profId = profId + "'0'";
                }

                if (Master.StrRecordId.Length > 3)
                {
                    dtProfileData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetSuccessPlanDtlDtl(profId)).Tables[0];
                }
                else
                {
                    dtProfileData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetProfileData(profId)).Tables[0];
                }

                BindProfileGrid(dtProfileData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GetProcess();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Enrollment ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlCourse = gvr.FindControl("ddlCourse") as DropDownList;
            DropDownList ddlProg = gvr.FindControl("ddlProgram") as DropDownList;
            DropDownList ddlSubject = gvr.FindControl("ddlSubject") as DropDownList;
            DropDownList ddlSchedule = gvr.FindControl("ddlSchedule") as DropDownList;
            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["enrl_dtl_id"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }


            slControls[0] = ddlCourse;
            slControls[1] = ddlProg;
            slControls[2] = ddlSubject;
            slControls[3] = ddlStaff;
            slControls[4] = ddlSchedule;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~DropDownList~DropDownList";
            string strMessage = Prop_File_Data["Course_P"] + " ~ " + Prop_File_Data["Program_P"] + " ~ " + Prop_File_Data["Subject_P"] + " ~ " + Prop_File_Data["Staff_P"] + " ~ " + Prop_File_Data["Schedule_P"] + "";
            // string strMessage = "Course ~ Program ~ Subject ~ Staff~Schedule";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = FINColumnConstants.PROG_ID + "='" + ddlProg.SelectedValue.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.SUBJECT_ID + "='" + ddlSubject.SelectedValue.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.COURSE_ID + "='" + ddlCourse.SelectedItem.Value.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.EMP_ID + "='" + ddlStaff.SelectedValue + "'";
            strCondition += " AND " + FINColumnConstants.TRN_SCH_HDR_ID + "='" + ddlSchedule.SelectedValue + "'";

            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (ddlCourse.SelectedItem != null)
            {
                drList["COURSE_ID"] = ddlCourse.SelectedItem.Value;
                drList["COURSE_DESC"] = ddlCourse.SelectedItem.Text;
            }
            if (ddlProg.SelectedItem != null)
            {
                drList["PROG_ID"] = ddlProg.SelectedItem.Value;
                drList["PROG_DESC"] = ddlProg.SelectedItem.Text;
            }
            if (ddlSubject.SelectedItem != null)
            {
                drList["SUBJECT_ID"] = ddlSubject.SelectedItem.Value;
                drList["SUBJECT_DESC"] = ddlSubject.SelectedItem.Text;
            }
            if (ddlSchedule.SelectedItem != null)
            {
                drList["TRN_SCH_HDR_ID"] = ddlSchedule.SelectedItem.Value;
                drList["TRN_DESC"] = ddlSchedule.SelectedItem.Text;
            }
            if (ddlStaff.SelectedItem != null)
            {
                drList["emp_id"] = ddlStaff.SelectedItem.Value;
                drList["emp_name"] = ddlStaff.SelectedItem.Text;
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void gvProfile_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_SUCCESSION_PLAN_HDR>(HR_SUCCESSION_PLAN_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Selected Index Changed Events
        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_CareerPath();
        }

        private void fn_fill_CareerPath()
        {
            CareerPlanDtls_BLL.GetCareerPath(ref ddlCareerPathName, ddlDept.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
        }
        protected void ddlCareerPathName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                fill_path_des();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void fill_path_des()
        {
            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(CareerPlanDtls_DAL.GetCareerPath(ddlDept.SelectedValue, ddlDesignation.SelectedValue, ddlCareerPathName.SelectedValue)).Tables[0];

            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    hf_PathHdrId.Value = dtData.Rows[0]["PATH_HDR_ID"].ToString();
                    txtPathName.Text = dtData.Rows[0]["PATH_DESC"].ToString();
                }
            }

            CareerPlanDtls_BLL.GetCareerPlan(ref ddlCareerPlan, ddlCareerPathName.SelectedValue);
        }

        private void GetEmpProfile()
        {
            dtGridData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetEmpProfile(ddlCareerPlan.SelectedValue)).Tables[0];
            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 0)
                {
                    BindGrid(dtGridData);
                }
            }
        }
        protected void ddlCareerPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetEmpProfile();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void GetEmpProfileDetail()
        {
            try
            {

                ErrorCollection.Clear();


                DataTable dtData = new DataTable();

                GridViewRow gvr = gvData.SelectedRow;

                DropDownList ddlProfileId = gvr.FindControl("ddlProfileId") as DropDownList;
                TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;

                dtData = DBMethod.ExecuteQuery(SuccessionPlanning_DAL.GetEmpProfileName(ddlProfileId.SelectedValue)).Tables[0];

                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtdesc.Text = dtData.Rows[0]["prof_name"].ToString();
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savbe", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlProfileId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GetEmpProfileDetail();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savbe", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void ddlDetail_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

    }
}