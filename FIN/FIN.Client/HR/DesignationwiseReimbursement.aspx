﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DesignationwiseReimbursement.aspx.cs" Inherits="FIN.Client.HR.DesignationwiseReimbursement" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblGroupCode">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlGroupCode" runat="server" AutoPostBack="True" Width="150px" CssClass="validate[required] RequiredField ddlStype"
                    onselectedindexchanged="ddlGroupCode_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px; height: 18px;">
                <asp:TextBox ID="txtDescription" Enabled="true" TabIndex="2" CssClass="txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddldesignation" runat="server" AutoPostBack="True" Width="150px" CssClass="validate[required] RequiredField ddlStype"
                     TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px; height: 18px;">
                <asp:TextBox ID="TextBox1" Enabled="true" TabIndex="2" CssClass="txtBox"
                    runat="server" MaxLength="50"></asp:TextBox>
            </div>
        </div>
          <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DELETED,HR_REIMB_DESIG_ID,REIMB_TYP_CODE,REIMB_TYP_ID" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Reimbursement Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlreimbtyp" Width="180px" TabIndex="11" runat="server" CssClass="RequiredField EntryFont ddlStype"
                            AutoPostBack="True"  onselectedindexchanged="ddlElementCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlreimbtyp" Width="180px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype"
                            AutoPostBack="True"  onselectedindexchanged="ddlElementCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElementCode"  CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("REIMB_TYP_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="12" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("REIMB_TYP_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtElementDescription" TabIndex="4" Enabled="true" Width="130px" MaxLength="100"
                                runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblElementDescription" Width="130px" Enabled="true" runat="server"
                                Text='<%# Eval("REIMB_TYP_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>

                          <asp:TemplateField HeaderText="Max Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="12" Enabled="false" Width="130px" MaxLength="100"
                                runat="server" CssClass=" RequiredField  txtBox" Text='<%# Eval("REIMB_MAX_AMT") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="4" Enabled="true" Width="130px" MaxLength="100"
                                runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmaxamt" Width="130px" Enabled="true" runat="server"
                                Text='<%# Eval("REIMB_MAX_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Frequency">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlreimbfreq" Width="180px" TabIndex="11" runat="server" CssClass="RequiredField EntryFont ddlStype"
                           >
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlreimbfreq" Width="180px" TabIndex="3" runat="server" CssClass="RequiredField EntryFont ddlStype"
                           >
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblreimbfrq"  CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("REIMB_FREQ") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="13" Width="130px" runat="server" CssClass="EntryFont RequiredField txtBox" Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpfromDate" TabIndex="5" Width="130px" runat="server" CssClass="EntryFont RequiredField txtBox" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpfromDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblfromDate" runat="server" Width="130px" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpToDate" runat="server" TabIndex="14" Width="130px" CssClass="EntryFont  txtBox" Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpToDate" runat="server" TabIndex="6" Width="130px" CssClass="EntryFont  txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" Width="130px" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="8" runat="server" AlternateText="Edit" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="9" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="15" runat="server" AlternateText="Update" CommandName="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" TabIndex="16" AlternateText="Cancel" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="7" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                             />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn"  />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
