﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_Reports
{
    public partial class RPTAccountingPeriodParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingPeriodReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingPeriodReportATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            FIN.BLL.GL.Organisation_BLL.getPeriodCalendarDtl(ref ddlCalendarYear,true);
            Lookup_BLL.GetLookUpValues(ref ddlPeriodStatus, "CPS", true);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();

          
            ErrorCollection = CommonUtils.ValidateDateRange(txtCreatedFromDate.Text, txtCreatedToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtModifiedFromDate.Text, txtModToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (ddlCalendarYear.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("CalendarID", ddlCalendarYear.SelectedValue.ToString());
                    htFilterParameter.Add("CalYearName", ddlCalendarYear.SelectedItem.Text.ToString());
                }

                if (ddlPeriodStatus.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("PeriodStatusID", ddlPeriodStatus.SelectedValue.ToString());
                    htFilterParameter.Add("PeriodStatus", ddlPeriodStatus.SelectedItem.Text.ToString());
                }
                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtCreatedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtCreatedFromDate.Text);
                }
                if (txtCreatedToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtCreatedToDate.Text);
                }
                if (txtModifiedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtModifiedFromDate.Text);
                }
                if (txtModToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtModToDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getAcctPeriodReportData());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountingPeriodReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}