﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;
using FIN.BLL.GL;

namespace FIN.Client.GL_Reports
{
    public partial class RPTRevenueReport : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenue", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
                FillJournalNumber();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.getSegmentValueNotInGlobalSegment(ref ddlSegment);
            ddlSegment.Items.RemoveAt(0);
            ddlSegment.Items.Insert(0, new ListItem("All", "All"));


            AccountCodes_BLL.getAccCodeBasedOrgR(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgR(ref ddlToAccNumber);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            ddlAccCode.Items.RemoveAt(0);
            ddlAccCode.Items.Insert(0, new ListItem("All", "All"));
        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            FillJournalNumber();
        }
        private void FillJournalNumber()
        {
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlFromJournal, txtFromDate.Text, txtToDate.Text);
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlToJournal, txtFromDate.Text, txtToDate.Text);
        }


        private void ParamValidation()
        {

            ErrorCollection.Clear();
           
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtDebitFromAmt.Text, txtDebitToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtCreditFromAmt.Text, txtCreditToAmt.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                /* if (txtFromAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                 }

                 if (txtToAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                 }*/


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (ddlSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CostCentre", ddlSegment.SelectedItem.Text);
                }

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.FROM_DATE, txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.TO_DATE, txtToDate.Text);
                }
                if (txtDebitFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_From_Amt", txtDebitFromAmt.Text);
                    htFilterParameter.Add("DebitFromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtDebitFromAmt.Text));
                }
                if (txtDebitToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_To_Amt", txtDebitToAmt.Text);
                    htFilterParameter.Add("DebitToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtDebitToAmt.Text));
                }
                if (txtCreditFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_From_Amt", txtCreditFromAmt.Text);
                    htFilterParameter.Add("CreditFromAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditFromAmt.Text));
                }
                if (txtCreditToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_To_Amt", txtCreditToAmt.Text);
                    htFilterParameter.Add("CreditToAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditToAmt.Text));
                }
                if (ddlFromJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromJournal", ddlFromJournal.SelectedValue.ToString());
                    htFilterParameter.Add("From_Journal", ddlFromJournal.SelectedItem.Text.ToString());
                }

                if (ddlToJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToJournal", ddlToJournal.SelectedValue.ToString());
                    htFilterParameter.Add("To_Journal", ddlToJournal.SelectedItem.Text.ToString());
                }
                string str_segment = "";
                string str_acctcode = "";
                string str_unpost = "0";
                if (chkUnPosted.Checked)
                {
                    str_unpost = "1";
                }
                if (ddlAccCode.SelectedValue.ToString().Trim().Length == 0)
                {
                    str_acctcode = "ALL";
                }
                else
                {
                    str_acctcode = ddlAccCode.SelectedValue;
                }

                if (ddlSegment.SelectedValue.ToString().Trim().Length == 0)
                {
                    str_segment = "ALL";
                }
                else
                {
                    str_segment = ddlSegment.SelectedValue;
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.GetRevenueRPT(str_unpost, str_acctcode, str_segment));

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}