﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BalanceSheetTemplateDataViewParam.aspx.cs" Inherits="FIN.Client.GL_Reports.BalanceSheetTemplateDataViewParam1" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_showGraph() {
            fn_JqueryTreeView();


        }
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }



        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#<%=hf_tmpl_id.ClientID %>").val(str_tmpl_id);
            $("#<%=btnTmpl.ClientID %>").click();



        }
        
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                Template Name
            </div>
            <div class="divtxtBox" style="float: left; width: 450px">
                <asp:DropDownList ID="ddlTemplateName" runat="server" TabIndex="1" CssClass="validate[required] RequiredField  ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="Div3">
                Type
            </div>
            <div class="divtxtBox" style="float: left; width: 450px">
                <asp:RadioButtonList ID="rbType" runat="server" CssClass="lblBox" 
                    RepeatDirection="Horizontal"  AutoPostBack="true"
                    onselectedindexchanged="rbType_SelectedIndexChanged">
                    <asp:ListItem Value="Y" Selected="True">Yearly</asp:ListItem>
                    <asp:ListItem Value="Q">Quartly</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divYearly" runat="server" visible="true">
            <div class="lblBox" style="float: left; width: 150px" id="lblDate">
                No Of years
            </div>
            <div class="divtxtBox" style="float: left; width: 50px">
                <asp:TextBox ID="txtNoOfYear" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"
                    Text="1" MaxLength="1"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                    TargetControlID="txtNoOfYear" />
            </div>
        </div>
        <div class="divRowContainer" id="div_Quartly" runat="server" visible="false">
            <div class="lblBox" style="float: left; width: 150px">
                Quarter Type
            </div>
            <div class="divtxtBox" style="float: left; width:250px">
                <asp:RadioButtonList ID="rbQuarterType" runat="server" CssClass="lblBox" 
                    RepeatDirection="Horizontal" CellSpacing="1">
                    <asp:ListItem Value="1" Selected="True">Q1 &nbsp;&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="2">Q2 &nbsp;&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="3">Q3 &nbsp;&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="4">Q4 &nbsp;&nbsp;&nbsp;</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 570px">
                <div style="float: right">
                    <asp:ImageButton ID="imgBtnExport" runat="server" ImageUrl="~/Images/print.png" ToolTip="Export Result as PDF"
                        CommandName="PDF" AlternateText="PDF" Visible="false" OnClick="imgBtnExport_Click" />
                    <asp:HiddenField ID="hf_PrintData" runat="server" Value="" />
                </div>
                <div style="float: right">
                    <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" ToolTip="View"
                        AlternateText="View" OnClick="btnView_Click" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="float: left; width: 30%;overflow:scroll;">
                <div id="div_Template" runat="server" class="tree" style=" width:800px">
                </div>
                <asp:HiddenField ID="hf_tmpl_id" runat="server" Value="" />
                <asp:Button ID="btnTmpl" runat="server" Text="Button" OnClick="btnTmpl_Click" Style="display: none" />
            </div>
            <div style="float: left; width: 70%">
                <div class="divRowContainer" align="center" id="divExpandLevel" runat="server" visible="false">
                    <div class="lblBox" style="float: left; width: 150px" id="Div1">
                        Expand Level
                    </div>
                    <div class="divtxtBox" style="float: left; width: 50px">
                        <asp:TextBox ID="txtExpandLevel" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"
                            Text="1" MaxLength="1" AutoPostBack="True" OnTextChanged="txtExpandLevel_TextChanged"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtExpandLevel" />
                    </div>
                    <div class="lblBox" style="float: left; width: 150px" id="Div2">
                        <asp:Button ID="btnExpandAll" runat="server" Text="Expand All" OnClick="btnExpandAll_Click" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" id="div_gvData">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                        CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                        DataKeyNames="B_TYPE,ACCT_GRP_ID,PARENT_GROUP_ID,GROUP_LEVEL,ACCT_GRP_NAME,NEW_ACCT_GRP_NAME,GROUP_NUMBER">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px" Visible="true">
                                <ItemTemplate>
                                    <asp:ImageButton ID="img_Det" ImageUrl="../Images/expand_blue.png" runat="server"
                                        CommandName="PLUS" AlternateText="Details" OnClick="img_Det_Click" />
                                </ItemTemplate>
                                <ItemStyle Width="40px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAcct_grp_Name" runat="server" Text='<%# Eval("NEW_ACCT_GRP_NAME") %>'></asp:Label>
                                    <asp:HiddenField ID="hf_Parent_group_id" runat="server" Value='<%# Eval("PARENT_GROUP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_acct_grp_id" runat="server" Value='<%# Eval("ACCT_GRP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_Group_Level" runat="server" Value='<%# Eval("GROUP_LEVEL") %>'
                                        ClientIDMode="Static" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="GROUP_NUMBER" HeaderText="Notes">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR1_BALANCE" HeaderText="Year1">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR2_BALANCE" HeaderText="Year2" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR3_BALANCE" HeaderText="Year3" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR4_BALANCE" HeaderText="Year4" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR5_BALANCE" HeaderText="Year5" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PARENT_GROUP_ID" HeaderText="PARENT_GROUP_ID" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
