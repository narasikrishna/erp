﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTGroupLinks.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTGroupLinks" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
<div class="divFormcontainer" style="width: 400px" id="div1">
<div class="divRowContainer">
 <div class="lblBox LNOrient" style="  width: 150px" id="lblGroupName">
                Group Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlGroupName" runat="server" 
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1" Width="150px">
                </asp:DropDownList>
            </div>
            </div>

    <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                       <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn"  />--%>
                    </td>
                    
                </tr>
            </table>
        </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
