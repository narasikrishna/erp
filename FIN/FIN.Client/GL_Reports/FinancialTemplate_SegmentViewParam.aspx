﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="FinancialTemplate_SegmentViewParam.aspx.cs" Inherits="FIN.Client.GL_Reports.FinancialTemplate_SegmentViewParam" %>



<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_showGraph() {
            fn_JqueryTreeView();


        }
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }



        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#<%=hf_tmpl_id.ClientID %>").val(str_tmpl_id);
            $("#<%=btnTmpl.ClientID %>").click();



        }
        
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
                Template Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlTemplateName" runat="server" TabIndex="1" CssClass="validate[required] RequiredField  ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
       
        <div class="divRowContainer" id="divYearly" >
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                Year
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
              <asp:DropDownList ID="ddlYear" runat="server" TabIndex="2" 
                    CssClass="validate[required] RequiredField  ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_Period" >
             <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
              <asp:DropDownList ID="ddlPeriod" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div4" >
             <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
               Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:CheckBox ID="chkWithZero" runat="server" />
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 570px">
                <div style="float: right">
                    <asp:ImageButton ID="imgBtnExport" runat="server" ImageUrl="~/Images/print.png" ToolTip="Export Result as PDF"
                        CommandName="PDF" AlternateText="PDF" Visible="false" OnClick="imgBtnExport_Click" />
                    <asp:HiddenField ID="hf_PrintData" runat="server" Value="" />
                </div>
                <div style="float: right">
                    <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" ToolTip="View"
                        AlternateText="View" OnClick="btnView_Click" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="  width: 30%;overflow:scroll;">
                <div id="div_Template" runat="server" class="tree" style=" width:800px">
                </div>
                <asp:HiddenField ID="hf_tmpl_id" runat="server" Value="" />
                <asp:Button ID="btnTmpl" runat="server" Text="Button" OnClick="btnTmpl_Click" Style="display: none" />
            </div>
            <div style="  width: 70%">
                <div class="divRowContainer" align="center" id="divExpandLevel" runat="server" visible="false">
                    <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                        Expand Level
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 50px">
                        <asp:TextBox ID="txtExpandLevel" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"
                            Text="1" MaxLength="1" AutoPostBack="True" OnTextChanged="txtExpandLevel_TextChanged"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtExpandLevel" />
                    </div>
                    <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                        <asp:Button ID="btnExpandAll" runat="server" Text="Expand All" OnClick="btnExpandAll_Click" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="LNOrient" id="div_gvData">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="TRUE" Width="100%"
                        CssClass="DisplayFont Grid" ShowFooter="false" OnRowDataBound="gvData_RowDataBound"
                        DataKeyNames="B_TYPE,ACCT_GRP_ID,PARENT_GROUP_ID,GROUP_LEVEL">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px" Visible="true">
                                <ItemTemplate>
                                    <asp:ImageButton ID="img_Det" ImageUrl="../Images/expand_blue.png" runat="server"
                                        CommandName="PLUS" AlternateText="Details" OnClick="img_Det_Click" />
                                </ItemTemplate>
                                <ItemStyle Width="40px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAcct_grp_Name" runat="server" Text='<%# Eval("ACCT_GRP_NAME") %>'></asp:Label>                                   
                                </ItemTemplate>
                                 <ItemStyle Width="200px"></ItemStyle>
                            </asp:TemplateField>
                            
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnView").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
