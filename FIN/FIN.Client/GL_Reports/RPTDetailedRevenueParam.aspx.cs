﻿using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;
using FIN.BLL.GL;

namespace FIN.Client.GL_Reports
{
    public partial class RPTDetailedRevenueParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenue", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                txtNoYear.Text = "1";
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
      
        protected void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlsegment, false);



            AccountCodes_BLL.getRevenueAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getRevenueAccCodeBasedOrgRep(ref ddlToAccNumber);

            ddlsegment.SelectedIndex = 1;
            //FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtNoYear.Text.ToString() == "0")
                {
                    ErrorCollection.Add("ZeroNOTALLOED", "No of year must be greater then or equal to 1 ");
                    return;
                }
                if (txtNoYear.Text.ToString().Trim().Length == 0)
                {
                    txtNoYear.Text = "1";
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_FROM_DT, txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_TO_DT, txtToDate.Text);
                }
                //if (ddlperiod.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_NAME, ddlperiod.SelectedItem.Text.ToString());
                //}
                if (ddlsegment.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.SEGMENT_VALUE_ID, ddlsegment.SelectedValue.ToString());
                }
                /*  if (txtFromAccount.Text.ToString().Length > 0)
                  {
                      htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                  }

                  if (txtToAccount.Text.ToString().Length > 0)
                  {
                      htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                  }*/


                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }

                if (txtNoYear.Text != string.Empty)
                {
                    htFilterParameter.Add("noyrs", txtNoYear.Text);
                }
                if (chkUnPosted.Checked)
                {
                    htFilterParameter.Add("UnPost", "1");
                }
                else
                {
                    htFilterParameter.Add("UnPost", "0");
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }

                string str_Years = "";
                for (int iLoop = 0; iLoop < int.Parse(txtNoYear.Text); iLoop++)
                {
                    str_Years = str_Years + (DateTime.Now.Year - iLoop).ToString() + " - ";
                }


                str_Years = str_Years.Substring(0, str_Years.Length - 2);

                htFilterParameter.Add("CurrentYear", str_Years);


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getDetailedRevenue(txtNoYear.Text));

                //if (int.Parse(txtNoYear.Text) == 2)
                //{

                //    DataTable dt_diffData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getDetailedRevwithDiff()).Tables[0];

                //    for (int iLoop = 0; iLoop < dt_diffData.Rows.Count; iLoop++)
                //    {
                //        DataRow dr = ReportData.Tables[0].NewRow();
                //        for (int Kloop = 0; Kloop < ReportData.Tables[0].Columns.Count; Kloop++)
                //        {
                //            dr[Kloop] = dt_diffData.Rows[iLoop][Kloop];
                //        }
                //        ReportData.Tables[0].Rows.Add(dr);
                //    }
                //}


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}