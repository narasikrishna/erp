﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.Reports.GL
{
    public partial class RPTTrailBalance_DetailParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();
                txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Text);
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                }
                if (txtDate.Text != string.Empty)
                {
                    htFilterParameter.Add("Balance_Date", txtDate.Text);
                }
                if (txtFromAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                }

                if (txtToAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                }
                if (txtFromDebitAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_DebitAmt", txtFromDebitAmt.Text);
                }
                if (txtDebitToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_DebitAmt", txtDebitToAmt.Text);
                }
                if (txtFromCreditAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_CreditAmt", txtFromCreditAmt.Text);
                }
                if (txtToCreditAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_CreditAmt", txtToCreditAmt.Text);
                }
                string str_Unpost = "0";
                if (chkUnPost.Checked)
                {
                    str_Unpost = "1";
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportData = FIN.DAL.GL.TrailBalance_DAL.GetSP_TrailBalanceDetail(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text),str_Unpost, FIN.BLL.GL.TrailBalance_BLL.GetTrailBalanceDetailReportData());


                

               

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}