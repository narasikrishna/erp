﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_Reports
{
    public partial class ExchangeRateParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
              
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateReportATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }

        private void FillComboBox()
        {
            string baseCurrency = string.Empty;
            DataTable dtbasecurrency = new DataTable();

            FIN.BLL.GL.Currency_BLL.getCurrencyDetails(ref ddlCurrency, true);

            dtbasecurrency = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.getBasecurrency(VMVServices.Web.Utils.OrganizationID).ToString()).Tables[0];
            baseCurrency = dtbasecurrency.Rows[0]["CURRENCY_DESC"].ToString();
            
            htFilterParameter.Add("BaseCurrency", baseCurrency);

            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);
        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromCreatedDate.Text, txtToCreatedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromModifiedDate.Text, txtToModifiedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlCurrency.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CurrencyID", ddlCurrency.SelectedValue);
                    htFilterParameter.Add("CurrencyName", ddlCurrency.SelectedItem.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("FromDate", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("ToDate", txtToDate.Text);
                }

                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtFromCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtFromCreatedDate.Text);
                }
                if (txtToCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtToCreatedDate.Text);
                }
                if (txtFromModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtFromModifiedDate.Text);
                }
                if (txtToModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtToModifiedDate.Text);
                }
              
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.getExchangeRateReportData());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExchangeRateReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}