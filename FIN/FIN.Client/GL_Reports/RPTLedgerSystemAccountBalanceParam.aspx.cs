﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;
using FIN.BLL.GL;

namespace FIN.Client.GL_Reports
{
    public partial class RPTLedgerSystemAccountBalanceParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedSysAccBal", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                //FillStartDate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedSysAccBalATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeswithAll(ref ddlAccountCodes);

            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            //ddlAccCode.Items.RemoveAt(0);
            //ddlAccCode.Items.Insert(0, new ListItem("All", "All"));
        }
        //// private void FillStartDate()
        // {

        //     string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
        //     DataTable dtDate = new DataTable();
        //     if (str_finyear != string.Empty)
        //     {
        //         dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
        //         txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
        //         txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
        //     }



        //     //if (ddl_GB_FINYear.Items.Count > 0)
        //     //{
        //     //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
        //     //    ddl_GB_FINYear.SelectedValue = str_finyear;
        //     //}
        // }

        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromValue.Text, txtToValue.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                if (ddlperiod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                    htFilterParameter.Add(FINColumnConstants.PERIOD_NAME, ddlperiod.SelectedItem.Text.ToString());
                }
                if (ddlAccountCodes.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.CODE_ID, ddlAccountCodes.SelectedValue.ToString());

                }

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                /* if (txtFromAccNo.Text != string.Empty)
                 {
                     htFilterParameter.Add("From_AccNo" ,txtFromAccNo.Text);
                 }
                 if (txtToAccNo.Text != string.Empty)
                 {
                     htFilterParameter.Add("To_AccNo", txtToAccNo.Text);
                 }*/

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("From_AccNo", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("To_AccNo", ddlToAccNumber.SelectedValue);
                }

                if (chkWithZero.Checked)
                {
                    if (txtFromValue.Text.Length > 0 && txtToValue.Text.Length > 0)
                    {
                        ErrorCollection.Add("FromToZero", "Please enter (From Value & To Value) or Include Zeros");
                        return;
                    }
                }
                if (txtFromValue.Text != string.Empty && txtToValue.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Value", txtFromValue.Text);
                    htFilterParameter.Add("FromValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromValue.Text));
                    htFilterParameter.Add("To_Value", txtToValue.Text);
                    htFilterParameter.Add("ToValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtToValue.Text));
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                //if (ddlGlobalSegment.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SEGMENT_VALUE", ddlGlobalSegment.SelectedItem.Text);
                //    htFilterParameter.Add("SEGMENT_VALUE_ID", ddlGlobalSegment.SelectedItem.Value);
                //}
                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.FROM_DATE txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.TO_DATE, txtToDate.Text);
                //}
                //if (ddlAccCode.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.CODE_ID, ddlAccCode.SelectedValue.ToString());
                //}

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportData = FIN.BLL.GL.AccountingGroupLinks_BLL.GetAccountBalanceReportData();
                // ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodes());

                ReportFormulaParameter = htHeadingParameters;
                if (ErrorCollection.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedSysAccBalReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}