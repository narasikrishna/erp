﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTLedgerSystemAccountBalanceParam.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTLedgerSystemAccountBalanceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div5" runat="server" visible="false">
                Account Codes
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px" id="accCodes" runat="server"
                visible="false">
                <asp:DropDownList ID="ddlAccountCodes" runat="server" CssClass="ddlStype" TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 120px" id="lblAccountingYear">
                From Account Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtFromAccNo" CssClass="txtBox" runat="server" TabIndex="3"></asp:TextBox>
            </div>
          <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div1">
                To Account Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtToAccNo" CssClass="txtBox" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 500px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 500px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                From Value
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtFromValue" CssClass="txtBox_N" runat="server" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                    ValidChars=".,-" TargetControlID="txtFromValue" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 80px" id="Div3">
                To Value
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtToValue" CssClass="txtBox_N" runat="server" TabIndex="6"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                    ValidChars=".,-" TargetControlID="txtToValue" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div6">
                Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" TabIndex="7" Checked="false" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="8" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--<div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10"></div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10"></div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="Div3">
                    Account Code
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlAccCode" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10"></div>
            <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
   <%-- <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
