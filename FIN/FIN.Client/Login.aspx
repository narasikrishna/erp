﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FIN.Client.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FIN Login</title>
    <link href="Styles/Controls.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <style type="text/css">
        .Container
        {
            background-color:rgb(109,142,195);
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(16,22,35)), to(rgb(109,142,195)));
            background: -moz-linear-gradient(top, rgb(16,22,35) 0%, rgb(109,142,195) 100%);
       
        }
        .MCHeader
        {
            <%--background-color: #f5eeeb;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(16,22,35)), to(rgb(109,142,195)));
            background: -moz-linear-gradient(top, rgb(16,22,35) 0%, rgb(109,142,195) 100%);--%>
        }
        .MCContent
        {
           <%-- background-color: #f5eeeb;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#f5eeeb), to(#9b847a));
            background: -moz-linear-gradient(top, #f5eeeb 0%, #9b847a 100%);
            --%>
        }
        .divtxtbox
        {
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
        }
         .divLoginBtn
        {
            background-color: rgb(109,142,195);
            background: -webkit-gradient(radial, 50% 50%, 0, 50% 50%, 100, from(rgb(109,142,195)), to(rgb(16,22,35)));
            background: -moz-radial-gradient(50% 50%, farthest-side,rgb(109,142,195) , rgb(16,22,35));
             -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
        }
    </style>
</head>
<body>
    <form id="frmLogin" runat="server">
    <div id="Container" align="center" style="border: 0px solid #f5eeeb" class="Container">
        <div id="MainContainer" style="width: 1024px; height: 600px; border: 0px solid #f5eeeb">
            <div style="height: 100px; width: 100%" class="MCHeader">
            </div>
            <div style="height: 500px; width: 100%" class="MCContent">
                <div style="height: 80px">
                </div>
                <div>
                    <img src="Images/login.png" width="40px" height="40px" />
                    <label for="lblloginhead" style="font-size: 50px; font-family: Sans-Serif; font-weight: bold;
                        color: White;">
                        LOGIN</label>
                </div>
                <div style="height: 30px">
                </div>
                <div>
                    <table>
                        <tr>
                            <td>
                                <div class="divtxtbox" style="width: 200px; height: 30px; background-color: Black">
                                    <asp:TextBox ID="txtUserID" runat="server" Style="background-color: transparent;
                                        color: White" Width="100%" Height="100%" BorderStyle="None" placeholder="UserName"
                                        required></asp:TextBox>
                                </div>
                                <br />
                                <div class="divtxtbox" style="width: 200px; height: 30px; background-color: Black">
                                    <asp:TextBox ID="txtPassword" runat="server" Style="background-color: transparent;
                                        color: White" Width="100%" Height="100%" BorderStyle="None" TextMode="Password"
                                        placeholder="Password" required></asp:TextBox>
                                </div>
                            </td>
                            <td  valign="middle">
                                &nbsp;&nbsp;
                                <div style="width: 80px; height: 80px" class="divLoginBtn">
                                    <asp:Button ID="btnLogin" OnClick="btnlogin_Click" runat="server" Text="Login" style="background-color: transparent;font-size: 20px; font-family: Sans-Serif; font-weight: bold" BorderStyle="None" Width="100%" Height="100%" /> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <%--<div align="center">
        <div style="border: 2px solid #000000; width: 1024px; margin-top: 5px" align="left">
            <table width="1024" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td colspan="2">
                        <div style="height: 180px; margin-top: 0px; vertical-align: middle">
                            <asp:Label runat="server" ID="lbl_iportal" Text="FIN" Font-Bold="True" Font-Names="Lucida Sans Typewriter"
                                Font-Size="50pt" ForeColor="#DCECF8"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr align="left">
                    <td style="width: 300px;">
                        <div style="margin-left: 10%; margin-top: 2px; border: 0px solid #000000; border-right: 1px solid #000;
                            height: 300px">
                            <table>
                                <tr>
                                    <td class="adminFormFieldHeading">
                                        UserName
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adminFormFieldData">
                                        <asp:TextBox CssClass="standardTextBox" MaxLength="50" Width="190px" ID="txtUserID"
                                            Height="15px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adminFormValidator">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserID">* Required</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="adminFormFieldHeading">
                                        Password
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adminFormFieldHeading">
                                        <asp:TextBox CssClass="standardTextbox" Width="190px" ID="txtPassword" runat="server"
                                            TextMode="Password" BorderStyle="None" BorderWidth="0px" BackColor="Aqua"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adminFormValidator">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                            TextMode="Password">* Required</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Label runat="server" Visible="false" ID="lblError" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr align="right">
                                    <td class="adminFormButtons" style="height: 5px; padding: 0px;" align="right">
                                        <table id="Table3" cellspacing="2" cellpadding="2" border="0" align="right">
                                            <tr align="right">
                                                <td>
                                                    <asp:Button CssClass="button" ID="btnlogin" Text="Login" runat="server" OnClick="btnlogin_Click">
                                                    </asp:Button>
                                                </td>
                                                <td>
                                                    <input class="button" type="reset" value="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 724px;" valign="top" align="left">
                        <div style="margin-left: 2%; margin-top: 2px; border: 0px solid #000; margin-right: 2%;">
                            <table>
                                <tr>
                                    <td>
                                        <div style="width: 700px; overflow: hidden">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <div style="margin-left: 0%;">
                            <img src="Images/footer.jpg" width="100%" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
    </form>
</body>
</html>
