CREATE OR REPLACE PACKAGE GL_PKG IS
  --- ***************************************************************************************************************
  --- $Header: GL_PKG 1.0 2014-04-01 $
  --- ***************************************************************************************************************
  --- Program      :   GL_PKG
  --- Description          :   All GL related codes goes here
  --- Author       :   C.S.Ilampooranan ,VMV Systems Pvt Ltd
  --- Date   :   01\04\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    01\04\2014          Ilam
  --- ***************************************************************************************************************

  PROCEDURE generate_accounting_period(p_cal_hdr_id IN VARCHAR2,
                                       p_adj_period IN NUMBER,
                                       p_cal_dtl_id IN VARCHAR2,
                                       p_from_dt    IN DATE,
                                       p_to_dt      IN DATE);

  PROCEDURE gl_exchange_rates(p_currency_id IN VARCHAR2,
                              p_from_dt     DATE,
                              p_to_dt       DATE,
                              p_std_rate    NUMBER,
                              p_comp_id     VARCHAR2);

  FUNCTION get_acct_group_desc(p_acct_grp_id IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_acct_period_name(p_comp_id IN VARCHAR2,
                                p_cal_id  IN VARCHAR2,
                                p_date    IN DATE) RETURN VARCHAR2;

  PROCEDURE GL_Trial_Balance_account_group(p_org_id            IN VARCHAR2,
                                           p_global_segment_id IN VARCHAR2,
                                           p_date              IN DATE);

  PROCEDURE GL_Trial_Balance_detail(p_org_id            IN VARCHAR2,
                                    p_global_segment_id IN VARCHAR2,
                                    p_date              IN DATE);

  PROCEDURE GL_Account_Group_Summary(p_org_id            IN VARCHAR2,
                                     p_global_segment_id IN VARCHAR2,
                                     p_from_date         IN DATE,
                                     p_to_date           IN DATE,
                                     p_group_id          IN VARCHAR2,
                                     p_unposted          IN VARCHAR2);

  PROCEDURE GL_Account_Group_Monthly_Sum(p_org_id            IN VARCHAR2,
                                         p_global_segment_id IN VARCHAR2,
                                         p_from_date         IN DATE,
                                         p_to_date           IN DATE);

  FUNCTION Fn_Get_Exchange_Rate(P_COMP_ID     VARCHAR2,
                                P_CURRENCY_ID VARCHAR2,
                                P_DATE        DATE,
                                P_TYPE        VARCHAR2) RETURN NUMBER;

  PROCEDURE GL_Account_Monthly_Summary(p_org_id            IN VARCHAR2,
                                       p_global_segment_id IN VARCHAR2,
                                       p_from_date         IN DATE,
                                       p_to_date           IN DATE,
                                       P_ACCT_CODE         IN VARCHAR2,
                                       P_UNPOSTED          IN VARCHAR2);

  PROCEDURE GL_OPENING_BALANCE(P_CAL_DTL_ID        IN VARCHAR2,
                               P_PERIOD_NAME       IN VARCHAR2,
                               P_PERIOD_STATUS_OLD IN VARCHAR2,
                               P_PERIOD_STATUS_NEW IN VARCHAR2,
                               P_PERIOD_NUMBER     IN NUMBER,
                               P_PERIOD_FROM_DATE  IN DATE,
                               P_COMP_ID           IN VARCHAR2,
                               P_PERIOD_ID         IN VARCHAR2,
                               P_PERIOD_TYPE       IN VARCHAR2);

  PROCEDURE GL_BALANCE_UPDATE(P_JOURNAL_ID           IN VARCHAR2,
                              P_JE_COMP_ID           IN VARCHAR2,
                              P_je_currency_id       IN VARCHAR2,
                              P_je_global_segment_id IN VARCHAR2,
                              P_je_period_id         IN VARCHAR2);
  FUNCTION Get_acct_codes(p_grp_id IN VARCHAR2) RETURN clob;
  FUNCTION Get_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION Get_first_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2;
  PROCEDURE GL_ACC_GRP_LINK(p_org_id IN VARCHAR2, p_acc_grp_id IN VARCHAR2);
  PROCEDURE GL_ACC_GRP_LINK1(p_org_id     IN VARCHAR2,
                             p_acc_grp_id IN VARCHAR2);

  PROCEDURE GL_Statement_Of_Accounts(p_org_id            IN VARCHAR2,
                                     p_global_segment_id IN VARCHAR2,
                                     p_from_date         IN DATE,
                                     p_to_date           IN DATE,
                                     P_ACCT_CODE         IN VARCHAR2,
                                     P_UNPOSTED          IN VARCHAR2);
END GL_PKG;

 
 
 
 
 
 
 
/
CREATE OR REPLACE PACKAGE BODY GL_PKG AS
  --- ***************************************************************************************************************
  --- $Body: GL_PKG 1.0 2014-04-01 $
  --- ***************************************************************************************************************
  --- Program      :   INV_PKG
  --- Description          :   All  Inventory related codes goes here
  --- Author       :   C.S.Ilampooranan ,VMV Systems Pvt Ltd
  --- Date   :   01\04\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    01\04\2014          Ilam
  --- ***************************************************************************************************************

  FUNCTION currency_validation(p_currency_id VARCHAR2) RETURN BOOLEAN IS
    v_exists NUMBER := 0;
  BEGIN
    SELECT COUNT(1)
      INTO v_exists
      FROM ssm_currencies sc
     WHERE sc.currency_id = p_currency_id;

    IF v_exists > 0 THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END currency_validation;

  FUNCTION company_validation(p_comp_id VARCHAR2) RETURN BOOLEAN IS
    v_exists NUMBER := 0;
  BEGIN
    SELECT COUNT(*)
      INTO v_exists
      FROM gl_companies_hdr gch
     WHERE gch.comp_id = p_comp_id;

    IF v_exists > 0 THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END company_validation;

  PROCEDURE generate_accounting_period(p_cal_hdr_id IN VARCHAR2,
                                       p_adj_period IN NUMBER,
                                       p_cal_dtl_id IN VARCHAR2,
                                       p_from_dt    IN DATE,
                                       p_to_dt      IN DATE) IS
    CURSOR c_apd IS
      SELECT ah.*
        FROM gl_acct_calendar_hdr ah, gl_acct_calendar_dtl ad
       WHERE ah.pk_id = ad.pk_id;

    v_from_dt         DATE;
    v_start_dt        DATE;
    v_end_dt          DATE;
    v_adj_period      NUMBER := 0;
    v_adj_period_name VARCHAR2(240);
    v_period_number   NUMBER := 0;
  BEGIN
    DELETE FROM gl_acct_period_dtl WHERE cal_dtl_id = p_cal_dtl_id;

    COMMIT;

    v_from_dt    := p_from_dt;
    v_adj_period := p_adj_period;

    WHILE v_from_dt <= p_to_dt LOOP
      --dbms_output.put_line('Dates '||to_char(v_from_dt,'DD-MON-YY'));
      v_start_dt := LAST_DAY(ADD_MONTHS(v_from_dt, -1)) + 1;
      v_end_dt   := LAST_DAY(v_from_dt);
      DBMS_OUTPUT.put_line('Start date ' ||
                           TO_CHAR(v_start_dt, 'DD-MON-YYYY') ||
                           ' - End date ' ||
                           TO_CHAR(v_end_dt, 'DD-MON-YYYY'));
      v_period_number := v_period_number + 1;

      --start
      INSERT INTO gl_acct_period_dtl
        (pk_id,
         child_id,
         period_id,
         period_name,
         period_type,
         period_from_dt,
         period_to_dt,
         period_status,
         period_first_close_dt,
         cal_dtl_id,
         enabled_flag,
         workflow_completion_status,
         created_by,
         created_date,
         modified_by,
         modified_date,
         attribute1,
         attribute2,
         attribute3,
         attribute4,
         attribute5,
         attribute6,
         attribute7,
         attribute8,
         attribute9,
         attribute10,
         period_number)
      VALUES
        (1, --pk_id
         1, --child_id
         ssm.get_next_sequence('GLPRD', 'EN'), --GL_ACCT_PERIOD_DTL_SEQ.NEXTVAL, --period_id
         --'PERIOD-' ||
         TO_CHAR(v_from_dt, 'MON-YYYY'), --period_name
         'ACTPERIOD', --period_type
         v_start_dt, --period_from_dt
         v_end_dt, --period_to_dt
         'NOP', --period_status
         NULL, --period_first_close_dt
         p_cal_dtl_id, --cal_dtl_id
         '1', --enabled_flag
         '1', --workflow_completion_status
         'Admin', --created_by
         SYSDATE, --created_date
         NULL, --modified_by
         NULL, --modified_date
         NULL, --attribute1
         NULL, --attribute2
         NULL, --attribute3
         NULL, --attribute4
         NULL, --attribute5
         NULL, --attribute6
         NULL, --attribute7
         NULL, --attribute8
         NULL, --attribute9
         NULL, --attribute10
         v_period_number --period_number
         );

      --end
      v_from_dt := ADD_MONTHS(v_from_dt, 1);
    END LOOP;

    IF v_adj_period > 0 THEN
      FOR l_count IN 1 .. v_adj_period LOOP
        v_adj_period_name := 'Adj ' ||
                             TO_CHAR(ADD_MONTHS(v_from_dt, -1), 'MON-YYYY') || '_' ||
                             TO_CHAR(l_count);
        DBMS_OUTPUT.put_line('Adjustment period ' || v_adj_period_name);
        v_period_number := v_period_number + 1;

        --start
        INSERT INTO gl_acct_period_dtl
          (pk_id,
           child_id,
           period_id,
           period_name,
           period_type,
           period_from_dt,
           period_to_dt,
           period_status,
           period_first_close_dt,
           cal_dtl_id,
           enabled_flag,
           workflow_completion_status,
           created_by,
           created_date,
           modified_by,
           modified_date,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           period_number)
        VALUES
          (1, --pk_id
           1, --child_id
           ssm.get_next_sequence('GLPRD', 'EN'), --GL_ACCT_PERIOD_DTL_SEQ.NEXTVAL, --period_id
           --'ADJ PERIOD-' ||
           TO_CHAR(LAST_DAY(ADD_MONTHS(v_from_dt, -1)), 'MON-YYYY'), --period_name
           'ADJPERIOD', --period_type
           LAST_DAY(ADD_MONTHS(v_from_dt, -1)), --period_from_dt
           LAST_DAY(ADD_MONTHS(v_from_dt, -1)), --period_to_dt
           'NOP', --period_status
           NULL, --period_first_close_dt
           p_cal_dtl_id, --cal_dtl_id
           '1', --enabled_flag
           '1', --workflow_completion_status
           'Admin', --created_by
           SYSDATE, --created_date
           NULL, --modified_by
           NULL, --modified_date
           NULL, --attribute1
           NULL, --attribute2
           NULL, --attribute3
           NULL, --attribute4
           NULL, --attribute5
           NULL, --attribute6
           NULL, --attribute7
           NULL, --attribute8
           NULL, --attribute9
           NULL, --attribute10
           v_period_number);
        --end

      END LOOP;
    END IF;

    COMMIT;
  END generate_accounting_period;

  PROCEDURE gl_exchange_rates(p_currency_id IN VARCHAR2,
                              p_from_dt     DATE,
                              p_to_dt       DATE,
                              p_std_rate    NUMBER,
                              p_comp_id     VARCHAR2) IS
    v_from_dt DATE;
  BEGIN
    DELETE FROM SSM_CURRENCY_EXCHANGE_RATES SCER
     WHERE SCER.CURRENCY_ID = p_currency_id --'CURR_ID-0000000023'
       AND SCER.CURRENCY_RATE_DT BETWEEN p_from_dt AND p_to_dt;

    --AND SCER.ORG_ID =

    COMMIT;

    v_from_dt := p_from_dt;

    IF currency_validation(p_currency_id) AND company_validation(p_comp_id) THEN
      WHILE v_from_dt <= p_to_dt LOOP
        INSERT INTO ssm_currency_exchange_rates
          (pk_id,
           child_id,
           currency_rate_id,
           currency_rate_dt,
           currency_id,
           currency_std_rate,
           currency_sel_rate,
           currency_buy_rate,
           comp_id,
           enabled_flag,
           workflow_completion_status,
           created_by,
           created_date,
           modified_by,
           modified_date,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10)
        VALUES
          (1, --pk_id
           1, --child_id
           ssm.get_next_sequence('GL_0002', 'EN'), --currency_rate_id
           v_from_dt, --currency_rate_dt
           p_currency_id, --currency_id
           p_std_rate, --currency_std_rate
           p_std_rate, --currency_sel_rate
           p_std_rate, --currency_buy_rate
           p_comp_id, --comp_id
           '1', --enabled_flag
           '1', --workflow_completion_status
           'Admin', --created_by
           SYSDATE, --created_date
           NULL, --modified_by
           NULL, --modified_date
           NULL, --attribute1
           NULL, --attribute2
           NULL, --attribute3
           NULL, --attribute4
           NULL, --attribute5
           NULL, --attribute6
           NULL, --attribute7
           NULL, --attribute8
           NULL, --attribute9
           NULL --attribute10
           );

        v_from_dt := v_from_dt + 1;
      END LOOP;
    END IF;

    COMMIT;
  END gl_exchange_rates;

  FUNCTION get_acct_group_desc(p_acct_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_desc VARCHAR2(240);
  BEGIN
    SELECT ACCT_GRP_DESC
      INTO v_desc
      FROM GL_ACCT_GROUPS
     WHERE ACCT_GRP_ID = p_acct_grp_id
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';

    RETURN v_desc;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN ' ';
  END get_acct_group_desc;

  FUNCTION get_acct_period_name(p_comp_id IN VARCHAR2,
                                p_cal_id  IN VARCHAR2,
                                p_date    IN DATE) RETURN VARCHAR2 IS
    v_period_name VARCHAR2(500);
  BEGIN
    SELECT gp.period_name
      INTO v_period_name
      FROM GL_ACCT_CALENDAR_HDR chdr,
           gl_acct_calendar_dtl chdtl,
           gl_acct_period_dtl   gp,
           gl_companies_dtl     glchdtl,
           gl_companies_hdr     glchdr
     WHERE chdr.cal_id = chdtl.cal_id
       AND gp.cal_dtl_id = chdtl.cal_dtl_id
       AND glchdr.comp_id = glchdtl.comp_id
       AND chdr.cal_id = glchdtl.comp_cal_id
       AND chdr.enabled_flag = '1'
       AND chdr.workflow_completion_status = '1'
       AND chdtl.enabled_flag = '1'
       AND chdtl.workflow_completion_status = '1'
       AND gp.period_status = 'OPEN'
       AND gp.enabled_flag = '1'
       AND gp.workflow_completion_status = '1'
       AND glchdtl.enabled_flag = '1'
       AND glchdtl.workflow_completion_status = '1'
       AND glchdtl.effective_end_dt IS NULL
       AND glchdr.enabled_flag = '1'
       AND glchdr.workflow_completion_status = '1'
       AND chdr.cal_id = p_cal_id --'CAL-0000000158'
       AND glchdr.comp_id = p_comp_id --'ORG-0000000066'
       AND gp.period_from_dt >= (LAST_DAY(ADD_MONTHS(p_date, -1)) + 1)
       AND gp.period_to_dt <= LAST_DAY(p_date);

    --and gp.period_from_dt>=(last_day(add_months('01-MAY-14',-1))+1) and gp.period_to_dt<=last_day('01-MAY-14')
    RETURN v_period_name;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN ' ';
  END get_acct_period_name;

  PROCEDURE GL_Trial_Balance_account_group(p_org_id            IN VARCHAR2,
                                           p_global_segment_id IN VARCHAR2,
                                           p_date              IN DATE) IS
    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_prev_month_amt_dr     NUMBER;
    v_prev_month_amt_cr     NUMBER;
    v_prev_month_ac_amt_dr  NUMBER;
    v_prev_month_ac_amt_cr  NUMBER;
  BEGIN
    DELETE gl_trial_balance;

    INSERT INTO gl_trial_balance
      (SELECT gag.acct_grp_desc,
              sc.currency_code,
              SUM(gcb.closing_balance_acct_dr),
              SUM(gcb.closing_balance_acct_cr)
         FROM gl_acct_period_dtl   gapd,
              gl_acct_calendar_dtl gacd,
              gl_acct_calendar_hdr gach,
              gl_companies_dtl     gcd,
              gl_companies_hdr     gch,
              gl_current_balances  gcb,
              gl_acct_groups       gag,
              gl_acct_codes        gac,
              ssm_currencies       sc
        WHERE gapd.cal_dtl_id = gacd.cal_dtl_id
          AND gacd.cal_id = gach.cal_id
          AND gach.cal_id = gcd.comp_cal_id
          AND gcd.comp_id = gch.comp_id
          AND gcb.je_comp_id = gch.comp_id
          AND gcb.period_name = gapd.period_name
          AND gac.acct_code = gcb.acct_code
          AND gac.acct_grp_id = gag.acct_grp_id
          AND gcb.period_name = gapd.period_name
          AND sc.currency_id = gcd.comp_base_currency
          AND gch.comp_id = p_org_id
          AND gcb.global_segment_id = p_global_segment_id
          AND TRUNC(gapd.period_from_dt) <= ADD_MONTHS(TRUNC(p_date), -1)
          AND TRUNC(gapd.period_to_dt) >= ADD_MONTHS(TRUNC(p_date), -1)
          AND gapd.workflow_completion_status = '1'
          AND gapd.enabled_flag = '1'
          AND gacd.workflow_completion_status = '1'
          AND gacd.enabled_flag = '1'
          AND gach.workflow_completion_status = '1'
          AND gach.enabled_flag = '1'
          AND gcd.workflow_completion_status = '1'
          AND gcd.enabled_flag = '1'
          AND gch.workflow_completion_status = '1'
          AND gch.enabled_flag = '1'
        GROUP BY gag.acct_grp_desc, sc.currency_code
       UNION ALL
       SELECT gag.acct_grp_desc,
              sc.currency_code,
              SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
              SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
         FROM Gl_Journal_Hdr    gjhb,
              Gl_Journal_Dtl    gjdb,
              Gl_Acct_Codes     gacb,
              gl_segment_values gsv,
              gl_companies_hdr  gch,
              gl_acct_groups    gag,
              gl_companies_dtl  gcd,
              ssm_currencies    sc
        WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
          AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
          AND gjhb.je_global_segment_id = gsv.segment_value_id
          AND gch.comp_id = gjhb.Je_Comp_Id
          AND gacb.acct_grp_id = gag.acct_grp_id
          AND sc.currency_id = gcd.comp_base_currency
          AND gch.comp_id = gcd.comp_id
          AND gch.comp_id = p_org_id
          AND gjhb.je_global_segment_id = p_global_segment_id
          AND TRUNC(gjhb.je_date) <= TRUNC(p_date)
          AND TRUNC(gjhb.je_date) >= TRUNC(p_date, 'MM')
          AND gjhb.workflow_completion_status = '1'
          AND gjhb.enabled_flag = '1'
          AND gjdb.workflow_completion_status = '1'
          AND gjdb.enabled_flag = '1'
          AND gacb.workflow_completion_status = '1'
          AND gacb.enabled_flag = '1'
          AND gsv.workflow_completion_status = '1'
          AND gsv.enabled_flag = '1'
          AND gch.workflow_completion_status = '1'
          AND gch.enabled_flag = '1'
          AND gag.workflow_completion_status = '1'
          AND gag.enabled_flag = '1'
        GROUP BY gag.acct_grp_desc, sc.currency_code);

    COMMIT;
  END GL_Trial_Balance_account_group;

  PROCEDURE GL_Trial_Balance_detail(p_org_id            IN VARCHAR2,
                                    p_global_segment_id IN VARCHAR2,
                                    p_date              IN DATE) IS
    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_prev_month_amt_dr     NUMBER;
    v_prev_month_amt_cr     NUMBER;
    v_prev_month_ac_amt_dr  NUMBER;
    v_prev_month_ac_amt_cr  NUMBER;
  BEGIN
    DELETE gl_trial_balance_detail;

    INSERT INTO gl_trial_balance_detail
      (SELECT gac.acct_code,
              gac.acct_code_desc,
              sc.currency_code,
              SUM(gcb.Begin_Balance_dr) closing_balance_acct_dr,
              SUM(gcb.Begin_Balance_Cr) closing_balance_acct_cr
         FROM gl_acct_period_dtl   gapd,
              gl_acct_calendar_dtl gacd,
              gl_acct_calendar_hdr gach,
              gl_companies_dtl     gcd,
              gl_companies_hdr     gch,
              gl_current_balances  gcb,
              gl_acct_groups       gag,
              gl_acct_codes        gac,
              ssm_currencies       sc
        WHERE gapd.cal_dtl_id = gacd.cal_dtl_id
          AND gacd.cal_id = gach.cal_id
          AND gach.cal_id = gcd.comp_cal_id
          AND gcd.comp_id = gch.comp_id
          AND gcb.je_comp_id = gch.comp_id
          AND gcb.period_name = gapd.period_name
          AND gac.acct_code = gcb.acct_code
          AND gac.acct_grp_id = gag.acct_grp_id
          AND gcb.period_name = gapd.period_name
          AND sc.currency_id = gcd.comp_base_currency
          AND gch.comp_id = p_org_id
          AND gcb.global_segment_id = p_global_segment_id
          AND TRUNC(gapd.period_from_dt) <= TRUNC(p_date)
          AND TRUNC(gapd.period_to_dt) >= TRUNC(p_date)
          AND gapd.workflow_completion_status = '1'
          AND gapd.enabled_flag = '1'
          AND gacd.workflow_completion_status = '1'
          AND gacd.enabled_flag = '1'
          AND gach.workflow_completion_status = '1'
          AND gach.enabled_flag = '1'
          AND gcd.workflow_completion_status = '1'
          AND gcd.enabled_flag = '1'
          AND gch.workflow_completion_status = '1'
          AND gch.enabled_flag = '1'
        GROUP BY gac.acct_code, gac.acct_code_desc, sc.currency_code
       UNION ALL
       SELECT gacb.acct_code,
              gacb.acct_code_desc,
              sc.currency_code,
              SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
              SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
         FROM Gl_Journal_Hdr    gjhb,
              Gl_Journal_Dtl    gjdb,
              Gl_Acct_Codes     gacb,
              gl_segment_values gsv,
              gl_companies_hdr  gch,
              gl_acct_groups    gag,
              gl_companies_dtl  gcd,
              ssm_currencies    sc
        WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
          AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
          AND gjhb.je_global_segment_id = gsv.segment_value_id
          AND gch.comp_id = gjhb.Je_Comp_Id
          AND gacb.acct_grp_id = gag.acct_grp_id
          AND sc.currency_id = gcd.comp_base_currency
          AND gch.comp_id = gcd.comp_id
          AND gch.comp_id = p_org_id
          AND gjhb.je_global_segment_id = p_global_segment_id
          AND TRUNC(gjhb.je_date) <= TRUNC(p_date)
          AND TRUNC(gjhb.je_date) >= TRUNC(p_date, 'MM')
          AND gjhb.workflow_completion_status = '1'
          AND gjhb.enabled_flag = '1'
          AND gjdb.workflow_completion_status = '1'
          AND gjdb.enabled_flag = '1'
          AND gacb.workflow_completion_status = '1'
          AND gacb.enabled_flag = '1'
          AND gsv.workflow_completion_status = '1'
          AND gsv.enabled_flag = '1'
          AND gch.workflow_completion_status = '1'
          AND gch.enabled_flag = '1'
          AND gag.workflow_completion_status = '1'
          AND gag.enabled_flag = '1'
        GROUP BY gacb.acct_code, gacb.acct_code_desc, sc.currency_code);

    COMMIT;
  END GL_Trial_Balance_detail;

  PROCEDURE GL_Account_Group_Monthly_Sum(p_org_id            IN VARCHAR2,
                                         p_global_segment_id IN VARCHAR2,
                                         p_from_date         IN DATE,
                                         p_to_date           IN DATE) IS
    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_open_balance          NUMBER;
    v_close_dr              NUMBER;
    v_close_cr              NUMBER;
    v_close_bal             NUMBER;
  BEGIN
    DELETE gl_account_group_monthly_sum;

    FOR acct_summary IN (SELECT balance.acct_grp_desc,
                                balance.period_name     bal_period_name,
                                balance.period_number   bal_period_num,
                                cur_trans.period_name   cur_period_name,
                                cur_trans.period_number cur_period_num,
                                balance.bal_dr,
                                balance.bal_cr,
                                cur_trans.accounted_dr,
                                cur_trans.accounted_cr
                           FROM (SELECT gag.acct_grp_desc,
                                        gag.acct_grp_id,
                                        sc.currency_code,
                                        gcb.period_name,
                                        gcb.period_number,
                                        SUM(gcb.closing_balance_acct_dr) bal_dr,
                                        SUM(gcb.closing_balance_acct_cr) bal_cr
                                   FROM gl_acct_period_dtl   gapd,
                                        gl_acct_calendar_dtl gacd,
                                        gl_acct_calendar_hdr gach,
                                        gl_companies_dtl     gcd,
                                        gl_companies_hdr     gch,
                                        gl_current_balances  gcb,
                                        gl_acct_groups       gag,
                                        gl_acct_codes        gac,
                                        ssm_currencies       sc
                                  WHERE gapd.cal_dtl_id = gacd.cal_dtl_id
                                    AND gacd.cal_id = gach.cal_id
                                    AND gach.cal_id = gcd.comp_cal_id
                                    AND gcd.comp_id = gch.comp_id
                                    AND gcb.je_comp_id = gch.comp_id
                                    AND gcb.period_name = gapd.period_name
                                    AND gac.acct_code = gcb.acct_code
                                    AND gac.acct_grp_id = gag.acct_grp_id
                                    AND gcb.period_name = gapd.period_name
                                    AND sc.currency_id =
                                        gcd.comp_base_currency
                                    AND gch.comp_id = p_org_id
                                    AND gcb.global_segment_id =
                                        p_global_segment_id
                                    AND TRUNC(gapd.period_from_dt) <=
                                        ADD_MONTHS(TRUNC(p_from_date), -1)
                                    AND TRUNC(gapd.period_to_dt) >=
                                        ADD_MONTHS(TRUNC(p_from_date), -1)
                                    AND gapd.workflow_completion_status = '1'
                                    AND gapd.enabled_flag = '1'
                                    AND gacd.workflow_completion_status = '1'
                                    AND gacd.enabled_flag = '1'
                                    AND gach.workflow_completion_status = '1'
                                    AND gach.enabled_flag = '1'
                                    AND gcd.workflow_completion_status = '1'
                                    AND gcd.enabled_flag = '1'
                                    AND gch.workflow_completion_status = '1'
                                    AND gch.enabled_flag = '1'
                                  GROUP BY gag.acct_grp_desc,
                                           sc.currency_code,
                                           gcb.period_name,
                                           gcb.period_number) balance,
                                (SELECT gag.acct_grp_desc,
                                        gag.acct_grp_id,
                                        sc.currency_code,
                                        gapd.period_name,
                                        gapd.period_number,
                                        SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                                        SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
                                   FROM Gl_Journal_Hdr       gjhb,
                                        Gl_Journal_Dtl       gjdb,
                                        Gl_Acct_Codes        gacb,
                                        gl_segment_values    gsv,
                                        gl_companies_hdr     gch,
                                        gl_acct_groups       gag,
                                        gl_companies_dtl     gcd,
                                        ssm_currencies       sc,
                                        gl_acct_calendar_dtl gacd,
                                        gl_acct_calendar_hdr gach,
                                        Gl_Acct_Period_Dtl   Gapd
                                  WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
                                    AND gjdb.Je_Acct_Code_Id =
                                        gacb.Acct_Code_Id
                                    AND gjhb.je_global_segment_id =
                                        gsv.segment_value_id
                                    AND gch.comp_id = gjhb.Je_Comp_Id
                                    AND gacb.acct_grp_id = gag.acct_grp_id
                                    AND sc.currency_id =
                                        gcd.comp_base_currency
                                    AND gjhb.Je_Period_Id = Gapd.Period_Id
                                    AND gch.comp_id = gcd.comp_id
                                    AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
                                    AND gacd.cal_id = gach.cal_id
                                    AND gach.cal_id = gcd.comp_cal_id
                                    AND gch.comp_id = p_org_id
                                    AND gjhb.je_global_segment_id =
                                        p_global_segment_id
                                    AND TRUNC(gjhb.je_date) <=
                                        TRUNC(p_from_date)
                                    AND TRUNC(gjhb.je_date) >=
                                        TRUNC(p_from_date, 'MM')
                                    AND gjhb.workflow_completion_status = '1'
                                    AND gjhb.enabled_flag = '1'
                                    AND gjdb.workflow_completion_status = '1'
                                    AND gjdb.enabled_flag = '1'
                                    AND gacb.workflow_completion_status = '1'
                                    AND gacb.enabled_flag = '1'
                                    AND gsv.workflow_completion_status = '1'
                                    AND gsv.enabled_flag = '1'
                                    AND gch.workflow_completion_status = '1'
                                    AND gch.enabled_flag = '1'
                                    AND gag.workflow_completion_status = '1'
                                    AND gag.enabled_flag = '1'
                                  GROUP BY gag.acct_grp_desc,
                                           sc.currency_code,
                                           gapd.period_name,
                                           gapd.period_number) cur_trans
                          WHERE balance.acct_grp_id = cur_trans.acct_grp_id) LOOP
      v_open_balance := NVL(acct_summary.bal_dr, 0) -
                        NVL(acct_summary.bal_cr, 0);

      IF v_open_balance < 0 THEN
        v_close_dr := acct_summary.accounted_dr;
        v_close_cr := acct_summary.accounted_cr + (v_open_balance * -1);
      ELSE
        v_close_dr := acct_summary.accounted_dr + v_open_balance;
        v_close_cr := acct_summary.accounted_cr;
      END IF;

      v_close_bal := v_close_dr - v_close_cr;

      INSERT INTO gl_account_group_monthly_sum
      VALUES
        (acct_summary.acct_grp_desc,
         acct_summary.cur_period_name,
         acct_summary.cur_period_num,
         v_open_balance,
         acct_summary.accounted_dr,
         acct_summary.accounted_cr,
         v_close_dr,
         v_close_cr,
         v_close_bal,
         acct_summary.bal_period_name,
         acct_summary.bal_period_num);
    END LOOP;

    COMMIT;
  END GL_Account_Group_monthly_Sum;

  PROCEDURE GL_Account_Group_Summary(p_org_id            IN VARCHAR2,
                                     p_global_segment_id IN VARCHAR2,
                                     p_from_date         IN DATE,
                                     p_to_date           IN DATE,
                                     p_group_id          IN VARCHAR2,
                                     p_unposted          IN VARCHAR2) IS

    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_open_balance          NUMBER;
    v_close_dr              NUMBER;
    v_close_cr              NUMBER;
    v_close_bal             NUMBER;
    v_group_id              varchar2(50);
    v_Accounted_dr          NUMBER;
    v_Accounted_cr          NUMBER;
  BEGIN
    DELETE gl_account_group_summary;
    if upper(p_group_id) = 'ALL' then
      v_group_id := null;
    else
      v_group_id := p_group_id;
    end if;

    FOR acct_summary IN (select gjsp.group_name,
                                group_id,
                                currency_id,
                                gjsp.period_id,
                                global_segment_id,
                                sum(bb_accounted_dr) bb_accounted_dr,
                                sum(bb_accounted_cr) bb_accounted_cr,
                                gjsp.period_name,
                                gjsp.period_number
                           from gl_je_summary_period    gjsp,
                                gl_comp_acct_period_dtl gcapd
                          where gjsp.period_id = gcapd.period_id
                            and gcapd.comp_id = gjsp.comp_id
                            and gcapd.comp_id = p_org_id
                            and gjsp.group_id =
                                nvl(v_group_id, gjsp.group_id)
                            and gcapd.period_to_dt >= p_from_date
                            and gcapd.period_to_dt <= p_to_date
                          group by gjsp.group_name,
                                   group_id,
                                   currency_id,
                                   gjsp.period_id,
                                   global_segment_id,
                                   gjsp.period_name,
                                   gjsp.period_number) loop

      v_Accounted_dr := 0;
      v_Accounted_cr := 0;
      if p_unposted = '1' then

        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1'
           GROUP BY gag.acct_grp_desc,
                    gag.acct_grp_id,
                    sc.currency_code,
                    gapd.period_name,
                    gapd.period_number,
                    gacb.acct_code,
                    gacb.acct_code_desc;
        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
      else
        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             AND gjhb.workflow_completion_status = '1'
             AND gjhb.enabled_flag = '1'
             AND gjdb.workflow_completion_status = '1'
             AND gjdb.enabled_flag = '1'
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1'
           GROUP BY gag.acct_grp_desc,
                    gag.acct_grp_id,
                    sc.currency_code,
                    gapd.period_name,
                    gapd.period_number,
                    gacb.acct_code,
                    gacb.acct_code_desc;

        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
      end if;
      v_open_balance := 0;
      v_close_dr     := 0;
      v_close_cr     := 0;
      v_close_bal    := 0;

      v_open_balance := NVL(acct_summary.bb_accounted_dr, 0) -
                        NVL(acct_summary.bb_accounted_cr, 0);

      IF v_open_balance < 0 THEN
        v_close_dr := v_accounted_dr;
        v_close_cr := v_accounted_cr + (v_open_balance * -1);
      ELSE
        v_close_dr := v_accounted_dr + v_open_balance;
        v_close_cr := v_accounted_cr;
      END IF;

      v_close_bal := v_close_dr - v_close_cr;

      INSERT INTO gl_account_group_summary
        (ACCOUNT_GROUP_DESCRIPTION,
         OPENING_BALANCE,
         TRANSACTION_DEBIT,
         TRANSACTION_CREDIT,
         CLOSING_BALANCE_DEBIT,
         CLOSING_BALANCE_CREDIT,
         CLOSING_BALANCE,
         ACCOUNT_GROUP_ID)
      VALUES
        (acct_summary.group_name,
         v_open_balance,
         v_accounted_dr,
         v_accounted_cr,
         v_close_dr,
         v_close_cr,
         v_close_bal,
         acct_summary.group_id);

    END LOOP;

    COMMIT;
  END gl_account_group_summary;

  PROCEDURE GL_Account_Monthly_Summary(p_org_id            IN VARCHAR2,
                                       p_global_segment_id IN VARCHAR2,
                                       p_from_date         IN DATE,
                                       p_to_date           IN DATE,
                                       p_acct_code         IN VARCHAR2,
                                       p_unposted          IN VARCHAR2) IS

    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_open_balance          NUMBER;
    v_close_dr              NUMBER;
    v_close_cr              NUMBER;
    v_close_bal             NUMBER;
    v_acct_code_id          varchar2(50);
    v_Accounted_dr          NUMBER;
    v_Accounted_cr          NUMBER;
    v_last_month            date;
    v_PREV_UN_Accounted_dr  NUMBER;
    v_PREV_UN_Accounted_cr  NUMBER;
    v_Period_number number;
    v_tot_Accounted_cr NUMBER;
    v_tot_Accounted_dr NUMBER;
    v_period_count NUMBER;
  BEGIN

    DELETE gl_account_monthly_summary;
    commit;

    if upper(p_acct_code) = 'ALL' then
      v_acct_code_id := null;
    else
      v_acct_code_id := p_acct_code;
    end if;

    IF TO_CHAR(p_from_date,'MM-RRRR')=TO_CHAR(p_to_date,'MM-RRRR') then

  FOR acct_period IN (select * from
                                gl_comp_acct_period_dtl gcapd
                                where gcapd.comp_id = p_org_id
                            and gcapd.period_to_dt >= p_from_date
                            and gcapd.period_to_dt <= p_to_date
                            and gcapd.period_status !='NOP'
                            order by gcapd.period_from_dt, gcapd.period_number
                            ) LOOP

    FOR acct_summary IN (select gjsp.*,to_number(gag.attribute10) group_number
                           from gl_je_summary_period    gjsp,gl_acct_groups gag
                          where gjsp.period_id = acct_period.period_id
                            and acct_period.comp_id = gjsp.comp_id
                            and gjsp.acct_code_id =
                                nvl(v_acct_code_id, gjsp.acct_code_id)
                                and gag.acct_grp_id=gjsp.group_id
                            ) loop

      v_Accounted_dr := 0;
      v_Accounted_cr := 0;
      v_PREV_UN_Accounted_dr:=0;
      v_PREV_UN_Accounted_cr:=0;
      if p_unposted = '1' then

      select (gapd.period_from_dt) into v_last_month from gl_acct_period_dtl gapd where gapd.period_id=acct_summary.period_id
      and gapd.cal_dtl_id=acct_summary.cal_dtl_id;

              begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_PREV_UN_Accounted_dr, v_PREV_UN_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_date < v_last_month
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             and gjhb.workflow_completion_status='0'
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';
        exception
          when others then
            v_PREV_UN_Accounted_cr := 0;
            v_PREV_UN_Accounted_dr := 0;
        end;

        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';
        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
        commit;

      else
        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             AND gjhb.workflow_completion_status = '1'
             AND gjhb.enabled_flag = '1'
             AND gjdb.workflow_completion_status = '1'
             AND gjdb.enabled_flag = '1'
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';

        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
      end if;

      v_open_balance := 0;
      v_close_dr     := 0;
      v_close_cr     := 0;
      v_close_bal    := 0;

      v_open_balance := NVL(acct_summary.bb_accounted_dr, 0) -
                        NVL(acct_summary.bb_accounted_cr, 0)+
                        nvl(v_PREV_UN_Accounted_dr,0)-nvl(v_PREV_UN_Accounted_cr,0);

      IF v_open_balance < 0 THEN
        v_close_dr := nvl(v_accounted_dr,0);
        v_close_cr := nvl(v_accounted_cr,0) + (nvl(v_open_balance,0) * -1);
      ELSE
        v_close_dr := nvl(v_accounted_dr,0) + nvl(v_open_balance,0);
        v_close_cr := nvl(v_accounted_cr,0);
      END IF;

      v_close_bal := v_close_dr - v_close_cr;

      if nvl(v_open_balance, 0) = 0 and nvl(v_Accounted_cr, 0) = 0 and
         nvl(v_Accounted_dr, 0) = 0 and nvl(v_close_bal, 0) = 0 then
        null;
      else

        INSERT INTO gl_account_monthly_summary
        VALUES
          (acct_summary.group_name,
           acct_summary.period_name,
           acct_summary.period_number,
           acct_summary.acct_code,
           acct_summary.acct_code_desc,
           nvl(v_open_balance,0),
           nvl(v_accounted_dr,0),
           nvl(v_accounted_cr,0),
           nvl(v_close_dr,0),
           nvl(v_close_cr,0),
           nvl(v_close_bal,0),
           acct_summary.period_name,
           acct_summary.period_number,
           acct_summary.currency_code,
           acct_summary.group_number);
           end if;
    END LOOP;
end loop;

else

select count('x') into v_period_count from
                                gl_comp_acct_period_dtl gcapd
                                where gcapd.comp_id = p_org_id
                            and gcapd.period_to_dt >= p_from_date
                            and gcapd.period_to_dt <= p_to_date
                            and gcapd.period_status!='NOP';

  FOR acct IN (select * from gl_acct_codes gac where gac.acct_code_id=
    nvl(v_acct_code_id, gac.acct_code_id) )LOOP

    v_Period_number :=1;
    v_tot_Accounted_cr:=0;
    v_tot_Accounted_dr:=0;
  FOR acct_period IN (select * from
                                gl_comp_acct_period_dtl gcapd
                                where gcapd.comp_id = p_org_id
                            and gcapd.period_to_dt >= p_from_date
                            and gcapd.period_to_dt <= p_to_date
                            and gcapd.period_status!='NOP'
                            order by gcapd.period_from_dt,gcapd.period_number) LOOP

      if v_Period_number = 1 then

      v_open_balance := 0;
end if;
    FOR acct_summary IN (select gjsp.*,to_number(gag.attribute10)group_number
                           from gl_je_summary_period    gjsp,gl_acct_groups gag
                          where gjsp.period_id = acct_period.period_id
                            and acct_period.comp_id = gjsp.comp_id
                            and gjsp.acct_code_id = acct.acct_code_id
                            and gag.acct_grp_id=gjsp.group_id

                            ) loop

      v_Accounted_dr := 0;
      v_Accounted_cr := 0;
      v_PREV_UN_Accounted_dr:=0;
      v_PREV_UN_Accounted_cr:=0;


      if p_unposted = '1' then

      if v_Period_number = 1 then

      v_open_balance := 0;

      select (gapd.period_from_dt) into v_last_month from gl_acct_period_dtl gapd where gapd.period_id=acct_summary.period_id
      and gapd.cal_dtl_id=acct_summary.cal_dtl_id;

              begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_PREV_UN_Accounted_dr, v_PREV_UN_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_date < v_last_month
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             and gjhb.workflow_completion_status='0'
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';

      v_open_balance := NVL(acct_summary.bb_accounted_dr, 0) -
                        NVL(acct_summary.bb_accounted_cr, 0)+NVL(v_PREV_UN_Accounted_dr,0)-
                        NVL(v_PREV_UN_Accounted_cr,0);

        exception
          when others then
            v_PREV_UN_Accounted_cr := 0;
            v_PREV_UN_Accounted_dr := 0;
      v_open_balance := NVL(acct_summary.bb_accounted_dr, 0) -
                        NVL(acct_summary.bb_accounted_cr, 0)+NVL(v_PREV_UN_Accounted_dr,0)-
                        NVL(v_PREV_UN_Accounted_cr,0);
        end;

        end if;

        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';
        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
        commit;

      else
        begin
          SELECT SUM(NVL(Gjdb.Je_Accounted_Amt_dr, 0)) Accounted_dr,
                 SUM(NVL(Gjdb.Je_Accounted_Amt_cr, 0)) Accounted_cr
            INTO v_Accounted_dr, v_Accounted_cr
            FROM Gl_Journal_Hdr       gjhb,
                 Gl_Journal_Dtl       gjdb,
                 Gl_Acct_Codes        gacb,
                 gl_segment_values    gsv,
                 gl_companies_hdr     gch,
                 gl_acct_groups       gag,
                 gl_companies_dtl     gcd,
                 ssm_currencies       sc,
                 gl_acct_calendar_dtl gacd,
                 gl_acct_calendar_hdr gach,
                 Gl_Acct_Period_Dtl   Gapd
           WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
             AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
             AND gjhb.je_global_segment_id = gsv.segment_value_id
             AND gch.comp_id = gjhb.Je_Comp_Id
             AND gacb.acct_grp_id = gag.acct_grp_id
             AND sc.currency_id = gcd.comp_base_currency
             AND gjhb.Je_Period_Id = Gapd.Period_Id
             AND gch.comp_id = gcd.comp_id
             AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
             AND gacd.cal_id = gach.cal_id
             AND gach.cal_id = gcd.comp_cal_id
             AND gch.comp_id = p_org_id
             AND gjhb.je_global_segment_id = acct_summary.global_segment_id
             AND gjhb.je_period_id = acct_summary.period_id
             AND gjhb.je_currency_id = acct_summary.currency_id
             and gag.acct_grp_id = acct_summary.group_id
             and gjdb.je_acct_code_id = acct_summary.acct_code_id
             AND gjhb.workflow_completion_status = '1'
             AND gjhb.enabled_flag = '1'
             AND gjdb.workflow_completion_status = '1'
             AND gjdb.enabled_flag = '1'
             AND gacb.workflow_completion_status = '1'
             AND gacb.enabled_flag = '1'
             AND gsv.workflow_completion_status = '1'
             AND gsv.enabled_flag = '1'
             AND gch.workflow_completion_status = '1'
             AND gch.enabled_flag = '1'
             AND gag.workflow_completion_status = '1'
             AND gag.enabled_flag = '1';

        exception
          when others then
            v_Accounted_cr := 0;
            v_Accounted_dr := 0;
        end;
      end if;

v_tot_Accounted_cr  := nvl(v_tot_Accounted_cr,0)   +nvl(v_Accounted_cr,0);
v_tot_Accounted_dr  := nvl(v_tot_Accounted_dr,0)   +nvl(v_Accounted_dr,0);


if v_Period_number = v_period_count then

      IF v_open_balance < 0 THEN
        v_close_dr := nvl(v_tot_accounted_dr,0);
        v_close_cr := nvl(v_tot_accounted_cr,0) + (nvl(v_open_balance,0) * -1);
      ELSE
        v_close_dr := nvl(v_tot_accounted_dr,0) + nvl(v_open_balance,0);
        v_close_cr := nvl(v_tot_accounted_cr,0);
      END IF;

      v_close_bal := nvl(v_close_dr,0) - nvl(v_close_cr,0);

      if nvl(v_open_balance, 0) = 0 and nvl(v_tot_Accounted_cr, 0) = 0 and
         nvl(v_tot_Accounted_dr, 0) = 0 and nvl(v_close_bal, 0) = 0  then
        null;
      else

        INSERT INTO gl_account_monthly_summary
        VALUES
          (acct_summary.group_name,
           acct_summary.period_name,
           acct_summary.period_number,
           acct_summary.acct_code,
           acct_summary.acct_code_desc,
           nvl(v_open_balance,0),
           nvl(v_tot_accounted_dr,0),
           nvl(v_tot_accounted_cr,0),
           nvl(v_close_dr,0),
           nvl(v_close_cr,0),
           nvl(v_close_bal,0),
           acct_summary.period_name,
           acct_summary.period_number,
           acct_summary.currency_code,
           acct_summary.group_number);
           end if;
           end if;

    END LOOP;
    v_period_number:=v_Period_number+1;
end loop;

end loop;
end if;

    COMMIT;

  END GL_Account_Monthly_Summary;

  FUNCTION Fn_Get_Exchange_Rate(P_COMP_ID     VARCHAR2,
                                P_CURRENCY_ID VARCHAR2,
                                P_DATE        DATE,
                                P_TYPE        VARCHAR2) RETURN NUMBER IS
    v_rate number(10, 2);

  BEGIN

    IF P_TYPE = 'BUYING' THEN

      SELECT SCER.CURRENCY_BUY_RATE
        INTO v_rate
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.COMP_ID = P_COMP_ID
         AND SCER.CURRENCY_ID = P_CURRENCY_ID
         AND SCER.CURRENCY_RATE_DT = P_DATE
         AND SCER.ENABLED_FLAG = '1';

    ELSIF P_TYPE = 'SELLING' THEN
      SELECT SCER.CURRENCY_SEL_RATE
        INTO v_rate
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.COMP_ID = P_COMP_ID
         AND SCER.CURRENCY_ID = P_CURRENCY_ID
         AND SCER.CURRENCY_RATE_DT = P_DATE
         AND SCER.ENABLED_FLAG = '1';

    ELSIF P_TYPE = 'STANDARD' THEN
      SELECT SCER.CURRENCY_STD_RATE
        INTO v_rate
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.COMP_ID = P_COMP_ID
         AND SCER.CURRENCY_ID = P_CURRENCY_ID
         AND SCER.CURRENCY_RATE_DT = P_DATE
         AND SCER.ENABLED_FLAG = '1';

    END IF;

    RETURN v_rate;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END FN_GET_EXCHANGE_RATE;

  PROCEDURE GL_OPENING_BALANCE(P_CAL_DTL_ID        IN VARCHAR2,
                               P_PERIOD_NAME       IN VARCHAR2,
                               P_PERIOD_STATUS_OLD IN VARCHAR2,
                               P_PERIOD_STATUS_NEW IN VARCHAR2,
                               P_PERIOD_NUMBER     IN NUMBER,
                               P_PERIOD_FROM_DATE  IN DATE,
                               P_COMP_ID           IN VARCHAR2,
                               P_PERIOD_ID         IN VARCHAR2,
                               P_PERIOD_TYPE       IN VARCHAR2) IS
    v_retained_earning_account    varchar2(50);
    v_retained_earning_acct_count number := 0;
    v_acct_count                  number := 0;
    v_cur_cal_acct_year           varchar2(50);
    v_cur_cal_id                  varchar2(50);
    v_group_id                    varchar2(50);
    v_group_name                  varchar2(50);
    v_acct_code_id                varchar2(50);
    v_acct_code_desc              varchar2(50);
    v_acct_type                   varchar2(50);
    v_is_control_acct             varchar2(50);
    v_control_acct_categroy       varchar2(50);
    v_prev_period_number          number;
    v_prev_period_to_date         date;
    v_period_id                   varchar2(50);
    v_cal_dtl_id                  varchar2(50);
    v_cal_acct_year               varchar2(50);
    v_cal_id                      varchar2(50);
  BEGIN

    v_prev_period_to_date := null;
    v_prev_period_number  := null;
    v_period_id           := null;

    IF P_PERIOD_STATUS_OLD = 'NOP' AND P_PERIOD_STATUS_NEW = 'OPEN' AND
       P_PERIOD_NUMBER = 1 then

      select max(gcapd.period_to_dt)
        into v_prev_period_to_date
        from gl_comp_acct_period_dtl_g gcapd
       where gcapd.comp_id = P_COMP_ID
         and gcapd.period_to_dt < P_PERIOD_FROM_DATE;

      if v_prev_period_to_date is not null then

        select max(gcapd.period_number)
          into v_prev_period_number
          from gl_comp_acct_period_dtl_g gcapd
         where gcapd.period_to_dt = v_prev_period_to_date
           and gcapd.period_status in ('OPEN', 'CLOSED', 'AUDITED');

        select period_id, gcapd.cal_dtl_id
          into v_period_id, v_cal_dtl_id
          from gl_comp_acct_period_dtl_g gcapd
         where gcapd.period_number = v_prev_period_number
           and gcapd.period_to_dt = v_prev_period_to_date
           and gcapd.period_status in ('OPEN', 'CLOSED', 'AUDITED');

        select gcacd.cal_acct_year, gcacd.cal_id
          into v_cal_acct_year, v_cal_id
          from gl_comp_acct_calendar_dtl gcacd
         where gcacd.comp_id = P_COMP_ID
           and gcacd.cal_dtl_id = v_cal_dtl_id;

        select gcacd.cal_acct_year, gcacd.cal_id
          into v_cur_cal_acct_year, v_cur_cal_id
          from gl_comp_acct_calendar_dtl gcacd
         where gcacd.comp_id = P_COMP_ID
           and gcacd.cal_dtl_id = P_CAL_DTL_ID;

        for bal_ins in (select gjsp.*
                          from gl_je_summary_period gjsp
                         where gjsp.period_id = v_period_id
                           and gjsp.comp_id = P_COMP_ID
                           and gjsp.acct_type = 'B'
                        /* and not exists
                        (select *
                                 from gl_je_summary_period gjsps
                                where gjsp.comp_id = gjsps.comp_id
                                  and gjsp.period_id = P_PERIOD_ID
                                  and gjsp.global_segment_id =
                                      gjsps.global_segment_id
                                  and gjsp.group_id = gjsps.group_id
                                  and gjsp.acct_code_id = gjsps.acct_code_id)*/
                        ) loop

          v_acct_count := 0;

          select count('x')
            into v_acct_count
            from gl_je_summary_period gjsp
           where gjsp.comp_id = P_COMP_ID
             and gjsp.cal_dtl_id = P_CAL_DTL_ID
             and gjsp.period_id = P_PERIOD_ID
             and gjsp.global_segment_id = bal_ins.global_segment_id
             and gjsp.group_id = bal_ins.group_id
             and gjsp.acct_code_id = bal_ins.acct_code
             and gjsp.currency_id = bal_ins.currency_id;

          if v_acct_count > 0 then
            update gl_je_summary_period gjsp
               set bb_amount_cr    = bal_ins.cb_amount_cr,
                   bb_amount_dr    = bal_ins.cb_amount_dr,
                   bb_accounted_cr = bal_ins.cb_accounted_cr,
                   bb_accounted_dr = bal_ins.cb_accounted_dr,
                   cb_amount_cr    = bal_ins.cb_amount_cr +
                                     nvl(amount_cr, 0),
                   cb_amount_dr    = bal_ins.cb_amount_dr +
                                     nvl(amount_dr, 0),
                   cb_accounted_cr = bal_ins.cb_accounted_cr +
                                     nvl(accounted_cr, 0),
                   cb_accounted_dr = bal_ins.cb_accounted_dr +
                                     nvl(accounted_dr, 0)
             where gjsp.comp_id = P_COMP_ID
               and gjsp.cal_dtl_id = P_CAL_DTL_ID
               and gjsp.period_id = P_PERIOD_ID
               and gjsp.global_segment_id = bal_ins.global_segment_id
               and gjsp.group_id = bal_ins.group_id
               and gjsp.acct_code_id = bal_ins.acct_code_id
               and gjsp.currency_id = bal_ins.currency_id;
          else
            insert into gl_je_summary_period
              (comp_id,
               comp_short_name,
               cal_id,
               cal_acct_year,
               cal_dtl_id,
               period_id,
               period_type,
               period_number,
               period_name,
               global_segment_id,
               global_segment,
               group_id,
               group_name,
               acct_code_id,
               acct_code,
               acct_code_desc,
               acct_type,
               is_control_acct,
               control_acct_categroy,
               currency_id,
               currency_code,
               bb_amount_cr,
               bb_amount_dr,
               bb_accounted_cr,
               bb_accounted_dr,
               amount_cr,
               amount_dr,
               accounted_cr,
               accounted_dr,
               cb_amount_cr,
               cb_amount_dr,
               cb_accounted_cr,
               cb_accounted_dr,
               audited)
            values
              (bal_ins.comp_id,
               bal_ins.comp_short_name,
               v_cur_cal_id,
               v_cur_cal_acct_year,
               P_cal_dtl_id,
               P_PERIOD_ID,
               P_PERIOD_TYPE,
               P_period_number,
               P_period_name,
               bal_ins.global_segment_id,
               bal_ins.global_segment,
               bal_ins.group_id,
               bal_ins.group_name,
               bal_ins.acct_code_id,
               bal_ins.acct_code,
               bal_ins.acct_code_desc,
               bal_ins.acct_type,
               bal_ins.is_control_acct,
               bal_ins.control_acct_categroy,
               bal_ins.currency_id,
               bal_ins.currency_code,
               bal_ins.cb_amount_cr,
               bal_ins.cb_amount_dr,
               bal_ins.cb_accounted_cr,
               bal_ins.cb_accounted_dr,
               0,
               0,
               0,
               0,
               bal_ins.cb_amount_cr,
               bal_ins.cb_amount_dr,
               bal_ins.cb_accounted_cr,
               bal_ins.cb_accounted_dr,
               'N');

          end if;

        end loop;

        v_retained_earning_account := ssm.get_parameter_value(p_param_code => 'GL_RETAINED_EARNINGS');

        select acct_code_id,
               acct_code_desc,
               acct_type,
               is_control_acct,
               control_acct_categroy,
               gac.acct_grp_id,
               gag.acct_grp_desc
          into v_acct_code_id,
               v_acct_code_desc,
               v_acct_type,
               v_is_control_acct,
               v_control_acct_categroy,
               v_group_id,
               v_group_name
          from gl_acct_codes gac, gl_acct_groups gag
         where gac.acct_code = v_retained_earning_account
           and gac.acct_org_id = P_COMP_ID
           and gac.acct_grp_id = gag.acct_grp_id;

        for bal_ins in (select gjsp.comp_id,
                               gjsp.comp_short_name,
                               gjsp.global_segment_id,
                               gjsp.global_segment,
                               v_group_id,
                               v_group_name,
                               v_acct_code_id,
                               v_retained_earning_account,
                               v_acct_code_desc,
                               v_acct_type,
                               v_is_control_acct,
                               v_control_acct_categroy,
                               gjsp.currency_id,
                               gjsp.currency_code,
                               sum(gjsp.cb_amount_cr) sum_cb_amount_cr,
                               sum(gjsp.cb_amount_dr) sum_cb_amount_dr,
                               sum(gjsp.cb_accounted_cr) sum_cb_accounted_cr,
                               sum(gjsp.cb_accounted_dr) sum_cb_accounted_dr
                          from gl_je_summary_period gjsp
                         where gjsp.period_id = v_period_id
                           and gjsp.comp_id = P_COMP_ID
                           and gjsp.acct_type = 'P'
                         group by gjsp.comp_id,
                                  gjsp.comp_short_name,
                                  gjsp.global_segment_id,
                                  gjsp.global_segment,
                                  v_group_id,
                                  v_group_name,
                                  v_acct_code_id,
                                  v_retained_earning_account,
                                  v_acct_code_desc,
                                  v_acct_type,
                                  v_is_control_acct,
                                  v_control_acct_categroy,
                                  gjsp.currency_id,
                                  gjsp.currency_code) loop

          v_retained_earning_acct_count := 0;

          select count('x')
            into v_retained_earning_acct_count
            from gl_je_summary_period gjsp
           where gjsp.comp_id = P_COMP_ID
             and gjsp.cal_dtl_id = P_CAL_DTL_ID
             and gjsp.period_id = P_PERIOD_ID
             and gjsp.global_segment_id = bal_ins.global_segment_id
             and gjsp.acct_code_id = v_acct_code_id
             and gjsp.currency_id = bal_ins.currency_id;

          if v_retained_earning_acct_count > 0 then
            update gl_je_summary_period gjsp
               set bb_amount_cr    = bal_ins.sum_cb_amount_cr,
                   bb_amount_dr    = bal_ins.sum_cb_amount_dr,
                   bb_accounted_cr = bal_ins.sum_cb_accounted_cr,
                   bb_accounted_dr = bal_ins.sum_cb_accounted_dr,
                   cb_amount_cr    = bal_ins.sum_cb_amount_cr +
                                     nvl(amount_cr, 0),
                   cb_amount_dr    = bal_ins.sum_cb_amount_dr +
                                     nvl(amount_dr, 0),
                   cb_accounted_cr = bal_ins.sum_cb_accounted_cr +
                                     nvl(accounted_cr, 0),
                   cb_accounted_dr = bal_ins.sum_cb_accounted_dr +
                                     nvl(accounted_dr, 0)
             where gjsp.comp_id = P_COMP_ID
               and gjsp.cal_dtl_id = P_CAL_DTL_ID
               and gjsp.period_id = P_PERIOD_ID
               and gjsp.global_segment_id = bal_ins.global_segment_id
               and gjsp.acct_code_id = v_acct_code_id
               and gjsp.currency_id = bal_ins.currency_id;
          else
            insert into gl_je_summary_period
              (comp_id,
               comp_short_name,
               cal_id,
               cal_acct_year,
               cal_dtl_id,
               period_id,
               period_type,
               period_number,
               period_name,
               global_segment_id,
               global_segment,
               group_id,
               group_name,
               acct_code_id,
               acct_code,
               acct_code_desc,
               acct_type,
               is_control_acct,
               control_acct_categroy,
               currency_id,
               currency_code,
               bb_amount_cr,
               bb_amount_dr,
               bb_accounted_cr,
               bb_accounted_dr,
               amount_cr,
               amount_dr,
               accounted_cr,
               accounted_dr,
               cb_amount_cr,
               cb_amount_dr,
               cb_accounted_cr,
               cb_accounted_dr,
               audited)
            values
              (bal_ins.comp_id,
               bal_ins.comp_short_name,
               v_cur_cal_id,
               v_cur_cal_acct_year,
               P_cal_dtl_id,
               P_PERIOD_ID,
               P_PERIOD_TYPE,
               P_period_number,
               P_period_name,
               bal_ins.global_segment_id,
               bal_ins.global_segment,
               bal_ins.v_group_id,
               bal_ins.v_group_name,
               bal_ins.v_acct_code_id,
               bal_ins.v_retained_earning_account,
               bal_ins.v_acct_code_desc,
               bal_ins.v_acct_type,
               bal_ins.v_is_control_acct,
               bal_ins.v_control_acct_categroy,
               bal_ins.currency_id,
               bal_ins.currency_code,
               bal_ins.sum_cb_amount_cr,
               bal_ins.sum_cb_amount_dr,
               bal_ins.sum_cb_accounted_cr,
               bal_ins.sum_cb_accounted_dr,
               0,
               0,
               0,
               0,
               bal_ins.sum_cb_amount_cr,
               bal_ins.sum_cb_amount_dr,
               bal_ins.sum_cb_accounted_cr,
               bal_ins.sum_cb_accounted_dr,
               'N');
          end if;
        end loop;

        for bal_ins in (select gjsp.comp_id,
                               gjsp.comp_short_name,
                               gjsp.global_segment_id,
                               gjsp.global_segment,
                               gjsp.group_id,
                               gjsp.group_name,
                               gjsp.acct_code_id,
                               gjsp.acct_code,
                               gjsp.acct_code_desc,
                               gjsp.acct_type,
                               gjsp.is_control_acct,
                               gjsp.control_acct_categroy,
                               gjsp.currency_id,
                               gjsp.currency_code
                          from gl_je_summary_period gjsp
                         where gjsp.period_id = v_period_id
                           and gjsp.comp_id = P_COMP_ID
                           and gjsp.acct_type = 'P') loop

          v_retained_earning_acct_count := 0;

          select count('x')
            into v_retained_earning_acct_count
            from gl_je_summary_period gjsp
           where gjsp.comp_id = P_COMP_ID
             and gjsp.cal_dtl_id = P_CAL_DTL_ID
             and gjsp.period_id = P_PERIOD_ID
             and gjsp.global_segment_id = bal_ins.global_segment_id
             and gjsp.acct_code_id = bal_ins.acct_code_id
             and gjsp.currency_id = bal_ins.currency_id;

          if v_retained_earning_acct_count > 0 then
            null;
          else
            insert into gl_je_summary_period
              (comp_id,
               comp_short_name,
               cal_id,
               cal_acct_year,
               cal_dtl_id,
               period_id,
               period_type,
               period_number,
               period_name,
               global_segment_id,
               global_segment,
               group_id,
               group_name,
               acct_code_id,
               acct_code,
               acct_code_desc,
               acct_type,
               is_control_acct,
               control_acct_categroy,
               currency_id,
               currency_code,
               bb_amount_cr,
               bb_amount_dr,
               bb_accounted_cr,
               bb_accounted_dr,
               amount_cr,
               amount_dr,
               accounted_cr,
               accounted_dr,
               cb_amount_cr,
               cb_amount_dr,
               cb_accounted_cr,
               cb_accounted_dr,
               audited)
            values
              (bal_ins.comp_id,
               bal_ins.comp_short_name,
               v_cur_cal_id,
               v_cur_cal_acct_year,
               P_cal_dtl_id,
               P_PERIOD_ID,
               P_PERIOD_TYPE,
               P_period_number,
               P_period_name,
               bal_ins.global_segment_id,
               bal_ins.global_segment,
               bal_ins.group_id,
               bal_ins.group_name,
               bal_ins.acct_code_id,
               bal_ins.acct_code,
               bal_ins.acct_code_desc,
               bal_ins.acct_type,
               bal_ins.is_control_acct,
               bal_ins.control_acct_categroy,
               bal_ins.currency_id,
               bal_ins.currency_code,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               'N');
          end if;
        end loop;
      
/*      ELSE

        select gcacd.cal_acct_year, gcacd.cal_id
          into v_cur_cal_acct_year, v_cur_cal_id
          from gl_comp_acct_calendar_dtl gcacd
         where gcacd.comp_id = P_COMP_ID
           and gcacd.cal_dtl_id = P_CAL_DTL_ID;
    
    for bal_ins in (select gch.comp_short_name,gsv.segment_value_id global_segment_id,
               gsv.segment_value global_segment,
               gag.acct_grp_id group_id,
               gag.acct_grp_desc group_name,
               gac.acct_code_id,
               gac.acct_code,
               gac.acct_code_desc,
               gac.acct_type,
               gac.is_control_acct,
               gac.control_acct_categroy,
               sc.currency_id,
               sc.currency_code
                from gl_acct_codes gac,gl_segment_values gsv,gl_companies_hdr gch, ssm_currencies sc,gl_acct_groups gag
      where gch.comp_id=P_COMP_ID
      and gch.COMP_GLOBAL_SEGMENT_ID=gsv.SEGMENT_ID
      and gac.ACCT_ORG_ID=gch.comp_id
      and gac.ACCT_GRP_ID=gag.ACCT_GRP_ID)loop
        
  insert into gl_je_summary_period
              (comp_id,
               comp_short_name,
               cal_id,
               cal_acct_year,
               cal_dtl_id,
               period_id,
               period_type,
               period_number,
               period_name,
               global_segment_id,
               global_segment,
               group_id,
               group_name,
               acct_code_id,
               acct_code,
               acct_code_desc,
               acct_type,
               is_control_acct,
               control_acct_categroy,
               currency_id,
               currency_code,
               bb_amount_cr,
               bb_amount_dr,
               bb_accounted_cr,
               bb_accounted_dr,
               amount_cr,
               amount_dr,
               accounted_cr,
               accounted_dr,
               cb_amount_cr,
               cb_amount_dr,
               cb_accounted_cr,
               cb_accounted_dr,
               audited)
            values
              (p_comp_id,
               bal_ins.comp_short_name,
               v_cur_cal_id,
               v_cur_cal_acct_year,
               P_cal_dtl_id,
               P_PERIOD_ID,
               P_PERIOD_TYPE,
               P_period_number,
               P_period_name,
               bal_ins.global_segment_id,
               bal_ins.global_segment,
               bal_ins.group_id,
               bal_ins.group_name,
               bal_ins.acct_code_id,
               bal_ins.acct_code,
               bal_ins.acct_code_desc,
               bal_ins.acct_type,
               bal_ins.is_control_acct,
               bal_ins.control_acct_categroy,
               bal_ins.currency_id,
               bal_ins.currency_code,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               'N');
end loop;
*/              
      end if;

      /*elsif P_PERIOD_STATUS_OLD = 'OPEN' and P_PERIOD_STATUS_NEW='CLOSED' then
      elsif P_PERIOD_STATUS_OLD = 'CLOSED' and P_PERIOD_STATUS_NEW='AUDITED' then
      */
    ELSIF P_PERIOD_STATUS_OLD = 'NOP' AND P_PERIOD_STATUS_NEW = 'OPEN' AND
          P_PERIOD_NUMBER > 1 then

      select period_id
        into v_period_id
        from gl_comp_acct_period_dtl_g gcapd
       where gcapd.period_number = P_PERIOD_NUMBER - 1
         and gcapd.comp_id = P_COMP_ID
         and gcapd.cal_dtl_id = P_CAL_DTL_ID
         and gcapd.period_status in ('OPEN', 'CLOSED', 'AUDITED');

      for bal_ins in (select *
                        from gl_je_summary_period gjsp
                       where gjsp.period_id = v_period_id
                         and gjsp.comp_id = P_COMP_ID) loop

        insert into gl_je_summary_period
          (comp_id,
           comp_short_name,
           cal_id,
           cal_acct_year,
           cal_dtl_id,
           period_id,
           period_type,
           period_number,
           period_name,
           global_segment_id,
           global_segment,
           group_id,
           group_name,
           acct_code_id,
           acct_code,
           acct_code_desc,
           acct_type,
           is_control_acct,
           control_acct_categroy,
           currency_id,
           currency_code,
           bb_amount_cr,
           bb_amount_dr,
           bb_accounted_cr,
           bb_accounted_dr,
           amount_cr,
           amount_dr,
           accounted_cr,
           accounted_dr,
           cb_amount_cr,
           cb_amount_dr,
           cb_accounted_cr,
           cb_accounted_dr,
           audited)
        values
          (bal_ins.comp_id,
           bal_ins.comp_short_name,
           bal_ins.cal_id,
           bal_ins.cal_acct_year,
           bal_ins.cal_dtl_id,
           P_PERIOD_ID,
           P_PERIOD_TYPE,
           P_period_number,
           P_period_name,
           bal_ins.global_segment_id,
           bal_ins.global_segment,
           bal_ins.group_id,
           bal_ins.group_name,
           bal_ins.acct_code_id,
           bal_ins.acct_code,
           bal_ins.acct_code_desc,
           bal_ins.acct_type,
           bal_ins.is_control_acct,
           bal_ins.control_acct_categroy,
           bal_ins.currency_id,
           bal_ins.currency_code,
           bal_ins.cb_amount_cr,
           bal_ins.cb_amount_dr,
           bal_ins.cb_accounted_cr,
           bal_ins.cb_accounted_dr,
           0,
           0,
           0,
           0,
           bal_ins.cb_amount_cr,
           bal_ins.cb_amount_dr,
           bal_ins.cb_accounted_cr,
           bal_ins.cb_accounted_dr,
           'N');

      end loop;

    end if;

  END GL_OPENING_BALANCE;

  PROCEDURE GL_BALANCE_UPDATE(P_JOURNAL_ID           IN VARCHAR2,
                              P_JE_COMP_ID           IN VARCHAR2,
                              P_je_currency_id       IN VARCHAR2,
                              P_je_global_segment_id IN VARCHAR2,
                              P_je_period_id         IN VARCHAR2) IS

    v_available         number := 0;
    v_cur_period_number number;
    v_cal_dtl_id        varchar2(100);

    v_ins_comp_short_name       varchar2(100);
    v_ins_cal_id                varchar2(100);
    v_ins_cal_acct_year         varchar2(100);
    v_ins_cal_dtl_id            varchar2(100);
    v_ins_period_number         number;
    v_ins_period_name           varchar2(100);
    v_ins_period_type           varchar2(100);
    v_ins_segment_value         varchar2(100);
    v_ins_acct_grp_id           varchar2(100);
    v_ins_acct_grp_desc         varchar2(100);
    v_ins_acct_code             varchar2(100);
    v_ins_acct_code_desc        varchar2(100);
    v_ins_acct_type             varchar2(100);
    v_ins_is_control_acct       varchar2(100);
    v_ins_control_acct_categroy varchar2(100);
    v_ins_currency_code         varchar2(100);
  BEGIN

    for JURUP in (select je_acct_code_id,
                         je_amount_cr,
                         je_amount_dr,
                         je_accounted_amt_cr,
                         je_accounted_amt_dr
                    from gl_journal_dtl gjd
                   where gjd.je_hdr_id = P_JOURNAL_ID) loop

      v_available := 0;

      select count('x')
        into v_available
        from gl_je_summary_period gjsp
       where gjsp.comp_id = p_je_comp_id
         and gjsp.period_id = p_je_period_id
         and gjsp.global_segment_id = P_je_global_segment_id
         and gjsp.acct_code_id = jurup.je_acct_code_id
         and gjsp.currency_id = P_je_currency_id;

      if v_available > 0 then

        update gl_je_summary_period gjspu
           set gjspu.amount_cr       = nvl(gjspu.amount_cr, 0) +
                                       nvl(jurup.je_amount_cr, 0),
               gjspu.amount_dr       = nvl(gjspu.amount_dr, 0) +
                                       nvl(jurup.je_amount_dr, 0),
               gjspu.accounted_cr    = nvl(gjspu.accounted_cr, 0) +
                                       nvl(jurup.je_accounted_amt_cr, 0),
               gjspu.accounted_dr    = nvl(gjspu.accounted_dr, 0) +
                                       nvl(jurup.je_accounted_amt_dr, 0),
               gjspu.cb_amount_cr    = nvl(gjspu.cb_amount_cr, 0) +
                                       nvl(jurup.je_amount_cr, 0),
               gjspu.cb_amount_dr    = nvl(gjspu.cb_amount_dr, 0) +
                                       nvl(jurup.je_amount_dr, 0),
               gjspu.cb_accounted_cr = nvl(gjspu.cb_accounted_cr, 0) +
                                       nvl(jurup.je_accounted_amt_cr, 0),
               gjspu.cb_accounted_dr = nvl(gjspu.cb_accounted_dr, 0) +
                                       nvl(jurup.je_accounted_amt_dr, 0)
         where gjspu.comp_id = P_je_comp_id
           and gjspu.period_id = P_je_period_id
           and gjspu.global_segment_id = P_je_global_segment_id
           and gjspu.acct_code_id = jurup.je_acct_code_id
           and gjspu.currency_id = P_je_currency_id;

        v_cur_period_number := 0;
        v_cal_dtl_id        := null;

        select gcapd.period_number, gcapd.cal_dtl_id
          into v_cur_period_number, v_cal_dtl_id
          from gl_comp_acct_period_dtl gcapd
         where gcapd.period_id = P_je_period_id
           and gcapd.comp_id = P_je_comp_id;

        for juraup in (select gjsp.rowid row_id, gjsp.*
                         from gl_je_summary_period gjsp
                        where gjsp.comp_id = P_je_comp_id
                          and gjsp.cal_dtl_id = v_cal_dtl_id
                          and gjsp.period_number > v_cur_period_number
                          and gjsp.acct_code_id = jurup.je_acct_code_id
                          and gjsp.currency_id = P_je_currency_id
                          and gjsp.global_segment_id =
                              P_je_global_segment_id) loop

          update gl_je_summary_period gjsp
             set gjsp.bb_amount_cr    = nvl(gjsp.bb_amount_cr, 0) +
                                        nvl(jurup.je_amount_cr, 0),
                 gjsp.bb_amount_dr    = nvl(gjsp.bb_amount_dr, 0) +
                                        nvl(jurup.je_amount_dr, 0),
                 gjsp.bb_accounted_cr = nvl(gjsp.bb_accounted_cr, 0) +
                                        nvl(jurup.je_accounted_amt_cr, 0),
                 gjsp.bb_accounted_dr = nvl(gjsp.bb_accounted_dr, 0) +
                                        nvl(jurup.je_accounted_amt_dr, 0),
                 gjsp.cb_amount_cr    = nvl(gjsp.cb_amount_cr, 0) +
                                        nvl(jurup.je_amount_cr, 0),
                 gjsp.cb_amount_dr    = nvl(gjsp.cb_amount_dr, 0) +
                                        nvl(jurup.je_amount_dr, 0),
                 gjsp.cb_accounted_cr = nvl(gjsp.cb_accounted_cr, 0) +
                                        nvl(jurup.je_accounted_amt_cr, 0),
                 gjsp.cb_accounted_dr = nvl(gjsp.cb_accounted_dr, 0) +
                                        nvl(jurup.je_accounted_amt_dr, 0)
           where rowid = juraup.row_id;

        end loop;

      else

        select gch.comp_short_name,
               gcacd.cal_id,
               gcacd.cal_acct_year,
               gcapd.cal_dtl_id,
               gcapd.period_number,
               gcapd.period_name,
               gcapd.period_type
          into v_ins_comp_short_name,
               v_ins_cal_id,
               v_ins_cal_acct_year,
               v_ins_cal_dtl_id,
               v_ins_period_number,
               v_ins_period_name,
               v_ins_period_type
          from gl_comp_acct_calendar_dtl gcacd,
               gl_comp_acct_period_dtl   gcapd,
               gl_companies_hdr          gch
         where gcapd.period_id = P_je_period_id
           and gcacd.comp_id = gcapd.comp_id
           and gcacd.cal_dtl_id = gcapd.cal_dtl_id
           and gch.comp_id = gcacd.comp_id;

        select segment_value
          into v_ins_segment_value
          from gl_segment_values gsv
         where gsv.segment_value_id = P_je_global_segment_id;

        select gac.acct_grp_id,
               gag.acct_grp_desc,
               gac.acct_code,
               gac.acct_code_desc,
               gac.acct_type,
               gac.is_control_acct,
               gac.control_acct_categroy
          into v_ins_acct_grp_id,
               v_ins_acct_grp_desc,
               v_ins_acct_code,
               v_ins_acct_code_desc,
               v_ins_acct_type,
               v_ins_is_control_acct,
               v_ins_control_acct_categroy
          from gl_acct_groups gag, gl_acct_codes gac
         where gac.acct_code_id = jurup.je_acct_code_id
           and gag.acct_grp_id = gac.acct_grp_id;

        select currency_code
          into v_ins_currency_code
          from ssm_currencies sc
         where sc.currency_id = P_je_currency_id;

        insert into gl_je_summary_period
          (comp_id,
           comp_short_name,
           cal_id,
           cal_acct_year,
           cal_dtl_id,
           period_id,
           period_type,
           period_number,
           period_name,
           global_segment_id,
           global_segment,
           group_id,
           group_name,
           acct_code_id,
           acct_code,
           acct_code_desc,
           acct_type,
           is_control_acct,
           control_acct_categroy,
           currency_id,
           currency_code,
           bb_amount_cr,
           bb_amount_dr,
           bb_accounted_cr,
           bb_accounted_dr,
           amount_cr,
           amount_dr,
           accounted_cr,
           accounted_dr,
           cb_amount_cr,
           cb_amount_dr,
           cb_accounted_cr,
           cb_accounted_dr,
           audited)
        values
          (P_je_comp_id,
           v_ins_comp_short_name,
           v_ins_cal_id,
           v_ins_cal_acct_year,
           v_ins_cal_dtl_id,
           P_je_period_id,
           v_ins_period_type,
           v_ins_period_number,
           v_ins_period_name,
           P_je_global_segment_id,
           v_ins_segment_value,
           v_ins_acct_grp_id,
           v_ins_acct_grp_desc,
           jurup.je_acct_code_id,
           v_ins_acct_code,
           v_ins_acct_code_desc,
           v_ins_acct_type,
           v_ins_is_control_acct,
           v_ins_control_acct_categroy,
           P_je_currency_id,
           v_ins_currency_code,
           0,
           0,
           0,
           0,
           jurup.je_amount_cr,
           jurup.je_amount_dr,
           jurup.je_accounted_amt_cr,
           jurup.je_accounted_amt_dr,
           jurup.je_amount_cr,
           jurup.je_amount_dr,
           jurup.je_accounted_amt_cr,
           jurup.je_accounted_amt_dr,
           'N');

        for juraup in (select gjsp.period_id,
                              gjsp.period_type,
                              gjsp.period_number,
                              gjsp.period_name
                         from gl_je_summary_period gjsp
                        where gjsp.comp_id = P_je_comp_id
                          and gjsp.cal_dtl_id = v_cal_dtl_id
                          and gjsp.period_number > v_cur_period_number) loop

          insert into gl_je_summary_period
            (comp_id,
             comp_short_name,
             cal_id,
             cal_acct_year,
             cal_dtl_id,
             period_id,
             period_type,
             period_number,
             period_name,
             global_segment_id,
             global_segment,
             group_id,
             group_name,
             acct_code_id,
             acct_code,
             acct_code_desc,
             acct_type,
             is_control_acct,
             control_acct_categroy,
             currency_id,
             currency_code,
             bb_amount_cr,
             bb_amount_dr,
             bb_accounted_cr,
             bb_accounted_dr,
             amount_cr,
             amount_dr,
             accounted_cr,
             accounted_dr,
             cb_amount_cr,
             cb_amount_dr,
             cb_accounted_cr,
             cb_accounted_dr,
             audited)
          values
            (P_je_comp_id,
             v_ins_comp_short_name,
             v_ins_cal_id,
             v_ins_cal_acct_year,
             v_ins_cal_dtl_id,
             juraup.period_id,
             juraup.period_type,
             juraup.period_number,
             juraup.period_name,
             P_je_global_segment_id,
             v_ins_segment_value,
             v_ins_acct_grp_id,
             v_ins_acct_grp_desc,
             jurup.je_acct_code_id,
             v_ins_acct_code,
             v_ins_acct_code_desc,
             v_ins_acct_type,
             v_ins_is_control_acct,
             v_ins_control_acct_categroy,
             P_je_currency_id,
             v_ins_currency_code,
             jurup.je_amount_cr,
             jurup.je_amount_dr,
             jurup.je_accounted_amt_cr,
             jurup.je_accounted_amt_dr,
             0,
             0,
             0,
             0,
             jurup.je_amount_cr,
             jurup.je_amount_dr,
             jurup.je_accounted_amt_cr,
             jurup.je_accounted_amt_dr,
             'N');

        end loop;

      end if;

    end loop;

    --commit;

  END;
  FUNCTION Get_acct_codes(p_grp_id IN VARCHAR2) RETURN clob IS
    v_list clob;
    -- v_list VARCHAR2(4000);
    CURSOR acc_cur(c_grp IN VARCHAR2) IS
      select acct_code, ACCT_CODE_DESC, ATTRIBUTE10
        from gl_acct_codes --CHANGED HERE
       where acct_grp_id = c_grp; -- order by to_number(ATTRIBUTE10) ;
  BEGIN
    for m in acc_cur(p_grp_id) Loop
      --v_list:=''''||v_list||','||m.acct_code||'''';
      v_list := v_list || chr(10) ||
                ltrim(rtrim(m.acct_code || '-' || m.ACCT_CODE_DESC || '-' ||
                            m.ATTRIBUTE10));
    end loop;
    --  v_list:=dbms_lob.substr(ltrim(v_list,','),length(v_list),1);
    v_list := ltrim(v_list, ',');
    --dbms_output.put_line('Acct codes '||v_list);
    RETURN v_list;
  END Get_acct_codes;
  FUNCTION Get_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list1 VARCHAR2(2000);
  BEGIN
    select cc.acct_grp_desc
      into v_list1
      from GL_ACCT_GROUPS cc --CHANGED HERE
     where cc.acct_grp_id = p_grp_id;

    RETURN v_list1;
  END Get_acct_Groups;

  FUNCTION Get_first_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list   VARCHAR2(2000);
    v_grp_id varchar2(50);
  BEGIN
    select gaglh.acct_grp_id
      into v_grp_id
      from gl_acct_group_link_hdr gaglh, gl_acct_group_link_dtl gagld --CHANGED HERE
     where gaglh.acct_grp_lnk_id = gagld.acct_grp_lnk_id
       and gagld.acct_grp_id = p_grp_id;

    select cc.acct_grp_desc
      into v_list
      from GL_ACCT_GROUPS cc --CHANGED HERE
     where cc.acct_grp_id = v_grp_id;

    RETURN v_list;
  END Get_first_acct_Groups;

  PROCEDURE GL_ACC_GRP_LINK(p_org_id IN VARCHAR2, p_acc_grp_id IN VARCHAR2) IS
    v_acc_grp_id varchar2(3000);

    CURSOR acc_cur IS
      select acct_code || '-' || ACCT_CODE_DESC || '-' || ATTRIBUTE10 as acct_codes
      -- select ATTRIBUTE10 || '- ' || ACCT_CODE_DESC_AR ||'- '|| acct_code as acct_codes
        from gl_acct_codes --CHANGED HERE
       where acct_grp_id = v_acc_grp_id
       order by to_number(ATTRIBUTE10);

    CURSOR c_grp_code(v_mst_grp_id varchar2) is
      select gl_pkg.Get_first_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                               '/'))),
                                                               1,
                                                               19)),
                                                  '/')) as level1,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         1,
                                                         19)),
                                            '/')) as level2,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         21,
                                                         19)),
                                            '/')) as level3,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         39,
                                                         20)),
                                            '/')) as level4,

             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         59,
                                                         30)),
                                            '/')) as level5,

             (replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                   '/'))),
                                   59,
                                   30)),
                      '/')) as acc_grp_id,

             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         79,
                                                         40)),
                                            '/')) as level6,

             gl_pkg.get_acct_codes(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                '/'))),
                                                Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                      '/'))),
                                                      '/',
                                                      -1,
                                                      1),
                                                length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                       '/'))))),
                                         '/')) as acct_codes,
             LEVEL AS LEVEL7

        from gl_acct_group_link_hdr a, --CHANGED HERE
             gl_acct_group_link_dtl b, --CHANGED HERE
             GL_ACCT_GROUPS         C --CHANGED HERE
       where b.ACCT_GRP_LNK_ID = a.ACCT_GRP_LNK_ID
         and c.acct_grp_id = a.acct_grp_id
         and a.created_by in ('SYSTEM1', 'SYSTEM2')
         and level <= 5
       start with a.acct_grp_id = v_mst_grp_id
      connect by prior b.acct_grp_id = a.acct_Grp_id
             and level <= 5
       order by LEVEL1, level2, level3, level4, level5, level6;

  BEGIN
    delete from temp_gl_acc_grp_link_hdr;
    commit;
    for i in (select acct_grp_id
                from gl_acct_groups a
               where not exists (select *
                        from gl_acct_group_link_dtl b
                       where a.acct_grp_id = b.acct_grp_id)) loop

      FOR f_grp_code IN c_grp_code(i.acct_grp_id) LOOP

        v_acc_grp_id := f_grp_code.acc_grp_id;

        if (v_acc_grp_id is not null) then

          FOR f_acc_cur IN acc_cur LOOP

            BEGIN
              insert into temp_gl_acc_grp_link_hdr
                (LEVEL1,
                 LEVEL2,
                 LEVEL3,
                 LEVEL4,
                 LEVEL5,
                 LEVEL6,
                 LEVEL7,
                 LEVEL8,
                 LEVEL9,
                 LEVEL10,
                 ACC_GRP_ID,
                 TOOLS)
              values
                (f_grp_code.level1,
                 f_grp_code.level2,
                 f_grp_code.level3,
                 f_grp_code.level4,
                 f_grp_code.level5,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.acc_grp_id,
                 acc_grp_link_typ(f_acc_cur.acct_codes));

            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERR1 ' || SQLERRM);
            END;
          END LOOP;

        else

          insert into temp_gl_acc_grp_link_hdr
            (LEVEL1,
             LEVEL2,
             LEVEL3,
             LEVEL4,
             LEVEL5,
             LEVEL6,
             LEVEL7,
             LEVEL8,
             LEVEL9,
             LEVEL10,
             TOOLS)
          values
            (f_grp_code.level1,
             f_grp_code.level2,
             f_grp_code.level3,
             f_grp_code.level4,
             f_grp_code.level5,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             acc_grp_link_typ(f_grp_code.acct_codes));

        end if;
      END LOOP;

    end loop;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Encounted an exception -' || sqlerrm);
  END GL_ACC_GRP_LINK;

  --START
  PROCEDURE GL_ACC_GRP_LINK1(p_org_id     IN VARCHAR2,
                             p_acc_grp_id IN VARCHAR2) IS
    v_acc_grp_id varchar2(3000);

    CURSOR acc_cur IS
      select acct_code || '-' || ACCT_CODE_DESC || '-' || ATTRIBUTE10 as acct_codes
      -- select ATTRIBUTE10 || '- ' || ACCT_CODE_DESC_AR ||'- '|| acct_code as acct_codes
        from gl_acct_codes --CHANGED HERE
       where acct_grp_id = v_acc_grp_id
       order by to_number(ATTRIBUTE10);

    CURSOR c_grp_code(v_mst_grp_id varchar2) is
      select gl_pkg.Get_first_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                               '/'))),
                                                               1,
                                                               19)),
                                                  '/')) as level1,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         1,
                                                         19)),
                                            '/')) as level2,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         21,
                                                         19)),
                                            '/')) as level3,
             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         39,
                                                         20)),
                                            '/')) as level4,

             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         59,
                                                         30)),
                                            '/')) as level5,

             (replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                   '/'))),
                                   59,
                                   30)),
                      '/')) as acc_grp_id,

             gl_pkg.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         79,
                                                         40)),
                                            '/')) as level6,

             gl_pkg.get_acct_codes(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                '/'))),
                                                Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                      '/'))),
                                                      '/',
                                                      -1,
                                                      1),
                                                length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                       '/'))))),
                                         '/')) as acct_codes,
             LEVEL AS LEVEL7

        from gl_acct_group_link_hdr a, --CHANGED HERE
             gl_acct_group_link_dtl b, --CHANGED HERE
             GL_ACCT_GROUPS         C --CHANGED HERE
       where b.ACCT_GRP_LNK_ID = a.ACCT_GRP_LNK_ID
         and c.acct_grp_id = a.acct_grp_id
         and a.created_by in ('SYSTEM1', 'SYSTEM2')
         and level <= 5
       start with a.acct_grp_id = v_mst_grp_id
      connect by prior b.acct_grp_id = a.acct_Grp_id
             and level <= 5
       order by LEVEL1, level2, level3, level4, level5, level6;

  BEGIN
    delete from temp_gl_acc_grp_link_hdr1;
    commit;
    for i in (select acct_grp_id
                from gl_acct_groups a
               where not exists (select *
                        from gl_acct_group_link_dtl b
                       where a.acct_grp_id = b.acct_grp_id)) loop

      FOR f_grp_code IN c_grp_code(i.acct_grp_id) LOOP

        v_acc_grp_id := f_grp_code.acc_grp_id;

        if (v_acc_grp_id is not null) then

          FOR f_acc_cur IN acc_cur LOOP

            BEGIN
              insert into temp_gl_acc_grp_link_hdr1
                (LEVEL1,
                 LEVEL2,
                 LEVEL3,
                 LEVEL4,
                 LEVEL5,
                 LEVEL6,
                 LEVEL7,
                 LEVEL8,
                 LEVEL9,
                 LEVEL10,
                 ACC_GRP_ID,
                 TOOLS)
              values
                (f_grp_code.level1,
                 f_grp_code.level2,
                 f_grp_code.level3,
                 f_grp_code.level4,
                 f_grp_code.level5,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.acc_grp_id,
                 ACCT_OBJ_NT(ACCT_OBJ(substr(f_acc_cur.acct_codes, 1, 6),
                                      f_acc_cur.acct_codes)));

            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERR1 ' || SQLERRM);
            END;
          END LOOP;

        else

          insert into temp_gl_acc_grp_link_hdr1
            (LEVEL1,
             LEVEL2,
             LEVEL3,
             LEVEL4,
             LEVEL5,
             LEVEL6,
             LEVEL7,
             LEVEL8,
             LEVEL9,
             LEVEL10,
             TOOLS)
          values
            (f_grp_code.level1,
             f_grp_code.level2,
             f_grp_code.level3,
             f_grp_code.level4,
             f_grp_code.level5,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             ACCT_OBJ_NT(ACCT_OBJ(substr(f_grp_code.acct_codes, 1, 6),
                                  f_grp_code.acct_codes)));

        end if;
      END LOOP;

    end loop;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Encounted an exception -' || sqlerrm);
  END GL_ACC_GRP_LINK1;

  --END

  PROCEDURE GL_Statement_of_accounts(p_org_id            IN VARCHAR2,
                                     p_global_segment_id IN VARCHAR2,
                                     p_from_date         IN DATE,
                                     p_to_date           IN DATE,
                                     p_acct_code         IN VARCHAR2,
                                     p_unposted          IN VARCHAR2) IS
    v_error varchar2(2000);
    v_previous_period       VARCHAR2(10);
    v_current_period_number NUMBER;
    v_open_balance          NUMBER;
    v_close_dr              NUMBER;
    v_close_cr              NUMBER;
    v_close_bal             NUMBER;
    v_acct_code_id          varchar2(50);
    v_line_balance          Number;
    v_sl_no                 Number;
    v_prev_acct_code        VARCHAR2(50);
  BEGIN

    DELETE gl_statement_of_accounts;
    commit;

    if upper(p_acct_code) = 'ALL' then
      v_acct_code_id := null;
    else
      v_acct_code_id := p_acct_code;
    end if;

      v_line_balance := 0;
      v_prev_acct_code := '';
      v_sl_no := 0;
    FOR acct_summary IN (select gjsp.*
                           from gl_je_summary_period    gjsp,
                                gl_comp_acct_period_dtl gcapd
                          where gjsp.period_id = gcapd.period_id
                            and gcapd.comp_id = gjsp.comp_id
                            and gcapd.comp_id = p_org_id
                            and gjsp.acct_code_id =
                                nvl(v_acct_code_id, gjsp.acct_code_id)
                            and gcapd.period_to_dt >= p_from_date
                            and gcapd.period_to_dt <= p_to_date
                            order by gcapd.period_from_dt asc) loop

      if v_prev_acct_code != acct_summary.acct_code_id then

      v_line_balance := nvl(acct_summary.bb_accounted_dr, 0) -
                        nvl(acct_summary.bb_accounted_cr, 0);

      end if;
      v_sl_no:=v_sl_no+1;
      insert into gl_statement_of_accounts
        (Period_ID,
         Period_Name,
         Period_Number,
         Account_id,
         account_code,
         account_name,
         Journal_Line_Particulars,
         Journal_Line_Balance,
         SL_NO)
      values
        (acct_summary.period_id,
         acct_summary.period_name,
         acct_summary.period_number,
         acct_summary.acct_code_id,
         acct_summary.acct_code,
         acct_summary.acct_code_desc,
         'Opening Balance',
         v_line_balance,
         v_sl_no);

      if p_unposted = '1' then
        begin
          for ad in (SELECT gjhb.je_period_id,
                            gapd.period_name,
                            gapd.period_number,
                            gjhb.je_hdr_id,
                            gjhb.je_date,
                            sc.currency_code curr_code,
                            sc.currency_desc currency,
                            gjhb.je_exchange_rate_value je_ex_rate,
                            gjdb.je_particulars,
                            nvl(gjdb.je_amount_cr, 0) tran_cr,
                            nvl(gjdb.je_amount_dr, 0) tran_dr,
                            NVL(Gjdb.Je_Accounted_Amt_dr, 0) jour_dr,
                            NVL(Gjdb.Je_Accounted_Amt_cr, 0) jour_cr,
                            decode(gjhb.workflow_completion_status,
                                   '1',
                                   'Posted',
                                   'Unposted') jour_status,
                            gjdb.je_particulars particulars
                       FROM Gl_Journal_Hdr       gjhb,
                            Gl_Journal_Dtl       gjdb,
                            Gl_Acct_Codes        gacb,
                            gl_segment_values    gsv,
                            gl_companies_hdr     gch,
                            gl_acct_groups       gag,
                            gl_companies_dtl     gcd,
                            ssm_currencies       sc,
                            gl_acct_calendar_dtl gacd,
                            gl_acct_calendar_hdr gach,
                            Gl_Acct_Period_Dtl   Gapd
                      WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
                        AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
                        AND gjhb.je_global_segment_id = gsv.segment_value_id
                        AND gch.comp_id = gjhb.Je_Comp_Id
                        AND gacb.acct_grp_id = gag.acct_grp_id
                        AND sc.currency_id = gcd.comp_base_currency
                        AND gjhb.Je_Period_Id = Gapd.Period_Id
                        AND gch.comp_id = gcd.comp_id
                        AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
                        AND gacd.cal_id = gach.cal_id
                        AND gach.cal_id = gcd.comp_cal_id
                        AND gch.comp_id = p_org_id
                        AND gjhb.je_global_segment_id =
                            acct_summary.global_segment_id
                        AND gjhb.je_period_id = acct_summary.period_id
                        AND gjhb.je_currency_id = acct_summary.currency_id
                        and gag.acct_grp_id = acct_summary.group_id
                        and gjdb.je_acct_code_id = acct_summary.acct_code_id
                        AND gacb.workflow_completion_status = '1'
                        AND gacb.enabled_flag = '1'
                        AND gsv.workflow_completion_status = '1'
                        AND gsv.enabled_flag = '1'
                        AND gch.workflow_completion_status = '1'
                        AND gch.enabled_flag = '1'
                        AND gag.workflow_completion_status = '1'
                        AND gag.enabled_flag = '1'
                        order by gjhb.je_hdr_id) loop

            v_line_balance := v_line_balance + ad.jour_dr - ad.jour_cr;
        v_sl_no:=v_sl_no+1;

            insert into gl_statement_of_accounts
              (Period_ID,
               Period_Name,
               Period_Number,
               Account_id,
               account_code,
               account_name,
               Journal_number,
               Journal_date,
               Journal_Curr_Code,
               Journal_Currency,
               Journal_Ex_Rate,
               Journal_Line_Particulars,
               Journal_Tran_Cr,
               Journal_Tran_Dr,
               Journal_Acct_Cr,
               Journal_Acct_Dr,
               Journal_Line_Balance,
               Journal_Status,
               SL_NO)
            values
              (ad.je_period_id,
               ad.period_name,
               ad.period_number,
               acct_summary.acct_code_id,
               acct_summary.acct_code,
               acct_summary.acct_code_desc,
               ad.je_hdr_id,
               ad.je_date,
               ad.curr_code,
               ad.currency,
               ad.je_ex_rate,
               ad.particulars,
               ad.tran_cr,
               ad.tran_dr,
               ad.jour_cr,
               ad.jour_dr,
               v_line_balance,
               ad.jour_status,
               v_sl_no);

          end loop;
        exception
          when others then
            v_error := sqlerrm;
        end;
        commit;

      else
        begin
          for ad in (SELECT gjhb.je_period_id,
                            gapd.period_name,
                            gapd.period_number,
                            gjhb.je_hdr_id,
                            gjhb.je_date,
                            sc.currency_code curr_code,
                            sc.currency_desc currency,
                            gjhb.je_exchange_rate_value je_ex_rate,
                            gjdb.je_particulars,
                            nvl(gjdb.je_amount_cr, 0) tran_cr,
                            nvl(gjdb.je_amount_dr, 0) tran_dr,
                            NVL(Gjdb.Je_Accounted_Amt_dr, 0) jour_dr,
                            NVL(Gjdb.Je_Accounted_Amt_cr, 0) jour_cr,
                            decode(gjhb.workflow_completion_status,
                                   '1',
                                   'Posted',
                                   'Unposted') jour_status,
                            gjdb.je_particulars particulars
                       FROM Gl_Journal_Hdr       gjhb,
                            Gl_Journal_Dtl       gjdb,
                            Gl_Acct_Codes        gacb,
                            gl_segment_values    gsv,
                            gl_companies_hdr     gch,
                            gl_acct_groups       gag,
                            gl_companies_dtl     gcd,
                            ssm_currencies       sc,
                            gl_acct_calendar_dtl gacd,
                            gl_acct_calendar_hdr gach,
                            Gl_Acct_Period_Dtl   Gapd
                      WHERE gjhb.Je_Hdr_Id = gjdb.Je_Hdr_Id
                        AND gjdb.Je_Acct_Code_Id = gacb.Acct_Code_Id
                        AND gjhb.je_global_segment_id = gsv.segment_value_id
                        AND gch.comp_id = gjhb.Je_Comp_Id
                        AND gacb.acct_grp_id = gag.acct_grp_id
                        AND sc.currency_id = gcd.comp_base_currency
                        AND gjhb.Je_Period_Id = Gapd.Period_Id
                        AND gch.comp_id = gcd.comp_id
                        AND gacd.Cal_Dtl_Id = Gapd.Cal_Dtl_Id
                        AND gacd.cal_id = gach.cal_id
                        AND gach.cal_id = gcd.comp_cal_id
                        AND gch.comp_id = p_org_id
                        AND gjhb.je_global_segment_id =
                            acct_summary.global_segment_id
                        AND gjhb.je_period_id = acct_summary.period_id
                        AND gjhb.je_currency_id = acct_summary.currency_id
                        and gag.acct_grp_id = acct_summary.group_id
                        and gjdb.je_acct_code_id = acct_summary.acct_code_id
                        AND gjhb.workflow_completion_status = '1'
                        AND gjhb.enabled_flag = '1'
                        AND gjdb.workflow_completion_status = '1'
                        AND gjdb.enabled_flag = '1'
                        AND gacb.workflow_completion_status = '1'
                        AND gacb.enabled_flag = '1'
                        AND gsv.workflow_completion_status = '1'
                        AND gsv.enabled_flag = '1'
                        AND gch.workflow_completion_status = '1'
                        AND gch.enabled_flag = '1'
                        AND gag.workflow_completion_status = '1'
                        AND gag.enabled_flag = '1'
                        order by gjhb.je_hdr_id) loop
            v_sl_no:=v_sl_no+1;
            v_line_balance := v_line_balance + ad.jour_dr - ad.jour_cr;

            insert into gl_statement_of_accounts
              (Period_ID,
               Period_Name,
               Period_Number,
               Account_id,
               account_code,
               account_name,
               Journal_number,
               Journal_date,
               Journal_Curr_Code,
               Journal_Currency,
               Journal_Ex_Rate,
               Journal_Line_Particulars,
               Journal_Tran_Cr,
               Journal_Tran_Dr,
               Journal_Acct_Cr,
               Journal_Acct_Dr,
               Journal_Line_Balance,
               Journal_Status,
               SL_NO)
            values
              (ad.je_period_id,
               ad.period_name,
               ad.period_number,
               acct_summary.acct_code_id,
               acct_summary.acct_code,
               acct_summary.acct_code_desc,
               ad.je_hdr_id,
               ad.je_date,
               ad.curr_code,
               ad.currency,
               ad.je_ex_rate,
               ad.particulars,
               ad.tran_cr,
               ad.tran_dr,
               ad.jour_cr,
               ad.jour_dr,
               v_line_balance,
               ad.jour_status,
               v_sl_no);

          end loop;
        exception
          when others then

            v_error := sqlerrm;
        end;
      end if;
    v_prev_acct_code := acct_summary.acct_code_id;
    END LOOP;

    COMMIT;

  END GL_Statement_Of_Accounts;

END GL_PKG;
/
