CREATE OR REPLACE PACKAGE GL_ACC_GRP_CODE IS
  FUNCTION Get_acct_codes_Id_Var(p_grp_id IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION Get_acct_codes_Desc_Var(p_grp_id IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION Get_acct_codes_id(p_grp_id IN VARCHAR2) RETURN clob;
  FUNCTION Get_acct_Groups_Id(p_grp_id IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION Get_first_acct_Groups_Id(p_grp_id IN VARCHAR2) RETURN VARCHAR2;

  PROCEDURE GL_ACC_GRP_LINK_Id_Name(p_org_id     IN VARCHAR2,
                                    p_acc_grp_id IN VARCHAR2);
  FUNCTION Get_acct_codes(p_grp_id IN VARCHAR2) RETURN clob;
  FUNCTION Get_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION Get_first_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2;

  PROCEDURE Get_Acct_Code_Based_Group(p_org_id     IN VARCHAR2,
                                      p_acc_grp_id IN VARCHAR2);

END GL_ACC_GRP_CODE;
 
 
 
 
 
 
 
/
CREATE OR REPLACE PACKAGE BODY GL_ACC_GRP_CODE AS
  FUNCTION Get_acct_codes_Id_Var(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS

    v_list VARCHAR2(4000);
    CURSOR acc_cur(c_grp IN VARCHAR2) IS
      select acct_code, ACCT_CODE_DESC, ATTRIBUTE10, ACCT_CODE_ID
        from gl_acct_codes --CHANGED HERE
       where ENABLED_FLAG = '1'
         and acct_grp_id = c_grp; -- order by to_number(ATTRIBUTE10) ;
  BEGIN
    for m in acc_cur(p_grp_id) Loop
      v_list := v_list || chr(10) || ltrim(rtrim(m.ACCT_CODE_ID));
    end loop;
    v_list := ltrim(v_list, ',');
    RETURN v_list;
  END Get_acct_codes_Id_Var;

  FUNCTION Get_acct_codes_Desc_Var(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS

    v_list VARCHAR2(4000);
    CURSOR acc_cur(c_grp IN VARCHAR2) IS
      select acct_code, ACCT_CODE_DESC, ATTRIBUTE10
        from gl_acct_codes --CHANGED HERE
       where ENABLED_FLAG = '1'
         and acct_grp_id = c_grp; -- order by to_number(ATTRIBUTE10) ;
  BEGIN
    for m in acc_cur(p_grp_id) Loop
      v_list := v_list || chr(10) ||
                ltrim(rtrim(m.acct_code || '-' || m.ACCT_CODE_DESC || '-' ||
                            m.ATTRIBUTE10));
    end loop;
    v_list := ltrim(v_list, ',');
    RETURN v_list;
  END Get_acct_codes_Desc_Var;

  FUNCTION Get_acct_codes(p_grp_id IN VARCHAR2) RETURN clob IS
    v_list clob;
    -- v_list VARCHAR2(4000);
    CURSOR acc_cur(c_grp IN VARCHAR2) IS
      select acct_code, ACCT_CODE_DESC, ATTRIBUTE10
        from gl_acct_codes --CHANGED HERE
       where ENABLED_FLAG = '1'
         and acct_grp_id = c_grp; -- order by to_number(ATTRIBUTE10) ;
  BEGIN
    for m in acc_cur(p_grp_id) Loop
      --v_list:=''''||v_list||','||m.acct_code||'''';
      v_list := v_list || chr(10) ||
                ltrim(rtrim(m.acct_code || '-' || m.ACCT_CODE_DESC || '-' ||
                            m.ATTRIBUTE10));
    end loop;
    --  v_list:=dbms_lob.substr(ltrim(v_list,','),length(v_list),1);
    v_list := ltrim(v_list, ',');
    --dbms_output.put_line('Acct codes '||v_list);
    RETURN v_list;
  END Get_acct_codes;
  FUNCTION Get_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list1 VARCHAR2(2000);
  BEGIN
    select cc.acct_grp_desc
      into v_list1
      from GL_ACCT_GROUPS cc --CHANGED HERE
     where ENABLED_FLAG = '1'
       and cc.acct_grp_id = p_grp_id;

    RETURN v_list1;
  END Get_acct_Groups;

  FUNCTION Get_first_acct_Groups(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list   VARCHAR2(2000);
    v_grp_id varchar2(50);
  BEGIN
    select gaglh.acct_grp_id
      into v_grp_id
      from gl_acct_group_link_hdr gaglh, gl_acct_group_link_dtl gagld --CHANGED HERE
     where gaglh.ENABLED_FLAG = '1'
       and gaglh.acct_grp_lnk_id = gagld.acct_grp_lnk_id
       and gagld.acct_grp_id = p_grp_id;

    select cc.acct_grp_desc
      into v_list
      from GL_ACCT_GROUPS cc --CHANGED HERE
     where cc.acct_grp_id = v_grp_id;

    RETURN v_list;
  END Get_first_acct_Groups;

  FUNCTION Get_acct_codes_Id(p_grp_id IN VARCHAR2) RETURN clob IS
    v_list clob;
    -- v_list VARCHAR2(4000);
    CURSOR acc_cur(c_grp IN VARCHAR2) IS
      select acct_code, ACCT_CODE_DESC, ACCT_CODE_ID
        from gl_acct_codes
       where ENABLED_FLAG = '1'
         and acct_grp_id = c_grp;
    --order by ATTRIBUTE10;
  BEGIN
    for m in acc_cur(p_grp_id) Loop
      -- v_list:=''''||m.ACCT_CODE_ID||''''||','||''''||v_list;
      --  v_list:=''''||m.ACCT_CODE_ID||''''||','||''''||v_list;
      --  v_list:=''''||v_list||','||m.ACCT_CODE_ID||'''';
      --  v_list:=v_list||''''||','||''''||m.ACCT_CODE_ID||'''';
      v_list := v_list || '''' || ',' || '''' || chr(10) ||
                ltrim(rtrim(m.ACCT_CODE_ID));

    end loop;
    v_list := v_list || '''';
    v_list := substr(v_list, 3, length(v_list)); --(length(v_list)-2));
    --  v_list := ltrim(v_list, ',');
    --  dbms_output.put_line(v_list);
    RETURN v_list;
  END Get_acct_codes_Id;

  FUNCTION Get_acct_Groups_id(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list VARCHAR2(2000);
  BEGIN
    select cc.acct_grp_id
      into v_list
      from GL_ACCT_GROUPS cc
     where ENABLED_FLAG = '1'
       and cc.acct_grp_id = p_grp_id;

    RETURN v_list;
  END Get_acct_Groups_id;

  FUNCTION Get_first_acct_Groups_Id(p_grp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_list   VARCHAR2(2000);
    v_grp_id varchar2(50);
  BEGIN
    select gaglh.acct_grp_id
      into v_grp_id
      from gl_acct_group_link_hdr gaglh, gl_acct_group_link_dtl gagld
     where gaglh.acct_grp_lnk_id = gagld.acct_grp_lnk_id
       and gaglh.ENABLED_FLAG = '1'
       and gagld.acct_grp_id = p_grp_id;

    select cc.acct_grp_id
      into v_list
      from GL_ACCT_GROUPS cc
     where ENABLED_FLAG = '1'
       and cc.acct_grp_id = v_grp_id;

    RETURN v_list;
  END Get_first_acct_Groups_Id;

  PROCEDURE Get_Acct_Code_Based_Group(p_org_id     IN VARCHAR2,
                                      p_acc_grp_id IN VARCHAR2) is

    v_acc_grp_id  varchar2(4000);
    v_acc_code_id varchar2(4000);
    CURSOR acc_cur1 IS
      select GL_ACC_GRP_CODE.Get_acct_codes_Desc_Var(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                        '/'))),
                                                                        '/',
                                                                        -1,
                                                                        1),
                                                                  length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                         '/'))))),
                                                           '/')) as ACCT_CODE_DESC,
             GL_ACC_GRP_CODE.Get_acct_codes_Id_Var(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,

                                                                                                '/'))),
                                                                Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                      '/'))),
                                                                      '/',
                                                                      -1,
                                                                      1),
                                                                length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                       '/'))))),
                                                         '/')) as acct_code_id

        from gl_acct_group_link_hdr a,
             gl_acct_group_link_dtl b,
             GL_ACCT_GROUPS         C
       where c.enabled_flag = '1'
         and b.ACCT_GRP_LNK_ID = a.ACCT_GRP_LNK_ID
         and c.acct_grp_id = a.acct_grp_id
         and c.acct_org_id=p_org_id
            --   and a.created_by in ('SYSTEM1', 'SYSTEM2')
         and level <= 5
       start with a.acct_grp_id = p_acc_grp_id
      connect by prior b.acct_grp_id = a.acct_Grp_id
             and level <= 5;

    CURSOR acc_cur IS
      select m.acct_code,
             (m.acct_code || '-' || m.ACCT_CODE_DESC || '-' ||
             m.ATTRIBUTE10) as ACCT_CODE_DESC,
             m.ATTRIBUTE10,
             m.ACCT_CODE_ID
        from gl_acct_codes m
       where m.enabled_flag = '1'
         and m.acct_grp_id = p_acc_grp_id;

  begin
    delete from TEMP_GL_Acc_grp_code_hier;
    commit;

    for f_acc_cur in acc_cur loop
      v_acc_code_id := f_acc_cur.ACCT_CODE_ID;

      if (v_acc_code_id is not null) then
        BEGIN
          insert into TEMP_GL_Acc_grp_code_hier
            (LEVEL10, LEVEL10_ID, ACC_GRP_ID_VALUE)
          values
            (f_acc_cur.ACCT_CODE_DESC,
             f_acc_cur.acct_code_id,
             p_acc_grp_id);

        EXCEPTION
          WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('ERR1 ' || SQLERRM);
        END;
      end if;
    end loop;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Encounted an exception -' || sqlerrm);

  end Get_Acct_Code_Based_Group;

  PROCEDURE GL_ACC_GRP_LINK_Id_Name(p_org_id     IN VARCHAR2,
                                    p_acc_grp_id IN VARCHAR2) IS
    v_acc_grp_id varchar2(3000);

    CURSOR acc_cur IS
      select cc.ACCT_CODE_ID,
             cc.acct_code || '-' || cc.ACCT_CODE_DESC || '-' ||
             cc.ATTRIBUTE10 as acct_codes
        from gl_acct_codes cc
       where cc.ENABLED_FLAG = '1'
         and cc.acct_grp_id = v_acc_grp_id
       order by to_number(cc.ATTRIBUTE10) asc;

    CURSOR c_grp_code(v_mst_grp_id varchar2) is
      select GL_ACC_GRP_CODE.Get_first_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                        '/'))),
                                                                        1,
                                                                        19)),
                                                           '/')) as level1,
             GL_ACC_GRP_CODE.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  1,
                                                                  19)),
                                                     '/')) as level2,
             GL_ACC_GRP_CODE.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  21,
                                                                  19)),
                                                     '/')) as level3,
             GL_ACC_GRP_CODE.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  39,
                                                                  20)),
                                                     '/')) as level4,

             GL_ACC_GRP_CODE.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  59,
                                                                  30)),
                                                     '/')) as level5,

             GL_ACC_GRP_CODE.Get_acct_Groups(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  79,
                                                                  40)),
                                                     '/')) as level6,

             GL_ACC_GRP_CODE.Get_first_acct_Groups_Id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                           '/'))),
                                                                           1,
                                                                           19)),
                                                              '/')) as level1_Id,
             GL_ACC_GRP_CODE.Get_acct_Groups_id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                     '/'))),
                                                                     1,
                                                                     19)),
                                                        '/')) as level2_id,
             GL_ACC_GRP_CODE.Get_acct_Groups_id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                     '/'))),
                                                                     21,
                                                                     19)),
                                                        '/')) as level3_Id,
             GL_ACC_GRP_CODE.Get_acct_Groups_id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                     '/'))),
                                                                     39,
                                                                     20)),
                                                        '/')) as level4_Id,

             GL_ACC_GRP_CODE.Get_acct_Groups_id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                     '/'))),
                                                                     59,
                                                                     30)),
                                                        '/')) as level5_Id,

             GL_ACC_GRP_CODE.Get_acct_Groups_id(replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                     '/'))),
                                                                     79,
                                                                     40)),
                                                        '/')) as level6_Id,

             (replace(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                   '/'))),
                                   59,
                                   30)),
                      '/')) as acc_grp_id,

             GL_ACC_GRP_CODE.get_acct_codes_Id(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                            '/'))),
                                                            Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                  '/'))),
                                                                  '/',
                                                                  -1,
                                                                  1),
                                                            length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                   '/'))))),
                                                     '/')) as acct_codes_id,
             GL_ACC_GRP_CODE.get_acct_codes(ltrim(substr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                         '/'))),
                                                         Instr(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                               '/'))),
                                                               '/',
                                                               -1,
                                                               1),
                                                         length(ltrim(rtrim(SYS_CONNECT_BY_PATH(b.acct_grp_id,
                                                                                                '/'))))),
                                                  '/')) as acct_codes,
             LEVEL AS LEVEL7

        from gl_acct_group_link_hdr a,
             gl_acct_group_link_dtl b,
             GL_ACCT_GROUPS         C
       where c.ENABLED_FLAG = '1'
         and b.ACCT_GRP_LNK_ID = a.ACCT_GRP_LNK_ID
         and c.acct_grp_id = a.acct_grp_id
         and c.acct_org_id = p_org_id
            -- and a.created_by in ('SYSTEM1', 'SYSTEM2')
         and level <= 5
      --   and rownum<15
       start with a.acct_grp_id = v_mst_grp_id
      connect by prior b.acct_grp_id = a.acct_Grp_id
             and level <= 5
       order by LEVEL1, level2, level3, level4, level5, level6 asc;

  BEGIN
    delete from TEMP_GL_ACC_Id_Name;
    commit;

    for i in (select a.acct_grp_id
                from gl_acct_groups a
               where a.acct_org_id = p_org_id
                 AND not exists
               (select *
                        from gl_acct_group_link_dtl b
                       where a.acct_grp_id = b.acct_grp_id)) loop

      FOR f_grp_code IN c_grp_code(i.acct_grp_id) LOOP

        v_acc_grp_id := f_grp_code.acc_grp_id;

        if (v_acc_grp_id is not null) then

          FOR f_acc_cur IN acc_cur LOOP

            BEGIN
              insert into TEMP_GL_ACC_Id_Name
                (LEVEL1,
                 LEVEL2,
                 LEVEL3,
                 LEVEL4,
                 LEVEL5,
                 LEVEL6,
                 LEVEL7,
                 LEVEL8,
                 LEVEL9,
                 LEVEL10,
                 TOOLS,
                 LEVEL1_ID,
                 LEVEL2_ID,
                 LEVEL3_ID,
                 LEVEL4_ID,
                 LEVEL5_ID,
                 LEVEL6_ID,
                 LEVEL7_ID,
                 LEVEL8_ID,
                 LEVEL9_ID,
                 LEVEL10_ID,
                 ACC_GRP_ID_VALUE,
                 ACC_VALUE)
              values
                (f_grp_code.level1,
                 f_grp_code.level2,
                 f_grp_code.level3,
                 f_grp_code.level4,
                 f_grp_code.level5,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 f_grp_code.level6,
                 acc_grp_link_typ(f_acc_cur.acct_codes),
                 f_grp_code.level1_id,
                 f_grp_code.level2_id,
                 f_grp_code.level3_id,
                 f_grp_code.level4_id,
                 f_grp_code.level5_id,
                 f_grp_code.level6_id,
                 f_grp_code.level6_id,
                 f_grp_code.level6_id,
                 f_grp_code.level6_id,
                 f_grp_code.level6_id,
                 f_grp_code.acct_codes_id,
                 acc_grp_link_typ_id(f_grp_code.acct_codes_id));

            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERR1 ' || SQLERRM);
            END;
          END LOOP;

        else

          insert into TEMP_GL_ACC_Id_Name
            (LEVEL1,
             LEVEL2,
             LEVEL3,
             LEVEL4,
             LEVEL5,
             LEVEL6,
             LEVEL7,
             LEVEL8,
             LEVEL9,
             LEVEL10,
             TOOLS,
             LEVEL1_ID,
             LEVEL2_ID,
             LEVEL3_ID,
             LEVEL4_ID,
             LEVEL5_ID,
             LEVEL6_ID,
             LEVEL7_ID,
             LEVEL8_ID,
             LEVEL9_ID,
             LEVEL10_ID,
             ACC_GRP_ID_VALUE,
             ACC_VALUE)
          values
            (f_grp_code.level1,
             f_grp_code.level2,
             f_grp_code.level3,
             f_grp_code.level4,
             f_grp_code.level5,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             f_grp_code.level6,
             acc_grp_link_typ(f_grp_code.acct_codes),
             f_grp_code.level1_id,
             f_grp_code.level2_id,
             f_grp_code.level3_id,
             f_grp_code.level4_id,
             f_grp_code.level5_id,
             f_grp_code.level6_id,
             f_grp_code.level6_id, --7
             f_grp_code.level6_id, --8
             f_grp_code.level6_id, --9
             f_grp_code.level6_id, --10
             f_grp_code.acct_codes_id,
             acc_grp_link_typ_id(f_grp_code.acct_codes_id));

        end if;
      END LOOP;

    end loop;

    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Encounted an exception -' || sqlerrm);
  END GL_ACC_GRP_LINK_Id_Name;

END GL_ACC_GRP_CODE;
/
