CREATE OR REPLACE PACKAGE INV_PKG IS
--- ***************************************************************************************************************
   --- $Header: INV_PKG 1.0 2014-04-01 $
   --- ***************************************************************************************************************
   --- Program       :   INV_PKG
   --- Description          :   All  Inventory related codes goes here
   --- Author        :   C.S.Ilampooranan ,VMV Systems Pvt Ltd
   --- Date    :   01\04\2014
   --- Notes                :
   --- ****************************************************************************************************************
   --- Modification Log
   --- ------------------------------------
   --- Ver    Date                    Author                   Description
   --- ---    ----------   ------------------------ ---------------------------------------------------------
   --- 1.0    01\04\2014          Ilam
   --- ***************************************************************************************************************


TYPE t_rec_lotdtls IS RECORD (
    lot_id     VARCHAR2(50),
    lot_qty    NUMBER);

TYPE rec_tab IS TABLE OF t_rec_lotdtls INDEX BY VARCHAR2(50);

t_tab rec_tab;

FUNCTION item_validation(p_item_id VARCHAR2) RETURN BOOLEAN;

FUNCTION warehouses_validation(p_wh_id VARCHAR2) RETURN BOOLEAN;

FUNCTION item_txn_ledger_entry(p_item_id VARCHAR2,
                               p_wh_id VARCHAR2) RETURN NUMBER;--BOOLEAN;

FUNCTION item_current_balance(p_item_id VARCHAR2,
                              p_wh_id   VARCHAR2
                              ) RETURN NUMBER;
PROCEDURE inv_txn_ledger(p_tran_type       VARCHAR2,
                         p_inv_item_id     VARCHAR2,
                         p_qty             NUMBER,
                         p_wh_id           VARCHAR2,
                         p_trans_date      DATE,
                         p_receipt_id      VARCHAR2,
                         p_po_header_id    VARCHAR2,
                         p_po_line_id      VARCHAR2,
                         p_remarks         VARCHAR2);

PROCEDURE inv_item_transfer(p_trans_dt        DATE,
                            p_from_wh_id      VARCHAR2,
                            p_to_wh_id        VARCHAR2,
                            p_item_id         VARCHAR2,
                            p_item_qty        NUMBER,
                            p_from_acc_code   VARCHAR2,
                            p_to_acc_code     VARCHAR2,
              p_receipt_id      VARCHAR2,
                            p_lot_dtls        rec_tab);



FUNCTION item_warehouse_check(p_item_id VARCHAR2,
                               p_wh_id   VARCHAR2,
                 p_receipt_id VARCHAR2
                              ) RETURN NUMBER;


PROCEDURE inv_receipt_wh_item(p_item_id IN VARCHAR2,
                              p_wh_id IN VARCHAR2,
                p_receipt_id IN VARCHAR2,
                p_qty IN NUMBER,
                p_lot_dtls rec_tab);

PROCEDURE inv_item_rates(p_rate_date DATE,
                         p_item_id VARCHAR2,
                         p_price NUMBER,
                         p_currency varchar2 DEFAULT 'INR',
                         p_item_qty NUMBER);

FUNCTION get_inv_item_price(p_item_id VARCHAR2,
                            p_date DATE,
                            p_qty NUMBER) RETURN NUMBER;


END INV_PKG;
 
/
CREATE OR REPLACE PACKAGE BODY INV_PKG AS
--- ***************************************************************************************************************
   --- $Body: GL_PKG 1.0 2014-04-01 $
   --- ***************************************************************************************************************
   --- Program       :   INV_PKG
   --- Description          :   All  Inventory related codes goes here
   --- Author        :   C.S.Ilampooranan ,VMV Systems Pvt Ltd
   --- Date    :   01\04\2014
   --- Notes                :
   --- ****************************************************************************************************************
   --- Modification Log
   --- ------------------------------------
   --- Ver    Date                    Author                   Description
   --- ---    ----------   ------------------------ ---------------------------------------------------------
   --- 1.0    01\04\2014          Ilam
   --- ***************************************************************************************************************


FUNCTION item_validation(p_item_id VARCHAR2) RETURN BOOLEAN IS
    v_exists   NUMBER := 0;
BEGIN
    SELECT COUNT(1)
    INTO   v_exists
    FROM   inv_item_master iim
    WHERE  iim.item_id = p_item_id;

    IF     v_exists > 0 THEN
    RETURN TRUE;
    ELSE
    RETURN FALSE;
    END IF;

END item_validation;

FUNCTION warehouses_validation(p_wh_id VARCHAR2) RETURN BOOLEAN IS
    v_exists   NUMBER := 0;
BEGIN
    SELECT COUNT(1)
    INTO   v_exists
    FROM   inv_warehouses iw
    WHERE  iw.inv_wh_id = p_wh_id;

    IF     v_exists > 0 THEN
    RETURN TRUE;
    ELSE
    RETURN FALSE;
    END IF;

END warehouses_validation;

FUNCTION item_txn_ledger_entry(p_item_id VARCHAR2,
                               p_wh_id VARCHAR2) RETURN NUMBER IS
  v_count NUMBER:=0;
BEGIN
  SELECT COUNT(*)
    INTO v_count
    FROM INV_TRANSACTION_LEDGER
  WHERE   INV_ITEM_ID = p_item_id
    AND     INV_WH_ID   = p_wh_id;
  /*
  IF v_count>0 THEN
   RETURN TRUE;
  ELSE
   RETURN FALSE;
  END IF;
  */
  RETURN v_count;
END item_txn_ledger_entry;


FUNCTION item_warehouse_check(p_item_id VARCHAR2,
                               p_wh_id   VARCHAR2,
                 p_receipt_id VARCHAR2
                              ) RETURN NUMBER IS
v_item_exists NUMBER(10):=0;
BEGIN
  SELECT count('*')
    INTO v_item_exists
  FROM INV_RECEIPT_ITEM_WH_HDR
  WHERE ITEM_ID=p_item_id
    AND WH_ID=p_wh_id
    AND RECEIPT_ID=NVL(p_receipt_id,RECEIPT_ID);

    IF v_item_exists>0 THEN
      RETURN 1;
    ELSE
      RETURN 0;
    END IF;
END item_warehouse_check;


FUNCTION item_current_balance(p_item_id VARCHAR2,
                              p_wh_id   VARCHAR2
                              ) RETURN NUMBER IS
    v_inv_item_open_balance   NUMBER := 0;
BEGIN
    BEGIN
    SELECT  lti.inv_qoh
    INTO    v_inv_item_open_balance
    FROM    inv_transaction_ledger lti
    WHERE   lti.inv_tran_id =
    (
    SELECT  MAX(itl.inv_tran_id)
    FROM    inv_transaction_ledger itl
    WHERE   itl.inv_item_id = p_item_id
    AND     itl.inv_wh_id   = p_wh_id
    );
    EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_inv_item_open_balance:=0;
    --debug_proc('itembal '||sqlerrm);
    WHEN OTHERS THEN
         v_inv_item_open_balance := 0;
    END;


    RETURN(v_inv_item_open_balance);


END item_current_balance;

PROCEDURE inv_receipt_wh_item(p_item_id IN VARCHAR2,
                              p_wh_id IN VARCHAR2,
                p_receipt_id IN VARCHAR2,
                p_qty IN NUMBER,
                p_lot_dtls rec_tab) IS
v_trans_id VARCHAR2(50);
BEGIN
  v_trans_id:=ssm.get_next_sequence('AP_016_M','EN');
  INSERT INTO INV_RECEIPT_ITEM_WH_HDR(PK_ID,
                    CHILD_ID,
                    INV_RECEIPT_ITEM_WH_ID,
                    ITEM_ID,
                    WH_ID,
                    RECEIPT_ID,
                    WH_QTY,
                    ENABLED_FLAG,
                    WORKFLOW_COMPLETION_STATUS,
                    CREATED_BY,
                    CREATED_DATE)
                  VALUES(1,
                       1,
                     v_trans_id,
                     p_item_id,
                     p_wh_id,
                     p_receipt_id,
                     p_qty,
                     '1',
                     '1',
                     'ADMIN',
                     SYSDATE);

  FOR m_count IN 1..p_lot_dtls.count LOOP
    INSERT INTO INV_RECEIPT_ITEM_WH_DTL(PK_ID,
                      CHILD_ID,
                      INV_RECEIPT_ITEM_WH_ID,
                      LOT_ID,
                      LOT_QTY,
                      ENABLED_FLAG,
                      WORKFLOW_COMPLETION_STATUS,
                      CREATED_BY,
                      CREATED_DATE)
                   VALUES(1,
                          1,
                      v_trans_id,
                      p_lot_dtls(m_count).LOT_ID,
                      p_lot_dtls(m_count).LOT_QTY,
                      '1',
                      '1',
                      'ADMIN',
                      SYSDATE);
    END LOOP;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('ERR2.1'||SQLERRM);
END inv_receipt_wh_item;

PROCEDURE inv_txn_ledger(p_tran_type       VARCHAR2,
                         p_inv_item_id     VARCHAR2,
                         p_qty             NUMBER,
                         p_wh_id           VARCHAR2,
                         p_trans_date      DATE,
                         p_receipt_id      VARCHAR2,
                         p_po_header_id    VARCHAR2,
                         p_po_line_id      VARCHAR2,
                         p_remarks         VARCHAR2) IS

    v_tran_type              VARCHAR2(100);
    v_inv_qoh                NUMBER := 0;
    v_inv_item_open_balance  NUMBER := 0;

BEGIN
  -- debug_proc('1');
   IF  item_validation(p_inv_item_id) AND warehouses_validation(p_wh_id) THEN
   --debug_proc('2');
   v_inv_item_open_balance := item_current_balance(p_inv_item_id,p_wh_id);

  --debug_proc('3 -' ||v_inv_item_open_balance);

  IF p_tran_type IN ('RECEIVE','ADJ') THEN
      -- debug_proc('4');
     v_tran_type := p_tran_type;
       v_inv_qoh   := v_inv_item_open_balance + p_qty;
    ELSIF p_tran_type = 'ISSUE' THEN
      -- debug_proc('5');
     v_tran_type := p_tran_type;
       v_inv_qoh   := v_inv_item_open_balance - p_qty;
    ELSE
       --debug_proc('6');
     v_tran_type := 'OPENBAL';
       v_inv_item_open_balance := 0;
       v_inv_qoh               := p_qty+v_inv_item_open_balance;
    END IF;
   -- debug_proc('7');
    INSERT INTO inv_transaction_ledger    (
                                          pk_id,
                                          child_id,
                                          inv_tran_id,
                                          inv_trans_type,
                                          inv_item_id,
                                          inv_qty,
                                          receipt_id,
                                          inv_po_header_id,
                                          inv_po_line_id,
                                          inv_trans_date,
                                          inv_trans_remarks,
                                          inv_wh_id,
                                          inv_item_open_balance,
                                          inv_qoh,
                                          enabled_flag,
                                          workflow_completion_status,
                                          created_by,
                                          created_date,
                                          modified_by,
                                          modified_date,
                                          attribute1,
                                          attribute2,
                                          attribute4,
                                          attribute5,
                                          attribute6,
                                          attribute7,
                                          attribute8,
                                          attribute9,
                                          attribute10
                                          )
                                          VALUES
                                          (
                                          1,--ssm.get_next_sequence('PKID','EN'),--pk_id
                                          1,--ssm.get_next_sequence('CHILDID','EN'),--child_id
                                          ssm.get_next_sequence('INVTRAN','EN'),--inv_tran_id
                                          v_tran_type,--inv_trans_type
                                          p_inv_item_id,--inv_item_id
                                          p_qty,--NULL,--inv_qty
                                          p_receipt_id,--receipt_id
                                          p_po_header_id,--inv_po_header_id
                                          p_po_line_id,--inv_po_line_id
                                          p_trans_date,--inv_trans_date
                                          p_remarks,--inv_trans_remarks
                                          p_wh_id,--inv_wh_id
                                          v_inv_item_open_balance,--inv_item_open_balance
                                          v_inv_qoh,--inv_qoh
                                          '1',--enabled_flag
                                          '1',--workflow_completion_status
                                          'SYSADMIN',--created_by
                                          SYSDATE,--created_date
                                          NULL,--modified_by
                                          NULL,--modified_date
                                          NULL,--attribute1
                                          NULL,--attribute2
                                          NULL,--attribute4
                                          NULL,--attribute5
                                          NULL,--attribute6
                                          NULL,--attribute7
                                          NULL,--attribute8
                                          NULL,--attribute9
                                          NULL--attribute10
                                          );
  --debug_proc('8');
  COMMIT;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
   -- DEBUG_PROC('OTHERS '||SUBSTR(SQLERRM,1,2000));
   NULL;
END;

PROCEDURE inv_item_transfer       (
                                  p_trans_dt        DATE,
                                  p_from_wh_id      VARCHAR2,
                                  p_to_wh_id        VARCHAR2,
                                  p_item_id         VARCHAR2,
                                  p_item_qty        NUMBER,
                                  p_from_acc_code   VARCHAR2,
                                  p_to_acc_code     VARCHAR2,
                  p_receipt_id      VARCHAR2,
                                  p_lot_dtls        rec_tab
                                  ) IS
   r_tab rec_tab;
   v_item_exists NUMBER(10):=0;
   v_inv_trans_id VARCHAR2(50);
   v_inv_wh_item_id_f VARCHAR2(50);
   v_inv_wh_item_id_t VARCHAR2(50);
   v_lot_exists NUMBER(10):=0;
BEGIN
    --v_item_exists:=
  debug_proc('Start');
  debug_proc('-------Parameters start----------');
  debug_proc('Trans date '||to_char(p_trans_dt,'DD-MON-YY'));
  debug_proc('From warehouse '||p_from_wh_id);
  debug_proc('To warehouse '||p_to_wh_id);
  debug_proc('p_item_id '||p_item_id);
  debug_proc('p_item_qty '||p_item_qty);
  debug_proc('p_receipt_id '||p_receipt_id);
  debug_proc('p_lot_dtls '||p_lot_dtls.count);
  r_tab:=p_lot_dtls;
  debug_proc('r_tab '||r_tab.count);
  debug_proc('-------Parameters End----------');
  BEGIN
  debug_proc('1');
  v_inv_trans_id:=ssm.get_next_sequence('INVMOV','EN');
  debug_proc('2');
    INSERT INTO inv_transfer_hdr  (
                                  pk_id,
                                  child_id,
                                  inv_trans_id,
                                  inv_trans_date,
                                  inv_from_wh_id,
                                  inv_to_wh_id,
                                  inv_from_loc_id,
                                  inv_to_loc_id,
                                  inv_item_id,
                                  inv_item_qty,
                                  inv_trans_reason,
                                  inv_trans_from_acc,
                                  inv_trans_to_acc,
                                  enabled_flag,
                                  workflow_completion_status,
                                  created_by,
                                  created_date,
                                  modified_by,
                                  modified_date,
                                  attribute1,
                                  attribute2,
                                  attribute3,
                                  attribute4,
                                  attribute5,
                                  attribute6,
                                  attribute7,
                                  attribute8,
                                  attribute9,
                                  attribute10
                                  )
                                  VALUES
                                  (
                                  1,--ssm.get_next_sequence('PKID','EN'),--pk_id
                                  1,--ssm.get_next_sequence('CHILDID','EN'),--child_id
                                  v_inv_trans_id,--inv_trans_id
                                  p_trans_dt,--inv_trans_date
                                  p_from_wh_id,--inv_from_wh_id
                                  p_to_wh_id,--inv_to_wh_id
                                  NULL,--inv_from_loc_id
                                  NULL,--inv_to_loc_id
                                  p_item_id,--inv_item_id
                                  p_item_qty,--inv_item_qty
                                  'Transfer of '||p_item_id||' - From '||p_from_wh_id||'- To - '||p_to_wh_id,--inv_trans_reason
                                  p_from_acc_code,--inv_trans_from_acc
                                  p_to_acc_code,--inv_trans_to_acc
                                  '1',--enabled_flag
                                  '1',--workflow_completion_status
                                  'SYSADMIN',--created_by
                                  SYSDATE,--created_date
                                  NULL,--modified_by
                                  NULL,--modified_date
                                  NULL,--attribute1
                                  NULL,--attribute2
                                  NULL,--attribute3
                                  NULL,--attribute4
                                  NULL,--attribute5
                                  NULL,--attribute6
                                  NULL,--attribute7
                                  NULL,--attribute8
                                  NULL,--attribute9
                                  NULL--attribute10
                                  );
  debug_proc('3');
    EXCEPTION
    WHEN OTHERS THEN
      debug_proc('ERR1 '||SQLERRM);
    dbms_output.put_line('ERR1 '||SQLERRM);
  END;

  debug_proc('4');
  FOR m_count IN 1..r_tab.COUNT LOOP
    BEGIN
  INSERT INTO inv_transfer_dtl  (
                                  pk_id,
                                  child_id,
                                  inv_trans_dtl_id,
                                  inv_trans_id,
                                  inv_lot_id,
                                  inv_item_qty,
                                  enabled_flag,
                                  workflow_completion_status,
                                  created_by,
                                  created_date,
                                  modified_by,
                                  modified_date,
                                  attribute1,
                                  attribute2,
                                  attribute3,
                                  attribute4,
                                  attribute5,
                                  attribute6,
                                  attribute7,
                                  attribute8,
                                  attribute9,
                                  attribute10
                                  )
                                  VALUES
                                  (
                                  1,--ssm.get_next_sequence('PKID','EN'),--pk_id
                                  1,--ssm.get_next_sequence('CHILDID','EN'),--child_id
                                  ssm.get_next_sequence('INVMOVL','EN'),--inv_trans_dtl_id
                                  v_inv_trans_id,--inv_trans_id
                                 r_tab(m_count).lot_id,--inv_lot_id
                                 r_tab(m_count).LOT_QTY,--inv_item_qty
                                  '1',--enabled_flag
                                  '1',--workflow_completion_status
                                  'SYSADMIN',--created_by
                                  SYSDATE,--created_date
                                  NULL,--modified_by
                                  NULL,--modified_date
                                  NULL,--attribute1
                                  NULL,--attribute2
                                  NULL,--attribute3
                                  NULL,--attribute4
                                  NULL,--attribute5
                                  NULL,--attribute6
                                  NULL,--attribute7
                                  NULL,--attribute8
                                  NULL,--attribute9
                                  NULL--attribute10
                                  );

  EXCEPTION
    WHEN OTHERS THEN
      debug_proc('err2 '||substr(sqlerrm,1,1000));
      dbms_output.put_line('ERR2 '||SQLERRM);
  END;
    END LOOP;
  debug_proc('5');

  IF item_warehouse_check(p_item_id,p_to_wh_id,NULL)=1 AND item_warehouse_check(p_item_id,p_to_wh_id,p_receipt_id)=1 THEN
     debug_proc('1-1');

     --Reduce Qty from INV_RECEIPT_ITEM_WH_HDR for source warehouse
     UPDATE INV_RECEIPT_ITEM_WH_HDR
           SET WH_QTY=WH_QTY-p_item_qty
         WHERE ITEM_ID=p_item_id
       AND WH_ID=p_from_wh_id
       AND RECEIPT_ID=p_receipt_id RETURNING INV_RECEIPT_ITEM_WH_ID INTO v_inv_wh_item_id_f;
    debug_proc('1-2');

     --Increase Qty from INV_RECEIPT_ITEM_WH_HDR for destination warehouse
     UPDATE INV_RECEIPT_ITEM_WH_HDR
           SET WH_QTY=WH_QTY+p_item_qty
         WHERE ITEM_ID=p_item_id
       AND WH_ID=p_to_wh_id
       AND RECEIPT_ID=p_receipt_id RETURNING INV_RECEIPT_ITEM_WH_ID INTO v_inv_wh_item_id_t;
    debug_proc('1-3');

    --Reduce Qty from INV_RECEIPT_ITEM_WH_DTL for source warehouse
    FOR m_count IN 1..r_tab.COUNT LOOP
          debug_proc('1-3-1');
      UPDATE INV_RECEIPT_ITEM_WH_DTL
        SET LOT_QTY=LOT_QTY-r_tab(m_count).LOT_QTY
      WHERE INV_RECEIPT_ITEM_WH_ID=v_inv_wh_item_id_f
        AND LOT_ID= r_tab(m_count).LOT_ID;
      debug_proc('1-3-2');
          SELECT COUNT(*)
            INTO v_lot_exists
            FROM INV_RECEIPT_ITEM_WH_DTL
           WHERE INV_RECEIPT_ITEM_WH_ID=v_inv_wh_item_id_t
             AND LOT_ID=r_tab(m_count).LOT_ID;
          debug_proc('1-3-3');
           IF v_lot_exists>0 THEN
             debug_proc('1-3-3A');
       UPDATE INV_RECEIPT_ITEM_WH_DTL
        SET LOT_QTY=LOT_QTY+r_tab(m_count).LOT_QTY
       WHERE INV_RECEIPT_ITEM_WH_ID=v_inv_wh_item_id_t
         AND LOT_ID= r_tab(m_count).LOT_ID;
             v_lot_exists:=0;
           ELSE
             debug_proc('1-3-3B');
       BEGIN
                 INSERT INTO INV_RECEIPT_ITEM_WH_DTL(PK_ID,
                      CHILD_ID,
                      INV_RECEIPT_ITEM_WH_ID,
                      LOT_ID,
                      LOT_QTY,
                      ENABLED_FLAG,
                      WORKFLOW_COMPLETION_STATUS,
                      CREATED_BY,
                      CREATED_DATE)
                   VALUES(1,
                          1,
                      v_inv_wh_item_id_t,
                      r_tab(m_count).LOT_ID,
                      r_tab(m_count).LOT_QTY,
                      '1',
                      '1',
                      'ADMIN',
                      SYSDATE);
              v_lot_exists:=0;
             EXCEPTION
              WHEN OTHERS THEN
         debug_proc('1-3-3'||SUBSTR(SQLERRM,1,1000));
               DBMS_OUTPUT.PUT_LINE('ERR3 '||SQLERRM);
         v_lot_exists:=0;
             END;
          END IF;
      debug_proc('1-3-4');
        END LOOP;
    debug_proc('1-4');
  ELSE --item_warehouse_check(p_item_id,p_to_wh_id,NULL)=1 AND item_warehouse_check(p_item_id,p_to_wh_id,p_receipt_id)=0 THEN
     debug_proc('1-0');
     inv_receipt_wh_item(p_item_id    =>p_item_id,
                           p_wh_id      =>p_to_wh_id,
               p_receipt_id =>p_receipt_id,
               p_qty        =>p_item_qty,
               p_lot_dtls   =>r_tab);
  END IF;


  debug_proc('6');
  --Start Transaction Ledger Entry
  BEGIN
    inv_pkg.inv_txn_ledger(p_tran_type    =>'ISSUE',
               p_inv_item_id  =>p_item_id,
               p_qty          =>p_item_qty,
               p_wh_id        =>p_from_wh_id,
               p_trans_date   =>p_trans_dt,
               p_receipt_id   =>p_receipt_id,
               p_po_header_id =>NULL,
               p_po_line_id   =>NULL,
               p_remarks      =>'Transfer of '||p_item_id||' - From '||p_from_wh_id||'- To - '||p_to_wh_id);
    EXCEPTION
      WHEN OTHERS THEN
        debug_proc('err1.1 '||substr(sqlerrm,1,1000));
      DBMS_OUTPUT.PUT_LINE('ERR1.1 '||SQLERRM);
    END;
  debug_proc('7');
    BEGIN
    inv_pkg.inv_txn_ledger(p_tran_type    =>'RECEIVE',
               p_inv_item_id  =>p_item_id,
               p_qty          =>p_item_qty,
               p_wh_id        =>p_to_wh_id,
               p_trans_date   =>p_trans_dt,
               p_receipt_id   =>p_receipt_id,
               p_po_header_id =>NULL,
               p_po_line_id   =>NULL,
               p_remarks      =>'Transfer of '||p_item_id||' - From '||p_from_wh_id||'- To - '||p_to_wh_id);
    EXCEPTION
      WHEN OTHERS THEN
        debug_proc('err1.2 '||substr(sqlerrm,1,1000));
      DBMS_OUTPUT.PUT_LINE('ERR1.2 '||SQLERRM);
    END;
  --End Transaction Ledger Entry
  debug_proc('8');
  debug_proc('End');
  COMMIT;

END inv_item_transfer;

PROCEDURE inv_item_rates(p_rate_date DATE,
                         p_item_id VARCHAR2,
                         p_price NUMBER,
                         p_currency varchar2 DEFAULT 'INR',
                         p_item_qty NUMBER) IS
BEGIN
  INSERT INTO INV_ITEM_RATES(PK_ID,
               CHILD_ID,
               RATE_ID,
               RATE_DATE,
               RATE_ITEM_ID,
               RATE_PRICE,
               RATE_CURRENCY,
               RATE_ITEM_QTY,
               ENABLED_FLAG,
               WORKFLOW_COMPLETION_STATUS,
               CREATED_BY,
               CREATED_DATE)
            VALUES(1,
                   1,
               ssm.get_next_sequence('ITMRATE','EN'),
               p_rate_date,
               p_item_id,
               p_price,
               p_currency,
               p_item_qty,
               '1',
               '1',
               'ADMIN',
               SYSDATE);
COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('ERR4 '||SQLERRM);
END inv_item_rates;

FUNCTION get_inv_item_price(p_item_id VARCHAR2,
                            p_date DATE,
                            p_qty NUMBER) RETURN NUMBER IS
 v_tot_cost NUMBER:=0;
 v_qty NUMBER;
 CURSOR lifo_item_cur(c_item_id VARCHAR2,
                c_date DATE) IS
    SELECT * FROM INV_ITEM_RATES
       WHERE RATE_ITEM_ID=c_item_id
         AND RATE_DATE<=c_date
     AND NVL(RATE_ITEM_QTY,0)>0
     ORDER BY RATE_DATE DESC;

 CURSOR fifo_item_cur(c_item_id VARCHAR2,
                c_date DATE) IS
    SELECT * FROM INV_ITEM_RATES
       WHERE RATE_ITEM_ID=c_item_id
         AND RATE_DATE>=c_date
     AND NVL(RATE_ITEM_QTY,0)>0
     ORDER BY RATE_DATE ASC;
BEGIN
  v_qty:=p_qty;
  debug_proc('Parameters '||p_item_id||' - '||to_char(p_date,'DD-MON-YY')||' - '||p_qty);
  debug_proc('Sys parameter '||ssm.get_parameter_value('ITMRATE'));
  IF ssm.get_parameter_value('ITMRATE')='LIFO' THEN
    FOR m_cur IN lifo_item_cur(p_item_id,p_date) LOOP
    debug_proc('Values '||m_cur.RATE_ITEM_ID ||' -Date '||to_char(m_cur.RATE_DATE)||' - Price '||m_cur.RATE_PRICE);
    IF v_qty>m_cur.RATE_ITEM_QTY THEN
       v_qty:=v_qty-m_cur.RATE_ITEM_QTY;
     debug_proc('If part '||v_qty);
      v_tot_cost:=v_tot_cost+(m_cur.RATE_ITEM_QTY*m_cur.RATE_PRICE);

      UPDATE INV_ITEM_RATES
         SET RATE_ITEM_SOLD_QTY=m_cur.RATE_ITEM_QTY
       WHERE RATE_ID=m_cur.RATE_ID;

    ELSIF v_qty<m_cur.RATE_ITEM_QTY THEN
      --v_qty:=m_cur.RATE_ITEM_QTY-v_qty;
    debug_proc('Else part '||v_qty);
    v_tot_cost:=v_tot_cost+(v_qty*m_cur.RATE_PRICE);
    UPDATE INV_ITEM_RATES
         SET RATE_ITEM_SOLD_QTY=v_qty
       WHERE RATE_ID=m_cur.RATE_ID;

    END IF;
    IF v_qty<=0 THEN
      RETURN v_tot_cost;
      EXIT;
    END IF;
    END LOOP;
  ELSIF ssm.get_parameter_value('ITMRATE')='FIFO' THEN
  NULL;
  ELSIF ssm.get_parameter_value('ITMRATE')='AVG' THEN
  NULL;
  END IF;
   debug_proc('Good Selling Price '||v_tot_cost);
  COMMIT;
 RETURN 0;
EXCEPTION
  WHEN OTHERS THEN
   debug_proc('erro fun '||substr(sqlerrm,1,2000));
END get_inv_item_price;

end INV_PKG;
/
