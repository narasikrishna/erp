CREATE OR REPLACE PACKAGE PKG_VALIDATIONS IS

  --- ***************************************************************************************************************
  --- $Body: PKG_VALLIDATIONS 1.0 2014-05-19 $
  --- ***************************************************************************************************************
  --- Program      :   PKG_VALLIDATIONS
  --- Description  :   All  Basic Validations related codes goes here
  --- Author       :   Ram A ,VMV Systems Pvt Ltd
  --- Date   :   13\05\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    19\05\2014          Ram
  --- ***************************************************************************************************************

  -- GL
  FUNCTION FUNC_DUPLICATE_MGR_CURR_CODE(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_CURRENCY_CODE IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_CURRENCY_CODE(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_CURRENCY_CODE IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2,
                                       P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_ACC_GROUP(P_MODULE_CODE    IN VARCHAR2,
                                        P_SCREEN_CODE    IN VARCHAR2,
                                        P_ACC_GROUP_NAME IN VARCHAR2,
                                        P_RECORD_ID      IN VARCHAR2)

   RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_GROUP(P_MODULE_CODE    IN VARCHAR2,
                                   P_SCREEN_CODE    IN VARCHAR2,
                                   P_ACC_GROUP_NAME IN VARCHAR2,
                                   P_RECORD_ID      IN VARCHAR2,
                                   P_FROM_DATE      IN VARCHAR2,
                                   P_TO_DATE        IN VARCHAR2,
                                   P_RET            OUT VARCHAR2);

  FUNCTION FUNC_EFF_DATE_VALIDATOR(P_FROM_DATE IN DATE, P_TO_DATE IN DATE)
    RETURN NUMBER;

  FUNCTION FUNC_DUPLICATE_SEGMENT(P_MODULE_CODE  IN VARCHAR2,
                                  P_SCREEN_CODE  IN VARCHAR2,
                                  P_SEGMENT_NAME IN VARCHAR2,
                                  P_RECORD_ID    IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_SEGMENT(P_MODULE_CODE  IN VARCHAR2,
                                 P_SCREEN_CODE  IN VARCHAR2,
                                 P_SEGMENT_NAME IN VARCHAR2,
                                 P_RECORD_ID    IN VARCHAR2,
                                 P_FROM_DATE    IN VARCHAR2,
                                 P_TO_DATE      IN VARCHAR2,
                                 P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_ACC_STRUCTURE(P_MODULE_CODE      IN VARCHAR2,
                                        P_SCREEN_CODE      IN VARCHAR2,
                                        P_ACCT_STRUCT_NAME IN VARCHAR2,
                                        P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_STRUCTURE(P_MODULE_CODE      IN VARCHAR2,
                                       P_SCREEN_CODE      IN VARCHAR2,
                                       P_ACCT_STRUCT_NAME IN VARCHAR2,
                                       P_RECORD_ID        IN VARCHAR2,
                                       P_FROM_DATE        IN VARCHAR2,
                                       P_TO_DATE          IN VARCHAR2,
                                       P_RET              OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_ACC_GRP_DFL_SEG(P_MODULE_CODE     IN VARCHAR2,
                                          P_SCREEN_CODE     IN VARCHAR2,
                                          P_ACCT_GRP_ID     IN VARCHAR2,
                                          P_ACCT_SEGMENT_ID IN VARCHAR2,
                                          P_RECORD_ID       IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_GRP_DEF_SEG(P_MODULE_CODE     IN VARCHAR2,
                                         P_SCREEN_CODE     IN VARCHAR2,
                                         P_ACCT_GRP_ID     IN VARCHAR2,
                                         P_ACCT_SEGMENT_ID IN VARCHAR2,
                                         P_RECORD_ID       IN VARCHAR2,
                                         P_FROM_DATE       IN VARCHAR2,
                                         P_TO_DATE         IN VARCHAR2,
                                         P_RET             OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_ACCOUNT_CODE(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_ACC_STRUCTURE IN VARCHAR2,
                                       P_ACC_GROUP     IN VARCHAR2,
                                       P_ACCT_CODE     IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2)

   RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_CODE(P_MODULE_CODE IN VARCHAR2,

                                  P_SCREEN_CODE   IN VARCHAR2,
                                  P_ACC_STRUCTURE IN VARCHAR2,
                                  P_ACC_GROUP     IN VARCHAR2,
                                  P_ACCT_CODE     IN VARCHAR2,
                                  P_RECORD_ID     IN VARCHAR2,
                                  P_FROM_DATE     IN VARCHAR2,
                                  P_TO_DATE       IN VARCHAR2,
                                  P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLI_MGR_ACC_CALR_DTL(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       p_ACC_YR      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_CALENDAR_DTL(P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          p_ACC_YR      IN VARCHAR2,
                                          P_RECORD_ID   IN VARCHAR2,
                                          P_FROM_DATE   IN VARCHAR2,
                                          P_TO_DATE     IN VARCHAR2,
                                          P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_ACC_CALNDR(P_MODULE_CODE   IN VARCHAR2,
                                         P_SCREEN_CODE   IN VARCHAR2,
                                         P_CALENDAR_NAME IN VARCHAR2,
                                         P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ACC_CALENDAR(P_MODULE_CODE   IN VARCHAR2,
                                      P_SCREEN_CODE   IN VARCHAR2,
                                      P_CALENDAR_NAME IN VARCHAR2,
                                      P_RECORD_ID     IN VARCHAR2,
                                      P_FROM_DATE     IN VARCHAR2,
                                      P_TO_DATE       IN VARCHAR2,
                                      P_RET           OUT VARCHAR2);

  PROCEDURE PROC_ERR_MGR_ACC_GRP_LINK(P_FROM_DATE IN VARCHAR2,
                                      P_TO_DATE   IN VARCHAR2,
                                      P_RET       OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_ORG_INTR_NM(P_MODULE_CODE   IN VARCHAR2,
                                          P_SCREEN_CODE   IN VARCHAR2,
                                          P_INTERNAL_NAME IN VARCHAR2,
                                          P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  FUNCTION FUNC_DUPLICATE_MGR_ORG_SHRT_NM(P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          P_SHORT_NAME  IN VARCHAR2,
                                          P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ORGANIZATION(P_MODULE_CODE   IN VARCHAR2,
                                      P_SCREEN_CODE   IN VARCHAR2,
                                      P_INTERNAL_NAME IN VARCHAR2,
                                      P_SHORT_NAME    IN VARCHAR2,
                                      P_RECORD_ID     IN VARCHAR2,
                                      P_RET           OUT VARCHAR2);

  --AP

  FUNCTION FUNC_DUPLICATE_MGR_TAX_TERM(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_TAX_NAME    IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_TAX_TERM(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_TAX_NAME    IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_FROM_DATE   IN VARCHAR2,
                                  P_TO_DATE     IN VARCHAR2,
                                  P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_CATOG(P_MODULE_CODE   IN VARCHAR2,
                                         P_SCREEN_CODE   IN VARCHAR2,
                                         P_ITEM_CAT_NAME IN VARCHAR2,
                                         P_ORG_ID        IN VARCHAR2,
                                         P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ITEM_CATOG(P_MODULE_CODE   IN VARCHAR2,
                                    P_SCREEN_CODE   IN VARCHAR2,
                                    P_ITEM_CAT_NAME IN VARCHAR2,
                                    P_ORG_ID        IN VARCHAR2,
                                    P_RECORD_ID     IN VARCHAR2,
                                    P_FROM_DATE     IN VARCHAR2,
                                    P_TO_DATE       IN VARCHAR2,
                                    P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_UOM_MST(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_UOM_CODE    IN VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  FUNCTION FUNC_DUPLICATE_MGR_UOM_BASE(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_UOM_BASE    IN VARCHAR2,
                                       P_ORG_ID      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGER_UOM_MST(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_UOM_CODE    IN VARCHAR2,
                                  P_UOM_BASE    IN VARCHAR2,
                                  P_ORG_ID      IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_UOM_CONV(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_UOM_FROM    IN VARCHAR2,
                                       P_UOM_TO      IN VARCHAR2,
                                       P_ORG_ID      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_UOM_CONV_VALIDATOR(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_UOM_FROM    IN VARCHAR2,
                                    P_UOM_TO      IN VARCHAR2,
                                    P_ORG_ID      IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_SUPPLIER(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_VENDOR_CODE IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_SUPPLIER(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_VENDOR_CODE IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_SUP_BRANCH(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_BRANCH_CODE IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_SUP_BRANCH(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_BRANCH_CODE IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_PO(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_PO_NUMBER   IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_PO(P_MODULE_CODE IN VARCHAR2,
                            P_SCREEN_CODE IN VARCHAR2,
                            P_GRN_NUMBER  IN VARCHAR2,
                            P_RECORD_ID   IN VARCHAR2,
                            P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_PO_RECP(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_GRN_NUMBER  IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_PO_RECP(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_GRN_NUMBER  IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2,
                                 P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_RECP_LOT(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_LOT_NUMBER  IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_RECP_LOT(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_LOT_NUMBER  IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2);

  FUNCTION FUNC_LEAVE_DATE_MGR(P_MODULE_CODE            IN VARCHAR2,
                               P_SCREEN_CODE            IN VARCHAR2,
                               P_LEAVE_REQ_ID           IN VARCHAR2,
                               P_LEAVE_CANCELATION_DATE IN DATE)
    RETURN NUMBER;

  PROCEDURE PROC_LEAVE_DATE_MGR(P_MODULE_CODE            IN VARCHAR2,
                                P_SCREEN_CODE            IN VARCHAR2,
                                P_LEAVE_REQ_ID           IN VARCHAR2,
                                P_LEAVE_CANCELATION_DATE IN VARCHAR2,
                                P_RET                    OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_CATEGORY(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_CATEGORY_CODE IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_CATEGORY(P_MODULE_CODE   IN VARCHAR2,
                                  P_SCREEN_CODE   IN VARCHAR2,
                                  P_CATEGORY_CODE IN VARCHAR2,
                                  P_RECORD_ID     IN VARCHAR2,
                                  P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_GRADE(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_CATEGORY_ID IN VARCHAR2,
                                    P_GRADE_CODE  IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_GRADE(P_MODULE_CODE IN VARCHAR2,
                               P_SCREEN_CODE IN VARCHAR2,
                               P_CATEGORY_ID IN VARCHAR2,
                               P_GRADE_CODE  IN VARCHAR2,
                               P_RECORD_ID   IN VARCHAR2,
                               P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_JOBS(P_MODULE_CODE IN VARCHAR2,
                                   P_SCREEN_CODE IN VARCHAR2,
                                   P_JOB_CODE    IN VARCHAR2,
                                   P_RECORD_ID   IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_JOBS(P_MODULE_CODE IN VARCHAR2,
                              P_SCREEN_CODE IN VARCHAR2,
                              P_JOB_CODE    IN VARCHAR2,
                              P_RECORD_ID   IN VARCHAR2,
                              P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_JOB_RESP(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_JOB_ID      IN VARCHAR2,
                                       P_JR_TYPE     IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_JOB_RESP(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_JOB_ID      IN VARCHAR2,
                                  P_JR_TYPE     IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_POSITIONS(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_JOB_ID        IN VARCHAR2,
                                        P_POSITION_CODE IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_POSITIONS(P_MODULE_CODE   IN VARCHAR2,
                                   P_SCREEN_CODE   IN VARCHAR2,
                                   P_JOB_ID        IN VARCHAR2,
                                   P_POSITION_CODE IN VARCHAR2,
                                   P_RECORD_ID     IN VARCHAR2,
                                   P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_DEPARTMENT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_DEPT_ID     IN VARCHAR2,
                                         P_DEPT_TYPE   IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_DEPARTMENT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_DEPT_ID     IN VARCHAR2,
                                    P_DEPT_TYPE   IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_DESIGNATION(P_MODULE_CODE      IN VARCHAR2,
                                          P_SCREEN_CODE      IN VARCHAR2,
                                          P_DEPT_ID          IN VARCHAR2,
                                          P_DESIG_SHORT_NAME IN VARCHAR2,
                                          P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_DESIGNATION(P_MODULE_CODE      IN VARCHAR2,
                                     P_SCREEN_CODE      IN VARCHAR2,
                                     P_DEPT_ID          IN VARCHAR2,
                                     P_DESIG_SHORT_NAME IN VARCHAR2,
                                     P_RECORD_ID        IN VARCHAR2,
                                     P_RET              OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_VACATIONS(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_VACATION_DESC IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_VACATIONS(P_MODULE_CODE   IN VARCHAR2,
                                   P_SCREEN_CODE   IN VARCHAR2,
                                   P_VACATION_DESC IN VARCHAR2,
                                   P_RECORD_ID     IN VARCHAR2,
                                   P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_COMPETENCY(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_COM_DESC    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_COMPETENCY(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_COM_DESC    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_DEF(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_LEAVE_DESC  IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_LEAVE_DEF(P_MODULE_CODE IN VARCHAR2,
                                   P_SCREEN_CODE IN VARCHAR2,
                                   P_LEAVE_DESC  IN VARCHAR2,
                                   P_RECORD_ID   IN VARCHAR2,
                                   P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_DEPT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_FISCAL_YEAR IN VARCHAR2,
                                         P_DEPT_ID     IN VARCHAR2,
                                         P_LEAVE_ID    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_LEAVE_DEPT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_FISCAL_YEAR IN VARCHAR2,
                                    P_DEPT_ID     IN VARCHAR2,
                                    P_LEAVE_ID    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_APPL(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_STAFF_ID    IN VARCHAR2,
                                         P_LEAVE_FROM  IN DATE,
                                         P_LEAVE_TO    IN DATE,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_LEAVE_APPL(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_STAFF_ID    IN VARCHAR2,
                                    P_LEAVE_FROM  IN VARCHAR2,
                                    P_LEAVE_TO    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  FUNCTION FUNC_LEAVE_CANCEL_OR_NOT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_STAFF_ID    IN VARCHAR2,
                                    P_FISCAL_YR   IN VARCHAR2,
                                    P_LEAVE_FROM  IN DATE,
                                    P_LEAVE_TO    IN DATE,
                                    P_RECORD_ID   IN VARCHAR2) RETURN NUMBER;
  PROCEDURE PROC_ERR_LEAVE_CANCEL_OR_NOT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_STAFF_ID    IN VARCHAR2,
                                         P_FISCAL_YR   IN VARCHAR2,
                                         P_LEAVE_FROM  IN VARCHAR2,
                                         P_LEAVE_TO    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2,
                                         P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_CANC(P_MODULE_CODE  IN VARCHAR2,
                                         P_SCREEN_CODE  IN VARCHAR2,
                                         P_STAFF_ID     IN VARCHAR2,
                                         P_LEAVE_FROM   IN DATE,
                                         P_LEAVE_TO     IN DATE,
                                         P_RECORD_ID    IN VARCHAR2,
                                         P_LEAVE_REQ_ID IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_LEAVE_CANC(P_MODULE_CODE  IN VARCHAR2,
                                    P_SCREEN_CODE  IN VARCHAR2,
                                    P_STAFF_ID     IN VARCHAR2,
                                    P_LEAVE_FROM   IN VARCHAR2,
                                    P_LEAVE_TO     IN VARCHAR2,
                                    P_RECORD_ID    IN VARCHAR2,
                                    P_LEAVE_REQ_ID IN VARCHAR2,
                                    P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_REGLR(P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          P_STAFF_ID    IN VARCHAR2,
                                          P_LEAVE_FROM  IN DATE,
                                          P_LEAVE_TO    IN DATE,
                                          P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_LEAVE_REGLR(P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     P_STAFF_ID    IN VARCHAR2,
                                     P_LEAVE_FROM  IN VARCHAR2,
                                     P_LEAVE_TO    IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_APP_OBJ(P_MODULE_CODE  IN VARCHAR2,
                                      P_SCREEN_CODE  IN VARCHAR2,
                                      P_PER_CODE     IN VARCHAR2,
                                      P_PER_CATEGORY IN VARCHAR2,
                                      P_PER_TYPE     IN VARCHAR2,
                                      P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_APP_OBJ(P_MODULE_CODE  IN VARCHAR2,
                                 P_SCREEN_CODE  IN VARCHAR2,
                                 P_PER_CODE     IN VARCHAR2,
                                 P_PER_CATEGORY IN VARCHAR2,
                                 P_PER_TYPE     IN VARCHAR2,
                                 P_RECORD_ID    IN VARCHAR2,
                                 P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_APP_DEF(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_APP_CODE    IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_APP_DEF(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_APP_CODE    IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2,
                                 P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_APP_DEF_KRA(P_MODULE_CODE    IN VARCHAR2,
                                          P_SCREEN_CODE    IN VARCHAR2,
                                          P_ASSIGN_APP_ID  IN VARCHAR2,
                                          P_ASSIGN_DEPT_ID IN VARCHAR2,
                                          P_ASSIGN_JOB_ID  IN VARCHAR2,
                                          P_RECORD_ID      IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_APP_DEF_KRA(P_MODULE_CODE    IN VARCHAR2,
                                     P_SCREEN_CODE    IN VARCHAR2,
                                     P_ASSIGN_APP_ID  IN VARCHAR2,
                                     P_ASSIGN_DEPT_ID IN VARCHAR2,
                                     P_ASSIGN_JOB_ID  IN VARCHAR2,
                                     P_RECORD_ID      IN VARCHAR2,
                                     P_RET            OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_REQ(P_MODULE_CODE  IN VARCHAR2,
                                         P_SCREEN_CODE  IN VARCHAR2,
                                         P_RESIG_EMP_ID IN VARCHAR2,
                                         P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_RESIGN_REQ(P_MODULE_CODE  IN VARCHAR2,
                                    P_SCREEN_CODE  IN VARCHAR2,
                                    P_RESIG_EMP_ID IN VARCHAR2,
                                    P_RECORD_ID    IN VARCHAR2,
                                    P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_CANC(P_MODULE_CODE       IN VARCHAR2,
                                          P_SCREEN_CODE       IN VARCHAR2,
                                          P_RESIG_CANC_EMP_ID IN VARCHAR2,
                                          P_RECORD_ID         IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_RESIGN_CANC(P_MODULE_CODE       IN VARCHAR2,
                                     P_SCREEN_CODE       IN VARCHAR2,
                                     P_RESIG_CANC_EMP_ID IN VARCHAR2,
                                     P_RECORD_ID         IN VARCHAR2,
                                     P_RET               OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_INTR(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_RESIG_HDR_ID IN VARCHAR2,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER;
  PROCEDURE PROC_ERR_MGR_RESIGN_INTR(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_RESIG_HDR_ID IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_HAND(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_RESIG_HDR_ID IN VARCHAR2,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_RESIGN_HAND(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_RESIG_HDR_ID IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_HOLIDAY_MST(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_HOLIDAY_DATE IN DATE,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_HOLIDAY_MST(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_HOLIDAY_DATE IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_TM_ATTN_SCH(P_MODULE_CODE     IN VARCHAR2,
                                          P_SCREEN_CODE     IN VARCHAR2,
                                          P_TIME_SCHDULE_ID IN VARCHAR2,
                                          P_RECORD_ID       IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_TM_ATTN_SCH(P_MODULE_CODE     IN VARCHAR2,
                                     P_SCREEN_CODE     IN VARCHAR2,
                                     P_TIME_SCHDULE_ID IN VARCHAR2,
                                     P_RECORD_ID       IN VARCHAR2,
                                     P_RET             OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_TM_DEPT_SCH(P_MODULE_CODE   IN VARCHAR2,
                                          P_SCREEN_CODE   IN VARCHAR2,
                                          P_DEPT_ID       IN VARCHAR2,
                                          P_GROUP_ID      IN VARCHAR2,
                                          P_SCHEDULE_CODE IN VARCHAR2,
                                          P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_TM_DEPT_SCH(P_MODULE_CODE   IN VARCHAR2,
                                     P_SCREEN_CODE   IN VARCHAR2,
                                     P_DEPT_ID       IN VARCHAR2,
                                     P_GROUP_ID      IN VARCHAR2,
                                     P_SCHEDULE_CODE IN VARCHAR2,
                                     P_RECORD_ID     IN VARCHAR2,
                                     P_RET           OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_PERMISSION(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_EMP_ID      IN VARCHAR2,
                                         P_FROM_DATE   IN DATE,
                                         P_TO_DATE     IN DATE,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_PERMISSION(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_EMP_ID      IN VARCHAR2,
                                    P_FROM_DATE   IN VARCHAR2,
                                    P_TO_DATE     IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2);

  PROCEDURE PROC_DELETE_MANAGER(P_MODULE_CODE IN VARCHAR2,
                                P_SCREEN_CODE IN VARCHAR2,
                                P_RECORD_ID   IN VARCHAR2,
                                P_RET         OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_BANK_BRANCH(P_MODULE_CODE      IN VARCHAR2,
                                      P_SCREEN_CODE      IN VARCHAR2,
                                      P_BANK_ID          IN VARCHAR2,
                                      P_BANK_BRANCH_NAME IN VARCHAR2,
                                      P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_BANK_BRANCH(P_MODULE_CODE      IN VARCHAR2,
                                     P_SCREEN_CODE      IN VARCHAR2,
                                     P_BANK_ID          IN VARCHAR2,
                                     P_BANK_BRANCH_NAME IN VARCHAR2,
                                     P_RECORD_ID        IN VARCHAR2,

                                     P_RET OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_BANK_ACCOUNTS(P_MODULE_CODE      IN VARCHAR2,
                                        P_SCREEN_CODE      IN VARCHAR2,
                                        P_BANK_ID          IN VARCHAR2,
                                        P_BANK_BRANCH_NAME IN VARCHAR2,
                                        P_ACCOUNT_CODE     IN VARCHAR2,
                                        P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_BANK_ACCOUNTS(P_MODULE_CODE      IN VARCHAR2,
                                       P_SCREEN_CODE      IN VARCHAR2,
                                       P_BANK_ID          IN VARCHAR2,
                                       P_BANK_BRANCH_NAME IN VARCHAR2,
                                       P_ACCOUNT_CODE     IN VARCHAR2,
                                       P_RECORD_ID        IN VARCHAR2,

                                       P_RET OUT VARCHAR2);

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_NAME(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_ITEM_NAME   IN VARCHAR2,
                                        P_ORG_ID      IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER;

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_CODE(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_ITEM_CODE   IN VARCHAR2,
                                        P_ORG_ID      IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)

   RETURN NUMBER;

  PROCEDURE PROC_ERR_MGR_ITEM_MASTER(P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     P_ITEM_CODE   IN VARCHAR2,
                                     P_ITEM_NAME   IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_RET         OUT VARCHAR2);

  FUNCTION FUNC_ERR_ACC_CAL_DTL_EDIT(p_cal_dtl_id IN VARCHAR2,
                                     P_ORG_ID     IN VARCHAR2,
                                     P_RECORD_ID  IN VARCHAR2)

   RETURN NUMBER;

  PROCEDURE PROC_ERR_ACC_CAL_DTL_EDIT(

                                      P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      p_cal_dtl_id  IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2,
                                      P_RET         OUT VARCHAR2);

  FUNCTION FUNC_ERR_IS_PERIOD_AVIAL(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    p_data_id     IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_ORG_ID      IN VARCHAR2,
                                    P_TRANS_DATE  IN VARCHAR2)

   RETURN NUMBER;

  PROCEDURE PROC_ERR_IS_PERIOD_AVIAL(

                                     P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     p_data_id     IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2,
                                     P_TRANS_DATE  IN VARCHAR2,
                                     P_RET         OUT VARCHAR2);

END PKG_VALIDATIONS;

 
/
CREATE OR REPLACE PACKAGE BODY PKG_VALIDATIONS IS

  --- ***************************************************************************************************************
  --- $Body: PKG_VALLIDATIONS 1.0 2014-05-19 $
  --- ***************************************************************************************************************
  --- Program      :   PKG_VALLIDATIONS
  --- Description  :   All  Basic Validations related codes goes here
  --- Author       :   Ram A ,VMV Systems Pvt Ltd
  --- Date   :   13\05\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    19\05\2014          Ram
  --- ***************************************************************************************************************

  GV_COUNT     NUMBER;
  GV_COUNT_1   NUMBER;
  GV_COUNT_2   NUMBER;
  GV_STR       NVARCHAR2(4000) := NULL;
  GV_FROM_DATE DATE;
  GV_TO_DATE   DATE;
  GV_STATUS    CHAR(1) := 0;

  FUNCTION FUNC_DUPLICATE_MGR_CURR_CODE(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_CURRENCY_CODE IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(CURRENCY_CODE)), 0)
        INTO GV_COUNT
        FROM SSM_CURRENCIES SC
       WHERE TRIM(SC.CURRENCY_CODE) = TRIM(P_CURRENCY_CODE)
         AND SC.CURRENCY_ID <> P_RECORD_ID
         AND SC.WORKFLOW_COMPLETION_STATUS = '1'
         AND SC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_CURR_CODE;

  PROCEDURE PROC_ERR_MGR_CURRENCY_CODE(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_CURRENCY_CODE IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2,
                                       P_RET           OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_CURR_CODE(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_CURRENCY_CODE,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_001');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_CURRENCY_CODE;

  FUNCTION FUNC_DUPLICATE_MGR_ACC_GROUP(P_MODULE_CODE    IN VARCHAR2,
                                        P_SCREEN_CODE    IN VARCHAR2,
                                        P_ACC_GROUP_NAME IN VARCHAR2,
                                        P_RECORD_ID      IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(ACCT_GRP_DESC)), 0)
        INTO GV_COUNT
        FROM GL_ACCT_GROUPS AG
       WHERE TRIM(AG.ACCT_GRP_DESC) = TRIM(P_ACC_GROUP_NAME)
         AND AG.ACCT_GRP_ID <> P_RECORD_ID
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_ACC_GROUP;

  PROCEDURE PROC_ERR_MGR_ACC_GROUP(P_MODULE_CODE    IN VARCHAR2,
                                   P_SCREEN_CODE    IN VARCHAR2,
                                   P_ACC_GROUP_NAME IN VARCHAR2,
                                   P_RECORD_ID      IN VARCHAR2,
                                   P_FROM_DATE      IN VARCHAR2,
                                   P_TO_DATE        IN VARCHAR2,
                                   P_RET            OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_ACC_GROUP(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_ACC_GROUP_NAME,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_002');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_GROUP;

  FUNCTION FUNC_EFF_DATE_VALIDATOR(P_FROM_DATE IN DATE, P_TO_DATE IN DATE)
    RETURN NUMBER IS
    V_TEMP NUMBER;

  BEGIN

    IF P_FROM_DATE IS NOT NULL THEN
      IF P_TO_DATE IS NOT NULL THEN
        IF P_FROM_DATE >= P_TO_DATE THEN
          V_TEMP := 1;
        ELSE
          V_TEMP := 0;
        END IF;

      ELSE
        V_TEMP := 0;
      END IF;
    ELSE
      V_TEMP := 0;
    END IF;
    RETURN(V_TEMP);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_EFF_DATE_VALIDATOR;

  FUNCTION FUNC_DUPLICATE_SEGMENT(P_MODULE_CODE  IN VARCHAR2,
                                  P_SCREEN_CODE  IN VARCHAR2,
                                  P_SEGMENT_NAME IN VARCHAR2,
                                  P_RECORD_ID    IN VARCHAR2) RETURN NUMBER IS
  BEGIN
    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(SEGMENT_NAME)), 0)
        INTO GV_COUNT
        FROM GL_SEGMENTS GS
       WHERE TRIM(GS.SEGMENT_NAME) = TRIM(P_SEGMENT_NAME)
         AND GS.SEGMENT_ID <> P_RECORD_ID
         AND GS.WORKFLOW_COMPLETION_STATUS = '1'
         AND GS.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_SEGMENT;

  PROCEDURE PROC_ERR_MGR_SEGMENT(P_MODULE_CODE  IN VARCHAR2,
                                 P_SCREEN_CODE  IN VARCHAR2,
                                 P_SEGMENT_NAME IN VARCHAR2,
                                 P_RECORD_ID    IN VARCHAR2,
                                 P_FROM_DATE    IN VARCHAR2,
                                 P_TO_DATE      IN VARCHAR2,
                                 P_RET          OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_SEGMENT(P_MODULE_CODE,
                              P_SCREEN_CODE,
                              P_SEGMENT_NAME,
                              P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_003');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_SEGMENT;

  FUNCTION FUNC_DUPLICATE_ACC_STRUCTURE(P_MODULE_CODE      IN VARCHAR2,
                                        P_SCREEN_CODE      IN VARCHAR2,
                                        P_ACCT_STRUCT_NAME IN VARCHAR2,
                                        P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER

   IS
  BEGIN
    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(ACCT_STRUCT_NAME)), 0)
        INTO GV_COUNT
        FROM GL_ACCT_STRUCTURE GAS
       WHERE TRIM(GAS.ACCT_STRUCT_NAME) = TRIM(P_ACCT_STRUCT_NAME)
         AND GAS.ACCT_STRUCT_ID <> P_RECORD_ID
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_ACC_STRUCTURE;

  PROCEDURE PROC_ERR_MGR_ACC_STRUCTURE(P_MODULE_CODE      IN VARCHAR2,
                                       P_SCREEN_CODE      IN VARCHAR2,
                                       P_ACCT_STRUCT_NAME IN VARCHAR2,
                                       P_RECORD_ID        IN VARCHAR2,
                                       P_FROM_DATE        IN VARCHAR2,
                                       P_TO_DATE          IN VARCHAR2,
                                       P_RET              OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_ACC_STRUCTURE(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_ACCT_STRUCT_NAME,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_004');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_STRUCTURE;

  FUNCTION FUNC_DUPLICATE_ACC_GRP_DFL_SEG(P_MODULE_CODE     IN VARCHAR2,
                                          P_SCREEN_CODE     IN VARCHAR2,
                                          P_ACCT_GRP_ID     IN VARCHAR2,
                                          P_ACCT_SEGMENT_ID IN VARCHAR2,
                                          P_RECORD_ID       IN VARCHAR2)
    RETURN NUMBER

   IS
  BEGIN
    BEGIN

      SELECT COUNT(*)
        Into Gv_Count
        FROM GL_ACCT_GROUP_DEFAULT_SEGMENTS GDS,
             Gl_Acct_Grp_Default_Sgmnts_Dtl GDSD
       Where Gds.Acct_Grp_Id = P_Acct_Grp_Id
         AND GDSD.ACCT_DEF_ID = GDS.ACCT_DEF_ID
         AND GDSD.ACCT_SEGMENT_ID = P_ACCT_SEGMENT_ID
         And Gds.Acct_Def_Id <> P_Record_Id
         And Gds.Workflow_Completion_Status = '1'
         AND GDS.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_ACC_GRP_DFL_SEG;

  PROCEDURE PROC_ERR_MGR_ACC_GRP_DEF_SEG(P_MODULE_CODE     IN VARCHAR2,
                                         P_SCREEN_CODE     IN VARCHAR2,
                                         P_ACCT_GRP_ID     IN VARCHAR2,
                                         P_ACCT_SEGMENT_ID IN VARCHAR2,
                                         P_RECORD_ID       IN VARCHAR2,
                                         P_FROM_DATE       IN VARCHAR2,
                                         P_TO_DATE         IN VARCHAR2,
                                         P_RET             OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_ACC_GRP_DFL_SEG(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_ACCT_GRP_ID,
                                      P_ACCT_SEGMENT_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_005');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_GRP_DEF_SEG;

  FUNCTION FUNC_DUPLICATE_ACCOUNT_CODE(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_ACC_STRUCTURE IN VARCHAR2,
                                       P_ACC_GROUP     IN VARCHAR2,
                                       P_ACCT_CODE     IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2)

   RETURN NUMBER IS
  BEGIN
    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(ACCT_CODE)), 0)
        INTO GV_COUNT
        FROM GL_ACCT_CODES AC
       WHERE AC.ACCT_STRUCT_ID = P_ACC_STRUCTURE
         AND AC.ACCT_GRP_ID = P_ACC_GROUP
         AND TRIM(AC.ACCT_CODE) = TRIM(P_ACCT_CODE)
         AND AC.ACCT_CODE_ID <> P_RECORD_ID
         AND AC.WORKFLOW_COMPLETION_STATUS = '1'
         AND AC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_ACCOUNT_CODE;

  PROCEDURE PROC_ERR_MGR_ACC_CODE(P_MODULE_CODE   IN VARCHAR2,
                                  P_SCREEN_CODE   IN VARCHAR2,
                                  P_ACC_STRUCTURE IN VARCHAR2,
                                  P_ACC_GROUP     IN VARCHAR2,
                                  P_ACCT_CODE     IN VARCHAR2,
                                  P_RECORD_ID     IN VARCHAR2,
                                  P_FROM_DATE     IN VARCHAR2,
                                  P_TO_DATE       IN VARCHAR2,
                                  P_RET           OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_ACCOUNT_CODE(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_ACC_STRUCTURE,
                                   P_ACC_GROUP,
                                   P_ACCT_CODE,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_006');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_CODE;
  FUNCTION FUNC_DUPLI_MGR_ACC_CALR_DTL(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       p_ACC_YR      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN
    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(CAL_ACCT_YEAR)), 0)
        INTO GV_COUNT
        FROM GL_ACCT_CALENDAR_DTL CH, gl_acct_calendar_hdr d
       WHERE TRIM(CH.CAL_ACCT_YEAR) = TRIM(p_ACC_YR)
         and upper(trim(d.cal_desc)) = upper(trim(P_SCREEN_CODE))
         and d.cal_id = ch.cal_id
         AND CH.CAL_DTL_ID <> P_RECORD_ID
         AND CH.WORKFLOW_COMPLETION_STATUS = '1'
         AND CH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLI_MGR_ACC_CALR_DTL;
  PROCEDURE PROC_ERR_MGR_ACC_CALENDAR_DTL(

                                          P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          p_ACC_YR      IN VARCHAR2,
                                          P_RECORD_ID   IN VARCHAR2,
                                          P_FROM_DATE   IN VARCHAR2,
                                          P_TO_DATE     IN VARCHAR2,
                                          P_RET         OUT VARCHAR2)

   IS

  BEGIN

    IF FUNC_DUPLI_MGR_ACC_CALR_DTL(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   p_ACC_YR,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_100');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_CALENDAR_DTL;

  FUNCTION FUNC_DUPLICATE_MGR_ACC_CALNDR(P_MODULE_CODE   IN VARCHAR2,
                                         P_SCREEN_CODE   IN VARCHAR2,
                                         P_CALENDAR_NAME IN VARCHAR2,
                                         P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN
    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(CAL_DESC)), 0)
        INTO GV_COUNT
        FROM GL_ACCT_CALENDAR_HDR CH
       WHERE TRIM(CH.CAL_DESC) = TRIM(P_CALENDAR_NAME)
         AND CH.CAL_ID <> P_RECORD_ID
         AND CH.WORKFLOW_COMPLETION_STATUS = '1'
         AND CH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_ACC_CALNDR;

  PROCEDURE PROC_ERR_MGR_ACC_CALENDAR(P_MODULE_CODE   IN VARCHAR2,
                                      P_SCREEN_CODE   IN VARCHAR2,
                                      P_CALENDAR_NAME IN VARCHAR2,
                                      P_RECORD_ID     IN VARCHAR2,
                                      P_FROM_DATE     IN VARCHAR2,
                                      P_TO_DATE       IN VARCHAR2,
                                      P_RET           OUT VARCHAR2)

   IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_ACC_CALNDR(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_CALENDAR_NAME,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_008');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_CALENDAR;

  PROCEDURE PROC_ERR_MGR_ACC_GRP_LINK(P_FROM_DATE IN VARCHAR2,
                                      P_TO_DATE   IN VARCHAR2,
                                      P_RET       OUT VARCHAR2) IS
  BEGIN
    P_RET        := null;
    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ACC_GRP_LINK;

  FUNCTION FUNC_DUPLICATE_MGR_ORG_INTR_NM(P_MODULE_CODE   IN VARCHAR2,
                                          P_SCREEN_CODE   IN VARCHAR2,
                                          P_INTERNAL_NAME IN VARCHAR2,
                                          P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(CH.COMP_INTERNAL_NAME)), 0)
        INTO GV_COUNT
        FROM GL_COMPANIES_HDR CH
       WHERE TRIM(CH.COMP_INTERNAL_NAME) = TRIM(P_INTERNAL_NAME)
         AND CH.COMP_ID <> P_RECORD_ID
         AND CH.WORKFLOW_COMPLETION_STATUS = '1'
         AND CH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_ORG_INTR_NM;

  FUNCTION FUNC_DUPLICATE_MGR_ORG_SHRT_NM(P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          P_SHORT_NAME  IN VARCHAR2,
                                          P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(CH.COMP_SHORT_NAME)), 0)
        INTO GV_COUNT
        FROM GL_COMPANIES_HDR CH
       WHERE TRIM(CH.COMP_SHORT_NAME) = TRIM(P_SHORT_NAME)
         AND CH.COMP_ID <> P_RECORD_ID
         AND CH.WORKFLOW_COMPLETION_STATUS = '1'
         AND CH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_ORG_SHRT_NM;

  PROCEDURE PROC_ERR_MGR_ORGANIZATION(P_MODULE_CODE   IN VARCHAR2,
                                      P_SCREEN_CODE   IN VARCHAR2,
                                      P_INTERNAL_NAME IN VARCHAR2,
                                      P_SHORT_NAME    IN VARCHAR2,
                                      P_RECORD_ID     IN VARCHAR2,
                                      P_RET           OUT VARCHAR2) IS

  BEGIN

    IF P_INTERNAL_NAME IS NOT NULL THEN

      IF FUNC_DUPLICATE_MGR_ORG_INTR_NM(P_MODULE_CODE,
                                        P_SCREEN_CODE,
                                        P_INTERNAL_NAME,
                                        P_RECORD_ID) = 1 THEN

        GV_STR := SSM.Get_Err_Message('EN', 'ERR_009');

      END IF;
    END IF;

    IF P_SHORT_NAME IS NOT NULL THEN

      IF FUNC_DUPLICATE_MGR_ORG_INTR_NM(P_MODULE_CODE,
                                        P_SCREEN_CODE,
                                        P_SHORT_NAME,
                                        P_RECORD_ID) = 1 THEN

        GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_010');

      END IF;
    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;

  END PROC_ERR_MGR_ORGANIZATION;

  --AP

  FUNCTION FUNC_DUPLICATE_MGR_TAX_TERM(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_TAX_NAME    IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(TT.TAX_NAME)), 0)
        INTO GV_COUNT
        FROM TAX_TERMS TT
       WHERE TRIM(TT.TAX_NAME) = TRIM(P_TAX_NAME)
         AND TT.TAX_ID <> P_RECORD_ID
         AND TT.WORKFLOW_COMPLETION_STATUS = '1'
         AND TT.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_TAX_TERM;

  PROCEDURE PROC_ERR_MGR_TAX_TERM(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_TAX_NAME    IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_FROM_DATE   IN VARCHAR2,
                                  P_TO_DATE     IN VARCHAR2,
                                  P_RET         OUT VARCHAR2) IS
  BEGIN
    GV_STR := '';
    IF FUNC_DUPLICATE_MGR_TAX_TERM(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_TAX_NAME,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_011');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;

  END PROC_ERR_MGR_TAX_TERM;

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_CATOG(P_MODULE_CODE   IN VARCHAR2,
                                         P_SCREEN_CODE   IN VARCHAR2,
                                         P_ITEM_CAT_NAME IN VARCHAR2,
                                         P_ORG_ID        IN VARCHAR2,
                                         P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(IC.ITEM_CAT_NAME)), 0)
        INTO GV_COUNT
        FROM INV_ITEM_CATEGORY IC
       WHERE TRIM(IC.ITEM_CAT_NAME) = TRIM(P_ITEM_CAT_NAME)
         AND IC.ORG_ID = P_ORG_ID
         AND IC.ITEM_CATEGORY_ID <> P_RECORD_ID
         AND IC.WORKFLOW_COMPLETION_STATUS = '1'
         AND IC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_ITEM_CATOG;

  PROCEDURE PROC_ERR_MGR_ITEM_CATOG(P_MODULE_CODE   IN VARCHAR2,
                                    P_SCREEN_CODE   IN VARCHAR2,
                                    P_ITEM_CAT_NAME IN VARCHAR2,
                                    P_ORG_ID        IN VARCHAR2,
                                    P_RECORD_ID     IN VARCHAR2,
                                    P_FROM_DATE     IN VARCHAR2,
                                    P_TO_DATE       IN VARCHAR2,
                                    P_RET           OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_ITEM_CATOG(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_ITEM_CAT_NAME,
                                     P_ORG_ID,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_012');

    END IF;

    GV_FROM_DATE := TO_DATE(P_FROM_DATE, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_EFF_DATE_VALIDATOR(GV_FROM_DATE, GV_TO_DATE) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_007');

    END IF;
    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;

  END PROC_ERR_MGR_ITEM_CATOG;

  FUNCTION FUNC_DUPLICATE_MGR_UOM_MST(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_UOM_CODE    IN VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(UM.UOM_CODE)), 0)
        INTO GV_COUNT
        FROM INV_UOM_MASTER UM
       WHERE TRIM(UM.UOM_CODE) = TRIM(P_UOM_CODE)
         AND UM.UOM_ID <> P_RECORD_ID
         AND UM.ORG_ID = P_ORG_ID
         AND UM.WORKFLOW_COMPLETION_STATUS = '1'
         AND UM.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_UOM_MST;

  FUNCTION FUNC_DUPLICATE_MGR_UOM_BASE(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_UOM_BASE    IN VARCHAR2,
                                       P_ORG_ID      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(UM.UOM_CODE)), 0)
        INTO GV_COUNT
        FROM INV_UOM_MASTER UM
       WHERE UM.BASE_UNIT = P_UOM_BASE
         AND UM.UOM_ID <> P_RECORD_ID
         AND UM.ORG_ID = P_ORG_ID
         AND UM.WORKFLOW_COMPLETION_STATUS = '1'
         AND UM.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_UOM_BASE;

  PROCEDURE PROC_ERR_MGER_UOM_MST(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_UOM_CODE    IN VARCHAR2,
                                  P_UOM_BASE    IN VARCHAR2,
                                  P_ORG_ID      IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_UOM_MST(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_UOM_CODE,
                                  P_ORG_ID,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_013');

    END IF;

    IF P_UOM_BASE = '1' THEN

      IF FUNC_DUPLICATE_MGR_UOM_BASE(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_UOM_BASE,
                                     P_ORG_ID,
                                     P_RECORD_ID) = 1 THEN

        GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_053');

      END IF;
    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;

  END PROC_ERR_MGER_UOM_MST;

  FUNCTION FUNC_DUPLICATE_MGR_UOM_CONV(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_UOM_FROM    IN VARCHAR2,
                                       P_UOM_TO      IN VARCHAR2,
                                       P_ORG_ID      IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM INV_UOM_CONVERSION UC
       WHERE UC.UOM_FROM = P_UOM_FROM
         AND UC.UOM_TO = P_UOM_TO
         AND UC.UOM_CONV_ID <> P_RECORD_ID
         AND UC.ORG_ID = P_ORG_ID
         AND UC.WORKFLOW_COMPLETION_STATUS = '1'
         AND UC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_UOM_CONV;

  PROCEDURE PROC_UOM_CONV_VALIDATOR(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_UOM_FROM    IN VARCHAR2,
                                    P_UOM_TO      IN VARCHAR2,
                                    P_ORG_ID      IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2)

   IS

  BEGIN
    IF TRIM(P_UOM_FROM) = TRIM(P_UOM_TO) THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_014');

    END IF;

    IF FUNC_DUPLICATE_MGR_UOM_CONV(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_UOM_FROM,
                                   P_UOM_TO,
                                   P_ORG_ID,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_054');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_UOM_CONV_VALIDATOR;

  ---
  FUNCTION FUNC_DUPLICATE_MGR_SUPPLIER(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_VENDOR_CODE IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(SC.VENDOR_CODE)), 0)
        INTO GV_COUNT
        FROM SUPPLIER_CUSTOMERS SC
       WHERE TRIM(SC.VENDOR_CODE) = TRIM(P_VENDOR_CODE)
         AND SC.VENDOR_ID <> P_RECORD_ID
         AND SC.WORKFLOW_COMPLETION_STATUS = '1'
         AND SC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_SUPPLIER;

  PROCEDURE PROC_ERR_MGR_SUPPLIER(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_VENDOR_CODE IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_SUPPLIER(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_VENDOR_CODE,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_015');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_SUPPLIER;

  FUNCTION FUNC_DUPLICATE_MGR_SUP_BRANCH(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_BRANCH_CODE IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(SCB.VENDOR_BRANCH_CODE)), 0)
        INTO GV_COUNT
        FROM SUPPLIER_CUSTOMER_BRANCH SCB
       WHERE TRIM(SCB.VENDOR_BRANCH_CODE) = TRIM(P_BRANCH_CODE)
         AND SCB.VENDOR_LOC_ID <> P_RECORD_ID
         AND SCB.WORKFLOW_COMPLETION_STATUS = '1'
         AND SCB.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_SUP_BRANCH;

  PROCEDURE PROC_ERR_MGR_SUP_BRANCH(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_BRANCH_CODE IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_SUP_BRANCH(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_BRANCH_CODE,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_016');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_SUP_BRANCH;

  FUNCTION FUNC_DUPLICATE_MGR_PO(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_PO_NUMBER   IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2) RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(PH.PO_NUM)), 0)
        INTO GV_COUNT
        FROM PO_HEADERS PH
       WHERE TRIM(PH.PO_NUM) = TRIM(P_PO_NUMBER)
         AND PH.PO_HEADER_ID <> P_RECORD_ID
         AND PH.WORKFLOW_COMPLETION_STATUS = '1'
         AND PH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_PO;

  PROCEDURE PROC_ERR_MGR_PO(P_MODULE_CODE IN VARCHAR2,
                            P_SCREEN_CODE IN VARCHAR2,
                            P_GRN_NUMBER  IN VARCHAR2,
                            P_RECORD_ID   IN VARCHAR2,
                            P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_PO_RECP(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_GRN_NUMBER,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_019');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_PO;

  FUNCTION FUNC_DUPLICATE_MGR_PO_RECP(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_GRN_NUMBER  IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(RH.GRN_NUM)), 0)
        INTO GV_COUNT
        FROM INV_RECEIPTS_HDR RH
       WHERE TRIM(RH.GRN_NUM) = TRIM(P_GRN_NUMBER)
         AND RH.RECEIPT_ID <> P_RECORD_ID
         AND RH.WORKFLOW_COMPLETION_STATUS = '1'
         AND RH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_PO_RECP;

  PROCEDURE PROC_ERR_MGR_PO_RECP(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_GRN_NUMBER  IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2,
                                 P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_PO_RECP(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_GRN_NUMBER,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_019');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_PO_RECP;

  FUNCTION FUNC_DUPLICATE_MGR_RECP_LOT(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_LOT_NUMBER  IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS
  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(RLH.LOT_NUMBER)), 0)
        INTO GV_COUNT
        FROM INV_RECEIPT_LOTS_HDR RLH
       WHERE TRIM(RLH.LOT_NUMBER) = TRIM(P_LOT_NUMBER)
         AND RLH.LOT_ID <> P_RECORD_ID
         AND RLH.WORKFLOW_COMPLETION_STATUS = '1'
         AND RLH.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);

  END FUNC_DUPLICATE_MGR_RECP_LOT;

  PROCEDURE PROC_ERR_MGR_RECP_LOT(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_LOT_NUMBER  IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_MGR_PO_RECP(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_LOT_NUMBER,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_020');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_RECP_LOT;

  FUNCTION FUNC_LEAVE_DATE_MGR(P_MODULE_CODE            IN VARCHAR2,
                               P_SCREEN_CODE            IN VARCHAR2,
                               P_LEAVE_REQ_ID           IN VARCHAR2,
                               P_LEAVE_CANCELATION_DATE IN DATE)
    RETURN NUMBER IS
    V_FROM_DATE DATE;
    V_TO_DATE   DATE;
    V_TEMP      NUMBER;
  BEGIN

    BEGIN

      SELECT TRIM(LA.LEAVE_DATE_FROM), TRIM(LA.LEAVE_DATE_TO)
        INTO V_FROM_DATE, V_TO_DATE
        FROM HR_LEAVE_APPLICATIONS LA
       WHERE LA.WORKFLOW_COMPLETION_STATUS = '1'
         AND LA.ENABLED_FLAG = '1'
         AND LA.LEAVE_REQ_ID = P_LEAVE_REQ_ID;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    IF V_FROM_DATE IS NOT NULL THEN
      IF V_TO_DATE IS NOT NULL THEN
        IF V_FROM_DATE <= P_LEAVE_CANCELATION_DATE THEN
          IF V_TO_DATE >= P_LEAVE_CANCELATION_DATE THEN
            V_TEMP := 0;
          ELSE
            V_TEMP := 1;
          END IF;
        ELSE
          V_TEMP := 1;
        END IF;

      ELSE
        V_TEMP := 1;
      END IF;
    ELSE
      V_TEMP := 1;
    END IF;

    RETURN(V_TEMP);

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(1);

  END FUNC_LEAVE_DATE_MGR;

  PROCEDURE PROC_LEAVE_DATE_MGR(P_MODULE_CODE            IN VARCHAR2,
                                P_SCREEN_CODE            IN VARCHAR2,
                                P_LEAVE_REQ_ID           IN VARCHAR2,
                                P_LEAVE_CANCELATION_DATE IN VARCHAR2,
                                P_RET                    OUT VARCHAR2) IS

  BEGIN

    GV_FROM_DATE := TO_DATE(P_LEAVE_CANCELATION_DATE, 'DD-MM-YYYY');

    IF FUNC_LEAVE_DATE_MGR(P_MODULE_CODE,
                           P_SCREEN_CODE,
                           P_LEAVE_REQ_ID,
                           GV_FROM_DATE) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_021');
    ELSE

      GV_STR := 0;

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_LEAVE_DATE_MGR;

  --HR

  FUNCTION FUNC_DUPLICATE_MGR_CATEGORY(P_MODULE_CODE   IN VARCHAR2,
                                       P_SCREEN_CODE   IN VARCHAR2,
                                       P_CATEGORY_CODE IN VARCHAR2,
                                       P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(CATEGORY_CODE)), 0)
        INTO GV_COUNT
        FROM HR_CATEGORIES HC
       WHERE TRIM(HC.CATEGORY_CODE) = TRIM(P_CATEGORY_CODE)
         AND HC.CATEGORY_ID <> P_RECORD_ID
         AND HC.WORKFLOW_COMPLETION_STATUS = '1'
         AND HC.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_CATEGORY;

  PROCEDURE PROC_ERR_MGR_CATEGORY(P_MODULE_CODE   IN VARCHAR2,
                                  P_SCREEN_CODE   IN VARCHAR2,
                                  P_CATEGORY_CODE IN VARCHAR2,
                                  P_RECORD_ID     IN VARCHAR2,
                                  P_RET           OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_CATEGORY(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_CATEGORY_CODE,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_022');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_CATEGORY;

  FUNCTION FUNC_DUPLICATE_MGR_GRADE(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_CATEGORY_ID IN VARCHAR2,
                                    P_GRADE_CODE  IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2) RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_GRADES HG
       WHERE TRIM(HG.GRADE_ID) = TRIM(P_CATEGORY_ID)
         AND TRIM(HG.GRADE_CODE) = TRIM(P_GRADE_CODE)
         AND HG.GRADE_ID <> P_RECORD_ID
         AND HG.WORKFLOW_COMPLETION_STATUS = '1'
         AND HG.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_GRADE;

  PROCEDURE PROC_ERR_MGR_GRADE(P_MODULE_CODE IN VARCHAR2,
                               P_SCREEN_CODE IN VARCHAR2,
                               P_CATEGORY_ID IN VARCHAR2,
                               P_GRADE_CODE  IN VARCHAR2,
                               P_RECORD_ID   IN VARCHAR2,
                               P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_GRADE(P_MODULE_CODE,
                                P_SCREEN_CODE,
                                P_CATEGORY_ID,
                                P_GRADE_CODE,
                                P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_023');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_GRADE;

  FUNCTION FUNC_DUPLICATE_MGR_JOBS(P_MODULE_CODE IN VARCHAR2,
                                   P_SCREEN_CODE IN VARCHAR2,
                                   P_JOB_CODE    IN VARCHAR2,
                                   P_RECORD_ID   IN VARCHAR2) RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(HJ.JOB_CODE)), 0)
        INTO GV_COUNT
        FROM HR_JOBS HJ
       WHERE TRIM(HJ.JOB_CODE) = TRIM(P_JOB_CODE)
         AND HJ.JOB_ID <> P_RECORD_ID
         AND HJ.WORKFLOW_COMPLETION_STATUS = '1'
         AND HJ.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_JOBS;

  PROCEDURE PROC_ERR_MGR_JOBS(P_MODULE_CODE IN VARCHAR2,
                              P_SCREEN_CODE IN VARCHAR2,
                              P_JOB_CODE    IN VARCHAR2,
                              P_RECORD_ID   IN VARCHAR2,
                              P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_JOBS(P_MODULE_CODE,
                               P_SCREEN_CODE,
                               P_JOB_CODE,
                               P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_024');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_JOBS;

  FUNCTION FUNC_DUPLICATE_MGR_JOB_RESP(P_MODULE_CODE IN VARCHAR2,
                                       P_SCREEN_CODE IN VARCHAR2,
                                       P_JOB_ID      IN VARCHAR2,
                                       P_JR_TYPE     IN VARCHAR2,
                                       P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_JOB_RESPONSIBILITY HJR
       WHERE TRIM(HJR.JOB_ID) = TRIM(P_JOB_ID)
         AND TRIM(HJR.JR_TYPE) = TRIM(P_JR_TYPE)
         AND HJR.JR_ID <> P_RECORD_ID
         AND HJR.WORKFLOW_COMPLETION_STATUS = '1'
         AND HJR.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_JOB_RESP;

  PROCEDURE PROC_ERR_MGR_JOB_RESP(P_MODULE_CODE IN VARCHAR2,
                                  P_SCREEN_CODE IN VARCHAR2,
                                  P_JOB_ID      IN VARCHAR2,
                                  P_JR_TYPE     IN VARCHAR2,
                                  P_RECORD_ID   IN VARCHAR2,
                                  P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_JOB_RESP(P_MODULE_CODE,
                                   P_SCREEN_CODE,
                                   P_JOB_ID,
                                   P_JR_TYPE,
                                   P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_025');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_JOB_RESP;

  FUNCTION FUNC_DUPLICATE_MGR_POSITIONS(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_JOB_ID        IN VARCHAR2,
                                        P_POSITION_CODE IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(HP.POSITION_CODE)), 0)
        INTO GV_COUNT
        FROM HR_POSITIONS HP
       WHERE TRIM(HP.JOB_ID) = TRIM(P_JOB_ID)
         AND TRIM(HP.POSITION_CODE) = TRIM(P_POSITION_CODE)
         AND HP.POSITION_ID <> P_RECORD_ID
         AND HP.WORKFLOW_COMPLETION_STATUS = '1'
         AND HP.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_POSITIONS;

  PROCEDURE PROC_ERR_MGR_POSITIONS(P_MODULE_CODE   IN VARCHAR2,
                                   P_SCREEN_CODE   IN VARCHAR2,
                                   P_JOB_ID        IN VARCHAR2,
                                   P_POSITION_CODE IN VARCHAR2,
                                   P_RECORD_ID     IN VARCHAR2,
                                   P_RET           OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_POSITIONS(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_JOB_ID,
                                    P_POSITION_CODE,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_026');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_POSITIONS;

  FUNCTION FUNC_DUPLICATE_MGR_DEPARTMENT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_DEPT_ID     IN VARCHAR2,
                                         P_DEPT_TYPE   IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_DEPARTMENTS HD
       WHERE TRIM(HD.DEPT_ID) = TRIM(P_DEPT_ID)
         AND TRIM(HD.DEPT_TYPE) = TRIM(P_DEPT_TYPE)
            -- AND HD.DEPT_ID <> P_RECORD_ID
         AND HD.WORKFLOW_COMPLETION_STATUS = '1'
         AND HD.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_DEPARTMENT;

  PROCEDURE PROC_ERR_MGR_DEPARTMENT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_DEPT_ID     IN VARCHAR2,
                                    P_DEPT_TYPE   IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_DEPARTMENT(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_DEPT_ID,
                                     P_DEPT_TYPE,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_027');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_DEPARTMENT;

  FUNCTION FUNC_DUPLICATE_MGR_DESIGNATION(P_MODULE_CODE      IN VARCHAR2,
                                          P_SCREEN_CODE      IN VARCHAR2,
                                          P_DEPT_ID          IN VARCHAR2,
                                          P_DESIG_SHORT_NAME IN VARCHAR2,
                                          P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_DEPT_DESIGNATIONS HDD
       WHERE TRIM(HDD.DEPT_ID) = TRIM(P_DEPT_ID)
         AND TRIM(HDD.DESIG_SHORT_NAME) = TRIM(P_DESIG_SHORT_NAME)
         AND HDD.DEPT_DESIG_ID <> P_RECORD_ID
         AND HDD.WORKFLOW_COMPLETION_STATUS = '1'
         AND HDD.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_DESIGNATION;

  PROCEDURE PROC_ERR_MGR_DESIGNATION(P_MODULE_CODE      IN VARCHAR2,
                                     P_SCREEN_CODE      IN VARCHAR2,
                                     P_DEPT_ID          IN VARCHAR2,
                                     P_DESIG_SHORT_NAME IN VARCHAR2,
                                     P_RECORD_ID        IN VARCHAR2,
                                     P_RET              OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_DESIGNATION(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_DEPT_ID,
                                      P_DESIG_SHORT_NAME,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_028');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_DESIGNATION;

  FUNCTION FUNC_DUPLICATE_MGR_VACATIONS(P_MODULE_CODE   IN VARCHAR2,
                                        P_SCREEN_CODE   IN VARCHAR2,
                                        P_VACATION_DESC IN VARCHAR2,
                                        P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(HV.VACATION_DESC)), 0)
        INTO GV_COUNT
        FROM HR_VACATIONS HV
       WHERE TRIM(HV.VACATION_DESC) = TRIM(P_VACATION_DESC)
         AND HV.VACATION_ID <> P_RECORD_ID
         AND HV.WORKFLOW_COMPLETION_STATUS = '1'
         AND HV.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_VACATIONS;

  PROCEDURE PROC_ERR_MGR_VACATIONS(P_MODULE_CODE   IN VARCHAR2,
                                   P_SCREEN_CODE   IN VARCHAR2,
                                   P_VACATION_DESC IN VARCHAR2,
                                   P_RECORD_ID     IN VARCHAR2,
                                   P_RET           OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_VACATIONS(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_VACATION_DESC,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_029');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_VACATIONS;

  FUNCTION FUNC_DUPLICATE_MGR_COMPETENCY(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_COM_DESC    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_COMPETENCY_HDR HCH
       WHERE TRIM(HCH.COM_DESC) = TRIM(P_COM_DESC)
         AND HCH.COM_HDR_ID <> P_RECORD_ID
         AND HCH.WORKFLOW_COMPLETION_STATUS = '1'
         AND HCH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_COMPETENCY;

  PROCEDURE PROC_ERR_MGR_COMPETENCY(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_COM_DESC    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_COMPETENCY(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_COM_DESC,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_030');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_COMPETENCY;

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_DEF(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_LEAVE_DESC  IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_LEAVE_DEFINITIONS HLD
       WHERE TRIM(HLD.LEAVE_DESC) = TRIM(P_LEAVE_DESC)
         AND HLD.LEAVE_ID <> P_RECORD_ID
         AND HLD.WORKFLOW_COMPLETION_STATUS = '1'
         AND HLD.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_LEAVE_DEF;

  PROCEDURE PROC_ERR_MGR_LEAVE_DEF(P_MODULE_CODE IN VARCHAR2,
                                   P_SCREEN_CODE IN VARCHAR2,
                                   P_LEAVE_DESC  IN VARCHAR2,
                                   P_RECORD_ID   IN VARCHAR2,
                                   P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_LEAVE_DEF(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_LEAVE_DESC,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_031');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_LEAVE_DEF;

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_DEPT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_FISCAL_YEAR IN VARCHAR2,
                                         P_DEPT_ID     IN VARCHAR2,
                                         P_LEAVE_ID    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_LEAVE_DEPT HLD
       WHERE TRIM(HLD.FISCAL_YEAR) = TRIM(P_FISCAL_YEAR)
         AND TRIM(HLD.DEPT_ID) = TRIM(P_DEPT_ID)
         AND TRIM(HLD.LEAVE_ID) = TRIM(P_LEAVE_ID)
         AND HLD.DEPT_LEAVE_ID <> P_RECORD_ID
         AND HLD.WORKFLOW_COMPLETION_STATUS = '1'
         AND HLD.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_LEAVE_DEPT;

  PROCEDURE PROC_ERR_MGR_LEAVE_DEPT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_FISCAL_YEAR IN VARCHAR2,
                                    P_DEPT_ID     IN VARCHAR2,
                                    P_LEAVE_ID    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_LEAVE_DEPT(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_FISCAL_YEAR,
                                     P_DEPT_ID,
                                     P_LEAVE_ID,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_032');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_LEAVE_DEPT;

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_APPL(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_STAFF_ID    IN VARCHAR2,
                                         P_LEAVE_FROM  IN DATE,
                                         P_LEAVE_TO    IN DATE,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_LEAVE_APPLICATIONS LA
       WHERE LA.STAFF_ID = P_STAFF_ID
         AND LA.LEAVE_DATE_FROM = P_LEAVE_FROM
         AND LA.LEAVE_DATE_TO = P_LEAVE_TO
         AND LA.LEAVE_REQ_ID <> P_RECORD_ID
         AND LA.WORKFLOW_COMPLETION_STATUS = '1'
         AND LA.ENABLED_FLAG = '1';

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT_1
        FROM HR_LEAVE_APPLICATIONS LA
       WHERE LA.STAFF_ID = P_STAFF_ID
         AND LA.LEAVE_DATE_FROM BETWEEN P_LEAVE_FROM AND P_LEAVE_TO
         AND LA.LEAVE_REQ_ID <> P_RECORD_ID
         AND LA.WORKFLOW_COMPLETION_STATUS = '1'
         AND LA.ENABLED_FLAG = '1';

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT_2
        FROM HR_LEAVE_APPLICATIONS LA
       WHERE LA.STAFF_ID = P_STAFF_ID
         and LA.LEAVE_DATE_TO BETWEEN P_LEAVE_FROM AND P_LEAVE_TO
         AND LA.LEAVE_REQ_ID <> P_RECORD_ID
         AND LA.WORKFLOW_COMPLETION_STATUS = '1'
         AND LA.ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT   := 0;
        GV_COUNT_1 := 0;
        GV_COUNT_2 := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT   := -1;
        GV_COUNT_1 := -1;
        GV_COUNT_2 := -1;
      WHEN OTHERS THEN
        GV_COUNT   := -1;
        GV_COUNT_1 := -1;
        GV_COUNT_2 := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    elsif GV_COUNT_1 > 0 THEN
      RETURN(1);
    elsif GV_COUNT_2 > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_LEAVE_APPL;

  PROCEDURE PROC_ERR_MGR_LEAVE_APPL(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_STAFF_ID    IN VARCHAR2,
                                    P_LEAVE_FROM  IN VARCHAR2,
                                    P_LEAVE_TO    IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS
  BEGIN
    GV_STR       := '';
    GV_FROM_DATE := TO_DATE(P_LEAVE_FROM, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_LEAVE_TO, 'DD-MM-YYYY'));

    IF FUNC_DUPLICATE_MGR_LEAVE_APPL(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_STAFF_ID,
                                     GV_FROM_DATE,
                                     GV_TO_DATE,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_033');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_LEAVE_APPL;

  FUNCTION FUNC_LEAVE_CANCEL_OR_NOT(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_STAFF_ID    IN VARCHAR2,
                                    P_FISCAL_YR   IN VARCHAR2,
                                    P_LEAVE_FROM  IN DATE,
                                    P_LEAVE_TO    IN DATE,
                                    P_RECORD_ID   IN VARCHAR2) RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        from hr_leave_cancellation_hdr lc, hR_LEAVE_APPLICATIONS LA
       where lc.emp_id = P_STAFF_ID
         AND LA.FISCAL_YEAR = P_FISCAL_YR
         AND LC.LEAVE_REQ_ID = LA.LEAVE_REQ_ID
         and lc.leave_date_from = P_LEAVE_FROM
         and lc.leave_date_to = P_LEAVE_TO
         AND Lc.WORKFLOW_COMPLETION_STATUS = '1'
         AND Lc.ENABLED_FLAG = '1';

      select NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT_1
        from hr_leave_applications la
       where la.staff_id = P_STAFF_ID
         and la.fiscal_year = P_FISCAL_YR
            -- and la.leave_date_from between '07/Apr/2015' and '08/Apr/2015'
         and la.leave_date_to between P_LEAVE_FROM and P_LEAVE_TO
         and la.enabled_flag = 1
         and la.workflow_completion_status = 1;

      IF GV_COUNT > 0 THEN
        GV_COUNT := 0;
      ELSIF GV_COUNT_1 > 0 THEN
        GV_COUNT := 1;
      ELSE
        GV_COUNT := 0;
      END IF;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT   := 0;
        GV_COUNT_1 := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT   := -1;
        GV_COUNT_1 := -1;
      WHEN OTHERS THEN
        GV_COUNT   := -1;
        GV_COUNT_1 := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_LEAVE_CANCEL_OR_NOT;

  PROCEDURE PROC_ERR_LEAVE_CANCEL_OR_NOT(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_STAFF_ID    IN VARCHAR2,
                                         P_FISCAL_YR   IN VARCHAR2,
                                         P_LEAVE_FROM  IN VARCHAR2,
                                         P_LEAVE_TO    IN VARCHAR2,
                                         P_RECORD_ID   IN VARCHAR2,
                                         P_RET         OUT VARCHAR2) IS
  BEGIN
    GV_STR       := '';
    GV_FROM_DATE := TO_DATE(P_LEAVE_FROM, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_LEAVE_TO, 'DD-MM-YYYY'));

    IF FUNC_LEAVE_CANCEL_OR_NOT(P_MODULE_CODE,
                                P_SCREEN_CODE,
                                P_STAFF_ID,
                                P_FISCAL_YR,
                                GV_FROM_DATE,
                                GV_TO_DATE,
                                P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_033');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_LEAVE_CANCEL_OR_NOT;

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_CANC(P_MODULE_CODE  IN VARCHAR2,
                                         P_SCREEN_CODE  IN VARCHAR2,
                                         P_STAFF_ID     IN VARCHAR2,
                                         P_LEAVE_FROM   IN DATE,
                                         P_LEAVE_TO     IN DATE,
                                         P_RECORD_ID    IN VARCHAR2,
                                         P_LEAVE_REQ_ID IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_LEAVE_CANCELLATION_HDR LCH
       WHERE TRIM(LCH.EMP_ID) = TRIM(P_STAFF_ID)
         AND LCH.LEAVE_DATE_FROM >= P_LEAVE_FROM
         AND LCH.LEAVE_DATE_TO <= P_LEAVE_TO
         AND LCH.LC_HDR_ID <> P_RECORD_ID
         AND LCH.LEAVE_REQ_ID = P_LEAVE_REQ_ID
         AND LCH.WORKFLOW_COMPLETION_STATUS = '1'
         AND LCH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_LEAVE_CANC;

  PROCEDURE PROC_ERR_MGR_LEAVE_CANC(P_MODULE_CODE  IN VARCHAR2,
                                    P_SCREEN_CODE  IN VARCHAR2,
                                    P_STAFF_ID     IN VARCHAR2,
                                    P_LEAVE_FROM   IN VARCHAR2,
                                    P_LEAVE_TO     IN VARCHAR2,
                                    P_RECORD_ID    IN VARCHAR2,
                                    P_LEAVE_REQ_ID IN VARCHAR2,
                                    P_RET          OUT VARCHAR2) IS
  BEGIN

    GV_FROM_DATE := TO_DATE(P_LEAVE_FROM, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_LEAVE_TO, 'DD-MM-YYYY'));

    IF FUNC_DUPLICATE_MGR_LEAVE_CANC(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_STAFF_ID,
                                     GV_FROM_DATE,
                                     GV_TO_DATE,
                                     P_RECORD_ID,
                                     P_LEAVE_REQ_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_034');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_LEAVE_CANC;

  FUNCTION FUNC_DUPLICATE_MGR_LEAVE_REGLR(P_MODULE_CODE IN VARCHAR2,
                                          P_SCREEN_CODE IN VARCHAR2,
                                          P_STAFF_ID    IN VARCHAR2,
                                          P_LEAVE_FROM  IN DATE,
                                          P_LEAVE_TO    IN DATE,
                                          P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_LEAVE_REGULARIZATION LR
       WHERE TRIM(LR.LR_EMP_ID) = TRIM(P_STAFF_ID)
         AND LR.LR_FROM_DT <= P_LEAVE_FROM
         AND LR.LR_TO_DT <= P_LEAVE_TO
         AND LR.LR_ID <> P_RECORD_ID
         AND LR.WORKFLOW_COMPLETION_STATUS = '1'
         AND LR.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_LEAVE_REGLR;

  PROCEDURE PROC_ERR_MGR_LEAVE_REGLR(P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     P_STAFF_ID    IN VARCHAR2,
                                     P_LEAVE_FROM  IN VARCHAR2,
                                     P_LEAVE_TO    IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_RET         OUT VARCHAR2) IS
  BEGIN

    GV_FROM_DATE := TO_DATE(P_LEAVE_FROM, 'DD-MM-YYYY');
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_LEAVE_TO, 'DD-MM-YYYY'));

    IF FUNC_DUPLICATE_MGR_LEAVE_REGLR(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_STAFF_ID,
                                      GV_FROM_DATE,
                                      GV_TO_DATE,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_035');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_LEAVE_REGLR;

  FUNCTION FUNC_DUPLICATE_MGR_APP_OBJ(P_MODULE_CODE  IN VARCHAR2,
                                      P_SCREEN_CODE  IN VARCHAR2,
                                      P_PER_CODE     IN VARCHAR2,
                                      P_PER_CATEGORY IN VARCHAR2,
                                      P_PER_TYPE     IN VARCHAR2,
                                      P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_PER_OBJECTIVE_SET POS
       WHERE TRIM(POS.PER_CODE) = TRIM(P_PER_CODE)
         AND TRIM(POS.PER_CATEGORY) = TRIM(P_PER_CATEGORY)
         AND TRIM(POS.PER_TYPE) = TRIM(P_PER_TYPE)
         AND POS.PER_OBJ_ID <> P_RECORD_ID
         AND POS.WORKFLOW_COMPLETION_STATUS = '1'
         AND POS.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_APP_OBJ;

  PROCEDURE PROC_ERR_MGR_APP_OBJ(P_MODULE_CODE  IN VARCHAR2,
                                 P_SCREEN_CODE  IN VARCHAR2,
                                 P_PER_CODE     IN VARCHAR2,
                                 P_PER_CATEGORY IN VARCHAR2,
                                 P_PER_TYPE     IN VARCHAR2,
                                 P_RECORD_ID    IN VARCHAR2,
                                 P_RET          OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_APP_OBJ(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_PER_CODE,
                                  P_PER_CATEGORY,
                                  P_PER_TYPE,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_036');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_APP_OBJ;

  FUNCTION FUNC_DUPLICATE_MGR_APP_DEF(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      P_APP_CODE    IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_PER_APPRAISAL_DEF PPD
       WHERE TRIM(PPD.APP_CODE) = TRIM(P_APP_CODE)
         AND PPD.APP_ID <> P_RECORD_ID
         AND PPD.WORKFLOW_COMPLETION_STATUS = '1'
         AND PPD.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_APP_DEF;

  PROCEDURE PROC_ERR_MGR_APP_DEF(P_MODULE_CODE IN VARCHAR2,
                                 P_SCREEN_CODE IN VARCHAR2,
                                 P_APP_CODE    IN VARCHAR2,
                                 P_RECORD_ID   IN VARCHAR2,
                                 P_RET         OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_APP_DEF(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_APP_CODE,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_037');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_APP_DEF;

  FUNCTION FUNC_DUPLICATE_MGR_APP_DEF_KRA(P_MODULE_CODE    IN VARCHAR2,
                                          P_SCREEN_CODE    IN VARCHAR2,
                                          P_ASSIGN_APP_ID  IN VARCHAR2,
                                          P_ASSIGN_DEPT_ID IN VARCHAR2,
                                          P_ASSIGN_JOB_ID  IN VARCHAR2,
                                          P_RECORD_ID      IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_PER_APP_ASSIGNMENT_HDR AAH
       WHERE TRIM(AAH.ASSIGN_APP_ID) = TRIM(P_ASSIGN_APP_ID)
         AND TRIM(AAH.ASSIGN_DEPT_ID) = TRIM(P_ASSIGN_DEPT_ID)
         AND TRIM(AAH.ASSIGN_JOB_ID) = TRIM(P_ASSIGN_JOB_ID)
         AND AAH.ASSIGN_HDR_ID <> P_RECORD_ID
         AND AAH.WORKFLOW_COMPLETION_STATUS = '1'
         AND AAH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_APP_DEF_KRA;

  PROCEDURE PROC_ERR_MGR_APP_DEF_KRA(P_MODULE_CODE    IN VARCHAR2,
                                     P_SCREEN_CODE    IN VARCHAR2,
                                     P_ASSIGN_APP_ID  IN VARCHAR2,
                                     P_ASSIGN_DEPT_ID IN VARCHAR2,
                                     P_ASSIGN_JOB_ID  IN VARCHAR2,
                                     P_RECORD_ID      IN VARCHAR2,
                                     P_RET            OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_APP_DEF_KRA(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_ASSIGN_APP_ID,
                                      P_ASSIGN_DEPT_ID,
                                      P_ASSIGN_JOB_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_038');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_APP_DEF_KRA;

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_REQ(P_MODULE_CODE  IN VARCHAR2,
                                         P_SCREEN_CODE  IN VARCHAR2,
                                         P_RESIG_EMP_ID IN VARCHAR2,
                                         P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_RESIG_REQUEST_HDR RRH
       WHERE TRIM(RRH.RESIG_EMP_ID) = TRIM(P_RESIG_EMP_ID)
         AND RRH.RESIG_HDR_ID <> P_RECORD_ID
         AND RRH.WORKFLOW_COMPLETION_STATUS = '1'
         AND RRH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_RESIGN_REQ;

  PROCEDURE PROC_ERR_MGR_RESIGN_REQ(P_MODULE_CODE  IN VARCHAR2,
                                    P_SCREEN_CODE  IN VARCHAR2,
                                    P_RESIG_EMP_ID IN VARCHAR2,
                                    P_RECORD_ID    IN VARCHAR2,
                                    P_RET          OUT VARCHAR2) IS
  BEGIN

    GV_STR := '';

    IF FUNC_DUPLICATE_MGR_RESIGN_REQ(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_RESIG_EMP_ID,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_039');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_RESIGN_REQ;

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_CANC(P_MODULE_CODE       IN VARCHAR2,
                                          P_SCREEN_CODE       IN VARCHAR2,
                                          P_RESIG_CANC_EMP_ID IN VARCHAR2,
                                          P_RECORD_ID         IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_RESIG_CANCELLATION RC
       WHERE TRIM(RC.RESIG_HDR_ID) = TRIM(P_RESIG_CANC_EMP_ID)
         AND RC.RES_CANCEL_ID <> P_RECORD_ID
         AND RC.WORKFLOW_COMPLETION_STATUS = '1'
         AND RC.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_RESIGN_CANC;

  PROCEDURE PROC_ERR_MGR_RESIGN_CANC(P_MODULE_CODE       IN VARCHAR2,
                                     P_SCREEN_CODE       IN VARCHAR2,
                                     P_RESIG_CANC_EMP_ID IN VARCHAR2,
                                     P_RECORD_ID         IN VARCHAR2,
                                     P_RET               OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_RESIGN_CANC(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_RESIG_CANC_EMP_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_040');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_RESIGN_CANC;

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_INTR(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_RESIG_HDR_ID IN VARCHAR2,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_EXIT_INTERVIEW_HDR EIH
       WHERE TRIM(EIH.RESIG_HDR_ID) = TRIM(P_RESIG_HDR_ID)
         AND EIH.EX_INT_ID <> P_RECORD_ID
         AND EIH.WORKFLOW_COMPLETION_STATUS = '1'
         AND EIH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_RESIGN_INTR;

  PROCEDURE PROC_ERR_MGR_RESIGN_INTR(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_RESIG_HDR_ID IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_RESIGN_INTR(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_RESIG_HDR_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_041');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_RESIGN_INTR;

  FUNCTION FUNC_DUPLICATE_MGR_RESIGN_HAND(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_RESIG_HDR_ID IN VARCHAR2,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_HANDOVER_HDR HHH
       WHERE TRIM(HHH.RESIG_HDR_ID) = TRIM(P_RESIG_HDR_ID)
         AND HHH.HO_HDR_ID <> P_RECORD_ID
         AND HHH.WORKFLOW_COMPLETION_STATUS = '1'
         AND HHH.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_RESIGN_HAND;

  PROCEDURE PROC_ERR_MGR_RESIGN_HAND(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_RESIG_HDR_ID IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_RESIGN_HAND(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_RESIG_HDR_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_042');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_RESIGN_HAND;

  FUNCTION FUNC_DUPLICATE_MGR_HOLIDAY_MST(P_MODULE_CODE  IN VARCHAR2,
                                          P_SCREEN_CODE  IN VARCHAR2,
                                          P_HOLIDAY_DATE IN DATE,
                                          P_RECORD_ID    IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_HOLIDAYS_MASTER HM
       WHERE TRIM(HM.HOLIDAY_DATE) = TRIM(P_HOLIDAY_DATE)
         AND HM.HOLIDAY_ID <> P_RECORD_ID
         AND HM.WORKFLOW_COMPLETION_STATUS = '1'
         AND HM.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_HOLIDAY_MST;

  PROCEDURE PROC_ERR_MGR_HOLIDAY_MST(P_MODULE_CODE  IN VARCHAR2,
                                     P_SCREEN_CODE  IN VARCHAR2,
                                     P_HOLIDAY_DATE IN VARCHAR2,
                                     P_RECORD_ID    IN VARCHAR2,
                                     P_RET          OUT VARCHAR2) IS
  BEGIN
    P_RET        := '';
    GV_FROM_DATE := TO_DATE(P_HOLIDAY_DATE, 'DD-MM-YYYY');

    IF FUNC_DUPLICATE_MGR_HOLIDAY_MST(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      GV_FROM_DATE,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_043');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_HOLIDAY_MST;

  FUNCTION FUNC_DUPLICATE_MGR_TM_ATTN_SCH(P_MODULE_CODE     IN VARCHAR2,
                                          P_SCREEN_CODE     IN VARCHAR2,
                                          P_TIME_SCHDULE_ID IN VARCHAR2,
                                          P_RECORD_ID       IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM TM_SCHEDULES TS
       WHERE TRIM(TS.TM_SCH_CODE) = TRIM(P_TIME_SCHDULE_ID)
            --AND HM.HOLIDAY_ID <> P_RECORD_ID
         AND TS.WORKFLOW_COMPLETION_STATUS = '1'
         AND TS.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_TM_ATTN_SCH;

  PROCEDURE PROC_ERR_MGR_TM_ATTN_SCH(P_MODULE_CODE     IN VARCHAR2,
                                     P_SCREEN_CODE     IN VARCHAR2,
                                     P_TIME_SCHDULE_ID IN VARCHAR2,
                                     P_RECORD_ID       IN VARCHAR2,
                                     P_RET             OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_TM_ATTN_SCH(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_TIME_SCHDULE_ID,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_044');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_TM_ATTN_SCH;

  FUNCTION FUNC_DUPLICATE_MGR_TM_DEPT_SCH(P_MODULE_CODE   IN VARCHAR2,
                                          P_SCREEN_CODE   IN VARCHAR2,
                                          P_DEPT_ID       IN VARCHAR2,
                                          P_GROUP_ID      IN VARCHAR2,
                                          P_SCHEDULE_CODE IN VARCHAR2,
                                          P_RECORD_ID     IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM TM_DEPT_SCH DS
       WHERE TRIM(DS.TM_DEPT_ID) = TRIM(P_DEPT_ID)
         AND TRIM(DS.TM_GRP_CODE) = TRIM(P_GROUP_ID)
         AND TRIM(DS.TM_SCH_CODE) = TRIM(P_SCHEDULE_CODE)
         AND DS.TM_SCH_ID <> P_RECORD_ID
         AND DS.WORKFLOW_COMPLETION_STATUS = '1'
         AND DS.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_TM_DEPT_SCH;

  PROCEDURE PROC_ERR_MGR_TM_DEPT_SCH(P_MODULE_CODE   IN VARCHAR2,
                                     P_SCREEN_CODE   IN VARCHAR2,
                                     P_DEPT_ID       IN VARCHAR2,
                                     P_GROUP_ID      IN VARCHAR2,
                                     P_SCHEDULE_CODE IN VARCHAR2,
                                     P_RECORD_ID     IN VARCHAR2,
                                     P_RET           OUT VARCHAR2) IS
  BEGIN

    IF FUNC_DUPLICATE_MGR_TM_DEPT_SCH(P_MODULE_CODE,
                                      P_SCREEN_CODE,
                                      P_DEPT_ID,
                                      P_GROUP_ID,
                                      P_SCHEDULE_CODE,
                                      P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_045');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_TM_DEPT_SCH;

  FUNCTION FUNC_DUPLICATE_MGR_PERMISSION(P_MODULE_CODE IN VARCHAR2,
                                         P_SCREEN_CODE IN VARCHAR2,
                                         P_EMP_ID      IN VARCHAR2,
                                         P_FROM_DATE   IN DATE,
                                         P_TO_DATE     IN DATE,
                                         P_RECORD_ID   IN VARCHAR2)
    RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM HR_PERMISSIONS HP
       WHERE TRIM(HP.PER_EMP_ID) = TRIM(P_EMP_ID)
         AND TRIM(HP.PER_FROM_DT) = TRIM(P_FROM_DATE)
         AND TRIM(HP.PER_TO_DT) = TRIM(P_TO_DATE)
         AND HP.PER_ID <> P_RECORD_ID
         AND HP.WORKFLOW_COMPLETION_STATUS = '1'
         AND HP.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_PERMISSION;

  PROCEDURE PROC_ERR_MGR_PERMISSION(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    P_EMP_ID      IN VARCHAR2,
                                    P_FROM_DATE   IN VARCHAR2,
                                    P_TO_DATE     IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_RET         OUT VARCHAR2) IS
  BEGIN

    GV_FROM_DATE := TO_CHAR(TO_DATE(P_FROM_DATE, 'DD-MM-YYYY'));
    GV_TO_DATE   := TO_CHAR(TO_DATE(P_TO_DATE, 'DD-MM-YYYY'));

    IF FUNC_DUPLICATE_MGR_PERMISSION(P_MODULE_CODE,
                                     P_SCREEN_CODE,
                                     P_EMP_ID,
                                     GV_FROM_DATE,
                                     GV_TO_DATE,
                                     P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_046');
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_PERMISSION;

  PROCEDURE PROC_DELETE_MANAGER(P_MODULE_CODE IN VARCHAR2,
                                P_SCREEN_CODE IN VARCHAR2,
                                P_RECORD_ID   IN VARCHAR2,
                                P_RET         OUT VARCHAR2) IS
  BEGIN
    IF P_SCREEN_CODE IS NOT NULL AND P_RECORD_ID IS NOT NULL THEN
      BEGIN
        GV_STATUS := 0;
        FOR LOOP_RUN1 in (SELECT B.LIST_TABLE_NAME AS TABLE_NAME,
                                 A.PK_COLUMN_NAME  AS COLUMN_NAME
                            FROM SSM_MENU_ITEMS A, SSM_LIST_HDR B
                           WHERE A.FORM_CODE = B.LIST_SCREEN_CODE
                             AND TRIM(A.FORM_CODE) = TRIM(P_SCREEN_CODE))

         LOOP
          EXECUTE IMMEDIATE ' UPDATE ' || LOOP_RUN1.TABLE_NAME ||
                            Q'( SET ENABLED_FLAG = '0')' || ' WHERE ' ||
                            LOOP_RUN1.COLUMN_NAME || ' = ' || P_RECORD_ID;
        END LOOP;
        COMMIT;
        GV_STATUS := 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          GV_STR := 'No Valid Base table found!';
          P_RET  := GV_STR;

        WHEN TOO_MANY_ROWS THEN
          GV_STR := 'Many Base table names found!';
          P_RET  := GV_STR;

        WHEN OTHERS THEN
          P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
          P_RET := GV_STR;
      END;

    END IF;
    IF GV_STATUS = '1' THEN
      GV_STR := SSM.Get_Err_Message('EN', 'ERR_047');
      P_RET  := GV_STR;
    ELSE
      GV_STR := 'Unaccepted Action - Need Proper Data feeding';
      P_RET  := GV_STR;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error! ' || CHR(10) || SQLCODE || ' - ' || SQLERRM;
  END PROC_DELETE_MANAGER;

  FUNCTION FUNC_DUPLICATE_BANK_BRANCH(P_MODULE_CODE      IN VARCHAR2,
                                      P_SCREEN_CODE      IN VARCHAR2,
                                      P_BANK_ID          IN VARCHAR2,
                                      P_BANK_BRANCH_NAME IN VARCHAR2,
                                      P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER

   IS
  BEGIN
    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM CA_BANK_BRANCH BB
       WHERE BB.BANK_ID = P_BANK_ID
         AND TRIM(BB.BANK_BRANCH_NAME) = TRIM(P_BANK_BRANCH_NAME)
         AND BB.BANK_BRANCH_ID <> P_RECORD_ID
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_BANK_BRANCH;

  PROCEDURE PROC_ERR_MGR_BANK_BRANCH(P_MODULE_CODE      IN VARCHAR2,
                                     P_SCREEN_CODE      IN VARCHAR2,
                                     P_BANK_ID          IN VARCHAR2,
                                     P_BANK_BRANCH_NAME IN VARCHAR2,
                                     P_RECORD_ID        IN VARCHAR2,

                                     P_RET OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_BANK_BRANCH(P_MODULE_CODE,
                                  P_SCREEN_CODE,
                                  P_BANK_ID,
                                  P_BANK_BRANCH_NAME,
                                  P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_049');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_BANK_BRANCH;

  FUNCTION FUNC_DUPLICATE_BANK_ACCOUNTS(P_MODULE_CODE      IN VARCHAR2,
                                        P_SCREEN_CODE      IN VARCHAR2,
                                        P_BANK_ID          IN VARCHAR2,
                                        P_BANK_BRANCH_NAME IN VARCHAR2,
                                        P_ACCOUNT_CODE     IN VARCHAR2,
                                        P_RECORD_ID        IN VARCHAR2)
    RETURN NUMBER

   IS
  BEGIN
    BEGIN

      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM CA_BANK_ACCOUNTS BA
       WHERE BA.BANK_ID = P_BANK_ID
         AND BA.BANK_BRANCH_ID = P_BANK_BRANCH_NAME
         AND BA.VENDOR_BANK_ACCOUNT_CODE = P_ACCOUNT_CODE
         AND BA.ACCOUNT_ID <> P_RECORD_ID
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND ENABLED_FLAG = '1';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_BANK_ACCOUNTS;

  PROCEDURE PROC_ERR_MGR_BANK_ACCOUNTS(P_MODULE_CODE      IN VARCHAR2,
                                       P_SCREEN_CODE      IN VARCHAR2,
                                       P_BANK_ID          IN VARCHAR2,
                                       P_BANK_BRANCH_NAME IN VARCHAR2,
                                       P_ACCOUNT_CODE     IN VARCHAR2,
                                       P_RECORD_ID        IN VARCHAR2,

                                       P_RET OUT VARCHAR2) IS

  BEGIN

    IF FUNC_DUPLICATE_BANK_ACCOUNTS(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_BANK_ID,
                                    P_BANK_BRANCH_NAME,
                                    P_ACCOUNT_CODE,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_050');

    END IF;

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_BANK_ACCOUNTS;

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_NAME(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_ITEM_NAME   IN VARCHAR2,
                                        P_ORG_ID      IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)

   RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM INV_ITEM_MASTER IM
       WHERE TRIM(IM.ITEM_NAME) = TRIM(P_ITEM_NAME)
         AND IM.ORG_ID = P_ORG_ID
         AND IM.ITEM_ID <> P_RECORD_ID
         AND IM.WORKFLOW_COMPLETION_STATUS = '1'
         AND IM.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_ITEM_NAME;

  FUNCTION FUNC_DUPLICATE_MGR_ITEM_CODE(P_MODULE_CODE IN VARCHAR2,
                                        P_SCREEN_CODE IN VARCHAR2,
                                        P_ITEM_CODE   IN VARCHAR2,
                                        P_ORG_ID      IN VARCHAR2,
                                        P_RECORD_ID   IN VARCHAR2)

   RETURN NUMBER IS

  BEGIN

    BEGIN
      SELECT NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        FROM INV_ITEM_MASTER IM
       WHERE TRIM(IM.ITEM_CODE) = TRIM(P_ITEM_CODE)
         AND IM.ORG_ID = P_ORG_ID
         AND IM.ITEM_ID <> P_RECORD_ID
         AND IM.WORKFLOW_COMPLETION_STATUS = '1'
         AND IM.ENABLED_FLAG = '1';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_DUPLICATE_MGR_ITEM_CODE;

  PROCEDURE PROC_ERR_MGR_ITEM_MASTER(P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     P_ITEM_CODE   IN VARCHAR2,
                                     P_ITEM_NAME   IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_RET         OUT VARCHAR2) IS
  BEGIN
    GV_STR := '';
    IF FUNC_DUPLICATE_MGR_ITEM_NAME(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_ITEM_NAME,
                                    P_ORG_ID,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := SSM.Get_Err_Message('EN', 'ERR_051');
    END IF;

    IF FUNC_DUPLICATE_MGR_ITEM_CODE(P_MODULE_CODE,
                                    P_SCREEN_CODE,
                                    P_ITEM_CODE,
                                    P_ORG_ID,
                                    P_RECORD_ID) = 1 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_052');

    END IF;

    P_RET := NVL(GV_STR, 0);

    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_MGR_ITEM_MASTER;

  FUNCTION FUNC_ERR_ACC_CAL_DTL_EDIT(p_cal_dtl_id IN VARCHAR2,
                                     P_ORG_ID     IN VARCHAR2,
                                     P_RECORD_ID  IN VARCHAR2) RETURN NUMBER IS
  BEGIN
    BEGIN

      select NVL(sum(test.counts), 0) as counts
        INTO GV_COUNT
        from (select count(1) as counts
                from GL_BUDGET_HDR
               where BUDGET_COMP = P_ORG_ID
                 and cal_dtl_id = p_cal_dtl_id
                 and ENABLED_FLAG = '1'
              union
              select count(1) as counts
                from gl_comp_acct_period_dtl
               where COMP_ID = P_ORG_ID
                 and cal_dtl_id = p_cal_dtl_id
                 and ENABLED_FLAG = '1'
              union
              select count(1) as counts
                from gl_comp_acct_calendar_dtl
               where COMP_ID = P_ORG_ID
                 and cal_dtl_id = p_cal_dtl_id
                 and ENABLED_FLAG = '1') test;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;

    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_ERR_ACC_CAL_DTL_EDIT;
  PROCEDURE PROC_ERR_ACC_CAL_DTL_EDIT(P_MODULE_CODE IN VARCHAR2,
                                      P_SCREEN_CODE IN VARCHAR2,
                                      p_cal_dtl_id  IN VARCHAR2,
                                      P_RECORD_ID   IN VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2,
                                      P_RET         OUT VARCHAR2)

   IS

  BEGIN

    /* IF FUNC_ERR_ACC_CAL_DTL_EDIT(p_cal_dtl_id, P_ORG_ID, P_RECORD_ID) = 1 THEN

    --  GV_STR := SSM.Get_Err_Message('EN', 'ERR_101');

    END IF;*/

    P_RET := NVL(GV_STR, 0);

  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_ACC_CAL_DTL_EDIT;

  FUNCTION FUNC_ERR_IS_PERIOD_AVIAL(P_MODULE_CODE IN VARCHAR2,
                                    P_SCREEN_CODE IN VARCHAR2,
                                    p_data_id     IN VARCHAR2,
                                    P_RECORD_ID   IN VARCHAR2,
                                    P_ORG_ID      IN VARCHAR2,
                                    P_TRANS_DATE  IN VARCHAR2) RETURN NUMBER IS
  BEGIN
 --  GV_COUNT :=null;
    BEGIN
      select NVL(TO_NUMBER(COUNT(*)), 0)
        INTO GV_COUNT
        from gl_comp_acct_period_dtl gg
       where gg.enabled_flag = '1'
         and gg.workflow_completion_status = '1'
         and gg.PERIOD_STATUS = 'OPEN'
         and gg.COMP_ID = P_ORG_ID
         and (to_date(P_TRANS_DATE,'dd/MM/yyyy')) between gg.period_from_dt and gg.period_to_dt
       order by gg.PERIOD_NAME asc;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GV_COUNT := 0;
      WHEN TOO_MANY_ROWS THEN
        GV_COUNT := -1;
      WHEN OTHERS THEN
        GV_COUNT := -1;
    END;
 -- debug_proc(GV_COUNT||'count'||P_ORG_ID);
    IF GV_COUNT > 0 THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN(-1);
  END FUNC_ERR_IS_PERIOD_AVIAL;

  PROCEDURE PROC_ERR_IS_PERIOD_AVIAL(P_MODULE_CODE IN VARCHAR2,
                                     P_SCREEN_CODE IN VARCHAR2,
                                     p_data_id     IN VARCHAR2,
                                     P_RECORD_ID   IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2,
                                     P_TRANS_DATE  IN VARCHAR2,
                                     P_RET         OUT VARCHAR2)

   IS

  BEGIN
    GV_STR := '';
    IF FUNC_ERR_IS_PERIOD_AVIAL(P_MODULE_CODE,
                                P_SCREEN_CODE,
                                p_data_id,
                                P_RECORD_ID,
                                P_ORG_ID,
                                P_TRANS_DATE) = 0 THEN

      GV_STR := GV_STR || CHR(10) || SSM.Get_Err_Message('EN', 'ERR_200');
    END IF;

    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_ERR_IS_PERIOD_AVIAL;

END PKG_VALIDATIONS;
/
