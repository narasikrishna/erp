CREATE OR REPLACE PACKAGE PKG_FROM_END_ACTIVE_VALDN IS

  PROCEDURE PROC_FROM_DATE_VALDN(P_SCREEN_CODE IN VARCHAR2,
                                 P_PK_ID       IN VARCHAR2,
                                 P_FROM_DATE   IN VARCHAR2,
                                 P_TO_DATE     IN VARCHAR2,
                                 P_ACTIVE      IN NUMBER,
                                 P_RET         OUT VARCHAR2);

  FUNCTION FN_CHECK_ACTIVE(P_TABLE_NAME  IN VARCHAR2,
                           P_PK_COL_NAME IN VARCHAR2,
                           P_PK_ID       IN VARCHAR2,
                           P_COL_NAME    IN VARCHAR2,
                           P_ACTIVE      IN NUMBER) RETURN NUMBER;

  FUNCTION FN_CHECK_FROM_DATE(P_TABLE_NAME  IN VARCHAR2,
                              P_PK_COL_NAME IN VARCHAR2,
                              P_PK_ID       IN VARCHAR2,
                              P_COL_NAME    IN VARCHAR2,
                              P_FROM_DATE   IN VARCHAR2) RETURN NUMBER;

  FUNCTION FN_CHECK_END_DATE(P_TABLE_NAME  IN VARCHAR2,
                             P_PK_COL_NAME IN VARCHAR2,
                             P_PK_ID       IN VARCHAR2,
                             P_COL_NAME    IN VARCHAR2,
                             P_TO_DATE     IN VARCHAR2) RETURN NUMBER;

END PKG_FROM_END_ACTIVE_VALDN;
 
/
CREATE OR REPLACE PACKAGE BODY PKG_FROM_END_ACTIVE_VALDN AS

  PROCEDURE PROC_FROM_DATE_VALDN(P_SCREEN_CODE IN VARCHAR2,
                                 P_PK_ID       IN VARCHAR2,
                                 P_FROM_DATE   IN VARCHAR2,
                                 P_TO_DATE     IN VARCHAR2,
                                 P_ACTIVE      IN NUMBER,
                                 P_RET         OUT VARCHAR2) IS
    v_table_name VARCHAR2(100);
    GV_STR       NVARCHAR2(4000) := NULL;

    v_active_valdn_rec SSM_ACTIVE_VALIDATION%rowtype;

    CURSOR c_valdn_data IS
      SELECT SAV.FRM_DT_COL_NM,
             SAV.TO_DT_COL_NM,
             SAV.ACTIVE_COL_NM,
             SAV.COLUMN_NM,
             SAV.FROM_DT_ERR,
             SAV.TO_DT_ERR,
             SAV.ACTIVE_ERR,
             SAV.TABLE_NM,
             SAV.TO_SCREEN_CODE
        FROM SSM_ACTIVE_VALIDATION SAV
       WHERE SAV.FRM_SCREEN_CODE = P_SCREEN_CODE;

  BEGIN

    FOR r_active IN c_valdn_data LOOP

      IF (r_active.table_nm IS NOT NULL) THEN
        v_table_name := r_active.table_nm;
      ELSE
        BEGIN
          SELECT SS.TABLE_NAME
            INTO v_table_name
            FROM SSM_SCREENS SS
           WHERE SS.SCREEN_CODE = r_active.TO_SCREEN_CODE;
        EXCEPTION
          WHEN OTHERS THEN
            P_RET        := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
            v_table_name := 0;
        END;
      END IF;
      IF (P_ACTIVE != 1 and P_ACTIVE IS NOT NULL) THEN
        IF (FN_CHECK_ACTIVE(v_table_name,
                            r_active.Column_Nm,
                            P_PK_ID,
                            r_active.Active_Col_Nm,
                            P_ACTIVE) = 1) THEN
          GV_STR := SSM.Get_Err_Message('EN', r_active.ACTIVE_ERR);
        END IF;
      ELSIF (P_FROM_DATE IS NOT NULL) THEN
        IF (FN_CHECK_FROM_DATE(v_table_name,
                               r_active.Column_Nm,
                               P_PK_ID,
                               r_active.frm_dt_col_nm,
                               P_FROM_DATE) = 1) THEN
          GV_STR := SSM.Get_Err_Message('EN', r_active.from_dt_err);
        ELSIF (P_TO_DATE iS NOT NULL) THEN
          IF (FN_CHECK_END_DATE(v_table_name,
                                r_active.Column_Nm,
                                P_PK_ID,
                                r_active.to_dt_col_nm,
                                P_TO_DATE) = 1) THEN
            GV_STR := SSM.Get_Err_Message('EN', r_active.to_dt_err);
          END IF;
        END IF;
      END IF;
      P_RET := NVL(GV_STR, 0);
      EXIT WHEN GV_STR IS NOT NULL;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_FROM_DATE_VALDN;

  FUNCTION FN_CHECK_FROM_DATE(P_TABLE_NAME  IN VARCHAR2,
                              P_PK_COL_NAME IN VARCHAR2,
                              P_PK_ID       IN VARCHAR2,
                              P_COL_NAME    IN VARCHAR2,
                              P_FROM_DATE   IN VARCHAR2) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
  BEGIN

    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '= ' || '''' ||
                        P_PK_ID || '''' || ' and ' || P_COL_NAME || '<' || '''' ||
                        P_FROM_DATE || '''' || ' and ENABLED_FLAG = 1';

    EXECUTE IMMEDIATE lv_sql_statement
      INTO v_count;

    IF (v_count >= 1) THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_CHECK_FROM_DATE;

  FUNCTION FN_CHECK_ACTIVE(P_TABLE_NAME  IN VARCHAR2,
                           P_PK_COL_NAME IN VARCHAR2,
                           P_PK_ID       IN VARCHAR2,
                           P_COL_NAME    IN VARCHAR2,
                           P_ACTIVE      IN NUMBER) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
  BEGIN

    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '= ' || '''' ||
                        P_PK_ID || '''' || ' and ENABLED_FLAG = 1';

    EXECUTE IMMEDIATE lv_sql_statement
      INTO v_count;

    IF (v_count >= 1) THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_CHECK_ACTIVE;

  FUNCTION FN_CHECK_END_DATE(P_TABLE_NAME  IN VARCHAR2,
                             P_PK_COL_NAME IN VARCHAR2,
                             P_PK_ID       IN VARCHAR2,
                             P_COL_NAME    IN VARCHAR2,
                             P_TO_DATE     IN VARCHAR2) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
  BEGIN

    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '= ' || '''' ||
                        P_PK_ID || '''' || ' and (' || P_COL_NAME ||
                        ' IS NULL OR ' || P_COL_NAME || '>' || '''' ||
                        P_TO_DATE || '''' || ')' || ' and ENABLED_FLAG = 1';

    EXECUTE IMMEDIATE lv_sql_statement
      INTO v_count;

    IF (v_count >= 1) THEN
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_CHECK_END_DATE;

END PKG_FROM_END_ACTIVE_VALDN;
/
