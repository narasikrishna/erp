CREATE OR REPLACE PACKAGE Workflow_Handler IS
  /*
  * Package  Name         : workflow_handler
  * Author    : Natesh
  * Date Written      : 02/04/2014
  * Description       : This is Package Defination. It Contains Various
  *      Procedure and Functions(Public , Private)  to
  *       WorkFlow in ERP System.
  *
  * Version No.  Date  Brief description of change
  * ---------------------------------------------------------------------------------------------
  * 1.0   02/04/2014 Original Version
  *
   * ---------------------------------------------------------------------------------------------
  */
  --
  -- This function is called to check if a workflow is defined on a table or not.
  -- This Function will be called in Pre-Insert of every Table
  -- will return 1 if workflow is not defined on table or Transaction
  -- will return 0 if a workflow is define.
  -- and Insert Record in monitor Table
  --
  --
  FUNCTION get_workflow_status(form_trn_name VARCHAR2,
                               login_user    VARCHAR2,
                               p_key         VARCHAR2,
                               P_ORG_ID      varchar2) RETURN NUMBER;

  FUNCTION get_workflow_status(form_trn_name VARCHAR2,
                               login_user    VARCHAR2,
                               p_key         VARCHAR2,
                               p_message     VARCHAR2) RETURN NUMBER;

  FUNCTION get_workflow_status(form_trn_name        VARCHAR2,
                               login_user           VARCHAR2,
                               p_key                VARCHAR2,
                               p_message            VARCHAR2,
                               p_transaction_amount VARCHAR2) RETURN NUMBER;

  FUNCTION get_workflow_m_status(form_trn_name        VARCHAR2,
                                 login_user           VARCHAR2,
                                 p_key                VARCHAR2,
                                 p_message            VARCHAR2,
                                 p_transaction_amount VARCHAR2) RETURN NUMBER;

  FUNCTION get_workflow_status_amt(form_trn_name        VARCHAR2,
                                   login_user           VARCHAR2,
                                   p_key                VARCHAR2,
                                   p_message            VARCHAR2,
                                   p_transaction_amount NUMBER) RETURN NUMBER;

  FUNCTION GET_WORKFLOW_GROUP(P_SCREEN_CODE      IN VARCHAR2,
                              P_TRANSACTION_CODE IN VARCHAR2) RETURN VARCHAR2;

  --
  -- It Returnd True if the Login User Can approve
  -- the current Record Else False
  -- It also Take Care of whther the approver Turn has come or Not
  -- Called at many Instance to check The Status
  --
  FUNCTION is_approver(form_trn_name VARCHAR2,
                       login_user    VARCHAR2,
                       p_key         VARCHAR2,
                       p_level_code  NUMBER) RETURN BOOLEAN;

  --
  --
  --

  -----------------------------------------------------------------------------------------------------------------
  --------------- THIS PROCEDURE IS USED TO GET THE WORKFLOW MESSAGE FOR MENU SCREEN ------------------------------
  -----------------------------------------------------------------------------------------------------------------
  FUNCTION GET_WORKFLOW_MESSAGE(P_WORKFLOW_CODE    IN VARCHAR2,
                                P_TRANSACTION_CODE IN VARCHAR2)
    RETURN VARCHAR2;

  -----------------------------------------------------------------------------------------------------------------
  -----------------------------------------------------------------------------------------------------------------

  PROCEDURE GET_WORKFLOW_MESSAGE(P_WORKFLOW_CODE    IN VARCHAR2,
                                 P_TRANSACTION_CODE IN VARCHAR2,
                                 P_WF_MESSAGE       OUT VARCHAR2);

  PROCEDURE update_wf_status(P_SCREEN_CODE      IN VARCHAR2,
                             P_STATUS           IN VARCHAR2,
                             P_USERID           IN VARCHAR2,
                             P_TRANSACTION_CODE IN VARCHAR2,
                             P_DESCRIPTION      IN VARCHAR2,
                             P_LEVEL_CODE       IN VARCHAR2,
                             P_RETURN_STATUS    OUT NUMBER);

  PROCEDURE ABOUT_THIS_RECORD(P_SCREEN_CODE      IN VARCHAR2,
                              P_TRANSACTION_CODE IN VARCHAR2,
                              P_ABT_INFO_1       OUT VARCHAR2,
                              P_ABT_INFO_2       OUT VARCHAR2,
                              P_ABT_INFO_3       OUT VARCHAR2,
                              P_ABT_INFO_4       OUT VARCHAR2,
                              P_ABT_INFO_5       OUT VARCHAR2,
                              P_ABT_INFO_6       OUT VARCHAR2,
                              P_ABT_INFO_7       OUT VARCHAR2);
END; -- *** End of Package defination workflow_handler
 
/
CREATE OR REPLACE PACKAGE BODY Workflow_Handler
IS
   /*
    * Package  Name         : workflow_handler
    * Author      : Reshad
    * Date Written    : 30/10/2002
    * Description         : This is Package Body. It Contains Various
    *         Procedure and Functions(Public )  to
    *           WorkFlow in DRC System.
    * Calling Programs      : All DRC Procedures
    * Called Programs     : nothing
    * Errors Raised   : Error Page of Session Expiry
    * Date Modified   : N/A
    * Modification History    :
    *
    * Version No.   Date    Brief description of change
    * ---------------------------------------------------------------------------------------------
    * 1.0     30/10/2002  Original Version
    *
    * ---------------------------------------------------------------------------------------------
   */
   --
   -- Local Procedures and Funtions Defination area
   --
   FUNCTION is_workflow_defined (form_trn_name VARCHAR2)
      RETURN BOOLEAN;

   --
   -- End of Defination area for Local Procedure and Function
   --
   /*--------------------------------------------------------------------*/
   FUNCTION GET_WORKFLOW_GROUP (P_SCREEN_CODE        IN VARCHAR2,
                                P_TRANSACTION_CODE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      V_FILTER_QUERY   VARCHAR2 (3000);
      V_GROUP          VARCHAR2 (5) := '00';
      V_TRANS_CODE     VARCHAR2 (100);
   BEGIN
      /*  SELECT FILTER_QUERY
           INTO V_FILTER_QUERY
           FROM WFM_WORKFLOW_FILTER
          WHERE SCREEN_CODE = P_SCREEN_CODE
            AND ENABLED_FLAG='1'
          AND WORKFLOW_COMPLETION_STATUS='1';
      */
      --    V_FILTER_QUERY := V_FILTER_QUERY ||''''||P_TRANSACTION_CODE||'''';

      --    EXECUTE IMMEDIATE V_FILTER_QUERY INTO V_TRANS_CODE;

      /*SELECT A.REFERENCE_NUMBER
      INTO V_TRANS_CODE
      FROM RM_PENSIONER_ENROLLMENT A,ASM_ASSESSMENT B WHERE A.PENSIONER_CODE = B.PENSIONER_CODE AND B.ASSESSMENT_CODE =P_TRANSACTION_CODE;
      */
      /*IF P_SCREEN_CODE = 'ASMF0002' THEN
      SELECT A.REFERENCE_NUMBER
      INTO V_TRANS_CODE
      FROM RM_PENSIONER_ENROLLMENT A,ASM_ASSESSMENT B WHERE A.PENSIONER_CODE = B.PENSIONER_CODE AND B.ASSESSMENT_CODE =P_TRANSACTION_CODE;
              V_GROUP := Clm.GET_GROUP_ASM(V_TRANS_CODE);
          ELSIF P_SCREEN_CODE = 'RMF0001' THEN

      SELECT A.REFERENCE_NUMBER
      INTO V_TRANS_CODE
      FROM RM_PENSIONER_ENROLLMENT A WHERE A.PENSIONER_NUMBER =P_TRANSACTION_CODE;

              V_GROUP := Clm.Get_Group(V_TRANS_CODE);
          END IF;*/
      RETURN V_GROUP;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '00';
   END;

   FUNCTION FINDING_DATATYPES (P_INPUT_VALUE VARCHAR2)
      RETURN CHAR
   IS
      v_number   NUMBER := 0;
      v_date     DATE;
   BEGIN
      BEGIN
         v_number := TO_NUMBER (P_INPUT_VALUE);
         RETURN ('N');
      EXCEPTION
         WHEN OTHERS
         THEN
            BEGIN
               v_date := TO_DATE (P_INPUT_VALUE, 'DD-MM-YYYY');
               RETURN ('D');
            EXCEPTION
               WHEN OTHERS
               THEN
                  RETURN ('C');
            END;
      END;
   END;

   FUNCTION get_workflow_status (form_trn_name    VARCHAR2,
                                 login_user       VARCHAR2,
                                 p_key            VARCHAR2,
                                 p_org_id         VARCHAR2)
      RETURN NUMBER
   IS
      wf_status              NUMBER := 1;
      is_last_rec            NUMBER := 0;
      rec_counter            NUMBER := 1;
      no_of_level            NUMBER := 0;
      v_already_approved     NUMBER := 0;
      v_remarks              VARCHAR2 (50);
      v_approval_on_modify   CHAR (1);
      v_approval_on_delete   CHAR (1);
      V_ROLE_CODE            WFM_WORKFLOW_LEVELS.ROLE_CODE%TYPE;

      CURSOR c_wf_level (p_form_trn_name VARCHAR2, p_deptcode VARCHAR2)
      IS
           SELECT *
             FROM WFM_WORKFLOW_LEVELS
            WHERE WORKFLOW_CODE = p_form_trn_name AND IS_MANDATORY = 1
         ORDER BY LEVEL_CODE;

      v_deptcode             VARCHAR2 (50);
   BEGIN
      -- Check if workflow is defined on a Table or Not
      IF is_workflow_defined (form_trn_name)
      THEN
         -- To Find No of Levels i
         SELECT NVL (MAX (APPROVED_ALREADY), -1)
           INTO v_already_approved
           FROM WFM_WORKFLOW_MONITORS
          WHERE WORKFLOW_CODE = form_trn_name AND TRANSACTION_CODE = p_key;

         IF v_approval_on_modify = 'N' AND v_already_approved > -1
         THEN
            RETURN wf_status;
         END IF;

         --dbms_output.put_line('deleteflag:'||p_deleteflag);
         DBMS_OUTPUT.put_line (
            'v_approval_on_delete:' || v_approval_on_delete);

         IF v_already_approved > -1
         THEN
            v_remarks := 'Record Modifier';
         ELSE
            v_remarks := 'Record Creator';
         END IF;

         v_already_approved := v_already_approved + 1;

         --This is modified by Senthil on 3-6-2003
         SELECT COUNT (*)
           INTO no_of_level
           FROM WFM_WORKFLOW_LEVELS
          WHERE WORKFLOW_CODE = form_trn_name --and UPPER(TRIM(dept_code)) =nvl(UPPER(TRIM(v_deptcode)),UPPER(TRIM(dept_code)))
                                             AND IS_MANDATORY = 1;

         DBMS_OUTPUT.put_line (
            'loop starts:' || form_trn_name || ', deptcode:' || v_deptcode);

         --
         -- Inserts a record into Monitor table
         --
         FOR wf_level IN c_wf_level (form_trn_name, v_deptcode)
         LOOP
            IF rec_counter = no_of_level
            THEN
               is_last_rec := 1;
            END IF;

            DBMS_OUTPUT.put_line ('rec_counter:' || rec_counter);

            --
            -- In Monitor Table
            -- Insert The Creator of The Record at '0' level
            -- Only add once this record i.e at level '0'
            --
            IF rec_counter = 1
            THEN
               BEGIN
                  SELECT ROLE_CODE
                    INTO V_ROLE_CODE
                    FROM SSM_USER_ROLE_INTERSECTS
                   WHERE     UPPER (USER_CODE) = UPPER (LOGIN_USER)
                         AND ENABLED_FLAG = '1'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     V_ROLE_CODE := 'ACCESSALL';
               END;

               -- CHANGES ENDS
               DBMS_OUTPUT.put_line (
                  'inserting into wfm_workflow_monitors ' || v_remarks);

               INSERT INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                  ROLE_CODE,
                                                  LEVEL_CODE,
                                                  TRANSACTION_CODE,
                                                  STATUS,
                                                  IS_THIS_FINAL,
                                                  WORKFLOW_EXECUTED_BY,
                                                  WORKFLOW_EXECUTED_DATE,
                                                  REMARKS,
                                                  WORKFLOW_COMPLETION_STATUS,
                                                  CREATED_BY,
                                                  CREATED_DATE,
                                                  MODIFIED_BY,
                                                  MODIFIED_DATE,
                                                  IS_MANDATORY,
                                                  APPROVED_ALREADY,
                                                  WORKFLOW_MESSAGE,
                                                  ORG_ID,
                                                  ENABLED_FLAG)
                       VALUES (
                                 wf_level.WORKFLOW_CODE,     --- WORKFLOW_CODE
                                 V_ROLE_CODE, --' REMOVED BY HARI ACCESSALL',  --- Default Role Code For all User which is
                                 0,                    --- Always at Level '0'
                                 p_key,  -- Primary Key for The Current Record
                                 'WFST01', -- Enter Status Approved By default the Creator always approves his Record
                                 0,    -- 0 indicates This is not Final record
                                 login_user,           -- WORKFLOW_EXECUTED_BY
                                 SYSDATE,            -- WORKFLOW_EXECUTED_DATE
                                 v_remarks,                         -- REMARKS
                                 1, -- WorkFlow Completion Status By default 1
                                 login_user,         -- Current user Logged In
                                 SYSDATE,                      -- Current Date
                                 NULL,
                                 NULL,
                                 1,             --  By Default it is Mandatory
                                 v_already_approved,
                                 NVL (
                                    Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                       wf_level.WORKFLOW_CODE,
                                       P_KEY)||' - '||P_KEY,
                                    P_KEY),
                                 P_ORG_ID,
                                 '1');
            END IF;

            --
            -- Insert all Other Level into Monitor Table
            --
            DBMS_OUTPUT.put_line (
               'inserting into wfm_workflow_monitors ' || wf_level.ROLE_CODE);

            INSERT INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                               ROLE_CODE,
                                               LEVEL_CODE,
                                               TRANSACTION_CODE,
                                               STATUS,
                                               IS_THIS_FINAL,
                                               WORKFLOW_EXECUTED_BY,
                                               WORKFLOW_EXECUTED_DATE,
                                               REMARKS,
                                               WORKFLOW_COMPLETION_STATUS,
                                               CREATED_BY,
                                               CREATED_DATE,
                                               MODIFIED_BY,
                                               MODIFIED_DATE,
                                               IS_MANDATORY,
                                               APPROVED_ALREADY,
                                               WORKFLOW_MESSAGE,
                                               ORG_ID,
                                               ENABLED_FLAG)
                    VALUES (
                              wf_level.WORKFLOW_CODE,          --WORKFLOW_CODE
                              wf_level.ROLE_CODE,
                              wf_level.LEVEL_CODE,
                              p_key,     -- Primary Key for The Current Record
                              'WFST03', -- Enter Status In Progress By default
                              is_last_rec,                    -- IS_THIS_FINAL
                              NULL,                    -- WORKFLOW_EXECUTED_BY
                              NULL,                 --  WORKFLOW_EXECUTED_DATE
                              NULL,                                 -- REMARKS
                              1,    -- WorkFlow Completion Status By default 1
                              login_user,            -- Current user Logged In
                              SYSDATE,
                              NULL,
                              NULL,
                              wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                              v_already_approved,
                              NVL (
                                 Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                    wf_level.WORKFLOW_CODE,
                                    P_KEY)||' - '||P_KEY,
                                 P_KEY),
                              P_ORG_ID,
                              '1');

            rec_counter := rec_counter + 1;
         END LOOP;

         wf_status := 0;
      END IF;

      -- COMMIT;
      RETURN wf_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20200, SQLERRM);
         RETURN NULL;
   END;                                              -- ***get_workflow_status

   FUNCTION get_workflow_status (form_trn_name    VARCHAR2,
                                 login_user       VARCHAR2,
                                 p_key            VARCHAR2,
                                 p_message        VARCHAR2)
      RETURN NUMBER
   IS
      wf_status              NUMBER := 1;
      is_last_rec            NUMBER := 0;
      rec_counter            NUMBER := 1;
      no_of_level            NUMBER := 0;
      v_already_approved     NUMBER := 0;
      v_remarks              VARCHAR2 (50);
      v_approval_on_modify   CHAR (1);
      v_approval_on_delete   CHAR (1);
      V_ROLE_CODE            WFM_WORKFLOW_LEVELS.ROLE_CODE%TYPE;
      v_branch_code          VARCHAR2 (50);

      CURSOR c_wf_level (p_form_trn_name VARCHAR2, p_deptcode VARCHAR2)
      IS
           SELECT *
             FROM WFM_WORKFLOW_LEVELS
            WHERE WORKFLOW_CODE = p_form_trn_name AND IS_MANDATORY = 1
         ORDER BY LEVEL_CODE;

      v_deptcode             VARCHAR2 (50);
   BEGIN
      -- Check if workflow is defined on a Table or Not
      IF is_workflow_defined (form_trn_name)
      THEN
         -- To Find No of Levels i
         SELECT NVL (MAX (APPROVED_ALREADY), -1)
           INTO v_already_approved
           FROM WFM_WORKFLOW_MONITORS
          WHERE WORKFLOW_CODE = form_trn_name AND TRANSACTION_CODE = p_key;

         IF v_approval_on_modify = 'N' AND v_already_approved > -1
         THEN
            RETURN wf_status;
         END IF;

         --dbms_output.put_line('deleteflag:'||p_deleteflag);
         DBMS_OUTPUT.put_line (
            'v_approval_on_delete:' || v_approval_on_delete);

         IF v_already_approved > -1
         THEN
            v_remarks := 'Record Modifier';
         ELSE
            v_remarks := 'Record Creator';
         END IF;

         v_already_approved := v_already_approved + 1;

         SELECT COUNT (*)
           INTO no_of_level
           FROM WFM_WORKFLOW_LEVELS
          WHERE WORKFLOW_CODE = form_trn_name AND IS_MANDATORY = 1;

         DBMS_OUTPUT.put_line (
            'loop starts:' || form_trn_name || ', deptcode:' || v_deptcode);

         --
         -- Inserts a record into Monitor table
         --
         FOR wf_level IN c_wf_level (form_trn_name, v_deptcode)
         LOOP
            IF rec_counter = no_of_level
            THEN
               is_last_rec := 1;
            END IF;

            DBMS_OUTPUT.put_line ('rec_counter:' || rec_counter);

            --
            -- In Monitor Table
            -- Insert The Creator of The Record at '0' level
            -- Only add once this record i.e at level '0'
            --
            IF rec_counter = 1
            THEN
               BEGIN
                  SELECT ROLE_CODE
                    INTO V_ROLE_CODE
                    FROM SSM_USER_ROLE_INTERSECTS
                   WHERE     UPPER (USER_CODE) = UPPER (LOGIN_USER)
                         AND ENABLED_FLAG = '1'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     V_ROLE_CODE := 'ACCESSALL';
               END;

               -- CHANGES ENDS
               DBMS_OUTPUT.put_line (
                  'inserting into wfm_workflow_monitors ' || v_remarks);

               -- Added by Sakthivel to get branch_code on 09-dec-2011
               IF wf_level.branch_code IS NULL
               THEN
                  BEGIN
                     SELECT su.GROUP_ID
                       INTO v_branch_code
                       FROM ssm_users su
                      WHERE su.user_code = login_user;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_branch_code := NULL;
                  END;
               ELSE
                  v_branch_code := wf_level.branch_code;
               END IF;

               INSERT INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                  ROLE_CODE,
                                                  LEVEL_CODE,
                                                  TRANSACTION_CODE,
                                                  STATUS,
                                                  IS_THIS_FINAL,
                                                  WORKFLOW_EXECUTED_BY,
                                                  WORKFLOW_EXECUTED_DATE,
                                                  REMARKS,
                                                  WORKFLOW_COMPLETION_STATUS,
                                                  CREATED_BY,
                                                  CREATED_DATE,
                                                  MODIFIED_BY,
                                                  MODIFIED_DATE,
                                                  IS_MANDATORY,
                                                  APPROVED_ALREADY,
                                                  WORKFLOW_MESSAGE) --,       BRANCH_CODE)-- Added by Sakthivel on 09-dec-2011
                       VALUES (
                                 wf_level.WORKFLOW_CODE,     --- WORKFLOW_CODE
                                 V_ROLE_CODE, --' REMOVED BY HARI ACCESSALL',  --- Default Role Code For all User which is
                                 0,                    --- Always at Level '0'
                                 p_key,  -- Primary Key for The Current Record
                                 'WFST01', -- Enter Status Approved By default the Creator always approves his Record
                                 0,    -- 0 indicates This is not Final record
                                 login_user,           -- WORKFLOW_EXECUTED_BY
                                 SYSDATE,            -- WORKFLOW_EXECUTED_DATE
                                 v_remarks,                         -- REMARKS
                                 1, -- WorkFlow Completion Status By default 1
                                 login_user,         -- Current user Logged In
                                 SYSDATE,                      -- Current Date
                                 NULL,
                                 NULL,
                                 1,             --  By Default it is Mandatory
                                 v_already_approved,
                                 NVL (
                                    NVL (
                                       Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                          wf_level.WORKFLOW_CODE,
                                          P_KEY),
                                       P_MESSAGE),
                                    P_KEY));                --,v_branch_code);
            END IF;

            --
            -- Insert all Other Level into Monitor Table
            --
            DBMS_OUTPUT.put_line (
               'inserting into wfm_workflow_monitors ' || wf_level.ROLE_CODE);

            IF wf_level.branch_code IS NULL
            THEN
               BEGIN
                  SELECT su.GROUP_ID
                    INTO v_branch_code
                    FROM ssm_users su
                   WHERE su.user_code = login_user;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_branch_code := NULL;
               END;

               IF v_branch_code = 'HQ'
               THEN
                  v_role_code := wf_level.ROLE_CODE;
               ELSE
                  v_role_code := 'BRMGR';
               END IF;
            ELSE
               v_branch_code := wf_level.branch_code;
               v_role_code := wf_level.ROLE_CODE;
            END IF;

            INSERT INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                               ROLE_CODE,
                                               LEVEL_CODE,
                                               TRANSACTION_CODE,
                                               STATUS,
                                               IS_THIS_FINAL,
                                               WORKFLOW_EXECUTED_BY,
                                               WORKFLOW_EXECUTED_DATE,
                                               REMARKS,
                                               WORKFLOW_COMPLETION_STATUS,
                                               CREATED_BY,
                                               CREATED_DATE,
                                               MODIFIED_BY,
                                               MODIFIED_DATE,
                                               IS_MANDATORY,
                                               APPROVED_ALREADY,
                                               WORKFLOW_MESSAGE)
                 --,BRANCH_CODE)-- Added by Sakthivel on 09-dec-2011
                 VALUES (wf_level.WORKFLOW_CODE,               --WORKFLOW_CODE
                         v_ROLE_CODE,
                         wf_level.LEVEL_CODE,
                         p_key,          -- Primary Key for The Current Record
                         'WFST03',      -- Enter Status In Progress By default
                         is_last_rec,                         -- IS_THIS_FINAL
                         NULL,                         -- WORKFLOW_EXECUTED_BY
                         NULL,                      --  WORKFLOW_EXECUTED_DATE
                         NULL,                                      -- REMARKS
                         1,         -- WorkFlow Completion Status By default 1
                         login_user,                 -- Current user Logged In
                         SYSDATE,
                         NULL,
                         NULL,
                         WF_LEVEL.IS_MANDATORY, -- Is this Check is mandaory or not
                         V_ALREADY_APPROVED --, nvl(nvl(Workflow_Handler.GET_WORKFLOW_MESSAGE(wf_level.WORKFLOW_CODE,P_MESSAGE),P_KEY)
                                           ,
                         v_branch_code);

            rec_counter := rec_counter + 1;
         END LOOP;

         wf_status := 0;
      END IF;

      -- COMMIT;
      RETURN wf_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20200, SQLERRM);
         RETURN NULL;
   END;                                              -- ***get_workflow_status

   FUNCTION get_workflow_status (form_trn_name           VARCHAR2,
                                 login_user              VARCHAR2,
                                 p_key                   VARCHAR2,
                                 p_message               VARCHAR2,
                                 p_transaction_amount    VARCHAR2)
      RETURN NUMBER
   IS
      wf_status               NUMBER := 1;
      is_last_rec             NUMBER := 0;
      rec_counter             NUMBER := 1;
      no_of_level             NUMBER := 0;
      v_already_approved      NUMBER := 0;
      v_remarks               VARCHAR2 (50);
      v_approval_on_modify    CHAR (1);
      v_approval_on_delete    CHAR (1);
      V_ROLE_CODE             WFM_WORKFLOW_LEVELS.ROLE_CODE%TYPE;
      v_branch_code           VARCHAR (3);
      v_cnd_datatype          CHAR;

      CURSOR c_wf_level (p_form_trn_name VARCHAR2, p_deptcode VARCHAR2)
      IS
           SELECT *
             FROM WFM_WORKFLOW_LEVELS
            WHERE WORKFLOW_CODE = p_form_trn_name AND IS_MANDATORY = 1
         ORDER BY LEVEL_CODE;

      v_deptcode              VARCHAR2 (50);
      v_level_code_is_final   NUMBER (2);
   BEGIN
      v_level_code_is_final := 0;

      -- Check if workflow is defined on a Table or Not
      IF is_workflow_defined (form_trn_name)
      THEN
         -- To Find No of Levels i
         SELECT NVL (MAX (APPROVED_ALREADY), -1)
           INTO v_already_approved
           FROM WFM_WORKFLOW_MONITORS
          WHERE WORKFLOW_CODE = form_trn_name AND TRANSACTION_CODE = p_key;

         IF v_approval_on_modify = 'N' AND v_already_approved > -1
         THEN
            RETURN wf_status;
         END IF;

         --dbms_output.put_line('deleteflag:'||p_deleteflag);
         DBMS_OUTPUT.put_line (
            'v_approval_on_delete:' || v_approval_on_delete);

         IF v_already_approved > -1
         THEN
            v_remarks := 'Record Modifier';
         ELSE
            v_remarks := 'Record Creator';
         END IF;

         v_already_approved := v_already_approved + 1;

         SELECT COUNT (*)
           INTO no_of_level
           FROM WFM_WORKFLOW_LEVELS
          WHERE WORKFLOW_CODE = form_trn_name AND IS_MANDATORY = 1;

         DBMS_OUTPUT.put_line (
            'loop starts:' || form_trn_name || ', deptcode:' || v_deptcode);

         --
         -- Inserts a record into Monitor table
         --
         FOR wf_level IN c_wf_level (form_trn_name, v_deptcode)
         LOOP
            IF rec_counter = no_of_level
            THEN
               is_last_rec := 1;
            END IF;

            DBMS_OUTPUT.put_line ('rec_counter:' || rec_counter);

            --
            -- In Monitor Table
            -- Insert The Creator of The Record at '0' level
            -- Only add once this record i.e at level '0'
            --
            IF rec_counter = 1
            THEN
               BEGIN
                  SELECT ROLE_CODE
                    INTO V_ROLE_CODE
                    FROM SSM_USER_ROLE_INTERSECTS
                   WHERE     UPPER (USER_CODE) = UPPER (LOGIN_USER)
                         AND ENABLED_FLAG = '1'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     V_ROLE_CODE := 'ACCESSALL';
               END;

               -- CHANGES ENDS
               DBMS_OUTPUT.put_line (
                  'inserting into wfm_workflow_monitors ' || v_remarks);

               -- Added by Sakthivel to get branch_code on 09-dec-2011
               IF wf_level.branch_code IS NULL
               THEN
                  BEGIN
                     SELECT su.GROUP_ID
                       INTO v_branch_code
                       FROM ssm_users su
                      WHERE su.user_code = login_user;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_branch_code := NULL;
                  END;
               ELSE
                  v_branch_code := wf_level.branch_code;
               END IF;

               IF NVL (wf_level.escalation_value, 0) <=
                     NVL (p_transaction_amount, 0)
               THEN
                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     --,BRANCH_CODE)-- Added by Sakthivel on 09-dec-2011
                     VALUES (
                               wf_level.WORKFLOW_CODE,       --- WORKFLOW_CODE
                               V_ROLE_CODE, --' REMOVED BY HARI ACCESSALL',  --- Default Role Code For all User which is
                               0,                      --- Always at Level '0'
                               p_key,    -- Primary Key for The Current Record
                               'WFST01', -- Enter Status Approved By default the Creator always approves his Record
                               0,      -- 0 indicates This is not Final record
                               login_user,             -- WORKFLOW_EXECUTED_BY
                               SYSDATE,              -- WORKFLOW_EXECUTED_DATE
                               v_remarks,                           -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,                        -- Current Date
                               NULL,
                               NULL,
                               1,               --  By Default it is Mandatory
                               V_ALREADY_APPROVED,
                               NVL (
                                  WORKFLOW_HANDLER.GET_WORKFLOW_MESSAGE (
                                     WF_LEVEL.WORKFLOW_CODE,
                                     P_MESSAGE),
                                  P_KEY));
               END IF;
            END IF;

            --
            -- Insert all Other Level into Monitor Table
            --
            DBMS_OUTPUT.put_line (
               'inserting into wfm_workflow_monitors ' || wf_level.ROLE_CODE);
            v_cnd_datatype := finding_datatypes (wf_level.escalation_value);

            IF v_cnd_datatype = 'N'
            THEN
               IF wf_level.escalation_value IS NOT NULL
               THEN
                  IF NVL (TO_NUMBER (wf_level.escalation_value), 0) <=
                        NVL (TO_NUMBER (p_transaction_amount), 0)
                  THEN
                     IF wf_level.branch_code IS NULL
                     THEN
                        BEGIN
                           SELECT su.GROUP_ID
                             INTO v_branch_code
                             FROM ssm_users su
                            WHERE su.user_code = login_user;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              v_branch_code := NULL;
                        END;

                        IF v_branch_code = 'HQ'
                        THEN
                           v_role_code := wf_level.ROLE_CODE;
                        ELSE
                           v_role_code := 'BRMGR';
                        END IF;
                     ELSE
                        v_branch_code := wf_level.branch_code;
                        v_role_code := wf_level.ROLE_CODE;
                     END IF;

                     v_level_code_is_final := v_level_code_is_final + 1;

                     INSERT
                       INTO WFM_WORKFLOW_MONITORS (
                               WORKFLOW_CODE,
                               ROLE_CODE,
                               LEVEL_CODE,
                               TRANSACTION_CODE,
                               STATUS,
                               IS_THIS_FINAL,
                               WORKFLOW_EXECUTED_BY,
                               WORKFLOW_EXECUTED_DATE,
                               REMARKS,
                               WORKFLOW_COMPLETION_STATUS,
                               CREATED_BY,
                               CREATED_DATE,
                               MODIFIED_BY,
                               MODIFIED_DATE,
                               IS_MANDATORY,
                               APPROVED_ALREADY,
                               WORKFLOW_MESSAGE)
                        VALUES (
                                  wf_level.WORKFLOW_CODE,      --WORKFLOW_CODE
                                  v_role_code,
                                  v_level_code_is_final, --wf_level.LEVEL_CODE,
                                  p_key, -- Primary Key for The Current Record
                                  'WFST03', -- Enter Status In Progress By default
                                  0,           --is_last_rec, -- IS_THIS_FINAL
                                  NULL,                -- WORKFLOW_EXECUTED_BY
                                  NULL,             --  WORKFLOW_EXECUTED_DATE
                                  NULL,                             -- REMARKS
                                  1, -- WorkFlow Completion Status By default 1
                                  login_user,        -- Current user Logged In
                                  SYSDATE,
                                  NULL,
                                  NULL,
                                  wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                                  v_already_approved,
                                  NVL (
                                     NVL (
                                        Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                           wf_level.WORKFLOW_CODE,
                                           P_KEY),
                                        p_message),
                                     p_key));

                     rec_counter := rec_counter + 1;
                  END IF;
               ELSE
                  IF NVL (wf_level.escalation_value, 0) <=
                        NVL (p_transaction_amount, 0)
                  THEN
                     IF wf_level.branch_code IS NULL
                     THEN
                        BEGIN
                           SELECT su.GROUP_ID
                             INTO v_branch_code
                             FROM ssm_users su
                            WHERE su.user_code = login_user;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              v_branch_code := NULL;
                        END;

                        IF v_branch_code = 'HQ'
                        THEN
                           v_role_code := wf_level.ROLE_CODE;
                        ELSE
                           v_role_code := 'BRMGR';
                        END IF;
                     ELSE
                        v_branch_code := wf_level.branch_code;
                        v_role_code := wf_level.ROLE_CODE;
                     END IF;

                     v_level_code_is_final := v_level_code_is_final + 1;

                     INSERT
                       INTO WFM_WORKFLOW_MONITORS (
                               WORKFLOW_CODE,
                               ROLE_CODE,
                               LEVEL_CODE,
                               TRANSACTION_CODE,
                               STATUS,
                               IS_THIS_FINAL,
                               WORKFLOW_EXECUTED_BY,
                               WORKFLOW_EXECUTED_DATE,
                               REMARKS,
                               WORKFLOW_COMPLETION_STATUS,
                               CREATED_BY,
                               CREATED_DATE,
                               MODIFIED_BY,
                               MODIFIED_DATE,
                               IS_MANDATORY,
                               APPROVED_ALREADY,
                               WORKFLOW_MESSAGE)
                        VALUES (
                                  wf_level.WORKFLOW_CODE,      --WORKFLOW_CODE
                                  v_role_code,
                                  v_level_code_is_final, --wf_level.LEVEL_CODE,
                                  p_key, -- Primary Key for The Current Record
                                  'WFST03', -- Enter Status In Progress By default
                                  0,           --is_last_rec, -- IS_THIS_FINAL
                                  NULL,                -- WORKFLOW_EXECUTED_BY
                                  NULL,             --  WORKFLOW_EXECUTED_DATE
                                  NULL,                             -- REMARKS
                                  1, -- WorkFlow Completion Status By default 1
                                  login_user,        -- Current user Logged In
                                  SYSDATE,
                                  NULL,
                                  NULL,
                                  wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                                  v_already_approved,
                                  NVL (
                                     NVL (
                                        Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                           wf_level.WORKFLOW_CODE,
                                           P_KEY),
                                        p_message),
                                     p_key));

                     rec_counter := rec_counter + 1;
                  END IF;
               END IF;
            ELSIF v_cnd_datatype = 'D'
            THEN
               IF TO_DATE (wf_level.escalation_value, 'DD-MM-YYYY') <=
                     TO_DATE (p_transaction_amount, 'DD-MM-YYYY')
               THEN
                  IF wf_level.branch_code IS NULL
                  THEN
                     BEGIN
                        SELECT su.GROUP_ID
                          INTO v_branch_code
                          FROM ssm_users su
                         WHERE su.user_code = login_user;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_branch_code := NULL;
                     END;

                     IF v_branch_code = 'HQ'
                     THEN
                        v_role_code := wf_level.ROLE_CODE;
                     ELSE
                        v_role_code := 'BRMGR';
                     END IF;
                  ELSE
                     v_branch_code := wf_level.branch_code;
                     v_role_code := wf_level.ROLE_CODE;
                  END IF;

                  v_level_code_is_final := v_level_code_is_final + 1;

                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,         --WORKFLOW_CODE
                               v_role_code,
                               v_level_code_is_final,   --wf_level.LEVEL_CODE,
                               p_key,    -- Primary Key for The Current Record
                               'WFST03', -- Enter Status In Progress By default
                               0,              --is_last_rec, -- IS_THIS_FINAL
                               NULL,                   -- WORKFLOW_EXECUTED_BY
                               NULL,                --  WORKFLOW_EXECUTED_DATE
                               NULL,                                -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,
                               NULL,
                               NULL,
                               wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));

                  rec_counter := rec_counter + 1;
               END IF;
            ELSIF v_cnd_datatype = 'C'
            THEN
               IF NVL (wf_level.escalation_value, 0) =
                     NVL (p_transaction_amount, 0)
               THEN
                  IF wf_level.branch_code IS NULL
                  THEN
                     BEGIN
                        SELECT su.GROUP_ID
                          INTO v_branch_code
                          FROM ssm_users su
                         WHERE su.user_code = login_user;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_branch_code := NULL;
                     END;

                     IF v_branch_code = 'HQ'
                     THEN
                        v_role_code := wf_level.ROLE_CODE;
                     ELSE
                        v_role_code := 'BRMGR';
                     END IF;
                  ELSE
                     v_branch_code := wf_level.branch_code;
                     v_role_code := wf_level.ROLE_CODE;
                  END IF;

                  v_level_code_is_final := v_level_code_is_final + 1;

                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,         --WORKFLOW_CODE
                               v_role_code,
                               v_level_code_is_final,   --wf_level.LEVEL_CODE,
                               p_key,    -- Primary Key for The Current Record
                               'WFST03', -- Enter Status In Progress By default
                               0,              --is_last_rec, -- IS_THIS_FINAL
                               NULL,                   -- WORKFLOW_EXECUTED_BY
                               NULL,                --  WORKFLOW_EXECUTED_DATE
                               NULL,                                -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,
                               NULL,
                               NULL,
                               wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));

                  rec_counter := rec_counter + 1;
               END IF;
            END IF;
         END LOOP;

         wf_status := 0;
      END IF;

      UPDATE WFM_WORKFLOW_MONITORS
         SET IS_THIS_FINAL = 1
       WHERE     WORKFLOW_CODE = form_trn_name
             AND TRANSACTION_CODE = p_key
             AND LEVEL_CODE = v_level_code_is_final;

      DELETE FROM wfm_workflow_monitors wwm
            WHERE     wwm.transaction_code = p_key
                  AND EXISTS
                         (SELECT 'X'
                            FROM wfm_workflow_levels wwl
                           WHERE     wwl.workflow_code = wwm.workflow_code
                                 AND wwl.role_code = wwm.role_code
                                 AND NVL (wwl.wf_reuqired, 'Y') = 'N');

      /*
      DELETE FROM wfm_workflow_monitors w WHERE w.is_this_final = '0' AND (w.workflow_code,w.role_code,w.transaction_code) IN
      (
      SELECT wwm.workflow_code,wwm.role_code,wwm.transaction_code
      FROM wfm_workflow_monitors wwm
      WHERE wwm.transaction_code = p_key
      AND   wwm.status           = 'WFST03'
      AND   wwm.role_code        = 'BRMGR'
      AND ROWID NOT IN
      (SELECT MAX(ROWID) FROM wfm_workflow_monitors wm
      WHERE wm.workflow_code = wwm.workflow_code
      AND   wm.role_code     = wwm.role_code
      AND   wm.transaction_code = wwm.transaction_code
      )
      );
       */
      -- COMMIT;
      RETURN wf_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20200, SQLERRM);
         RETURN NULL;
   END;                                              -- ***get_workflow_status

   FUNCTION get_workflow_m_status (form_trn_name           VARCHAR2,
                                   login_user              VARCHAR2,
                                   p_key                   VARCHAR2,
                                   p_message               VARCHAR2,
                                   p_transaction_amount    VARCHAR2)
      RETURN NUMBER
   IS
      wf_status               NUMBER := 1;
      is_last_rec             NUMBER := 0;
      rec_counter             NUMBER := 1;
      no_of_level             NUMBER := 0;
      v_already_approved      NUMBER := 0;
      v_remarks               VARCHAR2 (50);
      v_approval_on_modify    CHAR (1);
      v_approval_on_delete    CHAR (1);
      V_ROLE_CODE             WFM_WORKFLOW_LEVELS.ROLE_CODE%TYPE;
      v_branch_code           VARCHAR (3);
      v_cnd_datatype          CHAR;
      v_group_id              VARCHAR (3);

      CURSOR c_wf_level (p_form_trn_name VARCHAR2, p_deptcode VARCHAR2)
      IS
           SELECT *
             FROM WFM_WORKFLOW_LEVELS
            WHERE WORKFLOW_CODE = p_form_trn_name AND IS_MANDATORY = 1
         ORDER BY LEVEL_CODE;

      v_deptcode              VARCHAR2 (50);
      v_level_code_is_final   NUMBER (2);
   BEGIN
      v_level_code_is_final := 0;

      -- Check if workflow is defined on a Table or Not
      IF is_workflow_defined (form_trn_name)
      THEN
         -- To Find No of Levels i
         SELECT NVL (MAX (APPROVED_ALREADY), -1)
           INTO v_already_approved
           FROM WFM_WORKFLOW_MONITORS
          WHERE WORKFLOW_CODE = form_trn_name AND TRANSACTION_CODE = p_key;

         IF v_approval_on_modify = 'N' AND v_already_approved > -1
         THEN
            RETURN wf_status;
         END IF;

         --dbms_output.put_line('deleteflag:'||p_deleteflag);
         DBMS_OUTPUT.put_line (
            'v_approval_on_delete:' || v_approval_on_delete);

         IF v_already_approved > -1
         THEN
            v_remarks := 'Record Modifier';
         ELSE
            v_remarks := 'Record Creator';
         END IF;

         v_already_approved := v_already_approved + 1;

         SELECT COUNT (*)
           INTO no_of_level
           FROM WFM_WORKFLOW_LEVELS
          WHERE WORKFLOW_CODE = form_trn_name AND IS_MANDATORY = 1;

         DBMS_OUTPUT.put_line (
            'loop starts:' || form_trn_name || ', deptcode:' || v_deptcode);

         --
         -- Inserts a record into Monitor table
         --
         FOR wf_level IN c_wf_level (form_trn_name, v_deptcode)
         LOOP
            IF rec_counter = no_of_level
            THEN
               is_last_rec := 1;
            END IF;

            DBMS_OUTPUT.put_line ('rec_counter:' || rec_counter);

            --
            -- In Monitor Table
            -- Insert The Creator of The Record at '0' level
            -- Only add once this record i.e at level '0'
            --
            IF rec_counter = 1
            THEN
               BEGIN
                  SELECT ROLE_CODE
                    INTO V_ROLE_CODE
                    FROM SSM_USER_ROLE_INTERSECTS
                   WHERE     UPPER (USER_CODE) = UPPER (LOGIN_USER)
                         AND ENABLED_FLAG = '1'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     V_ROLE_CODE := 'ACCESSALL';
               END;

               -- CHANGES ENDS
               DBMS_OUTPUT.put_line (
                  'inserting into wfm_workflow_monitors ' || v_remarks);

               -- Added by Sakthivel to get branch_code on 09-dec-2011
               IF wf_level.branch_code IS NULL
               THEN
                  BEGIN
                     SELECT su.GROUP_ID
                       INTO v_branch_code
                       FROM ssm_users su
                      WHERE su.user_code = login_user;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_branch_code := NULL;
                  END;
               ELSE
                  v_branch_code := wf_level.branch_code;
               END IF;

               IF NVL (wf_level.escalation_value, 0) <=
                     NVL (p_transaction_amount, 0)
               THEN
                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,       --- WORKFLOW_CODE
                               V_ROLE_CODE, --' REMOVED BY HARI ACCESSALL',  --- Default Role Code For all User which is
                               0,                      --- Always at Level '0'
                               p_key,    -- Primary Key for The Current Record
                               'WFST01', -- Enter Status Approved By default the Creator always approves his Record
                               0,      -- 0 indicates This is not Final record
                               login_user,             -- WORKFLOW_EXECUTED_BY
                               SYSDATE,              -- WORKFLOW_EXECUTED_DATE
                               v_remarks,                           -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,                        -- Current Date
                               NULL,
                               NULL,
                               1,               --  By Default it is Mandatory
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));
               END IF;
            END IF;

            --
            -- Insert all Other Level into Monitor Table
            --
            DBMS_OUTPUT.put_line (
               'inserting into wfm_workflow_monitors ' || wf_level.ROLE_CODE);
            v_cnd_datatype := finding_datatypes (wf_level.escalation_value);

            IF v_cnd_datatype = 'N'
            THEN
               IF wf_level.escalation_value IS NOT NULL
               THEN
                  IF NVL (TO_NUMBER (wf_level.escalation_value), 0) <=
                        NVL (TO_NUMBER (p_transaction_amount), 0)
                  THEN
                     IF wf_level.branch_code IS NULL
                     THEN
                        BEGIN
                           SELECT su.GROUP_ID
                             INTO v_branch_code
                             FROM ssm_users su
                            WHERE su.user_code = login_user;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              v_branch_code := NULL;
                        END;

                        IF v_branch_code = 'HQ'
                        THEN
                           v_role_code := wf_level.ROLE_CODE;
                        ELSE
                           v_role_code := 'BRMGR';
                        END IF;
                     ELSE
                        v_branch_code := wf_level.branch_code;
                        v_role_code := wf_level.ROLE_CODE;
                     END IF;

                     v_level_code_is_final := v_level_code_is_final + 1;

                     INSERT
                       INTO WFM_WORKFLOW_MONITORS (
                               WORKFLOW_CODE,
                               ROLE_CODE,
                               LEVEL_CODE,
                               TRANSACTION_CODE,
                               STATUS,
                               IS_THIS_FINAL,
                               WORKFLOW_EXECUTED_BY,
                               WORKFLOW_EXECUTED_DATE,
                               REMARKS,
                               WORKFLOW_COMPLETION_STATUS,
                               CREATED_BY,
                               CREATED_DATE,
                               MODIFIED_BY,
                               MODIFIED_DATE,
                               IS_MANDATORY,
                               APPROVED_ALREADY,
                               WORKFLOW_MESSAGE)
                        VALUES (
                                  wf_level.WORKFLOW_CODE,      --WORKFLOW_CODE
                                  v_role_code,
                                  v_level_code_is_final, --wf_level.LEVEL_CODE,
                                  p_key, -- Primary Key for The Current Record
                                  'WFST03', -- Enter Status In Progress By default
                                  0,           --is_last_rec, -- IS_THIS_FINAL
                                  NULL,                -- WORKFLOW_EXECUTED_BY
                                  NULL,             --  WORKFLOW_EXECUTED_DATE
                                  NULL,                             -- REMARKS
                                  1, -- WorkFlow Completion Status By default 1
                                  login_user,        -- Current user Logged In
                                  SYSDATE,
                                  NULL,
                                  NULL,
                                  wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                                  v_already_approved,
                                  NVL (
                                     NVL (
                                        Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                           wf_level.WORKFLOW_CODE,
                                           P_KEY),
                                        p_message),
                                     p_key));

                     rec_counter := rec_counter + 1;
                  END IF;
               ELSE
                  IF NVL (wf_level.escalation_value, 0) <=
                        NVL (p_transaction_amount, 0)
                  THEN
                     IF wf_level.branch_code IS NULL
                     THEN
                        BEGIN
                           SELECT su.GROUP_ID
                             INTO v_branch_code
                             FROM ssm_users su
                            WHERE su.user_code = login_user;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              v_branch_code := NULL;
                        END;

                        IF v_branch_code = 'HQ'
                        THEN
                           v_role_code := wf_level.ROLE_CODE;
                        ELSE
                           v_role_code := 'BRMGR';
                        END IF;
                     ELSE
                        v_branch_code := wf_level.branch_code;
                        v_role_code := wf_level.ROLE_CODE;
                     END IF;

                     v_level_code_is_final := v_level_code_is_final + 1;

                     INSERT
                       INTO WFM_WORKFLOW_MONITORS (
                               WORKFLOW_CODE,
                               ROLE_CODE,
                               LEVEL_CODE,
                               TRANSACTION_CODE,
                               STATUS,
                               IS_THIS_FINAL,
                               WORKFLOW_EXECUTED_BY,
                               WORKFLOW_EXECUTED_DATE,
                               REMARKS,
                               WORKFLOW_COMPLETION_STATUS,
                               CREATED_BY,
                               CREATED_DATE,
                               MODIFIED_BY,
                               MODIFIED_DATE,
                               IS_MANDATORY,
                               APPROVED_ALREADY,
                               WORKFLOW_MESSAGE)
                        VALUES (
                                  wf_level.WORKFLOW_CODE,      --WORKFLOW_CODE
                                  v_role_code,
                                  v_level_code_is_final, --wf_level.LEVEL_CODE,
                                  p_key, -- Primary Key for The Current Record
                                  'WFST03', -- Enter Status In Progress By default
                                  0,           --is_last_rec, -- IS_THIS_FINAL
                                  NULL,                -- WORKFLOW_EXECUTED_BY
                                  NULL,             --  WORKFLOW_EXECUTED_DATE
                                  NULL,                             -- REMARKS
                                  1, -- WorkFlow Completion Status By default 1
                                  login_user,        -- Current user Logged In
                                  SYSDATE,
                                  NULL,
                                  NULL,
                                  wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                                  v_already_approved,
                                  NVL (
                                     NVL (
                                        Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                           wf_level.WORKFLOW_CODE,
                                           P_KEY),
                                        p_message),
                                     p_key));

                     rec_counter := rec_counter + 1;
                  END IF;
               END IF;
            ELSIF v_cnd_datatype = 'D'
            THEN
               IF TO_DATE (wf_level.escalation_value, 'DD-MM-YYYY') <=
                     TO_DATE (p_transaction_amount, 'DD-MM-YYYY')
               THEN
                  IF wf_level.branch_code IS NULL
                  THEN
                     BEGIN
                        SELECT su.GROUP_ID
                          INTO v_branch_code
                          FROM ssm_users su
                         WHERE su.user_code = login_user;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_branch_code := NULL;
                     END;

                     IF v_branch_code = 'HQ'
                     THEN
                        v_role_code := wf_level.ROLE_CODE;
                     ELSE
                        v_role_code := 'BRMGR';
                     END IF;
                  ELSE
                     v_branch_code := wf_level.branch_code;
                     v_role_code := wf_level.ROLE_CODE;
                  END IF;

                  v_level_code_is_final := v_level_code_is_final + 1;

                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,         --WORKFLOW_CODE
                               v_role_code,
                               v_level_code_is_final,   --wf_level.LEVEL_CODE,
                               p_key,    -- Primary Key for The Current Record
                               'WFST03', -- Enter Status In Progress By default
                               0,              --is_last_rec, -- IS_THIS_FINAL
                               NULL,                   -- WORKFLOW_EXECUTED_BY
                               NULL,                --  WORKFLOW_EXECUTED_DATE
                               NULL,                                -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,
                               NULL,
                               NULL,
                               wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));

                  rec_counter := rec_counter + 1;
               END IF;
            ELSIF v_cnd_datatype = 'C'
            THEN
               IF NVL (wf_level.escalation_value, 0) =
                     NVL (p_transaction_amount, 0)
               THEN
                  IF wf_level.branch_code IS NULL
                  THEN
                     BEGIN
                        SELECT su.GROUP_ID
                          INTO v_branch_code
                          FROM ssm_users su
                         WHERE su.user_code = login_user;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_branch_code := NULL;
                     END;

                     IF v_branch_code = 'HQ'
                     THEN
                        v_role_code := wf_level.ROLE_CODE;
                     ELSE
                        v_role_code := 'BRMGR';
                     END IF;
                  ELSE
                     v_branch_code := wf_level.branch_code;
                     v_role_code := wf_level.ROLE_CODE;
                  END IF;

                  v_level_code_is_final := v_level_code_is_final + 1;

                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,         --WORKFLOW_CODE
                               v_role_code,
                               v_level_code_is_final,   --wf_level.LEVEL_CODE,
                               p_key,    -- Primary Key for The Current Record
                               'WFST03', -- Enter Status In Progress By default
                               0,              --is_last_rec, -- IS_THIS_FINAL
                               NULL,                   -- WORKFLOW_EXECUTED_BY
                               NULL,                --  WORKFLOW_EXECUTED_DATE
                               NULL,                                -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,
                               NULL,
                               NULL,
                               wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));

                  rec_counter := rec_counter + 1;
               END IF;
            END IF;
         END LOOP;

         wf_status := 0;
      END IF;

      UPDATE WFM_WORKFLOW_MONITORS
         SET IS_THIS_FINAL = 1
       WHERE     WORKFLOW_CODE = form_trn_name
             AND TRANSACTION_CODE = p_key
             AND LEVEL_CODE = v_level_code_is_final;

      BEGIN
         SELECT su.GROUP_ID
           INTO v_group_id
           FROM ssm_users su
          WHERE su.user_code = login_user;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_group_id := 'HQ';
      END;

      IF v_group_id <> 'HQ'
      THEN
         DELETE FROM wfm_workflow_monitors wwm
               WHERE     wwm.transaction_code = p_key
                     AND EXISTS
                            (SELECT 'X'
                               FROM wfm_workflow_levels wwl
                              WHERE     wwl.workflow_code = wwm.workflow_code
                                    AND wwl.role_code = wwm.role_code
                                    AND NVL (wwl.wf_reuqired, 'Y') = 'N');

         DELETE FROM wfm_workflow_monitors wwm
               WHERE     wwm.transaction_code = p_key
                     AND wwm.status = 'WFST03'
                     AND wwm.role_code <> 'BRMGR'
                     AND EXISTS
                            (SELECT 'X'
                               FROM wfm_workflow_monitors w
                              WHERE     w.transaction_code =
                                           wwm.transaction_code
                                    AND w.role_code = 'BRMGR');

         UPDATE wfm_workflow_monitors wwm
            SET wwm.is_this_final = '1'
          WHERE     wwm.transaction_code = p_key
                AND wwm.status = 'WFST03'
                AND wwm.role_code = 'BRMGR';
      END IF;

      -- COMMIT;
      RETURN wf_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20200, SQLERRM);
         RETURN NULL;
   END;                                              -- ***get_workflow_status

   FUNCTION get_workflow_status_amt (form_trn_name           VARCHAR2,
                                     login_user              VARCHAR2,
                                     p_key                   VARCHAR2,
                                     p_message               VARCHAR2,
                                     p_transaction_amount    NUMBER)
      RETURN NUMBER
   IS
      wf_status               NUMBER := 1;
      is_last_rec             NUMBER := 0;
      rec_counter             NUMBER := 1;
      no_of_level             NUMBER := 0;
      v_already_approved      NUMBER := 0;
      v_remarks               VARCHAR2 (50);
      v_approval_on_modify    CHAR (1);
      v_approval_on_delete    CHAR (1);
      V_ROLE_CODE             WFM_WORKFLOW_LEVELS.ROLE_CODE%TYPE;
      v_branch_code           VARCHAR2 (2);

      CURSOR c_wf_level (p_form_trn_name VARCHAR2, p_deptcode VARCHAR2)
      IS
           SELECT *
             FROM WFM_WORKFLOW_LEVELS
            WHERE WORKFLOW_CODE = p_form_trn_name AND IS_MANDATORY = 1
         ORDER BY LEVEL_CODE;

      v_deptcode              VARCHAR2 (50);
      v_level_code_is_final   NUMBER (2);
   BEGIN
      v_level_code_is_final := 0;

      -- Check if workflow is defined on a Table or Not
      IF is_workflow_defined (form_trn_name)
      THEN
         -- To Find No of Levels i
         SELECT NVL (MAX (APPROVED_ALREADY), -1)
           INTO v_already_approved
           FROM WFM_WORKFLOW_MONITORS
          WHERE WORKFLOW_CODE = form_trn_name AND TRANSACTION_CODE = p_key;

         IF v_approval_on_modify = 'N' AND v_already_approved > -1
         THEN
            RETURN wf_status;
         END IF;

         --dbms_output.put_line('deleteflag:'||p_deleteflag);
         DBMS_OUTPUT.put_line (
            'v_approval_on_delete:' || v_approval_on_delete);

         IF v_already_approved > -1
         THEN
            v_remarks := 'Record Modifier';
         ELSE
            v_remarks := 'Record Creator';
         END IF;

         v_already_approved := v_already_approved + 1;

         SELECT COUNT (*)
           INTO no_of_level
           FROM WFM_WORKFLOW_LEVELS
          WHERE WORKFLOW_CODE = form_trn_name AND IS_MANDATORY = 1;

         DBMS_OUTPUT.put_line (
            'loop starts:' || form_trn_name || ', deptcode:' || v_deptcode);

         --
         -- Inserts a record into Monitor table
         --
         FOR wf_level IN c_wf_level (form_trn_name, v_deptcode)
         LOOP
            IF rec_counter = no_of_level
            THEN
               is_last_rec := 1;
            END IF;

            DBMS_OUTPUT.put_line ('rec_counter:' || rec_counter);

            --
            -- In Monitor Table
            -- Insert The Creator of The Record at '0' level
            -- Only add once this record i.e at level '0'
            --
            IF rec_counter = 1
            THEN
               BEGIN
                  SELECT ROLE_CODE
                    INTO V_ROLE_CODE
                    FROM SSM_USER_ROLE_INTERSECTS
                   WHERE     UPPER (USER_CODE) = UPPER (LOGIN_USER)
                         AND ENABLED_FLAG = '1'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     V_ROLE_CODE := 'ACCESSALL';
               END;

               -- CHANGES ENDS
               DBMS_OUTPUT.put_line (
                  'inserting into wfm_workflow_monitors ' || v_remarks);

               -- Added by Sakthivel to get branch_code on 09-dec-2011
               IF wf_level.branch_code IS NULL
               THEN
                  BEGIN
                     SELECT su.GROUP_ID
                       INTO v_branch_code
                       FROM ssm_users su
                      WHERE su.user_code = login_user;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_branch_code := NULL;
                  END;
               ELSE
                  v_branch_code := wf_level.branch_code;
               END IF;

               IF NVL (wf_level.escalation_value, 0) <=
                     NVL (p_transaction_amount, 0)
               THEN
                  INSERT
                    INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                ROLE_CODE,
                                                LEVEL_CODE,
                                                TRANSACTION_CODE,
                                                STATUS,
                                                IS_THIS_FINAL,
                                                WORKFLOW_EXECUTED_BY,
                                                WORKFLOW_EXECUTED_DATE,
                                                REMARKS,
                                                WORKFLOW_COMPLETION_STATUS,
                                                CREATED_BY,
                                                CREATED_DATE,
                                                MODIFIED_BY,
                                                MODIFIED_DATE,
                                                IS_MANDATORY,
                                                APPROVED_ALREADY,
                                                WORKFLOW_MESSAGE)
                     VALUES (
                               wf_level.WORKFLOW_CODE,       --- WORKFLOW_CODE
                               V_ROLE_CODE, --' REMOVED BY HARI ACCESSALL',  --- Default Role Code For all User which is
                               0,                      --- Always at Level '0'
                               p_key,    -- Primary Key for The Current Record
                               'WFST01', -- Enter Status Approved By default the Creator always approves his Record
                               0,      -- 0 indicates This is not Final record
                               login_user,             -- WORKFLOW_EXECUTED_BY
                               SYSDATE,              -- WORKFLOW_EXECUTED_DATE
                               v_remarks,                           -- REMARKS
                               1,   -- WorkFlow Completion Status By default 1
                               login_user,           -- Current user Logged In
                               SYSDATE,                        -- Current Date
                               NULL,
                               NULL,
                               1,               --  By Default it is Mandatory
                               v_already_approved,
                               NVL (
                                  NVL (
                                     Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                        wf_level.WORKFLOW_CODE,
                                        P_KEY),
                                     p_message),
                                  p_key));
               END IF;
            END IF;

            --
            -- Insert all Other Level into Monitor Table
            --
            DBMS_OUTPUT.put_line (
               'inserting into wfm_workflow_monitors ' || wf_level.ROLE_CODE);

            IF NVL (TO_NUMBER (wf_level.escalation_value), 0) <=
                  NVL (p_transaction_amount, 0)
            THEN
               IF wf_level.branch_code IS NULL
               THEN
                  BEGIN
                     SELECT su.GROUP_ID
                       INTO v_branch_code
                       FROM ssm_users su
                      WHERE su.user_code = login_user;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_branch_code := NULL;
                  END;

                  IF v_branch_code = 'HQ'
                  THEN
                     v_role_code := wf_level.ROLE_CODE;
                  ELSE
                     v_role_code := 'BRMGR';
                  END IF;
               ELSE
                  v_branch_code := wf_level.branch_code;
                  v_role_code := wf_level.ROLE_CODE;
               END IF;

               v_level_code_is_final := v_level_code_is_final + 1;

               INSERT INTO WFM_WORKFLOW_MONITORS (WORKFLOW_CODE,
                                                  ROLE_CODE,
                                                  LEVEL_CODE,
                                                  TRANSACTION_CODE,
                                                  STATUS,
                                                  IS_THIS_FINAL,
                                                  WORKFLOW_EXECUTED_BY,
                                                  WORKFLOW_EXECUTED_DATE,
                                                  REMARKS,
                                                  WORKFLOW_COMPLETION_STATUS,
                                                  CREATED_BY,
                                                  CREATED_DATE,
                                                  MODIFIED_BY,
                                                  MODIFIED_DATE,
                                                  IS_MANDATORY,
                                                  APPROVED_ALREADY,
                                                  WORKFLOW_MESSAGE)
                       VALUES (
                                 wf_level.WORKFLOW_CODE,       --WORKFLOW_CODE
                                 v_role_code,
                                 v_level_code_is_final, --wf_level.LEVEL_CODE,
                                 p_key,  -- Primary Key for The Current Record
                                 'WFST03', -- Enter Status In Progress By default
                                 0,            --is_last_rec, -- IS_THIS_FINAL
                                 NULL,                 -- WORKFLOW_EXECUTED_BY
                                 NULL,              --  WORKFLOW_EXECUTED_DATE
                                 NULL,                              -- REMARKS
                                 1, -- WorkFlow Completion Status By default 1
                                 login_user,         -- Current user Logged In
                                 SYSDATE,
                                 NULL,
                                 NULL,
                                 wf_level.IS_MANDATORY, -- Is this Check is mandaory or not
                                 v_already_approved,
                                 NVL (
                                    NVL (
                                       Workflow_Handler.GET_WORKFLOW_MESSAGE (
                                          wf_level.WORKFLOW_CODE,
                                          P_KEY),
                                       p_message),
                                    p_key));

               rec_counter := rec_counter + 1;
            END IF;
         END LOOP;

         wf_status := 0;
      END IF;

      UPDATE WFM_WORKFLOW_MONITORS
         SET IS_THIS_FINAL = 1
       WHERE     WORKFLOW_CODE = form_trn_name
             AND TRANSACTION_CODE = p_key
             AND LEVEL_CODE = v_level_code_is_final;

      -- COMMIT;
      RETURN wf_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         RAISE_APPLICATION_ERROR (-20200, SQLERRM);
         RETURN NULL;
   END;                                              -- ***get_workflow_status

   /*--------------------------------------------------------------------*/
   --
   -- Local Procedures
   -- Return True if workflow is defiend for That Table
   -- Else False
   --
   FUNCTION is_workflow_defined (form_trn_name VARCHAR2)
      RETURN BOOLEAN
   IS
      is_wf   BOOLEAN := FALSE;
      tmp     VARCHAR2 (1);
   BEGIN
      SELECT 'x'
        INTO tmp
        FROM WFM_WORKFLOW_CODE_MASTERS
       WHERE     WORKFLOW_CODE = form_trn_name
             AND ENABLED_FLAG = 1
             AND WORKFLOW_COMPLETION_STATUS = 1;

      is_wf := TRUE;
      RETURN is_wf;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN is_wf;
   END;                                                 -- is_workflow_defined

   /*--------------------------------------------------------------------*/
   FUNCTION is_approver (form_trn_name    VARCHAR2,
                         login_user       VARCHAR2,
                         p_key            VARCHAR2,
                         p_level_code     NUMBER)
      RETURN BOOLEAN
   IS
      CURSOR c_check_role
      IS
           SELECT *
             FROM WFM_WORKFLOW_MONITORS
            WHERE     WORKFLOW_CODE = form_trn_name
                  AND TRANSACTION_CODE = p_key
                  AND STATUS NOT IN ('WFST01', 'WFST02')
                  AND APPROVED_ALREADY =
                         (SELECT MAX (APPROVED_ALREADY)
                            FROM WFM_WORKFLOW_MONITORS
                           WHERE     WORKFLOW_CODE = form_trn_name
                                 AND TRANSACTION_CODE = p_key)
         ORDER BY LEVEL_CODE;

      is_app                BOOLEAN := FALSE;
      v_role_code           VARCHAR2 (30);
      tmp                   VARCHAR2 (1);
      prev_mandatory_flag   NUMBER := 0;
   BEGIN
      FOR check_role IN c_check_role
      LOOP
         IF p_level_code < check_role.LEVEL_CODE
         THEN
            EXIT;
         END IF;

         --
         -- Check if the record is in Status needs clarification
         -- If Yes and record is at 0 Level Then the Creator of The record is approver
         -- Else It goes as It is
         --
         IF     check_role.IS_MANDATORY = 1
            AND check_role.STATUS IN ('WFST04')
            AND check_role.LEVEL_CODE = 0
         THEN
            IF check_role.CREATED_BY = login_user
            THEN
               is_app := TRUE;
               EXIT;
            END IF;
         END IF;

         --
         -- Check if Login User Has The  Priviledges
         --
         BEGIN
            SELECT 'x'
              INTO tmp
              FROM SSM_USER_ROLE_INTERSECTS
             WHERE     ROLE_CODE = check_role.ROLE_CODE
                   AND USER_CODE = login_user
                   AND ENABLED_FLAG = 1
                   AND WORKFLOW_COMPLETION_STATUS = 1;

            IF prev_mandatory_flag = 0
            THEN
               is_app := TRUE;
            END IF;

            EXIT;                             -- user Role Found Exit Checking

            --
            -- Check if current record is mandaotory or not
            --
            IF check_role.IS_MANDATORY = 1
            THEN
               prev_mandatory_flag := 1;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               IF check_role.IS_MANDATORY = 1
               THEN
                  prev_mandatory_flag := 1;
               END IF;
         END;
      END LOOP;

      RETURN is_app;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN is_app;
   END;                                                          --is_approver

   -----------------------------------------------------------------------------------------------------------------
   --------------- THIS PROCEDURE IS USED TO GET THE WORKFLOW MESSAGE FOR MENU SCREEN ------------------------------
   -----------------------------------------------------------------------------------------------------------------
   FUNCTION GET_WORKFLOW_MESSAGE (P_WORKFLOW_CODE      IN VARCHAR2,
                                  P_TRANSACTION_CODE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      V_WORKFLOW_MESSAGE_QUERY   WFM_WORKFLOW_CODE_MASTERS.WORKFLOW_MESSAGE_QUERY%TYPE;
      P_WF_MESSAGE               VARCHAR2 (500);
   BEGIN
      SELECT WORKFLOW_MESSAGE_QUERY
        INTO V_WORKFLOW_MESSAGE_QUERY
        FROM WFM_WORKFLOW_CODE_MASTERS
       WHERE WORKFLOW_CODE = P_WORKFLOW_CODE;

      IF V_WORKFLOW_MESSAGE_QUERY IS NOT NULL
      THEN
         EXECUTE IMMEDIATE V_WORKFLOW_MESSAGE_QUERY
            INTO P_WF_MESSAGE
            USING P_TRANSACTION_CODE;
      ELSE
         SELECT description
           INTO p_wf_message
           FROM wfm_workflow_code_masters
          WHERE workflow_code = p_workflow_code;
      END IF;

      RETURN P_WF_MESSAGE;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 'NULL';
   END;

   -----------------------------------------------------------------------------------------------------------------
   -----------------------------------------------------------------------------------------------------------------

   PROCEDURE GET_WORKFLOW_MESSAGE (P_WORKFLOW_CODE      IN     VARCHAR2,
                                   P_TRANSACTION_CODE   IN     VARCHAR2,
                                   P_WF_MESSAGE            OUT VARCHAR2)
   IS
      V_WORKFLOW_MESSAGE_QUERY   WFM_WORKFLOW_CODE_MASTERS.WORKFLOW_MESSAGE_QUERY%TYPE;
   BEGIN
      SELECT WORKFLOW_MESSAGE_QUERY
        INTO V_WORKFLOW_MESSAGE_QUERY
        FROM WFM_WORKFLOW_CODE_MASTERS
       WHERE WORKFLOW_CODE = P_WORKFLOW_CODE;

      IF V_WORKFLOW_MESSAGE_QUERY IS NOT NULL
      THEN
         EXECUTE IMMEDIATE V_WORKFLOW_MESSAGE_QUERY
            INTO P_WF_MESSAGE
            USING P_TRANSACTION_CODE;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   -----------------------------------------------------------------------------------------------------------------
   -----------------------------------------------------------------------------------------------------------------
   PROCEDURE update_wf_status (P_SCREEN_CODE        IN     VARCHAR2,
                               P_STATUS             IN     VARCHAR2,
                               P_USERID             IN     VARCHAR2,
                               P_TRANSACTION_CODE   IN     VARCHAR2,
                               P_DESCRIPTION        IN     VARCHAR2,
                               P_LEVEL_CODE         IN     VARCHAR2,
                               P_RETURN_STATUS         OUT NUMBER)
   IS
      CURSOR c_select_wf
      IS
           SELECT *
             FROM WFM_WORKFLOW_MONITORS
            WHERE     WORKFLOW_CODE = P_SCREEN_CODE
                  AND TRANSACTION_CODE = P_TRANSACTION_CODE
                  AND LEVEL_CODE = p_level_code
                  AND STATUS NOT IN ('WFST01', 'WFST02')
                  AND APPROVED_ALREADY =
                         (SELECT MAX (APPROVED_ALREADY)
                            FROM WFM_WORKFLOW_MONITORS
                           WHERE     WORKFLOW_CODE = P_SCREEN_CODE
                                 AND TRANSACTION_CODE = P_TRANSACTION_CODE)
         ORDER BY LEVEL_CODE;

      v_status               VARCHAR2 (100) := '';
      up_sql_str             VARCHAR2 (500) := '';
      ret_val                NUMBER (10) := 0;
      ret_val1               NUMBER (10) := 0;
      temp_level             VARCHAR2 (50);
      v_errm                 VARCHAR2 (2000);
      v_table_name           VARCHAR2 (100);
      v_transaction_column   VARCHAR2 (100);
      v_update_table         VARCHAR2 (500);
   BEGIN
      /*     IF name_in('RG_APPROVE') = 'NC' THEN
        v_status := 'WFST04';
      ELSIF name_in('RG_APPROVE') = 'A' THEN
        v_status := 'WFST01';
      ELSIF name_in('RG_APPROVE') = 'R' THEN
        v_status := 'WFST02';
      END IF;*/
      v_status := p_status;

      FOR select_wf IN c_select_wf
      LOOP
         --
         -- Update the workflow status in workflow monitor Table
         -- If Approver Selects Needs Clarification Then
         -- It Indicates This Status to One level Above the current approver
         -- Else It proceeds as Normal
         --

         IF v_status IN ('WFST04') AND select_wf.LEVEL_CODE >= 1
         THEN
            BEGIN
               SELECT MAX (LEVEL_CODE)
                 INTO temp_level
                 FROM WFM_WORKFLOW_MONITORS
                WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                      AND TRANSACTION_CODE = select_wf.TRANSACTION_CODE
                      AND LEVEL_CODE < select_wf.LEVEL_CODE
                      AND WORKFLOW_COMPLETION_STATUS = 1
                      AND APPROVED_ALREADY =
                             (SELECT MAX (APPROVED_ALREADY)
                                FROM WFM_WORKFLOW_MONITORS
                               WHERE     WORKFLOW_CODE =
                                            select_wf.WORKFLOW_CODE
                                     AND TRANSACTION_CODE =
                                            select_wf.TRANSACTION_CODE)
                      AND IS_MANDATORY = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ret_val := 0;
            END;

            UPDATE WFM_WORKFLOW_MONITORS
               SET STATUS = v_status,
                   WORKFLOW_EXECUTED_BY = P_USERID,
                   WORKFLOW_EXECUTED_DATE = SYSDATE,
                   REMARKS = P_DESCRIPTION,
                   MODIFIED_BY = P_USERID,
                   MODIFIED_DATE = SYSDATE
             WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                   AND TRANSACTION_CODE = select_wf.TRANSACTION_CODE
                   AND APPROVED_ALREADY =
                          (SELECT MAX (APPROVED_ALREADY)
                             FROM WFM_WORKFLOW_MONITORS
                            WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                                  AND TRANSACTION_CODE =
                                         select_wf.TRANSACTION_CODE)
                   AND LEVEL_CODE = temp_level;
         -- To stop futher approvals, once it is rejected.
         ELSIF v_status IN ('WFST02')
         THEN
            BEGIN
               --COPY('WF Code : '||select_wf.WORKFLOW_CODE||' Tr Cd : '||select_wf.TRANSACTION_CODE||' Lv cd : '||select_wf.LEVEL_CODE,'TITL.MESSAGE');pause;
               --message('WF Code : '||select_wf.WORKFLOW_CODE||' Tr Cd : '||select_wf.TRANSACTION_CODE||' Lv cd : '||select_wf.LEVEL_CODE);message(' ');
               UPDATE WFM_WORKFLOW_MONITORS
                  SET STATUS = v_status,
                      WORKFLOW_EXECUTED_BY = P_USERID,
                      WORKFLOW_EXECUTED_DATE = SYSDATE,
                      REMARKS = P_description,
                      MODIFIED_BY = P_USERID,
                      MODIFIED_DATE = SYSDATE
                --APPROVED_ALREADY='1'
                WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                      AND TRANSACTION_CODE = select_wf.TRANSACTION_CODE
                      AND APPROVED_ALREADY =
                             (SELECT MAX (APPROVED_ALREADY)
                                FROM WFM_WORKFLOW_MONITORS
                               WHERE     WORKFLOW_CODE =
                                            select_wf.WORKFLOW_CODE
                                     AND TRANSACTION_CODE =
                                            select_wf.TRANSACTION_CODE)
                      AND Status NOT IN ('WFST01', 'WFST02');

               SELECT slh.table_name, slh.primary_column_name
                 INTO v_table_name, v_transaction_column
                 FROM ssm_screens slh
                WHERE     slh.screen_code = p_screen_code
                      AND slh.language_code = 'EN';

               v_update_table :=
                     'update '
                  || v_table_name
                  || ' set workflow_completion_status=''2''	where '
                  || v_transaction_column
                  || ' ='''
                  || p_transaction_code
                  || '''';

               EXECUTE IMMEDIATE V_Update_Table;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errm := SQLERRM;
                  ret_val := 0;
            END;
         --END OF ADDITION

         ELSE
            -------------------------------------------------------------------------------------------
            -- To Update all levels which the use has privillage           ---
            --
            -------------------------------------------------------------------------------------------

            SELECT MAX (IS_THIS_FINAL)
              INTO RET_VAL1
              FROM WFM_WORKFLOW_MONITORS
             WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                   AND TRANSACTION_CODE = select_wf.TRANSACTION_CODE
                   AND STATUS NOT IN ('WFST01', 'WFST02') --To avoid updating approved record
                   AND APPROVED_ALREADY =
                          (SELECT MAX (APPROVED_ALREADY)
                             FROM WFM_WORKFLOW_MONITORS
                            WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                                  AND TRANSACTION_CODE =
                                         select_wf.TRANSACTION_CODE)
                   AND ROLE_CODE IN (SELECT ROLE_CODE -- To select all the roles
                                       FROM SSM_USER_ROLE_INTERSECTS
                                      WHERE UPPER (USER_CODE) =
                                               UPPER (p_userid))
                   AND LEVEL_CODE = select_wf.LEVEL_CODE;

            UPDATE WFM_WORKFLOW_MONITORS
               SET STATUS = v_status,
                   WORKFLOW_EXECUTED_BY = P_USERID,
                   WORKFLOW_EXECUTED_DATE = SYSDATE,
                   REMARKS = P_description,
                   MODIFIED_BY = P_USERID,
                   MODIFIED_DATE = SYSDATE
             WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                   AND TRANSACTION_CODE = select_wf.TRANSACTION_CODE
                   AND STATUS NOT IN ('WFST01', 'WFST02') --To avoid updating approved record
                   AND APPROVED_ALREADY =
                          (SELECT MAX (APPROVED_ALREADY)
                             FROM WFM_WORKFLOW_MONITORS
                            WHERE     WORKFLOW_CODE = select_wf.WORKFLOW_CODE
                                  AND TRANSACTION_CODE =
                                         select_wf.TRANSACTION_CODE)
                   AND ROLE_CODE IN (SELECT ROLE_CODE -- To select all the roles
                                       FROM SSM_USER_ROLE_INTERSECTS
                                      WHERE     UPPER (USER_CODE) =
                                                   UPPER (P_USERID)
                                            AND enabled_flag = '1'
                                            AND workflow_completion_status =
                                                   '1')
                   AND LEVEL_CODE = select_wf.LEVEL_CODE;
         END IF;

         IF SQL%FOUND
         THEN
            ret_val := 1;
         END IF;

         IF (RET_VAL1 = 1) AND (ret_val = 1) AND v_status = 'WFST01'
         THEN
            /*
              Purpose:to update the Last Approval record to APPROVED_ALREADY=1.
            */

            SELECT slh.table_name, slh.primary_column_name
              INTO v_table_name, v_transaction_column
              FROM ssm_screens slh
             WHERE     slh.screen_code = p_screen_code
                   AND slh.language_code = 'EN';

            v_update_table :=
                  'update '
               || v_table_name
               || ' set workflow_completion_status=''1''	where '
               || v_transaction_column
               || ' ='''
               || p_transaction_code
               || '''';

            EXECUTE IMMEDIATE v_update_table;

            ret_val := 1;
         END IF;

         P_RETURN_STATUS := ret_val;
      END LOOP;

      COMMIT;
   END;

   PROCEDURE ABOUT_THIS_RECORD (P_SCREEN_CODE        IN     VARCHAR2,
                                P_Transaction_Code   IN     VARCHAR2,
                                P_ABT_INFO_1            OUT VARCHAR2,
                                P_ABT_INFO_2            OUT VARCHAR2,
                                P_ABT_INFO_3            OUT VARCHAR2,
                                P_ABT_INFO_4            OUT VARCHAR2,
                                P_ABT_INFO_5            OUT VARCHAR2,
                                P_Abt_Info_6            OUT VARCHAR2,
                                P_Abt_Info_7            OUT VARCHAR2)
   IS
      V_TABLE_NAME                   VARCHAR2 (100);
      V_Transaction_Column           VARCHAR2 (100);
      V_ABT_INFO_1                   VARCHAR2 (100);
      V_Abt_Info_2                   VARCHAR2 (100);
      V_Abt_Info_3                   VARCHAR2 (100);
      V_Abt_Info_4                   VARCHAR2 (100);
      V_Abt_Info_5                   VARCHAR2 (100);
      V_Abt_Info_6                   VARCHAR2 (100);
      V_Abt_Info_7                   VARCHAR2 (100);
      V_Workflow_Completion_Status   CHAR (1);
      V_Status                       VARCHAR2 (10);
      v_abt_msg_6                    VARCHAR2 (50);
      v_abt_msg_7                    VARCHAR2 (50);
      v_exe_qry                      VARCHAR2 (2000);
   BEGIN
      SELECT slh.table_name, slh.primary_column_name
        INTO v_table_name, v_transaction_column
        FROM ssm_screens slh
       WHERE slh.screen_code = p_screen_code AND Slh.Language_Code = 'EN';

      IF NVL (v_table_name, 'NVL') = 'NVL'
      THEN
         P_Abt_Info_1 := 'Created By      : N/A';
         P_Abt_Info_2 := 'Created Date    : N/A';
         P_Abt_Info_3 := 'Modified By     : N/A';
         P_Abt_Info_4 := 'Modified Date   : N/A';
         P_Abt_Info_5 := 'Workflow Status : N/A';
         P_Abt_Info_6 := 'N/A';
         P_Abt_Info_7 := 'N/A';
      ELSE
         v_exe_qry :=
               'select created_by,created_date,modified_by,modified_date,workflow_completion_status from '
            || V_Table_Name
            || ' where '
            || V_Transaction_Column
            || ' = '''
            || P_Transaction_Code
            || '''';

         EXECUTE IMMEDIATE v_exe_qry
            INTO v_abt_info_1,
                 v_abt_info_2,
                 v_abt_info_3,
                 v_abt_info_4,
                 v_workflow_completion_status;

         IF workflow_handler.is_workflow_defined (p_screen_code)
         THEN
            IF v_workflow_completion_status = '1'
            THEN
               SELECT wwm.workflow_executed_by, wwm.workflow_executed_date
                 INTO v_abt_info_6, v_abt_info_7
                 FROM Wfm_Workflow_Monitors Wwm
                WHERE     Wwm.Workflow_Code = P_Screen_Code
                      AND Wwm.Transaction_Code = P_Transaction_Code
                      AND Wwm.Workflow_Executed_Date =
                             (SELECT MAX (Wwms.Workflow_Executed_Date)
                                FROM Wfm_Workflow_Monitors Wwms
                               WHERE     Wwms.Workflow_Code = P_Screen_Code
                                     AND Wwms.Transaction_Code =
                                            P_Transaction_Code
                                     AND wwms.is_this_final = '1')
                      AND Wwm.Is_This_Final = '1';

               v_abt_info_5 := 'Approved';
               v_abt_msg_6 := 'Approved By    : ';
               v_abt_msg_7 := 'Approved Date  : ';
            ELSIF v_workflow_completion_status = '2'
            THEN
               SELECT wwm.workflow_executed_by, wwm.workflow_executed_date
                 INTO v_abt_info_6, v_abt_info_7
                 FROM Wfm_Workflow_Monitors Wwm
                WHERE     Wwm.Workflow_Code = P_Screen_Code
                      AND Wwm.Transaction_Code = P_Transaction_Code
                      AND Wwm.Workflow_Executed_Date =
                             (SELECT MAX (Wwms.Workflow_Executed_Date)
                                FROM Wfm_Workflow_Monitors Wwms
                               WHERE     Wwms.Workflow_Code = P_Screen_Code
                                     AND Wwms.Transaction_Code =
                                            P_Transaction_Code
                                     AND wwms.status = 'WFST02')
                      AND Wwm.Status = 'WFST02';

               v_abt_info_5 := 'Rejected';
               v_abt_msg_6 := 'Rejected By    : ';
               v_abt_msg_7 := 'Rejected Date  : ';
            ELSE
               SELECT sr.Role_name, Wwm.Created_Date, wwm.Status
                 INTO V_Abt_Info_6, V_Abt_Info_7, V_Status
                 FROM Wfm_Workflow_Monitors Wwm, ssm_roles sr
                WHERE     Wwm.Workflow_Code = P_Screen_Code
                      AND Wwm.Transaction_Code = P_Transaction_Code
                      AND Wwm.Status IN ('WFST04', 'WFST03')
                      AND wwm.role_code = sr.role_code
                      AND Wwm.Level_Code =
                             (SELECT MIN (Wwms.Level_Code)
                                FROM Wfm_Workflow_Monitors Wwms
                               WHERE     Wwm.Workflow_Code = P_Screen_Code
                                     AND Wwms.Transaction_Code =
                                            P_Transaction_Code
                                     AND Wwms.Status IN ('WFST04', 'WFST03'));

               IF v_status = 'WFST03'
               THEN
                  V_Abt_Info_5 := 'Waiting for Approval';
                  v_abt_msg_6 := 'Pending With   : ';
                  v_abt_msg_7 := 'Pending Date   : ';
               ELSE
                  V_Abt_Info_5 := 'Waiting for Clarification';
                  v_abt_msg_6 := 'Clarification By   : ';
                  v_abt_msg_7 := 'Clarification Date : ';
               END IF;
            END IF;
         ELSE
            V_Abt_Info_5 := 'Not Applicable';
            v_abt_msg_6 := 'Not Applicable';
            v_abt_msg_7 := 'Not Applicable';
         END IF;

         P_Abt_Info_1 := 'Created By      : ' || v_Abt_Info_1;
         P_Abt_Info_2 := 'Created Date    : ' || v_Abt_Info_2;
         P_Abt_Info_3 := 'Modified By     : ' || v_Abt_Info_3;
         P_Abt_Info_4 := 'Modified Date   : ' || v_Abt_Info_4;
         P_Abt_Info_5 := 'Workflow Status : ' || v_Abt_Info_5;
         P_Abt_Info_6 := v_abt_msg_6 || v_Abt_Info_6;
         P_Abt_Info_7 := v_abt_msg_7 || v_Abt_Info_7;
      END IF;
   END;
END;                                          -- * End of workflow_handle Body
/
