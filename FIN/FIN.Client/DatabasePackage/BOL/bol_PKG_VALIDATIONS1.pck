CREATE OR REPLACE PACKAGE PKG_VALIDATIONS1 IS

  PROCEDURE PROC_DELETE_MANAGER(P_MODULE_CODE IN VARCHAR2,
                                P_SCREEN_CODE IN VARCHAR2,
                                P_RECORD_ID   IN VARCHAR2,
                                P_RET         OUT VARCHAR2);

END PKG_VALIDATIONS1;
 
/
CREATE OR REPLACE PACKAGE BODY PKG_VALIDATIONS1 IS

  GV_STR    NVARCHAR2(4000) := NULL;
  GV_STATUS CHAR(1) := 0;
  PROCEDURE PROC_DELETE_MANAGER(P_MODULE_CODE IN VARCHAR2,
                                P_SCREEN_CODE IN VARCHAR2,
                                P_RECORD_ID   IN VARCHAR2,
                                P_RET         OUT VARCHAR2) IS
  BEGIN
    IF P_SCREEN_CODE IS NOT NULL AND P_RECORD_ID IS NOT NULL THEN
      BEGIN
        GV_STATUS := 0;
        FOR LOOP_RUN1 in (SELECT B.LIST_TABLE_NAME AS TABLE_NAME,
                                 A.PK_COLUMN_NAME  AS COLUMN_NAME
                            FROM SSM_MENU_ITEMS A, SSM_LIST_HDR B
                           WHERE A.FORM_CODE = B.LIST_SCREEN_CODE
                             AND TRIM(A.FORM_CODE) = TRIM(P_SCREEN_CODE))

         LOOP
          EXECUTE IMMEDIATE ' UPDATE ' || LOOP_RUN1.TABLE_NAME ||
                            Q'( SET ENABLED_FLAG = '0')' || ' WHERE ' ||
                            LOOP_RUN1.COLUMN_NAME || ' = ' || P_RECORD_ID;
        END LOOP;
        COMMIT;
        GV_STATUS := 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          GV_STR := 'No Valid Base table found!';
          P_RET  := GV_STR;

        WHEN TOO_MANY_ROWS THEN
          GV_STR := 'Many Base table names found!';
          P_RET  := GV_STR;

        WHEN OTHERS THEN
          P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
          P_RET := GV_STR;
      END;

    END IF;
    IF GV_STATUS = '1' THEN
      GV_STR := SSM.Get_Err_Message('EN', 'ERR_047');
      P_RET  := GV_STR;
    ELSE
      GV_STR := 'Unaccepted Action - Need Proper Data feeding';
      P_RET  := GV_STR;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error! ' || CHR(10) || SQLCODE || ' - ' || SQLERRM;
  END PROC_DELETE_MANAGER;

END PKG_VALIDATIONS1;
/
