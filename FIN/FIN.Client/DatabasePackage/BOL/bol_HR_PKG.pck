CREATE OR REPLACE PACKAGE HR_PKG IS

  --- ***************************************************************************************************************
  --- $Header: PKG_GENERAL 1.0 2013-08-1 $
  --- ***************************************************************************************************************
  --- Program       :   HR_PKG
  --- Description        :   Package comprises of HR related procedures and functions - ERP
  --- Author        :   C.S.Ilampooranan ,VMV Systems Pvt Ltd.,
  --- Date    :   01\07\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date         Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    27\08\2013  Ilam
  --- ***************************************************************************************************************

  FUNCTION get_emp_basic(p_emp_id       IN VARCHAR2,
                         p_element_code IN VARCHAR2 DEFAULT 'BAS')
    RETURN NUMBER;

  FUNCTION get_notice_period(p_emp_id IN VARCHAR2) RETURN NUMBER;

  PROCEDURE hr_indemnity_ledger(p_org_id   IN VARCHAR2,
                                p_dept_id  IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_sep_type IN VARCHAR2,
                                p_date     IN DATE);

  FUNCTION get_indemnity_amount(p_org_id   IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_sep_type IN CHAR DEFAULT 'T',
                                p_date     IN DATE DEFAULT SYSDATE)
    RETURN NUMBER;

  FUNCTION get_prev_year_lp(p_org_id   IN VARCHAR2,
                            p_dept_id  IN VARCHAR2,
                            p_emp_id   IN VARCHAR2,
                            p_leave_id IN VARCHAR2,
                            p_date     IN DATE) RETURN NUMBER;

  FUNCTION get_curr_year_lp(p_org_id   IN VARCHAR2,
                            p_dept_id  IN VARCHAR2,
                            p_emp_id   IN VARCHAR2,
                            p_leave_id IN VARCHAR2,
                            p_date     IN DATE) RETURN NUMBER;

  FUNCTION get_leave_consumed_lp(p_org_id   IN VARCHAR2,
                                 p_dept_id  IN VARCHAR2,
                                 p_emp_id   IN VARCHAR2,
                                 p_leave_id IN VARCHAR2,
                                 p_date     IN DATE) RETURN NUMBER;

  FUNCTION get_leave_provision(p_org_id   IN VARCHAR2,
                               p_dept_id  IN VARCHAR2,
                               p_emp_id   IN VARCHAR2,
                               p_leave_id IN VARCHAR2,
                               p_date     IN DATE,
                               p_type     IN VARCHAR2 DEFAULT NULL)
    RETURN NUMBER;

  PROCEDURE post_leave_application(p_fiscal_year IN VARCHAR2,
                                   p_org_id      IN VARCHAR2,
                                   p_dept_id     IN VARCHAR2,
                                   p_staff_id    IN VARCHAR2,
                                   p_leave_id    IN VARCHAR2,
                                   p_from_dt     IN DATE,
                                   p_to_dt       IN DATE);

  PROCEDURE hr_prorate_leave(p_org_id      IN VARCHAR2,
                             p_fiscal_year IN VARCHAR2,
                             p_dept_id     IN VARCHAR2,
                             p_emp_id      IN VARCHAR2,
                             p_doj         IN DATE);

  PROCEDURE hr_update_leave_attendance(p_emp_id          IN VARCHAR2,
                                       p_leave_type      IN VARCHAR2,
                                       p_leave_from_date IN DATE,
                                       p_no_days         IN NUMBER,
                                       p_org_id          IN VARCHAR2,
                                       p_dept_id         IN VARCHAR2,
                                       p_fin_year        IN VARCHAR2,
                                       p_leave_reason    IN VARCHAR2,
                                       p_leave_req_id    IN VARCHAR2,
                                       p_user            IN VARCHAR2);

  PROCEDURE HR_INDEMNITY_PROVOSION(p_org_id  IN VARCHAR2,
                                   p_date    in DATE);

  PROCEDURE proc_leave_provision(p_org_id   IN VARCHAR2,
                                 p_date     IN DATE,
                                 p_type     IN VARCHAR2 DEFAULT 'R');

END HR_PKG;

 
/
CREATE OR REPLACE PACKAGE BODY HR_PKG AS

  FUNCTION get_emp_basic(p_emp_id       IN VARCHAR2,
                         p_element_code IN VARCHAR2 DEFAULT 'BAS')
    RETURN NUMBER IS
    v_base_amount NUMBER := 0;
    v_basic varchar2(50);
  Begin
select sso.hr_basic_element_code into v_basic from ssm_system_options sso;

    --dbms_output.put_line('Parameter '||p_emp_id||'  -  '||p_element_code);
    SELECT
    /*a.pay_element_id,  a.pay_element_code,  a.pay_ele_alias,  b.pay_emp_element_id,  b.pay_emp_id,  b.pay_element_id,*/
     b.pay_amount
      INTO v_base_amount
      FROM pay_elements a, pay_emp_element_value b
     WHERE a.pay_element_id = b.pay_element_id
       AND a.pay_element_code = v_basic
       AND a.enabled_flag = '1'
       AND a.workflow_completion_status = '1'
       AND a.effective_to_dt IS NULL
       AND b.enabled_flag = '1'
       AND b.workflow_completion_status = '1'
       AND b.effective_to_dt IS NULL
       AND b.pay_emp_id = ltrim(rtrim(p_emp_id))
       AND ltrim(rtrim(a.pay_element_code)) = ltrim(rtrim(v_basic));

    RETURN v_base_amount;
  EXCEPTION
    WHEN OTHERS THEN
      --dbms_output.put_line('Err1 '||sqlerrm);
      RETURN 0;
  END get_emp_basic;

  FUNCTION get_notice_period(p_emp_id IN VARCHAR2) RETURN NUMBER IS
    v_notice_period NUMBER := 0;
  BEGIN
    SELECT NVL(NOTICE_PERIOD, 0)
      INTO v_notice_period
      FROM HR_EMPLOYEES
     WHERE EMP_ID = p_emp_id
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';

    RETURN v_notice_period;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_notice_period;

  FUNCTION get_prev_year_lp(p_org_id   IN VARCHAR2,
                            p_dept_id  IN VARCHAR2,
                            p_emp_id   IN VARCHAR2,
                            p_leave_id IN VARCHAR2,
                            p_date     IN DATE) RETURN NUMBER IS

    --This function used to get the leave carry over for the given date
    v_year                NUMBER;
    v_cal_id              VARCHAR2(50);
    v_prev_year_leave_bal NUMBER := 0;
    v_eligible_days       NUMBER := 0;

  BEGIN
    v_year := to_number(to_char(p_date, 'YYYY')) - 1;
    -- v_month_first_day:=LAST_DAY(ADD_MONTHS(p_date,-1))+1;
    --  v_prev_last_day:=LAST_DAY(ADD_MONTHS(p_date,-1));
    -- dbms_output.put_line('From date '||v_from_dt||' To date '||v_to_dt||'  Previous month last day '||v_prev_last_day||' First day of the month '||v_month_first_day);
    --dbms_output.put_line('Previous year of '||to_char(p_date)||' is '||v_year);

    BEGIN
      SELECT CAL_DTL_ID
        INTO v_cal_id
        FROM GL_COMP_ACCT_CALENDAR_DTL
       WHERE CAL_ACCT_YEAR = v_year
         AND COMP_ID = p_org_id;
      dbms_output.put_line('until previous year leave bal for ' || ' - ' ||
                           p_leave_id || ' - ' ||
                           pkg_payroll.get_emp_leave_bal(v_cal_id,
                                                         p_org_id,
                                                         p_dept_id,
                                                         p_emp_id,
                                                         p_leave_id));
      v_prev_year_leave_bal := pkg_payroll.get_emp_leave_bal(v_cal_id,
                                                             p_org_id,
                                                             p_dept_id,
                                                             p_emp_id,
                                                             p_leave_id);
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('ERR33333 ' || SQLERRM);
        v_prev_year_leave_bal := 0;
    END;

    v_eligible_days := v_prev_year_leave_bal;

    dbms_output.put_line('Eligible days ' || v_eligible_days);
    --select pkg_payroll.get_emp_leave_bal('CDI-0000000109','ORG-0000000066','DEPT_ID-0000000012','EMP_ID-0000000037','SL') lbal from dual;
    RETURN v_eligible_days;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_prev_year_lp;

  FUNCTION get_curr_year_lp(p_org_id   IN VARCHAR2,
                            p_dept_id  IN VARCHAR2,
                            p_emp_id   IN VARCHAR2,
                            p_leave_id IN VARCHAR2,
                            p_date     IN DATE) RETURN NUMBER IS

    --This function used to get the leave provision for the current year for as on date
    v_no_days              NUMBER := 0;
    v_from_dt              DATE := TO_DATE('01-JAN-' ||
                                           to_char(p_date, 'YY'),
                                           'DD-MON-YY');
    v_to_dt                DATE := TO_DATE('31-DEC-' ||
                                           to_char(p_date, 'YY'),
                                           'DD-MON-YY');
    v_per_day              NUMBER := 0;
    v_prev_last_day        DATE;
    v_days_till_last_month NUMBER;
    v_eligible_days        NUMBER := 0;
    v_month_first_day      DATE;

  BEGIN

    v_month_first_day := LAST_DAY(ADD_MONTHS(p_date, -1)) + 1;
    v_prev_last_day   := LAST_DAY(ADD_MONTHS(p_date, -1));

    BEGIN
      SELECT NO_OF_DAYS
        INTO v_no_days
        FROM HR_LEAVE_DEFINITIONS
       WHERE LEAVE_ID = p_leave_id
         AND ORG_ID = p_org_id;
    EXCEPTION
      WHEN OTHERS THEN
        v_no_days := 0;
    END;
    dbms_output.put_line(' v_no_days ' || v_no_days);

    v_per_day              := v_no_days /
                              pkg_payroll.get_days(v_from_dt, v_to_dt);
    v_days_till_last_month := v_per_day *
                              (pkg_payroll.get_days(v_from_dt,
                                                    v_prev_last_day));

    v_eligible_days := v_days_till_last_month +
                       (v_per_day *
                       (pkg_payroll.get_days(v_month_first_day, p_date)));

    dbms_output.put_line('Eligible days ' || v_eligible_days);
    --select pkg_payroll.get_emp_leave_bal('CDI-0000000109','ORG-0000000066','DEPT_ID-0000000012','EMP_ID-0000000037','SL') lbal from dual;
    RETURN v_eligible_days;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_curr_year_lp;

  FUNCTION get_leave_consumed_lp(p_org_id   IN VARCHAR2,
                                 p_dept_id  IN VARCHAR2,
                                 p_emp_id   IN VARCHAR2,
                                 p_leave_id IN VARCHAR2,
                                 p_date     IN DATE) RETURN NUMBER IS

    --This function used to get the leave consumed/encashed for the current year for as on date
    v_cal_id_curr_year VARCHAR2(50);
    v_eligible_days    NUMBER := 0;
    v_leave_so_far     NUMBER := 0;
    v_encash           NUMBER := 0;

  BEGIN

    --v_cal_id_curr_year := pkg_payroll.get_fiscal_year(p_org_id,to_number(to_char(p_date,'YYYY')));
    v_cal_id_curr_year := pkg_payroll.get_fiscal_year(p_org_id, p_date);
    dbms_output.put_line(' v_cal_id_curr_year ' || v_cal_id_curr_year);

    SELECT COUNT(*)
      INTO v_leave_so_far
      FROM HR_LEAVE_APPLICATIONS
     WHERE ORG_ID = p_org_id
       AND DEPT_ID = p_dept_id
       AND FISCAL_YEAR = v_cal_id_curr_year
       AND STAFF_ID = p_emp_id
       AND LEAVE_ID = p_leave_id
       AND UPPER(LTRIM(RTRIM(APP_STATUS))) = 'APPROVED'
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';

    dbms_output.put_line(' v_leave_so_far ' || v_leave_so_far);

    SELECT SUM(LC_NO_OF_DAYS)
      INTO v_encash
      FROM HR_LEAVE_ENCASHMENT
     WHERE LC_DEPT_ID = p_dept_id
       AND LC_ORG_ID = p_org_id
       AND LC_EMP_ID = p_emp_id
       AND LEAVE_ID = p_leave_id
       AND LC_FISCAL_YEAR = v_cal_id_curr_year;

    IF v_leave_so_far > 0 THEN
      v_leave_so_far := pkg_payroll.get_emp_leave_bal(v_cal_id_curr_year,
                                                      p_org_id,
                                                      p_dept_id,
                                                      p_emp_id,
                                                      p_leave_id);
    END IF;

    v_eligible_days := v_leave_so_far + v_encash;

    dbms_output.put_line('Eligible days ' || v_eligible_days);
    --select pkg_payroll.get_emp_leave_bal('CDI-0000000109','ORG-0000000066','DEPT_ID-0000000012','EMP_ID-0000000037','SL') lbal from dual;
    RETURN v_eligible_days;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_leave_consumed_lp;

  FUNCTION get_leave_provision(p_org_id   IN VARCHAR2,
                               p_dept_id  IN VARCHAR2,
                               p_emp_id   IN VARCHAR2,
                               p_leave_id IN VARCHAR2,
                               p_date     IN DATE,
                               p_type     IN VARCHAR2 DEFAULT NULL)
    RETURN NUMBER IS

    v_year                 NUMBER;
    v_cal_id               VARCHAR2(50);
    v_cal_id_curr_year     VARCHAR2(50);
    v_no_days              NUMBER := 0;
    v_from_dt              DATE := TO_DATE('01-JAN-' ||
                                           to_char(p_date, 'YY'),
                                           'DD-MON-YY');
    v_to_dt                DATE := TO_DATE('31-DEC-' ||
                                           to_char(p_date, 'YY'),
                                           'DD-MON-YY');
    v_per_day              NUMBER := 0;
    v_year_day             NUMBER := 0;
    v_prev_last_day        DATE;
    v_prev_year_leave_bal  NUMBER := 0;
    v_days_till_last_month NUMBER;
    v_eligible_days        NUMBER := 0;
    v_leave_so_far         NUMBER := 0;
    v_encash               NUMBER := 0;
    v_month_first_day      DATE;

  BEGIN
    dbms_output.put_line('p_date ' || p_date || ' - ' || ' p_leave_id ' ||p_leave_id || ' - ' || ' p_dept_id ' || p_dept_id ||' - ' || ' p_emp_id ' || p_emp_id);
    v_year            := to_number(to_char(p_date, 'YYYY')) - 1;
    v_month_first_day := LAST_DAY(ADD_MONTHS(p_date, -1)) + 1;
    v_prev_last_day   := LAST_DAY(ADD_MONTHS(p_date, -1));
   -- dbms_output.put_line('From date ' || v_from_dt || ' To date ' ||v_to_dt || '  Previous month last day ' ||v_prev_last_day || ' First day of the month ' ||v_month_first_day);
    dbms_output.put_line('Previous year of ' || to_char(p_date) || ' is ' ||v_year);

    --start

    BEGIN
      SELECT --B.CAL_DTL_ID
       B.CAL_EFF_START_DT, B.CALL_EFF_END_DT
        INTO v_from_dt, v_to_dt
        FROM GL_ACCT_CALENDAR_HDR      A,
             GL_ACCT_CALENDAR_DTL      B,
             GL_COMP_ACCT_CALENDAR_DTL C
       WHERE B.CAL_ID = A.CAL_ID
         AND C.CAL_ID = A.CAL_ID
         AND C.CAL_DTL_ID = B.CAL_DTL_ID
         AND C.CAL_ACCT_YEAR = B.CAL_ACCT_YEAR
         AND C.COMP_ID = p_org_id
            --AND B.CAL_ACCT_YEAR = p_year
            -- AND B.CAL_EFF_START_DT>=p_year
            -- AND B.CALL_EFF_END_DT<=p_year
         AND p_date BETWEEN B.CAL_EFF_START_DT AND B.CALL_EFF_END_DT
         AND A.ENABLED_FLAG = '1'
         AND A.WORKFLOW_COMPLETION_STATUS = '1'
         AND B.ENABLED_FLAG = '1'
         AND B.WORKFLOW_COMPLETION_STATUS = '1'
         AND C.ENABLED_FLAG = '1'
         AND C.WORKFLOW_cOMPLETION_STATUS = '1';
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('ERR55555 ' || SQLERRM);
    END;
    DBMS_OUTPUT.PUT_LINE('dates ' || v_from_dt || ' - ' || v_to_dt);
    --end
    /*
      BEGIN
        SELECT CAL_DTL_ID
          INTO v_cal_id
          FROM GL_COMP_ACCT_CALENDAR_DTL
         WHERE CAL_ACCT_YEAR = v_year
           AND COMP_ID = p_org_id;
        dbms_output.put_line('until previous year leave bal for ' || ' - ' || p_leave_id ||' - '||v_cal_id|| ' - ' ||pkg_payroll.get_emp_leave_bal(v_cal_id,p_org_id,p_dept_id,p_emp_id,p_leave_id));
        v_prev_year_leave_bal := pkg_payroll.get_emp_leave_bal(v_cal_id,
                                                               p_org_id,
                                                               p_dept_id,
                                                               p_emp_id,
                                                               p_leave_id);
      EXCEPTION
        WHEN OTHERS THEN
          --DBMS_OUTPUT.PUT_LINE('ERR33333 ' || SQLERRM);
          BEGIN
            SELECT CAL_DTL_ID
              INTO v_cal_id
              FROM GL_COMP_ACCT_CALENDAR_DTL
             WHERE CAL_ACCT_YEAR = to_number(to_char(p_date, 'YYYY'))
               AND COMP_ID = p_org_id;
         dbms_output.put_line('I am here');
          EXCEPTION
            WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('ERR444444 ' || SQLERRM);
              RAISE_APPLICATION_ERROR(-20001,
                                      'Please check the Accounting calendar for ' ||
                                      p_org_id || ' - For year ' || v_year);
          END;
      END;
    */
    --v_cal_id_curr_year := pkg_payroll.get_fiscal_year(p_org_id,to_number(to_char(p_date,'YYYY')));
    v_cal_id_curr_year := pkg_payroll.get_fiscal_year(p_org_id, p_date);
    dbms_output.put_line(' v_cal_id_curr_year ' || v_cal_id_curr_year);

    SELECT --COUNT(*)
     SUM(NO_OF_DAYS)
      INTO v_leave_so_far
      FROM HR_LEAVE_APPLICATIONS
     WHERE ORG_ID = p_org_id
       AND DEPT_ID = p_dept_id
       AND FISCAL_YEAR = v_cal_id_curr_year
       AND STAFF_ID = p_emp_id
       AND LEAVE_ID = p_leave_id
       AND UPPER(LTRIM(RTRIM(APP_STATUS))) = 'APPROVED'
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1'
     AND p_date BETWEEN LEAVE_DATE_FROM AND LEAVE_DATE_TO;

    dbms_output.put_line(' v_leave_so_far ' || v_leave_so_far);
    /*
      IF UPPER(LTRIM(RTRIM(p_type)))='SETTLEMENT' THEN
        SELECT SUM(LC_NO_OF_DAYS)
          INTO v_encash
          FROM HR_LEAVE_ENCASHMENT
         WHERE LC_DEPT_ID = p_dept_id
           AND LC_ORG_ID = p_org_id
           AND LC_EMP_ID = p_emp_id
           AND LEAVE_ID = p_leave_id
           AND LC_FISCAL_YEAR = v_cal_id_curr_year;
      END IF;
    */
    BEGIN
      SELECT NO_OF_DAYS
        INTO v_no_days
        FROM HR_LEAVE_STAFF_DEFINTIONS HLSD
       WHERE HLSD.LEAVE_ID = p_leave_id
         AND HLSD.ORG_ID = p_org_id
         AND HLSD.STAFF_ID = p_emp_id
         AND HLSD.FISCAL_YEAR = v_cal_id_curr_year;
    EXCEPTION
      WHEN OTHERS THEN
        v_no_days := 0;
    END;

    v_per_day              := v_no_days /pkg_payroll.get_days(v_from_dt, v_to_dt);
    v_days_till_last_month := v_per_day *(pkg_payroll.get_days(v_from_dt,v_prev_last_day));
    /*
      IF v_leave_so_far > 0 THEN
        v_leave_so_far := pkg_payroll.get_emp_leave_bal(v_cal_id_curr_year,
                                                        p_org_id,
                                                        p_dept_id,
                                                        p_emp_id,
                                                        p_leave_id);
      END IF;
    */
    dbms_output.put_line('v_leave_so_far ' || v_leave_so_far || ' for ' ||v_cal_id_curr_year);
    dbms_output.put_line('Encashed ' || v_encash || ' ~ ' ||v_cal_id_curr_year);
    dbms_output.put_line('v_days_till_last_month ' ||v_days_till_last_month);
    dbms_output.put_line(' Start of month till as on date ' ||(v_per_day *(pkg_payroll.get_days(v_month_first_day, p_date))));

    v_eligible_days := v_days_till_last_month + v_prev_year_leave_bal +(v_per_day *(pkg_payroll.get_days(v_month_first_day, p_date)));
    dbms_output.put_line('Eligible days before '||v_eligible_days);
    v_eligible_days := nvl(v_eligible_days, 0) -(nvl(v_leave_so_far, 0) + nvl(v_encash, 0));
    /*
    IF v_leave_so_far<0 THEN
      v_eligible_days:=v_leave_so_far;
    ELSE
      IF v_leave_so_far=0  THEN
        v_eligible_days:=0;
      ELSE
      v_eligible_days:=v_eligible_days-v_leave_so_far;
      END IF;
    END IF;
    */
    --DBMS_OUTPUT.PUT_LINE('v_cal_id '||v_cal_id||' Leave Definition days '||v_no_days||' Till last month days '||v_days_till_last_month);
    dbms_output.put_line('Eligible days ' || v_eligible_days);
    --select pkg_payroll.get_emp_leave_bal('CDI-0000000109','ORG-0000000066','DEPT_ID-0000000012','EMP_ID-0000000037','SL') lbal from dual;
    RETURN v_eligible_days;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_leave_provision;

  FUNCTION get_indemnity_amount(p_org_id   IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_sep_type IN CHAR DEFAULT 'T',
                                p_date     IN DATE DEFAULT SYSDATE)
    RETURN NUMBER IS

    Cursor emp_cur is
      select *
        from hr_employees
       where emp_id = p_emp_id
         and emp_doj IS NOT NULL;

    CURSOR leave_cur(c_staff_id VARCHAR2, c_org_id VARCHAR2) IS
      SELECT distinct c.leave_id leave_id, B.ATTRIBUTE1, a.FISCAL_YEAR
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
         AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = c_org_id
            -- AND a.FISCAL_YEAR=c_fiscal_year
         AND a.STAFF_ID = c_staff_id
         AND a.ATTENDANCE_DATE <= p_date; --'31-DEC-14';
    --AND C.LEAVE_ID='CLD';

    v_lop_factor NUMBER := 1;
    tmp_lop_days NUMBER := 0;
    v_lop_days   NUMBER := 0;

    v_service_years    NUMBER;
    v_indem_period     NUMBER;
    tmp_indem_value    number := 0;
    v_last_value       number := 0;
    v_sep_type         CHAR(1) := p_sep_type;
    v_basic_amt        NUMBER := 0;
    v_basic_4_2years   NUMBER := 0;
    v_indem_amt        NUMBER := 0;
    v_dept_id          VARCHAR2(50);
    v_annual_leave_bal NUMBER := 0;
    v_notice_period    NUMBER := 0;
    v_sys_option1      SSM_SYSTEM_OPTIONS.HR_EARN_LEAVE%TYPE;
  BEGIN

    BEGIN
      SELECT X.HR_EARN_LEAVE
        INTO v_sys_option1
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.MODULE_CODE = 'HR'
         AND X.HR_EFFECTIVE_TO_DATE IS NULL;
    EXCEPTION
      WHEN OTHERS THEN
        --dbms_output.put_line('Err sys '||sqlerrm);
        v_sys_option1 := NULL;
    END;

    v_dept_id := pkg_payroll.get_dept_id(p_emp_id);
    --Annual Leave Balance
    v_annual_leave_bal := hr_pkg.get_leave_provision(p_org_id,
                                                     v_dept_id,
                                                     p_emp_id,
                                                     v_sys_option1,
                                                     p_date) / 365;
    --Notice Period
    v_notice_period := hr_pkg.get_notice_period(p_emp_id) / 365;

    --to be calculated for the indeminity amt
    for m in emp_cur loop
      v_service_years := ROUND((MONTHS_BETWEEN(TRUNC(p_date), m.emp_doj) / 12),
                               1);

      dbms_output.put_line('Employee ' || m.emp_no || ' - ' ||
                           m.emp_first_name || ' - ' || to_char(m.emp_doj) ||
                           ' -Service years ' || v_service_years || ' - ' ||
                           v_dept_id);
      v_service_years := v_service_years + v_annual_leave_bal +
                         v_notice_period;
      dbms_output.put_line('v_service_years1 ' || v_service_years);
      --Unpaid Leave Calculation Starts here
      FOR n IN leave_cur(p_emp_id, p_org_id) Loop
        pkg_payroll.get_lop_days(p_org_id      => p_org_id,
                                 p_fiscal_year => n.fiscal_year, --'CDI-0000000075',
                                 p_dept_id     => v_dept_id, --'DEPT_ID-0000000012',
                                 p_staff_id    => p_emp_id,
                                 p_leave_id    => n.leave_id,
                                 p_from_date   => m.emp_doj,
                                 p_to_date     => p_date,
                                 p_level       => n.ATTRIBUTE1,
                                 p_lop_factor  => v_lop_factor,
                                 p_lop_days    => tmp_lop_days);
        v_lop_days := v_lop_days + tmp_lop_days;
      END LOOP;
      IF v_lop_days > ssm.get_parameter_value('INDEM') THEN
        v_lop_days := v_lop_days - ssm.get_parameter_value('INDEM');
      END IF;
      dbms_output.put_line('LOP Days in Indeminity Calc ' || v_lop_days);
      --Unpaid Leave Calculation Ends here

      SELECT MAX(INDEM_LOW_VALUE)
        INTO v_last_value
        FROM HR_INDEM_CALC_SLAB
       WHERE INDEM_HIGH_VALUE IS NULL
         AND ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND INDEM_TYPE = p_sep_type;

      dbms_output.put_line('v_last_value' || v_last_value);
      IF v_service_years > v_last_value THEN
        BEGIN
          SELECT INDEM_VALUE
            INTO tmp_indem_value
            FROM HR_INDEM_CALC_SLAB
           WHERE INDEM_HIGH_VALUE IS NULL
             AND ENABLED_FLAG = '1'
             AND WORKFLOW_COMPLETION_STATUS = '1'
             AND INDEM_TYPE = p_sep_type;
        EXCEPTION
          WHEN OTHERS THEN
            tmp_indem_value := 0;
        END;
        tmp_indem_value := tmp_indem_value / 365;
        dbms_output.put_line('tmp_indem_value ' || tmp_indem_value);

        SELECT SUM(INDEM_VALUE)
          INTO v_indem_period
          FROM HR_INDEM_CALC_SLAB
         WHERE INDEM_HIGH_VALUE <= v_service_years
           AND ENABLED_FLAG = '1'
           AND WORKFLOW_COMPLETION_STATUS = '1'
           AND INDEM_TYPE = p_sep_type;

        dbms_output.put_line('v_indem_period 1  ' || v_indem_period);
        v_service_years := v_service_years - v_last_value;
        tmp_indem_value := tmp_indem_value * v_service_years;
        dbms_output.put_line('tmp_indem_value1 ' || tmp_indem_value);
        v_indem_period := v_indem_period + tmp_indem_value;
      ELSE
        SELECT SUM(INDEM_VALUE)
          INTO v_indem_period
          FROM HR_INDEM_CALC_SLAB
         WHERE ENABLED_FLAG = '1'
           AND WORKFLOW_COMPLETION_STATUS = '1'
           AND INDEM_TYPE = p_sep_type
           AND INDEM_HIGH_VALUE <=
               (SELECT INDEM_HIGH_VALUE
                  FROM HR_INDEM_CALC_SLAB
                 WHERE v_service_years >= INDEM_LOW_VALUE
                   AND v_service_years < INDEM_HIGH_VALUE);
      END IF;

      v_indem_period := v_indem_period - v_lop_days;
      dbms_output.put_line('Indeminity Period ' || v_indem_period);
      v_basic_amt := get_emp_basic(p_emp_id);
      dbms_output.put_line('v_basic_amt ' || v_basic_amt);
      v_basic_4_2years := v_basic_amt * 24;
      v_indem_amt      := v_indem_period * v_basic_amt / 26;

      IF v_indem_amt > v_basic_4_2years THEN
        --dbms_output.put_line('I am here');
        RETURN v_basic_4_2years;
      ELSE
        RETURN v_indem_amt;
      END IF;
    end loop;
    RETURN 0;

  EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Indem err ' || sqlerrm);
      RETURN 0;
  END get_indemnity_amount;
  PROCEDURE HR_INDEMNITY_PROVOSION(p_org_id  IN VARCHAR2,
                                   p_date    in DATE) IS
  v_emp_years number;

  v_eligible_days number:=0;
v_indem_amount number;
v_basic_code varchar2(50);
v_basic_amount number;
v_fixed_days number;
v_days number;
v_indem_open_bal number;

  BEGIN

v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');
if v_fixed_days > 0 then
  v_days := v_fixed_days;
else
  v_days := pkg_payroll.get_days(p_from_dt => trunc(p_date,'MM'),
                                         p_to_dt   => p_date);
  end if;
delete from HR_INDEM_TXN_LEDGER hitl where hitl.indem_txn_date=p_date;
 
for emp_dt in (select * from hr_employees he where he.emp_org_id=p_org_id) loop

  --select ((p_date-he.emp_doj)+1)/365 into v_emp_years from hr_employees he where he.emp_id=emp_id;

v_emp_years:= ((p_date-emp_dt.emp_doj)+1)/365;

if v_emp_years >0 then

select sso.hr_basic_element_code into v_basic_code from ssm_system_options sso;

    SELECT
     b.pay_amount
      INTO v_basic_amount
      FROM pay_emp_element_value b
     WHERE b.pay_element_id = v_basic_code
       AND b.enabled_flag = '1'
       AND b.workflow_completion_status = '1'
       AND nvl(b.effective_to_dt,p_date)>= p_date
       AND b.pay_emp_id = emp_dt.emp_id;

v_eligible_days:=0;

for indem_slab in (select * from hr_indem_calc_slab hics where hics.indem_name=emp_dt.indem_id order by hics.indem_high_value) loop

    if v_emp_years > indem_slab.indem_high_value then
        v_eligible_days := v_eligible_days+ (indem_slab.indem_high_value)*indem_slab.indem_value;
    elsif v_emp_years < indem_slab.indem_high_value and v_emp_years > indem_slab.indem_low_value then
        v_eligible_days := v_eligible_days+ (v_emp_years-indem_slab.indem_low_value)*indem_slab.indem_value;
    end if;
          
end loop;

begin
select hitl.indem_amt_bal into v_indem_open_bal 
from HR_INDEM_TXN_LEDGER hitl where hitl.indem_org_id=p_org_id and
hitl.indem_emp_id=emp_dt.emp_id and hitl.indem_txn_date=add_months(p_date,-1);
exception when others then
  v_indem_open_bal:=0;
  end;
   v_indem_amount := (v_eligible_days*(v_basic_amount/v_days))-v_indem_open_bal;

          INSERT INTO HR_INDEM_TXN_LEDGER 
          (INDEM_LDR_ID,
           INDEM_ORG_ID,
           INDEM_DEPT_ID,
           INDEM_EMP_ID,
           INDEM_TXN_TYPE,
           INDEM_AMT_BAL,
           INDEM_TXN_DATE,
           INDEM_SERVICE_YEARS,
           INDEM_ELIGIBLE_DAYS,
           INDEM_SEP_TYPE,
           INDEM_REMARKS,
           INDEM_EMP_SEPERATED,
           INDEM_SEPERATION_DT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           INDEM_OPEN_BAL,
           INDEM_AMT_AVAIL)
        VALUES
          (ssm.get_next_sequence('HR_101', 'EN'),
           p_org_id,
           null,
           emp_dt.emp_id,
           'PROVISION',
           v_indem_open_bal+v_indem_amount,
           p_date,
           v_emp_years,
           v_eligible_days,
           'Provision',
           'Provision Indemnity Amount for ' || to_char(p_date),
           '0',
           NULL,
           '1',
           '1',
           'SYSTEM',
           SYSDATE,
           v_indem_open_bal,
           v_indem_amount);
end if;

end loop;

  END HR_INDEMNITY_PROVOSION;
  PROCEDURE hr_indemnity_ledger(p_org_id   IN VARCHAR2,
                                p_dept_id  IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_sep_type IN VARCHAR2,
                                p_date     IN DATE) IS

    v_lop_factor        NUMBER := 1;
    tmp_lop_days        NUMBER := 0;
    v_lop_days          NUMBER := 0;
    v_service_years     NUMBER := 0;
    v_bal               NUMBER := 0;
    v_last_value        number := 0;
    v_indem_period      NUMBER;
    tmp_indem_value     number := 0;
    v_from_dt           DATE; --:='31-DEC-13';
    v_to_dt             DATE := p_date; --'31-JAN-14';
    v_days              NUMBER := 0;
    v_basic_4_2years    NUMBER := 0;
    v_indem_amt         NUMBER := 0;
    v_basic_amt         NUMBER := 0;
    tmp_service_years   NUMBER := 0;
    v_annual_leave_bal  NUMBER := 0;
    v_notice_period     NUMBER := 0;
    v_old_eligible_days NUMBER := 0;
    v_sys_option1       SSM_SYSTEM_OPTIONS.HR_EARN_LEAVE%TYPE;

    CURSOR leave_cur(c_staff_id VARCHAR2, c_org_id VARCHAR2, c_date DATE) IS
      SELECT distinct c.leave_id leave_id, B.ATTRIBUTE1, a.FISCAL_YEAR
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
         AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = c_org_id
            -- AND a.FISCAL_YEAR=c_fiscal_year
         AND a.STAFF_ID = c_staff_id
         AND a.ATTENDANCE_DATE <= c_date; --p_date;--'31-DEC-14';
    --AND C.LEAVE_ID='CLD';

  BEGIN

    BEGIN
      SELECT X.HR_EARN_LEAVE
        INTO v_sys_option1
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.MODULE_CODE = 'HR'
         AND X.HR_EFFECTIVE_TO_DATE IS NULL;
    EXCEPTION
      WHEN OTHERS THEN
        --dbms_output.put_line('Err sys '||sqlerrm);
        v_sys_option1 := NULL;
    END;

    --Annual Leave Balance
    --Annual Leave ID hardcoded to be removed
    v_annual_leave_bal := hr_pkg.get_leave_provision(p_org_id,
                                                     p_dept_id,
                                                     p_emp_id,
                                                     v_sys_option1,
                                                     p_date) / 365;
    --Notice Period
    v_notice_period := hr_pkg.get_notice_period(p_emp_id) / 365;

    --to be calculated for the indeminity amt
    BEGIN
      SELECT X.INDEM_SERVICE_YEARS,
             X.INDEM_AMT_BAL,
             X.INDEM_TXN_DATE,
             X.INDEM_ELIGIBLE_DAYS
        INTO v_service_years, v_bal, v_from_dt, v_old_eligible_days
        FROM HR_INDEM_TXN_LEDGER X
       WHERE X.INDEM_ORG_ID = p_org_id
         AND X.INDEM_DEPT_ID = p_dept_id
         AND X.INDEM_EMP_ID = p_emp_id
         AND X.INDEM_LDR_ID =
             (SELECT MAX(INDEM_LDR_ID)
                FROM HR_INDEM_TXN_LEDGER
               WHERE INDEM_ORG_ID = p_org_id
                 AND INDEM_DEPT_ID = p_dept_id
                 AND INDEM_EMP_ID = p_emp_id);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_service_years     := 0;
        v_bal               := 0;
        v_old_eligible_days := 0;
        select he.emp_doj
          into v_from_dt
          from hr_employees he
         where he.emp_id = p_emp_id;
      WHEN OTHERS THEN
        v_service_years := 0;
        v_bal           := 0;
    END;
    --Unpaid Leave Calculation Starts here
    FOR n IN leave_cur(p_emp_id, p_org_id, p_date) Loop
      pkg_payroll.get_lop_days(p_org_id      => p_org_id,
                               p_fiscal_year => n.fiscal_year, --'CDI-0000000075',
                               p_dept_id     => p_dept_id, --'DEPT_ID-0000000012',
                               p_staff_id    => p_emp_id,
                               p_leave_id    => n.leave_id,
                               p_from_date   => v_from_dt, --m.emp_doj,
                               p_to_date     => p_date,
                               p_level       => n.ATTRIBUTE1,
                               p_lop_factor  => v_lop_factor,
                               p_lop_days    => tmp_lop_days);
      v_lop_days := v_lop_days + tmp_lop_days;
    END LOOP;
    IF v_lop_days > ssm.get_parameter_value('INDEM') THEN
      v_lop_days := v_lop_days - ssm.get_parameter_value('INDEM');
    END IF;
    dbms_output.put_line('LOP Days in Indeminity Calc ' || v_lop_days);
    --Unpaid Leave Calculation Ends here

    dbms_output.put_line('v_service_years ' || v_service_years ||
                         ' - v_bal ' || v_bal || ' -v_from_dt ' ||
                         to_char(v_from_dt) || ' v_to_dt ' ||
                         to_char(v_to_dt));
    v_days := pkg_payroll.get_days(v_from_dt + 1, v_to_dt) / 365;
    dbms_output.put_line('v_days ' ||
                         pkg_payroll.get_days(v_from_dt + 1, v_to_dt));
    dbms_output.put_line('Converted year ' || v_days || ' - Annual days ' ||
                         v_annual_leave_bal || ' -Notice Period ' ||
                         v_notice_period);
    v_service_years   := nvl(v_service_years, 0) + nvl(v_days, 0) +
                         nvl(v_annual_leave_bal, 0) +
                         nvl(v_notice_period, 0);
    tmp_service_years := v_service_years;
    dbms_output.put_line('tmp_service_years ' || tmp_service_years);
    SELECT MAX(INDEM_LOW_VALUE)
      INTO v_last_value
      FROM HR_INDEM_CALC_SLAB
     WHERE INDEM_HIGH_VALUE IS NULL
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1'
       AND INDEM_TYPE = p_sep_type;
    -- v_last_value:=v_last_value/365;
    dbms_output.put_line('v_last_value  ' || v_last_value);

    IF v_service_years > v_last_value THEN
      BEGIN
        SELECT INDEM_VALUE
          INTO tmp_indem_value
          FROM HR_INDEM_CALC_SLAB
         WHERE INDEM_HIGH_VALUE IS NULL
           AND ENABLED_FLAG = '1'
           AND WORKFLOW_COMPLETION_STATUS = '1'
           AND INDEM_TYPE = p_sep_type;
      EXCEPTION
        WHEN OTHERS THEN
          tmp_indem_value := 0;
      END;
      tmp_indem_value := tmp_indem_value / 365;
      dbms_output.put_line('tmp_indem_value ' || tmp_indem_value);

      SELECT SUM(INDEM_VALUE)
        INTO v_indem_period
        FROM HR_INDEM_CALC_SLAB
       WHERE INDEM_HIGH_VALUE <= v_service_years
         AND ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND INDEM_TYPE = p_sep_type;

      v_indem_period := v_indem_period / 365;

      dbms_output.put_line('v_indem_period1 ' || v_indem_period);
      v_service_years := v_service_years - v_last_value;
      dbms_output.put_line('v_service_years after deduction ' ||
                           v_service_years);
      tmp_indem_value := tmp_indem_value * v_service_years;
      dbms_output.put_line('tmp_indem_value1 ' || tmp_indem_value);
      v_indem_period := v_indem_period + tmp_indem_value;
    ELSE
      SELECT SUM(INDEM_VALUE)
        INTO v_indem_period
        FROM HR_INDEM_CALC_SLAB
       WHERE ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND INDEM_TYPE = p_sep_type
         AND INDEM_HIGH_VALUE <=
             (SELECT INDEM_HIGH_VALUE
                FROM HR_INDEM_CALC_SLAB
               WHERE v_service_years >= INDEM_LOW_VALUE
                 AND v_service_years < INDEM_HIGH_VALUE);
    END IF;
    dbms_output.put_line('v_indem_period ' || v_indem_period);
    v_indem_period      := v_indem_period - v_lop_days;
    v_old_eligible_days := v_old_eligible_days + v_indem_period;
    --v_indem_period:=v_indem_period/365;
    dbms_output.put_line('Indeminity Period ' || v_indem_period);
    v_basic_amt := hr_pkg.get_emp_basic(p_emp_id);
    dbms_output.put_line('v_basic_amt ' || v_basic_amt);
    v_basic_4_2years := v_basic_amt * 24;
    v_indem_amt      := v_indem_period * v_basic_amt / 26;
    v_indem_amt      := v_indem_amt + v_bal;
    dbms_output.put_line('v_indem_amt ' || v_indem_amt);
    dbms_output.put_line('v_old_eligible_days ' || v_old_eligible_days);
    IF v_indem_amt > v_basic_4_2years THEN
      v_indem_amt := v_basic_4_2years;
    END IF;
    --dbms_output.put_line('v_indem_amt '||v_indem_amt);

    IF v_indem_amt > 0 THEN
      BEGIN
        INSERT INTO HR_INDEM_TXN_LEDGER
          (INDEM_LDR_ID,
           INDEM_ORG_ID,
           INDEM_DEPT_ID,
           INDEM_EMP_ID,
           INDEM_TXN_TYPE,
           INDEM_AMT_BAL,
           INDEM_TXN_DATE,
           INDEM_SERVICE_YEARS,
           INDEM_ELIGIBLE_DAYS,
           INDEM_SEP_TYPE,
           INDEM_REMARKS,
           INDEM_EMP_SEPERATED,
           INDEM_SEPERATION_DT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE)
        VALUES
          (ssm.get_next_sequence('HR_101', 'EN'),
           p_org_id,
           p_dept_id,
           p_emp_id,
           'CUMULATIVE',
           v_indem_amt,
           p_date,
           tmp_service_years,
           v_old_eligible_days,
           p_sep_type,
           'Cumulative Indemnity Amount for ' || to_char(p_date),
           '0',
           NULL,
           '1',
           '1',
           'System',
           SYSDATE);
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('EER333 3333' || SQLERRM);
      END;
    END IF;
    COMMIT;

  END hr_indemnity_ledger;

  PROCEDURE post_leave_application(p_fiscal_year IN VARCHAR2,
                                   p_org_id      IN VARCHAR2,
                                   p_dept_id     IN VARCHAR2,
                                   p_staff_id    IN VARCHAR2,
                                   p_leave_id    IN VARCHAR2,
                                   p_from_dt     IN DATE,
                                   p_to_dt       IN DATE) IS
    v_from_dt  DATE := p_from_dt;
    v_to_dt    DATE := p_to_dt;
    v_req_id   HR_LEAVE_APPLICATIONS.LEAVE_REQ_ID%TYPE;
    v_leave_id HR_LEAVE_DEFINITIONS.LEAVE_ID%TYPE;
  BEGIN
    BEGIN
      SELECT LEAVE_REQ_ID, HLA.LEAVE_ID
        INTO v_req_id, v_leave_id
        FROM HR_LEAVE_APPLICATIONS HLA
       WHERE ORG_ID = p_org_id
         AND FISCAL_YEAR = p_fiscal_year
         AND DEPT_ID = p_dept_id
         AND STAFF_ID = p_staff_id
         AND LEAVE_ID = p_leave_id
         AND LEAVE_DATE_FROM = p_from_dt
         AND LEAVE_DATE_TO = p_to_dt
         AND UPPER(LTRIM(RTRIM(APP_STATUS))) = 'APPROVED';
    EXCEPTION
      WHEN OTHERS THEN
        v_req_id := NULL;
    END;

    WHILE v_from_dt <= v_to_dt LOOP
      dbms_output.put_line('Date ' || v_from_dt);
      BEGIN
        INSERT INTO HR_STAFF_ATTENDANCE
          (STATT_ID,
           ORG_ID,
           FISCAL_YEAR,
           STAFF_ID,
           ATTENDANCE_DATE,
           ATTENDANCE_TYPE,
           LEAVE_REQ_ID,
           REASON,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           DEPT_ID)
        VALUES
          (ssm.get_next_sequence('HR_022', 'EN'),
           p_org_id,
           p_fiscal_year,
           p_staff_id,
           v_from_dt,
           v_leave_id,
           v_req_id,
           'On Leave',
           '1',
           '1',
           user,
           sysdate,
           p_dept_id);
      EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('Error post ' || sqlerrm);
      END;
      v_from_dt := v_from_dt + 1;
    END LOOP;
    COMMIT;
  END post_leave_application;

  PROCEDURE hr_prorate_leave(p_org_id      IN VARCHAR2,
                             p_fiscal_year IN VARCHAR2,
                             p_dept_id     IN VARCHAR2,
                             p_emp_id      IN VARCHAR2,
                             p_doj         IN DATE) IS

    CURSOR dep_cur(c_dept_id VARCHAR2, c_fiscal_year VARCHAR2) IS
      select a.leave_id, b.dept_id, b.fiscal_year, a.no_of_days
        from hr_leave_dept b, hr_leave_definitions a
       where a.leave_id = b.leave_id
         and b.dept_id = c_dept_id
         and b.fiscal_year = c_fiscal_year;

    v_from_dt DATE;
    v_to_dt   DATE;
    --p_emp_id VARCHAR2(50):='EMP_ID-0000000037';
    --p_doj DATE :='23-JUL-05';
    --p_org_id VARCHAR2(50):='ORG-0000000066';
    --p_fiscal_year VARCHAR2(50):='CDI-0000000108';
    --p_dept_id VARCHAR2(50):='DEPT_ID-0000000012';
    v_fiscal_months NUMBER := 0;
    v_doj_months    NUMBER := 0;
    v_permonth_days NUMBER := 0;
    v_seq           VARCHAR2(50);
  BEGIN

    SELECT MIN(PERIOD_FROM_DT), MAX(PERIOD_TO_DT)
      INTO v_from_dt, v_to_dt
      FROM GL_ACCT_PERIOD_DTL
     WHERE CAL_DTL_ID = p_fiscal_year
       AND PERIOD_TYPE = 'ACTPERIOD';

    dbms_output.put_line('Financial year start and end date ' || v_from_dt ||
                         ' - ' || v_to_dt);

    FOR m in dep_cur(p_dept_id, p_fiscal_year) LOOP
      v_fiscal_months := ROUND(MONTHS_BETWEEN(v_to_dt, v_from_Dt));
      v_doj_months    := MONTHS_BETWEEN(v_to_dt, p_doj);
      v_permonth_days := (m.no_of_days / v_fiscal_months) * v_doj_months;
      dbms_output.put_line('Dept Leaves are ' || m.leave_id || ' ~ ' ||
                           m.leave_id || ' ~ ' || m.fiscal_year || ' ~ ' ||
                           m.no_of_days || ' ~ ' || v_fiscal_months ||
                           ' ~ ' || v_doj_months || ' ~ ' ||
                           v_permonth_days);
      BEGIN
        v_seq := ssm.get_next_sequence('HR_021', 'EN');
        INSERT INTO HR_LEAVE_STAFF_DEFINTIONS
          (LSD_ID,
           ORG_ID,
           FISCAL_YEAR,
           STAFF_ID,
           LEAVE_ID,
           DEPT_ID,
           LEAVE_AVAILED,
           LEAVE_BALANCE,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           NO_OF_DAYS)
        VALUES
          (v_seq,
           p_org_id,
           p_fiscal_year,
           p_emp_id,
           m.leave_id,
           p_dept_id,
           0,
           v_permonth_days,
           '1',
           '1',
           'System',
           SYSDATE,
           v_permonth_days);
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('ERR LEAVE ' || SQLERRM);
      END;

      pkg_payroll.emp_leave_txn_ledger(p_tran_type   => 'OPENBAL',
                                       p_tran_id     => v_seq,
                                       p_txn_type    => 'LSD',
                                       p_trn_date    => p_doj,
                                       p_trn_month   => TO_NUMBER(TO_CHAR(p_doj,
                                                                          'MM')),
                                       p_fiscal_year => p_fiscal_year,
                                       p_emp_id      => p_emp_id,
                                       p_org_id      => p_org_id,
                                       p_leave_id    => m.LEAVE_ID,
                                       p_dept_id     => p_dept_id,
                                       p_days        => v_permonth_days);
    END LOOP;
    COMMIT;
  END hr_prorate_leave;

  PROCEDURE hr_update_leave_attendance(p_emp_id          IN VARCHAR2,
                                       p_leave_type      IN VARCHAR2,
                                       p_leave_from_date IN DATE,
                                       p_no_days         IN NUMBER,
                                       p_org_id          IN VARCHAR2,
                                       p_dept_id         IN VARCHAR2,
                                       p_fin_year        IN VARCHAR2,
                                       p_leave_reason    IN VARCHAR2,
                                       p_leave_req_id    IN VARCHAR2,
                                       p_user            IN VARCHAR2) IS

    v_from_dt  DATE;
    v_to_dt    DATE;
    v_req_id   HR_LEAVE_APPLICATIONS.LEAVE_REQ_ID%TYPE;
    v_leave_id HR_LEAVE_DEFINITIONS.LEAVE_ID%TYPE;
  BEGIN
    BEGIN
      SELECT LEAVE_REQ_ID,
             HLA.LEAVE_ID,
             hla.leave_date_from,
             hla.leave_date_to
        INTO v_req_id, v_leave_id, v_from_dt, v_to_dt
        FROM HR_LEAVE_APPLICATIONS HLA
       WHERE ORG_ID = p_org_id
         AND FISCAL_YEAR = p_fin_year
         AND DEPT_ID = p_dept_id
         AND STAFF_ID = p_emp_id
         AND hla.leave_req_id = p_leave_req_id
         AND UPPER(LTRIM(RTRIM(APP_STATUS))) = 'APPROVED';
    EXCEPTION
      WHEN OTHERS THEN
        v_req_id := NULL;
    END;

    WHILE v_from_dt <= v_to_dt LOOP
      dbms_output.put_line('Date ' || v_from_dt);
      BEGIN
        INSERT INTO HR_STAFF_ATTENDANCE
          (STATT_ID,
           ORG_ID,
           FISCAL_YEAR,
           STAFF_ID,
           ATTENDANCE_DATE,
           ATTENDANCE_TYPE,
           LEAVE_REQ_ID,
           REASON,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           DEPT_ID)
        VALUES
          (ssm.get_next_sequence('HR_022', 'EN'),
           p_org_id,
           p_fin_year,
           p_emp_id,
           v_from_dt,
           v_leave_id,
           v_req_id,
           p_leave_reason,
           '1',
           '1',
           user,
           sysdate,
           p_dept_id);
      EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('Error post ' || sqlerrm);
      END;
      v_from_dt := v_from_dt + 1;
    END LOOP;
    COMMIT;
  END hr_update_leave_attendance;

  PROCEDURE proc_leave_provision(p_org_id   IN VARCHAR2,
                                 p_date     IN DATE,
                                 p_type     IN VARCHAR2 DEFAULT 'R') IS

    v_year                 NUMBER;
    v_cal_id               VARCHAR2(50);
    v_cal_id_curr_year     VARCHAR2(50);
    v_no_days              NUMBER := 0;
    v_from_dt              DATE := TO_DATE('01-JAN-' ||to_char(p_date, 'YY'),'DD-MON-YY');
    v_to_dt                DATE := TO_DATE('31-DEC-' ||to_char(p_date, 'YY'),'DD-MON-YY');
    v_per_day              NUMBER := 0;
    v_year_day             NUMBER := 0;
    v_prev_last_day        DATE;
    v_prev_year_leave_bal  NUMBER := 0;
    v_days_till_last_month NUMBER;
    v_eligible_days        NUMBER := 0;
  v_prev_eligible_days   NUMBER :=0;
  v_leave_this_month     NUMBER :=0;
    v_leave_so_far         NUMBER := 0;
  v_basic_amt            NUMBER :=0;
    v_encash               NUMBER := 0;
  v_leave_bal            NUMBER :=0;
  v_tot_provision_amt    NUMBER :=0;
  v_this_month_amt       NUMBER :=0;
  v_fixed_days           NUMBER :=0;
    v_month_first_day      DATE;
v_leave_id VARCHAR2(50);
v_Accured_leave_Balance number;
                                 
  BEGIN
    select sso.hr_earn_leave into v_leave_id from ssm_system_options sso;

for emp_dtl in (select he.*,hewd.emp_dept_id from hr_employees he,hr_emp_work_dtls hewd
   where he.emp_org_id=p_org_id
   and he.emp_id=hewd.emp_id
   and hewd.effective_from_dt = (select max(hewdd.effective_from_dt) from hr_emp_work_dtls hewdd where 
   he.emp_id=hewdd.emp_id)
   --and he.emp_id='EMP_ID-0000000068'
   ) loop
      
    dbms_output.put_line('p_date ' || p_date || ' - ' || ' p_leave_id ' ||v_leave_id || ' - ' || ' p_dept_id ' || emp_dtl.emp_dept_id ||' - ' || ' p_emp_id ' || emp_dtl.emp_id);
    v_year            := to_number(to_char(p_date, 'YYYY')) - 1;
    v_month_first_day := LAST_DAY(ADD_MONTHS(p_date, -1)) + 1;
    v_prev_last_day   := LAST_DAY(ADD_MONTHS(p_date, -1));
   -- dbms_output.put_line('From date ' || v_from_dt || ' To date ' ||v_to_dt || '  Previous month last day ' ||v_prev_last_day || ' First day of the month ' ||v_month_first_day);
    dbms_output.put_line('Previous year of ' || to_char(p_date) || ' is ' ||v_year);
    v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');

    BEGIN
      SELECT --B.CAL_DTL_ID
       B.CAL_EFF_START_DT, B.CALL_EFF_END_DT
        INTO v_from_dt, v_to_dt
        FROM GL_ACCT_CALENDAR_HDR      A,
             GL_ACCT_CALENDAR_DTL      B,
             GL_COMP_ACCT_CALENDAR_DTL C
       WHERE B.CAL_ID = A.CAL_ID
         AND C.CAL_ID = A.CAL_ID
         AND C.CAL_DTL_ID = B.CAL_DTL_ID
         AND C.CAL_ACCT_YEAR = B.CAL_ACCT_YEAR
         AND C.COMP_ID = p_org_id
         AND p_date BETWEEN B.CAL_EFF_START_DT AND B.CALL_EFF_END_DT
         AND A.ENABLED_FLAG = '1'
         AND A.WORKFLOW_COMPLETION_STATUS = '1'
         AND B.ENABLED_FLAG = '1'
         AND B.WORKFLOW_COMPLETION_STATUS = '1'
         AND C.ENABLED_FLAG = '1'
         AND C.WORKFLOW_cOMPLETION_STATUS = '1';
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('ERR55555 ' || SQLERRM);
    END;

  IF v_fixed_days=0 THEN
    v_fixed_days := pkg_payroll.get_days(p_from_dt => v_from_dt,p_to_dt => v_to_dt);
  END IF;
    --start
    v_basic_amt:=PKG_PAYROLL.get_annual_amt(p_org_id,emp_dtl.emp_id)/v_fixed_days;

    DBMS_OUTPUT.PUT_LINE('dates ' || v_from_dt || ' - ' || v_to_dt);
    --end
    v_cal_id_curr_year := pkg_payroll.get_fiscal_year(p_org_id, p_date);
    dbms_output.put_line(' v_cal_id_curr_year ' || v_cal_id_curr_year);

    dbms_output.put_line(' v_leave_so_far ' || v_leave_so_far);

    BEGIN
      SELECT NO_OF_DAYS
        INTO v_no_days
        FROM HR_LEAVE_STAFF_DEFINTIONS HLSD
       WHERE HLSD.LEAVE_ID = v_leave_id
         AND HLSD.ORG_ID = p_org_id
         AND HLSD.STAFF_ID = emp_dtl.emp_id
         AND HLSD.FISCAL_YEAR = v_cal_id_curr_year;
    EXCEPTION
      WHEN OTHERS THEN
        v_no_days := 0;
    END;
    
    v_eligible_days := (v_no_days/365)*((p_date-trunc(p_date,'MM'))+1);
    v_this_month_amt:=(v_eligible_days*v_basic_amt);

  --Leave provision Ledger logic starts here
  BEGIN
   SELECT X.LEAVE_BAL,
          X.LEAVE_ELIGIBLE_DAYS,
             X.LEAVE_TOTAL_PROVISION_AMT
        INTO  v_leave_bal,v_prev_eligible_days,v_tot_provision_amt
        FROM HR_LEAVE_TXN_LEDGER X
       WHERE X.LEAVE_ORG_ID = p_org_id
         AND X.LEAVE_DEPT_ID = emp_dtl.emp_dept_id
         AND X.LEAVE_EMP_ID = emp_dtl.emp_id
         AND X.LEAVE_LDR_ID =
             (SELECT MAX(LEAVE_LDR_ID)
                FROM HR_LEAVE_TXN_LEDGER
               WHERE LEAVE_ORG_ID = p_org_id
                 AND LEAVE_DEPT_ID = emp_dtl.emp_dept_id
                 AND LEAVE_EMP_ID = emp_dtl.emp_id);
  EXCEPTION
      WHEN OTHERS THEN
       dbms_output.put_line('Err# '||sqlerrm);
         v_leave_bal:=0;
         v_tot_provision_amt :=0;
         v_prev_eligible_days:=0;
    END;
       dbms_output.put_line('v_tot_provision_amt  '||v_tot_provision_amt);
--     v_leave_this_month:=v_eligible_days-v_prev_eligible_days;
--     v_this_month_amt:=v_this_month_amt-v_tot_provision_amt;
       
     v_tot_provision_amt:=v_tot_provision_amt+v_this_month_amt;
  BEGIN
    
  select Accured_leave_Balance into v_Accured_leave_Balance FROM HR_LEAVE_STAFF_DEFINTIONS HLSD
       WHERE HLSD.LEAVE_ID = v_leave_id
         AND HLSD.ORG_ID = p_org_id
         AND HLSD.STAFF_ID = emp_dtl.emp_id
         AND HLSD.FISCAL_YEAR = v_cal_id_curr_year;

  v_leave_bal := nvl(v_Accured_leave_Balance,0)+nvl(v_eligible_days,0);
  
     INSERT INTO HR_LEAVE_TXN_LEDGER(LEAVE_LDR_ID,
                     LEAVE_ORG_ID,
                     LEAVE_DEPT_ID,
                     LEAVE_EMP_ID,
                     LEAVE_TXN_TYPE,
                     LEAVE_OPEN_BAL,
                     LEAVE_AVAIL,
                     LEAVE_BAL,
                     LEAVE_ELIGIBLE_DAYS,
                     LEAVE_ELE_FOR_MONTH,
                     LEAVE_TOTAL_PROVISION_AMT,
                     LEAVE_PROVISION_AMT,
                     LEAVE_TXN_DATE,
                     LEAVE_REMARKS,
                     ENABLED_FLAG,
                     WORKFLOW_COMPLETION_STATUS,
                     CREATED_BY,
                     CREATED_DATE)
                VALUES(ssm.get_next_sequence('HR_119', 'EN'),
                       p_org_id,
                     emp_dtl.emp_dept_id,
                     emp_dtl.emp_id,
                     'ACCRUVAL',
                     round(v_Accured_leave_Balance,3),
                     NVL(v_leave_so_far,0),
                     round(v_leave_bal,3),
                     round(v_eligible_days,3), --Should be substracted v_leave_bal
                     round(v_leave_this_month,3),
                     round(v_tot_provision_amt,3), --Should be added with this month amount
                     round(v_this_month_amt,3), --here comes the amount emp eligible for that month
                     p_date,
                     'Leave provision Amount and Days for '||to_char(p_date,'DD-MON-YY'),
                     '1',
                     '1',
                     'System',
                     SYSDATE);

                     update hr_leave_staff_defintions set Accured_leave_Balance=round(v_leave_bal,3)         
       WHERE LEAVE_ID = v_leave_id
         AND ORG_ID = p_org_id
         AND STAFF_ID = emp_dtl.emp_id
         AND FISCAL_YEAR = v_cal_id_curr_year;
                     
  COMMIT;
                     
  EXCEPTION
      WHEN OTHERS THEN
      dbms_output.put_line('Error insert1222 '||sqlerrm);
  END;
   END LOOP;                  

  COMMIT;

  --Leave provision Ledger logic Ends here
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END proc_leave_provision;
END HR_PKG;
/
