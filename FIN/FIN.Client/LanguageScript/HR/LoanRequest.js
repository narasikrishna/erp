﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblRequestNumber");
        x.innerHTML = result.RequestNumber_p

        x = document.getElementById("lblRequestDate");
        x.innerHTML = result.RequestDate_p

        x = document.getElementById("lblEmployeeName");
        x.innerHTML = result.EmployeeName_p

        x = document.getElementById("lblDepartment");
        x.innerHTML = result.Department_p

        x = document.getElementById("lblType");
        x.innerHTML = result.Type_P

        x = document.getElementById("lblDesignation");
        x.innerHTML = result.Designation_p

        x = document.getElementById("lblAmount");
        x.innerHTML = result.Amount_p

        x = document.getElementById("lblComments");
        x.innerHTML = result.Comments_p

        x = document.getElementById("lblStatus");
        x.innerHTML = result.Status_P

        x = document.getElementById("lblNoOfInstallments");
        x.innerHTML = result.NoOfInstallments_p

        x = document.getElementById("lblInstallmentAmount");
        x.innerHTML = result.InstallmentAmount_p

        x = document.getElementById("lblLoanStartDate");
        x.innerHTML = result.LoanStartDate_p

        x = document.getElementById("lblPaymentReleaseDate");
        x.innerHTML = result.PaymentReleaseDate_P

    });
}