﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblKRANumber");
        x.innerHTML = result.KRANumber_P

        x = document.getElementById("lblAppraisalCode");
        x.innerHTML = result.AppraisalCode_p

        x = document.getElementById("lblDepartment");
        x.innerHTML = result.Department_p

        x = document.getElementById("lblJob");
        x.innerHTML = result.Job_p

        x = document.getElementById("lblStatus");
        x.innerHTML = result.Status_p

        x = document.getElementById("lblVersion");
        x.innerHTML = result.Version_p

    });
}