﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblFinancialYear");
        x.innerHTML = result.FinancialYear_p

        x = document.getElementById("lblDepartment");
        x.innerHTML = result.Department_p

        x = document.getElementById("lblApplicationDate");
        x.innerHTML = result.ApplicationDate_p

        x = document.getElementById("lblStaffName");
        x.innerHTML = result.StaffName_p

        x = document.getElementById("lblLeaveType");
        x.innerHTML = result.LeaveType_p

        x = document.getElementById("lblLeaveFrom");
        x.innerHTML = result.LeaveFrom_P

        x = document.getElementById("lblLeaveTo");
        x.innerHTML = result.LeaveTo_P

        x = document.getElementById("lblNoOfDays");
        x.innerHTML = result.NoOfDays_P

        x = document.getElementById("lblReason");
        x.innerHTML = result.Reason_P

        x = document.getElementById("lblApplicationStatus");
        x.innerHTML = result.ApplicationStatus_P

        x = document.getElementById("lblLeaveAddress");
        x.innerHTML = result.LeaveAddress_P

        x = document.getElementById("lblContactNumber");
        x.innerHTML = result.ContactNumber_P

        x = document.getElementById("lblHalfDayFrom");
        x.innerHTML = result.HalfDayFrom_P

        x = document.getElementById("lblHalfDayTo");
        x.innerHTML = result.HalfDayTo_P

    });
}