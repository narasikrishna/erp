﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblAppraisalCode");
        x.innerHTML = result.AppraisalCode_p

        x = document.getElementById("lblPeriod");
        x.innerHTML = result.Period_p

        x = document.getElementById("lblDescription");
        x.innerHTML = result.Description_p

        x = document.getElementById("lblFromDate");
        x.innerHTML = result.FromDate_p

        x = document.getElementById("lblToDate");
        x.innerHTML = result.ToDate_p

        x = document.getElementById("lblDesignation");
        x.innerHTML = result.Designation_p

        x = document.getElementById("lblAmount");
        x.innerHTML = result.Amount_p
    });
}