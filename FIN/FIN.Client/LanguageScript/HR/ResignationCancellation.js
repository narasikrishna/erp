﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblCancellationNumber");
        x.innerHTML = result.CancellationNumber_P

        x = document.getElementById("lblCancellationDate");
        x.innerHTML = result.CancellationDate_P

        x = document.getElementById("lblRequestNumber");
        x.innerHTML = result.RequestNumber_p

        x = document.getElementById("lblRequestDate");
        x.innerHTML = result.RequestDate_p

        x = document.getElementById("lblDepartmentName");
        x.innerHTML = result.DepartmentName_p

        x = document.getElementById("lblEmployeeName");
        x.innerHTML = result.EmployeeName_p

        x = document.getElementById("lblReason");
        x.innerHTML = result.Reason_p

        x = document.getElementById("lblRemarks");
        x.innerHTML = result.Remarks_p

    });
}