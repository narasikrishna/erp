﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/GL_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblStructureName");
        x.innerHTML = result.StructureName_P

        x = document.getElementById("lblAccountCode");
        x.innerHTML = result.AccountCode_P

        x = document.getElementById("lblGroupName");
        x.innerHTML = result.GroupName_P

        x = document.getElementById("lblAccountCodeDescription");
        x.innerHTML = result.AccountCodeDescription_P

        x = document.getElementById("lblAccountType");
        x.innerHTML = result.AccountType_P

        x = document.getElementById("lblActive");
        x.innerHTML = result.Active_P

        x = document.getElementById("lblControlAccount");
        x.innerHTML = result.ControlAccount_P

        x = document.getElementById("lblControlAccountCategory");
        x.innerHTML = result.ControlAccountCategory_P

        x = document.getElementById("lblEffectiveDate");
        x.innerHTML = result.EffectiveDate_P

        x = document.getElementById("lblEndDate");
        x.innerHTML = result.EndDate_P

       
    });
}