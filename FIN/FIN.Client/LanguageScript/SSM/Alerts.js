﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/SSM_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblAlterCode");
        x.innerHTML = result.AlertCode_P

        x = document.getElementById("lblAlertDescription");
        x.innerHTML = result.AlertDescription_P


        x = document.getElementById("lblAlertMessage");
        x.innerHTML = result.AlertMessage_P


        x = document.getElementById("lblAlertType");
        x.innerHTML = result.AlertType_P


        x = document.getElementById("lblLanguageCode");
        x.innerHTML = result.LanguageCode_P


        x = document.getElementById("lblLanguageDescription");
        x.innerHTML = result.LanguageDescription_P


        x = document.getElementById("lblScreenCode");
        x.innerHTML = result.ScreenCode_P


        x = document.getElementById("lblScreenDescription");
        x.innerHTML = result.ScreenDescription_P


        x = document.getElementById("lblAlertLevelCode");
        x.innerHTML = result.AlertLevelCode_P


        x = document.getElementById("lblAlertLevelDescription");
        x.innerHTML = result.AlertLevelDescription_P


        x = document.getElementById("lblRoleCode");
        x.innerHTML = result.RoleCode_P


        x = document.getElementById("lblRoleDescription");
        x.innerHTML = result.RoleDescription_P


        x = document.getElementById("lblFormOpenModecode");
        x.innerHTML = result.FormOpenModecode_P


        x = document.getElementById("lblActive1");
        x.innerHTML = result.Active1_P



        x = document.getElementById("lblActive2");
        x.innerHTML = result.Active2_P


        x = document.getElementById("lblQuery");
        x.innerHTML = result.Query_P


        x = document.getElementById("lblProcessCode");
        x.innerHTML = result.ProcessCode_P


        x = document.getElementById("lblURL");
        x.innerHTML = result.URL_P


        x = document.getElementById("lblAlertTableName");
        x.innerHTML = result.AlertTableName_P


        x = document.getElementById("lblAlertColumnName");
        x.innerHTML = result.AlertColumnName_P


        x = document.getElementById("lblPreviousFieldName");
        x.innerHTML = result.PreviousFieldName_P


        x = document.getElementById("lblFromFormCode");
        x.innerHTML = result.FromFormCode_P


    });
}