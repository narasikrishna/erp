﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.BLL;
using FIN.DAL;
using VMVServices.Web;
namespace FIN.Client
{
    public partial class LoginNew : PageBase
    {

        SSM_USERS sSM_USERS = new SSM_USERS();

        protected void Page_Load(object sender, EventArgs e)
        {

        }



        protected void btnlogin_Click1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                
                   

                    sSM_USERS = null;
                    using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                    {
                        sSM_USERS = userCtx.Find(r =>
                            (r.USER_CODE.ToUpper() == txtUserID.Text.ToUpper()) &&
                            (r.USER_PASSWORD.ToUpper() == txtPassword.Text.ToUpper())
                            ).SingleOrDefault();
                    }



                   
                    //lblError.Visible = false;
                    if (sSM_USERS != null)
                    {

                        this.LoggedOnUserID = int.Parse(sSM_USERS.PK_ID.ToString());
                        this.LoggedUserName = sSM_USERS.USER_CODE;
                        Session[FINSessionConstants.PassWord] = sSM_USERS.USER_PASSWORD;
                        //Session[FINSessionConstants.UserGroupID] = sSM_USERS.GROUP_ID;
                        Session[FINSessionConstants.UserName] = sSM_USERS.USER_CODE;


                        VMVServices.Web.Utils.Multilanguage = false;
                        // VMVServices.Web.Utils.GroupID = long.Parse(sSM_USERS.GROUP_ID.ToString());


                        //   Response.Redirect("~/MainPage/" + System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
                        if (sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN == FINAppConstants.N)
                        {
                           // ModalPopupExtenderCP.Hide();
                            Response.Redirect("~/MainPage/MainPage.aspx");
                        }
                        else
                        {
                            ModalPopupExtenderCP.Show();
                            
                        }
                    }
                

            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("LoginError");
                ErrorCollection.Add("LoginError", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
            {
                sSM_USERS = userCtx.Find(r =>
                    (r.USER_CODE == txtUserID.Text)
                    ).SingleOrDefault();
            }

            if (sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN == FINAppConstants.Y)
            {
               
                if (FINColumnConstants.USER_CODE.ToString() != null)
                {
                    using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                    {
                        sSM_USERS = userCtx.Find(r =>
                            (r.USER_CODE == txtUserID.Text)
                            ).SingleOrDefault();
                    }


                    sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.N;
                    sSM_USERS.USER_PASSWORD = txtconfirmpw.Text;
                    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);
                    Response.Redirect("~/MainPage/MainPage.aspx");
                }
            }

        }

    }
}