﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTEmpDetwithFilterParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTEmpDetwithFilterParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="divMainContainer">
        <div class="divRowContainer">
            <asp:RadioButtonList ID="rbl_FilterCont" runat="server" CssClass="lblBox" 
                CellPadding="10" CellSpacing="10" RepeatColumns="3" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True"  Value="Department">Department</asp:ListItem>
                <asp:ListItem Value="Designation">Designation</asp:ListItem>
                <asp:ListItem Value="Category">Category</asp:ListItem>
                <asp:ListItem Value="Job">Job</asp:ListItem>
                <asp:ListItem Value="Position" >Position</asp:ListItem>
                <asp:ListItem Value="Grade">Grade</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="divClear_10">
        
        </div>
        <div class="divRowContainer" align="right">
          <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/View.png" OnClick="btnView_Click" 
                                    Style="border: 0px"  />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>