﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTEmpSalCertificateParm.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTEmpSalCertificateParm" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div4">
                Department Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlDepartmentName" runat="server" TabIndex="1" CssClass="ddlStype"
                    AutoPostBack="true"  OnSelectedIndexChanged="ddlDepartmentName_SelectedIndexChanged">
                </asp:DropDownList>
            </div> 
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 300px">
                <asp:DropDownList ID="ddlEmpName" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div3">
                Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:RadioButtonList ID="rbSalCer" runat="server" CssClass="lblBox LNOrient" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="BASIC">Basic</asp:ListItem>
                    <asp:ListItem Value="ALLOWANCE">Allowance</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
