﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.Reports.HR
{
    public partial class RPTPayslipParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayslipReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayslipReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Session["photoUrl"] = null;

                if (ddlEmployeeName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.EMP_ID, ddlEmployeeName.SelectedItem.Value);

                    string photoid = "";
                    string photoUrl = string.Empty;

                    DataTable dtphoto = new DataTable();

                    photoid = ddlEmployeeName.SelectedItem.Value.ToString();
                    dtphoto = DBMethod.ExecuteQuery(FIN.DAL.HR.Applicant_DAL.GetPhotoFileNameBasedEmpId(photoid.ToString())).Tables[0];
                   
                    if (dtphoto.Rows.Count > 0)
                    {
                        photoUrl = "~/UploadFile/PHOTO/" + ddlEmployeeName.SelectedItem.Value + "." + dtphoto.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "");
                    }
                 //   htHeadingParameters.Add("EmpFoto", photoUrl);
                    Session["photoUrl"] = photoUrl;
                }

                if (ddlPayrollPeriod.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.PAY_PERIOD_ID, ddlPayrollPeriod.SelectedItem.Value);
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                //htHeadingParameters.Add("OrgLogo", RPCServices.Web.Utils.OrgLogo.ToString());
                //ReportData = FIN.BLL.HR.Payslip_BLL.GetReportData();
                ReportData = FIN.BLL.HR.Payslip_BLL.GetEmployeeDtlsReportData();

                DataSet ds = FIN.BLL.HR.Payslip_BLL.GetEarningReportData();
                SubReportData = new DataSet();
              
                DataTable dt_2 = ds.Tables[0].Copy();
                dt_2.TableName = "Allowance";
                SubReportData.Tables.Add(dt_2);


                DataTable dt_3 = ds.Tables[0].Copy();
                dt_3.TableName = "Deducation";
                SubReportData.Tables.Add(dt_3);


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayslipReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            //ddlEmployeeName.Items.RemoveAt(0);
            //ddlEmployeeName.Items.Insert(0, new ListItem("All", ""));
            PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod);
            //ddlPayrollPeriod.Items.RemoveAt(0);
            //ddlPayrollPeriod.Items.Insert(0, new ListItem("All", ""));
        }

        //public void GetPayslipReportData()
        //{
        //    dtEmployeeDtls = DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EmpDtlsReportData()).Tables[0];
        //    dtEarningDeduc = DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EarningsReportData()).Tables[0];

        //    //if (dtEmployeeDtls != null)
        //    //{
        //    //    if (dtEmployeeDtls.Rows.Count > 0)
        //    //    {
        //    //        txtEmployeeName.Text = dtEmployeeDtls.Rows[0][0].ToString();
        //    //        txtEmployeeName.Enabled = false;
        //    //    }
        //    //}
        //}


    }
}