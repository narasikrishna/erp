﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTMobileBillingParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTMobileBillingParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                    Payroll Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 250px">
                    <asp:DropDownList ID="ddlPayrollPeriod" runat="server" TabIndex="1" CssClass="validate[required] EntryFont RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div6">
                    Department Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 250px">
                    <asp:DropDownList ID="ddlDepartment" TabIndex="2" runat="server" CssClass="ddlStype"
                      OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <%-- <div class="divRowContainer" style="display:none">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                    Designation
                </div>
                <div class="divtxtBox LNOrient" style="  width: 250px">
                    <asp:DropDownList ID="ddlDesig" runat="server" TabIndex="3" AutoPostBack="true" CssClass=" ddlStype"
                        OnSelectedIndexChanged="ddlDesig_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>--%>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                    Employee Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 250px">
                    <asp:DropDownList ID="ddlEmployeeName" TabIndex="4" runat="server" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
