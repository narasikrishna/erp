﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR_Reports
{
    public partial class RPTSocialInsuranceInstallTrailParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SocialInsuranceInstallmentTrailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SocialInsuranceInstallmentTrailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlPayrollPeriod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("PERIOD_ID", ddlPayrollPeriod.SelectedItem.Value);
                    htFilterParameter.Add("PAY_PERIOD_DESC", ddlPayrollPeriod.SelectedItem.Text.ToString());
                }
                else
                {
                    ErrorCollection.Add("Payroll", "Payroll period Can't be empty");
                    return;
                }

                if (ddlDepartment.Text != string.Empty)
                {
                    htFilterParameter.Add("dept_id", ddlDepartment.SelectedItem.Value);
                }
                if (ddlDesig.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("DEPT_DESIG_ID", ddlDesig.SelectedValue);
                }
                if (ddlEmployee.Text != string.Empty)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmployee.SelectedItem.Value);
                }
             
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.HR.Payslip_BLL.GetSocialInsuranceInstallmentTrailReport();
                SubReportData = ReportData;
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SocialInsuranceInstallmentTrailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod);
            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDepartment);
            FIN.BLL.HR.Department_BLL.GetDesignation(ref ddlDesig, false);
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee, false);
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Department_BLL.GetDesignationName_R(ref ddlDesig, ddlDepartment.SelectedValue);
                FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept_R(ref ddlEmployee, ddlDepartment.SelectedValue);
            }
            else
            {
                FIN.BLL.HR.Department_BLL.GetDesignation(ref ddlDesig, false);
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee, false);
            }
        }

        protected void ddlDesig_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesig.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmplNameNDeptDesig_R(ref ddlEmployee, ddlDepartment.SelectedValue, ddlDesig.SelectedValue);
            }
            else if (ddlDepartment.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept_R(ref ddlEmployee, ddlDepartment.SelectedValue);
            }
            else
            {
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee, false);
            }
        }


    }
}