﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTEmpOvertimeReportParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTEmpOvertimeReportParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                    Group Code
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlGroupcode" runat="server" TabIndex="1" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                    Department Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlDepartmentName" runat="server" TabIndex="2" CssClass="ddlStype"
                        AutoPostBack="true" Width="150px" 
                        onselectedindexchanged="ddlDepartmentName_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblPeriodID">
                    Employee Name
                </div>
                <div class="divtxtBox  LNOrient" style="width: 250px">
                    <asp:DropDownList ID="ddlEmployeeName" runat="server" TabIndex="3" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
                   From Date
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="4" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
             <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox  LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
