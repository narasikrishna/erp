﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.HR;
using System.Web.UI.WebControls;

namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpApprovedLeave : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpPermission", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillCombBox();
                FillStartDate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_EmpPermission", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void FillCombBox()
        {
            FIN.BLL.HR.LeaveDefinition_BLL.GetLeave(ref ddlLeaveType);
           
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmpName);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
            FIN.BLL.HR.DepartmentDetails_BLL.fn_GetDesigName(ref ddlDesig);
          
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }

        protected void ddlDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept(ref ddlEmpName, ddlDepartment.SelectedValue);
            }
            else
            {
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmpName);
            }
        }

        protected void ddlDesig_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesig.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmplNameNDeptDesig(ref ddlEmpName, ddlDepartment.SelectedValue, ddlDesig.SelectedValue);
            }
            else
            {
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmpName);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                if (ddlLeaveType.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("LeaveType", ddlLeaveType.SelectedValue);
                    htFilterParameter.Add("LeaveTypeDesc", ddlLeaveType.SelectedItem.Text.ToString());
                }
                if (ddlDepartment.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("DEPT_ID",ddlDepartment.SelectedValue.ToString());
                }
                if (ddlDesig.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("DESIG_ID", ddlDesig.SelectedValue.ToString());
                }
                if (ddlEmpName.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmpName.SelectedValue.ToString());
                }
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.StaffLeaveDetails_DAL.getAppEmpLeaveDtls());

                //htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpPermissionReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        

        //protected void btnSave_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        doc_tablecount = 0;

        //        string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
        //        string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

        //        string HRReportEN = HRReportPath.ToString() + "LeaveBalance_EN.docx";
        //        string HRReportAR = HRReportPath.ToString() + "LeaveBalance_AR.docx";

        //        string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
        //        string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + "HR_LeaveBalanceReport" + formatted + ".docx";

        //        switch (Session[FINSessionConstants.Sel_Lng].ToString())
        //        {
        //            case "AR":
        //                System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //            default:
        //                System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //        }

        //        using (DocX document = DocX.Load(saveHRTemplateAs))
        //        {
        //            document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));

        //            Novacode.Table tbl_LP = document.Tables[doc_tablecount];
        //            DataSet dsData = new DataSet();
        //            dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeaveLedgerData(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
        //            for (int iLoop = 0; iLoop < dsData.Tables[0].Rows.Count; iLoop++)
        //            {
        //                tbl_LP.InsertRow();
        //                if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //                {

        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                }
        //                else if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //                {
        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());                          
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                }
        //            }

        //            document.Tables[doc_tablecount] = tbl_LP;

        //            // Save all changes made to this document.
        //            document.Save();

        //            Session["FileName"] = Path.GetFileName(saveHRTemplateAs);
        //            Session["OnlyFileName"] = Path.GetFileNameWithoutExtension(saveHRTemplateAs);

        //            string linkToDownload = "../TmpWordDocs/" + Session["FileName"];

        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("SaveHREmpListEOS", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }
        //}
    }
}