﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.Reports.AP
{
    public partial class RPTAgingAnalysisReportParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;

                if (txt30.Text.ToString().Length == 0 || txt60.Text.ToString().Length == 0 || txt90.Text.ToString().Length == 0 || txtBucket4.Text.ToString().Length == 0 || txtBucket5.Text.ToString().Length == 0)
                {
                    ErrorCollection.Add("Pelase Enter All the Bucket", "Please Enter All the Bucket Value");
                    return;
                }
                if (txtFromDate.Text.ToString().Length == 0)
                {
                    ErrorCollection.Add("datemsg", "Date cannot be empty");
                    return;
                }


                if (CommonUtils.ConvertStringToInt(txt30.Text) > CommonUtils.ConvertStringToInt(txt60.Text))
                {
                    ErrorCollection.Add("datemsg1st", "Aging Bucket2 should be greater than Aging Bucket1");
                    return;
                }

                if (CommonUtils.ConvertStringToInt(txt60.Text) > CommonUtils.ConvertStringToInt(txt90.Text))
                {
                    ErrorCollection.Add("datemsg1st", "Aging Bucket3 should be greater than Aging Bucket2");
                    return;
                }


                if (CommonUtils.ConvertStringToInt(txt90.Text) > CommonUtils.ConvertStringToInt(txtBucket4.Text))
                {
                    ErrorCollection.Add("datemsg1st", "Aging Bucket4 should be greater than Aging Bucket3");
                    return;
                }

                if (CommonUtils.ConvertStringToInt(txtBucket4.Text) > CommonUtils.ConvertStringToInt(txtBucket5.Text))
                {
                    ErrorCollection.Add("datemsg1st", "Aging Bucket5 should be greater than Aging Bucket4");
                    return;
                }

                htFilterParameter.Add("firstCase", txt30.Text);
                htFilterParameter.Add("secondCase", txt60.Text);
                htFilterParameter.Add("thirdCase", txt90.Text);
                htFilterParameter.Add("fourthCase", txtBucket4.Text);
                htFilterParameter.Add("fifthCase", txtBucket5.Text);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                htFilterParameter.Add("From_Date", txtFromDate.Text);

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            // Supplier_BLL.GetSupplier(ref ddlSupplierName);
            //FIN.BLL.AP.APAginAnalysis_BLL.GetGroupName(ref ddlGlobalSegment);

        }
    }
}