﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTPaymentRegisterParam.aspx.cs" Inherits="FIN.Client.AP_Reports.RPTPaymentRegisterParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="div1">
        <div class="divFormcontainer" style="width: 800px" id="div2">
            <div class="divRowContainer">
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="width: 200px" id="lblRequisitionType">
                        Supplier Name
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 350px">
                        <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1" CssClass="ddlStype"
                            AutoPostBack="true" Width="350px" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox  LNOrient" style="width: 200px" id="Div7">
                        Bank Name
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 350px">
                        <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="2" CssClass="ddlStype"
                            OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged" AutoPostBack="true"
                            Width="350px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="width: 200px" id="Div13">
                        Bank Branch Name
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 350px">
                        <asp:DropDownList ID="ddlBankBranch" runat="server" TabIndex="3" CssClass="ddlStype"
                            Width="350px" AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style="width: 200px" id="Div11">
                    Bank Account Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 350px">
                    <asp:DropDownList ID="ddlBankAccountNum" runat="server" TabIndex="4" CssClass="ddlStype"
                        Width="350px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 200px" id="Div5">
                    Payment Number
                </div>
                <div class="divtxtBox LNOrient" style=" width: 350px">
                    <asp:DropDownList ID="ddlPaymentNumber" runat="server" TabIndex="5" CssClass="ddlStype"
                        Width="350px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div6">
                    Invoice Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 350px">
                    <asp:DropDownList ID="ddlInvoiceNum" runat="server" TabIndex="6" CssClass="ddlStype"
                        Width="350px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div8">
                    Currency
                </div>
                <div class="divtxtBox LNOrient" style="width: 350px">
                    <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="7" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div12">
                    Payment Type
                </div>
                <div class="divtxtBox LNOrient" style=" width: 350px">
                    <asp:DropDownList ID="ddlPaymentType" runat="server" TabIndex="8" CssClass="ddlStype"
                        Width="150px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                    From Cheque Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtFromCheqNum" runat="server" TabIndex="9" CssClass="txtBox_N"
                        MaxLength="13"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="txtFromAmount0_FilteredTextBoxExtender" runat="server"
                        FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtFromCheqNum" />
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="width: 200px" id="Div10">
                        To Cheque Number
                    </div>
                    <div class="divtxtBox  LNOrient" style="width: 150px">
                        <asp:TextBox ID="txtToCheqNum" runat="server" TabIndex="10" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="txtFromAmount1_FilteredTextBoxExtender" runat="server"
                            FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtToCheqNum" />
                    </div>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style="width: 200px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="11" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="12" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 200px" id="Div4">
                    From Payment Amount
                </div>
                <div class="divtxtBox  LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="13" CssClass="txtBox_N"
                        MaxLength="13"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".," TargetControlID="txtFromAmount" />
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style=" width: 200px" >
                        To Payment Amount
                    </div>
                    <div class="divtxtBox LNOrient" style=" width: 150px">
                        <asp:TextBox ID="txtToAmount" runat="server" TabIndex="14" CssClass=" txtBox_N" MaxLength="13"></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                            ValidChars=".," TargetControlID="txtToAmount" />
                    </div>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="Div14">
                    Payment Status
                </div>
                <div class="divtxtBox LNOrient" style="width: 550px" align="center">
                    <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                        <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="C">Cancelled &nbsp; &nbsp; &nbsp; </asp:ListItem>
                         <asp:ListItem Value="IU">All &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 570px" id="divMainContainer">
                <div class="divRowContainer LNOrient divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td>
                                <%-- <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                                    <img src="../Images/MainPage/settings-icon_B.png" alt="Show Report" width="15px"
                                        height="15px" title="Show Report" />--%>
                                <%-- </a>--%>
                                <%-- <asp:Button ID="btnSave1" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" Visible="false" />--%>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                            <%--<td>
                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
