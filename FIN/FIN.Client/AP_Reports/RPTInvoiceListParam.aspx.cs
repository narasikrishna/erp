﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AP_Reports
{
    public partial class RPTInvoiceListParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbUNPosted.Items[0].Text = (Prop_File_Data["Posted_P"]);
                    rbUNPosted.Items[0].Selected = true;
                    rbUNPosted.Items[1].Text = (Prop_File_Data["Unposted_P"]);
                    rbUNPosted.Items[2].Text = (Prop_File_Data["Cancelled_P"]);
                    rbUNPosted.Items[3].Text = (Prop_File_Data["All_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {

                    AmountParam();

                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                    if (ddlSupplierName.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SUPPLIERNAMES", ddlSupplierName.SelectedItem.Text.ToString());
                        htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue.ToString());
                    }
                    if (ddlInvoiceType.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("InvType", ddlInvoiceType.SelectedItem.Text.ToString());
                        htFilterParameter.Add("INV_TYPE", ddlInvoiceType.SelectedValue.ToString());
                    }

                    if (ddlFromInvoiceNumber.SelectedValue.ToString().Length > 0)
                    {
                        htFilterParameter.Add("FromInvNumber", ddlFromInvoiceNumber.SelectedValue.ToString());
                        htFilterParameter.Add("From_InvNum", ddlFromInvoiceNumber.SelectedItem.Text.ToString());
                    }

                    if (ddlToInvoiceNumber.SelectedValue.ToString().Length > 0)
                    {
                        htFilterParameter.Add("ToInvNumber", ddlToInvoiceNumber.SelectedValue.ToString());
                        htFilterParameter.Add("To_InvNum", ddlToInvoiceNumber.SelectedItem.Text.ToString());
                    }

                   
                    htFilterParameter.Add("POSTEDTYPE", rbUNPosted.SelectedValue.ToString());
                    htFilterParameter.Add("POSTEDSTATUS", rbUNPosted.SelectedItem.Text);

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.AP.APAginAnalysis_BLL.getInvoiceRegisterReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                }
                else
                {
                    ErrorCollection.Add("datddddemsg", "From Date and To Date cannot be empty");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlFromInvoiceNumber, true);
            FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlToInvoiceNumber, true);
            Supplier_BLL.GetSupplierName(ref ddlSupplierName, true);
            
            Lookup_BLL.GetLookUpValues(ref ddlInvoiceType, "INV_TY");
            //FIN.BLL.AR.SuplrStmtAccnt_BLL.GetSupplierName(ref ddlSupplierName);

        }
        private void AmountParam()
        {
            try
            {
                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtInvoiceFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount",txtInvoiceFromAmount.Text);
                    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtInvoiceToAmount.Text);
                        htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceToAmount.Text));
                    }
                }

                //ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtInvoiceFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtInvoiceToAmount.Text));
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("amtErr", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}