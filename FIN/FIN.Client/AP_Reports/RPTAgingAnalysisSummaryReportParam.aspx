﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAgingAnalysisSummaryReportParam.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTAgingAnalysisSummaryReportParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 370px" id="div1">
        <%-- <div class="divRowContainer">
            
            <div class="lblBox" style="float: left; width: 200px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1"  CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            </div>--%>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblRequisitionType">
                Supplier Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1" CssClass="validate[required]  RequiredField ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Aging Bucket1
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txt30" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox_N" MaxLength="2"></asp:TextBox>
                <cc2:FilteredTextBoxExtender runat="server" TargetControlID="txt30" ID="fil30"  FilterType="Numbers">
                </cc2:FilteredTextBoxExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
                Aging Bucket2
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txt60" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox_N" MaxLength="2"></asp:TextBox>
                <cc2:FilteredTextBoxExtender runat="server" TargetControlID="txt60" ID="FilteredTextBoxExtender1"
                 FilterType="Numbers">
                </cc2:FilteredTextBoxExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div4">
                Aging Bucket3
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txt90" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox_N" MaxLength="2"></asp:TextBox>
                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Numbers"
                  TargetControlID="txt90" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                            <%-- <asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
