﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AP
{
    public partial class RPTInvoiceRegisterParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
          

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {
                    if (ddlGlobalSegment.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Value);
                    }
                    if (ddlGlobalSegment.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SEGMENT_name", ddlGlobalSegment.SelectedItem.Text);
                    }
                    //if (chkSupplier.SelectedValue != string.Empty)
                    //{
                    //    htFilterParameter.Add(FINColumnConstants.VENDOR_ID, chkSupplier.SelectedItem.Value);
                    //}

                    int selectedCount = 0;
                    string vendorId = string.Empty;

                    if (chkSupplier.Items.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < chkSupplier.Items.Count; iLoop++)
                        {
                            if (chkSupplier.Items[iLoop].Selected == true)
                            {
                                vendorId += "'" + chkSupplier.Items[iLoop].Value + "',";                            
                                selectedCount = selectedCount + 1;
                            }
                        }
                        if (vendorId != string.Empty)
                        {
                            int index = vendorId.LastIndexOf(',');
                            vendorId = vendorId.Remove(index, 1);
                        }

                    }
                    if (selectedCount == 0)
                    {
                        ErrorCollection.Remove("checked");
                        ErrorCollection.Add("checked", "Please select atleast one supplier");
                        return;
                    }
                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                    AmountParam();

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    ReportData = FIN.BLL.AP.InvoiceRegister_BLL.GetReportData(vendorId.Trim());

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("datddemsg", "From Date and To Date cannot be empty");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void AmountParam()
        {
            try
            {
                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("From_Amt", txtFromAmount.Text);
                    htFilterParameter.Add("From_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("To_Amt", txtToAmount.Text);
                        htFilterParameter.Add("To_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmount.Text));
                    }
                }

                //ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtToAmount.Text));
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("amtErr", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            DataTable dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplier()).Tables[0];

            chkSupplier.DataSource = dtDropDownData;
            chkSupplier.DataTextField = FINColumnConstants.VENDOR_NAME;
            chkSupplier.DataValueField = FINColumnConstants.VENDOR_ID;
            chkSupplier.DataBind();

            // Supplier_BLL.GetSupplier(ref ddlSupplierName);
            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            ddlGlobalSegment.SelectedIndex = 1;
        }
    }
}