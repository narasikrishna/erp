﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTDayBookMaterialTransactionParam.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTDayBookMaterialTransactionParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 120px" id="lblTaxRate">
                    Item
                </div>
                <div class="divtxtBox LNOrient" style="width: 450px">
                    <asp:DropDownList ID="ddlItem" runat="server" CssClass="ddlStype"
                        TabIndex="1">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 120px" id="Div8">
                   Item Quantity From
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtFromItemQuantity" runat="server" TabIndex="2" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtFromItemQuantity" />
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 130px" id="Div9">
                   Item Quantity To
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtToItemQuantity" runat="server" TabIndex="3" CssClass="EntryFont txtBox_N"
                        Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".-" TargetControlID="txtToItemQuantity" />
                </div>
            </div>
             <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="Div3">
                    Transaction Type
                </div>
                <div class="divtxtBox LNOrient" style=" width: 450px">
                    <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="ddlStype" AutoPostBack="true"
                        TabIndex="4" 
                        onselectedindexchanged="ddlTransactionType_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="5" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                    <div class="lblBox LNOrient" style=" width: 130px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="6" CssClass="txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
             <div class="divClear_10">
            </div>
            <div class="divRowContainer" id="RefNum" runat="server" visible="false">
                <div class="lblBox LNOrient" style=" width: 120px" id="Div4">
                    Reference Number
                </div>
                <div class="divtxtBox LNOrient" style=" width: 450px">
                    <asp:DropDownList ID="ddlReferenceNumber" runat="server" CssClass="ddlStype" AutoPostBack="true"
                        TabIndex="7">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
           
            <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
                <div class="divRowContainer  LNOrient divReportAction">
                    <div>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js"" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
