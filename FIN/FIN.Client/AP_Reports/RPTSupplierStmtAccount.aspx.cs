﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AP
{
    public partial class RPTSupplierStmtAccount : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SuplrStmntOfAccountReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SuplrStmntOfAccountReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {
                    if (ddlGlobalSegment.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Value);
                    }
                    if (ddlCustomer.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("VENDOR_ID", ddlCustomer.SelectedValue);
                    }
                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }



                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    FINSP.GetSPFOR_SupplierStmtOfAccount(ddlCustomer.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtToDate.Text.ToString());
                    //GetSPFOR_SupplierStmtOfAccount

                    ReportData = FIN.BLL.AR.SuplrStmtAccnt_BLL.GetReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("frwomtdoedddate", "From Date and To Date cannot be empty");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SuplrStmntOfAccountReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.AR.SuplrStmtAccnt_BLL.GetGroupName(ref ddlGlobalSegment);
            FIN.BLL.AR.SuplrStmtAccnt_BLL.GetVendorName(ref ddlCustomer);

        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt_vendor_name = new DataTable();
            dt_vendor_name = DBMethod.ExecuteQuery(FIN.DAL.AR.SuplrStmtAccnt_DAL.getVendorName(ddlCustomer.SelectedValue.ToString())).Tables[0];
            if (dt_vendor_name != null)
            {
                if (dt_vendor_name.Rows.Count > 0)
                {
                    txtCustName.Text = dt_vendor_name.Rows[0][0].ToString();
                }
            }
        }
    }
}