﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.Reports.AP
{
    public partial class RPTSuppilerParam : PageBase
    {

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromCreatedDate.Text, txtToCreatedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromModifiedDate.Text, txtToModifiedDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                //if (ddlSupplierType.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add(FINColumnConstants.VENDOR_TYPE, ddlSupplierType.SelectedItem.Value);
                //}
                if (ddlFromSupplier.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("From_Supplier_Id", ddlFromSupplier.SelectedItem.Value);
                }
                if (ddlToSupplier.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("To_Supplier_Id", ddlToSupplier.SelectedItem.Value);
                }
                if (ddlSupplierCountry.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add(FINColumnConstants.COUNTRY_CODE, ddlSupplierCountry.SelectedItem.Value);
                }
                if (ddlState.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add(FINColumnConstants.STATE_CODE, ddlState.SelectedItem.Value);
                }
                if (ddlCity.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add(FINColumnConstants.CITY_CODE, ddlCity.SelectedItem.Value);
                }

                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtFromCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtFromCreatedDate.Text);
                }
                if (txtToCreatedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtToCreatedDate.Text);
                }
                if (txtFromModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtFromModifiedDate.Text);
                }
                if (txtToModifiedDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtToModifiedDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                //ReportData = FIN.BLL.SSM.Country_BLL.GetReportData();
                ReportData = FIN.BLL.AP.Supplier_BLL.GetVendorReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
           //Country_BLL.getSupplierType(ref ddlSupplierType);
           Country_BLL.getCountryName(ref ddlSupplierCountry);
           Supplier_BLL.GetSupplier(ref ddlFromSupplier);
           Supplier_BLL.GetSupplier(ref ddlToSupplier);
           FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
           FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);

        }

        protected void ddlSupplierCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Supplier_BLL.fn_getState(ref ddlState,ddlSupplierCountry.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        
    }
}