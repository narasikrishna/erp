﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InventoryPriceListParam.aspx.cs" Inherits="FIN.Client.AP_Reports.InventoryPriceListParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer LNOrient">
            <div class="divFormcontainer" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td style=" width: 800px">
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
 <script type="text/javascript">
     $(document).ready(function () {
         fn_changeLng('<%= Session["Sel_Lng"] %>');
     });

     $(document).ready(function () {
         $("#form1").validationEngine();
         return fn_SaveValidation();
     });

     var prm = Sys.WebForms.PageRequestManager.getInstance();
     prm.add_endRequest(function () {
         return fn_SaveValidation();
     });

     function fn_SaveValidation() {
         $("#FINContent_btnSave").click(function (e) {
             //e.preventDefault();
             return $("#form1").validationEngine('validate')
         })
     }

    </script>
</asp:Content>