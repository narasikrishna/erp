﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AP
{
    public partial class RPTDayBookMaterialTransactionParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenue", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }
        protected void FillComboBox()
        {
            Item_BLL.GetItemName_frTransLedger(ref ddlItem);
            //Item_BLL.GetTransactionType(ref ddlTransactionType);
            FIN.BLL.Lookup_BLL.GetLookUpValue(ref ddlTransactionType, "TRANSAC_TYPE");
            //FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg(ref ddlperiod);
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2QuantityExp(txtFromItemQuantity.Text, txtToItemQuantity.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
           

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlItem.SelectedValue.ToString() != string.Empty)
                {
                    htFilterParameter.Add("Item_id", ddlItem.SelectedValue.ToString());
                    htFilterParameter.Add("Item", ddlItem.SelectedItem.Text.ToString());
                }
                if (ddlTransactionType.SelectedValue.ToString() != string.Empty)
                {
                    htFilterParameter.Add("Transaction_Type", ddlTransactionType.SelectedItem.Text);
                    htFilterParameter.Add("Transaction_Type_Id", ddlTransactionType.SelectedValue.ToString());

                }
                if (ddlReferenceNumber.SelectedValue.ToString() != string.Empty)
                {
                    htFilterParameter.Add("Reference_Number", ddlReferenceNumber.SelectedItem.Text);
                    htFilterParameter.Add("Reference_Number_Id", ddlReferenceNumber.SelectedValue.ToString());

                }
                if (txtFromItemQuantity.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Quantity", txtFromItemQuantity.Text);
                }
                if (txtToItemQuantity.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Quantity", txtToItemQuantity.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                //if (ddlperiod.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                //}

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getDayBook_Material_Trans());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //    if (ddlTransactionType.SelectedValue.ToString() == "INVOICE")
            //    {
            //        FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlReferenceNumber);
            //    }
            //    if (ddlTransactionType.SelectedValue.ToString() == "Purchase Order")
            //    {
            //        FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber(ref ddlReferenceNumber);
            //    }
            //    if (ddlTransactionType.SelectedValue.ToString() == "GRN")
            //    {
            //        PurchaseItemReceipt_BLL.GetGRNNumber(ref ddlReferenceNumber);
            //    }
        }


    }
}