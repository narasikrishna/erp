﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTSupplierwisePaymentParam.aspx.cs" Inherits="FIN.Client.AP_Reports.RPTSupplierwisePaymentParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 600px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblRequisitionType">
                Supplier Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1" CssClass="ddlStype"
                    AutoPostBack="true" Width="350px" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                Payment Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlPaymentNumber" runat="server" TabIndex="2" CssClass="ddlStype"
                    Width="350px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div7">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="2" CssClass="ddlStype"
                    Width="350px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div4">
                From Payment Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="6" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtFromAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                To Payment Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="7" CssClass=" txtBox_N" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtToAmount" />
            </div>
        </div>
    </div>
    <div class="divRowContainer">
        <div class="divFormcontainer" style="width: 370px" id="div3">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                            <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%--  <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
