﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AR_DASHBOARD
{
    public partial class ARTopCustomers_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["FIVE_CUSTOMER_GRAPH"] = null;
                AssignToControl();
                DisplayFiveCustomerDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            if (Session["FIVE_CUSTOMER_GRAPH"] != null)
            {
                chrtTop5Customer.DataSource = (DataTable)Session["FIVE_CUSTOMER_GRAPH"];
                chrtTop5Customer.Series["S_Customer_Count"].XValueMember = "vendor_name";
                chrtTop5Customer.Series["S_Customer_Count"].YValueMembers = "payment_amt";
                chrtTop5Customer.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["FIVE_CUSTOMER_GRAPH"];
                gvGraphdata.DataBind();
            }
        }

        private void DisplayFiveCustomerDetails()
        {
            DataTable dt_Top5Cust = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getTop5CustomerAmt()).Tables[0];
            Session["FIVE_CUSTOMER_GRAPH"] = dt_Top5Cust;
            if (dt_Top5Cust.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_Top5Cust.DefaultView.ToTable(true, "vendor_id", "vendor_name", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    DataRow[] dr_count = dt_Top5Cust.Select("vendor_id='" + dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString() + "'");
                    dr["payment_amt"] = FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString());
                    dt_Dept_List.Rows.Add(dr);
                }

                Session["FIVE_CUSTOMER_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                
                chrtTop5Customer.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.getTop5CustomerAmt());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}