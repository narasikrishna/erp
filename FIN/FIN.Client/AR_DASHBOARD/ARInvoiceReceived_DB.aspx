﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ARInvoiceReceived_DB.aspx.cs" Inherits="FIN.Client.AR_DASHBOARD.ARInvoiceReceived_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowInvoiceReceived() {
            $("#divInvoiceReceived").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpAge" style="width: 100%;">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Invoice/Received
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowInvoiceReceived()" />
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divInvoiceReceive" style="width: 100%">
            <asp:Chart ID="chrtInvoicedReceive" runat="server" Width="500px" Height="310px" EnableViewState="true">
                <Series>
                    <asp:Series ChartArea="ChartArea1" Name="invoice_amt" IsValueShownAsLabel="True"
                        XValueMember="vendor_name" YValueMembers="invoice_amt">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="payment_amt"
                        XValueMember="vendor_name" YValueMembers="cust_inv_amt">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divInvoiceReceived" style="width: 100%; background-color: ThreeDFace; position: absolute;
            z-index: 5000; top: 30px; left: 10px; display: none">
            <div id="divEmpDept" runat="server" visible="true">
                <asp:GridView ID="gvInvoiceReceived" runat="server" CssClass="Grid" Width="100%"
                    DataKeyNames="vendor_id" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                    EmptyDataText=" No Data Found">
                    <Columns>
                        <asp:TemplateField HeaderText="Supplier">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_Dept_Name" runat="server" Text='<%# Eval("vendor_name") %>'
                                    OnClick="lnk_InvoiceReceived_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="invoice_amt" HeaderText="Invoice Amount">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="cust_inv_amt" HeaderText="Received Amount">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="vendor_name" HeaderText="Supplier"></asp:BoundField>
                    <asp:BoundField DataField="invoice_amt" HeaderText="Invoice Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="payment_amt" HeaderText="Received Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
