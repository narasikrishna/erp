﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AR_DASHBOARD
{
    public partial class ARInvoiceReceived_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["INVOICE_RECEIVED_GRAPH"] = null;
                AssignToControl();
                DisplayInvoiceReceivedDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            if (Session["INVOICE_RECEIVED_GRAPH"] != null)
            {
                chrtInvoicedReceive.DataSource = (DataTable)Session["INVOICE_RECEIVED_GRAPH"];
                chrtInvoicedReceive.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["INVOICE_RECEIVED_GRAPH"];
                gvGraphdata.DataBind();
            }
        }

        private void DisplayInvoiceReceivedDetails()
        {
            DataTable dt_InvRec = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.GetInvoiceReceivedAmt()).Tables[0];
            Session["INVOICE_RECEIVE_GRAPH_LOAD"] = dt_InvRec;
            if (dt_InvRec.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_InvRec.DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "cust_inv_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("invoice_amt");
                dt_Dept_List.Columns.Add("cust_inv_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                    dr["cust_inv_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["cust_inv_amt"].ToString()).ToString());
                    dt_Dept_List.Rows.Add(dr);
                }
                gvInvoiceReceived.DataSource = dt_Dept_List;
                gvInvoiceReceived.DataBind();
                Session["INVOICE_RECEIVED_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
            else
            {
                gvInvoiceReceived.DataSource = new DataTable();
                gvInvoiceReceived.DataBind();
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrtInvoicedReceive.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AR.Invoice_DAL.GetInvoiceReceivedAmt());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void lnk_InvoiceReceived_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadInvoiceReceivedDetails(gvr);
        }
        private DataTable LoadData(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["INVOICE_RECEIVE_GRAPH_LOAD"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["vendor_id"].ToString().Contains(gvInvoiceReceived.DataKeys[gvr.RowIndex].Values["vendor_id"].ToString()))
                       .GroupBy(row => new
                       {
                           email = row.Field<string>("vendor_id"),
                           Name = row.Field<string>("vendor_name")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_DEPT = tables[0].DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "cust_inv_amt");


            DataTable dt_Dept_List = new DataTable();
            dt_Dept_List.Columns.Add("vendor_id");
            dt_Dept_List.Columns.Add("vendor_name");
            dt_Dept_List.Columns.Add("invoice_amt");
            dt_Dept_List.Columns.Add("cust_inv_amt");

            for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Dept_List.NewRow();
                dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                dr["cust_inv_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["cust_inv_amt"].ToString()).ToString());
                dt_Dept_List.Rows.Add(dr);
            }
            return dt_Dept_List;
        }
        private void LoadInvoiceReceivedDetails(GridViewRow gvr)
        {
            DataTable dt_Dept_List = new DataTable();
            dt_Dept_List = LoadData(gvr);
            gvInvoiceReceived.DataSource = dt_Dept_List;
            gvInvoiceReceived.DataBind();

            Session["EMP_DESIG_GRAP"] = dt_Dept_List;

            LoadGraph();

        }
    }
}