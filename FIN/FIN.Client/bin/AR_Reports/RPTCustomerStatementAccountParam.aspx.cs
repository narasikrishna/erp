﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTCustomerStatementAmountParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARCustomerStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARCustomerStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }

        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmt.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedValue);
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                }
                if (ddlCustomerFrom.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_FROM_ID", ddlCustomerFrom.SelectedItem.Value);
                    htFilterParameter.Add("CUST_FROM_NAME", ddlCustomerFrom.SelectedItem.Text);
                }
               
               
                if (ddlCustomerTo.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_TO_ID", ddlCustomerTo.SelectedItem.Value);
                    htFilterParameter.Add("CUST_TO_NAME", ddlCustomerTo.SelectedItem.Text);
                }
              
              
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                if ( txtFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_AMT", txtFromAmt.Text);
                }
                if (txtToAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_AMT", txtToAmount.Text);
                }
              
                //if (ddlCustomerNumber.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("VENDOR_ID", ddlCustomerNumber.SelectedValue);
                //}
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty && ddlCustomerFrom.SelectedValue != string.Empty && ddlCustomerTo.SelectedValue != string.Empty)
                {
                   // FIN.DAL.AR.CustomerStatementofAccount_DAL.GetSP_CustomerStatementOfAccount(ddlCustomerFrom.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtToDate.Text.ToString());
                    FIN.DAL.AR.CustomerStatementofAccount_DAL.GetSP_CustomerStatementOfAccount_New(ddlCustomerFrom.SelectedValue.ToString(),ddlCustomerTo.SelectedValue.ToString(),txtFromDate.Text.ToString(), txtToDate.Text.ToString());
                }
                else if (ddlCustomerFrom.SelectedValue == string.Empty && ddlCustomerTo.SelectedValue == string.Empty)
                {
                    FIN.DAL.AR.CustomerStatementofAccount_DAL.GetSP_CustomerStatementOfAccount_New("ALL", "ALL", txtFromDate.Text.ToString(), txtToDate.Text.ToString());
                }
                else
                {
                    ErrorCollection.Add("Date", "From date To date Can't be empty");
                    return;
                }
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
               
                ReportData = FIN.BLL.AR.CustomeStatementofAccounts_BLL.GetCustomerStatementOfAccountReportData();

                //ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARCustomerStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void FillComboBox()
        {

            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlCustomerFrom,true);
            //ddlCustomerFrom.Items.RemoveAt(0);
            //ddlCustomerFrom.Items.Insert(0, new ListItem("ALL", ""));
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlCustomerTo,true);
            //ddlCustomerTo.Items.RemoveAt(0);
            //ddlCustomerTo.Items.Insert(0, new ListItem("ALL", ""));
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.AP.APAginAnalysis_BLL.GetGroupName(ref ddlGlobalSegment);
            //FIN.BLL.AP.SupplierStatementOfAccount_BLL.GetVendorNumber(ref ddlCustomerNumber);

        }
    }
}