﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AR
{
    public partial class RPTDayBookPaymentReportParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookPaymentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookPaymentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();


            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
           

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_VALUE_ID", ddlGlobalSegment.SelectedItem.Value);
                   htFilterParameter.Add("segment_value",ddlGlobalSegment.SelectedItem.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (ddlFromCustomerName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromSupplierName", ddlFromCustomerName.SelectedItem.Text.ToString());
                    htFilterParameter.Add("FromSupplierID", ddlFromCustomerName.SelectedValue.ToString());
                }
                if (ddlToCustomerName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToSupplierName", ddlToCustomerName.SelectedItem.Text.ToString());
                    htFilterParameter.Add("ToSupplierID", ddlToCustomerName.SelectedValue.ToString());
                }
                if (ddlFromPaymentNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromPaymentName", ddlFromPaymentNum.SelectedItem.Text.ToString());
                    htFilterParameter.Add("FromPaymentID", ddlFromPaymentNum.SelectedValue.ToString());
                }
                if (ddlToPaymentNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToPaymentName", ddlToPaymentNum.SelectedItem.Text.ToString());
                    htFilterParameter.Add("ToPaymentID", ddlToPaymentNum.SelectedValue.ToString());
                }
                if (ddlCurrency.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CURRENCY_CODE", ddlCurrency.SelectedItem.Text.ToString());
                    htFilterParameter.Add("CURRENCY_ID", ddlCurrency.SelectedValue.ToString());
                }
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount", txtFromAmount.Text);
                    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtToAmount.Text);
                        htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmount.Text));
                    }
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.DayBookPayment_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DayBookPaymentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlFromCustomerName, true);
            //FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlToCustomerName, true);

            FIN.BLL.AP.Supplier_BLL.GetSupplierNumber(ref ddlFromCustomerName, true);
            FIN.BLL.AP.Supplier_BLL.GetSupplierNumber(ref ddlToCustomerName, true);

            FIN.BLL.AP.Supplier_BLL.GetPaymentDetails(ref ddlFromPaymentNum, false);
            FIN.BLL.AP.Supplier_BLL.GetPaymentDetails(ref ddlToPaymentNum, false);

            Currency_BLL.getCurrencyCode(ref ddlCurrency);
            ddlGlobalSegment.SelectedIndex = 1;
            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);

        }

    }
}