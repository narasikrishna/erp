﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTSalesRegisterParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }


        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromFunAmount.Text, txtToFunAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromTrnAmount.Text, txtToTrnAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlFromCustNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_FROM", ddlFromCustNumber.SelectedItem.Value);
                    htFilterParameter.Add("CUST_FROM_NAME", ddlFromCustNumber.SelectedItem.Text);
                }
                else
                {
                    htFilterParameter.Add("CUST_FROM_NAME", "All");
                }


                if (ddlToCustNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_TO", ddlToCustNumber.SelectedItem.Value);
                    htFilterParameter.Add("CUST_TO_NAME", ddlToCustNumber.SelectedItem.Text);
                }
                else
                {
                    htFilterParameter.Add("CUST_TO_NAME", "All");
                }

                if (ddlFromInvoiceNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("INV_FROM", ddlFromInvoiceNumber.SelectedItem.Text);
                }
                else
                {
                    htFilterParameter.Add("INV_FROM1", "All");
                }

                if (ddlToInvoiceNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("INV_TO", ddlToInvoiceNumber.SelectedItem.Text);
                }
                else
                {
                    htFilterParameter.Add("INV_TO1", "All");
                }


                if (txtFromTrnAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_TRN_AMOUNT", txtFromTrnAmount.Text);
                    htFilterParameter.Add("FROM_TRN_AMOUNT1", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromTrnAmount.Text));
                }

                if (txtToTrnAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_TRN_AMOUNT", txtToTrnAmount.Text);
                    htFilterParameter.Add("TO_TRN_AMOUNT1", DBMethod.GetAmtDecimalCommaSeparationValue(txtToTrnAmount.Text));
                }
                if (txtFromFunAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_FUN_AMOUNT", txtFromFunAmount.Text);
                    htFilterParameter.Add("FROM_FUN_AMOUNT1", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromFunAmount.Text));
                }

                if (txtToFunAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_FUN_AMOUNT", txtToFunAmount.Text);
                    htFilterParameter.Add("TO_FUN_AMOUNT1", DBMethod.GetAmtDecimalCommaSeparationValue(txtToFunAmount.Text));
                }




                if (ddlFromOrderNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ORDER_FROM", ddlFromOrderNumber.SelectedItem.Text);
                }

                if (ddlToOrderNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ORDER_TO", ddlToOrderNumber.SelectedItem.Text);
                }



                if (ddlTrancType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TRAN_TYPE", ddlTrancType.SelectedItem.Text);
                }

                if (ddlTrnCurrency.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TRANC_CURRENCY", ddlTrnCurrency.SelectedItem.Text);
                }




                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.SalesRegister_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void FillComboBox()
        {

            //FIN.BLL.AR.SalesRegister_BLL.GetGroupName(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlFromCustNumber);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlToCustNumber);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber(ref ddlFromInvoiceNumber, true);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber(ref ddlToInvoiceNumber, true);

            FIN.BLL.AR.SalesOrder_BLL.fn_GetOrderNo(ref ddlFromOrderNumber);
            FIN.BLL.AR.SalesOrder_BLL.fn_GetOrderNo(ref ddlToOrderNumber);
            FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref  ddlTrnCurrency);
            Lookup_BLL.GetLookUpValues(ref  ddlTrancType, "INV_TY");



        }

    }
}