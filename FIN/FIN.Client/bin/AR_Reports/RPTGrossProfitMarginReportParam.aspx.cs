﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTGrossProfitMarginReportParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GrossProfitMarginReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GrossProfitMarginReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }


        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFunFromAmt.Text, txtFunToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtTransFromAmt.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2QuantityExp(txtGPFrom.Text, txtGPTo.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlCustomerFrom.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_FROM_ID", ddlCustomerFrom.SelectedItem.Value);
                    htFilterParameter.Add("CUST_FROM_NAME", ddlCustomerFrom.SelectedItem.Text);
                }
                if (ddlCustomerTo.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CUST_TO_ID", ddlCustomerTo.SelectedItem.Value);
                    htFilterParameter.Add("CUST_TO_NAME", ddlCustomerTo.SelectedItem.Text);
                }
                if (ddlTransactionType.SelectedValue.ToString() != string.Empty)
                {
                    htFilterParameter.Add("Transaction_Type", ddlTransactionType.SelectedItem.Text);
                    htFilterParameter.Add("Transaction_Type_Id", ddlTransactionType.SelectedValue.ToString());

                }
                if (ddlCurrency.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CURRENCY_ID", ddlCurrency.SelectedItem.Value);
                    htFilterParameter.Add("CURRENCY_NAME", ddlCurrency.SelectedItem.Text);
                }
                if (ddlInvoiceNoFrom.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("INV_NO_FROM_ID", ddlInvoiceNoFrom.SelectedItem.Value);
                    htFilterParameter.Add("INV_NO_FROM_NAME", ddlInvoiceNoFrom.SelectedItem.Text);
                }
                if (ddlInvoiceNoTo.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("INV_NO_TO_ID", ddlInvoiceNoTo.SelectedItem.Value);
                    htFilterParameter.Add("INV_NO_TO_NAME", ddlInvoiceNoTo.SelectedItem.Text);
                }
                if (txtTransFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("TRANS_FROM_AMT", txtTransFromAmt.Text);
                }
                if (txtToAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("TRANS_TO_AMT", txtToAmount.Text);
                }
                if (txtGPFrom.Text != string.Empty)
                {
                    htFilterParameter.Add("GP_FROM", txtGPFrom.Text);
                }
                if (txtGPTo.Text != string.Empty)
                {
                    htFilterParameter.Add("GP_TO", txtGPTo.Text);
                }
                if (txtFunFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("FN_CURR_FROM", txtFunFromAmt.Text);
                }
                if (txtFunToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("FN_CURR_TO", txtFunToAmt.Text);
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }

                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportData = FIN.BLL.AR.Invoice_BLL.GetGrossProfitMarginReportData();
                //ReportData = FIN.BLL.HR.Payslip_BLL.GetAbsentReport();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GrossProfitMarginReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlCustomerFrom, true);
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlCustomerTo, true);
            FIN.BLL.CA.Bank_BLL.fn_getCurrency(ref ddlCurrency);
            FIN.BLL.Lookup_BLL.GetLookUpValue(ref ddlTransactionType, "SO_INV_TY");
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlInvoiceNoFrom);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlInvoiceNoTo);

        }

    }
   
}