﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTDayBookInvoiceParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTDayBookInvoiceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCustomerName">
                From Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromCustomerName" runat="server" TabIndex="2" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px; ">
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceType">
                To Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToCustomerName" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceNumber">
                From Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromInvoiceNumber" runat="server" TabIndex="4" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                To Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToInvoiceNumber" runat="server" TabIndex="5" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px;" id="Div5">
                From Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 249px">
                <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="6" MaxLength="13" CssClass="EntryFont txtBox_N"
                    FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtFromAmount" />
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px;" id="Div6">
                To Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 249px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="7" MaxLength="13" CssClass="EntryFont txtBox_N"
                     FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 249px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="8" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 249px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="9" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
