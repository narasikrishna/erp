﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTAcccountCostCenterSummay : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                FillStartDate();
                FillJournalNumber();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbUNPosted.Items[0].Text = (Prop_File_Data["Posted_P"]);
                    rbUNPosted.Items[0].Selected = true;
                    rbUNPosted.Items[1].Text = (Prop_File_Data["Unposted_P"]);
                    rbUNPosted.Items[2].Text = (Prop_File_Data["Both_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }

        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddlSegmentValue, "SEG_ID-0000000101", false);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            ddlGlobalSegment.SelectedIndex = 1;

            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);


            //  FIN.BLL.SSM.User_BLL.GetUserData(ref ddlUser);

        }
        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromValue.Text, txtToValue.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtCreditFrom.Text, txtCreditTo.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                    htFilterParameter.Add("global_segment_id", ddlGlobalSegment.SelectedItem.Value);
                }

                if (ddlSegmentValue.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_VALUE", ddlSegmentValue.SelectedItem.Text);
                    htFilterParameter.Add("SEGMENT_VALUE_ID", ddlSegmentValue.SelectedItem.Value);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                if (txtFromValue.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Value", txtFromValue.Text);
                    htFilterParameter.Add("FromValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromValue.Text));
                }

                if (txtToValue.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Value", txtToValue.Text);
                    htFilterParameter.Add("ToValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtToValue.Text));
                }


                if (txtCreditFrom.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Credit_Value", txtCreditFrom.Text);
                    htFilterParameter.Add("FromCreditValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditFrom.Text));
                }

                if (txtCreditTo.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Credit_Value", txtCreditTo.Text);
                    htFilterParameter.Add("ToCreditValue", DBMethod.GetAmtDecimalCommaSeparationValue(txtCreditTo.Text));
                }

                if (ddlUser.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("USER_NAME", ddlUser.SelectedItem.Text);
                    htFilterParameter.Add("USER_CODE", ddlUser.SelectedItem.Value);
                }

                //if (txtDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("Balance_Date", txtDate.Text);
                //}

                ////Changed to RBList..
                //string str_Unpost = "0";

                //if (chkUnPost.Checked)
                //{
                //    str_Unpost = "1";
                //    htFilterParameter.Add("IncludeUnposted", "Yes");
                //}
                //else
                //{ htFilterParameter.Add("IncludeUnposted", "NO"); }


                string str_Unpost = "";

                if (rbUNPosted.SelectedValue.ToString() == "P")
                {
                    str_Unpost = "1";
                }
                else if (rbUNPosted.SelectedValue.ToString() == "U")
                {
                    str_Unpost = "0";
                }
                else if (rbUNPosted.SelectedValue.ToString() == "B")
                {
                    str_Unpost = "1,0";
                }

                htFilterParameter.Add("UNPOST", str_Unpost);
                htFilterParameter.Add("JournalPosted", rbUNPosted.SelectedItem.Text);

                //if (chkWithZero.Checked)
                //{
                //    htFilterParameter.Add("WITHZERO", "TRUE");
                //}
                //else
                //{
                //    htFilterParameter.Add("WITHZERO", "FALSE");
                //}
                // ReportData = FIN.DAL.GL.TrailBalance_DAL.GetSP_GetAccountCostCenterSummary(DBMethod.ConvertStringToDate(txtToDate.Text), str_Unpost, FIN.BLL.GL.TrailBalance_BLL.GetAccountCostSummary());


                /*if (txtFromAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                }

                if (txtToAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                }*/

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }

                if (ddlFromJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromJournal", ddlFromJournal.SelectedValue.ToString());
                    htFilterParameter.Add("From_Journal", ddlFromJournal.SelectedItem.ToString());
                }

                if (ddlToJournal.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToJournal", ddlToJournal.SelectedValue.ToString());
                    htFilterParameter.Add("To_Journal", ddlToJournal.SelectedItem.ToString());
                }



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getDayBookCostCenterSummary());

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            FillJournalNumber();
        }

        private void FillJournalNumber()
        {
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlFromJournal, txtFromDate.Text, txtToDate.Text);
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlToJournal, txtFromDate.Text, txtToDate.Text);
        }


    }
}