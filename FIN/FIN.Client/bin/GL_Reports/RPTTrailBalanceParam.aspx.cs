﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.Reports.GL
{
    public partial class RPTTrailBalanceParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceGroupReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceGroupReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtDebitFromAmt.Text, txtDebitToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            
            ErrorCollection = CommonUtils.Validate2AmountsExp(txtCreditFromAmt.Text, txtCreditToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Text);
                }
                if (txtDate.Text != string.Empty)
                {
                    htFilterParameter.Add("Balance_Date", txtDate.Text);
                }
                if (ddlFromAcctGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAcctGrp", ddlFromAcctGroup.SelectedItem.Text);
                }
                if (ddlToAcctGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAcctGrp", ddlToAcctGroup.SelectedItem.Text);
                }
                if (txtDebitFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_From_Amt", txtDebitFromAmt.Text);
                }
                if (txtDebitToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Debit_To_Amt", txtDebitToAmt.Text);
                }
                if (txtCreditFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_From_Amt", txtCreditFromAmt.Text);
                }
                if (txtCreditToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("Credit_To_Amt", txtCreditToAmt.Text);
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.DAL.GL.TrailBalance_DAL.GetSP_TrailBalanceGroup(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), FIN.BLL.GL.TrailBalance_BLL.GetReportData());

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceGroupReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlFromAcctGroup, false);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlToAcctGroup, false);

            ddlGlobalSegment.SelectedIndex = 1;
            //FIN.BLL.GL.TrailBalance_BLL.GetGroupName(ref ddlGlobalSegment);
            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}