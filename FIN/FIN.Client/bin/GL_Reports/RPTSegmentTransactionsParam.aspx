﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTSegmentTransactionsParam.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTSegmentTransactionsParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.rbl input[type="radio"]
{
   margin-left: 15px;
   margin-right: 1px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
         <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="Div9">
                Segment
            </div>
            <div class="divtxtBox" style="float: left; width: 400px">
                <asp:DropDownList ID="ddlSegment" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlSegment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                Cost Centre
            </div>
            <div class="divtxtBox" style="float: left; width: 400px">
                <asp:DropDownList ID="ddlCostCentre" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
       
            <div class="lblBox" style="float: left; width: 100px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox" style="float: left; width: 130px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 150px" id="Div8">
            Segment Type
        </div>
        <div class="divtxtBox" style="float: left;">
            <asp:RadioButtonList ID="rbSelectSegments" runat="server" TabIndex="5" RepeatColumns="3"  CssClass="rbl">
                <asp:ListItem Text="Segment 1" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Segment 2" Value="2"></asp:ListItem>
                <asp:ListItem Text="Segment 3" Value="3"></asp:ListItem>
                <asp:ListItem Text="Segment 4" Value="4"></asp:ListItem>
                <asp:ListItem Text="Segment 5" Value="5"></asp:ListItem>
                <asp:ListItem Text="Segment 6" Value="6"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer" style="display: none">
        <div class="lblBox" style="float: left; width: 150px" id="Div1">
            Include with Zero
        </div>
        <div class="lblBox" style="float: left; width: 100px" id="Div3">
            <asp:CheckBox ID="chkShowZeroValues" runat="server" Checked="false" Text=" " TabIndex="6" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox" style="float: left; width: 150px" id="Div4">
            Include Unposted
        </div>
        <div class="lblBox" style="float: left; width: 100px" id="Div5">
            <asp:CheckBox ID="chkUnPosted" runat="server" Checked="false" Text=" " TabIndex="7" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer" style="display: none">
        <div class="lblBox" style="float: left; width: 150px" id="Div6">
            Groupwise
        </div>
        <div class="lblBox" style="float: left; width: 100px" id="Div7">
            <asp:CheckBox ID="chkGroupwise" runat="server" Checked="false" Text=" " TabIndex="8" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divReportAction">
        <table class="ReportTable">
            <tr>
                <td style="float: right; width: 520px;">
                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                        Width="35px" Height="25px" OnClick="btnSave_Click" TabIndex="9" />
                </td>
                <td>
                    <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                </td>
            </tr>
        </table>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
    
    
</asp:Content>
