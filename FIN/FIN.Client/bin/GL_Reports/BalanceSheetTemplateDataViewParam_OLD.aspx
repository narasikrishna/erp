﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    EnableEventValidation="true" CodeBehind="BalanceSheetTemplateDataViewParam_OLD.aspx.cs"
    Inherits="FIN.Client.GL_Reports.BalanceSheetTemplateDataViewParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/TreeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_showGraph() {
            fn_JqueryTreeView();
            fn_HideGvChildNode();
            fn_Showslider();
        }
        function fn_JqueryTreeView() {

            $('.tree li').each(function () {
                if ($(this).children('ul').length > 0) {
                    $(this).addClass('parent');
                }
            });

            $('.tree li.parent > a').click(function () {
                $(this).parent().toggleClass('active');
                $(this).parent().children('ul').slideToggle('fast');
            });

            $('#all').click(function () {

                $('.tree li').each(function () {
                    $(this).toggleClass('active');
                    $(this).children('ul').slideToggle('fast');
                });
            });

            $('.tree li').each(function () {
                $(this).toggleClass('active');
                $(this).children('ul').slideToggle('fast');
            });


        }

        function fn_HideGvChildNode() {
            $("#<%=gvData.ClientID %> tr").each(function () {

                if (!this.rowIndex) return;



                if ($(this).find("input[type=hidden][id*=hf_Parent_group_id]").val() != 'HEAD') {
                    $(this).hide();

                }

                if ($(this).find("input[type=hidden][id*=hf_Group_Level]").val() == '0') {
                    $(this).find("td:first").html(" ");

                }



            });
        }


        function fn_GroupAccClick(str_tmpl_id, str_Group_name) {

            $("#<%=gvData.ClientID %> tr").each(function () {
                if (!this.rowIndex) return;

                $(this).hide();


                if ($(this).find("input[type=hidden][id*=hf_Parent_group_id]").val() == str_tmpl_id) {
                    $(this).show();

                }

                if ($(this).find("input[type=hidden][id*=hf_acct_grp_id]").val() == str_tmpl_id) {
                    $(this).find("td:first").html("<img src='../Images/collapse_blue.png' id='img_Det' title='Collapse' alt='-' onclick=fn_DetClick('" + str_tmpl_id + "') />");
                    $(this).show();


                }


            });

        }
        function fn_DetClick(str_tmpl_id) {

            var var_showhide = "SHOW";
            $("#<%=gvData.ClientID %> tr").each(function () {

                if (!this.rowIndex) return;

                if ($(this).find("input[type=hidden][id*=hf_acct_grp_id]").val() == str_tmpl_id) {
                    if ($(this).find("td:first").html().toString().indexOf("expand_blue") >= 0) {
                        $(this).find("td:first").html("<img src='../Images/collapse_blue.png' id='img_Det' title='Collapse' alt='-' onclick=fn_DetClick('" + str_tmpl_id + "') />");
                        var_showhide = "SHOW";
                        return;
                    }
                    else {
                        $(this).find("td:first").html("<img src='../Images/expand_blue.png' id='img_Det' title='Collapse' alt='-' onclick=fn_DetClick('" + str_tmpl_id + "') />");
                        var_showhide = "HIDE";
                        return;
                    }
                }

            });


            $("#<%=gvData.ClientID %> tr").each(function () {

                if (!this.rowIndex) return;

                if ($(this).find("input[type=hidden][id*=hf_Parent_group_id]").val() == str_tmpl_id) {
                    if (var_showhide == "HIDE") {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                }

            });
        }
       
    </script>
    <script type="text/javascript">

        function fn_PrintDiv(div_printDiv_id) {

            /*

            var divElements = document.getElementById(div_printDiv_id).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;
            console.log(divElements);
            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;

            */

            var divToPrint = document.getElementById(div_printDiv_id);
            console.log(divToPrint.innerHTML);
            $("#<%=hf_PrintData.ClientID %>").val(divToPrint.innerHTML);
            $("#<%=btnExport.ClientID %>").click();
            

            /* var newWin = window.open('', 'Print-Window', 'width=100,height=100');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

            newWin.document.close();

            setTimeout(function () { newWin.close(); }, 10);
            */

        }

       
    </script>
    <script src="../Scripts/JQuery/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../Scripts/JQuery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../Scripts/Slider/jquery-ui-slider-pips.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Slider/jquery-ui-slider-pips.js" type="text/javascript"></script>
    <script type="text/javascript">



        function fn_Showslider() {

            alert("inside");
            $(".slider2")
    .slider({
        max: 10
    })
    .slider("pips", {
        rest: "label"
    })
      .on("slidechange", function (e, ui) {
          alert(ui.value);
      });

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                Template Name
            </div>
            <div class="divtxtBox" style="float: left; width: 450px">
                <asp:DropDownList ID="ddlTemplateName" runat="server" TabIndex="1" CssClass="validate[required] RequiredField  ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblDate">
                No Of years
            </div>
            <div class="divtxtBox" style="float: left; width: 50px">
                <asp:TextBox ID="txtNoOfYear" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"
                    Text="1" MaxLength="1"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                    TargetControlID="txtNoOfYear" />
            </div>
            <div class="lblBox" style="float: left; width: 380px">
                <div style="float: right">
                    <asp:ImageButton ID="imgBtnExport" runat="server" ImageUrl="~/Images/print.png" ToolTip="Export Result as PDF"
                        CommandName="PDF" AlternateText="PDF" Visible="false" OnClientClick="fn_PrintDiv('div_gvData')"
                        OnClick="imgBtnExport_Click" />
                    <asp:Button ID="btnExport" runat="server" Text="" onclick="btnExport_Click" style="display:none"   />
                    <asp:HiddenField ID="hf_PrintData" runat="server" Value="" />
                </div>
                <div style="float: right">
                    <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" ToolTip="View"
                        AlternateText="View" OnClick="btnView_Click" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div style="float: left; width: 30%">
                <div id="div_Template" runat="server" class="tree">
                </div>
            </div>
            <div style="float: left; width: 70%" >
                <div class="divRowContainer" align="center">
                    <div class="slider2">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" id="div_gvData" >
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                        CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                        DataKeyNames="ACCT_GRP_ID,PARENT_GROUP_ID,GROUP_LEVEL,ACCT_GRP_NAME,NEW_ACCT_GRP_NAME">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="40px" Visible="true">
                                <ItemTemplate>
                                    <img src="../Images/expand_blue.png" id="img_Det" title="Expand " alt="+" onclick="fn_DetClick('<%# Eval("ACCT_GRP_ID") %>')" />
                                </ItemTemplate>
                                <ItemStyle Width="40px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAcct_grp_Name" runat="server" Text='<%# Eval("NEW_ACCT_GRP_NAME") %>'></asp:Label>
                                    <asp:HiddenField ID="hf_Parent_group_id" runat="server" Value='<%# Eval("PARENT_GROUP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_acct_grp_id" runat="server" Value='<%# Eval("ACCT_GRP_ID") %>'
                                        ClientIDMode="Static" />
                                    <asp:HiddenField ID="hf_Group_Level" runat="server" Value='<%# Eval("GROUP_LEVEL") %>'
                                        ClientIDMode="Static" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="GROUP_NUMBER" HeaderText="Number">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR1_BALANCE" HeaderText="Year1">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR2_BALANCE" HeaderText="Year2" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR3_BALANCE" HeaderText="Year3" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR4_BALANCE" HeaderText="Year4" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YEAR5_BALANCE" HeaderText="Year5" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PARENT_GROUP_ID" HeaderText="PARENT_GROUP_ID" Visible="false">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
