﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTSegmentTransactionsParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SegmentTransactionReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();
                //FillStartDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SegmentTransactionReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
               // txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                //txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlCostCentre.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_VALUE", ddlCostCentre.SelectedItem.Text);
                    htFilterParameter.Add("SEGMENT_VALUE_ID", ddlCostCentre.SelectedItem.Value);
                }
                if (ddlSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_NAME", ddlSegment.SelectedItem.Text);
                    htFilterParameter.Add("SEGMENT_ID", ddlSegment.SelectedItem.Value);
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                
                string str_unpost = "0";
                string strWithZero = "0";
                string strGroupWise = "0";

                if (chkUnPosted.Checked)
                {
                    str_unpost = "1";
                }

                if (chkShowZeroValues.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                //if (chkShowZeroValues.Checked)
                //{
                //    strWithZero = "1";
                //}

                if (chkGroupwise.Checked)
                {
                    strGroupWise = "1";
                }

                htFilterParameter.Add("groupwise", strGroupWise);

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                if (rbSelectSegments.SelectedValue.ToString().Length > 0)
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.getSegmentTransactionSummary(rbSelectSegments.SelectedValue.ToString(), str_unpost));
                }
                
                //ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.getSegmentBalancesSummary(strWithZero, str_unpost, strPeriod));

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SegmentTransactionReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillComboBox()
        {
            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.GL.Segments_BLL.getSegmentValueNotInGlobalSegment(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentName(ref ddlSegment);
        }

        protected void ddlSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSegment.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.GL.Segments_BLL.getSegmentValueNotInGlobalSegment(ref ddlCostCentre, true, ddlSegment.SelectedValue.ToString());
            }
        }

    }
}