﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMVServices.Web;
using FIN.BLL;
using System.Collections;
using FIN.DAL;

namespace FIN.Client.GL_Reports
{
    public partial class BalanceSheetTemplateDataViewParam1 : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }
        private void FillComboBox()
        {
            FIN.BLL.GL.BalanceSheetTemplate_BLL.fn_getTemplateName(ref ddlTemplateName);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                gvData.DataSource = new DataTable();
                gvData.DataBind();
                if (txtNoOfYear.Text.ToString().Trim().Length == 0)
                {
                    txtNoOfYear.Text = "1";
                }

                if (int.Parse(txtNoOfYear.Text.ToString()) > 5)
                {
                    ErrorCollection.Add("InvalidYear", "Year Must be less then or equal to 5 ");
                    return;
                }
                if (rbType.SelectedValue.ToString() == "Q")
                {
                    txtNoOfYear.Text = rbQuarterType.SelectedValue.ToString();

                }

                ShowGridColumns();

              
                DataTable dt = FIN.DAL.GL.BalanceSheetTemplate_DAL.get_TmpleateData(ddlTemplateName.SelectedValue.ToString(), int.Parse(txtNoOfYear.Text),rbQuarterType.SelectedValue);

                Session["TemplateData"] = dt;
                var var_empList = dt.AsEnumerable().Where(r => r["GROUP_LEVEL"].ToString().Contains("20"));

                if (var_empList.Any())
                {
                    DataTable dt_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                    BindGrid(dt_tmp);
                }
                divExpandLevel.Visible = true;




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void BindGrid(DataTable dt_data)
        {
            if (dt_data.Rows.Count > 0)
            {
                dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("YEAR1_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("YEAR1_BALANCE"))));
                dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("YEAR2_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("YEAR2_BALANCE"))));
                dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("YEAR3_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("YEAR3_BALANCE"))));
                dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("YEAR4_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("YEAR4_BALANCE"))));
                dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("YEAR5_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("YEAR5_BALANCE"))));
                dt_data.AcceptChanges();
            }

            gvData.DataSource = dt_data;
            gvData.DataBind();
            Session[FINSessionConstants.GridData] = dt_data;
            imgBtnExport.Visible = true;
            Reload_BS_Structure();

        }
        private void ShowGridColumns()
        {

            gvData.Columns[4].Visible = false;
            gvData.Columns[5].Visible = false;
            gvData.Columns[6].Visible = false;
            gvData.Columns[7].Visible = false;
            if (rbType.SelectedValue.ToString() == "Y")
            {
                gvData.Columns[3].HeaderText = DateTime.Now.Year.ToString();
                gvData.Columns[4].HeaderText = DateTime.Now.AddYears(-1).Year.ToString();
                gvData.Columns[5].HeaderText = DateTime.Now.AddYears(-2).Year.ToString();
                gvData.Columns[6].HeaderText = DateTime.Now.AddYears(-3).Year.ToString();
                gvData.Columns[7].HeaderText = DateTime.Now.AddYears(-4).Year.ToString();
            }
            else
            {
                gvData.Columns[3].HeaderText = rbQuarterType.SelectedValue.ToString() + " / " + DateTime.Now.Year.ToString();
                gvData.Columns[4].HeaderText = "12 / " + (DateTime.Now.Year -1 ).ToString();
                gvData.Columns[5].HeaderText = rbQuarterType.SelectedValue.ToString() + " / " + (DateTime.Now.Year-1).ToString();
                gvData.Columns[4].Visible = true;
                gvData.Columns[5].Visible = true;
            }
            switch (int.Parse(txtNoOfYear.Text))
            {
                case 2:
                    gvData.Columns[4].Visible = true;
                    break;
                case 3:
                    gvData.Columns[4].Visible = true;
                    gvData.Columns[5].Visible = true;
                    break;
                case 4:
                    gvData.Columns[4].Visible = true;
                    gvData.Columns[5].Visible = true;
                    gvData.Columns[6].Visible = true;
                    break;
                case 5:
                    gvData.Columns[4].Visible = true;
                    gvData.Columns[5].Visible = true;
                    gvData.Columns[6].Visible = true;
                    gvData.Columns[7].Visible = true;
                    break;
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl_tmp = e.Row.FindControl("lblAcct_grp_Name") as Label;
                lbl_tmp.Text = lbl_tmp.Text.Replace(" ", "&nbsp;");

                ImageButton Head_img_btn = (ImageButton)e.Row.FindControl("img_Det");
                if (gvData.DataKeys[e.Row.RowIndex]["B_TYPE"].ToString() != "EXPAND")
                {
                    Head_img_btn.CommandName = "MINUS";
                    Head_img_btn.ImageUrl = "../Images/collapse_blue.png";
                }
                else
                {
                    Head_img_btn.CommandName = "PLUS";
                    Head_img_btn.ImageUrl = "../Images/expand_blue.png";
                }
                if (gvData.DataKeys[e.Row.RowIndex]["GROUP_LEVEL"].ToString() == "0")
                {
                    Head_img_btn.Visible = false;
                }
            }

        }

        protected void img_Det_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            string Acct_grp_id = gvData.DataKeys[gvr.RowIndex].Values["ACCT_GRP_ID"].ToString();

            ImageButton Head_img_btn = (ImageButton)gvData.Rows[gvr.RowIndex].FindControl("img_Det");

            DataTable dt = (DataTable)Session["TemplateData"];
            DataTable dt_tmp = (DataTable)Session[FINSessionConstants.GridData];


            int row_index = gvr.RowIndex;
            if (Head_img_btn.CommandName == "PLUS")
            {
                dt_tmp.Rows[row_index][0] = "COLLAPSE";
            }
            else
            {
                dt_tmp.Rows[row_index][0] = "EXPAND";
            }
            if (Head_img_btn.CommandName == "PLUS")
            {
                var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(Acct_grp_id));

                if (var_empList.Any())
                {
                    DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                    for (int kLoop = 0; kLoop < dt_Group_tmp.Rows.Count; kLoop++)
                    {
                        row_index += 1;
                        DataRow dr = dt_tmp.NewRow();

                        for (int rloop = 0; rloop < dt_Group_tmp.Columns.Count; rloop++)
                        {
                            dr[rloop] = dt_Group_tmp.Rows[kLoop][rloop];
                        }

                        // dr = dt_Group_tmp.Rows[kLoop];
                        dt_tmp.Rows.InsertAt(dr, row_index);
                        // dt_tmp.Rows.Add(dr);

                    }


                }
            }
            else
            {
                row_index += 1;
                for (int kLoop = row_index; kLoop < dt_tmp.Rows.Count; kLoop++)
                {
                    //if ( dt_tmp.Rows[kLoop]["PARENT_GROUP_ID"].ToString() == Acct_grp_id)
                    if (Acct_grp_id.Contains (dt_tmp.Rows[kLoop]["PARENT_GROUP_ID"].ToString() ))
                    {
                        Acct_grp_id +="~" + dt_tmp.Rows[kLoop]["ACCT_GRP_ID"].ToString();
                        dt_tmp.Rows.RemoveAt(kLoop);
                        kLoop--;
                    }
                }
            }
            BindGrid(dt_tmp);




            //for (int iLoop = gvr.RowIndex + 1; iLoop < gvData.Rows.Count; iLoop++)
            //{
            //    if (gvData.DataKeys[iLoop].Values["PARENT_GROUP_ID"].ToString() == Acct_grp_id)
            //    {

            //        gvData.Rows[iLoop].Visible = !gvData.Rows[iLoop].Visible;

            //        if (gvData.DataKeys[iLoop].Values["GROUP_LEVEL"].ToString() == "0")
            //        {
            //            ImageButton img_btn = (ImageButton)gvData.Rows[iLoop].FindControl("img_Det");
            //            if (img_btn != null)
            //                img_btn.Visible = false;

            //        }
            //        //string str_Text = gvData.Rows[iLoop].Cells[1].Text ;
            //        //gvData.Rows[iLoop].Cells[1].Text = str_Text.PadLeft(str_Text.Length + 5 - int.Parse(gvData.DataKeys[iLoop].Values["GROUP_LEVEL"].ToString()), ' ');
            //    }
            //    else
            //    {
            //        return;
            //    }
            //}
        }



        private void Reload_BS_Structure()
        {
            try
            {
                DataTable dt_BS_TMPL_DATA = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_GroupTemplateStructure(ddlTemplateName.SelectedValue.ToString())).Tables[0];
                string str_ORG_STR = "";


                if (dt_BS_TMPL_DATA.Rows.Count > 0)
                {
                    DataTable dt_EmpProcessList = new DataTable();
                    dt_EmpProcessList.Columns.Add("GROUP_NAME");
                    dt_EmpProcessList.Columns.Add("BS_TMPL_ID");
                    dt_EmpProcessList.Columns.Add("BS_DISPLAY_NAME");

                    dt_EmpProcessList.Columns.Add("STATUS");

                    var var_HeadEmpList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_GROUP_NAME"].ToString().Contains("HEAD"));
                    if (var_HeadEmpList.Any())
                    {
                        DataTable dt_data = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);



                        string str_Replace_str = "";
                        string str_new_replace_str = "";
                        string str_tmp;
                        str_ORG_STR += "  <ul  id='BSTemp'  > ";
                        str_ORG_STR += "  <li><a onclick=fn_GroupAccClick('','" + ddlTemplateName.SelectedValue.ToString() + "')> " + ddlTemplateName.SelectedValue.ToString() + "</a>";
                        str_ORG_STR += " <ul>";
                        for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                        {
                            DataRow dr = dt_EmpProcessList.NewRow();
                            dr["GROUP_NAME"] = dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                            dr["BS_TMPL_ID"] = dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            dr["BS_DISPLAY_NAME"] = dt_data.Rows[iLoop]["BS_DISPLAY_NAME"].ToString();
                            dr["STATUS"] = "N";
                            dt_EmpProcessList.Rows.Add(dr);

                            str_ORG_STR += "<li>";
                            str_ORG_STR += dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            str_ORG_STR += "</li>";
                            DataRow[] dr_col;
                            do
                            {
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                                if (dr_col.Length > 0)
                                {
                                    str_Replace_str = "<li>";
                                    str_Replace_str += dr_col[0]["BS_TMPL_ID"].ToString();
                                    str_Replace_str += "</li>";
                                    str_new_replace_str = "<li> <a onclick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["BS_DISPLAY_NAME"].ToString().Replace(" ", "_") + "')> " + dr_col[0]["BS_DISPLAY_NAME"].ToString() + "</a>";
                                    str_new_replace_str += "<UL>";

                                    var var_empList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_ID"].ToString().Contains(dr_col[0]["BS_TMPL_ID"].ToString()));
                                    str_tmp = "";
                                    if (var_empList.Any())
                                    {
                                        DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                                        for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                                        {
                                            str_tmp += "<li>";
                                            str_tmp += dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            str_tmp += "</li>";

                                            dr = dt_EmpProcessList.NewRow();
                                            dr["GROUP_NAME"] = dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                                            dr["BS_TMPL_ID"] = dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            dr["BS_DISPLAY_NAME"] = dt_EMP_List.Rows[jLoop]["BS_DISPLAY_NAME"].ToString();
                                            dr["STATUS"] = "N";
                                            dt_EmpProcessList.Rows.Add(dr);

                                        }
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                                    }
                                    else
                                    {
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                                    }

                                    dr_col[0]["STATUS"] = "Y";
                                    dt_EmpProcessList.AcceptChanges();
                                }
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                            } while (dr_col.Length > 0);

                        }
                        str_ORG_STR += " </ul>";
                        str_ORG_STR += "  </li> ";
                        str_ORG_STR += "  </ul> ";

                    }
                }
                else
                {
                    str_ORG_STR += "  <ul  id='BSTemp'> ";
                    str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + ddlTemplateName.SelectedValue.ToString() + "</div>";
                    str_ORG_STR += "  </li> ";
                    str_ORG_STR += "  </ul> ";
                }

                div_Template.InnerHtml = str_ORG_STR;
                ScriptManager.RegisterStartupScript(btnView, typeof(Button), "LoadGraph", "fn_showGraph()", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        protected void imgBtnExport_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = Master.ReportName;


                ReportData = new DataSet();
                ReportData.Tables.Add(((DataTable)Session[FINSessionConstants.GridData]).Copy());


                htHeadingParameters.Add("Year1", DateTime.Now.Year.ToString());
                htHeadingParameters.Add("Year2", DateTime.Now.AddYears(-1).Year.ToString());
                htHeadingParameters.Add("Year3", DateTime.Now.AddYears(-2).Year.ToString());
                htHeadingParameters.Add("Year4", DateTime.Now.AddYears(-3).Year.ToString());
                htHeadingParameters.Add("Year5", DateTime.Now.AddYears(-4).Year.ToString());
                htHeadingParameters.Add("NoOfYear", txtNoOfYear.Text);
                htHeadingParameters.Add("ReportName", ddlTemplateName.SelectedItem.Text);



                VMVServices.Web.Utils.ReportFilterParameter = htHeadingParameters;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

                Reload_BS_Structure();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnTmpl_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["TemplateData"];
            DataTable dt_tmp = new DataTable();



            var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(hf_tmpl_id.Value.ToString()));

            if (var_empList.Any())
            {
                DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                dt_tmp = dt_Group_tmp;
            }
            var_empList = dt.AsEnumerable().Where(r => r["ACCT_GRP_ID"].ToString().Contains(hf_tmpl_id.Value.ToString()));

            if (var_empList.Any())
            {
                DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                DataRow dr = dt_tmp.NewRow();
                for (int rloop = 0; rloop < dt_Group_tmp.Columns.Count; rloop++)
                {
                    dr[rloop] = dt_Group_tmp.Rows[0][rloop];
                }
                dr[0] = "COLLAPSE";
                dt_tmp.Rows.InsertAt(dr, 0);

            }

            BindGrid(dt_tmp);


        }

        protected void txtExpandLevel_TextChanged(object sender, EventArgs e)
        {

            ShowRecord(0);


        }

        private void ShowRecord(int int_groupLevel)
        {

            DataTable dt = (DataTable)Session["TemplateData"];

            DataView dv_groupLevel = dt.DefaultView;
            dv_groupLevel.Sort = "GROUP_LEVEL DESC";
            DataTable dt_GroupLevel = dv_groupLevel.ToTable(true, "GROUP_LEVEL");

            if (int.Parse(txtExpandLevel.Text) > dt_GroupLevel.Rows.Count)
            {
                txtExpandLevel.Text = (dt_GroupLevel.Rows.Count).ToString();
            }


            DataTable dt_data = new DataTable();
            DataTable dt_BindData = new DataTable();
            DataTable dt_NextLevelData = new DataTable();

            DataTable dt_tmp_bindData = new DataTable();

            var var_empList = dt.AsEnumerable().Where(r => r["GROUP_LEVEL"].ToString().Contains("20"));

            if (var_empList.Any())
            {
                DataTable dt_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                dt_data = dt_tmp.Copy();

                dt_BindData = dt_tmp.Copy();


                dt_NextLevelData = dt_tmp.Copy();


                for (int i = 1; i <= int.Parse(txtExpandLevel.Text); i++)
                {

                    dt_data = dt_NextLevelData.Copy();
                    dt_NextLevelData.Rows.Clear();


                    for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                    {
                        dt_tmp_bindData = dt_BindData.Copy();


                        var var_selAcct_GrpData = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString()) && int.Parse(r["GROUP_LEVEL"].ToString()) > int_groupLevel);
                        if (var_selAcct_GrpData.Any())
                        {
                            dt_BindData.Rows.Clear();
                            DataTable dt_sel_acct_data = System.Data.DataTableExtensions.CopyToDataTable(var_selAcct_GrpData);
                            DataRow dr;
                            for (int k = 0; k < dt_tmp_bindData.Rows.Count; k++)
                            {

                                dr = dt_BindData.NewRow();
                                for (int rloop = 0; rloop < dt_tmp_bindData.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_tmp_bindData.Rows[k][rloop];
                                }

                                dt_BindData.Rows.Add(dr);
                                if (dt_tmp_bindData.Rows[k]["ACCT_GRP_ID"].ToString() == dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString())
                                {
                                    dt_BindData.Rows[dt_BindData.Rows.Count - 1][0] = "COLLAPSE";
                                    break;
                                }
                            }

                            for (int kLoop = 0; kLoop < dt_sel_acct_data.Rows.Count; kLoop++)
                            {
                                dr = dt_BindData.NewRow();
                                for (int rloop = 0; rloop < dt_sel_acct_data.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_sel_acct_data.Rows[kLoop][rloop];
                                }
                                dt_BindData.Rows.Add(dr);
                            }

                            for (int kLoop = 0; kLoop < dt_sel_acct_data.Rows.Count; kLoop++)
                            {
                                dr = dt_NextLevelData.NewRow();
                                for (int rloop = 0; rloop < dt_sel_acct_data.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_sel_acct_data.Rows[kLoop][rloop];
                                }
                                dt_NextLevelData.Rows.Add(dr);
                            }


                            bool bol_addData = false;
                            for (int k = 0; k < dt_tmp_bindData.Rows.Count; k++)
                            {
                                if (bol_addData)
                                {
                                    dr = dt_BindData.NewRow();
                                    for (int rloop = 0; rloop < dt_tmp_bindData.Columns.Count; rloop++)
                                    {
                                        dr[rloop] = dt_tmp_bindData.Rows[k][rloop];
                                    }
                                    dt_BindData.Rows.Add(dr);
                                }
                                if (dt_tmp_bindData.Rows[k]["ACCT_GRP_ID"].ToString() == dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString())
                                {
                                    bol_addData = true;
                                }
                            }

                        }
                    }
                }
                BindGrid(dt_BindData);
            }
        }

        protected void btnExpandAll_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)Session["TemplateData"];

            DataView dv_groupLevel = dt.DefaultView;
            dv_groupLevel.Sort = "GROUP_LEVEL DESC";
            DataTable dt_GroupLevel = dv_groupLevel.ToTable(true, "GROUP_LEVEL");
            txtExpandLevel.Text = (dt_GroupLevel.Rows.Count).ToString();
            ShowRecord(-1);
        }

        protected void rbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            showYearlyQuartly();
        }
        private void showYearlyQuartly()
        {
            if (rbType.SelectedValue.ToString() == "Y")
            {
                divYearly.Visible = true;
                div_Quartly.Visible = false;
                txtNoOfYear.Text = "1";
            }
            else
            {
                divYearly.Visible = false;
                div_Quartly.Visible = true;
            }
        }

        

    }
}