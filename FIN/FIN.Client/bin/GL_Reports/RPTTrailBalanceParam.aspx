﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTTrailBalanceParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTTrailBalanceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="div1">
        <div class="divRowContainer" style="display:none;">
            <div class="lblBox LNOrient" style="  width: 150px;display:none;" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlFromAcctGroup" runat="server" TabIndex="2" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlToAcctGroup" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div8">
                Debit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtDebitFromAmt" runat="server" TabIndex="4" CssClass="EntryFont txtBox_N"
                    Width="200px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtDebitFromAmt" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div9">
                Debit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtDebitToAmt" runat="server" TabIndex="5" CssClass="EntryFont txtBox_N"
                    Width="200px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtDebitToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div10">
                Credit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtCreditFromAmt" runat="server" TabIndex="6" CssClass="EntryFont txtBox_N"
                    Width="200px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtCreditFromAmt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div11">
                Credit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtCreditToAmt" runat="server" TabIndex="7" CssClass="EntryFont txtBox_N"
                    Width="200px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtCreditToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtDate" runat="server" TabIndex="8" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div6">
                Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" TabIndex="9" Checked="false" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="10" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
