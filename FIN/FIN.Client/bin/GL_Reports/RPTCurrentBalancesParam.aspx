﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTCurrentBalancesParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTCurrentBalancesParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="div1">
        <div class="divRowContainer" runat="server" visible="false" id="divglob">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 455px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" validate[required]  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="divPeriod">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblPeriodName">
                Period Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 455px">
                <asp:DropDownList ID="ddlPeriodName" runat="server" TabIndex="2" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="3" CssClass="txtBox"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToAccount" runat="server" TabIndex="4" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div8">
                From Debit Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromDebitAmt" runat="server" TabIndex="5" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".,"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtFromDebitAmt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div9">
                To Debit Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDebitToAmt" runat="server" TabIndex="6" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".,"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtDebitToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                From Credit Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromCreditAmt" runat="server" TabIndex="7" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".,"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtFromCreditAmt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div5">
                To Credit Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToCreditAmt" runat="server" TabIndex="8" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".,"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtToCreditAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="9" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="10" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn"  />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
