﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTFinancialGroupSummaryParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        string fromDate = string.Empty;
        string toDate = string.Empty;
        string firstHalfYearlyToDate = string.Empty;
        string secondHalfYearlyFromDate = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }

        private void getHalfYearlyDate()
        {
            string calDtID = ddlFinancialYear.SelectedValue;
            DataTable dtDate = new DataTable();
            if (calDtID != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getHalfYearlyDate(calDtID)).Tables[0];
                fromDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                toDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
                firstHalfYearlyToDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["firstHalfYearly_to_date"].ToString());
                secondHalfYearlyFromDate = DBMethod.ConvertDateToString(dtDate.Rows[0]["secondHalfYearly_from_date"].ToString());
            }
        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmt.Text, txtToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void ddlAccoutStart_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAcctStartWith.SelectedValue.ToString().Length > 0)
            {
                AccountCodes_BLL.getAccCodeBasedStartupLetter(ref ddlFromAccNumber, ddlAcctStartWith.SelectedItem.Text);
                AccountCodes_BLL.getAccCodeBasedStartupLetter(ref ddlToAccNumber, ddlAcctStartWith.SelectedItem.Text);

            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbSelectMonths.Items[0].Text = Prop_File_Data["First_Six_Months_P"];
                    rbSelectMonths.Items[1].Text = Prop_File_Data["Last_Six_Months_P"];

                  
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedValue);
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                }
                if (ddlFinancialYear.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CAL_DTL_ID", ddlFinancialYear.SelectedValue);
                    htFilterParameter.Add("YEAR_NAME", ddlFinancialYear.SelectedItem.Text);
                }
                if (ddlAcctStartWith.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("ACCOUNT_START", ddlAcctStartWith.SelectedValue.ToString());
                    htFilterParameter.Add("ACCOUNT_START_WITH", ddlAcctStartWith.SelectedItem.Text);

                }
                getHalfYearlyDate();
                if (rbSelectMonths.SelectedValue == "1")
                {
                    htFilterParameter.Add("From_Date", fromDate);
                    htFilterParameter.Add("firstHalfYearly_to_date", firstHalfYearlyToDate);
                }

                if (rbSelectMonths.SelectedValue == "2")
                {
                    htFilterParameter.Add("secondHalfYearly_from_date", secondHalfYearlyFromDate);
                    htFilterParameter.Add("To_Date", toDate);
                }
                /*if (txtFromAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                }

                if (txtToAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                }*/


                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedItem.Text);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedItem.Text);
                }

                if (txtFromAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Amt", txtFromAmt.Text);
                }
                if (txtToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Amt", txtToAmt.Text);
                }
                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Date", txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}
                if (ddlAccountGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ACCOUNTGROUP", ddlAccountGroup.SelectedValue);
                    htFilterParameter.Add("ACCOUNTGROUPNAME", ddlAccountGroup.SelectedItem.Text);
                }
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                //htFilterParameter.Add("ReportName", Session["ProgramName"].ToString());
                //htFilterParameter.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                string str_Unpost = "0";
                if (chkUnPost.Checked)
                {
                    str_Unpost = "1";
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                string str_GroupId = "ALL";
                if (ddlAccountGroup.SelectedValue.ToString().Trim().Length > 0)
                {
                    str_GroupId = ddlAccountGroup.SelectedValue;
                }
                if (rbSelectMonths.SelectedValue == "1")
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountMonthlySummary_DAL.getFinancialAcctGroupSummary("Q1", str_Unpost));
                }
                else if (rbSelectMonths.SelectedValue == "2")
                {
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountMonthlySummary_DAL.getFinancialAcctGroupSummary("Q2", str_Unpost));
                }

                //ReportFormulaParameter = htHeadingParameters;

                //htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                //htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                //if (ddlGlobalSegment.SelectedValue != string.Empty)
                //{
                //    htHeadingParameters.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Text);
                //}
                //if (txtDate.Text != string.Empty)
                //{
                //    htHeadingParameters.Add("AccountSummary_Date", txtDate.Text);
                //}

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            ddlGlobalSegment.Items.RemoveAt(0);

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlAccountGroup, false);
            FIN.BLL.GL.AccountCodes_BLL.fn_geAccountStartWith(ref ddlAcctStartWith, false);
            
        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}