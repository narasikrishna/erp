﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using System.IO;
using Novacode;
using System.Diagnostics;
using System.Net;

using Microsoft.Office.Interop.Word;
using VMVServices.Services.Data;
namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpListDOA : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        public int doc_tablecount;
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListDOAReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListDOAReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            System.Data.DataTable dtDate = new System.Data.DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));


            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddlEmpName.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmpName.SelectedValue);
                }
                if (ddlDept.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("DEPT_ID", ddlDept.SelectedValue);
                }
             

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.HR.Payslip_BLL.GetEmpListDOA();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;



                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



                //doc_tablecount = 0;

                //string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
                //string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

                //string HRReportEN = HRReportPath.ToString() + "EMPListDOA_EN.docx";
                //string HRReportAR = HRReportPath.ToString() + "EMPListDOA_AR.docx";

                //string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
                //string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + "HR_EMPListDOA_" + formatted + ".docx";

                //switch (Session[FINSessionConstants.Sel_Lng].ToString())
                //{
                //    case "AR":
                //        System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
                //        System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
                //        break;
                //    default:
                //        System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
                //        System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
                //        break;
                //}

                //using (DocX document = DocX.Load(saveHRTemplateAs))
                //{
                //    document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));

                //    Novacode.Table tbl_LP = document.Tables[doc_tablecount];
                //    DataSet dsData = new DataSet();
                //    dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmpDetailsDOJ(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
                //    for (int iLoop = 0; iLoop < dsData.Tables[0].Rows.Count; iLoop++)
                //    {
                //        tbl_LP.InsertRow();
                //        if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
                //        {
                //            tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["nationality"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_doj"].ToString());
                //        }
                //        else if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
                //        {
                //            tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_doj"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
                //            tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["nationality"].ToString());
                //        }
                //    }
                //    document.Tables[doc_tablecount] = tbl_LP;

                //    ////Insert a new Table
                //    //Novacode.Table tmp = document.InsertTable(1, 1);
                //    //tmp.Rows[0].Cells[0].Paragraphs.First().Append("GROUP2");
                //    //tmp.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.right;
                //    //document.InsertTable(tbl_LP);

                //    // Save all changes made to this document.
                //    document.Save();

                //    Session["FileName"] = Path.GetFileName(saveHRTemplateAs);
                //    Session["OnlyFileName"] = Path.GetFileNameWithoutExtension(saveHRTemplateAs);

                //    string linkToDownload = "../TmpWordDocs/" + Session["FileName"];
                //    string linkToDownloadAsPdf = "../TmpWordDocs/" + Session["OnlyFileName"] + ".pdf";

                //    ////Word to PDF Conversion
                //    //Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
                //    //object oMissing = System.Reflection.Missing.Value;

                //    //word.Visible = false;
                //    //word.ScreenUpdating = false;

                //    //Object filename = Server.MapPath(linkToDownload);
                //    //Document doc = word.Documents.Open(ref filename, ref oMissing,
                //    //               ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //    //               ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //    //               ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                //    //doc.Activate();
                //    //object outputFileName = Server.MapPath(linkToDownloadAsPdf);
                //    //object fileFormat = WdSaveFormat.wdFormatPDF;

                //    //doc.SaveAs(ref outputFileName,
                //    //           ref fileFormat, ref oMissing, ref oMissing,
                //    //           ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //    //           ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                //    //           ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                //    //object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
                //    //((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
                //    //doc = null;

                //    //((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
                //    //word = null;
                //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                ////    ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownloadAsPdf + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                //    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsWord", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListDOAReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
           
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDept, false);
           
            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);

        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept_R(ref ddlEmpName,ddlDept.SelectedValue);
        }
    }
}