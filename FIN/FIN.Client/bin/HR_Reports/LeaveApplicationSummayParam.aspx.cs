﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMVServices.Services.Data;
using VMVServices.Web;
using System.Collections;
using FIN.BLL;
using FIN.DAL;
namespace FIN.Client.HR_Reports
{
    public partial class LeaveApplicationSummayParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                FillStartDate();
                Startup();
                FillComboBox();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment, true);
            Lookup_BLL.GetLookUpValues(ref ddlLeaveStatus, "SS");
            FIN.BLL.HR.LeaveDefinition_BLL.GetLeave(ref ddlLeaveType);
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;



                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }

                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                if (ddlDepartment.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("DEPT_ID", ddlDepartment.SelectedValue);
                    htFilterParameter.Add("DEPT_NAME", ddlDepartment.SelectedItem.Text);
                }
                if (ddlEmployeeName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmployeeName.SelectedItem.Value);
                }
                if (ddlLeaveType.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("LEAVE_TYPE", ddlLeaveType .SelectedValue);
                }

                if (ddlLeaveStatus.SelectedValue.ToString().Trim().Length > 0)
                {
                    htFilterParameter.Add("LEAVE_STATUS", ddlLeaveStatus.SelectedValue);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                
                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.getEmpLeaveApplicationSummary()); 
                

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept_R(ref ddlEmployeeName, ddlDepartment.SelectedValue);
        }




    }
}