﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using FIN.BLL.PER;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL.HR;
namespace FIN.Client.HR_Reports
{
    public partial class RPTBankTransformation : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRBankTransf", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_HRBankTransf", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPeriod,false);
            //ddlPeriod.Items.RemoveAt(0);
            //ddlPeriod.Items.Insert(0, new ListItem("All", ""));
            //int year = DateTime.Now.Year;
            //for (int i = 1995; i <= year; i++)
            //{
            //    ddlYear.Items.Add(i.ToString());
            //}
            //ddlYear.SelectedItem.Text = DateTime.Now.Year.ToString();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            //if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
            //{
            //    Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName_OL.ToString()]));
            //}

            //Session["ReportName"] = Master.ReportName;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;
                if (ddlPeriod.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("PAY_PERIOD_ID", ddlPeriod.SelectedItem.Value);
                }

                //if (int.Parse(ddlYear.SelectedValue.ToString()) > 0)
                //{
                //    htFilterParameter.Add("tran_year", ddlYear.SelectedValue.ToString());
                //}

                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_FROM_DT, txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_TO_DT, txtToDate.Text);
                //}

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                ReportData = DBMethod.ExecuteQuery(EmployeeProfile_DAL.GetRPTBankTransformation());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BankTransformationReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        //protected void btnSave_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        doc_tablecount = 0;

        //        string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
        //        string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

        //        string HRReportEN = HRReportPath.ToString() + "LeaveBalance_EN.docx";
        //        string HRReportAR = HRReportPath.ToString() + "LeaveBalance_AR.docx";

        //        string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
        //        string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + "HR_LeaveBalanceReport" + formatted + ".docx";

        //        switch (Session[FINSessionConstants.Sel_Lng].ToString())
        //        {
        //            case "AR":
        //                System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //            default:
        //                System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //        }

        //        using (DocX document = DocX.Load(saveHRTemplateAs))
        //        {
        //            document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));

        //            Novacode.Table tbl_LP = document.Tables[doc_tablecount];
        //            DataSet dsData = new DataSet();
        //            dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeaveLedgerData(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
        //            for (int iLoop = 0; iLoop < dsData.Tables[0].Rows.Count; iLoop++)
        //            {
        //                tbl_LP.InsertRow();
        //                if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //                {

        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                }
        //                else if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //                {
        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_no"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["emp_name"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["LEAVE_DESC"].ToString());                          
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["ldr_leave_bal"].ToString());
        //                }
        //            }

        //            document.Tables[doc_tablecount] = tbl_LP;

        //            // Save all changes made to this document.
        //            document.Save();

        //            Session["FileName"] = Path.GetFileName(saveHRTemplateAs);
        //            Session["OnlyFileName"] = Path.GetFileNameWithoutExtension(saveHRTemplateAs);

        //            string linkToDownload = "../TmpWordDocs/" + Session["FileName"];

        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("SaveHREmpListEOS", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }
        //}
    }
}