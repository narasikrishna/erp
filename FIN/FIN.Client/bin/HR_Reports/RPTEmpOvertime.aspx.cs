﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

using System.Diagnostics;
using System.Net;
using System.IO;
using FIN.BLL;
using FIN.DAL;
using Microsoft.Office.Interop.Word;

namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpOvertime : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        public int doc_tablecount;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListOT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillStartDate();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_HREmpListOT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            System.Data.DataTable dtDate = new System.Data.DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.getEmpOvertime());

                //htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpOverTimeReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        doc_tablecount = 0;

        //        string HRReportPath = System.Web.Configuration.WebConfigurationManager.AppSettings["HRReportsTemplate"].ToString();
        //        string TmpFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString();

        //        string HRReportEN = HRReportPath.ToString() + "EMPListOT_EN.docx";
        //        string HRReportAR = HRReportPath.ToString() + "EMPListOT_AR.docx";

        //        string formatted = DateTime.Now.ToString("MMddyyyyHHmmssfff");
        //        string saveHRTemplateAs = System.Web.Configuration.WebConfigurationManager.AppSettings["TmpWordDocFolder"].ToString() + "HR_EMPListOT_" + formatted + ".docx";

        //        switch (Session[FINSessionConstants.Sel_Lng].ToString())
        //        {
        //            case "AR":
        //                System.IO.File.Copy(HRReportAR, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //            default:
        //                System.IO.File.Copy(HRReportEN, saveHRTemplateAs);
        //                System.IO.File.SetAttributes(saveHRTemplateAs, FileAttributes.Normal);
        //                break;
        //        }

        //        using (DocX document = DocX.Load(saveHRTemplateAs))
        //        {
        //            DateTime fromDate = new DateTime(); 
        //            DateTime toDate = new DateTime();

        //            if (txtFromDate.Text.ToString() != null && txtFromDate.Text.ToString().Length > 0)
        //                fromDate = Convert.ToDateTime(txtFromDate.Text.ToString());
        //            if (txtToDate.Text.ToString() != null && txtToDate.Text.ToString().Length > 0)
        //                toDate = Convert.ToDateTime(txtToDate.Text.ToString());

        //            document.ReplaceText("__date__", DateTime.Now.ToString("dd/MM/yyyy"));
        //            document.ReplaceText("__monthname__", toDate.Month.ToString("MMM"));

        //            Novacode.Table tbl_LP = document.Tables[doc_tablecount];
        //            DataSet dsData = new DataSet();
        //            dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.OverTimeSelection_DAL.GetOverTimeDtls(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
        //            for (int iLoop = 0; iLoop < dsData.Tables[0].Rows.Count; iLoop++)
        //            {
        //                DateTime getMonthYear = DateTime.Parse(dsData.Tables[0].Rows[iLoop]["OVERTIME_DATE"].ToString());
        //                string monthYr = string.Empty;
        //                monthYr = getMonthYear.Month.ToString() + " / " + getMonthYear.Year.ToString();

        //                tbl_LP.InsertRow();
        //                if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //                {
        //                    monthYr = getMonthYear.Year.ToString() + " / " + getMonthYear.Month.ToString();
        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["Overtime_Paid"].ToString() + VMVServices.Web.Utils.DecimalPrecision);
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(monthYr.ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["OVERTIME_HOURS"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["OT_TYPE"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[4].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["basic_salary"].ToString() + VMVServices.Web.Utils.DecimalPrecision);
        //                    tbl_LP.Rows[iLoop + 1].Cells[5].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["EMP_FIRST_NAME"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[6].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["EMP_ID"].ToString());
        //                }
        //                else if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //                {
        //                    tbl_LP.Rows[iLoop + 1].Cells[0].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["EMP_ID"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[1].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["EMP_FIRST_NAME"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[2].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["basic_salary"].ToString() + VMVServices.Web.Utils.DecimalPrecision);
        //                    tbl_LP.Rows[iLoop + 1].Cells[3].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["OT_TYPE"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[4].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["OVERTIME_HOURS"].ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[5].Paragraphs.First().Append(monthYr.ToString());
        //                    tbl_LP.Rows[iLoop + 1].Cells[6].Paragraphs.First().Append(dsData.Tables[0].Rows[iLoop]["Overtime_Paid"].ToString() + VMVServices.Web.Utils.DecimalPrecision);
        //                }
        //            }
        //            document.Tables[doc_tablecount] = tbl_LP;

        //            ////Insert a new Table
        //            //Novacode.Table tmp = document.InsertTable(1, 1);
        //            //tmp.Rows[0].Cells[0].Paragraphs.First().Append("GROUP2");
        //            //tmp.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.right;
        //            //document.InsertTable(tbl_LP);
                    
        //            // Save all changes made to this document.
        //            document.Save();

        //            Session["FileName"] = Path.GetFileName(saveHRTemplateAs);
        //            Session["OnlyFileName"] = Path.GetFileNameWithoutExtension(saveHRTemplateAs);

        //            string linkToDownload = "../TmpWordDocs/" + Session["FileName"];
        //            string linkToDownloadAsPdf = "../TmpWordDocs/" + Session["OnlyFileName"] + ".pdf";

        //            //Word to PDF Conversion
        //            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
        //            object oMissing = System.Reflection.Missing.Value;

        //            ////Inserting page numbers in page footer
        //            //word.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekCurrentPageFooter;
        //            //word.Selection.TypeParagraph();

        //            ////String docNumber = "1";
        //            ////String revisionNumber = "0";

        //            ////INSERTING THE PAGE NUMBERS CENTRALLY ALIGNED IN THE PAGE FOOTER
        //            //word.Selection.Paragraphs.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
        //            //word.ActiveWindow.Selection.Font.Name = "Arial";
        //            //word.ActiveWindow.Selection.Font.Size = 8;
        //            ////word.ActiveWindow.Selection.TypeText("Document #: " + docNumber + " - Revision #: " + revisionNumber);

        //            ////INSERTING TAB CHARACTERS
        //            //word.ActiveWindow.Selection.TypeText("\t");
        //            //word.ActiveWindow.Selection.TypeText("\t");

        //            //if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //            //{
        //            //    word.ActiveWindow.Selection.TypeText("Page ");
        //            //}
        //            //else if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //            //{
        //            //    word.ActiveWindow.Selection.TypeText("صفحة ");
        //            //}
        //            //Object CurrentPage = Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage;
        //            //word.ActiveWindow.Selection.Fields.Add(word.Selection.Range, ref CurrentPage, ref oMissing, ref oMissing);

        //            //if (Session[FINSessionConstants.Sel_Lng].ToString() == "EN")
        //            //{
        //            //    word.ActiveWindow.Selection.TypeText(" of ");
        //            //}
        //            //else if (Session[FINSessionConstants.Sel_Lng].ToString() == "AR")
        //            //{
        //            //    word.ActiveWindow.Selection.TypeText(" من ");
        //            //}
                    
        //            //Object TotalPages = Microsoft.Office.Interop.Word.WdFieldType.wdFieldNumPages;
        //            //word.ActiveWindow.Selection.Fields.Add(word.Selection.Range, ref TotalPages, ref oMissing, ref oMissing);

        //            ////SETTING FOCUES BACK TO DOCUMENT
        //            //word.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekMainDocument;


        //            word.Visible = false;
        //            word.ScreenUpdating = false;

        //            Object filename = Server.MapPath(linkToDownload);
        //            Document doc = word.Documents.Open(ref filename, ref oMissing,
        //                           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                           ref oMissing, ref oMissing, ref oMissing, ref oMissing);
        //            doc.Activate();
        //            object outputFileName = Server.MapPath(linkToDownloadAsPdf);
        //            object fileFormat = WdSaveFormat.wdFormatPDF;

        //            doc.SaveAs(ref outputFileName,
        //                       ref fileFormat, ref oMissing, ref oMissing,
        //                       ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                       ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                       ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        //            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
        //            ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
        //            doc = null;

        //            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
        //            word = null;

        //            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownloadAsPdf + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsWord", "window.open('" + linkToDownload + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("SaveHREmpListOT", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }
        //}
    }
}