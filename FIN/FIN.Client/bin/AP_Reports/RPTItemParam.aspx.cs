﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.Reports.AP
{
    public partial class RPTItemParam1 : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();       

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtCreatedFromDate.Text, txtCreatedToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtModifiedFromDate.Text, txtModToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlItemType.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add(FINColumnConstants.ITEM_TYPE,ddlItemType.SelectedValue);
                }
                if (ddlItemCategory.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("ITEM_CAT_NAME", ddlItemCategory.SelectedValue);
                }
                if (ddlItemUOM.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("ITEM_UOM", ddlItemUOM.SelectedValue);
                }
                if (ddlItemGroup.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("ITEM_GROUP_NAME", ddlItemGroup.SelectedValue);
                }
                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtCreatedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtCreatedFromDate.Text);
                }
                if (txtCreatedToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtCreatedToDate.Text);
                }
                if (txtModifiedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtModifiedFromDate.Text);
                }
                if (txtModToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtModToDate.Text);
                }



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.Item_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            Item_BLL.GetItemType(ref ddlItemType);
            Item_BLL.GetItemCategory(ref ddlItemCategory);
            Item_BLL.GetItemUom(ref ddlItemUOM);
            Item_BLL.GetItemGroupName(ref ddlItemGroup);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}