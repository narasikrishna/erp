﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTCrystalReportViewer.aspx.cs"
    Inherits="FIN.Client.RPTCrystalReportViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    	  <%--<script src="Scripts/crystalreportviewers13/JS/crviewer/crv.js" type="text/javascript"></script>--%>
         
    <style>
        REPORTBODY
        {
            padding-right: 0px;
            padding-left: 0px;
            font-size: 11px;
            background: url(Images/bodyBg.gif) #4396CA repeat-x left top;
            padding-bottom: 0px;
            margin: 0px;
            color: #ffffff;
            padding-top: 0px;
            font-family: Arial, Helvetica, sans-serif;
            border: 0px;
            font-weight: normal;
        }
        .reportMainTitle
        {
            font-weight: bold;
            font-size: 14px;
            color: #333333;
            font-family: Verdana;
            text-align: center;
        }
        .reportFooterTitle
        {
            font-weight: lighter;
            font-size: 13px;
            color: #999999;
            font-family: Verdana;
            text-align: center;
        }
    </style>
</head>
<body class="REPORTBODY">
    <form id="form1" runat="server">
    <div>
        <table width="100%" bgcolor="white" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" HasDrillUpButton="False"
                        AutoDataBind="True" Height="1039px" Width="901px" ToolPanelView="None" />
                </td>
            </tr>
            <tr id="trNoRecords" runat="server">
                <td align="center" height="50">
                    <span class="reportMainTitle">No Records to display</span>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
