﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTSalesRegisterParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTSalesRegisterParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox" style="float: left; width: 200px" id="Div12">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 237px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="6" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierNumber">
                From Customer
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromCustNumber" runat="server" TabIndex="1" CssClass=" RequiredField ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                To Customer
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToCustNumber" runat="server" TabIndex="2" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblInvoiceNumber">
                From Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlFromInvoiceNumber" runat="server" TabIndex="3" CssClass="  RequiredField ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div5">
                To Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToInvoiceNumber" runat="server" TabIndex="4" CssClass="  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                From Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromOrderNumber" runat="server" TabIndex="5" CssClass=" RequiredField ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div4">
                To Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToOrderNumber" runat="server" TabIndex="6" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div7">
                Transaction Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlTrnCurrency" runat="server" TabIndex="7" CssClass=" RequiredField ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div8">
                Transaction Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlTrancType" runat="server" TabIndex="8" CssClass=" RequiredField ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div6">
                Transaction From Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtFromTrnAmount" runat="server" TabIndex="9" Width="150px" CssClass="   txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtFromTrnAmount" />
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                    Transaction To Amount
                </div>
                <div class="divtxtBox LNOrient" style="width: 250px">
                    <asp:TextBox ID="txtToTrnAmount" runat="server" TabIndex="10" Width="150px" CssClass="  txtBox_N"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".,-" TargetControlID="txtToTrnAmount" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                From Amount in Functional Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtFromFunAmount" runat="server" TabIndex="11" Width="150px" CssClass=" txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtFromFunAmount" />
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblToDate">
                    To Amount in Functional Currency
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtToFunAmount" runat="server" TabIndex="12" CssClass=" txtBox_N"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".,-" TargetControlID="txtToFunAmount" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div10">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtFromDate" runat="server" Width="150px" TabIndex="13" CssClass=" RequiredField validate[custom[ReqDateDDMMYYY]]   txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div11">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="14" CssClass="validate[custom[ReqDateDDMMYYY]] RequiredField   txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            <%-- <asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
<script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
