﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTSalesOrderParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {

                    if (ddlFromSalesOrderNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("FROM_ORDER_ID", ddlFromSalesOrderNumber.SelectedItem.Text);
                    }
                    else
                    {
                        htFilterParameter.Add("FROM_ORDER", ddlFromSalesOrderNumber.SelectedItem.Text);
                    }

                    if (ddlToSalesOrderNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("TO_ORDER_ID", ddlToSalesOrderNumber.SelectedItem.Text);
                    }
                    else
                    {
                        htFilterParameter.Add("TO_ORDER", ddlToSalesOrderNumber.SelectedItem.Text);
                    }
                  
                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }

                    if (ddlFromCustNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("CUST_FROM", ddlFromCustNumber.SelectedItem.Value);
                        htFilterParameter.Add("CUST_FROM_NAME", ddlFromCustNumber.SelectedItem.Text);
                    }
                    else
                    {
                        htFilterParameter.Add("CUST_FROM_NAME", ddlFromCustNumber.SelectedItem.Text);
                    }
                    if (ddlToCustNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("CUST_TO", ddlToCustNumber.SelectedItem.Value);
                        htFilterParameter.Add("CUST_TO_NAME", ddlToCustNumber.SelectedItem.Text);
                    }
                    else
                    {
                        htFilterParameter.Add("CUST_TO_NAME", ddlToCustNumber.SelectedItem.Text);
                    }
                    if (txtFromAmount.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Amt", txtFromAmount.Text);
                    }
                    if (txtToAmount.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Amt", txtToAmount.Text);
                    }


                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                    ReportData = FIN.BLL.AR.SalesOrder_BLL.GetReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }
                //else
                //{
                //    ErrorCollection.Add("datddemsg", "From Date and/or To Date cannot be empty");
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            FIN.BLL.AR.SalesOrder_BLL.fn_GetSalesOrderNo(ref ddlFromSalesOrderNumber);
            FIN.BLL.AR.SalesOrder_BLL.fn_GetSalesOrderNo(ref ddlToSalesOrderNumber);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlFromCustNumber);
            FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlToCustNumber);

        }

    }
}