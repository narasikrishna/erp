﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTSalesOrderParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTSalesOrderParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblGlobalSegment">
               From Sales Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromSalesOrderNumber" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
               To Sales Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToSalesOrderNumber" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierNumber">
            From Customer Number
        </div>
        <div class="divtxtBox LNOrient" style="width: 250px">
            <asp:DropDownList ID="ddlFromCustNumber" runat="server" TabIndex="2" CssClass="  ddlStype">
            </asp:DropDownList>
        </div>
        <div class="lblBox LNOrient" style="width: 200px" id="Div2">
            To Customer Number
        </div>
        <div class="divtxtBox LNOrient" style="width: 250px">
            <asp:DropDownList ID="ddlToCustNumber" runat="server" TabIndex="3" CssClass="  ddlStype">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div6">
            From Amount
        </div>
        <div class="divtxtBox LNOrient" style="width: 250px">
            <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="9" Width="150px" CssClass="txtBox_N"></asp:TextBox>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                ValidChars=".,-" TargetControlID="txtFromAmount" />
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                To Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="10" Width="150px" CssClass="  txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtToAmount" />
            </div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
            From Date
        </div>
        <div class="divtxtBox LNOrient" style="width: 250px">
            <asp:TextBox ID="txtFromDate" runat="server" Width="150px" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                OnClientDateSelectionChanged="checkDate" />
        </div>
        <div class="lblBox LNOrient" style="width: 200px" id="lblToDate">
            To Date
        </div>
        <div class="divtxtBox LNOrient" style="width: 150px">
            <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                OnClientDateSelectionChanged="checkDate" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                        <%-- <asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
