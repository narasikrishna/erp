﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTInvoiceRegisterParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTInvoiceRegisterParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceNumber">
                From Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromInvoiceNumber" runat="server" TabIndex="1" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                To Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToInvoiceNumber" runat="server" TabIndex="2" CssClass="  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCustomerName">
                From Customer
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlFromCustomer" runat="server" TabIndex="3" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceType">
                To Customer
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlToCustomer" runat="server" TabIndex="4" CssClass=" RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px;" id="Div5">
                From Invoice Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtInvoiceFromAmount" runat="server" TabIndex="5" MaxLength="13"
                    CssClass="  txtBox_N" Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtInvoiceFromAmount" />
            </div>
            <div class="colspace LNOrient" style="width: 20px;">
            </div>
            <div class="lblBox LNOrient" style="width: 150px;" id="Div6">
                To Invoice Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtInvoiceToAmount" runat="server" TabIndex="6" MaxLength="13" CssClass="txtBox_N"
                    Width="150px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtInvoiceToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 150px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 250px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="7" Width="150px" CssClass=" validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="colspace LNOrient" style="width: 20px;">
                </div>
                <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="8" CssClass="validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 120px" id="Div14">
                    Invoice Status
                </div>
                <div class="divtxtBox LNOrient" style="width: 550px" align="center">
                    <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                        <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="C">Cancelled &nbsp; &nbsp; &nbsp; </asp:ListItem>
                        <asp:ListItem Value="IU">All &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td style="width: 200px">
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    Width="35px" Height="25px" OnClick="btnSave_Click" />
                                <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                            </td>
                            <td>
                                <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
