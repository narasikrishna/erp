﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTDayBookRemittanceParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTDayBookRemittanceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlBankName" TabIndex="1" runat="server" AutoPostBack="True"
                    CssClass="ddlStype" 
                    onselectedindexchanged="ddlBankName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" 
                    Width="150px" CssClass="ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                            &nbsp</div>
             <div class="lblBox  LNOrient" style="width: 150px" id="lblBankShortName">
                Remittance Transaction Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
               <asp:DropDownList ID="ddlTransacType" runat="server" AutoPostBack="True" 
                    Width="150px" CssClass="ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
          
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblEffectiveDate">
                From Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromAmount" CssClass="txtBox_N" runat="server" TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtFromAmount" />
            </div>
           <div class="colspace  LNOrient" >
                            &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 150px" id="Div3">
                To Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToAmount" CssClass="txtBox_N" runat="server" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                            &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td style="float: left; width: 120px">
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
