﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LanguageEntry.aspx.cs" Inherits="FIN.Client.SystemSetup.LanguageEntry"  %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div style="width: 500px">
        <div class="divClear">
        </div>
        <div class="divRow">
            <div class="lblBox" style="float: left; width: 200px" id="divlngCode">
                Language Code
            </div>
            <div style="float: left;">
                <asp:TextBox runat="server" ID="txtlngCode" CssClass="validate[required] txtBox "></asp:TextBox>
            </div>
        </div>
        <div class="divClear">
        </div>
        <div class="divRow">
            <div class="lblBox" style="float: left; width: 200px" id="divDescription">
                Language Description
            </div>
            <div style="float: left; width: 290px">
                <asp:TextBox runat="server" ID="txtDescription" CssClass="validate[required] txtBox"
                    Style="width: 100%"></asp:TextBox>
            </div>
        </div>
        <div class="divClear">
        </div>
        <div class="divRow">
            <div class="lblBox" style="float: left; width: 200px" id="divEffectiDate">
                Effective Date
            </div>
            <div style="float: left">
                <asp:TextBox runat="server" ID="txtStartDate" CssClass="validate[required] txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtStartDate" />
                <cc2:FilteredTextBoxExtender ID="fteStartDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                    TargetControlID="txtStartDate" />
            </div>
        </div>
        <div class="divClear">
        </div>
        <div class="divRow">
            <div class="lblBox" style="float: left; width: 200px" id="divEndDate">
                End Date
            </div>
            <div style="float: left">
                <asp:TextBox runat="server" ID="txtEndDate" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEndDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear">
        </div>
        <div class="divRow">
            <div class="divAction" style="width: 100%;" align="right">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"   />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { $("#form1").validationEngine(); });       
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });

    </script>
</asp:Content>
