﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginNew.aspx.cs" Inherits="FIN.Client.LoginNew1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Images/logo.ico">
    <title>LOGIN</title>
    <script src="Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
   <%-- <script src="Scripts/JQueryValidatioin/jquery.validationEngine-en.js" type="text/javascript"></script>--%>
    <script src="Scripts/JQueryValidatioin/jquery.validationEngine.js" type="text/javascript"></script>
    <link href="Scripts/JQueryValidatioin/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#btnYes").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
        function fn_ShowLang() {
            $("#divListLang").toggle('slow');
        }

    </script>
    <%--<script src="../LanguageScript/Generic.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>--%>
    <script type="text/javascript">
        document.write("<script src='Scripts/JQueryValidatioin/jquery.validationEngine_" + '<%= Session["Sel_Lng"] %>' + ".js'><\/script>");
    </script>
    <style type="text/css">
        .Container
        {
            background-color: rgb(241,10,10);
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(141,2,2)), to(rgb(241,10,10)));
            background: -moz-linear-gradient(top, rgb(141,2,2) 0%, rgb(241,10,10) 100%);
        }
        .Button
        {
            color: Black;
            background-color: White;
            border-color: Gray;
            padding: 2px 2px 2px 2px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            border: 2px solid gray;
        }
        .txtBox
        {
            border: 0px;
            background-color: White;
            height: 25px;
            width: 200px;
            padding: 2px 2px 2px 2px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
        }
    </style>
    <style type="text/css">
        .modal
        {
          /*  position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
            */
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: transparent;
            z-index: 999;
        }
    </style>
</head>
<body topmargin="0" bottommargin="0">
    <form id="form1" runat="server">
    <div align="center">
        <div style="width: 1024px; height: 630px; background-color: White">
            <div class="Container" style="height: 130px" align="left">
                <div style="float: left">
                    <table style="height: 130px;">
                        <tr valign="middle" style="height: 130px">
                            <td style="padding-left: 10px;">
                                <img src="Images/LoginLogo.png" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: right">
                    <div style="float: right; padding: 10px; font-family: Verdana; font-size: 12px; color: White;"
                        id="divLang" onclick="fn_ShowLang()" runat="server">
                        Language
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="float: right; display: none; font-family: Verdana; font-size: 12px; color: White;
                        padding: 20px" id="divListLang">
                        <table cellspacing="10px" style="color: White">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnEN" runat="server" ToolTip="English" CommandName="Language"
                                        ForeColor="White" CommandArgument="EN" OnClick="ChangeLanguage">English</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnAr" runat="server" ToolTip="العربية" CommandName="لغة"
                                        CommandArgument="AR" ForeColor="White" OnClick="ChangeLanguage">العربية</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="clear: both">
            </div>
            <div style="height: 300px; vertical-align: bottom">
                <div style="float: left; height: 300px; width: 124px; background: url('/images/footertop.png') no-repeat left bottom">
                </div>
                <div style="float: left; width: 900px; height: 300px;">
                    <table width="100%" style="height: 350px">
                        <tr style="height: 20%">
                            <td style="width: 40%">
                            </td>
                            <td style="width: 60%" align="center" >
                                <asp:Label ID="lblError" runat="server" CssClass="lblBox" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 108px">
                            <td style="width: 40%" rowspan="2" align="right">
                                <img src="Images/clipart.jpg" />
                            </td>
                            <td style="width: 60%; background-image: url('Images/img-1.png'); background-repeat: no-repeat;"
                                align="left">
                                <div style="padding-left: 20%; padding-top: 2px">
                                    <asp:TextBox CssClass="validate[required] txtBox" MaxLength="50" ID="txtUserID" runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr style="height: 108px">
                            <td style="width: 60%; background-image: url('Images/img-2.png'); background-repeat: no-repeat;"
                                align="left">
                                <div style="padding-left: 20%; padding-top: 1px">
                                    <asp:TextBox CssClass="validate[required] txtBox" TextMode="Password" MaxLength="50"
                                        ID="txtPassword" runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr style="height: 10%">
                            <td style="width: 40%">
                            </td>
                            <td style="width: 60%; padding-left: 100px">
                                <asp:Button runat="server" CssClass="Button" ID="btnYes" Text="Login" Width="80px"
                                    Height="30px" OnClick="btnYes_Click1" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="Button" ID="btNo" Text="Cancel" Width="80px"
                                    Height="30px" />
                            </td>
                        </tr>
                        <tr style="height: 10%">
                            <td style="width: 40%">
                                &nbsp;
                            </td>
                            <td style="width: 60%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="height: 200px; background-image: url('Images/footer.png'); background-repeat: no-repeat">
            </div>
            <asp:HiddenField ID="hf_pwdtryCount" runat="server" Value="0" />
        </div>
    </div>

    <div id="div_ChangePassword">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:HiddenField ID="hid_ChangePwd" runat="server" Value="0" />
        <cc2:ModalPopupExtender ID="MPEChangePwd" runat="server" TargetControlID="hid_ChangePwd"
            PopupControlID="pnlChangePwd" CancelControlID="btnchangepwdCancel" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlChangePwd" runat="server">
            <div class="ConfirmForm" style="width: 450px;">
                <div class="divRowContainer">
                    <div style="float: right">
                        <asp:Button ID="btnchangepwdCancel" runat="server" Text="Cancel" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <asp:Label ID="lblNewPassword" style="float: left; font-family:Verdana; font-size:small; width: 200px" runat="server">New Password</asp:Label>
                    <%--<div class="lblBox" style="float: left; width: 200px" id="lblNewPassword">
                        New Password
                    </div>--%>
                    <div class="divtxtBox" style="float: left; width: 150px">
                        <asp:TextBox ID="txtNewPassword" CssClass="txtBox" runat="server" TextMode="Password"
                            TabIndex="3"></asp:TextBox>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <asp:Label ID="lblConfirmPassword" style="float: left; font-family:Verdana; font-size:small; width: 200px" runat="server">Confirm Password</asp:Label>
                    <%--<div class="lblBox" style="float: left; width: 200px" id="lblConfirmPassword">
                        Confirm Password
                    </div>--%>
                    <div class="divtxtBox" style="float: left; width: 150px">
                        <asp:TextBox ID="txtConfirmPassword" CssClass="txtBox" runat="server" TextMode="Password"
                            TabIndex="3"></asp:TextBox>
                    </div>
                </div>
                 <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <asp:Label ID="lblpwdNotMatch" runat="server" style="float: left; font-family:Verdana; font-size:small;" Text="Password And Confirm Password Not Matching" Visible="false"></asp:Label>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div align="center">
                        <asp:Button ID="btnChangePwd" runat="server" Text="Change Password" 
                            onclick="btnChangePwd_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="loading" align="center">        
        <img src="Images/loading.gif" alt="" />
    </div>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                //var modal = $('<div />');
                //modal.addClass("modal");
                //$('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        $("#btnYes").click(function (e) {
            ShowProgress();
        })
        $("#btnChangePwd").click(function (e) {
            ShowProgress();
        })
        $("#btNo").click(function (e) {
            ShowProgress();
        });
    </script>

    </form>
</body>
</html>
