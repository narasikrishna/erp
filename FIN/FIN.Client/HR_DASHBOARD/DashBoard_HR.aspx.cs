﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using FIN.DAL;
using FIN.BLL;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class DashBoard_HR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowFrameAsPerPermission();
            }
        }
        private void ShowFrameAsPerPermission()
        {
            DataTable dt_PerDet = DBMethod.ExecuteQuery(FIN.DAL.SSM.RoleModulesForms_DAL.getRoleDashBoardData4User(Session[FINSessionConstants.UserName].ToString(), Session[FINSessionConstants.ModuleName].ToString())).Tables[0];
            if (dt_PerDet.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dt_PerDet.Rows.Count; iLoop++)
                {

                    Control myControl1 = FindControl(dt_PerDet.Rows[iLoop]["DB_FRAME_DIV_ID"].ToString());
                    if (myControl1 != null)
                    {
                        if (dt_PerDet.Rows[iLoop]["VIEW_FLAG"].ToString().ToUpper() == "TRUE")
                        {
                            myControl1.Visible = true;
                        }
                        else
                        {
                            myControl1.Visible = false;
                        }
                    }
                }
            }
        }
    }
}