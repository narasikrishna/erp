﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="HolidayList_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.HolidayList_DB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divCompanyHoliday" style="width: 100%">
        <div class="GridHeader" style="float: left; width: 100%; height: 25px;">
            <div style="float: left;padding-left: 10px; padding-top: 2px">
                Holidays
            </div>
            <div style="width: 120px; float: right; padding-right: 10px">
                <asp:DropDownList ID="ddlHolFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlHolFinYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divHolidayGrid" style="height:300px; overflow:auto">
            <asp:GridView ID="gvHolidayDet" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="HOLIDAY_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}">
                    </asp:BoundField>
                    <asp:BoundField DataField="HOLIDAY_DESC" HeaderText="Description"></asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
