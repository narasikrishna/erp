﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpDistinYear_BI.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.EmpDistinYear_BI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="GridHeader" style="float: left; width: 100%; height: 25px">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Employee Distribution in Year
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divEmpExpirGraph">
            <asp:Chart ID="chrt_emp_expire" runat="server" Width="500px" Height="310px" EnableViewState="True">
                <Series>
                    <asp:Series Name="s_Emp_Expir" ChartType="Doughnut" XValueMember="EMP_EXPI" YValueMembers="No_Of_Emp"
                        IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Docking="Bottom" Name="Legend1">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
