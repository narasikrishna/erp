﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using FIN.DAL;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class PermitRenewalDetails_DB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              

                FillComboBox();               

                getEmployeePermitDetails();

            }
        }
        private void FillComboBox()
        {
            ddlPerFinYear.Visible = false;
          
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
          
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlPerFinYear);
            if (ddlPerFinYear.Items.Count > 0)
            {
                // ddlPerFinYear.SelectedIndex = ddlPerFinYear.Items.Count - 1;
                ddlPerFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddlPerFinPeriod, ddlPerFinYear.SelectedValue);
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            if (dt_cur_period.Rows.Count > 0)
            {
                ddlPerFinPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

        }

        protected void ddlPerFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlPerFinPeriod, ddlPerFinYear.SelectedValue);
           
        }

        protected void ddlPerFinPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeePermitDetails();
          
        }
        private void getEmployeePermitDetails()
        {
            if (ddlPerFinPeriod.SelectedValue.ToString().Trim().Length > 0)
            {
                DataTable dt_PermitDet = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeePermitDetails_DAL.getEmployeePermitExpiry4Period(ddlPerFinPeriod.SelectedValue)).Tables[0];
                if (dt_PermitDet.Rows.Count > 0)
                {
                    gvPermitGrid.DataSource = dt_PermitDet;
                    gvPermitGrid.DataBind();
                }
            }
        }
        protected void lnk_Permit_Dept_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getPermitExpiryEmpDetails(gvr);
        }
        private void getPermitExpiryEmpDetails(GridViewRow gvr)
        {
            DataTable dt_empDet = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeePermitDetails_DAL.getEmployeePermitExpiry4PeriodDept(ddlPerFinPeriod.SelectedValue, gvPermitGrid.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString())).Tables[0];
            divempPermitGrid.Visible = true;
            divpermitGrid.Visible = false;
            gvEmpPermitDet.DataSource = dt_empDet;
            gvEmpPermitDet.DataBind();
        }
        protected void img_emp_Permit_Det_Click(object sender, ImageClickEventArgs e)
        {
            divempPermitGrid.Visible = false;
            divpermitGrid.Visible = true;
        }

    }
}