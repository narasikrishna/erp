﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PermitRenewalDetails_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.PermitRenewalDetails_DB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divPermintDet" style="width: 100%">
        <div class="GridHeader" style="float: left; width: 100%; height:25px">
            <div style="float: left; padding-left: 10px;padding-top: 2px">
                Permit Renewal Details
            </div>
            <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
                <asp:DropDownList ID="ddlPerFinPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlPerFinPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="width: 120px; float: right; padding-right: 10px">
                <asp:DropDownList ID="ddlPerFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlPerFinYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divpermitGrid" runat="server" visible="true">
            <asp:GridView ID="gvPermitGrid" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID" 
                AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_Permit_Dept_Name" runat="server" Text='<%# Eval("DEPT_NAME") %>'
                                OnClick="lnk_Permit_Dept_Name_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="No_Of_Emp" HeaderText="Count">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divempPermitGrid" runat="server" visible="false" style="height:270px; overflow:auto" >
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="img_emp_Permit_Det" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                            Width="20px" Height="20px" OnClick="img_emp_Permit_Det_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvEmpPermitDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="DEPT_ID" 
                            AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="EMP_NO" HeaderText="Number"></asp:BoundField>
                                <asp:BoundField DataField="EMP_NAME" HeaderText="Name"></asp:BoundField>
                                <asp:BoundField DataField="EXPIRY_DATE" HeaderText="Expiry Date" DataFormatString="{0:dd/MM/yyyy}">
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
