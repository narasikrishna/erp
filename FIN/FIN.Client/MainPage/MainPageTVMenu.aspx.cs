﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using FIN.BLL;
using FIN.DAL;
using System.Collections;
using System.Web.Services;

namespace FIN.Client.MainPage
{
    public partial class MainPageTVMenu : System.Web.UI.Page
    {
        Dictionary<string, string> Prop_File_Data;
        System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        [WebMethod]
        public static string GetWFAlertCount()
        {
            DataTable dtWorkflow = DBMethod.ExecuteQuery(FINSQL.GetWorkFlowMonitor(VMVServices.Web.Utils.UserName)).Tables[0];
            DataTable dtAlert = DBMethod.ExecuteQuery(FINSQL.GetAlertUserLevel(VMVServices.Web.Utils.UserName)).Tables[0];
            return dtWorkflow.Rows.Count + "@" + dtAlert.Rows.Count;
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                   // HttpContext.Current.Session["OrganizationID"] = "ORG-0000000066";
                   // HttpContext.Current.Session["UserName"] = "admin";
                    //Session[FINSessionConstants.UserName] = "admin";
                    //Session["FullUserName"] = "Admin";
                    HttpContext.Current.Session["Multilanguage"] = "EN";
                    Session["Sel_Lng"] = "EN";
                   //VMVServices.Web.Utils.UserName = "admin";
                    //VMVServices.Web.Utils.LanguageCode = "";



                    if (HttpContext.Current.Session["OrganizationID"] == null || HttpContext.Current.Session["UserName"] == null || HttpContext.Current.Session["Multilanguage"] == null)
                    {
                        Response.Redirect("../SessionTimeOut.aspx");
                    }

                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

                    DataTable dt_menulist = new DataTable();

                    VMVServices.Web.Utils.IsAlert = "0";


                    if (Session[FINSessionConstants.Sel_Lng] != null)
                    {
                        dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetMenuList(Session[FINSessionConstants.UserName].ToString(), "", Session[FINSessionConstants.Sel_Lng].ToString())).Tables[0];
                    }

                    System.Text.StringBuilder str_LinkQuery = new System.Text.StringBuilder();

                    lblUName.Text = Session["FullUserName"].ToString();

                    DataTable dt_Module = dt_menulist.DefaultView.ToTable(true, "MODULECODE", "MODULENAME");

                    string str_MenuTree = "";
                    string str_MenuLocation = "";
                    //str_MenuTree += " <ul class='mtree transit'> ";

                    str_MenuTree += " <ul class='accordion' id='accordion1'> ";
                    for (int MLoop = 0; MLoop < dt_Module.Rows.Count; MLoop++)
                    {
                        str_MenuTree += " <li> <a href='#'> <img src='../Images/MainPage/" + dt_Module.Rows[MLoop]["MODULECODE"].ToString() + "_IN.png' width='25px' height='25px' /> " + dt_Module.Rows[MLoop]["MODULENAME"].ToString() + "</a>";
                       // str_MenuTree += " <li> <a href='#'>  " + dt_Module.Rows[MLoop]["MODULENAME"].ToString() + "</a>";

                        var tbl_ModMenu = dt_menulist.AsEnumerable()
                              .Where(r => r["MODULECODE"].ToString().Trim() ==dt_Module.Rows[MLoop]["MODULECODE"].ToString().Trim())
                              .ToList();

                        DataTable dt_ModMenu = System.Data.DataTableExtensions.CopyToDataTable(tbl_ModMenu);

                        var tbl_Menu = dt_menulist.AsEnumerable()
                             .Where(r => r["MODULECODE"].ToString().Trim() ==dt_Module.Rows[MLoop]["MODULECODE"].ToString().Trim() && r["PARENT_MENU"].ToString().Trim().Length == 0)
                             .ToList();

                        DataTable dt_MenuOnly = new DataTable();
                        if (tbl_Menu.Any())
                        {
                            dt_MenuOnly = System.Data.DataTableExtensions.CopyToDataTable(tbl_Menu);
                        }

                        str_MenuTree += " <ul >";
                        for (int KLoop = 0; KLoop < dt_MenuOnly.Rows.Count; KLoop++)
                        {
                            str_MenuTree += " <li> ";
                            str_MenuLocation = dt_MenuOnly.Rows[KLoop]["MODULENAME"].ToString() + " --> " + dt_MenuOnly.Rows[KLoop]["SCREEN_NAME"].ToString();
                            str_MenuTree += "<a onClick=\"fn_ResizeEntryScreen('" + str_MenuLocation + "','" + dt_MenuOnly.Rows[KLoop]["SHOW_ICONSET"].ToString() + "',this)\" href='" + dt_MenuOnly.Rows[KLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                            str_MenuTree += "ProgramID=" + dt_MenuOnly.Rows[KLoop]["MENU_KEY_ID"].ToString() + "&";
                            str_MenuTree += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                            str_MenuTree += QueryStringTags.ID.ToString() + "=0" + "&";
                            str_MenuTree += "MODULENAME" + "=" + dt_MenuOnly.Rows[KLoop]["MODULECODE"].ToString() + "&";
                            str_MenuTree += QueryStringTags.AddFlag.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                            str_MenuTree += QueryStringTags.UpdateFlag.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                            str_MenuTree += QueryStringTags.DeleteFlag.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                            str_MenuTree += QueryStringTags.QueryFlag.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                            str_MenuTree += QueryStringTags.ReportName.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["REPORT_NAME"].ToString() + "&";
                            str_MenuTree += QueryStringTags.ReportName_OL.ToString() + "=" + dt_MenuOnly.Rows[KLoop]["ATTRIBUTE3"].ToString() + "'";
                            str_MenuTree += "  target='centerfrm'><span>" + dt_MenuOnly.Rows[KLoop]["SCREEN_NAME"].ToString() + "</span></a>";

                            str_MenuTree += "</li>";
                        }

                        DataTable dt_ParentMenu = dt_ModMenu.DefaultView.ToTable(true, "PARENT_MENU_CODE", "PARENT_MENU");
                        for (int KLoop = 0; KLoop < dt_ParentMenu.Rows.Count; KLoop++)
                        {
                            if (dt_ParentMenu.Rows[KLoop]["PARENT_MENU"].ToString().Length > 0)
                            {


                                str_MenuTree += " <li> <a href='#'>" + dt_ParentMenu.Rows[KLoop]["PARENT_MENU"].ToString() + "</a>";
                                str_MenuTree += " <ul > ";


                                var tbl_ParentChildMenu = dt_ModMenu.AsEnumerable()
                             .Where(r => r["PARENT_MENU_CODE"].ToString().Trim() == dt_ParentMenu.Rows[KLoop]["PARENT_MENU_CODE"].ToString() && r["PARENT_PARENT_MENU_CODE"].ToString().Trim() == dt_ParentMenu.Rows[KLoop]["PARENT_MENU_CODE"].ToString().Trim())
                             .ToList();

                                DataTable dt_ParentChildMenu = new DataTable();
                                if (tbl_ParentChildMenu.Any())
                                {
                                    dt_ParentChildMenu = System.Data.DataTableExtensions.CopyToDataTable(tbl_ParentChildMenu);
                                }
                                for (int rLoop = 0; rLoop < dt_ParentChildMenu.Rows.Count; rLoop++)
                                {
                                    str_MenuTree += " <li>";
                                    str_MenuLocation = dt_ParentChildMenu.Rows[rLoop]["MODULENAME"].ToString() + "-->" + dt_ParentChildMenu.Rows[rLoop]["PARENT_MENU"].ToString() + " --> " + dt_ParentChildMenu.Rows[rLoop]["SCREEN_NAME"].ToString();
                                    str_MenuTree += "<a onClick=\"fn_ResizeEntryScreen('" + str_MenuLocation + "','" + dt_ParentChildMenu.Rows[rLoop]["SHOW_ICONSET"].ToString() + "',this)\" href='" + dt_ParentChildMenu.Rows[rLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                    str_MenuTree += "ProgramID=" + dt_ParentChildMenu.Rows[rLoop]["MENU_KEY_ID"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                    str_MenuTree += QueryStringTags.ID.ToString() + "=0" + "&";
                                    str_MenuTree += "MODULENAME" + "=" + dt_ParentChildMenu.Rows[rLoop]["MODULECODE"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.AddFlag.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.UpdateFlag.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.DeleteFlag.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.QueryFlag.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.ReportName.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["REPORT_NAME"].ToString() + "&";
                                    str_MenuTree += QueryStringTags.ReportName_OL.ToString() + "=" + dt_ParentChildMenu.Rows[rLoop]["ATTRIBUTE3"].ToString() + "'";
                                    str_MenuTree += "  target='centerfrm'><span>" + dt_ParentChildMenu.Rows[rLoop]["SCREEN_NAME"].ToString() + "</span></a>";
                                    str_MenuTree += " </li>";
                                }


                                var tbl_ParentChildMenuParent = dt_ModMenu.AsEnumerable()
                            .Where(r => r["PARENT_MENU_CODE"].ToString().Trim() == dt_ParentMenu.Rows[KLoop]["PARENT_MENU_CODE"].ToString().Trim() && r["PARENT_PARENT_MENU_CODE"].ToString().Trim() != dt_ParentMenu.Rows[KLoop]["PARENT_MENU_CODE"].ToString().Trim())
                            .ToList();
                                if (tbl_ParentChildMenuParent.Any())
                                {
                                    DataTable dt_ParentChildMenuParent = System.Data.DataTableExtensions.CopyToDataTable(tbl_ParentChildMenuParent).DefaultView.ToTable(true, "PARENT_PARENT_MENU","PARENT_PARENT_MENU_CODE");


                                    for (int PCLoop = 0; PCLoop < dt_ParentChildMenuParent.Rows.Count; PCLoop++)
                                    {



                                        str_MenuTree += " <li> <a href='#'>" + dt_ParentChildMenuParent.Rows[PCLoop]["PARENT_PARENT_MENU"].ToString() + "</a>";
                                        str_MenuTree += " <ul > ";

                                        //r["PARENT_PARENT_MENU"].ToString().Trim() == dt_ParentChildMenuParent.Rows[PCLoop]["PARENT_PARENT_MENU"].ToString().Trim() &&
                                        var tbl_PCPChildMenu = dt_ModMenu.AsEnumerable()
                                     .Where(r =>  r["PARENT_PARENT_MENU_CODE"].ToString().Trim().Contains(dt_ParentChildMenuParent.Rows[PCLoop]["PARENT_PARENT_MENU_CODE"].ToString().Trim()))
                                     .ToList();

                                        DataTable dt_PCPChildMenu = new DataTable();
                                        if (tbl_PCPChildMenu.Any())
                                        {
                                            dt_PCPChildMenu = System.Data.DataTableExtensions.CopyToDataTable(tbl_PCPChildMenu);
                                        }
                                        for (int rLoop = 0; rLoop < dt_PCPChildMenu.Rows.Count; rLoop++)
                                        {
                                            str_MenuTree += " <li>";
                                            str_MenuLocation = dt_PCPChildMenu.Rows[rLoop]["MODULENAME"].ToString() + "-->" + dt_PCPChildMenu.Rows[rLoop]["PARENT_MENU"].ToString() + " --> " + dt_PCPChildMenu.Rows[rLoop]["SCREEN_NAME"].ToString();
                                            str_MenuTree += "<a onClick=\"fn_ResizeEntryScreen('" + str_MenuLocation + "','" + dt_PCPChildMenu.Rows[rLoop]["SHOW_ICONSET"].ToString() + "',this)\" href='" + dt_PCPChildMenu.Rows[rLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                            str_MenuTree += "ProgramID=" + dt_PCPChildMenu.Rows[rLoop]["MENU_KEY_ID"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                            str_MenuTree += QueryStringTags.ID.ToString() + "=0" + "&";
                                            str_MenuTree += "MODULENAME" + "=" + dt_PCPChildMenu.Rows[rLoop]["MODULECODE"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.AddFlag.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.UpdateFlag.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.DeleteFlag.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.QueryFlag.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.ReportName.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["REPORT_NAME"].ToString() + "&";
                                            str_MenuTree += QueryStringTags.ReportName_OL.ToString() + "=" + dt_PCPChildMenu.Rows[rLoop]["ATTRIBUTE3"].ToString() + "'";
                                            str_MenuTree += "  target='centerfrm'><span>" + dt_PCPChildMenu.Rows[rLoop]["SCREEN_NAME"].ToString() + "</span></a>";
                                            str_MenuTree += " </li>";
                                        }

                                        str_MenuTree += " </ul> ";
                                        str_MenuTree += " </li>";

                                    }
                                }
                                str_MenuTree += " </ul> ";
                                str_MenuTree += " </li>";

                            }
                        }

                        str_MenuTree += "</ul>";
                        str_MenuTree += "</li>";
                    }
                    str_MenuTree += " </ul>";

                    str_LinkQuery.Append(str_MenuTree);

                    div_Menu.InnerHtml = str_LinkQuery.ToString();
                    FIN.Client.ClsGridBase.DataMenuList = dt_menulist;
                    Session[FINSessionConstants.MenuData] = dt_menulist;

                    AssignToWorkflow();
                    AssignToAlert();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }




        protected void imgClear_Click(object sender, ImageClickEventArgs e)
        {
            hf_ClearClick.Value = "Y";
            if (Request.QueryString["WF"] == null && Request.QueryString["Id"] == null)
            {
                //string entryFrameSrc = ifrmaster.Attributes["src"].ToString();
                //Uri tmp_uri = new Uri(entryFrameSrc);
                //string form_id = HttpUtility.ParseQueryString(tmp_uri.Query).Get("ProgramID");
                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }
        //for export into excel query datas
        protected void ImgCurrency_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Session["QUERY_FORM"] == FINAppConstants.N)
                {
                    DataTable dt = FIN.DAL.FINSP.get_RecordExportData(int.Parse(Session["ProgramID"].ToString()), Session["StrRecordId"].ToString());
                    Session["QueryFormData"] = null;
                    Session[FINSessionConstants.GridData] = dt;
                }
                if (Session[FINSessionConstants.GridData] != null)
                {
                    Response.Redirect("../ExportGridData.aspx?ExportId=EXCEL", true);
                }
                else
                {
                    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                }
            }

        }


        protected void imgWF_Click(object sender, ImageClickEventArgs e)
        {
            hf_ClearClick.Value = "Y";
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                mpeWF.Show();

                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }


        protected void imgbtnABR_Click(object sender, ImageClickEventArgs e)
        {
            hf_ClearClick.Value = "Y";
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["StrRecordId"].ToString() != "0")
                {
                    mpeABR.Show();

                    string[] str_abtRec = new string[7];
                    str_abtRec = FINSP.GetSPFOR_AboutThisRec(Session["FormCode"].ToString(), Session["StrRecordId"].ToString());

                    string str_Det = "<Table>";
                    str_Det += "<tr><td>1.</td><td>" + str_abtRec[0] + "</td></tr>";
                    str_Det += "<tr><td>2.</td><td>" + str_abtRec[1] + "</td></tr>";
                    str_Det += "<tr><td>3.</td><td>" + str_abtRec[2] + "</td></tr>";
                    str_Det += "<tr><td>4.</td><td>" + str_abtRec[3] + "</td></tr>";
                    str_Det += "<tr><td>5.</td><td>" + str_abtRec[4] + "</td></tr>";
                    str_Det += "<tr><td>6.</td><td>" + str_abtRec[5] + "</td></tr>";
                    str_Det += "<tr><td>7.</td><td>" + str_abtRec[6] + "</td></tr>";
                    str_Det += "</Table>";

                    div_ABTREC.InnerHtml = str_Det;

                }
                if (Session["FormCode"].ToString().Length > 0)
                {
                    string form_id = Session["ProgramID"].ToString();
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                    string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                    str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                    str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                    ifrmaster.Attributes.Add("src", str_LinkURL);
                }
            }
        }

        protected void imgHelp_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dtVideo = new DataTable();
            hf_ClearClick.Value = "Y";
            if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["HelpVideoFolder"].ToString() + Session[FINSessionConstants.ModuleName].ToString() + "/" + Session["FormCode"].ToString() + ".mp4"))
            {
                dtVideo = DBMethod.ExecuteQuery(FINSQL.GetVideopath(Session["FormCode"].ToString())).Tables[0];

                //dtVideo.Rows[0]["FilePath"] = Server.MapPath("../Help/" + Session[FINSessionConstants.ModuleName].ToString() + "/" + Session["FormCode"].ToString() + ".mp4");
                // dtVideo.Rows[0]["FilePath"] = Server.MapPath(dtVideo.Rows[0]["FilePath"].ToString());
                // dtVideo.Rows[0]["FilePath"] =(System.Web.Configuration.WebConfigurationManager.AppSettings["HelpVideoFolder"].ToString() + Session[FINSessionConstants.ModuleName].ToString() + "/" + Session["FormCode"].ToString() + ".mp4");
                // dtVideo.AcceptChanges();

                gvVideo.DataSource = dtVideo;
                gvVideo.DataBind();
                mpeVideo.Show();
            }

            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["StrRecordId"].ToString() != "0")
                {
                    // mpeABR.Show();

                    string[] str_abtRec = new string[7];
                    str_abtRec = FINSP.GetSPFOR_AboutThisRec(Session["FormCode"].ToString(), Session["StrRecordId"].ToString());

                    string str_Det = "<Table>";
                    str_Det += "<tr><td>1.</td><td>" + str_abtRec[0] + "</td></tr>";
                    str_Det += "<tr><td>2.</td><td>" + str_abtRec[1] + "</td></tr>";
                    str_Det += "<tr><td>3.</td><td>" + str_abtRec[2] + "</td></tr>";
                    str_Det += "<tr><td>4.</td><td>" + str_abtRec[3] + "</td></tr>";
                    str_Det += "<tr><td>5.</td><td>" + str_abtRec[4] + "</td></tr>";
                    str_Det += "<tr><td>6.</td><td>" + str_abtRec[5] + "</td></tr>";
                    str_Det += "<tr><td>7.</td><td>" + str_abtRec[6] + "</td></tr>";
                    str_Det += "</Table>";

                    div_ABTREC.InnerHtml = str_Det;

                }
                if (Session["FormCode"].ToString().Length > 0)
                {
                    string form_id = Session["ProgramID"].ToString();
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);

                    if (dr_list[0]["ENTRY_PAGE_OPEN"].ToString() != string.Empty)
                    {
                        string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                        str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                        str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                        str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                        str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                        ifrmaster.Attributes.Add("src", str_LinkURL);
                    }
                }
            }

        }

        protected void ChangeLanguage(object sender, EventArgs e)
        {
            hf_ClearClick.Value = "Y";
            LinkButton lb_lang = (LinkButton)sender;
            Session[FINSessionConstants.Sel_Lng] = lb_lang.CommandArgument.ToString();

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session[FINSessionConstants.Sel_Lng].ToString() + ".properties"));
            Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> " + Prop_File_Data["Home_P"] + "</a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white' target='centerfrm'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
            // hf_TreeLink.Value = Session["Tree_Link"].ToString();

            if (Session[FINSessionConstants.Sel_Lng] == null || Session[FINSessionConstants.Sel_Lng] == "EN")
            {
                Session[FINSessionConstants.Sel_Lng] = "EN";
                VMVServices.Web.Utils.LanguageCode = "";
            }
            else
            {
                VMVServices.Web.Utils.LanguageCode = "_OL";
            }

            if (Session["FormCode"] == null || Session["FormCode"] == "0")
            {
                Hashtable htProgram = Menu_BLL.GetMenuDetail(int.Parse(Session["ProgramID"].ToString()));
                Session["FormCode"] = htProgram[ProgramParameters.FormCode.ToString()].ToString();
            }
            if (Session["FormCode"].ToString().Length > 0)
            {
                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);

                if (dr_list[0]["ENTRY_PAGE_OPEN"].ToString() != string.Empty)
                {
                    string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                    str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                    if (Session["StrRecordId"] != null)
                    {
                        str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                    }
                    else if (Session["RecordID"] != null)
                    {
                        str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["RecordID"].ToString() + "&";
                    }
                    else
                    {
                        str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                    }
                    str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                    ifrmaster.Attributes.Add("src", str_LinkURL);
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (rblWF.SelectedValue.ToString() != "WFST01")
            {
                if (txtRemarks.Text.ToString().Trim().Length == 0)
                {
                    lblNeedRemarks.Visible = true;
                    mpeWF.Show();
                    return;
                }
            }
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["Mode"].ToString() == ProgramMode.WApprove.ToString().Substring(0, 1) && Session["StrRecordId"].ToString() != "0")
                {
                    string str_Ret_Status = FINSP.UpdateWorhflowstatus(Session["FormCode"].ToString(), rblWF.SelectedValue.ToString(), Session["UserName"].ToString(), Session["StrRecordId"].ToString(), txtRemarks.Text, Session[FINSessionConstants.LevelCode].ToString());
                    Session[FINSessionConstants.LevelCode] = null;

                    //FINSP.GetSP_GL_Posting(Session["StrRecordId"].ToString(), Session["FormCode"].ToString());

                    //  Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString());

                    if (Session["FormCode"].ToString().Length > 0)
                    {
                        string form_id = Session["ProgramID"].ToString();
                        System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                        System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);

                        if (dr_list[0]["ENTRY_PAGE_OPEN"].ToString() != string.Empty)
                        {
                            string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                            str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                            str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                            str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                            str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                            ifrmaster.Attributes.Add("src", str_LinkURL);
                        }
                    }
                }
            }
        }

        protected void gvWorkFlow_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink hFld = (HyperLink)e.Row.FindControl("hyperMsgID");

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {

                    string navigationMode = "WORKFLOW";

                    Session[FINSessionConstants.LevelCode] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["Level_Code"].ToString();

                    hFld.NavigateUrl = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&" + QueryStringTags.AddFlag + "=0&" + "&" + QueryStringTags.DeleteFlag + "=0&" + QueryStringTags.UpdateFlag + "=0&" + QueryStringTags.ReportName + "=''&" + QueryStringTags.Mode + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "&WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["workflow_code"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&LC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["Level_Code"].ToString() + "&MD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;


                    //string str_LinkWFQuery = string.Empty;

                    //str_LinkWFQuery += QueryStringTags.ID.ToString() + "=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[iWF]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[iWF]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[iWF]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[iWF]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[iWF]["REPORT_NAME"].ToString() + "&";
                    //str_LinkWFQuery += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[iWF]["ATTRIBUTE3"].ToString() + "&";
                    //str_LinkWFQuery += "NAV_MODE=" + Request.QueryString["NAV_MODE"].ToString() + "'";
                }
            }
        }

        protected void gvAlert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink hFld = (HyperLink)e.Row.FindControl("hyperAlertMsgID");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    string navigationMode = "ALERT";

                    //   hFld.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString() + "?WF=" + gvAlert.DataKeys[e.Row.RowIndex].Values["screen_code"].ToString() + "&Id=" + gvAlert.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&MD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;
                    hFld.NavigateUrl = gvAlert.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&" + QueryStringTags.AddFlag + "=0&" + "&" + QueryStringTags.DeleteFlag + "=0&" + QueryStringTags.UpdateFlag + "=0&" + QueryStringTags.ReportName + "=''&" + QueryStringTags.Mode + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "&WF=" + gvAlert.DataKeys[e.Row.RowIndex].Values["screen_code"].ToString() + "&Id=" + gvAlert.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&LC=0&MD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;




                }
            }
        }

        private void AssignToWorkflow()
        {
            try
            {


                ErrorCollection.Clear();
                DataTable dtWorkflow = DBMethod.ExecuteQuery(FINSQL.GetWorkFlowMonitor(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtWorkflow != null)
                {

                    Session["dtWorkflow"] = dtWorkflow;
                    gvWorkFlow.DataSource = dtWorkflow;
                    gvWorkFlow.DataBind();


                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToAlert()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtAlert = DBMethod.ExecuteQuery(FINSQL.GetAlertUserLevel(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtAlert != null)
                {

                    Session["dtAlert"] = dtAlert;
                    gvAlert.DataSource = dtAlert;
                    gvAlert.DataBind();
                    /* if (gvAlert.Rows.Count > 0)
                     {
                         divAlterCount.Visible = true;

                         divAlterCount.InnerText = gvAlert.Rows.Count.ToString();
                     }
                     else
                     {
                         divAlterCount.Visible = false;
                     }
                     */
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("alkert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvWorkFlow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvWorkFlow.PageIndex = e.NewPageIndex;
                gvWorkFlow.DataSource = (DataTable)Session["dtWorkflow"];
                gvWorkFlow.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvAlert_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvAlert.PageIndex = e.NewPageIndex;
                gvAlert.DataSource = (DataTable)Session["dtAlert"];
                gvAlert.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Alert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }


}