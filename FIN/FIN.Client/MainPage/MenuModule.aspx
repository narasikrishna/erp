﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuModule.aspx.cs" Inherits="FIN.Client.MainPage.MenuModule" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        function fn_ChangeBtnSize(cntrl) {
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(40);
            $("#" + cntrlId).height(40);

        }
        function fn_ChangeBtnReSize(cntrl) {
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(45);
            $("#" + cntrlId).height(45);
        }

        function fn_QueryFormDisplay() {
            $("#" + $("#hf_SubMenu").val()).css("display", "none");
            var hid_val = $("#hid_toggle").val();
            $("#hf_IdelMinutes").val(0);
            $("#hf_bol_IMStart").val("TRUE")
            $("#Searchfrm").contents().find("body").html('');
            if (hid_val == 'CLOSE') {
                $("#hid_toggle").val('OPEN');
                $('#divSearch').css("width", "88%");
                $('#divEntry').css("width", "0px");
                $("#divSearch").slideDown('slow');
                var sfrm = document.getElementById('Searchfrm');
                sfrm.src = sfrm.src;
                $('#divSearch').css("left", 50 + $("#img_MMHide").offset().left + 'px');
            }
            else {
                $("#hid_toggle").val('CLOSE');
                $('#divSearch').css("width", "0px");
                $('#divEntry').css("width", "88%");
                $('#divEntry').css("left", 50 + $("#img_MMHide").offset().left + 'px');
            }
            return false;
        }

        function fn_getSaveBtn() {

            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                btn = y.getElementById("ctl00_FINContent_btnSave");
                if (btn != null) {
                    btn.click();
                } else {
                    alert('Invalid Save Button Click');
                }
            }

        }
    </script>
    <script type="text/javascript">
        function fn_ModuleMouseOver(imgId, divId, imgName,ModuleName) {
            if ($("#hf_LastModuleDiv").val() != divId) {
                $("#" + $("#hf_LastModuleDiv").val()).css("display", "none");
                $("#" + $("#hf_LastModuleImg").val()).attr("src", '../Images/MMPercentage/' + $("#hf_LastModuleImgName").val() + '_OUT.png');
            }
            //$("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_IN.png');
            $("#" + divId).toggle("slow");
            $("#hf_LastModuleDiv").val(divId);
            $("#hf_LastModuleImg").val(imgId);
            $("#hf_LastModuleImgName").val(imgName);
            if ($("#" + imgId).attr('src').toString().indexOf("_IN") > 0) {
                $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_OUT.png');
            }
            else {
                $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_IN.png');
            }

            $("#hf_ModuleName").val(ModuleName);
        }
        function fn_ShowSubMenu(divSubmenuId,MainMenuName) {
            
            $('#divSearch').css("display", "none");

            if ($("#hf_SubMenu").val() != divSubmenuId) {
                $("#" + $("#hf_SubMenu").val()).css("display", "none");
                $("#TOP_" + $("#hf_SubMenu").val()).css("display", "none");
                $('#hf_divEntryStatus').val("CLOSE");
            }
            else {
                if ($('#hf_divEntryStatus').val() == "CLOSE") {
                    $('#hf_divEntryStatus').val("OPEN");
                }
                else {
                    $('#hf_divEntryStatus').val("CLOSE");
                }

            }

            if ($('#hf_divEntryStatus').val() == "CLOSE") {
                $('#divEntry').css("display", "none");
            }
            else {
                $('#divEntry').css("display", "inline");
                $('#divSearch').css("display", "none");
            }

            $("#" + divSubmenuId).toggle("slow");
            $("#hf_SubMenu").val(divSubmenuId);

            $("#TOP_" + divSubmenuId).css("display", "inline");
            $("#hf_MainMenu").val(MainMenuName);


        }
        function fn_showMenuBar() {
           
            $('#lblModuelName').text ($("#hf_ModuleName").val());
            $('#lblMainMenu').text($("#hf_MainMenu").val() );
            $('#lblMenu').text($("#hf_Menu").val());
            $("#div_Menubar").css("display", "inline");
            
        }
        function fn_hidemenu(menuName) {
            
            $("#" + $("#hf_SubMenu").val()).css("display", "none");
            $('#divEntry').css("display", "inline");
            $('#hf_divEntryStatus').val("OPEN");
            $("#hf_Menu").val(menuName);
            $('#divEntry').css("left", 50 + $("#img_MMHide").offset().left + 'px');
            fn_showMenuBar();
        }

        function fn_ShowModule() {
            $("#div_Module_List").toggle("slow");
            $("#div_MainMenu").toggle("slow");
            $("#img_MMHide").css("display", "inline");
            $("#img_MMShow").css("display", "none");
            $('#divEntry').css("left", "130px");

        }
        function fn_HideModule() {
            $("#div_Module_List").css("display", "none");
            $("#div_MainMenu").css("display", "none");
            $("#img_MMHide").css("display", "none");
            $("#img_MMShow").css("display", "inline");
            $('#divEntry').css("left", "50px");

        }

        $(function () {

            $("#slidedown").click(function (event) {
                event.preventDefault();
                $("#div_top_subMenu").slideDown();
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");

            });

            $("#SlideUp").click(function (event) {
                event.preventDefault();
                $("#div_top_subMenu").slideUp();
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");

            });
        });
        function fn_setEntryScreenPosition() {
            $('#divEntry').css("left", 50 + $("#img_MMHide").offset().left + 'px');

        }
        
    </script>
    <script type="text/javascript">
        $(function () {
            $('#div_MainMenu').on('mouseover', 'tr', function () {
                $(this).css({
                    'background-color': '#676060'
                });
            }).on('mouseout', 'tr', function () {
                $(this).css({
                    'background-color': '#0099FF'
                });
            }).on('click', 'td', function () {

                // Remove all active class from all td                 
                $("#div_MainMenu td").removeClass('active');
                // Add active class to current td target 
                $(this).addClass('active');
            });
        });
        
    </script>
    <style type="text/css">
        .active
        {
            background-color: rgb(11,118,162);
        }
        body
        {
            background-image: url('../Images/Smalllogo.jpg');
        }
        .divTop
        {
            background: rgb(0,153,255);
            width: 100%;
            height: 35px;
        }
        .divCenter
        {
            width: 100%;
            padding-top: 50px;
        }
        .divfooter
        {
            width: 100%;
            height: 25px; /* Height of the footer */
            background: rgb(0,153,255);
        }
        .divSubMenu
        {
            float: left;
            padding-left: 30px;
        }
        .menuNoAccess
        {
            background-image: url('../Images/Strike.png');           
            background-repeat: repeat;
        }
        .menuAccess
        {
        }
        
        .CenterHeight
        {
            height: 540px; /*border:2px solid blue;*/
        }
        .ActiveImageSize
        {
            width: 30px;
            height: 30px;
            padding-top: 10px;
        }
        .dispFontStyle
        {
            font-family: Verdana;
            font-size: 13px;
            color:White;
        }
        .dispControlImgSize
        {
            width:30px;
            height:30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%">
        <div id="div_Top" class="divTop">
            <div style="float: left; position: absolute; padding-top: 10px; padding-left: 20px;">
                <img src="../Images/logo.png" alt="miethree" title="miethree" width="80px" height="40px" />
            </div>
            <div style="float: left; padding-left: 120px; display:none" class="dispFontStyle" id="div_Menubar" >
                <asp:Label ID="lblModuelName" runat="server" Text="Moduel  "></asp:Label>
                &#8658;&#8658;&#8658;
                <asp:HiddenField ID="hf_ModuleName" runat="server" />
                <asp:Label ID="lblMainMenu" runat="server" Text="MainMenu"></asp:Label>
                &#8658;&#8658;&#8658;
                <asp:HiddenField ID="hf_MainMenu" runat="server" />
                 <asp:Label ID="lblMenu" runat="server" Text="Menu"></asp:Label>
                <asp:HiddenField ID="hf_Menu" runat="server" />
            </div>
            <div style="float: right; padding-right: 20px;">
                <table width="100px">
                    <tr>
                        <td>
                            <img src="../Images/wf_icon.png" alt="WF" title="WorkFlow" onclick="fn_ShowWorkFlow()"
                                width="25px" height="25px" />
                        </td>
                        <td>
                            <img src="../Images/alert-icon.png" alt="Alert" title="Alert" onclick="fn_ShowAlert()"
                                width="25px" height="25px" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="div_top_menu" style="left: 0; float: left; top: 50px; vertical-align: middle;
            width: 100%; border: 0px solid yellow; position: fixed; display: none;">
            <div id="div_top_subMenu" runat="server" class="divSubMenu dispFontStyle" style="position: absolute;
                display: none">
            </div>
            <div class="divClear_10">
            </div>
            <div style="float: left; left: 45%; position: relative;">
                <table>
                    <tr>
                        <td>
                            <img style="padding-left: 10px" src="../Images/MainPage/SlideDown_B.png" width="60px"
                                height="20px" id="slidedown" />
                            <img src="../Images/MainPage/SlideUp_B.png" width="60px" height="20px" id="SlideUp"
                                style="padding-left: 10px; display: none" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="div_Center" class="divCenter">
            <asp:HiddenField ID="hf_LastModuleDiv" runat="server" Value="" />
            <asp:HiddenField ID="hf_LastModuleImg" runat="server" Value="" />
            <asp:HiddenField ID="hf_LastModuleImgName" runat="server" Value="" />
            <asp:HiddenField ID="hf_SubMenu" runat="server" Value="" />
            <asp:HiddenField ID="hf_divEntryStatus" runat="server" Value="OPEN" />
            <div id="div_Module" style="left: 0px; float: left; top: 70px; vertical-align: middle;
                border: 0px solid yellow; position: fixed;">
                <table >
                    <tr>
                        <td>
                            <div style="float: left" id="div_Module_List" onmouseout="fn_setEntryScreenPosition()">
                                <table border="0px" width="100%">
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divGL" onclick="fn_ModuleMouseOver('imgGL','div_GL','GL','GL')">
                                                <div id="divGLImg" style="float: left">
                                                    <img id="imgGL" src="../Images/MMPercentage/GL_OUT.png" alt="GL" title="GL" class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divFA" onclick="fn_ModuleMouseOver('imgFA','div_FA','FA','FA')">
                                                <div id="divFAImg" style="float: left">
                                                    <img id="imgFA" src="../Images/MMPercentage/FA_OUT.png" alt="FA" title="FA"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divAP" onclick="fn_ModuleMouseOver('imgAP','div_AP','AP','AP')">
                                                <div id="divAPImg" style="float: left">
                                                    <img id="imgAP" src="../Images/MMPercentage/AP_OUT.png" alt="AP" title="AP"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divAR" onclick="fn_ModuleMouseOver('imgAR','div_AR','AR','AR')">
                                                <div id="divARImg" style="float: left">
                                                    <img id="imgAR" src="../Images/MMPercentage/AR_OUT.png" alt="AR" title="AR"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divHR" onclick="fn_ModuleMouseOver('imgHR','div_HR','HR','HR')">
                                                <div id="divHRImg" style="float: left">
                                                    <img id="imgHR" src="../Images/MMPercentage/HR_OUT.png" alt="HR" title="HR"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divSS" onclick="fn_ModuleMouseOver('imgSS','div_SSM','SS','SS')">
                                                <div id="divSSImg" style="float: left">
                                                    <img id="imgSS" src="../Images/MMPercentage/SS_OUT.png" alt="SS" title="SS"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divCM" onclick="fn_ModuleMouseOver('imgCM','div_CA','CM','CA')">
                                                <div id="divCMImg" style="float: left">
                                                    <img id="imgCM" src="../Images/MMPercentage/CM_OUT.png" alt="CM" title="CM"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px">
                                            <div id="divLoan" onclick="fn_ModuleMouseOver('imgLoan','div_LN','Loan','Loan')">
                                                <div id="divLoanImg" style="float: left">
                                                    <img id="imgLoan" src="../Images/MMPercentage/Loan_OUT.png" alt="Loan" title="Loan"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="divPayroll" onclick="fn_ModuleMouseOver('imgPayroll','div_PR','Payroll','PAYROLL')">
                                                <div id="divPayrollImg" style="float: left">
                                                    <img id="imgPayroll" src="../Images/MMPercentage/Payroll_OUT.png" alt="Payroll" title="Payroll"  class="dispControlImgSize" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div22">
                                                <asp:Button ID="btnModuleCLick" runat="server" Text="" OnClick="btnModuleCLick_Click"
                                                    Style="display: none" />
                                                <asp:HiddenField runat="server" ID="hf_MoudleName" Value="" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                            <div id="div_MainMenu" runat="server" class="dispFontStyle" style="float: left; background-color: RGB(0,153,255);
                                cursor: pointer; cursor: hand">
                            </div>
                        </td>
                        <td>
                            <div style="float: left;">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img id="img_MMShow" src="../Images/MENUICONS/ModuleMenu_IN.png" onclick="fn_ShowModule()"
                                                width="30px" alt=" " title="Open" style="display: none" />
                                            <img id="img_MMHide" src="../Images/MENUICONS/ModuleMenu_Out.png" onclick="fn_HideModule()"
                                                width="30px" title="Close" alt=" " />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                            <div id="div_submenu" runat="server" class="divSubMenu dispFontStyle" style="overflow:auto">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divEntry" class="CenterHeight" style="width: 88%; float: left; left: 150px;
                top: 70px; position: fixed; border: 0px solid yellow;" runat="server">
                <iframe runat="server" id="ifrmaster" name="centerfrm" class="frames" frameborder="0"
                    allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;">
                </iframe>
            </div>
            <div id="divSearch" class="CenterHeight" style="width: 0px; float: left; left: 150px;
                top: 70px; position: fixed; border-left: 0px solid yellow">
                <iframe id="Searchfrm" name="Searchfrm" class="frames" frameborder="0" style="border-style: solid;
                    border-width: 0px;" src="../QueryForm/QueryForm.aspx"></iframe>
            </div>
            <div id="div_ActionIcon" style="right: 0px; float: right; top: 70px; border: 0px solid yellow;
                width: 70px; vertical-align: middle; position: absolute;" align="center">
                <asp:HiddenField ID="hid_toggle" runat="server" Value="CLOSE" />
                <asp:HiddenField ID="hf_bol_IMStart" runat="server" Value="TRUE" />
                <table width="100%">
                    <tr>
                        <td rowspan="11" valign="middle">
                            <div style="float: right; position: static; display: none; font-family: Verdana;
                                font-size: 12px; color: White; padding: 20px" id="divListLang">
                                <table cellpadding="10" style="color: White">
                                    <tr>
                                        <td align="right">
                                            <a id="lngCancel" onclick="fn_HideLang()">
                                                <img src="../Images/close.png" alt="Close" width="15px" height="15px" id="imgClose" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnEN" runat="server" ToolTip="English" CommandName="Language"
                                                CommandArgument="EN" OnClick="ChangeLanguage">English</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnAr" runat="server" ToolTip="العربية" CommandName="لغة"
                                                CommandArgument="AR" OnClick="ChangeLanguage">العربية</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="div_wf_query" runat="server" visible="false">
                                <img src="../Images/MENUICONS/search.png" id="imgwfquery" title="Search" alt="Search"
                                    style="cursor: pointer; cursor: hand;" class="ActiveImageSize" /></div>
                            <div id="div_Query" runat="server" visible="true">
                                <img src="../Images/MENUICONS/search.png" id="ibQuery" title="Search" alt="Search"
                                    onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                    style="cursor: pointer; cursor: hand;" class="ActiveImageSize" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="div_wf_Save" runat="server" visible="false">
                                <img src="../Images/MENUICONS/Save.png" alt="Save" title="Save" id="imgWfSave" class="ActiveImageSize" />
                            </div>
                            <div id="div_Save" runat="server" visible="true">
                                <img src="../Images/MENUICONS/Save.png" alt="Save" id="imgWfSave1" title="Save" class="ActiveImageSize"
                                    onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgClear" ImageUrl="../Images/MENUICONS/Clear.png" ToolTip="Clear"
                                class="ActiveImageSize" runat="server" OnClick="imgClear_Click" onmouseover="fn_ChangeBtnSize(this)"
                                onmouseout="fn_ChangeBtnReSize(this)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div onclick="fn_ShowIFrame('docUploadDownload','UDfrm',300)">
                                <a style="border: 0px" href="../UploadFile/DocFileUploadPopup.aspx?Mode=U" target="UDfrm">
                                    <img src="../Images/MENUICONS/up-download.png" title="Document Upload" alt="Document Upload"
                                        onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" class="ActiveImageSize" />
                                </a>
                            </div>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td style="display: none">
                            <asp:ImageButton ID="imgOrg" ImageUrl="../Images/MENUICONS/org.png" ToolTip="Organization"
                                runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                class="ActiveImageSize" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImgCurrency" ImageUrl="../Images/MENUICONS/Export.png" ToolTip="Export"
                                class="ActiveImageSize" runat="server" OnClick="ImgCurrency_Click" onmouseover="fn_ChangeBtnSize(this)"
                                onmouseout="fn_ChangeBtnReSize(this)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgWF" ImageUrl="../Images/MENUICONS/wf.png" ToolTip="Approve"
                                class="ActiveImageSize" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                OnClick="imgWF_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgbtnABR" ImageUrl="../Images/MENUICONS/abr.png" ToolTip="About the Record"
                                class="ActiveImageSize" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                OnClick="imgbtnABR_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="div_wf_lng" runat="server" visible="false">
                                <img src="../Images/MENUICONS/language.png" alt="Language" title="Language" id="imgLang"
                                    class="ActiveImageSize" />
                            </div>
                            <div id="div_lng" runat="server" visible="true">
                                <img src="../Images/MENUICONS/language.png" width="25px" height="25px" onclick="fn_ShowLang()"
                                    class="ActiveImageSize" title="Language" id="imgChngLang" alt="Language" onmouseover="fn_ChangeBtnSize(this)"
                                    onmouseout="fn_ChangeBtnReSize(this)" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImgHelp" ImageUrl="../Images/MENUICONS/help.png" ToolTip="Help"
                                class="ActiveImageSize" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="divfooter" style="float: right; color: White; font-family: Verdana; font-size: 10px;
            position: fixed; bottom: 0px; height: 15px;" align="right">
            Powered by Powered by VMV Systems Pvt Ltd
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="clear: both">
    </div>
    <div id="divWorkFlow">
        <asp:HiddenField ID="hfWF" runat="server" />
        <cc2:ModalPopupExtender ID="mpeWF" runat="server" TargetControlID="hfWF" PopupControlID="pnlWF"
            CancelControlID="btnWFClose" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlWF" runat="server">
            <div class="ConfirmForm">
                <table width="100%" id="tblWF" runat="server">
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button runat="server" CssClass="btn" ID="btnWFClose" Text="Close" Width="60px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:RadioButtonList ID="rblWF" runat="server" CssClass="lblBox">
                                <asp:ListItem Value="WFST01" Selected="True">Approve</asp:ListItem>
                                <asp:ListItem Value="WFST02">Reject</asp:ListItem>
                                <asp:ListItem Value="WFST04">Need For Clarification</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="txtBox"
                                Width="200px" Height="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblNeedRemarks" runat="server" Text="Please Type Remarks" CssClass="lblBox"
                                Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both">
            </div>
            <div id="divABR">
                <asp:HiddenField ID="hfABR" runat="server" />
                <cc2:ModalPopupExtender ID="mpeABR" runat="server" TargetControlID="hfABR" PopupControlID="pnlABR"
                    CancelControlID="btnABRClose" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlABR" runat="server">
                    <div class="ConfirmForm">
                        <table width="100%" id="Table1" runat="server">
                            <tr>
                                <td align="right">
                                    <asp:Button runat="server" CssClass="btn" ID="btnABRClose" Text="Close" Width="60px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="div_ABTREC" runat="server" class="lblBox">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
            <%--<div style="float: right" onclick="fn_ShowIFrame('LeaveRequest','Leavefrm',300)">
           <a style="border: 0px" href="../UploadFile/DocFileUploadPopup.aspx?ProgramID=0&Mode=U&ID=0"
               target="Leavefrm">
               <img src="../Images/MainPage/up-down-load.png" title="Document Upload" alt="Document Upload" /></a>
       </div>--%>
            <div id="docUploadDownload" style="display: none; position: fixed; right: 100px;
                top: 30%" class="divLinkDetails">
                <div style="float: right; width: 8%; background-color: transparent;">
                    <img src="../Images/close.png" alt="Close" id="imgAClose" style="display: none" onclick="fn_uploaddownloadclose()" />
                </div>
                <div style="clear: both">
                </div>
                <div>
                    <iframe width="700px" height="500px" scrolling="no" frameborder="0" id="UDfrm" name="UDfrm"
                        style="background-color: transparent"></iframe>
                </div>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
