﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using FIN.BLL;
using FIN.DAL;
namespace FIN.Client.MainPage
{
    public partial class MainPageNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                FillComboBox();
                
                DataTable dt_menulist = new DataTable();

                if (Session[FINSessionConstants.UserName] != null && Session[FINSessionConstants.PassWord] != null)
                {

                    lblOrgName.Text = VMVServices.Web.Utils.OrganizationName;
                    lblUName.Text = Session[FINSessionConstants.UserName].ToString();
                    lblModuleName.Text = Session[FINSessionConstants.ModuleDescription].ToString();

                    Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> Home </a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
                    hf_TreeLink.Value = Session["Tree_Link"].ToString();

                    dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetMenuList(Session[FINSessionConstants.UserName].ToString(), Session[FINSessionConstants.ModuleName].ToString(),Session[FINSessionConstants.Sel_Lng].ToString())).Tables[0];

                    System.Text.StringBuilder str_LinkQuery = new System.Text.StringBuilder();
                    string str_LinkWFQuery = string.Empty;

                    Boolean bol_incrI = true;
                    str_LinkQuery.Append("<table width='100%'>");

                    string str_parent_menu = "";
                    for (int i = 0; i < dt_menulist.Rows.Count; i = i + 0)
                    {
                        str_LinkQuery.Append("<tr>");
                        string str_subMenuData = "";
                        
                        for (int jLoop = 1; jLoop <= 4; jLoop++)
                        {
                            bol_incrI = true;
                            string str_LinkURL = "";


                            if (dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString().Length > 0)
                            {

                                str_parent_menu = dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString();

                                str_LinkURL = "<td><div onclick=\"ShowSubMenu('C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "')\" id='" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "' >" + dt_menulist.Rows[i]["PARENT_MENU"].ToString() + "</div></td>";

                                str_subMenuData += "<tr><td colspan='4'><div style=\"display:none\" id='C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "'><table width='100%'><tr>";
                                int submenucount = 1;
                                for (int kLoop = i ; kLoop < dt_menulist.Rows.Count; kLoop++)
                                {
                                    if (str_parent_menu == dt_menulist.Rows[kLoop]["PARENT_MENU_CODE"].ToString())
                                    {
                                        if (submenucount == 5)
                                        {
                                            submenucount = 1;
                                            str_subMenuData += "</tr><tr>";
                                        }
                                        str_subMenuData += "<td><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[kLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                        str_subMenuData += "ProgramID=" + dt_menulist.Rows[kLoop]["MENU_KEY_ID"].ToString() + "&";
                                        str_subMenuData += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                        str_subMenuData += QueryStringTags.ID.ToString() + "=0" + "&";
                                        str_subMenuData += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                        str_subMenuData += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                        str_subMenuData += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                        str_subMenuData += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                        str_subMenuData += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[kLoop]["REPORT_NAME"].ToString() + "'";
                                        str_subMenuData += "  target='centerfrm'><span>" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "</span></a></td>";
                                        submenucount += 1;
                                        i = i + 1;
                                        bol_incrI = false;
                                        if (kLoop >= dt_menulist.Rows.Count)
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                str_subMenuData += "</tr></table></div></td></tr>";
                                str_LinkQuery.Append(str_LinkURL);
                            }
                            else
                            {
                                str_LinkURL = "<td><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                str_LinkURL += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "'";


                                str_LinkURL += "  target='centerfrm'><span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a></td>";

                                str_LinkQuery.Append(str_LinkURL);
                            }

                            if (Request.QueryString["WF"] != null && Request.QueryString["Id"] != null)
                            {
                                if (dt_menulist.Rows.Count != i)
                                {
                                    if (dt_menulist.Rows[i]["SCREEN_CODE"].ToString() != null && dt_menulist.Rows[i]["SCREEN_CODE"].ToString() != string.Empty)
                                    {
                                        if (dt_menulist.Rows[i]["SCREEN_CODE"].ToString().ToUpper() == Request.QueryString["WF"].ToString().ToUpper())
                                        {
                                            str_LinkWFQuery = dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                            str_LinkWFQuery += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Update.ToString().Substring(0, 1) + "&";
                                            str_LinkWFQuery += QueryStringTags.ID.ToString() + "=" + Request.QueryString["Id"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                            str_LinkWFQuery += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "'";
                                            Session["str_LinkWFQuery"] = str_LinkWFQuery;
                                        }
                                    }
                                }
                            }

                            if (bol_incrI)
                                i = i + 1;
                            if (i >= dt_menulist.Rows.Count)
                            {
                                break;
                            }
                        }
                        str_LinkQuery.Append("</tr>");
                        if (str_subMenuData.Length > 0)
                        {
                            str_LinkQuery.Append(str_subMenuData);
                            str_subMenuData = "";
                        }

                    }

                    str_LinkQuery.Append("</table>");

                    div_Menu.InnerHtml = str_LinkQuery.ToString();

                    FIN.Client.ClsGridBase.DataMenuList = dt_menulist;

                    Session[FINSessionConstants.MenuData] = dt_menulist;

                    if (Request.QueryString["WF"] != null && Request.QueryString["Id"] != null && Session["str_LinkWFQuery"] != null)
                    {
                        if (ifrmaster != null)
                        {
                            ifrmaster.Attributes.Add("src", "/" + Session["str_LinkWFQuery"]);
                        }
                    }
                }
                else
                {
                    Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["LoginPage"].ToString());
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.GL.Organisation_BLL.getOrganizationDet(ref ddlOrg);
        }
        protected void ChangeLanguage(object sender, EventArgs e)
        {
            LinkButton lb_lang = (LinkButton)sender;
            Session["Sel_Lng"] = lb_lang.CommandArgument.ToString();
        }

        protected void imgClear_Click(object sender, ImageClickEventArgs e)
        {
            //string entryFrameSrc = ifrmaster.Attributes["src"].ToString();
            //Uri tmp_uri = new Uri(entryFrameSrc);
            //string form_id = HttpUtility.ParseQueryString(tmp_uri.Query).Get("ProgramID");
            string form_id = Session["ProgramID"].ToString();
            System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
            System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


            string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
            str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
            str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
            str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
            str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
            str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
            str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
            str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
            str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

            ifrmaster.Attributes.Add("src", str_LinkURL);
        }

        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
           // mpeOrg.Show();
            VMVServices.Web.Utils.OrganizationID = ddlOrg.SelectedValue.ToString();
            VMVServices.Web.Utils.OrganizationName = ddlOrg.SelectedItem.Text;
            lblOrgName.Text = VMVServices.Web.Utils.OrganizationName;
            DataTable dt = FIN.BLL.GL.Organisation_BLL.getCompanyCurrency(ddlOrg.SelectedValue.ToString());
            if (dt.Rows.Count > 0)
            {
                Session[FINSessionConstants.ORGCurrency] = dt.Rows[0][FINColumnConstants.CURRENCY_ID].ToString();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["Mode"].ToString() == FINAppConstants.Update && Session["StrRecordId"].ToString() != "0") 
                {
                    //UpdateWF(Session["FormCode"], Session["StrRecordId"].ToString(), rblWF.SelectedValue.ToString(), txtRemarks.Text);
                }
            }
        }

        protected void imgWF_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                //mpeWF.Show();

                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        protected void imgbtnABR_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
               // mpeABR.Show();

                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        
    }


}