﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPagePercentage_new.aspx.cs"
    Inherits="FIN.Client.MainPage.MainPagePercentage_new" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/menu_style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fn_onClientClick() {
            $("#hf_onClientClick").val(1);

        }
        function fn_ChangeBtnSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(35);
            $("#" + cntrlId).height(35);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('png', 'gif'));
        }
        function fn_ChangeBtnReSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(25);
            $("#" + cntrlId).height(25);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('gif', 'png'));
        }
        function ShowSubMenu(SubMenuDiv) {

            // alert(document.getElementById(SubMenuDiv));//.setAttribute("display", "inline");
            $("#" + SubMenuDiv).toggle('slow');
            //$("#" + SubMenuDiv).css("backgroundColor", "red");
            //$("#" + SubMenuDiv.replace('C_','')).css("backgroundColor", "red");

            var hid_val = $("#hf_PreviousSubMenu").val();
            if (hid_val == '') {
                $("#hf_PreviousSubMenu").val(SubMenuDiv)
            }
            else {
                //alert(hid_val);
                if (hid_val == SubMenuDiv) {
                }
                else {
                    $("#" + hid_val).css('display', 'none');
                    $("#hf_PreviousSubMenu").val(SubMenuDiv)
                }

            }
        }
        function fn_ResizeEntryScreen(screenName) {

            $("#hid_toggle").val('CLOSE');
            $("#hf_IdelMinutes").val(0);
            $("#hf_bol_IMStart").val("TRUE")
            //alert($("#hid_toggle").val);
            $('#divSearch').css("width", "0px");
            $('#divEntry').css("width", "89%");
            $('#divSearch').hide();
            $('#divEntry').show();
            $("#MenuContainer").slideUp();
            $("#SlideUp").css("display", "none");
            $("#slidedown").css("display", "inline");
            // alert(screenName);
            if (screenName.length > 0) {
                $('#hf_ScreenName').val(screenName);
            }
            $("#div_MenuTree").html($("#" + '<%= hf_TreeLink.ClientID %>').val() + " --> " + $('#hf_ScreenName').val());
            document.getElementById('Searchfrm').contentDocument.location.reload(true);
            $("#ifrmaster").contents().find("body").html('');

            $("#docUploadDownload").css("display", "none");

            $("#divIconSet").css("display", "inline");


        }
        function fn_getSaveBtn() {

            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                btn = y.getElementById("ctl00_FINContent_btnSave");
                if (btn != null) {
                    btn.click();
                } else {
                    alert('Invalid Save Button Click');
                }
            }

        }

        $(function () {
            $(document).ready(function () {
                fn_ResizeEntryScreen('');
                $('#divSearch').hide();
                $("#divIconSet").css("display", "inline");
                $('#divEntry').css("width", "90%");
                fn_AfterClientClick();
            });
            $("#slidedown").click(function (event) {
                //alert('Down _ inside');
                event.preventDefault();
                $("#MenuContainer").slideDown();
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");

            });

            $("#SlideUp").click(function (event) {
                //alert('Up _ inside');
                event.preventDefault();
                $("#MenuContainer").slideUp();
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");

            });
        });
        function fn_AfterClientClick() {
            if ($("#hf_onClientClick").val() == "1") {
                $('#divEntry').css("width", "89%");
                $("#divIconSet").css("display", "inline");
            }
        }

        function fn_QueryFormDisplay() {

            var hid_val = $("#hid_toggle").val();
            $("#hf_IdelMinutes").val(0);
            $("#hf_bol_IMStart").val("TRUE")
            $("#Searchfrm").contents().find("body").html('');
            if (hid_val == 'CLOSE') {
                $("#hid_toggle").val('OPEN');
                $('#divSearch').css("width", "89%");
                //  $('#divSearch').show();
                // $('#divEntry').hide();                
                $('#divEntry').css("width", "0px");
                $("#divSearch").slideDown('slow');
                var sfrm = document.getElementById('Searchfrm');
                sfrm.src = sfrm.src;

            }
            else {
                $("#hid_toggle").val('CLOSE');
                $('#divSearch').css("width", "0px");
                // $('#divSearch').hide();
                // $('#divEntry').show();                
                $('#divEntry').css("width", "89%");

            }
            return false;
        }    
    </script>
    <style type="text/css">
        .divTR
        {
            clear: both;
            height: 2px;
            background-color: rgb(217,219,221);
        }
        .divTD
        {
            float: left;
            width: 250px;
            background-color: rgb(10,158,243);
        }
        .divTDSpace
        {
            float: left;
            background-color: rgb(217,219,221);
            width: 30px;
        }
        .SMHeader
        {
            cursor: hand;
            color: White;
            border-style: none;
            border-width: 0px;
        }
        .SMHeaderDet
        {
            clear: both;
            background-color: rgb(10,158,243);
            border-style: none;
            border-width: 0px;
            width: 90%;
            margin-left: 10px;
            margin-bottom: 20px;
        }
        
        .divMenu
        {
            display: none;
            padding: 5px;
            border: 1px solid #ddd;
            background-color: rgb(217,219,221);
            position: absolute; /* used to over the  div tag(box) on header */
            font-family: Verdana;
            font-size: 12px;
            text-decoration: none;
        }
        .menu table
        {
            border-spacing: 10px 3px;
        }
        .menu tr td
        {
            background-color: rgb(10,158,243);
            width: 25%;
        }
        /* .menu table tr td table 
        {
             border-spacing: 10px 3px;
             background-color : White ;
        }
        .menu table tr td table  tr td
        {
             background-color : rgb(10,158,243);
        }*/
        .menu A:link
        {
            color: White;
            text-decoration: none;
            font-weight: normal;
        }
        .menu A:visited
        {
            color: White;
            text-decoration: none;
            font-weight: normal;
        }
        .menu A:active
        {
            color: black;
            text-decoration: none;
        }
        .menu A:hover
        {
            color: black;
            text-decoration: none;
            font-weight: bold;
        }
        .divContainer
        {
            width: 100%;
            height: 670px;
        }
        .divCenterContainer
        {
            /*background-image: url('../Images/MainPage/Center.png');*/
            background-repeat: no-repeat;
            background-color: White;
        }
        .CenterHeight
        {
            height: 540px; /*border:2px solid blue;*/
        }
        
        ::-webkit-scrollbar
        {
            width: 12px;
        }
        
        ::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
        }
        
        ::-webkit-scrollbar-thumb
        {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
    </style>
    <style type="text/css">
        .modal
        {
            /*  position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
            */
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: transparent;
            z-index: 999;
        }
    </style>
    <script type="text/javascript">
        function fn_CloseBrowser() {
            window.close();
        }
        function fn_ShowLang() {

            $("#divListLang").toggle('slow');
        }
        function fn_HideLang() {
            $("#divListLang").toggle('slow');
            //            document.getElementById('ifrmaster').contentWindow.fn_changeLng('<%= Session["Sel_Lng"] %>');
        }
        

    </script>
    <script type="text/javascript">

        function updateClock() {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = (currentHours < 12) ? "AM" : "PM";

            // Convert the hours component to 12-hour format if needed
            currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = (currentHours == 0) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;


            $("#clock").html(currentTimeString);

        }

        $(document).ready(function () {
            setInterval('updateClock()', 1000);
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            setInterval('fn_updateIdelMin()', 60000);
        });
        function fn_updateIdelMin() {
            if ($("#hf_bol_IMStart").val() == "TRUE") {
                var curIdelMin = parseInt($("#hf_IdelMinutes").val());
                curIdelMin = curIdelMin + 1;
                $("#hf_IdelMinutes").val(curIdelMin)

                if (curIdelMin == 1500) {
                    $("#hf_IdelMinutes").val(0);
                    $("#hf_bol_IMStart").val("FALSE");
                    //$("#btnRelogin").click();
                    $("#divReLogin").toggle("slow");
                }
                // alert(curIdelMin);
            }

        }
        function fn_reloginClose() {
            $("#hf_IdelMinutes").val(0);
            $("#hf_bol_IMStart").val("TRUE");
            $("#divReLogin").toggle('slow');

        }
        function fn_uploaddownloadclose() {
            $("#docUploadDownload").toggle('slow');
        }

    </script>
    <script type="text/javascript">
        //WINDOW Scroll Position
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
            if (!isNaN(scrollY)) {
                window.scrollTo(0, scrollY);
            }
        };
        window.onscroll = function () {
            var scrollY = document.body.scrollTop;
            if (scrollY == 0) {
                if (window.pageYOffset) {
                    scrollY = window.pageYOffset;
                }
                else {
                    scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                }
            }
            if (scrollY > 0) {
                var input = document.getElementById("scrollY");
                if (input == null) {
                    input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("id", "scrollY");
                    input.setAttribute("name", "scrollY");
                    document.forms[0].appendChild(input);
                }
                input.value = scrollY;
            }
        };
    </script>
    <link href="../Scripts/SlideMenu/styleMD.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/JQueryTab/jquery-ui.js"></script>
    <script type="text/javascript">
        function fn_showSlideMenu() {
            // alert('inside');
            $("#divSlideMenuContainer").css("display", "inline");
            $('#divSlideLeft').toggle("slide", { direction: 'left' }, 800);
            $('#divSlideRight').toggle('slide', { direction: 'right' }, '800');

            // $('#divSlideMenuContainer').toggle('slide', { direction: 'left' }, '800');


            if ($('#hf_slideMenu').val() == "CLOSE") {
                $('#hf_slideMenu').val("OPEN");
                $("#CenterContainer").css("display", "none");
                $("#divSlideMenuContainer").css("display", "inline");
            }
            else {
                $('#hf_slideMenu').val("CLOSE");
                $("#CenterContainer").css("display", "inline");
                $("#divSlideMenuContainer").css("display", "none");
            }

        }

        $(function () {
            $(document).ready(function () {
                if ($('#hf_slideMenu').val() == "CLOSE") {

                    $("#CenterContainer").css("display", "inline");
                    $("#divSlideMenuContainer").css("display", "none");
                }
                else {

                    $("#CenterContainer").css("display", "none");
                    $("#divSlideMenuContainer").css("display", "inline");
                }
            });

        });

        function fn_ModuleMouseOver(imgId, txtId, imgName) {
            $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_IN.png');
            $("#" + txtId).attr("class", "divModuleTextIN");
        }
        function fn_ModuleMouseOut(imgId, txtId, imgName) {
            $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_OUT.png');
            $("#" + txtId).attr("class", "divModuleText");
        }
        function fn_ModuleClick(ClickedMoudle) {

            $("#hf_MoudleName").val(ClickedMoudle);
            $("#btnModuleCLick").click();
        }

    </script>
</head>
<body style="margin: 0px 0px 0px 0px; overflow: hidden">
    <form id="form1" runat="server">
    <div class="divContainer">
        <div id="divHeader">
            <div style="height: 100px; width: 100%; background: url('../images/MHeaderRight.jpg')">
                <div style="background: url('../images/MainPage/MHeader_B.png') no-repeat left; height: 100px">
                    <div style="float: left">
                        <table style="height: 100px">
                            <tr valign="middle" style="height: 100px">
                                <td style="padding-left: 10px">
                                    <%--<img src="../Images/Logo.png" />--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: right; padding: 10px 30px 0 0; display: none">
                        <table>
                            <tr>
                                <td>
                                    <div class="lblBox" style="float: left; width: 110px; font-weight: bolder" id="lblOrg">
                                        Organization
                                    </div>
                                </td>
                                <td>
                                    <div class="lblBox" style="float: left; font-weight: bolder" id="Div1">
                                        <asp:Label ID="lblOrgName" runat="server" Text="Organization"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td>
                                    <div class="lblBox" style="float: left; width: 110px; font-weight: bolder; display: none;"
                                        id="divModuleName">
                                        Module Name
                                    </div>
                                </td>
                                <td>
                                    <div class="lblBox" style="float: left; font-weight: bolder" id="Div4">
                                        <asp:Label ID="lblModuleName" runat="server" Text="Module Name"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="lblBox" style="float: left; width: 110px; font-weight: bolder" id="divTime">
                                        Time
                                    </div>
                                </td>
                                <td>
                                    <div class="lblBox" style="float: left; font-weight: bolder" id="clock">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="divCenter">
            <div style="height: 20px; width: 100%; background-color: rgb(10,158,243);" id="divMenuLink">
                <div style="float: left; padding-top: 5px;">
                    <img style="padding-left: 10px" src="../Images/MainPage/SlideDown_B.png" width="60px"
                        height="20px" id="slidedown" />
                    <img src="../Images/MainPage/SlideUp_B.png" width="60px" height="20px" id="SlideUp"
                        style="padding-left: 10px; display: none" />
                    <img alt="" src="../Scripts/SlideMenu/menu.jpg" width="60px" height="20px" onclick="fn_showSlideMenu()" />
                </div>
                <div id="div_MenuTree" style="float: left; padding-left: 10px; color: White; font-family: Verdana;
                    font-size: 12px;">
                </div>
                <div style="float: right; padding-right: 20px; padding-top: 0px">
                    <%--<a onclick="fn_CloseBrowser()">
                        <img src="../Images/BrowserClose.gif" alt="Close" width="20px" height="20px" onclick="fn_CloseBrowser()"
                            id="imgbtnClose1" /></a>--%>
                    <table style="border-spacing: 10px 0px;">
                        <tr valign="top">
                            <td>
                                <div class="lblBox" style="float: left; font-weight: bolder; color: White;" id="lblUserName">
                                    User
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left; font-weight: bolder; color: White" id="Div3">
                                    <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                                    &nbsp;&nbsp;
                                </div>
                            </td>
                            <td>
                                <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                                    <img src="../Images/MainPage/settings-icon_B.png" alt="Change Password" width="15px"
                                        height="15px" title="Change Password" />
                                </a>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/MainPage/home-icon_B.png"
                                    ToolTip="Home" Width="15px" Height="15px" />
                            </td>
                            <td>
                                <a onclick="fn_CloseBrowser()" style="cursor: pointer; cursor: hand;">
                                    <img src="../Images/MainPage/logout-icon_B.png" alt="Close" width="15px" height="15px"
                                        title="Close" onclick="fn_CloseBrowser()" id="imgbtnClose1" /></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both; height: 2px;">
            </div>
            <div style="width: 99%;" id="MenuContainer" class="divMenu">
                <div class="menu" runat="server" id="div_Menu">
                </div>
            </div>
            <div id="divSlideMenuContainer" style="display: none; width: 100%">
                <div id="divSlideLeft" style="float: left; width: 25%; background-color: #047b99;
                    display: none">
                    <table border="0px" width="100%">
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=GL">
                                    <div id="divGL" onmouseover="fn_ModuleMouseOver('imgGL','divGLtext','GL')" onmouseout="fn_ModuleMouseOut('imgGL','divGLtext','GL')">
                                        <div id="divGLImg" style="float: left">
                                            <img id="imgGL" src="../Images/MMPercentage/GL_OUT.png" alt="GL" title="GL" class="ModuleIconSize" />
                                        </div>
                                        <div id="divGLtext" class="divModuleText" style="float: left; text-align: center;
                                            padding-top: 10px">
                                            General Ledger
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <%-- <td rowspan="5" align="center" style="width: 30%">
                                <img src="../Images/MMPercentage/CenterGIF.gif" alt=" " title=" " width="100%" height="300px" />
                            </td>--%>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=FA">
                                    <div id="divFA" onmouseover="fn_ModuleMouseOver('imgFA','divFAtext','FA')" onmouseout="fn_ModuleMouseOut('imgFA','divFAtext','FA')">
                                        <div id="divFAImg" style="float: left">
                                            <img id="imgFA" src="../Images/MMPercentage/FA_OUT.png" alt="FA" title="FA" class="ModuleIconSize" />
                                        </div>
                                        <div id="divFAtext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Fixed Asset
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=AP">
                                    <div id="divAP" onmouseover="fn_ModuleMouseOver('imgAP','divAPtext','AP')" onmouseout="fn_ModuleMouseOut('imgAP','divAPtext','AP')">
                                        <div id="divAPImg" style="float: left">
                                            <img id="imgAP" src="../Images/MMPercentage/AP_OUT.png" alt="AP" title="AP" class="ModuleIconSize" />
                                        </div>
                                        <div id="divAPtext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Account Payable
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=AR">
                                    <div id="divAR" onmouseover="fn_ModuleMouseOver('imgAR','divARtext','AR')" onmouseout="fn_ModuleMouseOut('imgAR','divARtext','AR')">
                                        <div id="divARImg" style="float: left">
                                            <img id="imgAR" src="../Images/MMPercentage/AR_OUT.png" alt="AR" title="AR" class="ModuleIconSize" />
                                        </div>
                                        <div id="divARtext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Account Receivables
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=HR">
                                    <div id="divHR" onmouseover="fn_ModuleMouseOver('imgHR','divHRtext','HR')" onmouseout="fn_ModuleMouseOut('imgHR','divHRtext','HR')">
                                        <div id="divHRImg" style="float: left">
                                            <img id="imgHR" src="../Images/MMPercentage/HR_OUT.png" alt="HR" title="HR" class="ModuleIconSize" />
                                        </div>
                                        <div id="divHRtext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Human Resource
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=SS">
                                    <div id="divSS" onmouseover="fn_ModuleMouseOver('imgSS','divSStext','SS')" onmouseout="fn_ModuleMouseOut('imgSS','divSStext','SS')">
                                        <div id="divSSImg" style="float: left">
                                            <img id="imgSS" src="../Images/MMPercentage/SS_OUT.png" alt="SS" title="SS" class="ModuleIconSize" />
                                        </div>
                                        <div id="divSStext" class="divModuleText" style="float: left; padding-top: 10px">
                                            System Setup
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=CM">
                                    <div id="divCM" onmouseover="fn_ModuleMouseOver('imgCM','divCMtext','CM')" onmouseout="fn_ModuleMouseOut('imgCM','divCMtext','CM')">
                                        <div id="divCMImg" style="float: left">
                                            <img id="imgCM" src="../Images/MMPercentage/CM_OUT.png" alt="CM" title="CM" class="ModuleIconSize"
                                                class="ModuleIconSize" />
                                        </div>
                                        <div id="divCMtext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Cash Management
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=Loan">
                                    <div id="divLoan" onmouseover="fn_ModuleMouseOver('imgLoan','divLoantext','Loan')"
                                        onmouseout="fn_ModuleMouseOut('imgLoan','divLoantext','Loan')">
                                        <div id="divLoanImg" style="float: left">
                                            <img id="imgLoan" src="../Images/MMPercentage/Loan_OUT.png" alt="Loan" title="Loan"
                                                class="ModuleIconSize" />
                                        </div>
                                        <div id="divLoantext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Loan
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-right: 50px">
                                <a target='ParentMenu' href="LeftFrame.aspx?ModuleCode=Payroll">
                                    <div id="divPayroll" onmouseover="fn_ModuleMouseOver('imgPayroll','divPayrolltext','Payroll')"
                                        onmouseout="fn_ModuleMouseOut('imgPayroll','divPayrolltext','Payroll')">
                                        <div id="divPayrollImg" style="float: left">
                                            <img id="imgPayroll" src="../Images/MMPercentage/Payroll_OUT.png" alt="Payroll" title="Payroll"
                                                class="ModuleIconSize" />
                                        </div>
                                        <div id="divPayrolltext" class="divModuleText" style="float: left; padding-top: 10px">
                                            Payroll
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-left: 50px">
                                <div id="div22">
                                    <asp:Button ID="btnModuleCLick" runat="server" Text="" Style="display: none" OnClick="btnModuleCLick_Click" />
                                    <asp:HiddenField runat="server" ID="hf_MoudleName" Value="" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divSlideRight" style="float: right; width: 70%; background-color: Yellow;
                    display: none">
                    <iframe runat="server" id="ParentMenu" name="ParentMenu" class="frames" frameborder="0"
                        allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;
                        padding-left: 5px;"></iframe>
                    <div id="divDynamic_Menu" runat="server">
                    </div>
                </div>
                <asp:HiddenField ID="hf_slideMenu" runat="server" Value="CLOSE" />
            </div>
            <div style="width: 100%;" id="CenterContainer" class="divCenterContainer">
                <div style="float: left; width: 100%" id="DivCenter">
                    <div id="divEntry" class="CenterHeight" style="width: 85%; float: left; border: 0px solid gray;"
                        runat="server">
                        <iframe runat="server" id="ifrmaster" name="centerfrm" class="frames" frameborder="0"
                            src="../DashBoard.aspx" allowtransparency="true" style="border-style: solid;
                            border-width: 0px; background-color: transparent; padding-left: 5px;"></iframe>
                    </div>
                    <div id="divSearch" class="CenterHeight" style="width: 0px; float: left; border-left: 0px solid gray">
                        <iframe id="Searchfrm" name="Searchfrm" class="frames" frameborder="0" allowtransparency="false"
                            style="border-style: solid; border-width: 0px; background-color: white; padding-left: 5px;"
                            src="../QueryForm/QueryForm.aspx"></iframe>
                    </div>
                    <div class="CenterHeight" align="right" style="float: right; width: 10%; padding-top: 40px;
                        display: none; border: 0px solid black" id="divIconSet">
                        <div align="right" style="height: 100%; float: right; background-image: url('../Images/MainPage/curve-menu_B.png');
                            cursor: pointer; cursor: hand; background-repeat: no-repeat;" id="divSIcon">
                            <table width="115px">
                                <tr>
                                    <td rowspan="11" valign="middle">
                                        <div style="float: right; position: static; display: none; font-family: Verdana;
                                            font-size: 12px; color: White; padding: 20px" id="divListLang">
                                            <table cellspacing="10px" style="color: White">
                                                <tr>
                                                    <td align="right">
                                                        <a id="lngCancel" onclick="fn_HideLang()">
                                                            <img src="../Images/close.png" alt="Close" width="15px" height="15px" id="imgClose" /></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnEN" runat="server" ToolTip="English" CommandName="Language"
                                                            CommandArgument="EN" OnClick="ChangeLanguage">English</asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnAr" runat="server" ToolTip="العربية" CommandName="لغة"
                                                            CommandArgument="AR" OnClick="ChangeLanguage">العربية</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td style="height: 18px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 80px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%-- <img src="../Images/MainPage/search-icon.png" width="25px" height="25px" id="ibQuery" runat="server"
                                            title="Search" alt="Search" onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" />--%>
                                                    <div id="div_wf_query" runat="server" visible="false">
                                                        <img src="../Images/MainPage/search-icon_B.png" width="25px" height="25px" id="imgwfquery"
                                                            title="Search" alt="Search" style="cursor: pointer; cursor: hand;" /></div>
                                                    <div id="div_Query" runat="server" visible="true">
                                                        <img src="../Images/MainPage/search-icon_B.png" width="25px" height="25px" id="ibQuery"
                                                            title="Search" alt="Search" onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)"
                                                            onmouseout="fn_ChangeBtnReSize(this)" style="cursor: pointer; cursor: hand;" />
                                                    </div>
                                                </td>
                                                <td style="width: 10px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%--  <img src="../Images/MainPage/Save.png" width="25px" height="25px" id="Img1" alt="Save" runat="server"
                                            title="Save" onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                                    --%>
                                                    <div id="div_wf_Save" runat="server" visible="false">
                                                        <img src="../Images/MainPage/Save_B.png" width="25px" height="25px" alt="Save" title="Save"
                                                            id="imgWfSave" />
                                                    </div>
                                                    <div id="div_Save" runat="server" visible="true">
                                                        <img src="../Images/MainPage/Save_B.png" width="25px" height="25px" alt="Save" id="imgWfSave1"
                                                            title="Save" onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                                    </div>
                                                </td>
                                                <td style="width: 40px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 30px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%--<img src="../Images/MainPage/Clear.png" width="25px" height="25px" id="Img2" alt="Clear"  />--%>
                                                    <asp:ImageButton ID="imgClear" ImageUrl="../Images/MainPage/Clear_B.png" Width="25px"
                                                        ToolTip="Clear" Height="25px" runat="server" OnClick="imgClear_Click" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" OnClientClick="fn_onClientClick()" />
                                                </td>
                                                <td style="width: 60px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 20px">
                                                </td>
                                                <td style="width: 25px">
                                                    <div style="float: right" onclick="fn_ShowIFrame('docUploadDownload','UDfrm',300)">
                                                        <a style="border: 0px" href="../UploadFile/DocFileUploadPopup.aspx?Mode=U" target="UDfrm">
                                                            <img src="../Images/MainPage/up-down-load.png" title="Document Upload" alt="Document Upload"
                                                                height="25px" width="25px" />
                                                        </a>
                                                    </div>
                                                    <%--<asp:ImageButton ID="imgUpDownload" ImageUrl="../Images/MainPage/up-down-load.png" Width="25px"
                                                        ToolTip="Document Upload" Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" OnClick="getUploadedData" />--%>
                                                </td>
                                                <td style="width: 25px; display: none">
                                                    <asp:ImageButton ID="imgOrg" ImageUrl="../Images/MainPage/org_B.png" Width="25px"
                                                        ToolTip="Organization" Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" />
                                                </td>
                                                <td style="width: 70px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 15px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%--<img src="../Images/MainPage/s-icon.png" width="25px" height="25px" id="Img4" alt="$" title="Currency" runat="server"
                                            onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />--%>
                                                    <asp:ImageButton ID="ImgCurrency" ImageUrl="../Images/MainPage/export-B.png" Width="25px"
                                                        ToolTip="Export" Height="25px" runat="server" OnClick="ImgCurrency_Click" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" />
                                                </td>
                                                <td style="width: 75px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 17px">
                                                </td>
                                                <td style="width: 25px">
                                                    <asp:ImageButton ID="imgWF" ImageUrl="../Images/MainPage/wf_B.png" Width="25px" Height="25px"
                                                        ToolTip="Approve" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                                        OnClick="imgWF_Click" />
                                                </td>
                                                <td style="width: 73px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%-- <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25px">
                                    </td>
                                    <td style="width: 25px">
                                        <img src="../Images/MainPage/wf.png" width="25px" height="25px" id="Img6" alt="WFS" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                    </td>
                                    <td style="width: 65px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 25px">
                                                </td>
                                                <td style="width: 25px">
                                                    <asp:ImageButton ID="imgbtnABR" ImageUrl="../Images/MainPage/abr_B.png" Width="25px"
                                                        ToolTip="About the Record" Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" OnClick="imgbtnABR_Click" />
                                                </td>
                                                <td style="width: 65px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 40px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%--  <img src="../Images/MainPage/lang.png" width="25px" height="25px" onclick="fn_ShowLang()"  runat="server"
                                            id="Img8" alt="Language" title="Language" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                                    --%>
                                                    <div id="div_wf_lng" runat="server" visible="false">
                                                        <img src="../Images/MainPage/lang_B.png" width="25px" height="25px" alt="Language"
                                                            title="Language" id="imgLang" />
                                                    </div>
                                                    <div id="div_lng" runat="server" visible="true">
                                                        <img src="../Images/MainPage/lang_B.png" width="25px" height="25px" onclick="fn_ShowLang()"
                                                            title="Language" id="imgChngLang" alt="Language" onmouseover="fn_ChangeBtnSize(this)"
                                                            onmouseout="fn_ChangeBtnReSize(this)" /></div>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="width: 70px">
                                                </td>
                                                <td style="width: 25px">
                                                    <%--  <img src="../Images/MainPage/help.png" width="25px" height="25px" id="Img9" alt="Help" title="Help"
                                            onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                                    --%>
                                                    <asp:ImageButton ID="ImgHelp" ImageUrl="../Images/MainPage/help_B.png" Width="25px"
                                                        ToolTip="Help" Height="25px" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                                        onmouseout="fn_ChangeBtnReSize(this)" OnClick="imgHelp_Click" />
                                                </td>
                                                <td style="width: 20px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 100%; height: 15px; background-color: rgb(10,158,243); position: fixed;
            bottom: 0" id="divFooter">
            <div style="float: right; color: White; font-family: Verdana; font-size: 10px;">
                Powered by VMV Systems Pvt Ltd
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div id="divOrganization">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <cc2:ModalPopupExtender ID="mpeOrg" runat="server" TargetControlID="imgOrg" PopupControlID="pnlOrg"
                CancelControlID="btnClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <%-- <cc2:ModalPopupExtender ID="mpeUpDownload" runat="server" TargetControlID="imgUpDownload" PopupControlID="pnlUpDownload"
                CancelControlID="btnCloseUpDownload" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>--%>
            <asp:Panel ID="pnlOrg" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                    Organization
                                </div>
                            </td>
                            <td>
                                <div class="divtxtBox" style="float: left; width: 250px">
                                    <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <%-- <asp:Panel ID="pnlUpDownload" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="button" ID="btnCloseUpDownload" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <iframe id="frameUpload" runat="server" frameborder="0" style="width: 550px; height: 150px;"
                                        src="../UploadFile/DocFileUploadPopup.aspx"></iframe>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>--%>
        </div>
        <div style="clear: both">
        </div>
        <div id="divWorkFlow">
            <asp:HiddenField ID="hfWF" runat="server" />
            <cc2:ModalPopupExtender ID="mpeWF" runat="server" TargetControlID="hfWF" PopupControlID="pnlWF"
                CancelControlID="btnWFClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlWF" runat="server">
                <div class="ConfirmForm">
                    <table width="100%" id="tblWF" runat="server">
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button runat="server" CssClass="btn" ID="btnWFClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:RadioButtonList ID="rblWF" runat="server" CssClass="lblBox">
                                    <asp:ListItem Value="WFST01" Selected="True">Approve</asp:ListItem>
                                    <asp:ListItem Value="WFST02">Reject</asp:ListItem>
                                    <asp:ListItem Value="WFST04">Need For Clarification</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="txtBox"
                                    Width="200px" Height="50px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblNeedRemarks" runat="server" Text="Please Type Remarks" CssClass="lblBox"
                                    Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div style="clear: both">
        </div>
        <div id="div2">
            <asp:HiddenField ID="hdVideo" runat="server" />
            <cc2:ModalPopupExtender ID="mpeVideo" runat="server" TargetControlID="hdVideo" PopupControlID="pnlVideo"
                CancelControlID="btlVideoClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlVideo" runat="server">
                <div class="ConfirmForm">
                    <table width="100%" id="Table2" runat="server">
                        <tr>
                            <td>
                                <asp:DataList ID="gvVideo" Visible="true" runat="server" AutoGenerateColumns="false"
                                    RepeatColumns="4">
                                    <ItemTemplate>
                                        <video width="920px" height="600px" controls autoplay loop muted="muted">
                                          <source type="video/mp4"
                                              src="<%# Eval("FilePath") %>">  
                                        </video>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btlVideoClose" runat="server" Text="Close" CssClass="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div style="clear: both">
        </div>
        <div id="divABR">
            <asp:HiddenField ID="hfABR" runat="server" />
            <cc2:ModalPopupExtender ID="mpeABR" runat="server" TargetControlID="hfABR" PopupControlID="pnlABR"
                CancelControlID="btnABRClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlABR" runat="server">
                <div class="ConfirmForm">
                    <table width="100%" id="Table1" runat="server">
                        <tr>
                            <td align="right">
                                <asp:Button runat="server" CssClass="btn" ID="btnABRClose" Text="Close" Width="60px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="div_ABTREC" runat="server" class="lblBox">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <%--<div style="float: right" onclick="fn_ShowIFrame('LeaveRequest','Leavefrm',300)">
           <a style="border: 0px" href="../UploadFile/DocFileUploadPopup.aspx?ProgramID=0&Mode=U&ID=0"
               target="Leavefrm">
               <img src="../Images/MainPage/up-down-load.png" title="Document Upload" alt="Document Upload" /></a>
       </div>--%>
        <div id="docUploadDownload" style="display: none; position: fixed; right: 100px;
            top: 30%" class="divLinkDetails">
            <div style="float: right; width: 8%; background-color: transparent;">
                <img src="../Images/close.png" alt="Close" id="imgAClose" style="display: none" onclick="fn_uploaddownloadclose()" />
            </div>
            <div style="clear: both">
            </div>
            <div>
                <iframe width="700px" height="500px" scrolling="no" frameborder="0" id="UDfrm" name="UDfrm"
                    style="background-color: transparent"></iframe>
            </div>
        </div>
    </div>
    <div style="clear: both">
    </div>
    <div id="divReLogin" style="position: fixed; display: none; top: 30%; left: 10%;
        width: 800px; height: 380px; border-width: 1px; border-style: dashed; border-color: Black;
        overflow: hidden; background-color: White">
        <div id="divbtnreloginOk" align="right" style="width: 100%;">
            <img src="../Images/close.png" alt="Close" title="Close" width="15px" height="15px"
                onclick="fn_reloginClose()" />
        </div>
        <iframe runat="server" id="frmRelogin" name="frmRelogin" class="frames" frameborder="0"
            allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;"
            src="ReLogin.aspx" width="800px" height="300px" scrolling="no"></iframe>
    </div>
    <div style="clear: both">
    </div>
    <asp:HiddenField ID="hid_toggle" runat="server" Value="CLOSE" />
    <asp:HiddenField ID="hf_TreeLink" runat="server" Value="Home" />
    <asp:HiddenField ID="hf_PreviousSubMenu" runat="server" Value="" />
    <asp:HiddenField ID="hf_IdelMinutes" runat="server" Value="0" />
    <asp:HiddenField ID="hf_bol_IMStart" runat="server" Value="TRUE" />
    <asp:HiddenField ID="hf_pwdtryCount" runat="server" Value="0" />
    <asp:HiddenField ID="hf_onClientClick" runat="server" Value="0" />
    <asp:HiddenField ID="hf_ScreenName" runat="server" Value="" />
    <asp:Button ID="btnNotFoundLang" runat="server" OnClick="btnNotFoundLang_Click" Style="display: none"
        Text="Button" />
    <asp:HiddenField ID="hf_NotFoundLang" runat="server" Value="" />
    <div class="loading" align="center">
        <img src="../Images/loading.gif" alt="" />
    </div>
    <script src="../LanguageScript/MainPage.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
    <script type="text/javascript">
        document.write("<script src='../Scripts/JQueryValidatioin/jquery.validationEngine_" + '<%= Session["Sel_Lng"] %>' + ".js'><\/script>");
    </script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        function fn_ShowIFrame(frmDivId, ifrmid, ifrmheight) {

            //            if ($("#hf_LastMenu").val() != frmDivId) {
            //                $("#" + $("#hf_LastMenu").val()).css("display", "none");
            //            }
            $("#" + frmDivId).toggle("slow");
            var iFrameID = document.getElementById(ifrmid);
            iFrameID.height = ifrmheight + "px";
            //            $("#hf_LastMenu").val(frmDivId);
            //$("#LeaveRequest").offset({ top: 50, left: 50 });
        }

        function fn_ShowAlert() {

            $("#divAlert").toggle("slow");

            //$("#LeaveRequest").offset({ top: 50, left: 50 });
        }

        $("#btnSubmit").click(function (e) {
            ShowProgress();
        })
        $("#ddlOrg").change(function () {
            ShowProgress();
        });
    </script>
    <asp:HiddenField ID="hf_LastMenu" runat="server" Value="docUploadDownload" />
    </form>
</body>
</html>
