﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMVServices.Web;
using FIN.DAL;
using FIN.BLL;

namespace FIN.Client.MainPage
{
    public partial class WorkFlowList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AssignToWorkflow();
            }
        }


        private void AssignToWorkflow()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtWorkflow = DBMethod.ExecuteQuery(FINSQL.GetWorkFlowMonitor(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtWorkflow != null)
                {

                    Session["dtWorkflow"] = dtWorkflow;
                    gvWorkFlow.DataSource = dtWorkflow;
                    gvWorkFlow.DataBind();


                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        protected void gvWorkFlow_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnk_FormTarget = (LinkButton)e.Row.FindControl("lnkFormTarget");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {

                    string navigationMode = "WORKFLOW";

                    lnk_FormTarget.PostBackUrl = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&" + QueryStringTags.AddFlag + "=0&" + "&" + QueryStringTags.DeleteFlag + "=0&" + QueryStringTags.UpdateFlag + "=0&" + QueryStringTags.ReportName + "=''&" + QueryStringTags.Mode + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "&WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["workflow_code"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&LC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["Level_Code"].ToString() + "&MD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;


                }
            }
        }


        protected void gvWorkFlow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvWorkFlow.PageIndex = e.NewPageIndex;
                gvWorkFlow.DataSource = (DataTable)Session["dtWorkflow"];
                gvWorkFlow.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}