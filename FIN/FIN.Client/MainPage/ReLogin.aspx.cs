﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.BLL;
using VMVServices.Web;
using FIN.DAL;
namespace FIN.Client.MainPage
{
    public partial class ReLogin : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hf_S_OrganizationID.Value = HttpContext.Current.Session["OrganizationID"].ToString();
                hf_S_Multilanguage.Value = HttpContext.Current.Session["Multilanguage"].ToString();
                hf_S_ModuleName.Value = Session[FINSessionConstants.ModuleName].ToString();
                hf_S_ModuleDesc.Value = Session[FINSessionConstants.ModuleDescription].ToString();
                txtUserID.Text = Session["UserName"].ToString();
                txtUserID.Enabled = false;
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {

            SSM_USERS sSM_USERS = new SSM_USERS();
            Dictionary<string, string> Prop_File_Data;
             Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            try
            {
                ErrorCollection.Clear();

                lblError.Text = "";
                lblError.Visible = true;
                sSM_USERS = null;
                using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                {
                    sSM_USERS = userCtx.Find(r =>
                        (r.USER_CODE.ToUpper() == txtUserID.Text.ToUpper() && r.ATTRIBUTE1 != "EMPLOYEE")
                        ).SingleOrDefault();
                }
                if (sSM_USERS != null)
                {
                    if (sSM_USERS.PASSWORD_LOCK == FINAppConstants.Y)
                    {
                        
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["PasswordLocked_P"]);
                        lblError.Text = Prop_File_Data["PasswordLocked_P"];
                        return;
                    }

                    if (sSM_USERS.USER_PASSWORD.ToString() == EnCryptDecrypt.CryptorEngine.Encrypt(txtPassword.Text, true))
                    {
                        Session["userID"] = int.Parse(sSM_USERS.PK_ID.ToString());
                        Session["UserName"] = sSM_USERS.USER_CODE;
                        Session[FINSessionConstants.PassWord] = sSM_USERS.USER_PASSWORD;
                        //Session[FINSessionConstants.UserGroupID] = sSM_USERS.GROUP_ID;
                        Session[FINSessionConstants.UserName] = sSM_USERS.USER_CODE;
                        Session["FullUserName"] = sSM_USERS.FIRST_NAME + " " + sSM_USERS.MIDDLE_NAME + " " + sSM_USERS.LAST_NAME;

                        VMVServices.Web.Utils.UserName = sSM_USERS.USER_CODE;
                        VMVServices.Web.Utils.Multilanguage = false;
                       
                        HttpContext.Current.Session["OrganizationID"] = hf_S_OrganizationID.Value;
                        HttpContext.Current.Session["Multilanguage"] = hf_S_Multilanguage.Value;
                        Session[FINSessionConstants.ModuleName] = hf_S_ModuleName.Value;
                        Session[FINSessionConstants.ModuleDescription] = hf_S_ModuleDesc.Value;
                        lblError.Text = "Login Sccessfull. Please Close this Popup and Continue Your Work ";
                        ScriptManager.RegisterStartupScript(btnYes, typeof(Button), "CLose", "window.parent.fn_reloginClose()", true);
                    }
                    else
                    {
                        
                        hf_pwdtryCount.Value = (int.Parse(hf_pwdtryCount.Value) + 1).ToString();
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidPassword_P"]);
                        lblError.Text = Prop_File_Data["InvalidPassword_P"];
                        if (int.Parse(hf_pwdtryCount.Value) > 3)
                        {
                            sSM_USERS.PASSWORD_LOCK = FINAppConstants.Y;
                            DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);
                        }
                    }
                }
                else
                {
                    ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidUserName_P"]);
                    lblError.Text = Prop_File_Data["InvalidUserName_P"];
                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove(Prop_File_Data["LoginError_P"]);
                ErrorCollection.Add(Prop_File_Data["LoginError_P"], ex.Message);
                lblError.Text = ex.Message;
               
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
    }
}