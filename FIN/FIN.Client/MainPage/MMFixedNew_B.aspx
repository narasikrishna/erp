﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMFixedNew_B.aspx.cs" Inherits="FIN.Client.MainPage.MMFixedNew_B" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="../Scripts/Slide/jcarousellite_1.0.1.js" type="text/javascript"></script>
    <link href="../Scripts/Slide/Jcarousellite.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery.rotate.1-1.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

        });
        $(function () {
            $(".jCarouselLite").jCarouselLite({
                btnNext: ".prev",
                btnPrev: ".next",
                circular: true,
                visible: 5,
                speed: 1500,
                beforeStart: function (a) {
                    $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.gif');
                    $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.gif');
                    $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.gif');
                    $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.gif');
                    $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.gif');
                    $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.gif');
                    $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.gif');
                    $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.gif');
                    $("#<%= imgbtnLN.ClientID %>").attr("src", '../Images/Module/LN.gif');
                },
                afterEnd: function (a) {
                    $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.png');
                    $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.png');
                    $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.png');
                    $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.png');
                    $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.png');
                    $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.png');
                    $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.png');
                    $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.png');
                    $("#<%= imgbtnLN.ClientID %>").attr("src", '../Images/Module/LN.png');
                }
            });
        });

        function fn_OpenModule(ModuleName) {
            //            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP_OPEN.gif');
        }
        function fn_RotateModuel() {
            /* 
            $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.gif');
            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.gif');
            $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.gif');
            $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.gif');          
            $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.gif');
            $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.gif');
            $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.gif');
            $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.gif');*/
        }
        function fn_NormalModuel() {
            $("#<%= imgbtnGL.ClientID %>").attr("src", '../Images/Module/GL.png');
            $("#<%= imgbtnAP.ClientID %>").attr("src", '../Images/Module/AP.png');
            $("#<%= imgbtnHR.ClientID %>").attr("src", '../Images/Module/HR.png');
            $("#<%= imgbtnCA.ClientID %>").attr("src", '../Images/Module/CA.png');
            $("#<%= imgbtnPA.ClientID %>").attr("src", '../Images/Module/PA.png');
            $("#<%= imgbtnFA.ClientID %>").attr("src", '../Images/Module/FA.png');
            $("#<%= imgbtnAR.ClientID %>").attr("src", '../Images/Module/AR.png');
            $("#<%= imgbtnSSM.ClientID %>").attr("src", '../Images/Module/SSM.png');
            $("#<%= imgbtnLN.ClientID %>").attr("src", '../Images/Module/LN.png');
        }

        function fn_CloseBrowser() {
            window.close();
        }
        function fn_Showframe() {
            $("#divFrmContainer").toggle("slow");

        }
    </script>
    <script type="text/javascript">

        function updateClock() {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();

            // Pad the minutes and seconds with leading zeros, if required
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

            // Choose either "AM" or "PM" as appropriate
            var timeOfDay = (currentHours < 12) ? "AM" : "PM";

            // Convert the hours component to 12-hour format if needed
            currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

            // Convert an hours component of "0" to "12"
            currentHours = (currentHours == 0) ? 12 : currentHours;

            // Compose the string for display
            var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;


            $("#clock").html(currentTimeString);

        }

        $(document).ready(function () {
            setInterval('updateClock()', 1000);
        });
    </script>
    <script type="text/javascript">
        document.write("<script src='../Scripts/JQueryValidatioin/jquery.validationEngine_" + '<%= Session["Sel_Lng"] %>' + ".js'><\/script>");
    </script>
    <style type="text/css">
        body
        {
            margin: 0;
            padding: 0;
            height: 100%;
        }
        #container
        {
            min-height: 100%;
            position: relative;
        }
        #header
        {
            height: 100px;
        }
        #body
        {
            padding-bottom: 150px; /* Height of the footer */
        }
        #footer
        {
            position: fixed;
            bottom: 0;
            width: 100%;
            height: 15px; /* Height of the footer */
            background: rgb(10,158,243);
        }
        #footerTop
        {
            position: fixed;
            bottom: 15px;
            width: 100%;
            height: 150px; /* Height of the footer */
            background: white;
        }
    </style>
    <style type="text/css">
        .modal
        {
            /*  position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
            */
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: transparent;
            z-index: 999;
        }
    </style>
</head>
<body style="margin: 0px 0px 0px 0px">
    <form id="form1" runat="server">
    <div id="container">
        <div id="header" style="background-image: url('../Images/MainPage/MHeader_B.png')">
            <div style="float: right; padding: 10px 30px 0 0">
                <%--<table>
                    <tr>
                        <td>
                            <div class="lblBox" style="float: left; width: 110px; font-weight: bolder" id="lblUserName">
                                User
                            </div>
                        </td>
                        <td>
                            <div class="lblBox" style="float: left; font-weight: bolder" id="Div3">
                                <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="lblBox" style="float: left; width: 110px; font-weight: bolder" id="divTime">
                                Time
                            </div>
                        </td>
                        <td>
                            <div class="lblBox" style="float: left; font-weight: bolder" id="clock">
                            </div>
                        </td>
                    </tr>
                </table>--%>
            </div>
        </div>
        <div id="body">
            <div style="height: 20px; width: 100%; background-color: rgb(10,158,243);" id="divMenuLink">
                <div style="float: right; padding-right: 20px">
                    <table>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 50px; font-weight: bolder; color: White"
                                    id="lblUserName">
                                    User
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left; font-weight: bolder; color: White;" id="Div3">
                                    <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                                </div>
                            </td>
                            <td>
                                <div class="lblBox" style="float: left; font-weight: bolder; color: White; width: 50px"
                                    id="Div1" align="right">
                                    <a onclick="fn_CloseBrowser()">
                                        <img src="../Images/MainPage/logout-icon_B.png" alt="Close" width="15px" height="15px"
                                            onclick="fn_CloseBrowser()" /></a>
                                </div>
                            </td>
                            <td>
                                <img src="../Images/MainPage/settings-icon_B.png" alt="Change Password" width="15px"
                                    height="15px" title="Change Password" onclick="fn_Showframe()" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both; height: 10px;">
            </div>
            <div style="width: 50%; height: 100%; float: left;">
                <div class="myheadera" align="center">
                    <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
                        Style="width: 95%; height: 80%;" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
                        CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                        OnRowDataBound="gvWorkFlow_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                                DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
                            <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperMsgID" Text='<%#Eval("workflow_message") %>'
                                        Width="190px" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Text='<%#Eval("Level_Code") %>'
                                        Width="40px" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Text='<%#Eval("remarks") %>'
                                        Width="100px" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <PagerStyle CssClass="pgr"></PagerStyle>
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
            <div style="width: 50%; height: 100%; float: left;">
                <div class="myheadera" align="center">
                    <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc,ALERT_USER_LEVEL_CODE"
                        Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
                        PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderStyle-ForeColor="White" HeaderText="Alert Date" />
                            <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperAlertMsgID" Text='<%#Eval("alert_message") %>'
                                        Width="80%" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgCloseAlert" runat="server" ImageUrl="~/Images/close.png"
                                        Width="20px" Height="20px" OnClick="imgCloseAlert_Click" Visible="false" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" ForeColor="White"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <PagerStyle CssClass="pgr"></PagerStyle>
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div id="footerTop">
            <div align="center" style="width: 100%">
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                    Organization
                                </div>
                            </td>
                            <td>
                                <div class="divtxtBox" style="float: left; width: 250px">
                                    <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 100%; height: 100px;" align="center" runat="server" id="divModuleSel">
                <div class="carousel main" align="center" style="width: 1100px;">
                    <a class="prev" href="#"></a>
                    <div class="jCarouselLite" style="visibility: visible; overflow: hidden; position: relative;">
                        <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none;
                            z-index: 1;">
                            <li id="id_GL" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnGL" runat="server" ImageUrl="~/Images/Module/GL.png" OnClick="imgbtnGL_Click"
                                    ToolTip="General Ledger" Height="100px" Width="200px" /></li>
                            <li id="id_AP" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnAP" runat="server" ImageUrl="~/Images/Module/AP.png" OnClick="imgbtnAP_Click"
                                    ToolTip="Account Payable" Height="100px" Width="200px" onmouseover="fn_OpenModule('AP')"
                                    onmouseout="fn_NormalModuel()" /></li>
                            <li id="id_HR" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnHR" runat="server" ImageUrl="~/Images/Module/HR.png" OnClick="imgbtnHR_Click"
                                    ToolTip="Human Resource" Height="100px" Width="200px" /></li>
                            <li id="id_CA" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnCA" runat="server" ImageUrl="~/Images/Module/CA.png" OnClick="imgbtnCA_Click"
                                    ToolTip="Cash Management" Height="100px" Width="200px" /></li>
                            <li id="id_PA" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnPA" runat="server" ImageUrl="~/Images/Module/PA.png" OnClick="imgbtnPA_Click"
                                    ToolTip="Payroll" Height="100px" Width="200px" /></li>
                            <li id="id_FA" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnFA" runat="server" ImageUrl="~/Images/Module/FA.png" OnClick="imgbtnFA_Click"
                                    ToolTip="Fixed Assets" Height="100px" Width="200px" /></li>
                            <li id="id_AR" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnAR" runat="server" ImageUrl="~/Images/Module/AR.png" Height="100px"
                                    ToolTip="Account Receivables" Width="200px" OnClick="imgbtnAR_Click" /></li>
                            <li id="id_SSM" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnSSM" runat="server" ImageUrl="~/Images/Module/SSM.png"
                                    ToolTip="System Setup" Height="100px" Width="200px" OnClick="imgbtnSSM_Click1" /></li>
                            <li id="id_LN" runat="server" visible="true">
                                <asp:ImageButton ID="imgbtnLN" runat="server" ImageUrl="~/Images/Module/LN.png" ToolTip="Loan"
                                    Height="100px" Width="200px" OnClick="imgbtnLN_Click" /></li>
                        </ul>
                    </div>
                    <a class="next" href="#"></a>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
        <div id="divFrmContainer" style="display: none; position: fixed; width: 400px; height: 150px;
            right: 30px; top: 130px;">
            <iframe scrolling="no" runat="server" id="ifrmaster" name="frmContainer" class="frames"
                frameborder="0" allowtransparency="true" style="border-style: solid; border-width: 0px;
                background-color: White;" width="400px" height="150px" src="../HR/ChangePassword.aspx?ReqType=MainPage">
            </iframe>
        </div>
        <div id="footer">
            <div style="float: right; color: White; font-family: Verdana; font-size: 10px;">
                Powered by VMV Systems Pvt Ltd
            </div>
        </div>
    </div>
    <div class="loading" align="center">
        <img src="../Images/loading.gif" alt="" />
    </div>
    <script src="../LanguageScript/Generic.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        $("#ddlOrg").change(function () {
            ShowProgress();
        });
    </script>
    </form>
</body>
</html>
