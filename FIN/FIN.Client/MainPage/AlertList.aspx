﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AlertList.aspx.cs" Inherits="FIN.Client.MainPage.AlertList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc"
        Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
        PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
        <Columns>
            <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                HeaderStyle-ForeColor="White" HeaderText="Alert Date" />
            <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="White">
                <ItemTemplate>
                     <asp:LinkButton ID="lnkAlert" Style="text-decoration: none;" OnClientClick="fn_CallParentScript(' ')"
                                        Text='<%#Eval("alert_message") %>' runat="server" Width="190px"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
        <HeaderStyle CssClass="GridHeader" />
        <PagerStyle CssClass="pgr"></PagerStyle>
        <AlternatingRowStyle CssClass="GrdAltRow" />
    </asp:GridView>
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
