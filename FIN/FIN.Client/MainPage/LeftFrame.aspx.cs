﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VMVServices.Web;
using System.Data;
using FIN.DAL;
using FIN.BLL;

namespace FIN.Client.MainPage
{
    public partial class LeftFrame : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt_menulist = new DataTable();
            Session["Module"] = Request.QueryString["ModuleCode"];
             dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetMenuList(Session[FINSessionConstants.UserName].ToString(), Session["Module"].ToString(), Session[FINSessionConstants.Sel_Lng].ToString())).Tables[0];
            System.Text.StringBuilder str_LinkQuery = new System.Text.StringBuilder();
            string str_LinkWFQuery = string.Empty;

            Boolean bol_incrI = true;
            //str_LinkQuery.Append("<table width='100%' >");
            int int_menucolcount = 4;
            int int_resolution = 0;
            if (Session[FINSessionConstants.WindowWidth] != null)
            {
                if (int.TryParse(Session[FINSessionConstants.WindowWidth].ToString(), out int_resolution))
                {
                    if (int_resolution > 1090)
                    {
                    }
                    else
                    {
                        int_menucolcount = 3;
                    }
                }
            }

            string str_parent_menu = "";


            for (int i = 0; i < dt_menulist.Rows.Count; i = i + 0)
            {
                str_LinkQuery.Append("<div class='divTR'>&nbsp;</div>");
                string str_subMenuData = "";

                for (int jLoop = 1; jLoop <= int_menucolcount; jLoop++)
                {
                    bol_incrI = true;
                    string str_LinkURL = "";





                    if (dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString().Length > 0)
                    {

                        str_parent_menu = dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString();

                        str_LinkURL = "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><div class='SMHeader' onclick=\"ShowSubMenu('C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "')\" id='" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "' ><span> <img src='../Images/MainPage/grey.png' width='10px' height='10px' /> &nbsp;" + dt_menulist.Rows[i]["PARENT_MENU"].ToString() + "</div></div>";

                        str_subMenuData += "<div class='SMHeaderDet' style=\"display:none\" id='C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "'><div class='divTR' style='height:10px'>&nbsp;</div>";
                        int submenucount = 1;
                        for (int kLoop = i; kLoop < dt_menulist.Rows.Count; kLoop++)
                        {
                            if (str_parent_menu == dt_menulist.Rows[kLoop]["PARENT_MENU_CODE"].ToString())
                            {
                                if (submenucount == int_menucolcount + 1)
                                {
                                    submenucount = 1;
                                    str_subMenuData += "<div class='divTR'>&nbsp;</div>";
                                }
                                str_subMenuData += "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[kLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                str_subMenuData += "ProgramID=" + dt_menulist.Rows[kLoop]["MENU_KEY_ID"].ToString() + "&";
                                str_subMenuData += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                str_subMenuData += QueryStringTags.ID.ToString() + "=0" + "&";
                                str_subMenuData += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                str_subMenuData += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                str_subMenuData += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                str_subMenuData += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                str_subMenuData += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[kLoop]["REPORT_NAME"].ToString() + "&";
                                str_subMenuData += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[kLoop]["ATTRIBUTE3"].ToString() + "'";
                                str_subMenuData += "  target='centerfrm'><span> <img src='../Images/MainPage/white.png' width='10px' height='10px' /> </span> &nbsp;<span>" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "</span></a></div>";
                                submenucount += 1;
                                i = i + 1;
                                bol_incrI = false;
                                if (kLoop >= dt_menulist.Rows.Count)
                                {

                                    break;

                                }
                            }
                            else
                            {

                                break;

                            }
                        }
                        str_subMenuData += "</div>";
                        str_LinkQuery.Append(str_LinkURL);
                    }
                    else
                    {
                        str_LinkURL = "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                        str_LinkURL += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                        str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                        str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                        str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                        str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "&";
                        str_LinkURL += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[i]["ATTRIBUTE3"].ToString() + "'";

                        str_LinkURL += "  target='centerfrm'><span> <img src='../Images/MainPage/white.png' width='10px' height='10px' /> </span> &nbsp;<span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a></div>";

                        str_LinkQuery.Append(str_LinkURL);
                    }



                    if (bol_incrI)
                        i = i + 1;
                    if (i >= dt_menulist.Rows.Count)
                    {
                        break;
                    }
                }
                //str_LinkQuery.Append("</div>");
                if (str_subMenuData.Length > 0)
                {
                    str_LinkQuery.Append(str_subMenuData);
                    str_subMenuData = "";
                }

            }
            div_Menu.InnerHtml = str_LinkQuery.ToString();
     

        }

    }
}