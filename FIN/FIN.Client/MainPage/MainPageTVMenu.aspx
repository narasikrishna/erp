﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPageTVMenu.aspx.cs"
    Inherits="FIN.Client.MainPage.MainPageTVMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <script src="../Scripts/TreeMenu/jquery.min.js" type="text/javascript"></script>
    <%-- <link href="../Scripts/TreeMenu/mtree.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="../Scripts/JQueryTab/jquery-ui.js"></script>
    <style type="text/css">
        .mtree-demo .mtree
        {
            background: #EEE;
            margin: 20px auto;
            max-width: 320px;
            border-radius: 3px;
        }
        
        .mtree-skin-selector
        {
            text-align: center;
            background: #EEE;
            padding: 10px 0 15px;
        }
        
        .mtree-skin-selector li
        {
            display: inline-block;
            float: none;
        }
        
        .mtree-skin-selector button
        {
            padding: 5px 10px;
            margin-bottom: 1px;
            background: #BBB;
        }
        
        .mtree-skin-selector button:hover
        {
            background: #999;
        }
        
        .mtree-skin-selector button.active
        {
            background: #999;
            font-weight: bold;
        }
        
        .mtree-skin-selector button.csl.active
        {
            background: #FFC000;
        }
    </style>
    <script type="text/javascript">

        function fn_ShowIFrame(frmDivId, ifrmid, ifrmheight) {


            $("#" + frmDivId).toggle("slow");
            var iFrameID = document.getElementById(ifrmid);
            iFrameID.height = ifrmheight + "px";

        }

        function fn_ChangeBtnSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(35);
            $("#" + cntrlId).height(35);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('png', 'gif'));
        }
        function fn_ChangeBtnReSize(cntrl) {
            // alert('inside');
            //alert(a);
            var cntrlId = $(cntrl).attr('id');
            $("#" + cntrlId).width(35);
            $("#" + cntrlId).height(35);
            var imgsrc = $("#" + cntrlId).attr('src');
            //$("#" + cntrlId).attr("src", imgsrc.replace('gif', 'png'));
        }

        function fn_ResizeEntryScreen(screenName, str_iconset, cur_a) {

            $('#divMenuContainer').find("*").removeClass('current');
            $(cur_a).parent().addClass('current');

            $('#divSearch').css("width", "0px");
            $('#divEntry').css("width", "92%");
            $("#hid_toggle").val('CLOSE');
            $('#divSearch').hide();
            $('#divEntry').show();
            $("#hid_toggle").val('CLOSE');
            $("#divMenuContainer").hide("slide", { direction: 'left' }, 800);
            $("#SlideUp").css("display", "none");
            $("#slidedown").css("display", "inline");

            if (screenName.length > 0) {
                $('#hf_ScreenName').val(screenName);
            }
            $("#div_MenuTree").html($('#hf_ScreenName').val());

            document.getElementById('Searchfrm').contentDocument.location.reload(true);
            $("#ifrmaster").contents().find("body").html('');

            $("#docUploadDownload").css("display", "none");


            $("#divIconSet").css("display", str_iconset);

            if (str_iconset == 'none') {
                $('#divSearch').css("width", "0px");
                $('#divEntry').css("width", "98%");
            }

            if ($("#hf_ClearClick").val() == "Y") {
                $("#hf_ClearClick").val('N');
                $("#divIconSet").css("display", 'inline');
                $('#divSearch').css("width", "0px");
                $('#divEntry').css("width", "92%");
            }



        }

        function fn_QueryFormDisplay() {

            var hid_val = $("#hid_toggle").val();
            $("#Searchfrm").contents().find("body").html('');
            if (hid_val == 'CLOSE') {
                $("#hid_toggle").val('OPEN');
                $('#divSearch').css("width", "92%");
                $('#divEntry').css("width", "0px");
                $("#divSearch").slideDown('slow');
                // $("#divSearch").show("slide", { direction: 'left' }, 1200);
                // $("#divEntry").hide("slide", { direction: 'left' }, 1200);
                var sfrm = document.getElementById('Searchfrm');
                sfrm.src = sfrm.src;

            }
            else {
                $("#hid_toggle").val('CLOSE');
                $('#divSearch').css("width", "0px");
                $('#divEntry').css("width", "92%");
                // $("#divSearch").hide("slide", { direction: 'left' }, 1200);
                // $("#divEntry").show("slide", { direction: 'left' }, 1200);
            }
            return false;
        }
        $(function () {



            $("#slidedown").click(function (event) {
                //alert('Down _ inside');
                event.preventDefault();
                fn_setDivHeight();

                if ($("#hid_toggle").val() == 'CLOSE') {
                    // $('#divEntry').css("width", "67%");
                    $('#divEntry').animate({ width: '67%' }, 200);
                }
                else {
                    //$('#divSearch').css("width", "67%");
                    $('#divSearch').animate({ width: '67%' }, 200);
                }

                $("#divMenuContainer").show("slide", { direction: 'left' }, 800);
                $("#SlideUp").css("display", "inline");
                $("#slidedown").css("display", "none");



            });

            $("#SlideUp").click(function (event) {
                //alert('Up _ inside');
                event.preventDefault();
                fn_setDivHeight();
                $("#divMenuContainer").hide("slide", { direction: 'left' }, 200);
                $("#SlideUp").css("display", "none");
                $("#slidedown").css("display", "inline");
                if ($("#hid_toggle").val() == 'CLOSE') {
                    // $('#divEntry').css("width", "92%");
                    $('#divEntry').animate({ width: '92%' }, 800);
                }
                else {
                    // $('#divSearch').css("width", "92%");
                    $('#divSearch').animate({ width: '92%' }, 800);
                }

            });


        });

        function fn_getSaveBtn() {

            var x = document.getElementById("ifrmaster");
            var y = x.contentWindow.document;
            //y.body.style.backgroundColor = "red";
            var btn = y.getElementById("FINContent_btnSave");
            if (btn != null) {
                btn.click();
            }
            else {
                btn = y.getElementById("ctl00_FINContent_btnSave");
                if (btn != null) {
                    btn.click();
                } else {
                    alert('Invalid Save Button Click');
                }
            }

        }

        function fn_CloseBrowser() {
            window.close();
        }

        $(document).ready(function () {
            fn_ResizeEntryScreen($('#hf_ScreenName').val(), 'none', 'none');
            fn_setDivHeight();

        });

        function fn_setDivHeight() {
            var w_height = $(window).height() - 50;
            $('#divEntry').height(w_height);
            $('#divSearch').height(w_height);
            $('#divIconSet').height(w_height);
            $('#divMenuContainer').height(w_height);
        }
        function fn_HomeClick() {

            $('#hf_ScreenName').val(" ");
        }

        function fn_ShowLang() {

            $("#divListLang").toggle('slow');
        }
        function fn_HideLang() {
            $("#divListLang").toggle('slow');

        }
       
    </script>
    <style type="text/css">
        .imgIconSize
        {
            width: 35px;
            height: 35px;
        }
        .sidebar ul ul
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function fn_ShowAlert() {
            document.getElementById('frm_AlertList').contentDocument.location.reload(true);
            $("#div_Alert").toggle("slow");
        }
        function fn_ShowWorkFlow() {
            document.getElementById('frm_workFlow').contentDocument.location.reload(true);
            $("#div_workflow").toggle("slow");
        }
        /*
        $(document).ready(function () {
        $('.sidebar ul li a').click(function (ev) {
        $('.sidebar .sub-menu').not($(this).parents('.sub-menu')).slideUp();
        $('.sub-menu').css('background', 'white');
        $(this).next('.sub-menu').slideToggle();
        ev.stopPropagation();
        });
        });
        */
    </script>
    <script src="../Scripts/AccordionMenu/jquery.cookie.js" type="text/javascript"></script>
    <script src="../Scripts/AccordionMenu/jquery.dcjqaccordion.2.7.min.js" type="text/javascript"></script>
    <script src="../Scripts/AccordionMenu/jquery.hoverIntent.minified.js" type="text/javascript"></script>
    <link href="../Scripts/AccordionMenu/CSS/grey.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#accordion1').dcAccordion({
                eventType: 'click',
                autoClose: true,
                saveState: false,
                disableLink: true,
                speed: 'fast',
                classActive: 'test',
                showCount: false
            });
            /*
            classParent  : 'dcjq-parent',    //Class of parent menu item
            classActive  : 'active',    // Class of active parent link
            classArrow   : 'dcjq-icon',    // Class of span tag for parent arrows
            classCount   : 'dcjq-count',    // Class of span tag containing count (if addCount: true)
            classExpand : 'dcjq-current-parent',    // Class of parent li tag for auto-expand option
            eventType    : 'click',    // Event for activating menu - options are "click" or "hover"
            hoverDelay   : 300,    // Hover delay for hoverIntent plugin
            menuClose   : true,    // If set "true" with event "hover" menu will close fully when mouseout
            autoClose    : true,    // If set to "true" only one sub-menu open at any time
            autoExpand   : false,    // If set to "true" all sub-menus of parent tags with class 'classExpand' will expand on page load
            speed         : 'slow',    // Speed of animation
            saveState    : true,    // Save menu state using cookies
            disableLink  : true,    // Disable all links of parent items
            showCount : true,    // If "true" will add a count of the number of links under each parent menu item
            cookie  : 'dcjq-accordion'    // Sets the cookie name for saving menu state - each menu instance on a single page requires a unique cookie name.
  
            */


        });


        
    </script>
    <style type="text/css">
        .current
        {
            background-color: rgb(57,255,20);
            color: Black;
        }
    </style>
    <script type="text/javascript">
        function fn_ShowWFAlert() {
            $.ajax({
                type: "POST",
                url: "MainPageTVMenu.aspx/GetWFAlertCount",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var str_res = response.d.toString().split("@")

                    $("#divWFCount").text(str_res[0]);
                    $("#divAlterCount").text(str_res[1]);
                },
                failure: function (response) {
                    //alert(response.d);
                }
            });
        }
        $(document).ready(function () {
            setInterval(function () { fn_ShowWFAlert() }, 10000);
            fn_ShowWFAlert();
        });
    </script>
    <script type="text/javascript">
        function fn_ChangeToolTipLANG() {
            if ('<%= Session["Sel_Lng"] %>' == 'AR') {
                $('#img_changPassword').attr('title', 'تغيير كلمة المرور');
                $('#div_titlealert').attr('title', 'إنذار');
                $('#div_titlewf').attr('title', 'تدفق العمل');
                $('#img_titleHome').attr('title', 'الصفحة الرئيسية');
                $('#img_titleClose').attr('title', 'قريب');
                $('#imgwfquery').attr('title', 'سؤال');
                $('#ibQuery').attr('title', 'سؤال');
                $('#imgWfSave').attr('title', 'حفظ');
                $('#imgWfSave1').attr('title', 'حفظ');
                $('#imgClear').attr('title', 'واضح');
                $('#img_titleDU').attr('title', 'إيداع المستند');
                $('#imgOrg').attr('title', 'منظمة');
                $('#ImgCurrency').attr('title', 'تصدير');
                $('#imgWF').attr('title', 'الموافقة');
                $('#imgbtnABR').attr('title', 'حول هذا السجل');
                $('#imgLang').attr('title', 'لغة');
                $('#imgChngLang').attr('title', 'لغة');
                $('#ImgHelp').attr('title', 'مساعدة');
                $('#div_FooterIH').val("Powered by VMV Systems Pvt Ltd ");
                
            }
            else {
                $('#img_changPassword').attr('title', 'Change Password');
                $('#div_titlealert').attr('title', 'Alert');
                $('#div_titlewf').attr('title', 'Work Flow');
                $('#img_titleHome').attr('title', 'Home');
                $('#img_titleClose').attr('title', 'Close');
                $('#imgwfquery').attr('title', 'Query');
                $('#ibQuery').attr('title', 'Query');
                $('#imgWfSave').attr('title', 'Save');
                $('#imgWfSave1').attr('title', 'Save');
                $('#imgClear').attr('title', 'Clear');
                $('#img_titleDU').attr('title', 'Document Upload');
                $('#imgOrg').attr('title', 'Organization');
                $('#ImgCurrency').attr('title', 'Export');
                $('#imgWF').attr('title', 'Approve');
                $('#imgbtnABR').attr('title', 'About This Record');
                $('#imgLang').attr('title', 'Language');
                $('#imgChngLang').attr('title', 'Language');
                $('#ImgHelp').attr('title', 'Help');
                $('#div_FooterIH').val("مدعوم من VMV أنظمة المحدودة ");
                
            }
        }
        $(document).ready(function () {

            fn_ChangeToolTipLANG();
        });
    </script>
</head>
<body style="margin: 0px 0px 0px 0px;">
    <form id="form1" runat="server">
    <div id="div_Header" style="width: 100%; height: 100px; background: url('../images/MainPage/MHeader_B.png') no-repeat left;
        display: none;">
    </div>
    <div id="div_Controler" style="width: 100%; height: 25px; background-color: rgb(59,89,152)">
        <img src="../Images/MainPage/menu-icon.png" width="25px" height="25px" alt="Menu"
            title="Menu" style="padding-left: 10px; float: left" id="slidedown" />
        <img src="../Images/MainPage/menu-icon.png" width="25px" height="25px" alt="Menu"
            title="Menu" style="display: none; padding-left: 10px; float: left" id="SlideUp" />
        <div style="float: left; height: 100%; padding-left: 10px; border-right: 1px solid black">
        </div>
        <div id="div_MenuTree" style="float: left; padding-left: 20px; padding-top: 5px;
            color: White; font-family: Helvetica; font-size: 12px;">
        </div>
        <div style="float: right; padding-right: 20px; padding-top: 3px">
            <table style="border-spacing: 20px 0px;">
                <tr valign="top">
                    <td>
                        <div class="lblBox" style="float: left; font-weight: bolder; color: White; font-family: Helvetica;
                            font-size: 12px" id="Div3">
                            <asp:Label ID="lblUName" runat="server" Text="UserName"></asp:Label>
                            &nbsp;&nbsp;
                        </div>
                    </td>
                    <td>
                        <div title="Alert" style="float: left; width: 15px; height: 15px; background-image: url('../Images/alert-icon.png');
                            background-repeat: no-repeat" onclick="fn_ShowAlert()" id="div_titlealert">
                            <div style="float: left; background-color: Red; color: White" id="divAlterCount">
                            </div>
                        </div>
                    </td>
                    <td>
                        <div title="WorkFlow" style="float: left; width: 20px; height: 20px; background-image: url('../Images/wf_icon.png');
                            background-repeat: no-repeat" onclick="fn_ShowWorkFlow()" id="div_titlewf">
                            <div style="float: left; background-color: Red; color: White" id="divWFCount">
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="../HR/ChangePassword.aspx" target="centerfrm" id="hrefChangePassword">
                            <img src="../Images/MainPage/settings-icon_B.png" alt="Change Password" width="15px"
                                id="img_changPassword" height="15px" title="Change Password" />
                        </a>
                    </td>
                    <td>
                        <a href="WorkFlowAlert.aspx" target="centerfrm" id="A1" onclick="fn_HomeClick()">
                            <img src="../Images/MainPage/home-icon_B.png" title="Home" alt="Home" width="15px"
                                id="img_titleHome" height="15px" />
                        </a>
                    </td>
                    <td>
                        <a onclick="fn_CloseBrowser()" style="cursor: pointer; cursor: hand;">
                            <img src="../Images/MainPage/logout-icon_B.png" alt="Close" width="15px" height="15px"
                                id="img_titleClose" title="Close" onclick="fn_CloseBrowser()" id="imgbtnClose1" /></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="div_CenterContainer" style="width: 100%">
        <div id="divMenuContainer" style="float: left; width: 25%; height: 600px; overflow: auto;
            display: none; font-family: Helvetica; font-size: 12px;" class="grey demo-container">
            <div runat="server" id="div_Menu" style="height: 100%" class="sidebar">
            </div>
        </div>
        <!--
        <script src="../Scripts/TreeMenu/jquery.velocity.min.js" type="text/javascript"></script>
        <script src="../Scripts/TreeMenu/mtree.js" type="text/javascript"></script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
        -->
        <div id="divEntry" style="width: 92%; height: 600px; float: left; border: 0px solid gray;
            padding: 5px;" runat="server">
            <iframe runat="server" id="ifrmaster" name="centerfrm" frameborder="0" src="WorkFlowAlert.aspx"
                allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent; 
                padding-left: 5px; width: 100%; height: 100%;"></iframe>
        </div>
        <div id="divSearch" style="width: 0px; height: 600px; float: left; border-left: 0px solid gray;
            padding: 5px;">
            <iframe id="Searchfrm" name="Searchfrm" frameborder="0" allowtransparency="false"
                style="border-style: solid; border-width: 0px; background-color: white; padding-left: 5px;
                width: 100%; height: 100%;" src="../QueryForm/QueryForm.aspx"></iframe>
        </div>
        <div align="right" style="float: right; width: 5%; height: 600px; border: 0px solid black;
            display: none; cursor: pointer; cursor: hand;" id="divIconSet">
            <table style="height: 100%">
                <tr>
                    <td>
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    <div id="div_wf_query" runat="server" visible="false">
                                        <img src="../Images/MainPage/search.png" class="imgIconSize" id="imgwfquery" title="Search"
                                            alt="Search" style="cursor: pointer; cursor: hand;" /></div>
                                    <div id="div_Query" runat="server" visible="true">
                                        <img src="../Images/MainPage/search.png" class="imgIconSize" id="ibQuery" title="Search"
                                            alt="Search" onclick="fn_QueryFormDisplay()" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" style="cursor: pointer; cursor: hand;" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="div_wf_Save" runat="server" visible="false">
                                        <img src="../Images/MainPage/Save.png" class="imgIconSize" alt="Save" title="Save"
                                            id="imgWfSave" />
                                    </div>
                                    <div id="div_Save" runat="server" visible="true">
                                        <img src="../Images/MainPage/Save.png" class="imgIconSize" alt="Save" id="imgWfSave1"
                                            title="Save" onclick="fn_getSaveBtn()" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgClear" ImageUrl="../Images/MainPage/Cancel.png" class="imgIconSize"
                                        ToolTip="Clear" runat="server" OnClick="imgClear_Click" onmouseover="fn_ChangeBtnSize(this)"
                                        onmouseout="fn_ChangeBtnReSize(this)" OnClientClick="fn_ClearClick()" />
                                    <asp:HiddenField ID="hf_ClearClick" runat="server" Value="N" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="float: right" onclick="fn_ShowIFrame('docUploadDownload','UDfrm',300)">
                                        <a style="border: 0px" href="../UploadFile/DocFileUploadPopup.aspx?Mode=U" target="UDfrm">
                                            <img src="../Images/MainPage/Upload.png" title="Document Upload" alt="Document Upload"
                                                id="img_titleDU" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                                class="imgIconSize" />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px; display: none">
                                    <asp:ImageButton ID="imgOrg" ImageUrl="../Images/MainPage/org_B.png" class="imgIconSize"
                                         ToolTip="Organization" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                        onmouseout="fn_ChangeBtnReSize(this)" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImgCurrency" ImageUrl="../Images/MainPage/export.png" class="imgIconSize"
                                        ToolTip="Export" runat="server" OnClick="ImgCurrency_Click" onmouseover="fn_ChangeBtnSize(this)"
                                        onmouseout="fn_ChangeBtnReSize(this)" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgWF" ImageUrl="../Images/MainPage/workflow.png" class="imgIconSize"
                                        ToolTip="Approve" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                        OnClick="imgWF_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imgbtnABR" ImageUrl="../Images/MainPage/abr.png" class="imgIconSize"
                                        ToolTip="About the Record" runat="server" onmouseover="fn_ChangeBtnSize(this)"
                                        onmouseout="fn_ChangeBtnReSize(this)" OnClick="imgbtnABR_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="div_wf_lng" runat="server" visible="false">
                                        <img src="../Images/MainPage/language.png" class="imgIconSize" alt="Language" title="Language"
                                            id="imgLang" />
                                    </div>
                                    <div id="div_lng" runat="server" visible="true">
                                        <img src="../Images/MainPage/language.png" class="imgIconSize" onclick="fn_ShowLang()"
                                            title="Language" id="imgChngLang" alt="Language" onmouseover="fn_ChangeBtnSize(this)"
                                            onmouseout="fn_ChangeBtnReSize(this)" /></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImgHelp" ImageUrl="../Images/MainPage/help.png" class="imgIconSize"
                                        ToolTip="Help" runat="server" onmouseover="fn_ChangeBtnSize(this)" onmouseout="fn_ChangeBtnReSize(this)"
                                        OnClick="imgHelp_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="float: right; position: absolute; display: none; font-family: Verdana;
                top: 50%; left: 90%; font-size: 12px; color: White;" id="divListLang">
                <table cellspacing="10px" style="color: White">
                    <tr>
                        <td align="right">
                            <a id="lngCancel" onclick="fn_HideLang()">
                                <img src="../Images/close.png" alt="Close" width="15px" height="15px" id="imgClose" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnEN" runat="server" ToolTip="English" CommandName="Language"
                                CommandArgument="EN" OnClick="ChangeLanguage">English</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnAr" runat="server" ToolTip="العربية" CommandName="لغة"
                                CommandArgument="AR" OnClick="ChangeLanguage">العربية</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="width: 100%; height: 15px; background-color: rgb(59,89,152); position: fixed;
        bottom: 0" id="divFooter">
        <div style="float: right; color: White; font-family: Verdana; font-size: 10px;" id="div_FooterIH">
            Powered by VMV Systems Pvt Ltd
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divWorkFlow">
        <asp:HiddenField ID="hfWF" runat="server" />
        <cc2:ModalPopupExtender ID="mpeWF" runat="server" TargetControlID="hfWF" PopupControlID="pnlWF"
            CancelControlID="btnWFClose" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlWF" runat="server" Style="background-color: Gray">
            <div class="ConfirmForm">
                <table width="100%" id="tblWF" runat="server">
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button runat="server" CssClass="btn" ID="btnWFClose" Text="Close" Width="60px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:RadioButtonList ID="rblWF" runat="server" CssClass="lblBox">
                                <asp:ListItem Value="WFST01" Selected="True">Approve</asp:ListItem>
                                <asp:ListItem Value="WFST02">Reject</asp:ListItem>
                                <asp:ListItem Value="WFST04">Need For Clarification</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="txtBox"
                                Width="200px" Height="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblNeedRemarks" runat="server" Text="Please Type Remarks" CssClass="lblBox"
                                Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" OnClick="btnSubmit_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <div style="width: 500px; height: 500px; display: none; top: 25px; right: 25px; position: absolute;
        background-color: Gray;" id="div_workflow">
        <iframe runat="server" id="frm_workFlow" name="frm_workFlow" frameborder="0" src="WorkFlowList.aspx"
            allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;
            padding-left: 5px; width: 100%; height: 100%;"></iframe>
        <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
            Style="width: 95%; height: 80%; display: none" AllowPaging="true" PageSize="7"
            OnPageIndexChanging="gvWorkFlow_PageIndexChanging" CssClass="GridModule" EmptyDataText=" No Record Found"
            ShowHeaderWhenEmpty="True" OnRowDataBound="gvWorkFlow_RowDataBound">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                    ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                    DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
                <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:HyperLink Style="text-decoration: none;" ID="hyperMsgID" Target="centerfrm"
                            Text='<%#Eval("workflow_message") %>' Width="190px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Text='<%#Eval("Level_Code") %>'
                            Width="40px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Text='<%#Eval("remarks") %>'
                            Width="100px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <PagerStyle CssClass="pgr"></PagerStyle>
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div style="width: 500px; height: 500px; float: left; display: none; top: 25px; right: 25px;
        background-color: Gray; position: absolute;" id="div_Alert">
        <iframe runat="server" id="frm_AlertList" name="frm_AlertList" frameborder="0" src="AlertList.aspx"
            allowtransparency="true" style="border-style: solid; border-width: 0px; background-color: transparent;
            padding-left: 5px; width: 100%; height: 100%;"></iframe>
        <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc"
            Style="width: 95%; height: 80%; display: none" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
            CssClass="GridModule" PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
            OnRowDataBound="gvAlert_RowDataBound">
            <Columns>
                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                    ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                    HeaderStyle-ForeColor="White" HeaderText="Alert Date" />
                <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="White">
                    <ItemTemplate>
                        <asp:HyperLink Style="text-decoration: none;" ID="hyperAlertMsgID" Text='<%#Eval("alert_message") %>'
                            Width="190px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <PagerStyle CssClass="pgr"></PagerStyle>
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div id="divABR">
        <asp:HiddenField ID="hfABR" runat="server" />
        <cc2:ModalPopupExtender ID="mpeABR" runat="server" TargetControlID="hfABR" PopupControlID="pnlABR"
            CancelControlID="btnABRClose" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlABR" runat="server" Style="position: absolute; top: 50%; right: 10%">
            <div class="ConfirmForm" align="right">
                <table id="Table1" runat="server" style="background-color: Gray;">
                    <tr>
                        <td align="right">
                            <asp:Button runat="server" CssClass="btn" ID="btnABRClose" Text="Close" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="div_ABTREC" runat="server" class="lblBox">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <div id="docUploadDownload" style="display: none; position: fixed; right: 100px;
        top: 30%" class="divLinkDetails">
        <div style="float: right; width: 8%; background-color: transparent;">
            <img src="../Images/close.png" alt="Close" id="imgAClose" style="display: none" onclick="fn_uploaddownloadclose()" />
        </div>
        <div style="clear: both">
        </div>
        <div>
            <iframe width="700px" height="500px" scrolling="no" frameborder="0" id="UDfrm" name="UDfrm"
                style="background-color: transparent"></iframe>
        </div>
    </div>
    <div id="div2">
        <asp:HiddenField ID="hdVideo" runat="server" />
        <cc2:ModalPopupExtender ID="mpeVideo" runat="server" TargetControlID="hdVideo" PopupControlID="pnlVideo"
            CancelControlID="btlVideoClose" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlVideo" runat="server">
            <div class="ConfirmForm">
                <table width="100%" id="Table2" runat="server">
                    <tr>
                        <td>
                            <asp:DataList ID="gvVideo" Visible="true" runat="server" AutoGenerateColumns="false"
                                RepeatColumns="4">
                                <ItemTemplate>
                                    <video width="920px" height="600px" controls autoplay loop muted="muted">
                                        <source type="video/mp4" src="<%# Eval("FilePath") %>">
                                    </video>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btlVideoClose" runat="server" Text="Close" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField runat="server" ID="hf_ScreenName" Value="" />
    <asp:HiddenField runat="server" ID="hid_toggle" Value="CLOSE" />
    </form>
</body>
</html>
