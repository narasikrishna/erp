﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="PropertyProspectiveBuyEntry.aspx.cs" Inherits="FIN.Client.PMS.PropertyProspectiveBuyEntry" %>


<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
                Property Number
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtPropertyNumber" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
                Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDate" Enabled="false" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
                Property Name
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtPropertyName" CssClass="txtBox" Enabled="false" MaxLength="10"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
                Location
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                 <asp:DropDownList ID="ddlLocation" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
                Property Type
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlPropertyTyP" runat="server" TabIndex="2" 
                    CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
                Property Owner
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPropertyOwner" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div1">
              Total Property Value
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                  <asp:TextBox ID="txtTotalPropertyValue" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div2">
                Proposed Buy Value
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="TextBox1" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
           <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
              Total Area
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                  <asp:TextBox ID="TextBox2" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div4">
               Carpet Area
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="TextBox3" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div5">
              Number of Units
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                  <asp:TextBox ID="TextBox4" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div6">
               Buy Value
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="TextBox5" TabIndex="5" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="" Width="800px" ShowFooter="True">
                <Columns>
                 <asp:TemplateField HeaderText="Unit Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitNo" TabIndex="16" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUnitNo" TabIndex="6" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitNo" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Unit Area">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitArea" TabIndex="16" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUnitArea" TabIndex="6" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitArea" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Unit Proposed Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitProposedValue" TabIndex="16" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUnitProposedValue" TabIndex="6" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitProposedValue" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Location">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUnitLocation" runat="server" CssClass="ddlStype" TabIndex="17" Width="150px"
                                AutoPostBack="True">
                                <asp:ListItem Value="----Select----"></asp:ListItem>
                                <asp:ListItem Value="01">TEN0001</asp:ListItem>
                                <asp:ListItem Value="02">TEN0002</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlUnitLocation" runat="server" AutoPostBack="True" CssClass="ddlStype"
                                Width="150px" TabIndex="7">
                                <asp:ListItem Value="----Select----"></asp:ListItem>
                                <asp:ListItem Value="01">TEN0001</asp:ListItem>
                                <asp:ListItem Value="02">TEN0002</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitLocation" runat="server" Text='<%# Eval("GRN_NUM") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="ddlBranchName" TabIndex="6" Width="80px" MaxLength="18" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ddlBranchName" TabIndex="8" Width="80px" MaxLength="13" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("GRN_NUM") %>' AutoPostBack="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBranchName" Width="80px" runat="server" Text='<%# Eval("GRN_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                                  
                    
                  
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="14" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="15" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" TabIndex="23" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="24" runat="server" AlternateText="Cancel" 
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="13" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
