﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BuildingMasterEntry.aspx.cs" Inherits="FIN.Client.PMS.BuildingMasterEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
                Code
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="TextBox2" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
                Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtName" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
                Address
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtAddress" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
                City
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtCity" CssClass="validate[required] RequiredField txtBox" MaxLength="10"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer PopUpHeader" align="right">
                <div style="float: left">
                    <asp:Label ID="lblVendtl" runat="server" Text="Vendor Details" CssClass="lblBox"
                        Style="color: White"></asp:Label>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="lblSWIFTCode">
                    Code
                </div>
                <div class="divtxtBox" style="float: left; width: 152px">
                    <asp:DropDownList ID="DropDownList1" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer PopUpHeader" align="right">
                <div style="float: left">
                    <asp:Label ID="Label1" runat="server" Text="Unit Details" CssClass="lblBox" Style="color: White"></asp:Label>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
                    Total Units
                </div>
                <div class="divtxtBox" style="float: left; width: 155px">
                    <asp:TextBox ID="txtTotUnits" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
                <div class="colspace" style="float: left;">
                    &nbsp</div>
                <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
                    Parking Spaces
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:TextBox ID="txtParkingSpaces" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div5">
                Total
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtTotal" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer PopUpHeader" align="right">
                <div style="float: left">
                    <asp:Label ID="Label2" runat="server" Text="Contact Person Details" CssClass="lblBox"
                        Style="color: White"></asp:Label>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="Div3">
                    Name
                </div>
                <div class="divtxtBox" style="float: left; width: 155px">
                    <asp:TextBox ID="txtContactPersonName" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
                <div class="colspace" style="float: left;">
                    &nbsp</div>
                <div class="lblBox" style="float: left; width: 195px" id="Div4">
                    Office Phone No
                </div>
                <div class="divtxtBox" style="float: left; width: 152px">
                    <asp:TextBox ID="txtOfficePhoneNo" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div6">
                Designation
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtDesignation" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div7">
                Mobile No
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtMobNo" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div8">
                Email Address
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtEmail" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div9">
                Pager
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtPager" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div10">
                Res.Phone No
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtResphno" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div11">
                Fax
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtFax" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer PopUpHeader" align="right">
                <div style="float: left">
                    <asp:Label ID="Label3" runat="server" Text="Building Owner Details" CssClass="lblBox"
                        Style="color: White"></asp:Label>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="Div12">
                    Owner Code
                </div>
                <div class="divtxtBox" style="float: left; width: 155px">
                    <asp:DropDownList ID="ddlOwnercode" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
                <div class="colspace" style="float: left;">
                    &nbsp</div>
                <div class="lblBox" style="float: left; width: 195px" id="Div13">
                    Date
                </div>
                <div class="divtxtBox" style="float: left; width: 152px">
                    <asp:TextBox ID="txtDate" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div14">
                Name
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtBuildingOwnrName" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div15">
                Amount
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtAmount" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divRowContainer PopUpHeader" align="right">
                <div style="float: left">
                    <asp:Label ID="Label4" runat="server" Text="Contract Details" CssClass="lblBox" Style="color: White"></asp:Label>
                </div>
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 200px" id="Div16">
                    Opening Date
                </div>
                <div class="divtxtBox" style="float: left; width: 155px">
                    <asp:TextBox ID="txtOpeningDt" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
                <div class="colspace" style="float: left;">
                    &nbsp</div>
                <div class="lblBox" style="float: left; width: 195px" id="Div17">
                    Area Code
                </div>
                <div class="divtxtBox" style="float: left; width: 152px">
                    <asp:DropDownList ID="ddlAreacode" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Expiry Date
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:TextBox ID="txtExpiryDate" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblActive">
                Area Name
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAreaName" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <%--<script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
