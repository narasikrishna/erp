﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhotoCapture.aspx.cs" Inherits="FIN.Client.UploadFile.PhotoCapture" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>
    <script src='<%=ResolveUrl("../Webcam_Plugin/jquery.webcam.js") %>' type="text/javascript"></script>
    <script type="text/javascript">
        var pageUrl = '<%=ResolveUrl("../UploadFile/PhotoCapture.aspx") %>';
        $(function () {
            jQuery("#webcam").webcam({
                width: 320,
                height: 240,
                mode: "save",
                swffile: '<%=ResolveUrl("../Webcam_Plugin/jscam.swf") %>',
                debug: function (type, status) {
                  //  $('#camStatus').append(type + ": " + status + '<br /><br />');
                },
                onSave: function (data) {
                    $.ajax({
                        type: "POST",
                        url: pageUrl + "/GetCapturedImage",
                        data: '',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            $("[id*=imgCapture]").css("visibility", "visible");
                            $("[id*=imgCapture]").attr("src", r.d);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                },
                onCapture: function () {
                    webcam.save(pageUrl);
                }
            });
        });
        function Capture() {
            webcam.capture();
            return false;
        }
    </script>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td align="center">
                <u>Live Camera</u>
            </td>
            <td>
            </td>
            <td align="center">
                <u>Captured Picture</u>
            </td>
        </tr>
        <tr>
            <td>
                <div id="webcam">
                </div>
            </td>
            <td>
                <asp:ImageButton runat="server" ID="btnCapture" Text="Capture" Width="50px" OnClientClick="return Capture();"
                    ImageUrl="~/Images/webcam.png" /><br />
                <asp:ImageButton runat="server" ID="btnClose" Text="Capture" Width="50px" OnClick="btnSave_Click"
                    ImageUrl="~/Images/close.png" />
            </td>
            <td>
                <asp:Image ID="imgCapture"  runat="server" Style="visibility: inherit; width: 320px;
                    height: 240px"   />
                    
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnCapture11" Visible="false" Style="display: none;" Text="Capture"
        runat="server" Width="100px" OnClientClick="return Capture();" />
    &nbsp;
    <asp:Button Text="Cancel" ID="btnClose11" runat="server" Width="100px" OnClick="btnSave_Click"
        Style="display: none;" />
    <br />
    <span id="camStatus"></span>
    <div style="text-align: center">
        OR
    </div>
    <div>
        &nbsp;</div>
    <div style="float: left">
        <table width="350px" style="float: left">
            <tr>
                <td colspan="2" align="left">
                    <asp:FileUpload ID="fuAttachments" runat="server" CssClass="EntryFont  txtBox" Width="370px" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="fuAttachments" ValidationGroup="Upload"
                        CssClass="DisplayFont" ID="reqAttachemnts">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFileDet" runat="server" Text="File Det" Visible="false" CssClass="DisplayFont lblBox"> </asp:Label>
                </td>
                <td align="right">
                    <asp:Button ID="btnFileUpload" runat="server" Text="Upload" ValidationGroup="Upload"
                        CssClass="DisplayFont button" OnClick="btnFileUpload_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
