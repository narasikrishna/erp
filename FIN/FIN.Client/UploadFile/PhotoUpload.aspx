﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhotoUpload.aspx.cs" Inherits="FIN.Client.UploadFile.PhotoUpload" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="float: left">
    <table width="350px" style = "float:left">
            <tr>
                <td colspan="2" align = "left">
                    <asp:FileUpload ID="fuAttachments" runat="server" CssClass="EntryFont  txtBox" Width="370px"    />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="fuAttachments" ValidationGroup="Upload"
                        CssClass="DisplayFont" ID="reqAttachemnts">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFileDet" runat="server" Text="File Det" Visible="false" CssClass="DisplayFont lblBox"> </asp:Label>
                </td>
                <td align="right">
                    <asp:Button ID="btnFileUpload" runat="server" Text="Upload" ValidationGroup="Upload"
                        CssClass="DisplayFont button" OnClick="btnFileUpload_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
