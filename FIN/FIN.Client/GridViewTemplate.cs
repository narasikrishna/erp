using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//A customized class for displaying the Template Column
public class GridViewTemplate : ITemplate
{
    //A variable to hold the type of ListItemType.
    ListItemType _templateType;

    //A variable to hold the column name.
    string _columnName;
    string _disp_text;
    string _controlName;
    string _strURL;
    //Constructor where we define the template type and column name.
    public GridViewTemplate(ListItemType type, string colname, string disp_text)
    {
        //Stores the template type.
        _templateType = type;

        //Stores the column name.
        _columnName = colname;

        _disp_text = disp_text;
        _controlName = "LinkButton";

    }
    public GridViewTemplate(ListItemType type, string colname, string disp_text, string controlName)
    {
        //Stores the template type.
        _templateType = type;

        //Stores the column name.
        _columnName = colname;

        _disp_text = disp_text;
        _controlName = controlName;
    }

    void ITemplate.InstantiateIn(System.Web.UI.Control container)
    {
        switch (_templateType)
        {
            case ListItemType.Header:

                //Creates a new label control and add it to the container.
                Label lbl = new Label();            //Allocates the new label object.
                lbl.Text = _disp_text;             //Assigns the name of the column in the lable.
                lbl.CssClass = "SearchlblBox";                
                container.Controls.Add(lbl);        //Adds the newly created label control to the container.

                TextBox txt = new TextBox();
                txt.Text = "";
                txt.CssClass = "txtBox";
                txt.ID = _columnName;
                txt.Width = System.Web.UI.WebControls.Unit.Percentage(95);
                txt.Attributes.Add("onKeyPress", "fn_GridRecSearch(event,this,'" + _columnName + "')");
                txt.Attributes.Add("onKeyUp", "fn_GridMultiSearch(event,this,'" + _columnName + "')");
                container.Controls.Add(txt);

                //ImageButton img_btn = new ImageButton();
                //img_btn.ImageUrl = "../Images/Searchicon.png";
                //img_btn.Width = System.Web.UI.WebControls.Unit.Pixel(20);
                //img_btn.Height = System.Web.UI.WebControls.Unit.Pixel(20);
                //img_btn.Attributes.Add("onclientclick", "fn_searchclick()");


                //Table tbl = new Table();
                //tbl.Width = System.Web.UI.WebControls.Unit.Percentage(99);
                //tbl.BorderStyle = System.Web.UI.WebControls.BorderStyle.None;
                //tbl.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(0);
                //TableRow tbl_rw = new TableRow();
                //TableCell tbl_cl= new TableCell();
                //tbl_cl.ColumnSpan = 2;
                //tbl_cl.Controls.Add(lbl);
                //tbl_rw.Cells.Add(tbl_cl);
                //tbl.Rows.Add(tbl_rw);
                //tbl_cl = new TableCell();
                //tbl_rw = new TableRow();
                //tbl_cl.Controls.Add(txt);
                //tbl_rw.Cells.Add(tbl_cl);
                //tbl_cl = new TableCell();
                //tbl_cl.Controls.Add(img_btn);
                //tbl_rw.Cells.Add(tbl_cl);
                //tbl.Rows.Add(tbl_rw);
                //container.Controls.Add(tbl);
                break;

            case ListItemType.Item:
                if (_controlName == "DDL")
                {
                    //Creates a new text box control and add it to the container.
                    DropDownList tb1 = new DropDownList();                            //Allocates the new text box object.
                    tb1.ID = "grd_ddl_" + _columnName;

                    tb1.AutoPostBack = true;
                    //tb1.DataBinding += new EventHandler(tb1_DDLDataBinding);   //Attaches the data binding event.
                    //tb1.Columns = 4;                                        //Creates a column with size 4.
                    container.Controls.Add(tb1);
                }
                else if (_controlName == "LABEL")
                {
                    Label tb1 = new Label();                            //Allocates the new text box object.
                    tb1.DataBinding += new EventHandler(tb1_lblDataBinding);   //Attaches the data binding event.
                    //tb1.Columns = 4;                                        //Creates a column with size 4.
                    container.Controls.Add(tb1);
                }
                else
                {
                    //Creates a new text box control and add it to the container.

                    HyperLink tb1 = new HyperLink();                            //Allocates the new text box object.
                    tb1.DataBinding += new EventHandler(tb1_DataBinding);   //Attaches the data binding event.
                    //tb1.Columns = 4;                                       //Creates a column with size 4.      
                    tb1.Attributes.Add("onclick", "fn_onLinkClick()");
                    container.Controls.Add(tb1);


                }
                break;                     

            case ListItemType.EditItem:
                //As, I am not using any EditItem, I didnot added any code here.
                break;

            case ListItemType.Footer:
                CheckBox chkColumn = new CheckBox();
                chkColumn.ID = "Chk" + _columnName;
                container.Controls.Add(chkColumn);
                break;
        }
    }

    /// <summary>
    /// This is the event, which will be raised when the binding happens.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void tb1_DataBinding(object sender, EventArgs e)
    {
        HyperLink txtdata = (HyperLink)sender;
        GridViewRow container = (GridViewRow)txtdata.NamingContainer;
        object dataValue = DataBinder.Eval(container.DataItem, _columnName);
        object URLVlaue = DataBinder.Eval(container.DataItem, "ModifyURL");
        if (dataValue != DBNull.Value)
        {

            //if (_disp_text.ToUpper() == "EDIT")
            //{
            //    txtdata.Text = "<img src ='../Images/edit.gif'  style='border : 0 ;' />";
            //}
            //else
            //{
            //    txtdata.Text = "<img src ='../Images/Delete.jpg' style='border : 0 ;' />";
            //}
            txtdata.Text = dataValue.ToString();
            txtdata.CssClass = "GridDataStyle";            
            //txtdata.NavigateUrl = dataValue.ToString();
            txtdata.NavigateUrl = URLVlaue.ToString();
            txtdata.Target = "centerfrm";
            _strURL = dataValue.ToString();
        }
    }

    void tb1_lblDataBinding(object sender, EventArgs e)
    {
        Label txtdata = (Label)sender;
        GridViewRow container = (GridViewRow)txtdata.NamingContainer;
        object dataValue = DataBinder.Eval(container.DataItem, _columnName);
        if (dataValue != DBNull.Value)
        {
            txtdata.Text = dataValue.ToString();
        }
    }
}