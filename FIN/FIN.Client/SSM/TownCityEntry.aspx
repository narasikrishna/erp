﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TownCityEntry.aspx.cs" Inherits="FIN.Client.SSM.TownCityEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 950px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCountryCode">
                Country Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlCountryCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountryCode_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCountryName">
                Country Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtCountryName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblStateCode">
                State Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlStateCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStateCode_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblStateName">
                State Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStateName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PK_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="City Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCityCode" runat="server" Text='<%# Eval("CITY_CODE") %>' CssClass="EntryFont RequiredField txtBox"
                                TabIndex="5" MaxLength="50" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCityCode" runat="server" Text='' CssClass="EntryFont RequiredField txtBox"
                                TabIndex="5" MaxLength="50" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCityCode" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("CITY_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCityName" runat="server" Text='<%# Eval("FULL_NAME") %>' CssClass="EntryFont RequiredField txtBox_en"
                                TabIndex="6" MaxLength="100" Width="200px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCityName" runat="server" CssClass="EntryFont RequiredField txtBox_en"
                                TabIndex="6" MaxLength="100" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCityName" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Text='<%# Eval("FULL_NAME") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City Name(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCityNameol" runat="server" Text='<%# Eval("FULL_NAME_OL") %>'
                                CssClass="EntryFont txtBox_ol" TabIndex="7" MaxLength="100" Width="200px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCityNameol" runat="server" CssClass="EntryFont txtBox_ol" TabIndex="7"
                                MaxLength="100" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCityNameol" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Text='<%# Eval("FULL_NAME_OL") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City Short Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCityShortName" runat="server" Text='<%# Eval("SHORT_NAME") %>'
                                TabIndex="8" MaxLength="50" CssClass="EntryFont RequiredField txtBox" Width="150px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCityShortName" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="8" MaxLength="50" Width="150px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCityShortName" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Text='<%# Eval("SHORT_NAME") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkAct" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="9" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkAct" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkAct" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="9" Enabled="false"  />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="10" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
