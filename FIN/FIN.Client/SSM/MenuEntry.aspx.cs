﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;
using FIN.DAL.SSM;

namespace FIN.Client.SSM
{
    public partial class MenuEntry : PageBase
    {
        SSM_MENU_ITEMS sSM_MENU_ITEMS = new SSM_MENU_ITEMS();
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SSM_MENU_ITEMS> userCtx = new DataRepository<SSM_MENU_ITEMS>())
                    {
                        sSM_MENU_ITEMS = userCtx.Find(r =>
                            (r.PK_ID == int.Parse(Master.StrRecordId))
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_MENU_ITEMS;
                    
                    txtMenuCode.Text = sSM_MENU_ITEMS.MENU_CODE;
                    txtFormCode.Text = sSM_MENU_ITEMS.FORM_CODE;
                    
                    txtFormLabel.Text = sSM_MENU_ITEMS.FORM_LABEL;
                    txtFormLabelol.Text = sSM_MENU_ITEMS.FORM_LABEL_OL;
                    ddlLanguageCode.SelectedValue = sSM_MENU_ITEMS.LANGUAGE_CODE;

                    txtMenuURL.Text = sSM_MENU_ITEMS.MENU_URL;
                    txtReportURL.Text = sSM_MENU_ITEMS.REPORT_NAME;
                    txtReportCode.Text = sSM_MENU_ITEMS.REPORT_CODE;
                    txtPKColumnName.Text = sSM_MENU_ITEMS.PK_COLUMN_NAME;
                    txtOrderNo.Text = sSM_MENU_ITEMS.ORDER_NO.ToString();

                    if (sSM_MENU_ITEMS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_MENU_ITEMS = (SSM_MENU_ITEMS)EntityData;
                }

                sSM_MENU_ITEMS.MENU_CODE = txtMenuCode.Text;
                sSM_MENU_ITEMS.FORM_CODE = txtFormCode.Text;
                sSM_MENU_ITEMS.FORM_LABEL = txtFormLabel.Text;
                sSM_MENU_ITEMS.FORM_LABEL_OL = txtFormLabelol.Text;
                sSM_MENU_ITEMS.LANGUAGE_CODE = ddlLanguageCode.SelectedValue.ToString();
                sSM_MENU_ITEMS.REPORT_NAME = txtReportURL.Text;
                sSM_MENU_ITEMS.PK_COLUMN_NAME = txtPKColumnName.Text;                
                sSM_MENU_ITEMS.REPORT_CODE = txtReportCode.Text;
                sSM_MENU_ITEMS.ORDER_NO = decimal.Parse(txtOrderNo.Text);
                sSM_MENU_ITEMS.MENU_URL = txtMenuURL.Text;
                sSM_MENU_ITEMS.ENTRY_PAGE_OPEN = txtMenuURL.Text;
                sSM_MENU_ITEMS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                //sSM_MENU_ITEMS.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sSM_MENU_ITEMS.MODIFIED_BY = this.LoggedUserName;
                    sSM_MENU_ITEMS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sSM_MENU_ITEMS.PK_ID = DBMethod.GetPrimaryKeyValue("seq_menu_details");

                    sSM_MENU_ITEMS.CREATED_BY = this.LoggedUserName;
                    sSM_MENU_ITEMS.CREATED_DATE = DateTime.Today;

                }
                sSM_MENU_ITEMS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_MENU_ITEMS.MENU_CODE);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            Language_BLL.GetLanguageDetails(ref ddlLanguageCode);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                // Duplicate Check

                //ProReturn = null;

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_UOM_MST(txtUOMCode.Text, iNV_UOM_MASTER.BASE_UNIT, iNV_UOM_MASTER.ORG_ID, iNV_UOM_MASTER.UOM_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SSM_MENU_ITEMS>(sSM_MENU_ITEMS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SSM_MENU_ITEMS>(sSM_MENU_ITEMS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_MENU_ITEMS>(sSM_MENU_ITEMS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        
    }
}