﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LookupsEntry.aspx.cs" Inherits="FIN.Client.SSM.LookupsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="LNOrient" style="color: White; clear: both;">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DELETED,PK_ID,PARENT_CODE" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Master Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMasterCode" runat="server" Text='<%# Eval("MASTER_CODE") %>'
                                TabIndex="1" MaxLength="30" CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMasterCode" runat="server" MaxLength="30" TabIndex="1" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMasterCode" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("MASTER_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCode" runat="server" Text='<%# Eval("CODE") %>' MaxLength="30"
                                TabIndex="2" CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtCode" runat="server" MaxLength="30" TabIndex="2" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCode" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription"  Width="200px" runat="server" Text='<%# Eval("DESCRIPTION") %>'
                                MaxLength="100" TabIndex="3" CssClass="EntryFont RequiredField txtBox_en"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" Width="200px" runat="server" CssClass="EntryFont RequiredField txtBox_en"
                                MaxLength="100" TabIndex="3"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Width="200px" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Text='<%# Eval("DESCRIPTION") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription_ol" runat="server"  Width="200px" Text='<%# Eval("DESCRIPTION_OL") %>'
                                MaxLength="100" TabIndex="4" CssClass="EntryFont RequiredField txtBox_ol"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription_ol" runat="server"  Width="200px" CssClass="EntryFont RequiredField txtBox_ol"
                                MaxLength="100" TabIndex="4"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription_ol" runat="server" Style="word-wrap: break-word; white-space: pre-wrap;"
                                Text='<%# Eval("DESCRIPTION_OL") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parent Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlParentMasterCode" Width="130px" runat="server" TabIndex="5" CssClass="ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlParentMasterCode" Width="130px" runat="server" TabIndex="5" CssClass="ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblParentMasterCode" Width="130px" CssClass="adminFormFieldHeading" runat="server"
                                Text='<%# Eval("PARENT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parent Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtParentType" runat="server" Text='<%# Eval("PARENT_TYPE") %>'
                                MaxLength="30" TabIndex="6" CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtParentType" runat="server" MaxLength="30" TabIndex="6" CssClass="EntryFont RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblParentCode" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;
                                white-space: pre-wrap;" runat="server" Text='<%# Eval("PARENT_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOrderNo" runat="server"  Text='<%# Eval("ORDER_NO") %>'
                                TabIndex="15" Width="50px" CssClass="EntryFont RequiredField txtBox_N" MaxLength="50"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtOrderNo" runat="server" MaxLength="50"  TabIndex="15"
                                CssClass="EntryFont RequiredField txtBox_N" Width="50px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblOrderNo" CssClass="adminFormFieldHeading" Style="word-wrap: break-word;white-space: pre-wrap;" runat="server" Text='<%# Eval("ORDER_NO") %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective Start Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" Width="100px" TabIndex="7" MaxLength="10" runat="server"
                                CssClass="RequiredField EntryFont  txtBox" Text='<%#  Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>'
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" Width="100px" TabIndex="7" MaxLength="10" runat="server"
                                CssClass="RequiredField EntryFont  txtBox" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" Width="100px" runat="server" Text='<%# Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" Width="100px" TabIndex="8" MaxLength="10" runat="server"
                                CssClass="EntryFont  txtBox" Text='<%#  Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" Width="100px" TabIndex="8" MaxLength="10" runat="server"
                                CssClass="EntryFont  txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" Width="100px" runat="server" Text='<%# Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left"/>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="9" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="9" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="11" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="12" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="13" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="14" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="10" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
