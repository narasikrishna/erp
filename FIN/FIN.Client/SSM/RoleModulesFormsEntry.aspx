﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RoleModulesFormsEntry.aspx.cs" Inherits="FIN.Client.SSM.RoleModulesForms" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRoleCode">
                Role Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlRoleCode" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" OnSelectedIndexChanged="ddlRoleCode_SelectedIndexChanged" Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRoleDescription">
                Role Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtRoleDescription" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblModuleCode">
                Module Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlModuleCode" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4" OnSelectedIndexChanged="ddlModuleCode_SelectedIndexChanged" Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblModuleDescription">
                Module Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtModuleDescription" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divAct" visible="false">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive2">
                Access
            </div>
            <div class="divChkbox" style=" width: 250px">
                <asp:CheckBox ID="chkAccess" runat="server" Checked="true" TabIndex="6" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive1">
                Active
            </div>
            <div class="divChkbox" style=" width: 200px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="3" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="divSelectAll">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSelectAll">
                Select All
            </div>
            <div class="divChkbox" style=" width: 250px">
                <asp:CheckBox ID="chkSelectAll" runat="server" Checked="false" TabIndex="7" AutoPostBack="true" OnCheckedChanged="chkSelectAll_Changed" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="center">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PK_ID,DELETED,ROLE_CODE,MODULE_CODE" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Form Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFormCode" runat="server" CssClass="ddlStype" TabIndex="7">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <%-- <FooterTemplate>
                            <asp:DropDownList ID="ddlFormCode" runat="server" CssClass="ddlStype" TabIndex="7">
                            </asp:DropDownList>
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:Label ID="lblFormCode" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("FORM_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Form Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFormDescription" runat="server" Text='<%# Eval("FORM_DESC") %>'
                                TabIndex="8" CssClass="EntryFont RequiredField txtBox" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <%-- <FooterTemplate>
                            <asp:TextBox ID="txtFormDescription" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="8" Width="100px"></asp:TextBox>
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:Label ID="lblFormDescription" runat="server" Text='<%# Eval("FORM_DESC") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Insert Allowed">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkInsertAllowed" runat="server" Width="30px" TabIndex="9" Checked='<%# Convert.ToBoolean(Eval("INSERT_ALLOWED_FLAG")) %>' />
                        </EditItemTemplate>
                        <%--  <FooterTemplate>
                            <asp:CheckBox ID="chkInsertAllowed" runat="server" Checked="true" TabIndex="8" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkInsertAllowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("INSERT_ALLOWED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Update Allowed">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkUpdateAllowed" runat="server" Width="30px" TabIndex="10" Checked='<%# Convert.ToBoolean(Eval("UPDATE_ALLOWED_FLAG")) %>' />
                        </EditItemTemplate>
                        <%--  <FooterTemplate>
                            <asp:CheckBox ID="chkUpdateAllowed" runat="server" Checked="true" TabIndex="10" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkUpdateAllowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("UPDATE_ALLOWED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete Allowed">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkDeleteAllowed" runat="server" Width="30px" TabIndex="11" Checked='<%# Convert.ToBoolean(Eval("DELETE_ALLOWED_FLAG")) %>' />
                        </EditItemTemplate>
                        <%--    <FooterTemplate>
                            <asp:CheckBox ID="chkDeleteAllowed" runat="server" Checked="true" TabIndex="11" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDeleteAllowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("DELETE_ALLOWED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Query Allowed">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkQueryAllowed" runat="server" Width="30px" TabIndex="12" Checked='<%# Convert.ToBoolean(Eval("QUERY_ALLOWED_FLAG")) %>' />
                        </EditItemTemplate>
                        <%--   <FooterTemplate>
                            <asp:CheckBox ID="chkQueryAllowed" runat="server" Checked="true" TabIndex="12" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkQueryAllowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("QUERY_ALLOWED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Access Allowed">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkAccessAllowed" runat="server" Width="30px" TabIndex="13" Checked='<%# Convert.ToBoolean(Eval("CHECKFORM_ACCESS_FLAG_CROSSED_YN")) %>' />
                        </EditItemTemplate>
                        <%-- <FooterTemplate>
                            <asp:CheckBox ID="chkAccessAllowed" runat="server" Checked="true" TabIndex="13" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkAccessAllowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("FORM_ACCESS_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkEnabled" runat="server" Width="30px" TabIndex="14" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <%-- <FooterTemplate>
                            <asp:CheckBox ID="chkEnabled" runat="server" Checked="true" TabIndex="14" />
                        </FooterTemplate>--%>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkEnabled" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="15" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="16" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="17" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="18" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="19" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
            &nbsp;
        </div>
        <div align="left">
            <asp:GridView ID="gv_DB_Per" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="300px" DataKeyNames="PK_ID,ROLL_CODE,MODEL_CODE,DB_FRAME_ID" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Frame Name">
                        <ItemTemplate>
                            <asp:Label ID="lblFrameName" runat="server" Text='<%# Eval("DB_FRAME_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View Allowed">
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_View_Allowed" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("VIEW_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
