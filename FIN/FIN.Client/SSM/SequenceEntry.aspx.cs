﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using FIN.BLL.SSM;
using FIN.DAL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class SequenceEntry : PageBase
    {
        SSM_SEQUENCES sSM_SEQUENCES = new SSM_SEQUENCES();
        DataTable dtGridData = new DataTable();

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>



        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.SSM.Sequence_BLL.getSequenceDetails(int.Parse(Master.RecordID.ToString()));
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_PER_OBJECTIVE_SET> userCtx = new DataRepository<HR_PER_OBJECTIVE_SET>())
                //    {
                //        hR_PER_OBJECTIVE_SET = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_PER_OBJECTIVE_SET;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    sSM_SEQUENCES = new SSM_SEQUENCES();

                    if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                    //if (dtGridData.Rows[iLoop]["SEQUENCE_CODE"].ToString() != "0")
                    {
                        using (IRepository<SSM_SEQUENCES> userCtx = new DataRepository<SSM_SEQUENCES>())
                        {
                            sSM_SEQUENCES = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    sSM_SEQUENCES.SCREEN_CODE = dtGridData.Rows[iLoop]["SCREEN_CODE"].ToString();
                    sSM_SEQUENCES.SEQUENCE_CODE = dtGridData.Rows[iLoop]["SEQUENCE_CODE"].ToString();
                    sSM_SEQUENCES.DESCRIPTION = dtGridData.Rows[iLoop]["DESCRIPTION"].ToString();
                    sSM_SEQUENCES.WIDTH = short.Parse(dtGridData.Rows[iLoop]["WIDTH"].ToString());
                    sSM_SEQUENCES.SEQUENCE_FORMAT = dtGridData.Rows[iLoop]["SEQUENCE_FORMAT"].ToString();
                    sSM_SEQUENCES.START_WITH = short.Parse(dtGridData.Rows[iLoop]["START_WITH"].ToString());
                    sSM_SEQUENCES.CURRENT_DOCUMENT_NUMBER = short.Parse(dtGridData.Rows[iLoop]["CURRENT_DOCUMENT_NUMBER"].ToString());
                    sSM_SEQUENCES.INCREMENT_BY = short.Parse(dtGridData.Rows[iLoop]["INCREMENT_BY"].ToString());
                    sSM_SEQUENCES.PAD_ZERO = "1";


                    sSM_SEQUENCES.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    sSM_SEQUENCES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(sSM_SEQUENCES, "D"));
                    }
                    else
                    {
                        if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                        //if (dtGridData.Rows[iLoop]["SEQUENCE_CODE"].ToString() != "0")
                        {
                            sSM_SEQUENCES.PK_ID = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            sSM_SEQUENCES.MODIFIED_BY = this.LoggedUserName;
                            sSM_SEQUENCES.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_SEQUENCES>(sSM_SEQUENCES, true);
                            savedBool = true;
                        }
                        else
                        {

                            sSM_SEQUENCES.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_SEQUENCES_SEQ);
                            //sSM_COUNTRIES.COUNTRY_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_036.ToString(), false, true);
                            sSM_SEQUENCES.CREATED_BY = this.LoggedUserName;
                            sSM_SEQUENCES.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_SEQUENCES>(sSM_SEQUENCES);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_PER_OBJECTIVE_SET, "A"));
                        }
                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlScreenCode = tmpgvr.FindControl("ddlScreenCode") as DropDownList;
                FIN.BLL.SSM.Sequence_BLL.getScreenCode(ref ddlScreenCode);

                if (gvData.EditIndex >= 0)
                {
                    ddlScreenCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SCREEN_CODE].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Appraisal Objective");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
               

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtWidth = gvr.FindControl("txtWidth") as TextBox;
            TextBox txtPrefix = gvr.FindControl("txtPrefix") as TextBox;
            TextBox txtStartWith = gvr.FindControl("txtStartWith") as TextBox;
            TextBox txtStartNumber = gvr.FindControl("txtStartNumber") as TextBox;
            TextBox txtIncrementBy = gvr.FindControl("txtIncrementBy") as TextBox;
            DropDownList ddlScreenCode = gvr.FindControl("ddlScreenCode") as DropDownList;
            TextBox txtSequenceCode = gvr.FindControl("txtSequenceCode") as TextBox;
            CheckBox chkAct = gvr.FindControl("chkAct") as CheckBox;
            

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtDescription;
            slControls[1] = txtWidth;
            slControls[2] = txtPrefix;
            slControls[3] = txtStartWith;
            slControls[4] = txtStartNumber;
            slControls[5] = txtIncrementBy;
            slControls[6] = txtSequenceCode;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~TextBox~TextBox~TextBox";
            string strMessage = Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Width_P"] + " ~ " + Prop_File_Data["Prefix_P"] + " ~ " + Prop_File_Data["Start_With_P"] + " ~ " + Prop_File_Data["Current_Document_Number_P"] + " ~ " + Prop_File_Data["Increment_By_P"] + " ~ " + Prop_File_Data["Sequence_Code_P"] + "";
            //string strMessage = "Description ~ Width ~ Prefix ~ Start With ~ Start Number ~ Increment by ~ Sequence Code";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "SEQUENCE_FORMAT='" + txtPrefix.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            drList["SCREEN_CODE"] = ddlScreenCode.SelectedValue;
            drList["SCREEN_NAME"] = ddlScreenCode.SelectedItem.Text;
            drList["DESCRIPTION"] = txtDescription.Text;
            drList["SEQUENCE_CODE"] = txtSequenceCode.Text;
            drList["WIDTH"] = txtWidth.Text;
            drList["SEQUENCE_FORMAT"] = txtPrefix.Text;
            drList["START_WITH"] = txtStartWith.Text;
            drList["CURRENT_DOCUMENT_NUMBER"] = txtStartNumber.Text;
            drList["INCREMENT_BY"] = txtIncrementBy.Text;
            if (chkAct.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    sSM_SEQUENCES.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<SSM_SEQUENCES>(sSM_SEQUENCES);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }




    }
}