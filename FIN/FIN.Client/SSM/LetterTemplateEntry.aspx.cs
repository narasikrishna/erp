﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class LetterTemplateEntry : PageBase
    {
        SL_MST_LETTER_TEMPLATES sL_MST_LETTER_TEMPLATES = new SL_MST_LETTER_TEMPLATES();



        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SL_MST_LETTER_TEMPLATES> userCtx = new DataRepository<SL_MST_LETTER_TEMPLATES>())
                    {
                        sL_MST_LETTER_TEMPLATES = userCtx.Find(r =>
                            (r.LETTER_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sL_MST_LETTER_TEMPLATES;


                    txtLetterDesc.Text = sL_MST_LETTER_TEMPLATES.LETTER_DESC;
                    txtHeadLeft.Text = sL_MST_LETTER_TEMPLATES.HEADER_LEFT;
                    txtHeadright.Text = sL_MST_LETTER_TEMPLATES.HEADER_RIGHT;
                    txtHeadCenter.Text = sL_MST_LETTER_TEMPLATES.HEADER_CENTRE;
                    txtRefNum.Text = sL_MST_LETTER_TEMPLATES.OUR_REF_NUM;
                    txtDateline.Text = sL_MST_LETTER_TEMPLATES.DATE_LINE;
                    txtAddress.Text = sL_MST_LETTER_TEMPLATES.TO_ADDRESS;
                    txtSubline.Text = sL_MST_LETTER_TEMPLATES.SUB_LINE;
                    txtRefline.Text = sL_MST_LETTER_TEMPLATES.REF_LINE;
                    txtLettercnt1.Text = sL_MST_LETTER_TEMPLATES.LTR_CONTENTS1;
                    txtLettercnt2.Text = sL_MST_LETTER_TEMPLATES.LTR_CONTENTS2;
                    txtFooterleft.Text = sL_MST_LETTER_TEMPLATES.FOOTER_LEFT;
                    txtFooterright.Text = sL_MST_LETTER_TEMPLATES.FOOTER_RIGHT;
                    txtFootercenter.Text = sL_MST_LETTER_TEMPLATES.FOOTER_CENTRE;
                    txtSigntitle.Text = sL_MST_LETTER_TEMPLATES.SIGN_TITLE;
                    txtSignatory.Text = sL_MST_LETTER_TEMPLATES.SIGNATORY;
                    txtTitle.Text = sL_MST_LETTER_TEMPLATES.TITLE;



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sL_MST_LETTER_TEMPLATES = (SL_MST_LETTER_TEMPLATES)EntityData;
                }

                sL_MST_LETTER_TEMPLATES.LETTER_DESC = txtLetterDesc.Text;
                sL_MST_LETTER_TEMPLATES.HEADER_LEFT = txtHeadLeft.Text;
                sL_MST_LETTER_TEMPLATES.HEADER_RIGHT = txtHeadright.Text;
                sL_MST_LETTER_TEMPLATES.HEADER_CENTRE = txtHeadCenter.Text;
                sL_MST_LETTER_TEMPLATES.OUR_REF_NUM = txtRefNum.Text;
                sL_MST_LETTER_TEMPLATES.DATE_LINE = txtDateline.Text;
                sL_MST_LETTER_TEMPLATES.TO_ADDRESS = txtAddress.Text;
                sL_MST_LETTER_TEMPLATES.SUB_LINE = txtSubline.Text;
                sL_MST_LETTER_TEMPLATES.REF_LINE = txtRefline.Text;
                sL_MST_LETTER_TEMPLATES.LTR_CONTENTS1 = txtLettercnt1.Text;
                sL_MST_LETTER_TEMPLATES.LTR_CONTENTS2 = txtLettercnt2.Text;
                sL_MST_LETTER_TEMPLATES.FOOTER_LEFT = txtFooterleft.Text;
                sL_MST_LETTER_TEMPLATES.FOOTER_RIGHT = txtFooterright.Text;
                sL_MST_LETTER_TEMPLATES.FOOTER_CENTRE = txtFootercenter.Text;
                sL_MST_LETTER_TEMPLATES.SIGN_TITLE = txtSigntitle.Text;
                sL_MST_LETTER_TEMPLATES.SIGNATORY = txtSignatory.Text;
                sL_MST_LETTER_TEMPLATES.TITLE = txtTitle.Text;

                sL_MST_LETTER_TEMPLATES.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sL_MST_LETTER_TEMPLATES.MODIFIED_BY = this.LoggedUserName;
                    sL_MST_LETTER_TEMPLATES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sL_MST_LETTER_TEMPLATES.LETTER_ID = FINSP.GetSPFOR_SEQCode("SSM_025".ToString(), false, true);

                    sL_MST_LETTER_TEMPLATES.CREATED_BY = this.LoggedUserName;
                    sL_MST_LETTER_TEMPLATES.CREATED_DATE = DateTime.Today;

                }

                sL_MST_LETTER_TEMPLATES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sL_MST_LETTER_TEMPLATES.LETTER_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                // Duplicate Check

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ITEM_CATOG(txtCategoryName.Text, sL_MST_LETTER_TEMPLATES.ITEM_CATEGORY_ID, txtEffectiveStartDate.Text, txtEffectiveEnddate.Text);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {

                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SL_MST_LETTER_TEMPLATES>(sL_MST_LETTER_TEMPLATES);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SL_MST_LETTER_TEMPLATES>(sL_MST_LETTER_TEMPLATES, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SL_MST_LETTER_TEMPLATES>(sL_MST_LETTER_TEMPLATES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

       


    }
}