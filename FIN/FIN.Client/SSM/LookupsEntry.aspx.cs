﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class LookupsEntry : PageBase
    {
        SSM_CODE_MASTERS SSM_CODE_MASTERS = new SSM_CODE_MASTERS();
        DataTable dtGridData = new DataTable();

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Lookups_DAL.GetLookUpData(Master.RecordID)).Tables[0];
                if (dtGridData.Rows.Count > 0)
                {
                    dtGridData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Lookups_DAL.GetLookUpData4ParentCode(dtGridData.Rows[0]["PARENT_CODE"].ToString())).Tables[0];
                }
                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    SSM_CODE_MASTERS = new SSM_CODE_MASTERS();
                    if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString())>0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<SSM_CODE_MASTERS> userCtx = new DataRepository<SSM_CODE_MASTERS>())
                        {
                            SSM_CODE_MASTERS = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    SSM_CODE_MASTERS.MASTER_CODE = dtGridData.Rows[iLoop]["MASTER_CODE"].ToString();
                    SSM_CODE_MASTERS.CODE = dtGridData.Rows[iLoop]["CODE"].ToString();
                    SSM_CODE_MASTERS.DESCRIPTION = dtGridData.Rows[iLoop]["DESCRIPTION"].ToString();
                    SSM_CODE_MASTERS.DESCRIPTION_OL = dtGridData.Rows[iLoop]["DESCRIPTION_OL"].ToString();
                    SSM_CODE_MASTERS.PARENT_CODE = dtGridData.Rows[iLoop]["PARENT_CODE"].ToString();
                    SSM_CODE_MASTERS.PARENT_TYPE = dtGridData.Rows[iLoop]["PARENT_TYPE"].ToString();
                    SSM_CODE_MASTERS.ORDER_NO = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["ORDER_NO"].ToString());

                    if (dtGridData.Rows[iLoop][FINColumnConstants.effective_date] != DBNull.Value)
                    {
                        SSM_CODE_MASTERS.EFFECTIVE_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_DATE"].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.end_date] != DBNull.Value)
                    {
                        SSM_CODE_MASTERS.END_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["END_DATE"].ToString());
                    }

                    SSM_CODE_MASTERS.ENABLED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.N;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(SSM_CODE_MASTERS, "D"));
                    }
                    else
                    {
                        if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                        {
                            SSM_CODE_MASTERS.PK_ID = int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            SSM_CODE_MASTERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_CODE_MASTERS.PK_ID.ToString());
                            SSM_CODE_MASTERS.MODIFIED_BY = this.LoggedUserName;
                            SSM_CODE_MASTERS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_CODE_MASTERS>(SSM_CODE_MASTERS, true);
                            savedBool = true;
                        }
                        else
                        {
                            SSM_CODE_MASTERS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_CODE_MASTERS_SEQ);
                            SSM_CODE_MASTERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_CODE_MASTERS.PK_ID.ToString());
                            SSM_CODE_MASTERS.CREATED_BY = this.LoggedUserName;
                            SSM_CODE_MASTERS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_CODE_MASTERS>(SSM_CODE_MASTERS);
                            savedBool = true;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlParentMasterCode = tmpgvr.FindControl("ddlParentMasterCode") as DropDownList;
                Lookups_BLL.GetLookUp(ref ddlParentMasterCode);


                if (gvData.EditIndex >= 0)
                {
                    ddlParentMasterCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PARENT_CODE].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Lookup Data ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.FALSEFLAG;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtMasterCode = gvr.FindControl("txtMasterCode") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtDescription_ol = gvr.FindControl("txtDescription_ol") as TextBox;
            DropDownList ddlParentMasterCode = gvr.FindControl("ddlParentMasterCode") as DropDownList;
            TextBox txtCode = gvr.FindControl("txtCode") as TextBox;
            TextBox txtParentType = gvr.FindControl("txtParentType") as TextBox;
            TextBox txtOrderNo = gvr.FindControl("txtOrderNo") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["pk_id"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtMasterCode;
            slControls[1] = txtCode;
            slControls[2] = txtDescription;
            slControls[3] = txtDescription;
            //slControls[3] = ddlParentMasterCode;
            slControls[4] = dtpStartDate;
            slControls[5] = dtpStartDate;
            slControls[6] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Master_Code_P"] + " ~ " + Prop_File_Data["Code_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Order_No_P"] + " ~ " + Prop_File_Data["Effective_Start_Date_P"] + " ~ " + Prop_File_Data["Effective_Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Master Code ~ Code ~ Description ~ Effective Date~ Effective Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "CODE='" + txtCode.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            drList[FINColumnConstants.MASTER_CODE] = txtMasterCode.Text;
            drList[FINColumnConstants.CODE] = txtCode.Text;
            drList[FINColumnConstants.DESCRIPTION] = txtDescription.Text;
            drList[FINColumnConstants.DESCRIPTION_OL] = txtDescription_ol.Text;
            drList[FINColumnConstants.PARENT_CODE] = ddlParentMasterCode.SelectedValue.ToString();
            drList[FINColumnConstants.PARENT_TYPE] = txtParentType.Text;
            drList[FINColumnConstants.ORDER_NO] = txtOrderNo.Text;
            drList[FINColumnConstants.effective_date] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            if (dtpEndDate.Text.ToString().Trim().Length > 0)
            {
                drList[FINColumnConstants.end_date] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.end_date] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    SSM_CODE_MASTERS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<SSM_CODE_MASTERS>(SSM_CODE_MASTERS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }




    }
}