﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace FIN.Client.SSM
{
    public partial class MaisonetteIntegrationImport : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
        string connectionStringOracle = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringOracle"].ToString();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                lblImported.Text = "";
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayableATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        #endregion



        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablePOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion


        protected void btnPost_Click(object sender, EventArgs e)
        {
            string maxIdAP = "";
            string maxIdSC = "";
            string maxIdSCD = "";
            string maxIdSCA = "";
            string maxIdSCAD = "";

            try
            {
                //AccountPayable
                //Get the MAX Id
                using (OracleConnection connMax = new OracleConnection())
                {
                    connMax.ConnectionString = connectionStringOracle;
                    connMax.Open();
                    OracleCommand cmdMax = connMax.CreateCommand();
                    string sql = "";

                    sql = "Select max(ACCOUNTPAYABLEID) maxValue from accountpayable";
                    DataTable dtDataMax = new DataTable();
                    OracleDataAdapter oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[0]["maxValue"].ToString().Length > 0)
                        {
                            maxIdAP = dtDataMax.Rows[0]["maxValue"].ToString();
                        }
                    }

                    //ServiceContract - Get the MAX Id
                    sql = "select max(ServiceContractId) as maxIdSC from ServiceContract";
                    DataTable dtDataMaxSC = new DataTable();
                    OracleDataAdapter oaSC = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaSC.Fill(dtDataMaxSC);
                    if (dtDataMaxSC.Rows.Count > 0)
                    {
                        if (dtDataMaxSC.Rows[0]["maxIdSC"].ToString().Length > 0)
                        {
                            maxIdSC = dtDataMaxSC.Rows[0]["maxIdSC"].ToString();
                        }
                    }

                    //ServiceContractDetail - Get the MAX Id
                    sql = "select max(PaymentId) as maxIdSCD from ServiceContractPayment";
                    DataTable dtDataMaxSCD = new DataTable();
                    OracleDataAdapter oaSCD = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaSCD.Fill(dtDataMaxSCD);
                    if (dtDataMaxSCD.Rows.Count > 0)
                    {
                        if (dtDataMaxSCD.Rows[0]["maxIdSCD"].ToString().Length > 0)
                        {
                            maxIdSCD = dtDataMaxSCD.Rows[0]["maxIdSCD"].ToString();
                        }
                    }


                    //ServiceContractAmendment - Get the MAX Id
                    sql = "select max(ServiceContractAmendmentId) as maxIdSCA from ServiceContractAmendment";
                    DataTable dtDataMaxSCA = new DataTable();
                    OracleDataAdapter oaSCA = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaSCA.Fill(dtDataMaxSCA);
                    if (dtDataMaxSCA.Rows.Count > 0)
                    {
                        if (dtDataMaxSCA.Rows[0]["maxIdSCA"].ToString().Length > 0)
                        {
                            maxIdSCA = dtDataMaxSCA.Rows[0]["maxIdSCA"].ToString();
                        }
                    }

                    //ServiceContractAmendmentDetail - Get the MAX Id
                    sql = "select max(DetailId) as maxIdSCAD from ServiceContractAmendmentDetail";
                    DataTable dtDataMaxSCAD = new DataTable();
                    OracleDataAdapter oaSCAD = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaSCAD.Fill(dtDataMaxSCAD);
                    if (dtDataMaxSCAD.Rows.Count > 0)
                    {
                        if (dtDataMaxSCAD.Rows[0]["maxIdSCAD"].ToString().Length > 0)
                        {
                            maxIdSCAD = dtDataMaxSCAD.Rows[0]["maxIdSCAD"].ToString();
                        }
                    }

                    connMax.Close();
                }

                //Create Connection  -- SQL
                SqlConnection con = new SqlConnection(connectionStringSQL);

                string sqlQuery = " select ACCOUNTPAYABLEID,(TYPE*10) as TYPE,VENDORID,AMOUNT,CURRENCYID,convert(nvarchar,DueDate,103) as DueDate,PAIDAMOUNT,STATUS,convert(nvarchar,STATUSDATE,103) as [STATUSDATE]";
                sqlQuery += " ,REMARKS_EN,REMARKS_AR,COMPONENTID,PAYMENTMETHOD,BANKID,PAYMENTNO,CHECKID,convert(nvarchar,PAYMENTDATE,103) as PAYMENTDATE ,MERGEDAPID,SPLITTEDAPID,CUSERID,convert(nvarchar,CDATE,103) as CDATE ";
                sqlQuery += " ,MDATE,CASE DisablePartiallyPaid WHEN 'TRUE' THEN '1' ELSE '0' END as DisablePartiallyPaid,convert(nvarchar,CANCELDATE,103) as CANCELDATE,convert(nvarchar,INVOICEDATE,103) as INVOICEDATE ,ACCOUNTPAYABLENUMBER";
                sqlQuery += " from accountpayable where status=1";
                if (maxIdAP.ToString().Length > 0)
                {
                    sqlQuery += " and ACCOUNTPAYABLEID > '" + maxIdAP + "'";
                }
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                DataTable dtData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dtData);

                //ServiceContract - SQL
                string sqlQuerySC = " select ServiceContractID,ServiceContractNumber,VendorID,CurrencyID,convert(nvarchar,ServiceContractStart,103) as ServiceContractStart ,convert(nvarchar,ServiceContractEnd,103) as ServiceContractEnd,Amount,Coverage,Status,convert(nvarchar,StatusDate,103) as StatusDate,";
                sqlQuerySC += " TransactionReference, convert(nvarchar,CDate,103) as CDate, MDate, CUserID, DocumentID,";
                sqlQuerySC += " CASE IsComprehensive WHEN 'TRUE' THEN '1' ELSE '0' END as IsComprehensive,CASE IsOneJob WHEN 'TRUE' THEN '1' ELSE '0' END as IsOneJob,JobType, Notes_En,Notes_Ar,convert(nvarchar,BreakDate,103)  as BreakDate,MaintCategoryId,";
                sqlQuerySC += " convert(nvarchar,CancelDate,103) as CancelDate, convert(nvarchar,DeleteDate,103) as DeleteDate,Purpose, CASE IsRenewed WHEN 'TRUE' THEN '1' ELSE '0' END as IsRenewed,RelatedServiceContractId";
                sqlQuerySC += " from servicecontract";
                if (maxIdSC.ToString().Length > 0)
                {
                    sqlQuerySC += " where ServiceContractID > '" + maxIdSC + "'";
                }

                SqlCommand cmdSC = new SqlCommand(sqlQuerySC, con);
                DataTable dtDataSC = new DataTable();
                SqlDataAdapter daSC = new SqlDataAdapter(cmdSC);

                daSC.Fill(dtDataSC);


                //ServiceContractPayment - SQL
                string sqlQuerySCD = " Select PaymentId,ServiceContractComponentId,InvoiceNumber,convert(nvarchar,InvoiceDate,103) as InvoiceDate";
                sqlQuerySCD += " from ServiceContractPayment";
                if (maxIdSCD.ToString().Length > 0)
                {
                    sqlQuerySCD += " where PaymentId > '" + maxIdSCD + "'";
                }

                SqlCommand cmdSCD = new SqlCommand(sqlQuerySCD, con);
                DataTable dtDataSCD = new DataTable();
                SqlDataAdapter daSCD = new SqlDataAdapter(cmdSCD);

                daSCD.Fill(dtDataSCD);

                //ServiceContractAmendment - SQL
                string sqlQuerySCA = " select ServiceContractAmendmentId,ServiceContractId, AMOUNT, Reason_En, Reason_ar,";
                sqlQuerySCA += " convert(nvarchar,AmendmentDate,103) as AmendmentDate,  DocumentId, convert(nvarchar,cdate,103) as  cdate,";
                sqlQuerySCA += " mdate, CUserID";
                sqlQuerySCA += " from ServiceContractAmendment";
                if (maxIdSCA.ToString().Length > 0)
                {
                    sqlQuerySCA += " where ServiceContractAmendmentId > '" + maxIdSCA + "'";
                }

                SqlCommand cmdSCA = new SqlCommand(sqlQuerySCA, con);
                DataTable dtDataSCA = new DataTable();
                SqlDataAdapter daSCA = new SqlDataAdapter(cmdSCA);

                daSCA.Fill(dtDataSCA);


                //ServiceContractAmendmentDetail - SQL
                string sqlQuerySCAD = " select DetailId, ServiceContractAmendmentId,  ServiceContractComponentId,  amount ";
                sqlQuerySCAD += " from ServiceContractAmendmentDetail";
                if (maxIdSCAD.ToString().Length > 0)
                {
                    sqlQuerySCAD += " where DetailId > '" + maxIdSCAD + "'";
                }

                SqlCommand cmdSCAD = new SqlCommand(sqlQuerySCAD, con);
                DataTable dtDataSCAD = new DataTable();
                SqlDataAdapter daSCAD = new SqlDataAdapter(cmdSCAD);

                daSCAD.Fill(dtDataSCAD);


                //GenericServiceContract -- SQL
                string sqlQueryGSC = " Select  GenericServiceContractID,  GenericServiceContractNumber,  VendorID,  GenericServiceTypeID,  CurrencyID,  StartDate,  EndDate";
                sqlQueryGSC += " ,IssueDate,  Amount,  Status,  StatusDate,  TransactionReference,  CDate,  MDate,  CUserID,  DocumentID,  Notes_En,  Notes_Ar";
                sqlQueryGSC += " ,BreakDate,  CancelDate,  DeleteDate,  Purpose,  IsRenewed, RelatedGenServContractId";
                sqlQueryGSC += " from GenericServiceContract";
                sqlQueryGSC += " where Status=1";
                sqlQueryGSC += " and GenericServiceContractID > (select max(GenericServiceContractID) from GenericServiceContract)";
                
                SqlCommand cmdGSC = new SqlCommand(sqlQueryGSC, con);

                con.Open();
                DataTable dtDataGSC = new DataTable();
                SqlDataAdapter daGSC = new SqlDataAdapter(cmdGSC);

                daGSC.Fill(dtDataGSC);


                //GenericServiceContractPayment -- SQL
                string sqlQueryGSCP = " select PaymentId,  GenServContractComponentId ,   InvoiceNumber,  InvoiceDate from GenericServiceContractPayment";
                sqlQueryGSCP += " where Status=1";
                sqlQueryGSCP += " and PaymentId > (select max(PaymentId) from GenericServiceContractPayment)";

                SqlCommand cmdGSCP = new SqlCommand(sqlQueryGSCP, con);

                con.Open();
                DataTable dtDataGSCP = new DataTable();
                SqlDataAdapter daGSCP = new SqlDataAdapter(cmdGSCP);

                daGSCP.Fill(dtDataGSCP);



                //Create Connection -- Oracle
                //Account Payables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into accountpayable (accountpayableid,type,vendorid, amount, currencyid, duedate, paidamount, status, statusdate, remarks_en, remarks_ar, componentid, paymentmethod, ";
                            sql += "  bankid, paymentno, checkid, paymentdate, mergedapid, splittedapid, cuserid , cdate, mdate, disablepartiallypaid, canceldate, invoicedate, accountpayablenumber ) Values ('";
                            sql += dtData.Rows[iLoop]["ACCOUNTPAYABLEID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["TYPE"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["VENDORID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["AMOUNT"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CURRENCYID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["DUEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["PAIDAMOUNT"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["STATUS"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["STATUSDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["REMARKS_EN"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["REMARKS_AR"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["COMPONENTID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PAYMENTMETHOD"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["BANKID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PAYMENTNO"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CHECKID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["PAYMENTDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["MERGEDAPID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["SPLITTEDAPID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CUSERID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["CDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "NULL,'";
                            sql += dtData.Rows[iLoop]["DISABLEPARTIALLYPAID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["CANCELDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "to_date('" + dtData.Rows[iLoop]["INVOICEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["ACCOUNTPAYABLENUMBER"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //Service Contract
                if (dtDataSC.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataSC.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into ServiceContract Values('";
                            sql += dtDataSC.Rows[iLoop]["ServiceContractID"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["ServiceContractNumber"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["VendorID"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["CurrencyID"].ToString() + "',";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["ServiceContractStart"].ToString() + "','dd/MM/yyyy'),";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["ServiceContractEnd"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtDataSC.Rows[iLoop]["Amount"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["Coverage"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["Status"].ToString() + "',";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["StatusDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtDataSC.Rows[iLoop]["TransactionReference"].ToString() + "',";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["CDate"].ToString() + "','dd/MM/yyyy'),";
                            sql += "NULL,'";
                            sql += dtDataSC.Rows[iLoop]["CUserID"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["DocumentID"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["IsComprehensive"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["IsOneJob"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["JobType"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["Notes_En"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["Notes_Ar"].ToString() + "',";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["BreakDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtDataSC.Rows[iLoop]["MaintCategoryId"].ToString() + "',";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["CancelDate"].ToString() + "','dd/MM/yyyy'),";
                            sql += "to_date('" + dtDataSC.Rows[iLoop]["DeleteDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtDataSC.Rows[iLoop]["Purpose"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["IsRenewed"].ToString() + "','";
                            sql += dtDataSC.Rows[iLoop]["RelatedServiceContractId"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        //  lblImported.Text = "Data Imported";
                    }
                }


                //Service Contract Payment
                if (dtDataSCD.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataSCD.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into ServiceContractPayment Values('";
                            sql += dtDataSCD.Rows[iLoop]["PaymentId"].ToString() + "','";
                            sql += dtDataSCD.Rows[iLoop]["ServiceContractComponentId"].ToString() + "','";
                            sql += dtDataSCD.Rows[iLoop]["InvoiceNumber"].ToString() + "',";
                            sql += "to_date('" + dtDataSCD.Rows[iLoop]["InvoiceDate"].ToString() + "','dd/MM/yyyy')";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        //  lblImported.Text = "Data Imported";
                    }
                }

                //Service Contract Amendment
                if (dtDataSCA.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataSCA.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into ServiceContractAmendment Values('";
                            sql += dtDataSCA.Rows[iLoop]["ServiceContractAmendmentId"].ToString() + "','";
                            sql += dtDataSCA.Rows[iLoop]["ServiceContractId"].ToString() + "','";
                            sql += dtDataSCA.Rows[iLoop]["AMOUNT"].ToString() + "','";
                            sql += dtDataSCA.Rows[iLoop]["Reason_En"].ToString() + "','";
                            sql += dtDataSCA.Rows[iLoop]["Reason_Ar"].ToString() + "',";
                            sql += "to_date('" + dtDataSCA.Rows[iLoop]["AmendmentDate"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtDataSCA.Rows[iLoop]["DocumentId"].ToString() + "',";
                            sql += "to_date('" + dtDataSCA.Rows[iLoop]["cdate"].ToString() + "','dd/MM/yyyy'),";
                            sql += "NULL,'";
                            sql += dtDataSCA.Rows[iLoop]["CUserID"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //Service Contract Amendment Detail
                if (dtDataSCAD.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataSCAD.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into ServiceContractAmendmentDetail Values('";
                            sql += dtDataSCAD.Rows[iLoop]["DetailId"].ToString() + "','";
                            sql += dtDataSCAD.Rows[iLoop]["ServiceContractAmendmentId"].ToString() + "','";
                            sql += dtDataSCAD.Rows[iLoop]["ServiceContractComponentId"].ToString() + "','";
                            sql += dtDataSCAD.Rows[iLoop]["amount"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATAIMPORT);

                //Close Connection  
                con.Close();
                da.Dispose();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayables", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }






    }
}