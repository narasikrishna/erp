﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;
using FIN.DAL.SSM;


namespace FIN.Client.SSM
{
    public partial class RoleModulesForms : PageBase
    {
        DataTable dtGridData = new DataTable();
        SSM_ROLE_FORM_PERMISSIONS SSM_ROLE_FORM_PERMISSIONS = new SSM_ROLE_FORM_PERMISSIONS();

        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.RecordID = int.Parse((Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()])));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                EntityData = null;

                FillCombo();

                dtGridData = DBMethod.ExecuteQuery(RoleModulesForms_DAL.GetRoleModuleFormsData(Master.RecordID)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    dtGridData = DBMethod.ExecuteQuery(RoleModulesForms_DAL.GetRoleModuleFormsData(Master.RecordID)).Tables[0];

                    //SSM_ROLE_FORM_PERMISSIONS = RoleModuleForm_BLL.getClassEntity(dtGridData.Rows[0]["role_code"].ToString(), dtGridData.Rows[0]["module_code"].ToString());
                    //EntityData = SSM_ROLE_FORM_PERMISSIONS;
                    //ddlRoleCode.SelectedValue = SSM_ROLE_FORM_PERMISSIONS.ROLE_CODE;
                    //ddlModuleCode.SelectedValue = SSM_ROLE_FORM_PERMISSIONS.MODULE_CODE;
                    //chkAccess.Checked = SSM_ROLE_FORM_PERMISSIONS.ACCESS_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    //chkActive.Checked = SSM_ROLE_FORM_PERMISSIONS.ACTIVE_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    if (dtGridData.Rows.Count > 0)
                    {
                        ddlRoleCode.SelectedValue = dtGridData.Rows[0]["role_code"].ToString();
                        FillModuleName();
                        ddlModuleCode.SelectedValue = dtGridData.Rows[0]["module_code"].ToString();

                        BindModulename();
                        BindRolename();

                        //chkAccess.Checked = dtGridData.Rows[0]["ACCESS_FLAG"].ToString() == FINAppConstants.EnabledFlag ? true : false;
                        //chkActive.Checked = dtGridData.Rows[0]["ACTIVE_FLAG"].ToString() == FINAppConstants.EnabledFlag ? true : false;

                        DataTable dt_db_data = DBMethod.ExecuteQuery(RoleModulesForms_DAL.getRoleDashBoardData(ddlRoleCode.SelectedValue, ddlModuleCode.SelectedValue)).Tables[0];
                        BindDBGrid(dt_db_data);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillCombo()
        {
            try
            {
                ErrorCollection.Clear();

                Role_BLL.GetRole(ref ddlRoleCode);
                // Screens_BLL.GetModuleList(ref ddlModuleCode);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindDBGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["ROLE_DB_PER"] = dtData;

                gv_DB_Per.DataSource = dtData;
                gv_DB_Per.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.FALSEFLAG;
                    dr[FINColumnConstants.INSERT_ALLOWED_FLAG] = FINAppConstants.FALSEFLAG;
                    dr[FINColumnConstants.UPDATE_ALLOWED_FLAG] = FINAppConstants.FALSEFLAG;
                    dr[FINColumnConstants.DELETE_ALLOWED_FLAG] = FINAppConstants.FALSEFLAG;
                    dr[FINColumnConstants.FORM_ACCESS_FLAG] = FINAppConstants.FALSEFLAG;
                    dr[FINColumnConstants.QUERY_ALLOWED_FLAG] = FINAppConstants.FALSEFLAG;

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Form Lists ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    SSM_ROLE_FORM_PERMISSIONS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<SSM_ROLE_FORM_PERMISSIONS>(SSM_ROLE_FORM_PERMISSIONS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }




                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        SSM_ROLE_FORM_PERMISSIONS = new SSM_ROLE_FORM_PERMISSIONS();
                        if (int.Parse(gvData.DataKeys[iLoop].Values["PK_ID"].ToString()) > 0 && gvData.DataKeys[iLoop].Values["PK_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<SSM_ROLE_FORM_PERMISSIONS> userCtx = new DataRepository<SSM_ROLE_FORM_PERMISSIONS>())
                            {
                                SSM_ROLE_FORM_PERMISSIONS = userCtx.Find(r =>
                                    (r.PK_ID == int.Parse(gvData.DataKeys[iLoop].Values["PK_ID"].ToString()) && r.ROLE_CODE == gvData.DataKeys[iLoop].Values["ROLE_CODE"].ToString() && r.MODULE_CODE == gvData.DataKeys[iLoop].Values["MODULE_CODE"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        Label lblFormCode = (Label)gvData.Rows[iLoop].FindControl("lblFormCode");
                        CheckBox chkInsertAllowed = (CheckBox)gvData.Rows[iLoop].FindControl("chkInsertAllowed");
                        CheckBox chkUpdateAllowed = (CheckBox)gvData.Rows[iLoop].FindControl("chkUpdateAllowed");
                        CheckBox chkDeleteAllowed = (CheckBox)gvData.Rows[iLoop].FindControl("chkDeleteAllowed");
                        CheckBox chkQueryAllowed = (CheckBox)gvData.Rows[iLoop].FindControl("chkQueryAllowed");
                        CheckBox chkAccessAllowed = (CheckBox)gvData.Rows[iLoop].FindControl("chkAccessAllowed");

                        SSM_ROLE_FORM_PERMISSIONS.FORM_CODE = lblFormCode.Text;

                        SSM_ROLE_FORM_PERMISSIONS.ROLE_CODE = ddlRoleCode.SelectedValue.ToString();
                        SSM_ROLE_FORM_PERMISSIONS.MODULE_CODE = ddlModuleCode.SelectedValue.ToString();
                        SSM_ROLE_FORM_PERMISSIONS.ACCESS_FLAG = chkAccess.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                        SSM_ROLE_FORM_PERMISSIONS.ACTIVE_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;




                        SSM_ROLE_FORM_PERMISSIONS.INSERT_ALLOWED_FLAG = chkInsertAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        SSM_ROLE_FORM_PERMISSIONS.UPDATE_ALLOWED_FLAG = chkUpdateAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        SSM_ROLE_FORM_PERMISSIONS.DELETE_ALLOWED_FLAG = chkDeleteAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        SSM_ROLE_FORM_PERMISSIONS.FORM_ACCESS_FLAG = chkAccessAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        SSM_ROLE_FORM_PERMISSIONS.QUERY_ALLOWED_FLAG = chkQueryAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;
                        SSM_ROLE_FORM_PERMISSIONS.ENABLED_FLAG = chkAccessAllowed.Checked == true ? FINAppConstants.Y : FINAppConstants.N;

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(SSM_ROLE_FORM_PERMISSIONS, "D"));
                        }
                        else
                        {
                            if (int.Parse(gvData.DataKeys[iLoop].Values["PK_ID"].ToString()) > 0 && gvData.DataKeys[iLoop].Values["PK_ID"].ToString() != string.Empty && gvData.DataKeys[iLoop].Values["ROLE_CODE"].ToString() == ddlRoleCode.SelectedValue.ToString())
                            {
                                SSM_ROLE_FORM_PERMISSIONS.PK_ID = CommonUtils.ConvertStringToInt(gvData.DataKeys[iLoop].Values["PK_ID"].ToString());
                                SSM_ROLE_FORM_PERMISSIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_ROLE_FORM_PERMISSIONS.PK_ID.ToString());
                                SSM_ROLE_FORM_PERMISSIONS.MODIFIED_BY = this.LoggedUserName;
                                SSM_ROLE_FORM_PERMISSIONS.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<SSM_ROLE_FORM_PERMISSIONS>(SSM_ROLE_FORM_PERMISSIONS, true);
                                savedBool = true;
                            }
                            else
                            {
                                SSM_ROLE_FORM_PERMISSIONS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_ROLE_FORM_PERMISSIONS_SEQ);
                                SSM_ROLE_FORM_PERMISSIONS.CHILD_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_ROLE_FORM_PERMISSIONS_SEQ);
                                SSM_ROLE_FORM_PERMISSIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_ROLE_FORM_PERMISSIONS.PK_ID.ToString());
                                SSM_ROLE_FORM_PERMISSIONS.CREATED_BY = this.LoggedUserName;
                                SSM_ROLE_FORM_PERMISSIONS.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<SSM_ROLE_FORM_PERMISSIONS>(SSM_ROLE_FORM_PERMISSIONS);
                                savedBool = true;
                            }
                        }
                    }
                }



                if (gv_DB_Per.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gv_DB_Per.Rows.Count; iLoop++)
                    {
                        ROLL_DASHBOARD_PERMISSION rOLL_DASHBOARD_PERMISSION = new ROLL_DASHBOARD_PERMISSION();
                        if (int.Parse(gv_DB_Per.DataKeys[iLoop].Values["PK_ID"].ToString()) > 0 && gv_DB_Per.DataKeys[iLoop].Values["PK_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<ROLL_DASHBOARD_PERMISSION> userCtx = new DataRepository<ROLL_DASHBOARD_PERMISSION>())
                            {
                                rOLL_DASHBOARD_PERMISSION = userCtx.Find(r =>
                                    (r.PK_ID == int.Parse(gv_DB_Per.DataKeys[iLoop].Values["PK_ID"].ToString()) && r.ROLL_CODE == gv_DB_Per.DataKeys[iLoop].Values["ROLL_CODE"].ToString() && r.MODEL_CODE == gv_DB_Per.DataKeys[iLoop].Values["MODEL_CODE"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        CheckBox chk_ViewFlag = (CheckBox)gv_DB_Per.Rows[iLoop].FindControl("chk_View_Allowed");

                        rOLL_DASHBOARD_PERMISSION.DB_FRAME_ID = gv_DB_Per.DataKeys[iLoop].Values["DB_FRAME_ID"].ToString();
                        rOLL_DASHBOARD_PERMISSION.VIEW_FLAG = chk_ViewFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                        rOLL_DASHBOARD_PERMISSION.ROLL_CODE = ddlRoleCode.SelectedValue.ToString();
                        rOLL_DASHBOARD_PERMISSION.MODEL_CODE = ddlModuleCode.SelectedValue.ToString();
                        rOLL_DASHBOARD_PERMISSION.ENABLED_FLAG = FINAppConstants.Y;


                        if (int.Parse(gv_DB_Per.DataKeys[iLoop].Values["PK_ID"].ToString()) > 0 && gv_DB_Per.DataKeys[iLoop].Values["PK_ID"].ToString() != string.Empty && gv_DB_Per.DataKeys[iLoop].Values["ROLL_CODE"].ToString() == ddlRoleCode.SelectedValue.ToString())
                        {

                            rOLL_DASHBOARD_PERMISSION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, rOLL_DASHBOARD_PERMISSION.PK_ID.ToString());
                            rOLL_DASHBOARD_PERMISSION.MODIFIED_BY = this.LoggedUserName;
                            rOLL_DASHBOARD_PERMISSION.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<ROLL_DASHBOARD_PERMISSION>(rOLL_DASHBOARD_PERMISSION, true);
                            savedBool = true;

                        }
                        else
                        {
                            rOLL_DASHBOARD_PERMISSION.PK_ID = DBMethod.GetPrimaryKeyValue("ROLL_DASHBOARD_PERMISSION_SEQ");
                            rOLL_DASHBOARD_PERMISSION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, rOLL_DASHBOARD_PERMISSION.PK_ID.ToString());
                            rOLL_DASHBOARD_PERMISSION.CREATED_BY = this.LoggedUserName;
                            rOLL_DASHBOARD_PERMISSION.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<ROLL_DASHBOARD_PERMISSION>(rOLL_DASHBOARD_PERMISSION);
                            savedBool = true;
                        }

                    }
                }



                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    SSM_ROLE_FORM_PERMISSIONS = new SSM_ROLE_FORM_PERMISSIONS();
                //    if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                //    {
                //        using (IRepository<SSM_ROLE_FORM_PERMISSIONS> userCtx = new DataRepository<SSM_ROLE_FORM_PERMISSIONS>())
                //        {
                //            SSM_ROLE_FORM_PERMISSIONS = userCtx.Find(r =>
                //                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                //                ).SingleOrDefault();
                //        }
                //    }

                //    SSM_ROLE_FORM_PERMISSIONS.FORM_CODE = dtGridData.Rows[iLoop]["FORM_CODE"].ToString();

                //    SSM_ROLE_FORM_PERMISSIONS.INSERT_ALLOWED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.INSERT_ALLOWED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.FALSEFLAG;
                //    SSM_ROLE_FORM_PERMISSIONS.UPDATE_ALLOWED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.UPDATE_ALLOWED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.FALSEFLAG;
                //    SSM_ROLE_FORM_PERMISSIONS.DELETE_ALLOWED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.DELETE_ALLOWED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.FALSEFLAG;
                //    SSM_ROLE_FORM_PERMISSIONS.FORM_ACCESS_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.FORM_ACCESS_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.FALSEFLAG;
                //    SSM_ROLE_FORM_PERMISSIONS.ENABLED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.FALSEFLAG;

                //    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                //    {
                //        tmpChildEntity.Add(new Tuple<object, string>(SSM_ROLE_FORM_PERMISSIONS, "D"));
                //    }
                //    else
                //    {
                //        if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0 && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                //        {
                //            SSM_ROLE_FORM_PERMISSIONS.PK_ID = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                //            SSM_ROLE_FORM_PERMISSIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_ROLE_FORM_PERMISSIONS.PK_ID.ToString());
                //            SSM_ROLE_FORM_PERMISSIONS.MODIFIED_BY = this.LoggedUserName;
                //            SSM_ROLE_FORM_PERMISSIONS.MODIFIED_DATE = DateTime.Today;
                //            DBMethod.SaveEntity<SSM_ROLE_FORM_PERMISSIONS>(SSM_ROLE_FORM_PERMISSIONS, true);
                //            savedBool = true;
                //        }
                //        else
                //        {
                //            SSM_ROLE_FORM_PERMISSIONS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_ROLE_FORM_PERMISSIONS_SEQ);
                //            SSM_ROLE_FORM_PERMISSIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SSM_ROLE_FORM_PERMISSIONS.PK_ID.ToString());
                //            SSM_ROLE_FORM_PERMISSIONS.CREATED_BY = this.LoggedUserName;
                //            SSM_ROLE_FORM_PERMISSIONS.CREATED_DATE = DateTime.Today;
                //            DBMethod.SaveEntity<SSM_ROLE_FORM_PERMISSIONS>(SSM_ROLE_FORM_PERMISSIONS);
                //            savedBool = true;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                //DropDownList ddlCityCode = tmpgvr.FindControl("ddlCityCode") as DropDownList;

                //if (gvData.EditIndex >= 0)
                //{
                //    ddlCityCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LANGUAGE_CODE].ToString();                  
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillModuleName()
        {
            Role_BLL.GetModuleList4Role(ref ddlModuleCode, ddlRoleCode.SelectedValue.ToString());
        }
        protected void ddlRoleCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindRolename();
                FillModuleName();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindRolename()
        {
            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(Role_DAL.GetRole(ddlRoleCode.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtRoleDescription.Text = dtData.Rows[0]["role_name"].ToString();
                }
            }

        }
        private void BindModulename()
        {
            DataTable dtData = new DataTable();
            //GridViewRow tmpgvr = gvData.FooterRow;

            //DropDownList ddlModuleCode = tmpgvr.FindControl("ddlModuleCode") as DropDownList;
            //TextBox txtModuleDescription = tmpgvr.FindControl("txtModuleDescription") as TextBox;


            dtData = DBMethod.ExecuteQuery(Screens_DAL.GetModuleList(ddlModuleCode.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtModuleDescription.Text = dtData.Rows[0]["description"].ToString();
                }
            }

        }
        protected void ddlModuleCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindModulename();

                dtGridData = DBMethod.ExecuteQuery(RoleModulesForms_DAL.GetRoleModuleForms(ddlModuleCode.SelectedValue.ToString(), ddlRoleCode.SelectedValue)).Tables[0];
                BindGrid(dtGridData);

                DataTable dt_Rol_data = DBMethod.ExecuteQuery(RoleModulesForms_DAL.getRoleDashBoardData(ddlRoleCode.SelectedValue, ddlModuleCode.SelectedValue.ToString())).Tables[0];
                BindDBGrid(dt_Rol_data);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlFormCode = gvr.FindControl("ddlFormCode") as DropDownList;

            CheckBox chkInsertAllowed = gvr.FindControl("chkInsertAllowed") as CheckBox;
            CheckBox chkUpdateAllowed = gvr.FindControl("chkUpdateAllowed") as CheckBox;
            CheckBox chkDeleteAllowed = gvr.FindControl("chkDeleteAllowed") as CheckBox;
            CheckBox chkQueryAllowed = gvr.FindControl("chkQueryAllowed") as CheckBox;
            CheckBox chkAccessAllowed = gvr.FindControl("chkAccessAllowed") as CheckBox;
            CheckBox chkEnabled = gvr.FindControl("chkEnabled") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();

            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddlFormCode;



            ErrorCollection.Clear();

            string strCtrlTypes = "TextBox";
            string strMessage = "From Code";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = string.Empty;

            strCondition = "FORM_CODE='" + ddlFormCode.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;

            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            drList[FINColumnConstants.FORM_CODE] = ddlFormCode.SelectedValue;

            drList[FINColumnConstants.INSERT_ALLOWED_FLAG] = chkInsertAllowed.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.UPDATE_ALLOWED_FLAG] = chkUpdateAllowed.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.DELETE_ALLOWED_FLAG] = chkDeleteAllowed.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.QUERY_ALLOWED_FLAG] = chkQueryAllowed.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.FORM_ACCESS_FLAG] = chkAccessAllowed.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.ENABLED_FLAG] = chkEnabled.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        protected void chkSelectAll_Changed(object sender, EventArgs e)
        {
            if (chkSelectAll.Checked == true)
            {
                foreach (GridViewRow gvr in gvData.Rows)
                {
                    CheckBox chkInsertAllowed = gvr.FindControl("chkInsertAllowed") as CheckBox;
                    CheckBox chkUpdateAllowed = gvr.FindControl("chkUpdateAllowed") as CheckBox;
                    CheckBox chkDeleteAllowed = gvr.FindControl("chkDeleteAllowed") as CheckBox;
                    CheckBox chkQueryAllowed = gvr.FindControl("chkQueryAllowed") as CheckBox;
                    CheckBox chkAccessAllowed = gvr.FindControl("chkAccessAllowed") as CheckBox;
                    CheckBox chkEnabled = gvr.FindControl("chkEnabled") as CheckBox;

                    chkInsertAllowed.Checked = true;
                    chkUpdateAllowed.Checked = true;
                    chkDeleteAllowed.Checked = true;
                    chkQueryAllowed.Checked = true;
                    chkAccessAllowed.Checked = true;
                    chkEnabled.Checked = true;
                }
            }
        }


    }
}