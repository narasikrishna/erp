﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="UserEntry.aspx.cs" Inherits="FIN.Client.SSM.UserEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblUserCode">
                User Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtUsercode" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="25" runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblUserCodeol">
                User Code(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtUsercodeol" CssClass="txtBox_ol" MaxLength="25" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblFirstName">
                First Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFirstName" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="50" runat="server" TabIndex="3"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblFirstNameol">
                First Name(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFirstNameol" CssClass="txtBox_ol" MaxLength="50" runat="server"
                    TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblLastName">
                Last Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLastName" CssClass="txtBox" MaxLength="50" runat="server" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblMidName">
                Mid Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMidName" CssClass="validate[] txtBox" MaxLength="50" runat="server"
                    TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblSurName">
                SurName
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtSurName" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="7"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                Effective Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtEffdate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    runat="server" TabIndex="8"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEffdate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="9" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
            </div>
            <div>
                <asp:Button ID="btnsav" runat="server" CssClass="btn" Text="ADD" TabIndex="10" OnClick="btnsav_Click"
                    Visible="False" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center" id="divButtons" runat="server">
            <table width="100%" align="center">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnLoginDetails" runat="server" ImageUrl="~/Images/btnLogin.png"  CssClass="btn"
                            OnClick="btnLoginDetails_Click" Style="border: 0px" TabIndex="10" />
                        <%--<asp:Button ID="btnLoginDetails" runat="server" Text="Login Details" CssClass="btn"
                            OnClick="btnLoginDetails_Click" TabIndex="10" />--%>
                    </td>
                    <td>
                    <asp:ImageButton ID="btnAddress" runat="server" ImageUrl="~/Images/add-det.png"  CssClass="btn"
                            OnClick="btnAddress_Click" Style="border: 0px" TabIndex="11" />
                       <%-- <asp:Button ID="btnAddress" runat="server" Text="Address Details" CssClass="btn"
                            OnClick="btnAddress_Click" TabIndex="11" />--%>
                    </td>
                    <td>
                     <asp:ImageButton ID="btnUserOrg" runat="server" ImageUrl="~/Images/btnUserOrg.png"  CssClass="btn"
                            OnClick="btnUserOrg_Click" Style="border: 0px" TabIndex="12" />
                        <%--<asp:Button ID="btnUserOrg" runat="server" Text="User Organization" TabIndex="12"
                            OnClick="btnUserOrg_Click" />--%>
                    </td>
                    <td>
                    <asp:ImageButton ID="btnUserRole" runat="server" ImageUrl="~/Images/btnUserRole.png"  CssClass="btn"
                            OnClick="btnUserRole_Click" Style="border: 0px" TabIndex="13" />
                        <%--<asp:Button ID="btnUserRole" runat="server" Text="User Role" TabIndex="12" OnClick="btnUserRole_Click" />--%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divTabLoginDet" runat="server" visible="false" style="border: 1px solid black;
            width: 810px;">
            <div class="divFormcontainer" style="width: 800px">
                <div class="divRowContainer" align="right">
                    <asp:HiddenField ID="HiddenField3" runat="server" Value="0" />
                    <asp:Button ID="Button1" runat="server" Text="Close" Visible="False" />
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer" runat="server" id="divpwd">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                            Password
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtUCPassword" CssClass="txtBox" MaxLength="10" runat="server" TabIndex="8"
                                TextMode="Password" AutoPostBack="True" OnTextChanged="txtPassword_TextChanged"></asp:TextBox>
                            <asp:HiddenField ID="hfPassword" runat="server" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div4">
                            Gender
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 280px">
                            <asp:RadioButtonList ID="RBG" runat="server" RepeatDirection="Horizontal" TabIndex="9">
                                <asp:ListItem Selected="True" Value="M" >Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                            Title
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlTitle" runat="server" CssClass="ddlStype" TabIndex="10">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div6">
                            Date of Birth
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtDateofBirth" CssClass="validate[custom[ReqDateDDMMYYY],,dateRange[dg1]] txtBox"
                                runat="server" MaxLength="10" TabIndex="11"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtDateofBirth"
                                OnClientDateSelectionChanged="checkDate" Enabled="True" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Custom, Numbers" TargetControlID="txtDateofBirth" Enabled="True" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div7">
                            Nationality Code
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlNationality" runat="server" CssClass=" ddlStype" TabIndex="12">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div9">
                            Employee ID
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlEmpid" runat="server" CssClass="validate[]  ddlStype" TabIndex="13">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div10">
                            Change Password on Next Login
                        </div>
                        <div class="divChkbox LNOrient" style=" width: 150px">
                            <asp:CheckBox ID="chkChangePasswordonNextLogin" runat="server" Checked="True" TabIndex="14" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div11">
                            Change Password Interval
                        </div>
                        <div class="divChkbox LNOrient" style=" width: 250px">
                            <asp:CheckBox ID="chkChangePasswordInterval" runat="server" Checked="True" TabIndex="15" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div12">
                            Password Lock
                        </div>
                        <div class="divChkbox LNOrient" style=" width: 150px">
                            <asp:CheckBox ID="chkPasswordLock" runat="server" Checked="True" TabIndex="16" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divClear_10">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" align="center">
                    <asp:Button ID="Button2" runat="server" CssClass="btn" Text="ADD" TabIndex="29" OnClick="btnAddLogindtls_Click"
                        Visible="False" />
                    &nbsp;&nbsp;<asp:Button ID="Button3" runat="server" CssClass="btn" Text="Clear" TabIndex="30"
                        OnClick="btnAddLoginClear_Click" Visible="False" />
                </div>
            </div>
        </div>
        <div id="divTabAddDet" runat="server" visible="false" style="border: 1px solid black;
            width: 810px;">
            <div class="divFormcontainer" style="width: 800px">
                <div class="divRowContainer" align="right">
                    <asp:HiddenField ID="HiddenField4" runat="server" Value="0" />
                    <asp:Button runat="server" CssClass="DisplayFont button" ID="Button4" Text="Close"
                        Width="60px" Visible="False" />
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div17">
                            Address 1
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtAddress1" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="100" runat="server" TabIndex="17"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div18">
                            Address 2
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtAddress2" CssClass="validate[] txtBox" MaxLength="100" runat="server"
                                TabIndex="18"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div19">
                            Country
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                Width="150px" Style="width: 150px" TabIndex="19">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div21">
                            State
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlState" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                TabIndex="20">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div23">
                            City
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                TabIndex="21">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div25">
                            Phone
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtPhone" CssClass="validate[] txtBox" MaxLength="15" runat="server"
                                TabIndex="22"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers,Custom"
                                ValidChars="+" TargetControlID="txtPhone" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div26">
                            Mobile
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtMobile" CssClass="validate[required] RequiredField txtBox" MaxLength="15"
                                runat="server" TabIndex="23"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars="+" TargetControlID="txtMobile" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div27">
                            Fax
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtFax" CssClass="validate[] txtBox" MaxLength="15" runat="server"
                                TabIndex="24"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                ValidChars="+" TargetControlID="txtFax" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div28">
                            Email ID
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtEmailID" CssClass="validate[required,custom[email]] RequiredField txtBox"
                                MaxLength="70" runat="server" TabIndex="25"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox  LNOrient" style=" width: 200px" id="Div29">
                            Telex No
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtTelexNo" CssClass="validate[] txtBox" MaxLength="20" runat="server"
                                TabIndex="26"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers,Custom"
                                ValidChars="+" TargetControlID="txtTelexNo" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" align="center">
                    <asp:Button ID="Button5" runat="server" CssClass="btn" Text="ADD" TabIndex="29" OnClick="btnAddrsdtls_Click"
                        Visible="False" />
                    &nbsp;&nbsp;<asp:Button ID="Button6" runat="server" CssClass="btn" Text="Clear" TabIndex="30"
                        OnClick="btnAddrsdtlsClear_Click" Visible="False" />
                </div>
            </div>
        </div>
        <div id="divTabOrgDet" runat="server" visible="false" style="border: 1px solid black;
            width: 810px;">
            <div class="divFormcontainer" style="width: 800px">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    DataKeyNames="PK_ID,COMP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                    OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Organization">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlorg" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                    TabIndex="27">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlorg" TabIndex="27" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblOrg" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("COMP_INTERNAL_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="dtpStartDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                    TabIndex="28" Text='<%#  Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                    Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="dtpStartDate" TabIndex="28" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                    Onkeypress="return DateKeyCheck(event,this);" Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="dtpEndDate" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                    TabIndex="29" Text='<%#  Eval("END_DATE","{0:dd/MM/yyyy}") %>' Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                    TargetControlID="dtpEndDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="dtpEndDate" TabIndex="29" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                    Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                    TargetControlID="dtpEndDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                    TabIndex="30" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="chkact" TabIndex="30" runat="server" Checked="true" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                    TabIndex="30" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="31" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="32" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="33" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="34" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="35" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        </div>
        <div id="divTabUserRole" runat="server" visible="false" style="border: 1px solid black;
            width: 810px;">
            <div class="divFormcontainer" style="width: 800px">
                <asp:GridView ID="gvDataUR" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    DataKeyNames="PK_ID,ROLE_CODE,DELETED" OnRowCancelingEdit="gvData_URRowCancelingEdit"
                    OnRowCommand="gvData_URRowCommand" OnRowCreated="gvData_URRowCreated" OnRowDataBound="gvData_URRowDataBound"
                    OnRowDeleting="gvData_URRowDeleting" OnRowEditing="gvData_URRowEditing" OnRowUpdating="gvData_URRowUpdating"
                    ShowFooter="True">
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Role">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlRole" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                    TabIndex="36">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlRole" TabIndex="36" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRole" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ROLE_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="dtpStartDateUR" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                    TabIndex="37" Text='<%#  Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                    Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDateUR">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpStartDateUR" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="dtpStartDateUR" TabIndex="37" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                    Onkeypress="return DateKeyCheck(event,this);" Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDateUR">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpStartDateUR" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="dtpEndDateUR" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                    TabIndex="38" Text='<%#  Eval("END_DATE","{0:dd/MM/yyyy}") %>' Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                    TargetControlID="dtpEndDateUR">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEndDateUR" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="dtpEndDateUR" TabIndex="38" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                    Width="95%"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                    TargetControlID="dtpEndDateUR">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="dtpEndDateUR" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("END_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkactUR" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                    TabIndex="39" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="chkactUR" TabIndex="39" runat="server" Checked="true" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkactUR" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="40" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="41" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="42" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="43" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="44" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                </asp:GridView>
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
       

    </script>
</asp:Content>
