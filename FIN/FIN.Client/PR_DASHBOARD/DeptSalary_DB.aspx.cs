﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.PR_DASHBOARD
{
    public partial class DeptSalary_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["NET_SAL"] = null;
                AssignToControl();
                FillComboBox();
                getSal4FinYear();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void getSal4FinYear()
        {
            DataTable dtNetSal = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getSal4FinYear(ddlDept.SelectedValue, ddlFinYear.SelectedValue)).Tables[0];
            Session["NET_SAL"] = dtNetSal;
            LoadGraph();
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinYear);
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            if (ddlFinYear.Items.Count > 0)
            {
                ddlFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDept);
            if (ddlDept.Items.Count > 0)
            {
                ddlDept.SelectedIndex = ddlDept.Items.Count - 1;
            }
        }

        private void LoadGraph()
        {
            if (Session["NET_SAL"] != null)
            {
                chrt_SalCount.DataSource = (DataTable)Session["NET_SAL"];
                chrt_SalCount.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["NET_SAL"];
                gvGraphdata.DataBind();
            }
        }

        protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSal4FinYear();
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSal4FinYear();
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrt_SalCount.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");

                htFilterParameter.Add("FF_YEAR", ddlFinYear.SelectedItem.Text);


                htFilterParameter.Add("Department_id", ddlDept.SelectedValue.ToString());
                htFilterParameter.Add("Department", ddlDept.SelectedItem.ToString());

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                DataTable dt_data = (DataTable)Session["NET_SAL"];
                ReportData = new DataSet();
                DataTable dt = dt_data.Copy();
                dt.TableName = "vw_bi_dept_salary";
                dt.Columns[0].ColumnName = "pay_period_desc";
                ReportData.Tables.Add(dt.Copy());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}