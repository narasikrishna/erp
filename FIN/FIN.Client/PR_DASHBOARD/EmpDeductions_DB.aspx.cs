﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.PR_DASHBOARD
{
    public partial class EmpDeductions_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_DEDUCTIONS"] = null;
                AssignToControl();
                FillComboBox();
                getEmpDeductions();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinanceYr);
            if (ddlFinanceYr.Items.Count > 0)
            {
                ddlFinanceYr.SelectedValue = str_finyear;
            }
            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDeptName);
            FIN.BLL.PER.PayrollElements_BLL.getPayElementsDeductions(ref ddlPayElement);
        }

        private void LoadGraph()
        {
            if (Session["EMP_DEDUCTIONS"] != null)
            {
                chrt_Dept.DataSource = (DataTable)Session["EMP_DEDUCTIONS"];
                chrt_Dept.Series["S_Dept_Count"].XValueMember = "pay_period_desc";
                chrt_Dept.Series["S_Dept_Count"].YValueMembers = "USED_AMT";
                chrt_Dept.DataBind();
            }
        }

        private void getEmpDeductions()
        {
            DataTable dtEmpDed = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getEmpMobileExp(ddlDeptName.SelectedValue, ddlEmpl.SelectedValue, ddlFinanceYr.SelectedValue, ddlPayElement.SelectedValue)).Tables[0];
            Session["EMP_DEDUCTIONS"] = dtEmpDed;
            LoadGraph();
        }

        protected void ddlDeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDeptName.SelectedValue.ToString() != "")
            {
                FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmpl, ddlDeptName.SelectedValue.ToString());
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            getEmpDeductions();
        }

        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrt_Dept.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getEmpMobileExp(ddlDeptName.SelectedValue, ddlEmpl.SelectedValue, ddlFinanceYr.SelectedValue, ddlPayElement.SelectedValue));


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}