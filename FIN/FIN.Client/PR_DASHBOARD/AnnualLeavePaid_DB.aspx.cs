﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.PR_DASHBOARD
{
    public partial class AnnualLeavePaid_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_ANNUAL_LEAVE_PAID"] = null;
                AssignToControl();
                FillComboBox();
                getAnnualLeavePaid();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_ALP_Year);
            if (ddl_ALP_Year.Items.Count > 0)
            {
                ddl_ALP_Year.SelectedValue = str_finyear;
            }
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_ALP_Period, ddl_ALP_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_ALP_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        private void LoadGraph()
        {
            if (Session["EMP_ANNUAL_LEAVE_PAID"] != null)
            {
                chrtAnnualLeave.DataSource = (DataTable)Session["EMP_ANNUAL_LEAVE_PAID"];
                chrtAnnualLeave.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["EMP_ANNUAL_LEAVE_PAID"];
                gvGraphdata.DataBind();
            }
        }

        private void getAnnualLeavePaid()
        {
            DataTable dtEmpAnnualLeavePaid = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getAnnualLeavePaid(ddl_ALP_Year.SelectedValue, ddl_ALP_Period.SelectedValue)).Tables[0];
            Session["EMP_ANNUAL_LEAVE_PAID"] = dtEmpAnnualLeavePaid;

            gvGraphdata.DataSource = (DataTable)Session["EMP_ANNUAL_LEAVE_PAID"];
            gvGraphdata.DataBind();
        }

        protected void ddl_ALP_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_ALP_Period, ddl_ALP_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_ALP_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            getAnnualLeavePaid();
            LoadGraph();
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrtAnnualLeave.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                htFilterParameter.Add("FF_YEAR", ddl_ALP_Year.SelectedItem.Text);


                htFilterParameter.Add("PERIOD", ddl_ALP_Period.SelectedValue.ToString());
                htFilterParameter.Add("PERIOD_DESC", ddl_ALP_Period.SelectedItem.ToString());

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                DataTable dt_data = (DataTable)Session["EMP_ANNUAL_LEAVE_PAID"];
                ReportData = new DataSet();
                DataTable dt = dt_data.Copy();
                dt.TableName = "vw_bi_annual_leave_paid";
                dt.Columns[0].ColumnName = "dept_name";
                ReportData.Tables.Add(dt.Copy());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}