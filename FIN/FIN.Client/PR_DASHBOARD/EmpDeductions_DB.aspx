﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmpDeductions_DB.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.EmpDeductions_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowDeptEmp() {
            $("#divDeptEmpGrid").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpAge" style="width: 100%;">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Deductions
            </div>
            <div style="float: right; padding: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowDeptEmp()" />
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
            <div id="divDeptEmpGrid" style="width: 100%; background-color: ThreeDFace; position: fixed;
                z-index: 5000; top: 30px; right: 10px; display: none">
                <div class="divRowContainer" style="padding-left: 10px">
                    <div class="lblBox" style="float: left; width: 100px;" id="lblApplicantId">
                        Elements
                    </div>
                    <div style="width: 350px; float: right; padding-bottom: 10px; padding-top: 2px">
                        <asp:DropDownList ID="ddlPayElement" runat="server" CssClass="ddlStype">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" style="display: none">
                    <div class="lblBox" style="float: left; width: 100px;" id="Div2">
                        Employee
                    </div>
                    <div style="width: 350px; float: right; padding-bottom: 10px; padding-top: 2px; display: none">
                        <asp:DropDownList ID="ddlEmpl" runat="server" CssClass="ddlStype">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" style="padding-left: 10px">
                    <div class="lblBox" style="float: left; width: 100px;" id="Div3">
                        Department
                    </div>
                    <div style="width: 350px; float: right; padding-bottom: 10px; padding-top: 2px">
                        <asp:DropDownList ID="ddlDeptName" runat="server" CssClass="ddlStype" AutoPostBack="false"
                            OnSelectedIndexChanged="ddlDeptName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer" style="padding-left: 10px">
                    <div class="lblBox" style="float: left; width: 100px;" id="Div4">
                        Year
                    </div>
                    <div style="width: 350px; float: right; padding-bottom: 10px; padding-top: 2px">
                        <asp:DropDownList ID="ddlFinanceYr" runat="server" CssClass="ddlStype">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div style="width: 50px; float: right; padding-bottom: 10px; padding-top: 2px">
                        <asp:Button ID="btnView" runat="server" Text="View" OnClick="btnView_Click" CssClass="btn" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="div1" style="width: 100%">
            <asp:Chart ID="chrt_Dept" runat="server" Height="310px" Width="500px" EnableViewState="true">
                <Series>
                    <asp:Series Name="S_Dept_Count" ChartType="Spline" XValueMember="pay_period_desc"
                        YValueMembers="USED_AMT" IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                        <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="pay_period_desc" HeaderText="Payroll Period"></asp:BoundField>
                    <asp:BoundField DataField="USED_AMT" HeaderText="Used Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
