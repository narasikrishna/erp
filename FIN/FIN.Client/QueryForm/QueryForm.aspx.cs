﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL;
using System.Collections;
using VMVServices.Web;
namespace FIN.Client.QueryForm
{
    public partial class QueryForm : PageBase
    {
        string modifyURL = string.Empty;
        string deleteURL = string.Empty;
        string addURL = string.Empty;
        string searchURL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["QUERY_FORM"] = FINAppConstants.Y;
                LoadContent();
            }
        }
        private void Startup(int progID)
        {
            try
            {
                ErrorCollection.Clear();
                //Get Programs Information & set Properties
                Hashtable htProgram = Menu_BLL.GetMenuDetail(progID);



                Master.KeyName = htProgram[ProgramParameters.AccessibleName.ToString()].ToString();
                Master.ProgramID = int.Parse(htProgram[ProgramParameters.ProgramID.ToString()].ToString());
                Master.ListPageToOpen = htProgram[ProgramParameters.ListURL.ToString()].ToString();

                this.Title = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
                modifyURL = htProgram[ProgramParameters.ModifyURL.ToString()].ToString();
                deleteURL = htProgram[ProgramParameters.DeleteURL.ToString()].ToString();
                addURL = htProgram[ProgramParameters.AddURL.ToString()].ToString();
                searchURL = htProgram[ProgramParameters.AddURL.ToString()].ToString();
                hf_MURL.Value = modifyURL;
                hf_DURL.Value = deleteURL;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void LoadContent()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["ProgramID"] != null)
                {
                    DataTable dt_menulist = new DataTable();
                    dt_menulist = (DataTable)Session[FINSessionConstants.MenuData];
                    DataRow[] dr = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());
                    if (dr.Length > 0)
                    {
                        Startup(int.Parse(Session["ProgramID"].ToString()));
                        //if (dr[0]["MODULECODE"].ToString() == FIN.BLL.FINListConstants_BLL.GL)
                        //{
                        FN_GLListScreen(dr[0]["SCREEN_CODE"].ToString());
                        hf_ScreenCode.Value = dr[0]["SCREEN_CODE"].ToString();
                        // }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FN_GLListScreen(string str_ScrenCode)
        {
            try
            {
                ErrorCollection.Clear();
                string strlist = "";
                DataTable dtData = new DataTable();
                //  gridData = Master.SetupGridnew(gridData, FindData(modifyURL, deleteURL), Master.KeyName);
                strlist = FIN.DAL.FINSP.GetSPFOR_Listscreen(str_ScrenCode.ToString(), false, true);
                strlist = strlist.Replace("'MODIFYDELETE'", modifyURL + "," + deleteURL);
                dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];

                Session["QueryFormData"] = dtData;
                Session["FullQueryData"] = dtData;
                // Session[FINSessionConstants.GridData] = dtData;
                gridData = Master.SetupGridnew(gridData, dtData, Master.KeyName, str_ScrenCode);
                chkAllData.Visible = false;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ErrorCollection.Clear();
                    ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Bind the records into gridviw
        /// </summary>
        /// <param name="modifyURL"></param>
        /// <param name="deleteURL"></param>
        /// <returns></returns>

        private DataTable FindData(string modifyURL, string deleteURL)
        {
            //return DBMethod.ExecuteQuery(iAcademeSQL.GetCourseSubjectList(modifyURL, deleteURL)).Tables[0];
            return FIN.BLL.GL.AccountingGroups_BLL.getListData(modifyURL, deleteURL);
        }

        #region "Paging"
        /// <summary>
        /// Set the paging into Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                gridData.PageIndex = e.NewPageIndex;
                gridData.Columns.Clear();
                gridData = Master.SetupGridnew(gridData, (DataTable)Session["QueryFormData"], Master.KeyName, hf_ScreenCode.Value.ToString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //for (int rowIndex = gridData.Rows.Count - 2;
            //                                   rowIndex >= 0; rowIndex--)
            //{
            //    GridViewRow gvRow = gridData.Rows[rowIndex];
            //    GridViewRow gvPreviousRow = gridData.Rows[rowIndex + 1];
            //    for (int cellCount = 0; cellCount < gvRow.Cells.Count;
            //                                                  cellCount++)
            //    {
            //        if (gvRow.Cells[cellCount].Text ==
            //                               gvPreviousRow.Cells[cellCount].Text)
            //        {
            //            if (gvPreviousRow.Cells[cellCount].RowSpan < 2)
            //            {
            //                gvRow.Cells[cellCount].RowSpan = 2;
            //            }
            //            else
            //            {
            //                gvRow.Cells[cellCount].RowSpan =
            //                    gvPreviousRow.Cells[cellCount].RowSpan + 1;
            //            }
            //            gvPreviousRow.Cells[cellCount].Visible = false;
            //        }
            //    }

            //}
        }

        protected void btnGridContSearch_Click(object sender, EventArgs e)
        {
            if (!chkAllData.Checked)
            {
                FilterDBData();
            }
            else
            {
                FilterDBData();
                //FilterGridData();
            }


        }
        private void FilterGridData()
        {
            DataTable dt_QueryData = (DataTable)Session["FullQueryData"];
            var tmp_data = dt_QueryData.AsEnumerable().Where(r => r[hf_ColName.Value].ToString().ToUpper().Contains(hf_ColValue.Value.ToString().ToUpper()));
            DataTable dt_ResultQuery = new DataTable();
            gridData.Columns.Clear();
            if (tmp_data.Any())
            {
                dt_ResultQuery = tmp_data.CopyToDataTable();
                Session["QueryFormData"] = dt_ResultQuery;
            }
            else
            {
                dt_ResultQuery = dt_QueryData.Copy();
                dt_ResultQuery.Rows.Clear();
            }
            gridData = Master.SetupGridnew(gridData, dt_ResultQuery, Master.KeyName, hf_ScreenCode.Value);
            TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
            txt_tmp.Text = hf_ColValue.Value;
            txt_tmp.Focus();
        }

        private void FilterDBData()
        {

            try
            {
                ErrorCollection.Clear();
                string strlist = "";
                DataTable dtData = new DataTable();
                //  gridData = Master.SetupGridnew(gridData, FindData(modifyURL, deleteURL), Master.KeyName);
                if (hf_ColName.Value.ToString().Length > 0)
                {
                    strlist = FIN.DAL.FINSP.GetSPFOR_Filter_Listscreen(hf_ScreenCode.Value.ToString(), hf_ColName.Value, hf_ColValue.Value.ToString().ToUpper());

                }
                else
                {
                    strlist = FIN.DAL.FINSP.GetSPFOR_Listscreen(hf_ScreenCode.Value.ToString(), false, true);
                }
                strlist = strlist.Replace("'MODIFYDELETE'", hf_MURL.Value + "," + hf_DURL.Value);
                dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];
                Session["QueryFormData"] = dtData;
                Session["FullQueryData"] = dtData;
                gridData.Columns.Clear();

                gridData = Master.SetupGridnew(gridData, dtData, Master.KeyName, hf_ScreenCode.Value);

                if (hf_ColName.Value.ToString().Length > 0)
                {
                    TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
                    if (txt_tmp != null)
                    {
                        txt_tmp.Text = hf_ColValue.Value;
                        txt_tmp.Focus();
                    }

                }
                hf_keyValue.Value = "";
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("colsearcherror", ex.Message + ex.InnerException);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //ErrorCollection.Clear();
                    ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlnoOfRecord_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridData.PageSize = int.Parse(ddlnoOfRecord.SelectedItem.Text);
            DataTable dtData = (DataTable)Session["QueryFormData"];

            gridData.Columns.Clear();
            gridData = Master.SetupGridnew(gridData, dtData, Master.KeyName, hf_ScreenCode.Value);

        }

        protected void btnFirst_Click(object sender, EventArgs e)
        {
            if (hf_ColName.Value.ToString().Length > 0 && hf_ColValue.Value.ToString().Length > 0)
            {
                FilterDBData();
            }
            gridData.PageIndex = 1;
            gridData.Columns.Clear();
            gridData = Master.SetupGridnew(gridData, (DataTable)Session["QueryFormData"], Master.KeyName, hf_ScreenCode.Value.ToString());
        }

        protected void btnLast_Click(object sender, EventArgs e)
        {
            if (hf_ColName.Value.ToString().Length > 0 && hf_ColValue.Value.ToString().Length > 0)
            {
                FilterDBData();
            }
            DataTable dtData = (DataTable)Session["QueryFormData"];
            gridData.PageIndex = dtData.Rows.Count / gridData.PageSize;
            gridData.Columns.Clear();
            gridData = Master.SetupGridnew(gridData, (DataTable)Session["QueryFormData"], Master.KeyName, hf_ScreenCode.Value.ToString());
        }

        protected void btn_MultiText_Search_Click(object sender, EventArgs e)
        {
            string str_value = hf_keyValue.Value;
            if (str_value.Trim().Length > 0)
            {
                try
                {
                    str_value = str_value.Trim().Substring(1);
                    ErrorCollection.Clear();
                    string strlist = "";
                    DataTable dtData = new DataTable();

                    strlist = FIN.DAL.FINSP.GetSPFOR_Filter_MULTICOL_Listscreen(hf_ScreenCode.Value.ToString(), str_value);

                   
                        
                    
                    strlist = strlist.Replace("'MODIFYDELETE'", hf_MURL.Value + "," + hf_DURL.Value);
                    dtData = FIN.DAL.DBMethod.ExecuteQuery(strlist).Tables[0];
                    Session["QueryFormData"] = dtData;
                    Session["FullQueryData"] = dtData;
                    gridData.Columns.Clear();

                    gridData = Master.SetupGridnew(gridData, dtData, Master.KeyName, hf_ScreenCode.Value);

                    if (hf_ColName.Value.ToString().Length > 0)
                    {
                        TextBox txt_tmp = (TextBox)gridData.HeaderRow.FindControl(hf_ColName.Value);
                        if (txt_tmp != null)
                        {
                            txt_tmp.Text = hf_ColValue.Value;
                            txt_tmp.Focus();
                        }

                    }

                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("colsearcherror", ex.Message + ex.InnerException);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        //ErrorCollection.Clear();
                        ErrorCollection.Add("not allowed", "Query facility not allowed for this form");
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    }
                }

            }

        }

    }
}