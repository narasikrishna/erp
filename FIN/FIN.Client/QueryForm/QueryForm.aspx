﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="QueryForm.aspx.cs" Inherits="FIN.Client.QueryForm.QueryForm" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="ListGrid" align="left" style="width: 100%;">
        <div id="div_Filter" runat="server" style="width: 420px" visible="false">
            <div id="div_txtFilter_1" style="float: left;" runat="server" visible="false">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_txtlbl_1" runat="server">
                    TXT Filter 1
                </div>
                <div style="float: left; width: 100px" id="div_Filter_txt_1" runat="server" visible="false">
                    <asp:TextBox ID="txtFilter1" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                </div>
            </div>
            <div id="div_txtFilter_2" style="float: left;" runat="server">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_txtlbl_2" runat="server"
                    visible="false">
                    TXT Filter 2
                </div>
                <div style="float: right; width: 100px" id="div_Filter_txt_2" runat="server" visible="false">
                    <asp:TextBox ID="txtFilter2" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                </div>
            </div>
            <div id="div_ddlFilter_1" style="float: left;" runat="server">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_ddllbl_1" runat="server"
                    visible="false">
                    DDL Filter 1
                </div>
                <div style="float: left; width: 310px" id="div_Filter_ddl_1" runat="server" visible="false">
                    <asp:DropDownList ID="ddlFilter1" runat="server" CssClass="ddlStype" Width="99%">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="div_ddlFilter_2" style="float: left;" runat="server">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_ddllbl_2" runat="server"
                    visible="false">
                    DDL Filter 2
                </div>
                <div style="float: left; width: 310px" id="div_Filter_ddl_2" runat="server" visible="false">
                    <asp:DropDownList ID="ddlFilter2" runat="server" Width="99%">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="div_dtFilter_1" style="float: left;" runat="server">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_dtlbl_1" runat="server"
                    visible="false">
                    Date Filter 1
                </div>
                <div style="float: left; width: 100px" id="div_Filter_dt_1" runat="server" visible="false">
                    <asp:TextBox ID="dtFilter1" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="dtFilter1"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div id="div_dtFilter_2" style="float: left;" runat="server">
                <div class="lblBox" style="float: left; width: 100px" id="div_Filter_dtlbl_2" runat="server"
                    visible="false">
                    Date Filter 2
                </div>
                <div style="float: right; width: 100px" id="div_Filter_dt_2" runat="server" visible="false">
                    <asp:TextBox ID="dtFilter2" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="dtFilter2"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div style="clear: both">
            </div>
            <div style="float: right">
                <asp:Button ID="btnSearch" runat="server" Text="Search" />
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div style="width: 100%" align="center">
            <asp:CheckBox ID="chkAllData" runat="server" Text="Open Period" Checked="true" CssClass="lblBox"
                Visible="false" />
        </div>
        <div style="width: 100%">
            <div class="lblBox" style="float: left; width: 100px" id="lblInvoiceNumber">
                No Of Record
            </div>
            <div class="divtxtBox" style="float: left; width: 80px" id="lblInvoiceNoValue">
                <asp:DropDownList ID="ddlnoOfRecord" runat="server" Width="100%" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlnoOfRecord_SelectedIndexChanged">
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>40</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                    <asp:ListItem>60</asp:ListItem>
                    <asp:ListItem>70</asp:ListItem>
                    <asp:ListItem>80</asp:ListItem>
                    <asp:ListItem>90</asp:ListItem>
                    <asp:ListItem>100</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="float: right">
             <div class="lblBox" style="float: left; width: 100px" id="Div2">
                <asp:ImageButton ID="btnFirst" runat="server" ImageUrl="~/Images/btnFirst.png" OnClick="btnFirst_Click" Style="border: 0px;" />
                    <%--<asp:Button ID="btnFirst" runat="server" Text="First" 
                        onclick="btnFirst_Click" />--%>
                </div>
                <div class="lblBox" style="float: left; width: 100px" id="Div1">
                 <asp:ImageButton ID="btnLast" runat="server" ImageUrl="~/Images/btnLast.png" OnClick="btnLast_Click" Style="border: 0px;" />
                    <%--<asp:Button ID="btnLast" runat="server" Text="Last" onclick="btnLast_Click" />--%>
                </div>
                <div class="lblBox" style="float: left; width: 100px" id="Div3">
                <asp:ImageButton ID="btn_MultiText_Search" runat="server" ImageUrl="~/Images/btnSearch.png" OnClick="btn_MultiText_Search_Click" 
                Visible="false" Style="border: 0px;" />
                    <%--<asp:Button ID="btn_MultiText_Search" runat="server" Text="Search"  Visible="false"
                        onclick="btn_MultiText_Search_Click" />--%>
                </div>
            </div>
        </div>
        <div>
            <asp:GridView ID="gridData" runat="server" AutoGenerateColumns="False" GridLines="None"
                Style="width: 100%;" AllowPaging="true" CssClass="mGrid" PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" SelectedRowStyle-CssClass="selrow" OnPageIndexChanging="gridData_PageIndexChanging"
                EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gridData_RowDataBound">
                <Columns>
                </Columns>
                <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <PagerStyle CssClass="pgr"></PagerStyle>
                <SelectedRowStyle CssClass="selrow"></SelectedRowStyle>
            </asp:GridView>
        </div>
    </div>
    <asp:HiddenField ID="hf_ScreenCode" runat="server" />
    <asp:HiddenField ID="hf_MURL" runat="server" />
    <asp:HiddenField ID="hf_DURL" runat="server" />
    <asp:HiddenField ID="hf_ColName" runat="server" Value="" ClientIDMode="Static" />
    <asp:HiddenField ID="hf_ColValue" runat="server" Value="" ClientIDMode="Static" />
    <asp:Button ID="btnGridContSearch" runat="server" Text="Search" Style="display: none"
        OnClick="btnGridContSearch_Click" ClientIDMode="Static" />
        <asp:HiddenField ID="hf_keyValue" runat="server" Value=""  ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script type="text/javascript">
        function fn_GridMultiSearch(event, txt_obj, colName) {
            var colfiled = document.getElementById("hf_ColName");
            var valuefiled = document.getElementById("hf_ColValue");
            if (colfiled != null)
                colfiled.value = "";
            if (valuefiled != null)
                valuefiled.value = "";

            var var_keyvalue = $("#hf_keyValue").val();
            var split_keyvalue = var_keyvalue.split("~");
            $("#hf_keyValue").val(" ");
            //alert(split_keyvalue.length);
            var value_found = "0";
            if (split_keyvalue.length > 0) {
                for (i = 0; i < split_keyvalue.length; i++) {
                    var var_keyvalue = split_keyvalue[i].split(":");
                    if (var_keyvalue.length > 0) {
                        if (var_keyvalue[0] == colName) {
                            value_found = "1"
                            $("#hf_keyValue").val($("#hf_keyValue").val() + "~" + colName + ":" + txt_obj.value);
                        }
                        else {
                            if (var_keyvalue[0].length > 0) {
                                if (var_keyvalue[1] != undefined) {
                                    $("#hf_keyValue").val($("#hf_keyValue").val() + "~" + var_keyvalue[0] + ":" + var_keyvalue[1]);
                                }
                            }
                        }
                    }

                }
            }
            if (value_found == "0") {
                $("#hf_keyValue").val($("#hf_keyValue").val() + "~" + colName + ":" + txt_obj.value);
            }
          
        }
        function fn_GridRecSearch(event, txt_obj, colName) {
            
            var colfiled = document.getElementById("hf_ColName");
            var valuefiled = document.getElementById("hf_ColValue");
            if (colfiled != null)
                colfiled.value = "";
            if (valuefiled != null)
                valuefiled.value = "";
           

            if (event.keyCode == 13 || event.which == 13) {
                console.log("Inside keycode 13");
                console.log(colfiled);
                var btn_SearchCont = document.getElementById("btnGridContSearch")
                // alert(btn_SearchCont);
                if (btn_SearchCont != null) {
                    console.log("Inside btnSerach");
                    if (colfiled != null)
                        colfiled.value = colName;
                    if (valuefiled != null)
                        valuefiled.value = txt_obj.value;

                    console.log("Before click" + btn_SearchCont);
                    btn_SearchCont.click();
                    
                }
            }
        }
        function fn_searchclick() {
            var btn_SearchCont = document.getElementById("btnGridContSearch")
            alert(btn_SearchCont);
            if (btn_SearchCont != null) {
                btn_SearchCont.click();
            }
        }


       
        function fn_onLinkClick() {

            window.parent.fn_QueryFormDisplay();
        }
    </script>
</asp:Content>
