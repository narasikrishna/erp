﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="~/PR/DashBoard_PR.aspx.cs" Inherits="FIN.Client.PR.DashBoard_PR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowDeptEmp() {
            $("#divDeptEmpGrid").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="98%" border="0px">
            <tr>
                <td style="width: 40%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Salary
                                    </div>
                                    <div style="width: 200px; float: right; padding-right: 10px;">
                                        <asp:DropDownList ID="ddlDept" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 100px; float: right; padding-right: 10px">
                                        <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div id="divSalGraph" style="width: 98%">
                                    <asp:Chart ID="chrt_SalCount" runat="server" Width="500px" Height="230px">
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="s_Total_Net_Sal" IsValueShownAsLabel="True"
                                                XValueMember="pay_period_desc" YValueMembers="NET_SAL" CustomProperties="PieLabelStyle=Outside">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <AxisX>
                                                    <MajorGrid Enabled="false" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid Enabled="false" />
                                                </AxisY>
                                                <Area3DStyle Enable3D="True" Inclination="10" Rotation="20"></Area3DStyle>
                                            </asp:ChartArea>
                                            <%-- <asp:ChartArea Name="ChartArea2" >
                                                 </asp:ChartArea>--%>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Deductions
                                    </div>
                                    <div style="float: right; padding: 10px">
                                        <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowDeptEmp()" />
                                    </div>
                                    <div id="divDeptEmpGrid" style="width: 40%; background-color: ThreeDFace; position: fixed;
                                        z-index: 5000; top: 250px; left: 10px; display: none">
                                        <div style="width: 50px; float: right; padding-bottom: 10px;">
                                            <asp:Button ID="btnView" runat="server" Text="View" OnClick="btnView_Click" CssClass="btn" />
                                        </div>
                                        <div style="width: 150px; float: right; padding-bottom: 10px;">
                                            <asp:DropDownList ID="ddlPayElement" runat="server" CssClass="ddlStype">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 150px; float: right; padding-bottom: 10px; display:none">
                                            <asp:DropDownList ID="ddlEmpl" runat="server" CssClass="ddlStype" AutoPostBack="false"
                                                OnSelectedIndexChanged="ddlEmpl_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 150px; float: right; padding-bottom: 10px">
                                            <asp:DropDownList ID="ddlDeptName" runat="server" CssClass="ddlStype" AutoPostBack="false"
                                                OnSelectedIndexChanged="ddlDeptName_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 150px; float: right; padding-bottom: 10px;">
                                            <asp:DropDownList ID="ddlFinanceYr" runat="server" CssClass="ddlStype" AutoPostBack="false"
                                                OnSelectedIndexChanged="ddlFinanceYr_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div id="div1" style="width: 98%">
                                    <asp:Chart ID="chrt_Dept" runat="server" Height="250px" Width="500px">
                                        <Series>
                                            <asp:Series Name="S_Dept_Count" ChartType="Spline" XValueMember="pay_period_desc"
                                                YValueMembers="USED_AMT" IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                <AxisX>
                                                    <MajorGrid Enabled="false" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid Enabled="false" />
                                                </AxisY>
                                                <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 60%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divEmpExpir" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        <div style="float: left">
                                            Annual Leave Paid
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_ALP_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_ALP_Period_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_ALP_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_ALP_Year_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divAnnLeaveGraph">
                                        <asp:Chart ID="chrtAnnualLeave" runat="server" Width="800px" Height="230px">
                                            <Series>
                                                <asp:Series Name="sAnnualLeave" ChartType="Doughnut" XValueMember="DEPT_NAME" YValueMembers="tot_sal"
                                                    IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL" CustomProperties="PieLabelStyle=Outside">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                    <Area3DStyle Enable3D="True"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Right" Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                            <td valign="top" style="display: none">
                                <div id="div4" style="width: 100%">
                                    <div class="GridHeader" style="float: left; width: 100%">
                                        Indemnity Paid
                                    </div>
                                    <div class="divClear_10" style="width: 60%">
                                    </div>
                                    <div id="divIndemLeaveGraph">
                                        <asp:Chart ID="chrtIndemnityLeave" runat="server" Width="400px" Height="230px">
                                            <Series>
                                                <asp:Series Name="sIndemnityLeave" ChartType="Doughnut" XValueMember="EMP_EXPI" YValueMembers="No_Of_Emp"
                                                    IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Bottom" Name="Legend1">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div class="GridHeader" style="float: left; width: 100%">
                                    <div style="float: left">
                                        Employee Details
                                    </div>
                                    <div style="width: 250px; float: right; padding-right: 10px;">
                                        <asp:DropDownList ID="ddlEmplName" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlEmplName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="divClear_10" style="width: 60%">
                                </div>
                                <div id="divEmpDetails">
                                    <div class="divRowContainer">
                                        <div class="lblBox" style="float: left; width: 80px" id="lblGroupName">
                                            Number
                                        </div>
                                        <div class="lblBox" style="float: left; width: 100px; font-weight: bold">
                                            <asp:Label ID="lblEmpNo" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="colspace" style="float: left;">
                                            &nbsp</div>
                                        <div class="lblBox" style="float: left; width: 80px" id="Div2">
                                            Name
                                        </div>
                                        <div class="lblBox" style="float: left; width: 350px; font-weight: bold">
                                            <asp:Label ID="lblNmae" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <div class="divRowContainer">
                                        <div class="lblBox" style="float: left; width: 80px" id="Div3">
                                            Department
                                        </div>
                                        <div class="lblBox" style="float: left; width: 250px; font-weight: bold">
                                            <asp:Label ID="lblDepartmet" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="colspace" style="float: left;">
                                            &nbsp</div>
                                        <div class="lblBox" style="float: left; width: 80px" id="Div5">
                                            Designation
                                        </div>
                                        <div class="lblBox" style="float: left; width: 250px; font-weight: bold">
                                            <asp:Label ID="lblDesignation" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <div class="divRowContainer">
                                        <div class="lblBox" style="float: left; width: 80px" id="Div6">
                                            DOJ
                                        </div>
                                        <div class="lblBox" style="float: left; width: 80px; font-weight: bold">
                                            <asp:Label ID="lblDOJ" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="colspace" style="float: left;">
                                            &nbsp</div>
                                        <div class="lblBox" style="float: left; width: 80px" id="Div7">
                                            DOC
                                        </div>
                                        <div class="lblBox" style="float: left; width: 80px; font-weight: bold">
                                            <asp:Label ID="lblDOC" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <div style="width: 150px; float: right; padding-right: 270px;">
                                        <asp:DropDownList ID="ddlFinYr" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlFinYr_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <asp:Chart ID="chrtEmpDetails" runat="server" Width="750px" Height="150px">
                                        <Series>
                                            <asp:Series Name="sEmpDetails" XValueMember="pay_period_desc" YValueMembers="net_sal"
                                                IsValueShownAsLabel="True">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <AxisX>
                                                    <MajorGrid Enabled="false" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid Enabled="false" />
                                                </AxisY>
                                                <Area3DStyle Enable3D="false" Inclination="10" Rotation="20"></Area3DStyle>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
