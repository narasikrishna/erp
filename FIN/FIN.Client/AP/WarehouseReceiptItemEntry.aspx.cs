﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class WarehouseReceiptItemEntry : PageBase
    {

        INV_RECEIPT_ITEM_WH_HDR iNV_RECEIPT_ITEM_WH_HDR = new INV_RECEIPT_ITEM_WH_HDR();
        INV_RECEIPT_ITEM_WH_DTL iNV_RECEIPT_ITEM_WH_DTL = new INV_RECEIPT_ITEM_WH_DTL();
        WarehouseReceiptItem_BLL WarehouseReceiptItem_BLL = new WarehouseReceiptItem_BLL();
        PurchaseItemReceipt_BLL PurchaseItemReceipt_BLL = new PurchaseItemReceipt_BLL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        string trnType = string.Empty;
        int trnTypeBool = 0;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            WarehouseReceiptItem_BLL.fn_getWarehouse(ref ddlWarehouseNumber);
            PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddlGRNNumber);
            //  WarehouseReceiptItem_BLL.fn_getLineNo(ref ddlLineNumber);


        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtWarehouseName.Enabled = false;
                txtGRNDate.Enabled = false;
                txtPONumber.Enabled = false;
                txtItemDescription.Enabled = false;
                txtPOLineNumber.Enabled = false;
                //  txtWHQuantity.Enabled = false;
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.AP.WarehouseReceiptItem_BLL.getChildEntityDet(Master.StrRecordId);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INV_RECEIPT_ITEM_WH_HDR> userCtx = new DataRepository<INV_RECEIPT_ITEM_WH_HDR>())
                    {
                        iNV_RECEIPT_ITEM_WH_HDR = userCtx.Find(r =>
                            (r.INV_RECEIPT_ITEM_WH_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_RECEIPT_ITEM_WH_HDR;

                    ddlWarehouseNumber.SelectedItem.Text = iNV_RECEIPT_ITEM_WH_HDR.WH_ID.ToString();
                    ddlGRNNumber.SelectedValue = iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID.ToString();

                    INV_WAREHOUSES INV_WAREHOUSES = new INV_WAREHOUSES();
                    using (IRepository<INV_WAREHOUSES> userCtx = new DataRepository<INV_WAREHOUSES>())
                    {
                        INV_WAREHOUSES = userCtx.Find(r =>
                            (r.INV_WH_ID == (iNV_RECEIPT_ITEM_WH_HDR.WH_ID.ToString()))
                            ).SingleOrDefault();
                    }

                    if (INV_WAREHOUSES != null)
                    {
                        txtWarehouseName.Text = INV_WAREHOUSES.INV_WH_NAME;
                        ddlWarehouseNumber.SelectedValue = INV_WAREHOUSES.INV_WH_NAME;
                    }

                    FillLineNumberDetails();

                    //INV_RECEIPT_DTLS INV_RECEIPT_DTLS = new INV_RECEIPT_DTLS();
                    //using (IRepository<INV_RECEIPT_DTLS> userCtx = new DataRepository<INV_RECEIPT_DTLS>())
                    //{
                    //    INV_RECEIPT_DTLS = userCtx.Find(r =>
                    //        (r.ITEM_ID == (iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID.ToString()) && r.RECEIPT_ID == iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID)
                    //        ).SingleOrDefault();
                    //}

                    //if (INV_RECEIPT_DTLS != null)
                    //{
                    ddlLineNumber.SelectedValue = iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_DTL_ID.ToString();
                    // }

                    FillPODetails();
                    txtWHQuantity.Text = iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString();
                    txtWHQuantity.Enabled = false;
                    Session["Quantity"] = iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString();
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (EntityData != null)
                {
                    iNV_RECEIPT_ITEM_WH_HDR = (INV_RECEIPT_ITEM_WH_HDR)EntityData;
                }


                iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID = ddlGRNNumber.SelectedValue.ToString();
                if (Session["Item_Id"] != null)
                {
                    iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID = Session["Item_Id"].ToString();
                }
                iNV_RECEIPT_ITEM_WH_HDR.WH_ID = ddlWarehouseNumber.SelectedItem.Text;
                iNV_RECEIPT_ITEM_WH_HDR.WH_QTY = decimal.Parse(txtWHQuantity.Text);

                //iNV_RECEIPT_LOTS_HDR.ITEM_ID = hid_Item_Id.Value;
                iNV_RECEIPT_ITEM_WH_HDR.ENABLED_FLAG = FINAppConstants.Y;

                iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_DTL_ID = ddlLineNumber.SelectedValue.ToString();

                // iNV_RECEIPT_ITEM_WH_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_RECEIPT_ITEM_WH_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_RECEIPT_ITEM_WH_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_016_M.ToString(), false, true);
                    iNV_RECEIPT_ITEM_WH_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.INV_RECEIPT_ITEM_WH_HDR_SEQ);
                    iNV_RECEIPT_ITEM_WH_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_RECEIPT_ITEM_WH_HDR.CREATED_DATE = DateTime.Today;
                }
                iNV_RECEIPT_ITEM_WH_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                iNV_RECEIPT_ITEM_WH_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //FIN.BLL.CommonUtils.DeleteData(typeof(INV_RECEIPT_ITEM_WH_DTL).Name.ToString(), "INV_RECEIPT_ITEM_WH_ID", iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID, "", "", "");
                FIN.BLL.CommonUtils.DeleteData(typeof(INV_RECEIPT_ITEM_WH_DTL).Name.ToString(), "LOT_ID", iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID, "", "", "");

                //DBMethod.ExecuteNonQuery("Delete iNV_RECEIPT_ITEM_WH_DTL where INV_RECEIPT_ITEM_WH_ID='" + iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID + "'");

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_RECEIPT_ITEM_WH_DTL = new INV_RECEIPT_ITEM_WH_DTL();

                    if ((dtGridData.Rows[iLoop]["PK_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<INV_RECEIPT_ITEM_WH_DTL> userCtx = new DataRepository<INV_RECEIPT_ITEM_WH_DTL>())
                        {
                            iNV_RECEIPT_ITEM_WH_DTL = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    iNV_RECEIPT_ITEM_WH_DTL = new INV_RECEIPT_ITEM_WH_DTL();
                    iNV_RECEIPT_ITEM_WH_DTL.LOT_ID = dtGridData.Rows[iLoop]["LOT_ID"].ToString();
                    iNV_RECEIPT_ITEM_WH_DTL.LOT_QTY = decimal.Parse(dtGridData.Rows[iLoop]["LOT_QTY"].ToString());

                    iNV_RECEIPT_ITEM_WH_DTL.INV_RECEIPT_ITEM_WH_ID = iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID;
                    iNV_RECEIPT_ITEM_WH_DTL.CHILD_ID = iNV_RECEIPT_ITEM_WH_HDR.PK_ID;
                    iNV_RECEIPT_ITEM_WH_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    iNV_RECEIPT_ITEM_WH_DTL.ENABLED_FLAG = "1";

                    ErrorCollection.Remove("WAREHOUSE RECEIPT ITEMS WITH LOT");
                    //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_FOUR(Master.FormCode, iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID, iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_DTL_ID, iNV_RECEIPT_ITEM_WH_DTL.LOT_ID);
                    //if (ProReturn != string.Empty)
                    //{
                    //    if (ProReturn != "0")
                    //    {
                    //        ErrorCollection.Add("WAREHOUSE RECEIPT ITEMS WITH LOT", ProReturn);
                    //        if (ErrorCollection.Count > 0)
                    //        {
                    //            return;
                    //        }
                    //    }
                    //}
                    if (WarehouseReceiptItem_DAL.IsWHLotExists(iNV_RECEIPT_ITEM_WH_HDR.INV_RECEIPT_ITEM_WH_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID, iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_DTL_ID, iNV_RECEIPT_ITEM_WH_DTL.LOT_ID))
                    {
                        ErrorCollection.Add("WAREHOUSE RECEIPT ITEMS WITH LOT", "Warehouse already in use");
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_ITEM_WH_DTL, "D"));
                    }
                    else
                    {
                        if ((dtGridData.Rows[iLoop]["PK_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                        {
                            iNV_RECEIPT_ITEM_WH_DTL.PK_ID = int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            //iNV_RECEIPT_ITEM_WH_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.iNV_RECEIPT_ITEM_WH_DTL_SEQ);
                            iNV_RECEIPT_ITEM_WH_DTL.MODIFIED_BY = this.LoggedUserName;
                            iNV_RECEIPT_ITEM_WH_DTL.MODIFIED_DATE = DateTime.Today;
                            iNV_RECEIPT_ITEM_WH_DTL.CREATED_BY = this.LoggedUserName;
                            iNV_RECEIPT_ITEM_WH_DTL.CREATED_DATE = DateTime.Today;

                            //   iNV_RECEIPT_ITEM_WH_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_RECEIPT_ITEM_WH_DTL.PK_ID.ToString());

                            // tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_ITEM_WH_DTL, "A"));
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_ITEM_WH_DTL, "U"));
                        }
                        else
                        {
                            iNV_RECEIPT_ITEM_WH_DTL.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.iNV_RECEIPT_ITEM_WH_DTL_SEQ);
                            iNV_RECEIPT_ITEM_WH_DTL.CREATED_BY = this.LoggedUserName;
                            iNV_RECEIPT_ITEM_WH_DTL.CREATED_DATE = DateTime.Today;

                            // iNV_RECEIPT_ITEM_WH_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_RECEIPT_ITEM_WH_DTL.PK_ID.ToString());

                            tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_ITEM_WH_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_RECEIPT_ITEM_WH_HDR, INV_RECEIPT_ITEM_WH_DTL>(iNV_RECEIPT_ITEM_WH_HDR, tmpChildEntity, iNV_RECEIPT_ITEM_WH_DTL);

                            savedBool = true;

                            if (iNV_RECEIPT_ITEM_WH_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                trnType = FINSP.GetSPItemLedger(FINTableConstant.INV_PKG, FINTableConstant.item_txn_ledger_entry, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID);
                                trnTypeBool = FINSP.GetSPItemLedgerEntry(iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID);

                                trnTypeBool = int.Parse(trnType.ToString());
                                if (trnTypeBool == 0)
                                {
                                    trnType = "OPENBAL";
                                }
                                else if (trnTypeBool > 0)
                                {
                                    trnType = "RECEIVE";
                                }
                                //   FINSP.GetOracleFunction(FINTableConstant.INV_PKG, FINTableConstant.inv_txn_ledger, trnType, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString(), iNV_RECEIPT_ITEM_WH_HDR.WH_ID, DBMethod.ConvertDateToString(txtGRNDate.Text).ToString(), iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, Session["po_header_Id"].ToString(), Session["PO_LINE_ID"].ToString(), txtItemDescription.Text);
                                FINSP.GetSPItemtxnLedger(trnType, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString(), iNV_RECEIPT_ITEM_WH_HDR.WH_ID, DBMethod.ConvertDateToString(txtGRNDate.Text).ToString(), iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, Session["po_header_Id"].ToString(), Session["PO_LINE_ID"].ToString(), trnType + " of " + iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID);

                            }
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_RECEIPT_ITEM_WH_HDR, INV_RECEIPT_ITEM_WH_DTL>(iNV_RECEIPT_ITEM_WH_HDR, tmpChildEntity, iNV_RECEIPT_ITEM_WH_DTL, true);
                            savedBool = true;

                            if (iNV_RECEIPT_ITEM_WH_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                if (Session["Quantity"] != null)
                                {
                                    if (CommonUtils.ConvertStringToDecimal(Session["Quantity"].ToString()) != CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text.ToString()))
                                    {
                                        trnType = FINSP.GetSPItemLedger(FINTableConstant.INV_PKG, FINTableConstant.item_txn_ledger_entry, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID);
                                        trnTypeBool = FINSP.GetSPItemLedgerEntry(iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_ID);
                                        trnTypeBool = int.Parse(trnType.ToString());

                                        if (trnTypeBool == 0)
                                        {
                                            trnType = "OPENBAL";
                                        }
                                        else if (trnTypeBool > 0)
                                        {
                                            trnType = "RECEIVE";
                                        }

                                        // FINSP.GetOracleFunction(FINTableConstant.INV_PKG, FINTableConstant.inv_txn_ledger, trnType, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString(), iNV_RECEIPT_ITEM_WH_HDR.WH_ID, DBMethod.ConvertDateToString(txtGRNDate.Text).ToString(), iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, Session["po_header_Id"].ToString(), Session["PO_LINE_ID"].ToString(), txtItemDescription.Text);
                                        FINSP.GetSPItemtxnLedger(trnType, iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID, iNV_RECEIPT_ITEM_WH_HDR.WH_QTY.ToString(), iNV_RECEIPT_ITEM_WH_HDR.WH_ID, DBMethod.ConvertDateToString(txtGRNDate.Text).ToString(), iNV_RECEIPT_ITEM_WH_HDR.RECEIPT_ID, Session["po_header_Id"].ToString(), Session["PO_LINE_ID"].ToString(), trnType + " of " + iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID);

                                    }
                                }
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                TextBox txtquantity = tmpgvr.FindControl("txtquantity") as TextBox;
                //  WarehouseReceiptItem_BLL.fn_getLotNo(ref ddlLotNo);
                WarehouseReceiptItem_BLL.getLotNoBasedReceipt(ref ddlLotNo, ddlLineNumber.SelectedValue.ToString(), Master.Mode, Master.StrRecordId);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        txtquantity.Enabled = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();




                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                txtWHQuantity.Text = CommonUtils.CalculateTotalAmount(dtData, "LOT_QTY");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //    TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;
                //   txtquantity.Enabled = false;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
            TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    txtquantity.Enabled = false;
                }

            }

            slControls[0] = ddlLotNo;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList";
            string strMessage = Prop_File_Data["Lot_Number_P"] + "";
            //string strMessage = "Lot Number";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            //  txtWHQuantity.Text = WarehouseReceiptItem_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtquantity.Text));
            decimal recLotQty = 0;
            decimal receivableLotQty = 0;

            //DataTable dtqty = new DataTable();

            //dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.getReceivedLotqtyinWHI(ddlLotNo.SelectedValue)).Tables[0];
            //if (dtqty.Rows.Count > 0)
            //{
            //    recLotQty = CommonUtils.ConvertStringToDecimal(dtqty.Rows[0]["tot_qty"].ToString());


            //}

            if (hdQuantity.Value != string.Empty)
            {
                receivableLotQty = CommonUtils.ConvertStringToDecimal(hdQuantity.Value);// -recLotQty;
            }

            ErrorCollection.Remove("lotqty");

            if (CommonUtils.ConvertStringToDecimal(txtquantity.Text) > (receivableLotQty))
            {
                ErrorCollection.Add("lotqty", "Quantity Cannot be greater than purchase item quantity");
                return drList;
            }


            if (ddlLotNo.SelectedItem != null)
            {
                drList[FINColumnConstants.LOT_ID] = ddlLotNo.SelectedItem.Value;
                drList["LOT_NUMBER"] = ddlLotNo.SelectedItem.Text;
            }
            drList["LOT_QTY"] = txtquantity.Text;



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // GridViewRow gvr = (GridViewRow)((Control)e.Row).Parent.Parent;
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<INV_RECEIPT_ITEM_WH_HDR>(iNV_RECEIPT_ITEM_WH_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillPODetails()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtpodata = new DataTable();
                //  dtpodata = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getpodata(ddlLineNumber.SelectedValue)).Tables[0];
                dtpodata = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLineDetails(ddlGRNNumber.SelectedValue.ToString(), ddlLineNumber.SelectedValue.ToString())).Tables[0];
                if (dtpodata != null)
                {
                    if (dtpodata.Rows.Count > 0)
                    {
                        //txtPONumber.Text = dtpodata.Rows[0]["PO_NUM"].ToString();
                        //txtItemDescription.Text = dtpodata.Rows[0]["ITEM_DESCRIPTION"].ToString();
                        //txtPOLineNumber.Text = dtpodata.Rows[0]["PO_LINE_NUM"].ToString();

                        txtPONumber.Text = dtpodata.Rows[0][FINColumnConstants.PO_NUM].ToString();
                        txtPOLineNumber.Text = dtpodata.Rows[0][FINColumnConstants.PO_LINE_NUM].ToString();
                        txtItemDescription.Text = dtpodata.Rows[0][FINColumnConstants.inv_description].ToString();
                        Session["Item_Id"] = dtpodata.Rows[0][FINColumnConstants.ITEM_ID].ToString();
                        Session["po_header_Id"] = dtpodata.Rows[0][FINColumnConstants.PO_HEADER_ID].ToString();
                        Session["PO_LINE_ID"] = dtpodata.Rows[0][FINColumnConstants.PO_LINE_ID].ToString();

                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillPODetails();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                    BindGrid(dtGridData);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                decimal totLotQty = 0;
                DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
                TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;
                DataTable dtqty = new DataTable();

                if (ddlLotNo.SelectedValue != string.Empty)
                {
                    dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getQuatity(ddlLotNo.SelectedValue)).Tables[0];
                    totLotQty = CommonUtils.ConvertStringToDecimal(dtqty.Rows[0]["LOT_QTY"].ToString());


                    decimal receivedLotQty = 0;

                    dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.getReceivedLotqtyinWHI(ddlLotNo.SelectedValue)).Tables[0];
                    if (dtqty.Rows.Count > 0)
                    {
                        receivedLotQty = CommonUtils.ConvertStringToDecimal(dtqty.Rows[0]["tot_qty"].ToString());

                        txtquantity.Text = (totLotQty - receivedLotQty).ToString();
                        hdQuantity.Value = txtquantity.Text;

                    }

                }

                if (txtWHQuantity.Text.Trim() == string.Empty)
                {
                    txtWHQuantity.Text = "0";
                }
                //if (CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtquantity.Text) >= 0)
                //{
                //    txtWHQuantity.Text = (CommonUtils.ConvertStringToDecimal(txtquantity.Text) + CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text)).ToString();
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlWarehouseNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                txtWarehouseName.Text = ddlWarehouseNumber.SelectedValue;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillLineNumberDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getgrndata(ddlGRNNumber.SelectedValue)).Tables[0];
            txtGRNDate.Text = DateTime.Parse(dtgrndata.Rows[0]["GRN_DATE"].ToString()).ToString("dd/MM/yyyy");
            PurchaseItemReceipt_BLL.GetPOItemLineBasedLot(ref ddlLineNumber, ddlGRNNumber.SelectedValue.ToString());


        }
        protected void ddlGRNNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillLineNumberDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }







    }
}