﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TransactionLedgerEntry.aspx.cs" Inherits="FIN.Client.AP.TransactionLedgerEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <%--<div class="divRowContainer" runat="server" >
            <div class="lblBox" style="float: left; width: 200px" id="lblTaxName">
                Transaction Type
            </div>
            <div class="divtxtBox" style="float: left; width: 350px">
                <asp:DropDownList ID="ddlTransactiontype" runat="server" CssClass="validate[] ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxRate">
                Item
            </div>
            <div class="divtxtBox LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlItem" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblWarehouse">
                Warehouse
            </div>
            <div class="divtxtBox LNOrient" style=" width: 350px">
                <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div1">
            </div>
            <div class="divtxtBox LNOrient" style=" width: 350px">
                <asp:ImageButton ID="btnProcess" runat="server" ImageUrl="~/Images/Process.png" OnClick="btnProcess_Click" TabIndex="3"
                    Style="border: 0px" />
                <%-- <asp:Button runat="server" CssClass="button" ID="btnProcess" Text="Process"  TabIndex="3"
                    Width="60px" onclick="btnProcess_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblQuantity">
                Quantity
            </div>
            <div class="divtxtBox" style="float: left; width: 350px">
                <asp:TextBox ID="txtQuantity" MaxLength="50" CssClass="validate[required] RequiredField  txtBox_N"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblTransactiondate">
                Transaction Date
            </div>
            <div class="divtxtBox" style="float: left; width: 350px">
                <asp:TextBox ID="txtTransdate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtTransdate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblRemarks">
                Remarks
            </div>
            <div class="divtxtBox" style="float: left; width: 350px">
                <asp:TextBox ID="txtRemarks" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="7" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="INV_TRANS_TYPE" HeaderText="Transaction Type" />
                    <asp:BoundField DataField="ITEM_NAME" HeaderText="Item" />
                    <asp:BoundField DataField="INV_QTY" HeaderText="Quantity" />
                    <asp:BoundField DataField="INV_QOH" HeaderText="Quantity on Hand" />
                    <asp:BoundField DataField="INV_TRANS_DATE" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transaction Date" />
                    <asp:BoundField DataField="INV_WH_NAME" HeaderText="Warehouse" />
                    <%-- <asp:BoundField DataField="RECEIPT" HeaderText="Receipt" />                    
                    <asp:BoundField DataField="PO_NUM" HeaderText="PO Number" />--%>
                    <%-- <asp:BoundField DataField="PO_LINE_NUM" HeaderText="PO Line No" />--%>
                    <%-- <asp:BoundField DataField="LOT_NUMBER" HeaderText="Lot No" />--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
