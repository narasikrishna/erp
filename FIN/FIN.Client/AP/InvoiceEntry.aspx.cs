﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AP
{
    public partial class InvoiceEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        INV_INVOICES_HDR iNV_INVOICES_HDR = new INV_INVOICES_HDR();
        INV_INVOICES_DTLS iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();
        DataTable dtGridData = new DataTable();
        Terms_BLL Terms_BLL = new Terms_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session["INV_PREPAYMENT"] = null;
                    Session["InvTaxDet"] = null;
                    AssignToControl();

                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
                else
                {
                    SetFocusControls(sender);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            Supplier_BLL.GetSupplierName(ref ddlSupplierName);
            Supplier_BLL.GetSupplierNumber(ref ddlSupplierNumber);
            Lookup_BLL.GetLookUpValues(ref ddlInvoiceType, "INV_TY");
            Lookup_BLL.GetLookUpValues(ref ddlPaymentType, "PAYMENT_MODE");
            FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref ddlInvoiceCurrency);
            FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref ddlPaymentCurrency);
            Terms_BLL.getTermName(ref ddlTerm);
            InvoiceBatch_BLL.fn_getIvoiceBatch(ref ddlBatch);

            Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);
            Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");

            if (ddlInvoiceType.Items.Count >= 1)
            {
                ddlInvoiceType.SelectedIndex = 1;
            }
            if (ddlExchangeRateType.Items.Count >= 1)
            {
                ddlExchangeRateType.SelectedIndex = 1;
            }
            if (ddlGlobalSegment.Items.Count >= 1)
            {
                ddlGlobalSegment.SelectedIndex = 1;
            }

        }

        private Control GetControlThatCausedPostBack(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button || c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }
        private void SetFocusControls(object sender)
        {
            WebControl wcICausedPostBack = (WebControl)GetControlThatCausedPostBack(sender as Page);
            int indx = wcICausedPostBack.TabIndex;
            var ctrl = from control in wcICausedPostBack.Parent.Controls.OfType<WebControl>()
                       where control.TabIndex > indx
                       select control;
            ctrl.DefaultIfEmpty(wcICausedPostBack).First().Focus();
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                hf_Temp_RefNo.Value = "";
                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                ddlInvoiceCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
                ddlPaymentCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
                txtInvoiceDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = false;

                btnPrint.Visible = false;
                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnCancel.Visible = false;
                imgBtnCancelJV.Visible = false;
                imgbtnInvoiceCancel.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    imgbtnInvoiceCancel.Visible = true;
                    ddlInvoiceType.Enabled = false;
                    btnPrint.Visible = true;
                    iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = iNV_INVOICES_HDR;




                    txtInvoiceNumber.Text = iNV_INVOICES_HDR.INV_NUM;
                    if (iNV_INVOICES_HDR.INV_DATE != null)
                    {
                        txtInvoiceDate.Text = DBMethod.ConvertDateToString(iNV_INVOICES_HDR.INV_DATE.ToString());

                    }
                    ddlSupplierNumber.SelectedValue = iNV_INVOICES_HDR.VENDOR_ID;
                    BindSupplierName();
                    ddlSupplierName.SelectedValue = iNV_INVOICES_HDR.VENDOR_ID;
                    ddlInvoiceType.SelectedValue = iNV_INVOICES_HDR.INV_TYPE;
                    ddlPaymentType.SelectedValue = iNV_INVOICES_HDR.INV_PAYMENT_METHOD;

                    ddlInvoiceCurrency.SelectedValue = iNV_INVOICES_HDR.INV_CURR_CODE;
                    ddlPaymentCurrency.SelectedValue = iNV_INVOICES_HDR.INV_PAY_CURR_CODE;
                    txtReason.Text = iNV_INVOICES_HDR.INV_REMARKS;
                    txtRejectedReason.Text = iNV_INVOICES_HDR.INV_REJECT_REASON;
                    txtInvoiceAmount.Text = iNV_INVOICES_HDR.INV_AMT.ToString();
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                    txtTax.Text = iNV_INVOICES_HDR.INV_TAX_AMT.ToString();
                    ddlTerm.SelectedValue = iNV_INVOICES_HDR.TERM_ID;
                    ddlBatch.SelectedValue = iNV_INVOICES_HDR.INV_BATCH_ID;


                    if (iNV_INVOICES_HDR.VENDOR_LOC_ID != null)
                        ddlSupplierSite.SelectedValue = iNV_INVOICES_HDR.VENDOR_LOC_ID;
                    if (iNV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE != null)
                        ddlExchangeRateType.SelectedValue = iNV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE;
                    txtExchangeRate.Text = iNV_INVOICES_HDR.INV_EXCHANGE_RATE.ToString();


                    if (iNV_INVOICES_HDR.INV_DUE_DATE != null)
                    {
                        txtInvDueDate.Text = DBMethod.ConvertDateToString(iNV_INVOICES_HDR.INV_DUE_DATE.ToString());
                    }

                    if (iNV_INVOICES_HDR.GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = iNV_INVOICES_HDR.GLOBAL_SEGMENT_ID;
                    }
                    if (iNV_INVOICES_HDR.INV_RETENTION_AMT != null)
                    {
                        //txtRetAmount.Text = iNV_INVOICES_HDR.INV_RETENTION_AMT.ToString();
                        txtRetAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNV_INVOICES_HDR.INV_RETENTION_AMT.ToString());
                    }
                    if (iNV_INVOICES_HDR.INV_PREPAY_BALANCE != null)
                    {
                        //txtPrePaymentBal.Text = iNV_INVOICES_HDR.INV_PREPAY_BALANCE.ToString();
                        txtPrePaymentBal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNV_INVOICES_HDR.INV_PREPAY_BALANCE.ToString());
                    }
                    if (iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                    }
                    if (iNV_INVOICES_HDR.REV_FLAG != null)

                    {
                        if (iNV_INVOICES_HDR.REV_FLAG.ToString() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            imgbtnInvoiceCancel.Visible = false;
                            lblCancelled.Visible = true;
                        }
                    }
                    
                    if (iNV_INVOICES_HDR.POSTED_FLAG != null)
                    {
                        if (iNV_INVOICES_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;
                            imgbtnInvoiceCancel.Visible = false;

                            imgBtnPost.Visible = false;
                            lblPosted.Visible = true;

                            imgBtnJVPrint.Visible = true;

                            imgBtnCancel.Visible = true;
                            lblCancelled.Visible = false;


                            pnlgridview.Enabled = false;
                            pnltdHeader.Enabled = false;
                            pnlReason.Enabled = false;
                            pnlRejReason.Enabled = false;

                        }
                    }
                    if (iNV_INVOICES_HDR.REV_JE_HDR_ID != null)
                    {
                        if (iNV_INVOICES_HDR.REV_JE_HDR_ID.ToString().Length > 0)
                        {
                            imgBtnCancel.Visible = false;
                            lblCancelled.Visible = true;

                            imgBtnCancelJV.Visible = true;
                        }
                    }

                    if (FINSP.Is_workflow_defined("AP_021"))
                    {
                        if (iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS == "0")
                        {
                            lblWaitApprove.Visible = true;
                        }
                        else
                        {
                            lblWaitApprove.Visible = false;
                        }
                    }
                    else
                    {
                        lblWaitApprove.Visible = false;
                    }


                }
                dtGridData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode == FINAppConstants.Add)
                {
                    LoadTempData();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlAccountCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                BindGSegment(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccCode");

            if (ddl_AccountCodes.SelectedValue != null && ddlGlobalSegment.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty && ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), ddlGlobalSegment.SelectedValue)).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {
                        //Label lblSegment = (Label)gvr.FindControl("lblSegment" + (iLoop + 1));
                        //lblSegment.Text = dtdataAc.Rows[iLoop][FINColumnConstants.SEGMENT_NAME].ToString();
                        if (iLoop == 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlGSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (iLoop + 1));
                            ddlGSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["INV_SEGMENT_ID" + (iLoop + 1)].ToString();

                        }
                    }
                }
            }
        }

        protected void ddlGSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGSegmentValues(sender);
        }
        private void LoadGSegmentValues(object sender)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Seg = (DropDownList)sender;
            int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlGSegment", ""));
            DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
            DropDownList ddlGSegment = (DropDownList)gvr.FindControl("ddlGSegment" + (int_ddlNo + 1));
            if (ddlGSegment != null && int_ddlNo < dtdataAc.Rows.Count)
            {
                if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                }
                else
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlGSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                }

            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;

                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INV_ITEM_QTY_BILLED", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("INV_ITEM_QTY_BILLED"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INV_ITEM_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INV_ITEM_PRICE"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INV_ITEM_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INV_ITEM_AMT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                txtInvoiceAmount.Text = CommonUtils.CalculateTotalAmount(dtData, FINColumnConstants.INV_ITEM_AMT);
                txtTax.Text = CommonUtils.CalculateTotalAmount(dtData, "INV_LINE_TAX_AMT");
                if (txtInvoiceAmount.Text.Length > 0)
                {
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                }
                //if (txtTax.Text.Length > 0)
                //{
                //    txtTax.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTax.Text);
                //}
                hf_TotTaxAmt.Value = "";

                SetColVis4InvoiceType();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSupplierName();
        }
        private void BindSupplierName()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName(ddlSupplierNumber.SelectedValue.ToString())).Tables[0];

                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        ddlSupplierName.SelectedItem.Text = dtData.Rows[0][FINColumnConstants.VENDOR_NAME].ToString();
                    }
                }
                FIN.BLL.AP.SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref  ddlSupplierSite, ddlSupplierNumber.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnOkay_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlSegment1;
                slControls[1] = ddlSegment2;
                slControls[2] = ddlSegment3;
                slControls[3] = ddlSegment4;
                slControls[4] = ddlSegment5;
                slControls[5] = ddlSegment6;

                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

                string strMessage = "Segment1 ~ Segment2 ~ Segment3~ Segment4 ~ Segment5 ~ Segment6";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}
                //else
                //{
                ModalPopupExtender2.Hide();
                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
                DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
                DropDownList ddlAccCode = tmpgvr.FindControl("ddlAccCode") as DropDownList;
                DropDownList dd_lItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
                // PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddl_ReceiptNo);
                //Lookup_BLL.GetLookUpValues(ref ddl_LineType, "LINETYPE");
                ddl_LineType.Items.Clear();
                ddl_LineType.Items.Add(new ListItem("---Select---", ""));
                if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                {
                    ddl_LineType.Items.Add(new ListItem("Item", "Item"));
                    ddl_LineType.Items.Add(new ListItem("Other Charges", "Other_Charges"));
                    ddl_LineType.Items.Add(new ListItem("Service", "Service"));
                    ddl_LineType.Items.Add(new ListItem("Miscellaneous", "Miscellaneous"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
                {
                    ddl_LineType.Items.Add(new ListItem("PO ITEM", "PO_ITEM"));
                    ddl_LineType.Items.Add(new ListItem("Other Charges", "Other_Charges"));
                    ddl_LineType.Items.Add(new ListItem("Service", "Service"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                    ddl_LineType.Items.Add(new ListItem("PO", "PO"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "CREDIT_MEMO")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                }
                else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "DEBIT_MEMO")
                {
                    ddl_LineType.Items.Add(new ListItem("Invoice", "Invoice"));
                }
                AccountCodes_BLL.fn_getAccount(ref ddlAccCode);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_LineType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.INV_LINE_TYPE].ToString();
                    ddlAccCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();
                    FillEntityNumber(tmpgvr);
                    ddl_ReceiptNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.RECEIPT_ID].ToString();
                    FillItemNo(tmpgvr);

                    dd_lItemNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();



                    BindGSegment(tmpgvr);
                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID1"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID1"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment1 = tmpgvr.FindControl("ddlGSegment1") as DropDownList;
                        ddl_GSegment1.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID1"].ToString();
                        LoadGSegmentValues(ddl_GSegment1);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID2"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID2"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment2 = tmpgvr.FindControl("ddlGSegment2") as DropDownList;
                        ddl_GSegment2.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID2"].ToString();
                        LoadGSegmentValues(ddl_GSegment2);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID3"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID3"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment3 = tmpgvr.FindControl("ddlGSegment3") as DropDownList;
                        ddl_GSegment3.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID3"].ToString();
                        LoadGSegmentValues(ddl_GSegment3);
                    }


                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID4"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID4"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment4 = tmpgvr.FindControl("ddlGSegment4") as DropDownList;
                        ddl_GSegment4.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID4"].ToString();
                        LoadGSegmentValues(ddl_GSegment4);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID5"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID5"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment5 = tmpgvr.FindControl("ddlGSegment5") as DropDownList;
                        ddl_GSegment5.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID5"].ToString();
                        LoadGSegmentValues(ddl_GSegment5);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID6"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID6"].ToString() != string.Empty)
                    {
                        DropDownList ddl_GSegment6 = tmpgvr.FindControl("ddlGSegment6") as DropDownList;
                        ddl_GSegment6.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["INV_SEGMENT_ID6"].ToString();
                        LoadGSegmentValues(ddl_GSegment6);
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);


                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    TempDataSave(drList, "A");
                    dtGridData.Rows.Add(drList);
                    LoadPrepaymentAmount(gvr, dtGridData);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    BindGrid(dtGridData);
                    DropDownList ddl_LineType = gvr.FindControl("ddlLineType") as DropDownList;
                    ddl_LineType.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private void TempDataSave(DataRow dr_List, string str_Mode)
        {
            if (Master.StrRecordId.ToString() == "0")
            {
                TMP_INV_INVOICES_HDR tMP_INV_INVOICES_HDR = new TMP_INV_INVOICES_HDR();

                if (hf_Temp_RefNo.Value.ToString().Length > 0)
                {
                    using (IRepository<TMP_INV_INVOICES_HDR> userCtx = new DataRepository<TMP_INV_INVOICES_HDR>())
                    {
                        tMP_INV_INVOICES_HDR = userCtx.Find(r =>
                            (r.INV_ID == hf_Temp_RefNo.Value.ToString())
                            ).SingleOrDefault();
                    }
                }

                tMP_INV_INVOICES_HDR.INV_NUM = txtInvoiceNumber.Text;
                if (txtInvoiceDate.Text != string.Empty)
                {
                    tMP_INV_INVOICES_HDR.INV_DATE = DBMethod.ConvertStringToDate(txtInvoiceDate.Text.ToString());
                }
                tMP_INV_INVOICES_HDR.VENDOR_ID = ddlSupplierNumber.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.INV_TYPE = (ddlInvoiceType.SelectedValue.ToString());
                tMP_INV_INVOICES_HDR.INV_PAYMENT_METHOD = ddlPaymentType.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.INV_CURR_CODE = ddlInvoiceCurrency.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.INV_PAY_CURR_CODE = ddlPaymentCurrency.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.INV_REMARKS = txtReason.Text;
                tMP_INV_INVOICES_HDR.INV_REJECT_REASON = txtRejectedReason.Text;
                tMP_INV_INVOICES_HDR.INV_AMT = CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text);
                tMP_INV_INVOICES_HDR.INV_TAX_AMT = CommonUtils.ConvertStringToDecimal(txtTax.Text);
                tMP_INV_INVOICES_HDR.TERM_ID = ddlTerm.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.INV_BATCH_ID = ddlBatch.SelectedValue.ToString();
                tMP_INV_INVOICES_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;
                tMP_INV_INVOICES_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                tMP_INV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtInvDueDate.Text.ToString().Trim().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.INV_DUE_DATE = DBMethod.ConvertStringToDate(txtInvDueDate.Text);
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_DUE_DATE = null;
                }
                if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;
                }
                else
                {
                    tMP_INV_INVOICES_HDR.VENDOR_LOC_ID = null;
                }

                if (ddlExchangeRateType.SelectedValue.ToString().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE = ddlExchangeRateType.SelectedValue;
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE = null;
                }
                if (txtExchangeRate.Text.ToString().Length > 0)
                {

                    tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text);
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE = null;
                }
                if (txtRetAmount.Text.ToString().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.INV_RETENTION_AMT = CommonUtils.ConvertStringToDecimal(txtRetAmount.Text); //decimal.Parse(txtRetAmount.Text);
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_RETENTION_AMT = null;
                }
                if (txtPrePaymentBal.Text.ToString().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.INV_PREPAY_BALANCE = CommonUtils.ConvertStringToDecimal(txtPrePaymentBal.Text);  //decimal.Parse(txtPrePaymentBal.Text);
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_PREPAY_BALANCE = null;
                }
                tMP_INV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS = "0";

                tMP_INV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (hf_Temp_RefNo.Value.ToString().Length > 0)
                {
                    tMP_INV_INVOICES_HDR.MODIFIED_BY = this.LoggedUserName;
                    tMP_INV_INVOICES_HDR.MODIFIED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<TMP_INV_INVOICES_HDR>(tMP_INV_INVOICES_HDR, true);
                }
                else
                {
                    tMP_INV_INVOICES_HDR.INV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_M.ToString(), false, true);
                    tMP_INV_INVOICES_HDR.CREATED_BY = this.LoggedUserName;
                    tMP_INV_INVOICES_HDR.CREATED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<TMP_INV_INVOICES_HDR>(tMP_INV_INVOICES_HDR);
                }
                hf_Temp_RefNo.Value = tMP_INV_INVOICES_HDR.INV_ID;


                TMP_INV_INVOICES_DTLS tMP_INV_INVOICES_DTLS = new TMP_INV_INVOICES_DTLS();

                if (str_Mode == "U")
                {
                    using (IRepository<TMP_INV_INVOICES_DTLS> userCtx = new DataRepository<TMP_INV_INVOICES_DTLS>())
                    {
                        tMP_INV_INVOICES_DTLS = userCtx.Find(r =>
                            (r.INV_ID == hf_Temp_RefNo.Value.ToString() && r.INV_LINE_NUM == CommonUtils.ConvertStringToInt(dr_List[FINColumnConstants.INV_LINE_NUM].ToString()))
                            ).SingleOrDefault();
                    }
                }

                tMP_INV_INVOICES_DTLS.INV_LINE_TYPE = dr_List["INV_LINE_TYPE"].ToString();
                tMP_INV_INVOICES_DTLS.INV_LINE_NUM = CommonUtils.ConvertStringToInt(dr_List[FINColumnConstants.INV_LINE_NUM].ToString());
                tMP_INV_INVOICES_DTLS.INV_ITEM_ID = (dr_List[FINColumnConstants.ITEM_ID].ToString());
                if (dr_List[FINColumnConstants.INV_ITEM_QTY_BILLED].ToString() != "0" && dr_List[FINColumnConstants.INV_ITEM_QTY_BILLED].ToString() != string.Empty)
                {
                    tMP_INV_INVOICES_DTLS.INV_ITEM_QTY_BILLED = CommonUtils.ConvertStringToDecimal(dr_List[FINColumnConstants.INV_ITEM_QTY_BILLED].ToString());
                }
                if (dr_List[FINColumnConstants.INV_ITEM_PRICE].ToString() != "0" && dr_List[FINColumnConstants.INV_ITEM_PRICE].ToString() != string.Empty)
                {
                    tMP_INV_INVOICES_DTLS.INV_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(dr_List[FINColumnConstants.INV_ITEM_PRICE].ToString());
                }


                tMP_INV_INVOICES_DTLS.INV_ITEM_AMT = CommonUtils.ConvertStringToDecimal(dr_List[FINColumnConstants.INV_ITEM_AMT].ToString());
                tMP_INV_INVOICES_DTLS.INV_ITEM_ACCT_CODE_ID = dr_List[FINColumnConstants.CODE_ID].ToString();

                tMP_INV_INVOICES_DTLS.ATTRIBUTE1 = dr_List["MIS_NO"].ToString();

                tMP_INV_INVOICES_DTLS.RECEIPT_ID = dr_List[FINColumnConstants.RECEIPT_ID].ToString();


                tMP_INV_INVOICES_DTLS.INV_LINE_TAX_AMT = CommonUtils.ConvertStringToDecimal(dr_List["INV_LINE_TAX_AMT"].ToString());
                tMP_INV_INVOICES_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                tMP_INV_INVOICES_DTLS.INV_ID = tMP_INV_INVOICES_HDR.INV_ID;

                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID1 = dr_List[FINColumnConstants.INV_SEGMENT_ID1].ToString();
                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID2 = dr_List[FINColumnConstants.INV_SEGMENT_ID2].ToString();
                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID3 = dr_List[FINColumnConstants.INV_SEGMENT_ID3].ToString();
                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID4 = dr_List[FINColumnConstants.INV_SEGMENT_ID4].ToString();
                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID5 = dr_List[FINColumnConstants.INV_SEGMENT_ID5].ToString();
                tMP_INV_INVOICES_DTLS.INV_SEGMENT_ID6 = dr_List[FINColumnConstants.INV_SEGMENT_ID6].ToString();

                tMP_INV_INVOICES_DTLS.WORKFLOW_COMPLETION_STATUS = "1";

                if (dr_List[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    DBMethod.DeleteEntity<TMP_INV_INVOICES_DTLS>(tMP_INV_INVOICES_DTLS);
                }
                else
                {
                    if (str_Mode == "U")
                    {
                        tMP_INV_INVOICES_DTLS.MODIFIED_DATE = DateTime.Today;
                        tMP_INV_INVOICES_DTLS.MODIFIED_BY = this.LoggedUserName;
                        DBMethod.SaveEntity<TMP_INV_INVOICES_DTLS>(tMP_INV_INVOICES_DTLS, true);

                    }
                    else
                    {
                        tMP_INV_INVOICES_DTLS.INV_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_D.ToString(), false, true);
                        tMP_INV_INVOICES_DTLS.CREATED_BY = this.LoggedUserName;
                        tMP_INV_INVOICES_DTLS.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<TMP_INV_INVOICES_DTLS>(tMP_INV_INVOICES_DTLS);
                    }
                }


            }
        }

        private void LoadPrepaymentAmount(GridViewRow tmpgvr, DataTable dt_Data)
        {

            DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
            DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
            TextBox txt_Amount = tmpgvr.FindControl("txtAmount") as TextBox;
            if (txtPrePaymentAmt.Text.ToString().Length == 0)
            {
                txtPrePaymentAmt.Text = "0";
            }
            if (ddl_LineType.SelectedValue.ToString().ToUpper() == "PO_ITEM")
            {
                if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
                {
                    DataTable dt_PoPerPay = DBMethod.ExecuteQuery(Invoice_DAL.getPrepaymentAmount4PO(ddl_ReceiptNo.SelectedValue)).Tables[0];
                    if (dt_PoPerPay.Rows.Count > 0)
                    {

                        if (Session["INV_PREPAYMENT"] == null)
                        {

                            DataTable dt_PrePayment = new DataTable();

                            dt_PrePayment.Columns.Add("PP_ADJ_ID");
                            dt_PrePayment.Columns.Add("DELETED");
                            dt_PrePayment.Columns.Add("PO_HEADER_ID");

                            dt_PrePayment.Columns.Add("PO_ID");
                            dt_PrePayment.Columns.Add("PO_NUM");
                            dt_PrePayment.Columns.Add("PO_PRE_PAY_AMT", typeof(decimal));
                            dt_PrePayment.Columns.Add("PO_USED_AMT", typeof(decimal));
                            dt_PrePayment.Columns.Add("PO_CUR_AMT", typeof(decimal));
                            dt_PrePayment.Columns.Add("PO_ADJ_AMT", typeof(decimal));
                            DataRow dr = dt_PrePayment.NewRow();
                            dr["PO_ID"] = ddl_ReceiptNo.SelectedValue;
                            dr["PO_NUM"] = ddl_ReceiptNo.SelectedItem.Text;
                            dr["PO_HEADER_ID"] = ddl_ReceiptNo.SelectedValue;
                            dr["PP_ADJ_ID"] = "0";
                            dr["DELETED"] = "0";
                            dr["PO_PRE_PAY_AMT"] = CommonUtils.ConvertStringToDecimal(dt_PoPerPay.Rows[0]["INV_ITEM_AMT"].ToString());
                            dr["PO_USED_AMT"] = CommonUtils.ConvertStringToDecimal("0");
                            dr["PO_CUR_AMT"] = CommonUtils.ConvertStringToDecimal(txt_Amount.Text);
                            dr["PO_ADJ_AMT"] = CommonUtils.ConvertStringToDecimal("0");
                            dt_PrePayment.Rows.Add(dr);
                            Session["INV_PREPAYMENT"] = dt_PrePayment;
                            txtPrePaymentAmt.Text = (FIN.BLL.CommonUtils.ConvertStringToDecimal(txtPrePaymentAmt.Text) + FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_PoPerPay.Rows[0]["INV_ITEM_AMT"].ToString())).ToString();
                        }
                        else
                        {
                            DataTable dt_PrePayment = (DataTable)Session["INV_PREPAYMENT"];

                            DataRow[] dr_po = dt_PrePayment.Select("PO_ID='" + ddl_ReceiptNo.SelectedValue + "'");

                            if (dr_po.Length > 0)
                            {
                                decimal dec_amt = 0;
                                for (int kLoop = 0; kLoop < dt_Data.Rows.Count; kLoop++)
                                {
                                    if (dt_Data.Rows[kLoop]["RECEIPT_ID"].ToString() == ddl_ReceiptNo.SelectedValue.ToString())
                                    {
                                        dec_amt += CommonUtils.ConvertStringToDecimal(dt_Data.Rows[kLoop]["INV_ITEM_AMT"].ToString());
                                    }
                                }
                                dr_po[0]["PO_CUR_AMT"] = dec_amt;
                                dt_PrePayment.AcceptChanges();

                            }
                            else
                            {
                                DataRow dr = dt_PrePayment.NewRow();
                                dr["PO_ID"] = ddl_ReceiptNo.SelectedValue;
                                dr["PO_NUM"] = ddl_ReceiptNo.SelectedItem.Text;
                                dr["PO_PRE_PAY_AMT"] = CommonUtils.ConvertStringToDecimal(dt_PoPerPay.Rows[0]["INV_ITEM_AMT"].ToString());
                                dr["PO_USED_AMT"] = CommonUtils.ConvertStringToDecimal("0");
                                dr["PO_CUR_AMT"] = CommonUtils.ConvertStringToDecimal(txt_Amount.Text);
                                dr["PO_ADJ_AMT"] = CommonUtils.ConvertStringToDecimal("0");
                                dt_PrePayment.Rows.Add(dr);
                                txtPrePaymentAmt.Text = (FIN.BLL.CommonUtils.ConvertStringToDecimal(txtPrePaymentAmt.Text) + FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_PoPerPay.Rows[0]["INV_ITEM_AMT"].ToString())).ToString();

                            }
                            Session["INV_PREPAYMENT"] = dt_PrePayment;

                        }

                        gv_Po_PerPay.DataSource = (DataTable)Session["INV_PREPAYMENT"];
                        gv_Po_PerPay.DataBind();

                    }
                }
            }
        }
        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            // TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            DropDownList ddl_ReceiptNo = gvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = gvr.FindControl("ddlItemNo") as DropDownList;
            TextBox txt_Quantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txt_UnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            DropDownList ddl_LineType = gvr.FindControl("ddlLineType") as DropDownList;
            TextBox txtMisNo = gvr.FindControl("txtMisNo") as TextBox;
            DropDownList ddl_AccCode = gvr.FindControl("ddlAccCode") as DropDownList;



            DropDownList ddl_GSegment1 = gvr.FindControl("ddlGSegment1") as DropDownList;
            DropDownList ddl_GSegment2 = gvr.FindControl("ddlGSegment2") as DropDownList;
            DropDownList ddl_GSegment3 = gvr.FindControl("ddlGSegment3") as DropDownList;
            DropDownList ddl_GSegment4 = gvr.FindControl("ddlGSegment4") as DropDownList;
            DropDownList ddl_GSegment5 = gvr.FindControl("ddlGSegment5") as DropDownList;
            DropDownList ddl_GSegment6 = gvr.FindControl("ddlGSegment6") as DropDownList;


            ErrorCollection.Clear();
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.INV_LINE_ID] = "0";
                //  txtLineNo.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }



            if (ddl_LineType.SelectedValue == FINAppConstants.intialRowValueField && ddl_LineType.SelectedItem.Text == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ddlLineType", "Line Type cannot be empty");
                return drList;
            }
            else if (ddl_LineType.SelectedItem.Text == "Miscellaneous")
            {
                slControls[0] = txtMisNo;
                //slControls[1] = txt_Quantity;                
                slControls[1] = txt_UnitPrice;
                slControls[2] = ddl_AccCode;
                slControls[3] = txt_Amount;
                slControls[4] = ddl_GSegment1;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                //   string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST;
                //string strMessage = Prop_File_Data["Entity_Number_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
                string strMessage = Prop_File_Data["Entity_Number_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + "~" + " Segment1 ";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            }
            else
            {
                // slControls[0] = txtLineNo;
                slControls[0] = ddl_ReceiptNo;
                slControls[1] = ddl_ItemNo;
                slControls[2] = txt_Quantity;
                slControls[3] = txt_UnitPrice;
                slControls[4] = ddl_AccCode;
                slControls[5] = txt_Amount;
                slControls[6] = ddl_LineType;
                slControls[7] = ddl_GSegment1;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Entity_Number_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + " ~ " + Prop_File_Data["Line_Type_P"] + "~" + " Segment1 ";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            }


            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "GRN_NUM='" + ddl_ReceiptNo.SelectedValue.ToString() + "'";
            //string strMessage1 = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage1);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}

            ErrorCollection.Remove("unbilled quan");

            if (Session["unbilled_quantity"] != null)
            {
                if (CommonUtils.ConvertStringToDecimal(txt_Quantity.Text) > CommonUtils.ConvertStringToDecimal(Session["unbilled_quantity"].ToString()))
                {
                    ErrorCollection.Add("unbilled quan", "Quantity should not be greater than the Total Quantity");
                    return drList;
                }
            }


            if (decimal.Parse(txt_Amount.Text) == 0)
            {
                ErrorCollection.Add("invldAmt", "You have entered invalid amount.");
                return drList;
            }


            // drList[FINColumnConstants.INV_LINE_NUM] = txtLineNo.Text;
            drList["INV_LINE_TYPE"] = ddl_LineType.SelectedValue.ToString();
            drList["INV_LINE_TYPE_DESC"] = ddl_LineType.SelectedItem.Text;


            if (ddl_ReceiptNo.SelectedValue != null && ddl_ReceiptNo.SelectedItem != null)
            {
                if (ddl_ReceiptNo.SelectedValue.ToString() != FINAppConstants.intialRowValueField && ddl_ReceiptNo.SelectedItem.Text.ToString() != FINAppConstants.intialRowTextField)
                {
                    drList[FINColumnConstants.RECEIPT_ID] = ddl_ReceiptNo.SelectedValue.ToString();
                    drList[FINColumnConstants.GRN_NUM] = ddl_ReceiptNo.SelectedValue == null ? string.Empty : ddl_ReceiptNo.SelectedItem.Text;
                }
            }


            if (ddl_ItemNo.SelectedValue != null && ddl_ItemNo.SelectedItem != null)
            {
                if (ddl_ItemNo.SelectedValue.ToString() != FINAppConstants.intialRowValueField && ddl_ItemNo.SelectedItem.Text.ToString() != FINAppConstants.intialRowTextField)
                {
                    drList[FINColumnConstants.ITEM_ID] = ddl_ItemNo.SelectedValue == null ? string.Empty : ddl_ItemNo.SelectedValue;
                    drList[FINColumnConstants.ITEM_NAME] = ddl_ItemNo.SelectedItem.Text;
                }
            }

            if (txt_Quantity.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.INV_ITEM_QTY_BILLED] = txt_Quantity.Text;
            }
            else
            {
                drList[FINColumnConstants.INV_ITEM_QTY_BILLED] = null;
            }
            if (txt_UnitPrice.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.INV_ITEM_PRICE] = txt_UnitPrice.Text;
            }
            else
            {
                drList[FINColumnConstants.INV_ITEM_PRICE] = null;
            }


            drList[FINColumnConstants.INV_ITEM_AMT] = txt_Amount.Text;

            drList[FINColumnConstants.CODE_ID] = ddl_AccCode.SelectedValue.ToString();
            drList[FINColumnConstants.CODE_NAME] = ddl_AccCode.SelectedItem.Text;
            drList["MIS_NO"] = txtMisNo.Text;


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;
            if (hf_TotTaxAmt.Value.ToString().Length > 0)
            {
                drList["INV_LINE_TAX_AMT"] = hf_TotTaxAmt.Value.ToString();
            }

            //if (ddlSegment1.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID1] = ddlSegment1.SelectedValue.ToString();
            //if (ddlSegment2.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID2] = ddlSegment2.SelectedValue.ToString();
            //if (ddlSegment3.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID3] = ddlSegment3.SelectedValue.ToString();
            //if (ddlSegment4.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID4] = ddlSegment4.SelectedValue.ToString();
            //if (ddlSegment5.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID5] = ddlSegment5.SelectedValue.ToString();
            //if (ddlSegment6.SelectedValue.ToString().Length > 0)
            //    drList[FINColumnConstants.INV_SEGMENT_ID6] = ddlSegment6.SelectedValue.ToString();

            //ddlSegment1.SelectedIndex = 0;
            //ddlSegment2.SelectedIndex = 0;
            //ddlSegment3.SelectedIndex = 0;
            //ddlSegment4.SelectedIndex = 0;
            //ddlSegment5.SelectedIndex = 0;
            //ddlSegment6.SelectedIndex = 0;


            if (ddl_GSegment1.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID1"] = ddl_GSegment1.SelectedValue.ToString();
                drList["SEGMENT_1_TEXT"] = ddl_GSegment1.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID1"] = null;
                drList["SEGMENT_1_TEXT"] = null;
            }

            if (ddl_GSegment2.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID2"] = ddl_GSegment2.SelectedValue.ToString();
                drList["SEGMENT_2_TEXT"] = ddl_GSegment2.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID2"] = null;
                drList["SEGMENT_2_TEXT"] = null;
            }
            if (ddl_GSegment3.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID3"] = ddl_GSegment3.SelectedValue.ToString();
                drList["SEGMENT_3_TEXT"] = ddl_GSegment3.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID3"] = null;
                drList["SEGMENT_3_TEXT"] = null;
            }
            if (ddl_GSegment4.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID4"] = ddl_GSegment4.SelectedValue.ToString();
                drList["SEGMENT_4_TEXT"] = ddl_GSegment4.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID4"] = null;
                drList["SEGMENT_4_TEXT"] = null;
            }
            if (ddl_GSegment5.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID5"] = ddl_GSegment5.SelectedValue.ToString();
                drList["SEGMENT_5_TEXT"] = ddl_GSegment5.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID5"] = null;
                drList["SEGMENT_5_TEXT"] = null;
            }
            if (ddl_GSegment6.SelectedValue.ToString().Length > 0)
            {
                drList["INV_SEGMENT_ID6"] = ddl_GSegment6.SelectedValue.ToString();
                drList["SEGMENT_6_TEXT"] = ddl_GSegment6.SelectedItem.Text.ToString();
            }
            else
            {
                drList["INV_SEGMENT_ID6"] = null;
                drList["SEGMENT_6_TEXT"] = null;
            }



            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);


                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }

                LoadPrepaymentAmount(gvr, dtGridData);
                TempDataSave(drList, "U");

                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);

                DropDownList ddl_LineType = gvr.FindControl("ddlLineType") as DropDownList;
                ddl_LineType.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                    DataTable dtData1 = new DataTable();

                    Label lblUnbilledQuantity = e.Row.FindControl("lblUnbilledQuantity") as Label;
                    DropDownList ddlItemNo = e.Row.FindControl("ddlItemNo") as DropDownList;
                    DropDownList ddlReceiptNo = e.Row.FindControl("ddlReceiptNo") as DropDownList;
                    TextBox txtQuantity = e.Row.FindControl("txtQuantity") as TextBox;



                    if (lblUnbilledQuantity != null && ddlItemNo != null && ddlReceiptNo != null && txtQuantity != null)
                    {
                        lblUnbilledQuantity.Text = string.Empty;

                        dtData1 = DBMethod.ExecuteQuery(Invoice_DAL.GetQuantityDetails(ddlItemNo.SelectedValue, ddlSupplierNumber.SelectedValue, ddlInvoiceType.SelectedValue, ddlReceiptNo.SelectedValue)).Tables[0];
                        if (dtData1.Rows.Count > 0)
                        {
                            lblUnbilledQuantity.Text = (dtData1.Rows[0]["inv_item_qty_billed"].ToString());

                            if (CommonUtils.ConvertStringToDecimal(lblUnbilledQuantity.Text) > 0)
                            {
                                lblUnbilledQuantity.Text = "Invoiced Quantity-" + lblUnbilledQuantity.Text;
                                lblUnbilledQuantity.Visible = true;
                            }
                            else
                            {
                                lblUnbilledQuantity.Visible = false;
                            }
                        }
                        else
                        {
                            lblUnbilledQuantity.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion


        private void PaymentCurrecnyChecking()
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            if ((ddlPaymentCurrency.SelectedValue == ddlInvoiceCurrency.SelectedValue) || (ddlPaymentCurrency.SelectedValue == Session[FINSessionConstants.ORGCurrency].ToString()))
            {
            }
            else
            {
                ErrorCollection.Add("InvalidPayCurrecny", Prop_File_Data["Invalid_Payment_P"]);
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Master.Mode == FINAppConstants.WF)
                {
                    return;
                }
                DataTable dtAcctfrmSupplier = new DataTable();
                DataTable dtAcctfrmSysopt = new DataTable();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtInvoiceNumber;
                slControls[1] = ddlSupplierNumber;
                slControls[2] = ddlGlobalSegment;
                slControls[3] = ddlExchangeRateType;

                ErrorCollection.Clear();
                string strCtrlTypes = "TextBox~DropDownList~DropDownList~DropDownList";

                string strMessage = "Invoice Number ~ Supplier Number ~ Global Segment ~ Exchange Rate Type";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                PaymentCurrecnyChecking();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Remove("exerror");

                if (txtExchangeRate.Text == "0")
                {
                    ErrorCollection.Add("exerror", "Exchange Rate  cannot be zero");
                    return;
                }
                ErrorCollection.Clear();

                ErrorCollection.Clear();

                AssignToBE();
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_INVOICES_HDR.INV_ID, iNV_INVOICES_HDR.INV_NUM);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("INVOICE", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                txtTax.Text = "0";
                ErrorCollection.Clear();


                if (EntityData != null)
                {
                    iNV_INVOICES_HDR = (INV_INVOICES_HDR)EntityData;
                }

                iNV_INVOICES_HDR.INV_NUM = txtInvoiceNumber.Text;

                if (txtInvoiceDate.Text != string.Empty)
                {
                    iNV_INVOICES_HDR.INV_DATE = DBMethod.ConvertStringToDate(txtInvoiceDate.Text.ToString());
                }
                iNV_INVOICES_HDR.VENDOR_ID = ddlSupplierNumber.SelectedValue.ToString();
                iNV_INVOICES_HDR.INV_TYPE = (ddlInvoiceType.SelectedValue.ToString());
                iNV_INVOICES_HDR.INV_PAYMENT_METHOD = ddlPaymentType.SelectedValue.ToString();
                iNV_INVOICES_HDR.INV_CURR_CODE = ddlInvoiceCurrency.SelectedValue.ToString();
                iNV_INVOICES_HDR.INV_PAY_CURR_CODE = ddlPaymentCurrency.SelectedValue.ToString();
                iNV_INVOICES_HDR.INV_REMARKS = txtReason.Text;
                iNV_INVOICES_HDR.INV_REJECT_REASON = txtRejectedReason.Text;
                iNV_INVOICES_HDR.INV_AMT = CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text);
                iNV_INVOICES_HDR.INV_TAX_AMT = CommonUtils.ConvertStringToDecimal(txtTax.Text);
                iNV_INVOICES_HDR.TERM_ID = ddlTerm.SelectedValue.ToString();
                iNV_INVOICES_HDR.INV_BATCH_ID = ddlBatch.SelectedValue.ToString();
                iNV_INVOICES_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;


                iNV_INVOICES_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                // iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                iNV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (txtInvDueDate.Text.ToString().Trim().Length > 0)
                {
                    iNV_INVOICES_HDR.INV_DUE_DATE = DBMethod.ConvertStringToDate(txtInvDueDate.Text);
                }
                else
                {
                    iNV_INVOICES_HDR.INV_DUE_DATE = null;
                }
                if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                {
                    iNV_INVOICES_HDR.VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;
                }
                else
                {
                    iNV_INVOICES_HDR.VENDOR_LOC_ID = null;
                }

                if (ddlExchangeRateType.SelectedValue.ToString().Length > 0)
                {
                    iNV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE = ddlExchangeRateType.SelectedValue;
                }
                else
                {
                    iNV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE = null;
                }
                if (txtExchangeRate.Text.ToString().Length > 0)
                {

                    iNV_INVOICES_HDR.INV_EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text);
                }
                else
                {
                    iNV_INVOICES_HDR.INV_EXCHANGE_RATE = null;
                }
                if (txtRetAmount.Text.ToString().Length > 0)
                {
                    iNV_INVOICES_HDR.INV_RETENTION_AMT = CommonUtils.ConvertStringToDecimal(txtRetAmount.Text); //decimal.Parse(txtRetAmount.Text);
                }
                else
                {
                    iNV_INVOICES_HDR.INV_RETENTION_AMT = null;
                }
                if (txtPrePaymentBal.Text.ToString().Length > 0)
                {
                    iNV_INVOICES_HDR.INV_PREPAY_BALANCE = CommonUtils.ConvertStringToDecimal(txtPrePaymentBal.Text);  //decimal.Parse(txtPrePaymentBal.Text);
                }
                else
                {
                    iNV_INVOICES_HDR.INV_PREPAY_BALANCE = null;
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_INVOICES_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_INVOICES_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNV_INVOICES_HDR.INV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_M.ToString(), false, true);
                    iNV_INVOICES_HDR.POSTED_FLAG = FINAppConstants.N;
                    //iNV_INVOICES_HDR.INV_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_LN.ToString(), false, true);
                    iNV_INVOICES_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_INVOICES_HDR.CREATED_DATE = DateTime.Today;
                }
                iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS = "0";

                iNV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                // Duplicate Validation Through Backend Package PKG_VALIDATIONS
                //string ProReturn = FIN.DAL.HR.Employee_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_INVOICES_HDR.INV_NUM, iNV_INVOICES_HDR.INV_ID);

                // if (ProReturn != string.Empty)
                // {
                //     if (ProReturn != "0")
                //     {
                //         ErrorCollection.Add("InvoiceNumDuplication", ProReturn);
                //         if (ErrorCollection.Count > 0)
                //         {
                //             return;
                //         }
                //     }
                // }

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Invoice ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                INV_INVOICES_DTLS iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != string.Empty)
                    {
                        iNV_INVOICES_DTLS = Invoice_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString());
                    }
                    if (iNV_INVOICES_DTLS == null)
                    {
                        iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();
                    }
                    iNV_INVOICES_DTLS.INV_LINE_TYPE = dtGridData.Rows[iLoop]["INV_LINE_TYPE"].ToString();
                    iNV_INVOICES_DTLS.INV_LINE_NUM = iLoop;// CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_NUM].ToString());
                    iNV_INVOICES_DTLS.INV_ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    if (dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_QTY_BILLED].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_QTY_BILLED].ToString() != string.Empty)
                    {
                        iNV_INVOICES_DTLS.INV_ITEM_QTY_BILLED = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_QTY_BILLED].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_PRICE].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_PRICE].ToString() != string.Empty)
                    {
                        iNV_INVOICES_DTLS.INV_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_PRICE].ToString());
                    }


                    iNV_INVOICES_DTLS.INV_ITEM_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_AMT].ToString());
                    iNV_INVOICES_DTLS.INV_ITEM_ACCT_CODE_ID = dtGridData.Rows[iLoop][FINColumnConstants.CODE_ID].ToString();

                    iNV_INVOICES_DTLS.ATTRIBUTE1 = dtGridData.Rows[iLoop]["MIS_NO"].ToString();

                    iNV_INVOICES_DTLS.RECEIPT_ID = dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_ID].ToString();

                    iNV_INVOICES_HDR.INV_ACCT_CODE_ID = dtGridData.Rows[iLoop][FINColumnConstants.CODE_ID].ToString();
                    iNV_INVOICES_DTLS.INV_LINE_TAX_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INV_LINE_TAX_AMT"].ToString());
                    // iNV_INVOICES_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    iNV_INVOICES_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    iNV_INVOICES_DTLS.INV_ID = iNV_INVOICES_HDR.INV_ID;

                    iNV_INVOICES_DTLS.INV_SEGMENT_ID1 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID1].ToString();
                    iNV_INVOICES_DTLS.INV_SEGMENT_ID2 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID2].ToString();
                    iNV_INVOICES_DTLS.INV_SEGMENT_ID3 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID3].ToString();
                    iNV_INVOICES_DTLS.INV_SEGMENT_ID4 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID4].ToString();
                    iNV_INVOICES_DTLS.INV_SEGMENT_ID5 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID5].ToString();
                    iNV_INVOICES_DTLS.INV_SEGMENT_ID6 = dtGridData.Rows[iLoop][FINColumnConstants.INV_SEGMENT_ID6].ToString();



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != string.Empty)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_INVOICES_DTLS, FINAppConstants.Delete));
                        }
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_LINE_ID].ToString() != string.Empty)
                        {
                            iNV_INVOICES_DTLS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_INVOICES_DTLS, FINAppConstants.Update));
                        }
                        else
                        {
                            iNV_INVOICES_DTLS.INV_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_D.ToString(), false, true);
                            iNV_INVOICES_DTLS.CREATED_BY = this.LoggedUserName;
                            iNV_INVOICES_DTLS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_INVOICES_DTLS, FINAppConstants.Add));
                        }
                    }
                    iNV_INVOICES_DTLS.WORKFLOW_COMPLETION_STATUS = "1";

                }
                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(iNV_INVOICES_HDR.INV_ID, txtInvoiceDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<INV_INVOICES_HDR, INV_INVOICES_DTLS>(iNV_INVOICES_HDR, tmpChildEntity, iNV_INVOICES_DTLS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<INV_INVOICES_HDR, INV_INVOICES_DTLS>(iNV_INVOICES_HDR, tmpChildEntity, iNV_INVOICES_DTLS, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_INVOICES_HDR.INV_ID);
                DBMethod.SaveEntity<INV_INVOICES_HDR>(iNV_INVOICES_HDR, true);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (hf_Temp_RefNo.Value.ToString().Length > 0)
                {
                    DBMethod.ExecuteNonQuery("DELETE TMP_INV_INVOICES_DTLS WHERE INV_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                    DBMethod.ExecuteNonQuery("DELETE TMP_INV_INVOICES_HDR WHERE INV_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                }
                if (Session["InvTaxDet"] != null)
                {
                    DataTable dt_invTax = (DataTable)Session["InvTaxDet"];
                    if (dt_invTax.Rows.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < dt_invTax.Rows.Count; iLoop++)
                        {
                            INV_TAX_DTLS iNV_TAX_DTLS = new INV_TAX_DTLS();
                            decimal dbl_TaxAmount = 0;
                            if (dt_invTax.Rows[iLoop]["INV_TAX_AMT"] != null)
                            {
                                if (dt_invTax.Rows[iLoop]["INV_TAX_AMT"].ToString().Length > 0)
                                {
                                    dbl_TaxAmount = CommonUtils.ConvertStringToDecimal(dt_invTax.Rows[iLoop]["INV_TAX_AMT"].ToString());
                                }
                                else
                                {
                                    dbl_TaxAmount = 0;
                                }
                            }
                            else
                            {
                                dbl_TaxAmount = 0;
                            }

                            if (dt_invTax.Rows[iLoop]["INV_TAX_ID"].ToString() != "0")
                            {

                                using (IRepository<INV_TAX_DTLS> userCtx = new DataRepository<INV_TAX_DTLS>())
                                {
                                    iNV_TAX_DTLS = userCtx.Find(r =>
                                        (r.INV_TAX_ID == dt_invTax.Rows[iLoop]["INV_TAX_ID"].ToString())
                                        ).SingleOrDefault();
                                }
                                if (dbl_TaxAmount > 0)
                                {
                                    DBMethod.DeleteEntity<INV_TAX_DTLS>(iNV_TAX_DTLS);
                                }
                                else
                                {
                                    iNV_TAX_DTLS.MODIFIED_BY = this.LoggedUserName;
                                    iNV_TAX_DTLS.MODIFIED_DATE = DateTime.Now;
                                    iNV_TAX_DTLS.INV_TAX_AMT = CommonUtils.ConvertStringToDecimal(dbl_TaxAmount.ToString());
                                    DBMethod.SaveEntity<INV_TAX_DTLS>(iNV_TAX_DTLS, true);
                                }
                            }
                            else
                            {
                                INV_INVOICES_DTLS tmp_iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();
                                using (IRepository<INV_INVOICES_DTLS> userCtx = new DataRepository<INV_INVOICES_DTLS>())
                                {
                                    tmp_iNV_INVOICES_DTLS = userCtx.Find(r =>
                                        (r.INV_ID == iNV_INVOICES_HDR.INV_ID && r.RECEIPT_ID == dt_invTax.Rows[iLoop]["EntityNo"].ToString() && r.INV_ITEM_ID == dt_invTax.Rows[iLoop]["ItemNo"].ToString())
                                        ).SingleOrDefault();
                                }

                                if (dbl_TaxAmount > 0)
                                {
                                    iNV_TAX_DTLS.INV_ID = iNV_INVOICES_HDR.INV_ID;
                                    iNV_TAX_DTLS.INV_LINE_ID = tmp_iNV_INVOICES_DTLS.INV_LINE_ID;
                                    iNV_TAX_DTLS.CREATED_BY = this.LoggedUserName;
                                    iNV_TAX_DTLS.CREATED_DATE = DateTime.Now;
                                    iNV_TAX_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                                    iNV_TAX_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                                    iNV_TAX_DTLS.TAX_ID = gvTaxDet.DataKeys[iLoop].Values["TAX_ID"].ToString();
                                    iNV_TAX_DTLS.INV_TAX_AMT = CommonUtils.ConvertStringToDecimal(dbl_TaxAmount.ToString());
                                    iNV_TAX_DTLS.INV_TAX_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_T.ToString(), false, true);
                                    DBMethod.SaveEntity<INV_TAX_DTLS>(iNV_TAX_DTLS);
                                }
                            }
                        }

                    }
                }

                if (gv_Po_PerPay.Rows.Count > 0)
                {

                    for (int mLoop = 0; mLoop <= gv_Po_PerPay.Rows.Count; mLoop++)
                    {
                        TextBox txt_CurrentAmto = (TextBox)gv_Po_PerPay.Rows[mLoop].FindControl("txtCurrentAmto");
                        INV_PREPAYMENT_ADJ_DTLS iNV_PREPAYMENT_ADJ_DTLS = new INV_PREPAYMENT_ADJ_DTLS();
                        if (gv_Po_PerPay.DataKeys[mLoop].Values["PP_ADJ_ID"].ToString() != "0")
                        {

                            using (IRepository<INV_PREPAYMENT_ADJ_DTLS> userCtx = new DataRepository<INV_PREPAYMENT_ADJ_DTLS>())
                            {
                                iNV_PREPAYMENT_ADJ_DTLS = userCtx.Find(r =>
                                    (r.PP_ADJ_ID == gv_Po_PerPay.DataKeys[mLoop].Values["PP_ADJ_ID"].ToString())
                                    ).SingleOrDefault();
                            }

                        }
                        if (iNV_PREPAYMENT_ADJ_DTLS == null)
                        {
                            iNV_PREPAYMENT_ADJ_DTLS = new INV_PREPAYMENT_ADJ_DTLS();
                        }

                        iNV_PREPAYMENT_ADJ_DTLS.INV_ID = iNV_INVOICES_HDR.INV_ID;
                        iNV_PREPAYMENT_ADJ_DTLS.PO_HEADER_ID = gv_Po_PerPay.DataKeys[mLoop].Values["PO_HEADER_ID"].ToString();
                        iNV_PREPAYMENT_ADJ_DTLS.PO_PRE_PAY_AMT = CommonUtils.ConvertStringToDecimal(gv_Po_PerPay.DataKeys[mLoop].Values["PO_PRE_PAY_AMT"].ToString());
                        iNV_PREPAYMENT_ADJ_DTLS.PO_USER_AMT = CommonUtils.ConvertStringToDecimal(gv_Po_PerPay.DataKeys[mLoop].Values["PO_USED_AMT"].ToString());
                        iNV_PREPAYMENT_ADJ_DTLS.PO_CUR_AMT = CommonUtils.ConvertStringToDecimal(gv_Po_PerPay.DataKeys[mLoop].Values["PO_CUR_AMT"].ToString());
                        if (txt_CurrentAmto.Text.ToString().Trim().Length > 0)
                        {
                            iNV_PREPAYMENT_ADJ_DTLS.PO_ADJ_AMT = CommonUtils.ConvertStringToDecimal(txt_CurrentAmto.Text.ToString());
                        }
                        else
                        {
                            iNV_PREPAYMENT_ADJ_DTLS.PO_ADJ_AMT = 0;
                        }
                        iNV_PREPAYMENT_ADJ_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                        iNV_PREPAYMENT_ADJ_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                        if (gv_Po_PerPay.DataKeys[mLoop].Values["PP_ADJ_ID"].ToString() != "0")
                        {

                            iNV_PREPAYMENT_ADJ_DTLS.MODIFIED_BY = this.LoggedUserName;
                            iNV_PREPAYMENT_ADJ_DTLS.MODIFIED_DATE = DateTime.Now;

                            DBMethod.SaveEntity<INV_PREPAYMENT_ADJ_DTLS>(iNV_PREPAYMENT_ADJ_DTLS, true);

                        }
                        else
                        {
                            iNV_PREPAYMENT_ADJ_DTLS.PP_ADJ_ID = FINSP.GetSPFOR_SEQCode("AP_021_P".ToString(), false, true);
                            iNV_PREPAYMENT_ADJ_DTLS.CREATED_BY = this.LoggedUserName;
                            iNV_PREPAYMENT_ADJ_DTLS.CREATED_DATE = DateTime.Now;

                            DBMethod.SaveEntity<INV_PREPAYMENT_ADJ_DTLS>(iNV_PREPAYMENT_ADJ_DTLS);
                        }
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    //FINSP.GetSP_GL_Posting(iNV_INVOICES_HDR.INV_ID, "AP_021");

                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.INVOICE_ALERT);
                    if (VMVServices.Web.Utils.IsAlert == "1")
                    {
                        FINSQL.UpdateAlertUserLevel();
                    }

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnSegments_Click(object sender, EventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_AccCode = (DropDownList)gvr.FindControl("ddlAccCode");
            if (ddl_AccCode.SelectedValue.ToString().Trim().Length > 0)
            {
                //FIN.BLL.GL.AccountCodes_BLL.getSegmentValues(ref ddlSegment1, ref ddlSegment2, ref ddlSegment3, ref ddlSegment4, ref ddlSegment5, ref ddlSegment6, ddl_AccCode.SelectedValue.ToString());
                //if (gvData.EditIndex > 0)
                //{
                //    ddlSegment1.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID1].ToString();
                //    ddlSegment2.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID2].ToString();
                //    ddlSegment3.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID3].ToString();
                //    ddlSegment4.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID4].ToString();
                //    ddlSegment5.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID5].ToString();
                //    ddlSegment6.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values[FINColumnConstants.INV_SEGMENT_ID6].ToString();
                //}



                BindSegmentValues(gvr);
                ModalPopupExtender2.Show();
            }
            else
            {

                ErrorCollection.Add("SelectAccCode", "Please Select Account Code ");
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            }

            // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            //ImageButton btndetails = sender as ImageButton;
            //GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            //lblID.Text = gvData.DataKeys[gvrow.RowIndex].Value.ToString();



        }


        private void BindSegmentValues(GridViewRow gvr)
        {

            BindSegment(gvr);
            if (gvr.RowType != DataControlRowType.Footer)
            {
                int iLoop = gvr.RowIndex;
                string seg1 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_1].ToString();
                string seg2 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_2].ToString();
                string seg3 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_3].ToString();
                string seg4 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_4].ToString();
                string seg5 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_5].ToString();
                string seg6 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_6].ToString();

                ddlSegment1.SelectedValue = seg1;
                ddlSegment2.SelectedValue = seg2;
                ddlSegment3.SelectedValue = seg3;
                ddlSegment4.SelectedValue = seg4;
                ddlSegment5.SelectedValue = seg5;
                ddlSegment6.SelectedValue = seg6;
            }

        }


        private void BindSegment(GridViewRow gvr)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                DropDownList ddlAccountCodes = new DropDownList();



                ddlAccountCodes = (DropDownList)gvr.FindControl("ddlAccCode");

                if (ddlAccountCodes != null)
                {
                    if (ddlAccountCodes.SelectedValue != null)
                    {
                        dtdataAc = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentBasedAccCode(ddlAccountCodes.SelectedValue.ToString())).Tables[0];
                        if (dtdataAc != null)
                        {
                            if (dtdataAc.Rows.Count > 0)
                            {
                                int j = dtdataAc.Rows.Count;

                                for (int i = 0; i < dtdataAc.Rows.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        //segment1
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }

                                    }
                                    if (i == 1)
                                    {
                                        //segment2
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 2)
                                    {
                                        //Segments3
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 3)
                                    {
                                        //segment4
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 4)
                                    {
                                        //segment5
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 5)
                                    {
                                        //segment6
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                }
                                if (j == 1)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 2)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 3)
                                {
                                    // Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, VMVServices.Web.Utils.OrganizationID);

                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 4)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 5)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
                //throw ex;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<iNV_INVOICES_HDR>(iNV_INVOICES_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion
        # region events
        protected void ddlItemNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_ReceiptNo = gvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = gvr.FindControl("ddlItemNo") as DropDownList;
            DropDownList ddlAccCode = gvr.FindControl("ddlAccCode") as DropDownList;

            TextBox txtItemName = gvr.FindControl("txtItemName") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;

            DataTable dtData = new DataTable();
            DataTable dtAcctcode = new DataTable();

            DropDownList ddl_LineType = gvr.FindControl("ddlLineType") as DropDownList;

            if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
            {
                if (ddl_LineType.SelectedValue.ToString().ToUpper() == "PO_ITEM")
                {
                    dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.GetPODetails4ItemId(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];

                    dtAcctcode = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.GetAcctCode(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                    if (dtAcctcode != null)
                    {
                        if (dtAcctcode.Rows.Count > 0)
                        {
                            ddlAccCode.SelectedValue = dtAcctcode.Rows[0]["INV_MAT_ACCT_CODE"].ToString();
                        }
                    }
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            txtItemName.Visible = true;
                            txtItemName.Text = dtData.Rows[0][FINColumnConstants.ITEM_NAME].ToString();
                            txtQuantity.Text = dtData.Rows[0][FINColumnConstants.PO_QUANTITY].ToString();
                            txtUnitPrice.Text = dtData.Rows[0][FINColumnConstants.PO_UNIT_PRICE].ToString();
                            txtAmount.Text = dtData.Rows[0]["PO_line_Amount"].ToString();
                            Session["Item_Id"] = dtData.Rows[0][FINColumnConstants.ITEM_ID].ToString();
                        }
                    }
                }
            }
            else
            {
                // dtData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLineDetails(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                dtData = FIN.BLL.AP.PurchaseItemReceipt_BLL.fn_getGRNItemRecDet(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString());
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        dtAcctcode = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.GetAcctCode(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                        if (dtAcctcode != null)
                        {
                            if (dtAcctcode.Rows.Count > 0)
                            {
                                ddlAccCode.SelectedValue = dtAcctcode.Rows[0]["INV_MAT_ACCT_CODE"].ToString();
                            }
                        }

                        txtItemName.Visible = true;
                        txtItemName.Text = dtData.Rows[0][FINColumnConstants.item_service].ToString();
                        txtQuantity.Text = dtData.Rows[0][FINColumnConstants.QTY_RECEIVED].ToString();
                        txtUnitPrice.Text = dtData.Rows[0][FINColumnConstants.PO_UNIT_PRICE].ToString();
                        txtAmount.Text = dtData.Rows[0][FINColumnConstants.PO_Amount].ToString();
                        Session["Item_Id"] = dtData.Rows[0][FINColumnConstants.ITEM_ID].ToString();
                    }
                }
            }

            DataTable dtData1 = new DataTable();
            GridViewRow gvr1 = gvData.FooterRow;
            Label lblUnbilledQuantity = gvr1.FindControl("lblUnbilledQuantity") as Label;
            DropDownList ddlItemNo = gvr1.FindControl("ddlItemNo") as DropDownList;
            DropDownList ddlReceiptNo = gvr1.FindControl("ddlReceiptNo") as DropDownList;

            Session["unbilled_quantity"] = null;

            if (lblUnbilledQuantity != null)
            {
                lblUnbilledQuantity.Text = string.Empty;

                dtData1 = DBMethod.ExecuteQuery(Invoice_DAL.GetQuantityDetails(ddlItemNo.SelectedValue, ddlSupplierNumber.SelectedValue, ddlInvoiceType.SelectedValue, ddlReceiptNo.SelectedValue)).Tables[0];
                if (dtData1.Rows.Count > 0)
                {
                    lblUnbilledQuantity.Text = (dtData1.Rows[0]["inv_item_qty_billed"].ToString());

                    txtQuantity.Text = (dtData1.Rows[0]["unbilled_quantity"].ToString());

                    Session["unbilled_quantity"] = txtQuantity.Text;

                    if (CommonUtils.ConvertStringToDecimal(lblUnbilledQuantity.Text) > 0)
                    {
                        lblUnbilledQuantity.Text = "Invoiced Quantity-" + lblUnbilledQuantity.Text;
                        lblUnbilledQuantity.Visible = true;
                    }
                    else
                    {
                        lblUnbilledQuantity.Visible = false;
                    }
                }
                else
                {
                    lblUnbilledQuantity.Visible = false;
                }
            }
        }

        protected void ddlLineType_SelectedIndexChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            FillEntityNumber(gvr);
        }
        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txt_UnitPrice = (TextBox)gvr.FindControl("txtUnitPrice");
                txt_UnitPrice.Text = ((TextBox)sender).Text;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtAmount");
                }
                decimal dbl_exchangeRate = 1;
                if (txtExchangeRate.Text.ToString().Length > 0)
                {
                    if (double.Parse(txtExchangeRate.Text.ToString()) > 0)
                    {
                        dbl_exchangeRate = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());
                    }
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text) * dbl_exchangeRate).ToString();

                            txtPOAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOAmount.Text);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void SetColVis4InvoiceType()
        {
            gvData.Columns[3].Visible = true;
            gvData.Columns[4].Visible = true;
            gvData.Columns[5].Visible = true;

            if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
            {
                gvData.Columns[3].Visible = false;
            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
            {

                gvData.Columns[3].Visible = false;
                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;

            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "CREDIT_MEMO")
            {

                gvData.Columns[3].Visible = false;
                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;
            }
            else if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "DEBIT_MEMO")
            {

                gvData.Columns[3].Visible = false;
                gvData.Columns[4].Visible = false;
                gvData.Columns[5].Visible = false;
            }
        }
        private void FillEntityNumber(GridViewRow tmpgvr)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
                DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
                DropDownList ddl_ItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
                TextBox txt_Amount = tmpgvr.FindControl("txtAmount") as TextBox;
                TextBox txt_UnitPrice = tmpgvr.FindControl("txtUnitPrice") as TextBox;
                TextBox txt_Quantity = tmpgvr.FindControl("txtQuantity") as TextBox;
                TextBox txtMisNo = tmpgvr.FindControl("txtMisNo") as TextBox;

                gvData.Columns[3].Visible = true;
                gvData.Columns[4].Visible = true;
                gvData.Columns[5].Visible = true;

                txtMisNo.Visible = false;
                ddl_ReceiptNo.Visible = true;

                if (ddl_LineType.SelectedValue.ToString().ToUpper() == "ITEM")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Item");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Item");
                        gvData.Columns[3].Visible = false;
                    }

                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "MISCELLANEOUS")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                    {
                        txtMisNo.Visible = true;
                        ddl_ReceiptNo.Visible = false;
                        txt_Quantity.Text = "1";
                        gvData.Columns[3].Visible = false;
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "PO_ITEM")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
                    {
                        if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                        {
                            FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber4Supplier(ref ddl_ReceiptNo, ddlSupplierSite.SelectedValue);
                        }
                        else
                        {
                            ddl_LineType.SelectedIndex = 0;
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Supplier Site");

                        }
                    }
                }

                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "SERVICE")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Service");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Service");
                        gvData.Columns[3].Visible = false;
                    }
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
                    {
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ReceiptNo, "Service");
                        FIN.BLL.AP.Item_BLL.GetItemName(ref ddl_ItemNo, "Service");

                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "OTHER_CHARGES")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "STANDARD")
                    {
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ReceiptNo, "OC");
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ItemNo, "OC");
                        gvData.Columns[3].Visible = false;
                    }
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
                    {
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ReceiptNo, "OC");
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ItemNo, "OC");
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "INVOICE")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
                    {
                        if (ddlSupplierNumber.SelectedValue.ToString().Length > 0)
                        {
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ReceiptNo, ddlSupplierNumber.SelectedValue);
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ItemNo, ddlSupplierNumber.SelectedValue);
                            gvData.Columns[3].Visible = false;
                            gvData.Columns[4].Visible = false;
                            gvData.Columns[5].Visible = false;
                            txt_Amount.Enabled = true;
                            txt_Quantity.Text = "1";
                            txt_UnitPrice.Text = "1";
                        }
                        else
                        {
                            ddl_LineType.SelectedIndex = 0;
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Supplier Site");
                        }
                    }
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "CREDIT_MEMO")
                    {
                        if (ddlSupplierNumber.SelectedValue.ToString().Length > 0)
                        {
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ReceiptNo, ddlSupplierNumber.SelectedValue);
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ItemNo, ddlSupplierNumber.SelectedValue);
                            gvData.Columns[3].Visible = false;
                            gvData.Columns[4].Visible = false;
                            gvData.Columns[5].Visible = false;
                            txt_Amount.Enabled = true;
                            txt_Quantity.Text = "1";
                            txt_UnitPrice.Text = "1";
                        }
                        else
                        {
                            ddl_LineType.SelectedIndex = 0;
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Supplier ");
                        }
                    }
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "DEBIT_MEMO")
                    {
                        if (ddlSupplierNumber.SelectedValue.ToString().Length > 0)
                        {
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ReceiptNo, ddlSupplierNumber.SelectedValue);
                            Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddl_ItemNo, ddlSupplierNumber.SelectedValue);
                            gvData.Columns[3].Visible = false;
                            gvData.Columns[4].Visible = false;
                            gvData.Columns[5].Visible = false;
                            txt_Amount.Enabled = true;
                            txt_Quantity.Text = "1";
                            txt_UnitPrice.Text = "1";
                        }
                        else
                        {
                            ddl_LineType.SelectedIndex = 0;
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Supplier ");
                        }
                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "PO")
                {
                    if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PREPAYMENT")
                    {
                        if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
                        {

                            FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber4Supplier(ref ddl_ReceiptNo, ddlSupplierSite.SelectedValue);
                            FIN.BLL.AP.PurchaseOrder_BLL.GetPONumber4Supplier(ref ddl_ItemNo, ddlSupplierSite.SelectedValue);

                            gvData.Columns[3].Visible = false;
                            gvData.Columns[4].Visible = false;
                            gvData.Columns[5].Visible = false;
                            txt_Amount.Enabled = true;
                            txt_Quantity.Text = "1";
                            txt_UnitPrice.Text = "1";
                        }
                        else
                        {
                            ErrorCollection.Add("SelectSupplierSite", "Please Select Supplier Site");
                        }

                    }
                }
                else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "RECEIPT")
                {
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillEntitsdy", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void ddlReceiptNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            FillItemNo(gvr);
        }

        private void FillItemNo(GridViewRow tmpgvr)
        {
            DropDownList ddl_LineType = tmpgvr.FindControl("ddlLineType") as DropDownList;
            DropDownList ddl_ReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
            DropDownList ddl_ItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
            DropDownList ddlAccCode = tmpgvr.FindControl("ddlAccCode") as DropDownList;
            TextBox txtUnitPrice = tmpgvr.FindControl("txtUnitPrice") as TextBox;
            DataTable dtAcctcode = new DataTable();

            if (ddl_LineType.SelectedValue.ToString().ToUpper() == "PO_ITEM")
            {
                PurchaseOrder_BLL.GetItem4PoHeaderid(ref ddl_ItemNo, ddl_ReceiptNo.SelectedValue.ToString());
            }
            else if (ddl_LineType.SelectedValue.ToString().ToUpper() == "ITEM")
            {


                DataTable dtgetunitprice = new DataTable();

                ddl_ItemNo.SelectedValue = ddl_ReceiptNo.SelectedValue;

                dtgetunitprice = DBMethod.ExecuteQuery(Item_DAL.getUnitPrice(ddl_ItemNo.SelectedValue)).Tables[0];
                if (dtgetunitprice != null)
                {
                    if (dtgetunitprice.Rows.Count > 0)
                    {
                        txtUnitPrice.Text = dtgetunitprice.Rows[0]["item_unit_price"].ToString();
                    }
                }

                dtAcctcode = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.GetAcctCode(ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                if (dtAcctcode != null)
                {
                    if (dtAcctcode.Rows.Count > 0)
                    {
                        ddlAccCode.SelectedValue = dtAcctcode.Rows[0]["INV_MAT_ACCT_CODE"].ToString();
                    }
                }
            }

            else
            {
                ddl_ItemNo.SelectedValue = ddl_ReceiptNo.SelectedValue;

            }

            DataTable dtData1 = new DataTable();

            Label lblUnbilledQuantity = tmpgvr.FindControl("lblUnbilledQuantity") as Label;
            DropDownList ddlItemNo = tmpgvr.FindControl("ddlItemNo") as DropDownList;
            DropDownList ddlReceiptNo = tmpgvr.FindControl("ddlReceiptNo") as DropDownList;
            TextBox txtQuantity = tmpgvr.FindControl("txtQuantity") as TextBox;

            if (lblUnbilledQuantity != null && ddlItemNo != null && ddlReceiptNo != null && txtQuantity != null)
            {
                lblUnbilledQuantity.Text = string.Empty;

                dtData1 = DBMethod.ExecuteQuery(Invoice_DAL.GetQuantityDetails(ddlItemNo.SelectedValue, ddlSupplierNumber.SelectedValue, ddlInvoiceType.SelectedValue, ddlReceiptNo.SelectedValue)).Tables[0];
                if (dtData1.Rows.Count > 0)
                {
                    lblUnbilledQuantity.Text = (dtData1.Rows[0]["inv_item_qty_billed"].ToString());

                    if (CommonUtils.ConvertStringToDecimal(lblUnbilledQuantity.Text) > 0)
                    {
                        lblUnbilledQuantity.Text = "Invoiced Quantity-" + lblUnbilledQuantity.Text;
                        lblUnbilledQuantity.Visible = true;
                    }
                    else
                    {
                        lblUnbilledQuantity.Visible = false;
                    }
                }
                else
                {
                    lblUnbilledQuantity.Visible = false;
                }
            }

        }

        protected void btnTax_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_ReceiptNo = (DropDownList)gvr.FindControl("ddlReceiptNo");
            DropDownList ddl_ItemNo = (DropDownList)gvr.FindControl("ddlItemNo");

            if (ddl_ReceiptNo != null && ddl_ItemNo != null)
            {
                if (ddl_ReceiptNo.SelectedValue.ToString().Length > 0 && ddl_ItemNo.SelectedValue.ToString().Length > 0)
                {
                    hf_ItemNo.Value = ddl_ItemNo.SelectedValue.ToString();
                    hf_ReceitpNo.Value = ddl_ReceiptNo.SelectedValue.ToString();
                    DataTable dt_Tax = new DataTable();
                    DataTable dt_InvTax = new DataTable();
                    if (gvr.RowType == DataControlRowType.Footer)
                    {
                        dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails("0", ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                    }
                    else
                    {
                        DataRow[] dr = null;
                        if (Session["InvTaxDet"] != null)
                        {
                            dt_InvTax = (DataTable)Session["InvTaxDet"];
                            if (dt_InvTax != null)
                            {
                                if (dt_InvTax.Rows.Count > 0)
                                    dr = dt_InvTax.Select("EntityNo='" + ddl_ReceiptNo.SelectedValue.ToString() + "' and ItemNo='" + ddl_ItemNo.SelectedValue.ToString() + "'");
                            }
                        }
                        if (dr == null)
                        {
                            dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails(gvData.DataKeys[gvr.RowIndex].Values["INV_LINE_ID"].ToString(), ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                        }
                        else if (dr.Length == 0)
                        {
                            dt_Tax = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceTaxDetails(gvData.DataKeys[gvr.RowIndex].Values["INV_LINE_ID"].ToString(), ddl_ReceiptNo.SelectedValue.ToString(), ddl_ItemNo.SelectedValue.ToString())).Tables[0];
                        }
                    }
                    if (Session["InvTaxDet"] == null)
                    {
                        Session["InvTaxDet"] = dt_Tax;
                        dt_InvTax = dt_Tax;
                    }
                    else
                    {
                        dt_InvTax = (DataTable)Session["InvTaxDet"];
                        if (dt_Tax.Rows.Count > 0)
                        {
                            dt_InvTax.Merge(dt_Tax);
                        }
                    }
                    gvTaxDet.DataSource = dt_InvTax;
                    gvTaxDet.DataBind();
                    mptTaxDet.Show();
                }
            }

        }

        protected void ddlSegment1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment2.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment3.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment4.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment5.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void ddlSegment5_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSegment6.Enabled = true;
            ModalPopupExtender2.Show();
        }

        protected void btnTaxOk_Click(object sender, EventArgs e)
        {
            double dbl_TaxAmt = 0;
            DataTable dt_InvTax = (DataTable)Session["InvTaxDet"];
            for (int iLoop = 0; iLoop < gvTaxDet.Rows.Count; iLoop++)
            {
                TextBox txt_TaxAmount = (TextBox)gvTaxDet.Rows[iLoop].FindControl("txtTaxAmount");
                TextBox txt_TaxPercentage = (TextBox)gvTaxDet.Rows[iLoop].FindControl("txtTaxPercentage");
                DataRow[] dr = dt_InvTax.Select("EntityNo='" + hf_ReceitpNo.Value + "' and ItemNo='" + hf_ItemNo.Value + "' and tax_id='" + gvTaxDet.DataKeys[iLoop].Values["TAX_ID"].ToString() + "'");
                if (txt_TaxAmount.Text.ToString().Length > 0)
                {

                    dr[0]["INV_TAX_AMT"] = txt_TaxAmount.Text;
                    dbl_TaxAmt = dbl_TaxAmt + Convert.ToDouble(txt_TaxAmount.Text);
                    if (txt_TaxPercentage.Text.ToString().Length > 0)
                        dr[0]["INV_TAX_PER"] = txt_TaxPercentage.Text;
                }
                else
                {
                    dr[0]["INV_TAX_AMT"] = DBNull.Value;
                    dr[0]["INV_TAX_PER"] = DBNull.Value;
                }
            }
            Session["InvTaxDet"] = dt_InvTax;
            hf_TotTaxAmt.Value = dbl_TaxAmt.ToString();
            mptTaxDet.Hide();
        }

        protected void ddlInvoiceCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillExchangeRate();
        }
        protected void txtExchangeRate_TextChanged(object sender, EventArgs e)
        {
            AmountChangeBasedonER();
        }
        private void AmountChangeBasedonER()
        {
            DataTable dtData = new DataTable();

            if (Session[FINSessionConstants.GridData] != null)
            {
                dtData = (DataTable)Session[FINSessionConstants.GridData];

                if (dtData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        dtData.Rows[i]["INV_ITEM_AMT"] = CommonUtils.ConvertStringToDecimal(dtData.Rows[i]["INV_ITEM_QTY_BILLED"].ToString()) * CommonUtils.ConvertStringToDecimal(dtData.Rows[i]["INV_ITEM_PRICE"].ToString()) * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text);
                        dtData.AcceptChanges();
                    }
                    //                    foreach(var row in dtData.Rows)
                    //{
                    //    row.SetField<decimal>("Price",row.Field<decimal>("Price")*0.98m);
                    //}
                    //                    var myCopy=dtData.Clone();
                    //var rows=from row in dtData.AsEnumerable()
                    //         where row.Field<decimal>("INV_ITEM_AMT")>=0(select rows;
                    //foreach(var row in rows)
                    //{
                    //    row.SetField<decimal>("INV_ITEM_AMT",row.Field<decimal>("INV_ITEM_QTY_BILLED")*row.Field<decimal>("INV_ITEM_PRICE")* CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text));
                    //}

                    //foreach (var row in dtData.AsEnumerable().Where(x => x.Field<decimal>("Date") >= myDateTime))
                    //    row.SetField<double>("Price", row.Field<double>("Price") * 0.98);
                    //  dtData.AsEnumerable().ToList().ForEach(p => p.SetField<decimal>("INV_ITEM_AMT", dtData.AsEnumerable().ToList().ForEach(ii => ii.SetField<decimal>("INV_ITEM_PRICE", (ii.Field<decimal>("INV_ITEM_PRICE")))) * dtData.AsEnumerable().ToList().ForEach(q => q.SetField<decimal>("INV_ITEM_QTY_BILLED", (q.Field<decimal>("INV_ITEM_QTY_BILLED"))))));
                    //
                    //   dtData.AsEnumerable().ToList().ForEach(x => x["INV_ITEM_AMT"] = (decimal)x["INV_ITEM_PRICE"] * (decimal)x["INV_ITEM_QTY_BILLED"] * CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text));
                    dtData.AcceptChanges();
                }

                BindGrid(dtData);
            }

        }
        private void FillExchangeRate()
        {
            txtExchangeRate.Text = "";

            if (ddlInvoiceCurrency.SelectedValue != string.Empty)
            {
                if (ddlInvoiceCurrency.SelectedValue.ToString() == Session[FINSessionConstants.ORGCurrency].ToString())
                {
                    txtExchangeRate.Text = "1";
                }
                else if ((txtInvoiceDate.Text.ToString().Length > 0) && (ddlInvoiceCurrency.SelectedValue.ToString().Length > 0))
                {
                    DataTable dt_exValue = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.GetExchangeRate(txtInvoiceDate.Text, ddlInvoiceCurrency.SelectedValue)).Tables[0];
                    if (dt_exValue.Rows.Count > 0)
                    {
                        txtExchangeRate.Text = dt_exValue.Rows[0][0].ToString();
                    }
                }
                if (ddlInvoiceCurrency.SelectedValue != string.Empty && ddlPaymentCurrency.SelectedValue != string.Empty)
                {
                    if (ddlInvoiceCurrency.SelectedValue.ToString() == ddlPaymentCurrency.SelectedValue.ToString())
                    {
                        txtExchangeRate.Text = "1";
                        txtExchangeRate.Enabled = false;
                    }
                    else
                    {
                        txtExchangeRate.Text = string.Empty;
                        txtExchangeRate.Enabled = true;
                    }
                }
            }
            AmountChangeBasedonER();

        }

        protected void txtInvoiceDate_TextChanged(object sender, EventArgs e)
        {
            FillExchangeRate();
        }

        protected void gvTaxDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView tmp_erv = (DataRowView)e.Row.DataItem;
                    if ((tmp_erv.Row["ItemNo"].ToString() != hf_ItemNo.Value.ToString()) && (tmp_erv.Row["EntityNo"].ToString() != hf_ReceitpNo.Value.ToString()))
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlInvoiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceDetails(Master.StrRecordId)).Tables[0];
            BindGrid(dtGridData);
            ShowPrepayment();

        }

        private void ShowPrepayment()
        {
            if (ddlInvoiceType.SelectedValue.ToString().ToUpper() == "PO_INVOICE")
            {
                divPrepayment.Visible = true;

            }
            else
            {
                divPrepayment.Visible = false;
            }
        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtInvoiceDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtInvoiceDate.Text);
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("inv_id", Master.StrRecordId.ToString());
                }
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetInvoiceListReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlExchangeRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            getExchangeRate();
        }
        private void getExchangeRate()
        {
            if (ddlPaymentCurrency.SelectedValue == ddlInvoiceCurrency.SelectedValue)
            {
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = false;
            }
            else
            {
                DataTable dt_exRate = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.GetLastExchangeRate(txtInvoiceDate.Text, ddlPaymentCurrency.SelectedValue)).Tables[0];
                if (dt_exRate.Rows.Count > 0)
                {
                    if (ddlExchangeRateType.SelectedValue == "Selling")
                    {
                        txtExchangeRate.Text = dt_exRate.Rows[0]["CURRENCY_BUY_RATE"].ToString();
                    }
                    else if (ddlExchangeRateType.SelectedValue == "Buying")
                    {
                        txtExchangeRate.Text = dt_exRate.Rows[0]["CURRENCY_SEL_RATE"].ToString();
                    }
                    else if (ddlExchangeRateType.SelectedValue == "Standard")
                    {
                        txtExchangeRate.Text = dt_exRate.Rows[0]["CURRENCY_STD_RATE"].ToString();
                    }
                }

            }

        }

        private void LoadTempData()
        {
            DataTable dt_Tempdata = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getTempInvoice(this.LoggedUserName)).Tables[0];
            if (dt_Tempdata.Rows.Count > 0)
            {
                gvTempData.DataSource = dt_Tempdata;
                gvTempData.DataBind();
                mpeTempData.Show();

            }
            else
            {
                gvTempData.DataSource = new DataTable();
                gvTempData.DataBind();

            }

        }

        protected void btnTmpDataDelete_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Temp_RefNo.Value = gvTempData.DataKeys[gvr.RowIndex].Values["INV_ID"].ToString();
            if (hf_Temp_RefNo.Value.ToString().Length > 0)
            {
                DBMethod.ExecuteNonQuery("DELETE TMP_INV_INVOICES_DTLS WHERE INV_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
                DBMethod.ExecuteNonQuery("DELETE TMP_INV_INVOICES_HDR WHERE INV_ID='" + hf_Temp_RefNo.Value.ToString() + "'");
            }
            LoadTempData();
            hf_Temp_RefNo.Value = string.Empty;
            mpeTempData.Show();
        }

        protected void btnTmpDataselect_Click(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            hf_Temp_RefNo.Value = gvTempData.DataKeys[gvr.RowIndex].Values["INV_ID"].ToString();
            TMP_INV_INVOICES_HDR tMP_INV_INVOICES_HDR = new TMP_INV_INVOICES_HDR();
            using (IRepository<TMP_INV_INVOICES_HDR> userCtx = new DataRepository<TMP_INV_INVOICES_HDR>())
            {
                tMP_INV_INVOICES_HDR = userCtx.Find(r =>
                    (r.INV_ID == hf_Temp_RefNo.Value)
                    ).SingleOrDefault();
            }

            ddlSupplierNumber.SelectedValue = tMP_INV_INVOICES_HDR.VENDOR_ID;
            BindSupplierName();
            ddlSupplierName.SelectedValue = tMP_INV_INVOICES_HDR.VENDOR_ID;
            ddlInvoiceType.SelectedValue = tMP_INV_INVOICES_HDR.INV_TYPE;
            ddlPaymentType.SelectedValue = tMP_INV_INVOICES_HDR.INV_PAYMENT_METHOD;
            ddlInvoiceCurrency.SelectedValue = tMP_INV_INVOICES_HDR.INV_CURR_CODE;
            ddlPaymentCurrency.SelectedValue = tMP_INV_INVOICES_HDR.INV_PAY_CURR_CODE;
            txtReason.Text = tMP_INV_INVOICES_HDR.INV_REMARKS;
            txtRejectedReason.Text = tMP_INV_INVOICES_HDR.INV_REJECT_REASON;
            txtInvoiceAmount.Text = tMP_INV_INVOICES_HDR.INV_AMT.ToString();
            txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
            txtTax.Text = tMP_INV_INVOICES_HDR.INV_TAX_AMT.ToString();
            ddlTerm.SelectedValue = tMP_INV_INVOICES_HDR.TERM_ID;
            ddlBatch.SelectedValue = tMP_INV_INVOICES_HDR.INV_BATCH_ID;

            txtInvoiceNumber.Text = tMP_INV_INVOICES_HDR.INV_NUM;
            if (tMP_INV_INVOICES_HDR.INV_DATE != null)
            {
                txtInvoiceDate.Text = DBMethod.ConvertDateToString(tMP_INV_INVOICES_HDR.INV_DATE.ToString());
            }
            if (tMP_INV_INVOICES_HDR.VENDOR_LOC_ID != null)
                ddlSupplierSite.SelectedValue = tMP_INV_INVOICES_HDR.VENDOR_LOC_ID;
            if (tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE != null)
                ddlExchangeRateType.SelectedValue = tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE;
            txtExchangeRate.Text = tMP_INV_INVOICES_HDR.INV_EXCHANGE_RATE.ToString();


            if (tMP_INV_INVOICES_HDR.INV_DUE_DATE != null)
            {
                txtInvDueDate.Text = DBMethod.ConvertDateToString(tMP_INV_INVOICES_HDR.INV_DUE_DATE.ToString());
            }

            if (tMP_INV_INVOICES_HDR.GLOBAL_SEGMENT_ID != null)
            {
                ddlGlobalSegment.SelectedValue = tMP_INV_INVOICES_HDR.GLOBAL_SEGMENT_ID;
            }
            if (tMP_INV_INVOICES_HDR.INV_RETENTION_AMT != null)
            {
                //txtRetAmount.Text = iNV_INVOICES_HDR.INV_RETENTION_AMT.ToString();
                txtRetAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(tMP_INV_INVOICES_HDR.INV_RETENTION_AMT.ToString());
            }
            if (iNV_INVOICES_HDR.INV_PREPAY_BALANCE != null)
            {
                //txtPrePaymentBal.Text = iNV_INVOICES_HDR.INV_PREPAY_BALANCE.ToString();
                txtPrePaymentBal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(tMP_INV_INVOICES_HDR.INV_PREPAY_BALANCE.ToString());
            }
            if (tMP_INV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS == "1")
            {
                btnSave.Visible = false;
            }
            dtGridData = DBMethod.ExecuteQuery(Invoice_DAL.getTempInvoiceDetails(hf_Temp_RefNo.Value)).Tables[0];
            BindGrid(dtGridData);
            ShowPrepayment();
        }



        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            imgBtnPost.Enabled = false;
            try
            {

                ErrorCollection.Clear();
                string str_sp_output = FINSP.SP_POSTING_ACCTCODE_VALIDATION("AP_021", ddlSupplierNumber.SelectedValue, Master.StrRecordId);

                if (str_sp_output != "SUCCESS")
                {
                    ErrorCollection.Add("PostingAcctErr", str_sp_output);
                    return;
                }

                //DataTable dt_supp_cust = DBMethod.ExecuteQuery(Supplier_DAL.getSupplierCustomer4Id(ddlSupplierNumber.SelectedValue)).Tables[0];
                //DataTable dt_ssm = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemOptions_DAL.getSysoptID()).Tables[0];

               
                //if (dt_supp_cust.Rows[0]["AP_LIABILITY_ACCOUNT"].ToString() == string.Empty)
                //{
                //    if (dt_ssm.Rows[0]["AP_LIABILITY_ACCT"].ToString() == string.Empty)
                //    {
                //        ErrorCollection.Add("ADDLibacct", "Please Enter the Liability Account");
                //        return;
                //    }
                //}

              /*  if (dt_supp_cust.Rows[0]["AP_ADVANCE_ACCOUNT"].ToString() == string.Empty)
                {
                    if (dt_ssm.Rows[0]["AP_ADVANCE_ACCT"].ToString() == string.Empty)
                    {
                        ErrorCollection.Add("ADDADVacct", "Please Enter the Advance Account");
                        return;
                    }
                } 

                if (txtRetAmount.Text.ToString().Trim().Length > 0)
                {
                    if (dt_supp_cust.Rows[0]["VENDOR_RETENTION_ACCT_ID"].ToString() == string.Empty)
                    {
                        if (dt_ssm.Rows[0]["AP_RETENTION_ACCT"].ToString() == string.Empty)
                        {
                            ErrorCollection.Add("ADDRETacct", "Please Enter the Retention Account");
                            return;
                        }
                    }
                }

                */

                if (EntityData != null)
                {
                    iNV_INVOICES_HDR = (INV_INVOICES_HDR)EntityData;
                }
                if (iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(iNV_INVOICES_HDR.INV_ID, "AP_021");

                }

                iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
                iNV_INVOICES_HDR.POSTED_FLAG = FINAppConstants.Y;
                iNV_INVOICES_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<INV_INVOICES_HDR>(iNV_INVOICES_HDR, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


                imgBtnPost.Visible = false;
                imgbtnInvoiceCancel.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;
                pnlReason.Enabled = false;
                pnlRejReason.Enabled = false;

                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    ReportFile = "GL_REPORTS//RPTJournalVoucher_OL.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";
                }
                else
                {
                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";
                }


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", iNV_INVOICES_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                imgBtnPost.Enabled = true;
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    imgBtnPost.Enabled = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

            if (iNV_INVOICES_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

              

                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    ReportFile = "GL_REPORTS//RPTJournalVoucher_OL.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";
                }
                else
                {
                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";
                }


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", iNV_INVOICES_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
                    // iNV_INVOICES_HDR = (INV_INVOICES_HDR)EntityData;

                    ErrorCollection.Remove("Paythere");
                    if (Invoice_DAL.IsPaymentRecordFound(iNV_INVOICES_HDR.INV_ID))
                    {
                        ErrorCollection.Add("Paythere", "Payment is already exists for this invoice.Please stop the payment, before cancel the invoice");
                        return;
                    }


                    if (iNV_INVOICES_HDR.POSTED_FLAG != null)
                    {
                        if (iNV_INVOICES_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(iNV_INVOICES_HDR.JE_HDR_ID, "AP_021_R", this.LoggedUserName);
                        }
                    }

                    imgBtnPost.Visible = false;
                    lblCancelled.Visible = true;
                    imgBtnCancelJV.Visible = true;
                    imgBtnCancel.Visible = false;

                    lblPosted.Visible = true;

                    pnlgridview.Enabled = false;
                    pnltdHeader.Enabled = false;
                    pnlReason.Enabled = false;
                    pnlRejReason.Enabled = false;

                    btnSave.Visible = false;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                    iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
                }
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";



                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", iNV_INVOICES_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnCancelJV_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);

                if (iNV_INVOICES_HDR != null)
                {

                    Hashtable htParameters = new Hashtable();
                    Hashtable htHeadingParameters = new Hashtable();
                    Hashtable htFilterParameter = new Hashtable();

                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", iNV_INVOICES_HDR.REV_JE_HDR_ID));


                    Session["ProgramName"] = "Journal Voucher";
                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgbtnInvoiceCancel_Click(object sender, ImageClickEventArgs e)
        {
            iNV_INVOICES_HDR = Invoice_BLL.getClassEntity(Master.StrRecordId);
            iNV_INVOICES_HDR.POSTED_FLAG = FINAppConstants.CANCELED;
            iNV_INVOICES_HDR.REV_DATE = DateTime.Today;
            iNV_INVOICES_HDR.REV_FLAG = FINAppConstants.Y;
            DBMethod.SaveEntity<INV_INVOICES_HDR>(iNV_INVOICES_HDR, true);
            ErrorCollection.Add("Invoice Canceled", "Invoice Cancelled Successfully");
            imgBtnPost.Visible = false;
            imgbtnInvoiceCancel.Visible = false;
            lblCancelled.Visible = true;
            btnSave.Visible = false;
            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
           
        }
    }
        #endregion
}