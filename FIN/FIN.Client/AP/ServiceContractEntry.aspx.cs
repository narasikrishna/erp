﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.AP
{
    public partial class ServiceContractEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        AP_SERVICE_CONTRACT aP_SERVICE_CONTRACT = new AP_SERVICE_CONTRACT();
        AP_SERVICE_CONTRACT_DTL aP_SERVICE_CONTRACT_DTL = new AP_SERVICE_CONTRACT_DTL();
        DataTable dtGridData = new DataTable();

        string ProReturn = null;

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {

            FIN.BLL.AP.Item_BLL.GetItemName(ref ddlContType, "Service");
            // Lookup_BLL.GetLookUpValues(ref ddlContType, "CONT_TYPE");
            Lookup_BLL.GetLookUpValues(ref ddlPayType, "PAY_TYPE");
            Supplier_BLL.GetSupplierName(ref ddlSuppliername);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;
                btnPrint.Visible = false;
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.ServiceContract_DAL.getServiceContractDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<AP_SERVICE_CONTRACT> userCtx = new DataRepository<AP_SERVICE_CONTRACT>())
                    {
                        aP_SERVICE_CONTRACT = userCtx.Find(r =>
                            (r.SERV_CONTR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    btnPrint.Visible = true;
                    EntityData = aP_SERVICE_CONTRACT;

                    txtContNumber.Text = aP_SERVICE_CONTRACT.SERV_CONTR_NUM;
                    txtContDate.Text = DBMethod.ConvertDateToString(aP_SERVICE_CONTRACT.SERV_CONTR_DATE.ToShortDateString()).ToString();

                    ddlContType.SelectedValue = aP_SERVICE_CONTRACT.SERV_CONTR_TYPE;
                    ddlPayType.SelectedValue = aP_SERVICE_CONTRACT.SERV_CONTR_PAY_TYPE;
                    ddlSuppliername.SelectedValue = aP_SERVICE_CONTRACT.VENDOR_ID;
                    BindSupplierSite();
                    ddlSupplierSite.SelectedValue = aP_SERVICE_CONTRACT.VENDOR_SITE_ID;
                    txtContFrom.Text = DBMethod.ConvertDateToString(aP_SERVICE_CONTRACT.SERV_CONTR_FROM_DATE.ToString()).ToString();
                    txtContTo.Text = DBMethod.ConvertDateToString(aP_SERVICE_CONTRACT.SERV_CONTR_TO_DATE.ToString()).ToString();
                    txtContValue.Text = aP_SERVICE_CONTRACT.SERV_CONTR_VALUE.ToString();
                    txtRemarks.Text = aP_SERVICE_CONTRACT.ATTRIBUTE1;

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();



                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    aP_SERVICE_CONTRACT = (AP_SERVICE_CONTRACT)EntityData;
                }

                aP_SERVICE_CONTRACT.SERV_CONTR_NUM = txtContNumber.Text;


                aP_SERVICE_CONTRACT.SERV_CONTR_DATE = DBMethod.ConvertStringToDate(txtContDate.Text.ToString());

                aP_SERVICE_CONTRACT.SERV_CONTR_TYPE = ddlContType.SelectedValue.ToString();
                aP_SERVICE_CONTRACT.VENDOR_ID = (ddlSuppliername.SelectedValue.ToString());
                aP_SERVICE_CONTRACT.VENDOR_SITE_ID = ddlSupplierSite.SelectedValue.ToString();
                aP_SERVICE_CONTRACT.SERV_CONTR_PAY_TYPE = ddlPayType.SelectedValue.ToString();
                aP_SERVICE_CONTRACT.SERV_CONTR_FROM_DATE = DBMethod.ConvertStringToDate(txtContFrom.Text.ToString());
                aP_SERVICE_CONTRACT.SERV_CONTR_TO_DATE = DBMethod.ConvertStringToDate(txtContTo.Text);
                aP_SERVICE_CONTRACT.SERV_CONTR_VALUE = int.Parse(txtContValue.Text);

                aP_SERVICE_CONTRACT.ATTRIBUTE1 = txtRemarks.Text;
                aP_SERVICE_CONTRACT.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                aP_SERVICE_CONTRACT.SERV_CONTR_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    aP_SERVICE_CONTRACT.MODIFIED_BY = this.LoggedUserName;
                    aP_SERVICE_CONTRACT.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    aP_SERVICE_CONTRACT.SERV_CONTR_ID = FINSP.GetSPFOR_SEQCode("AP_030_M", false, true);
                    aP_SERVICE_CONTRACT.CREATED_BY = this.LoggedUserName;
                    aP_SERVICE_CONTRACT.CREATED_DATE = DateTime.Today;
                }
                aP_SERVICE_CONTRACT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aP_SERVICE_CONTRACT.SERV_CONTR_ID);


                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Invoice ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                AP_SERVICE_CONTRACT_DTL aP_SERVICE_CONTRACT_DTL = new AP_SERVICE_CONTRACT_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    aP_SERVICE_CONTRACT_DTL = new AP_SERVICE_CONTRACT_DTL();
                    if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["SERV_CONTR_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<AP_SERVICE_CONTRACT_DTL> userCtx = new DataRepository<AP_SERVICE_CONTRACT_DTL>())
                        {
                            aP_SERVICE_CONTRACT_DTL = userCtx.Find(r =>
                                (r.SERV_CONTR_DTL_ID == dtGridData.Rows[iLoop]["SERV_CONTR_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    aP_SERVICE_CONTRACT_DTL.SERV_CONTR_ID = aP_SERVICE_CONTRACT.SERV_CONTR_ID;
                    if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_FROM_DATE"] != null)
                    {
                        if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_FROM_DATE"].ToString().Length > 0)
                        {
                            aP_SERVICE_CONTRACT_DTL.SERV_CONTR_DTL_FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["SERV_CONTR_DTL_FROM_DATE"].ToString());
                        }
                    }
                    if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_TO_DATE"] != null)
                    {
                        if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_TO_DATE"].ToString().Length > 0)
                        {
                            aP_SERVICE_CONTRACT_DTL.SERV_CONTR_DTL_TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["SERV_CONTR_DTL_TO_DATE"].ToString());
                        }
                    }
                    if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_DAYS"] != null)
                    {
                        if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_DAYS"].ToString().Length > 0)
                        {
                            aP_SERVICE_CONTRACT_DTL.SERV_CONTR_DTL_DAYS = int.Parse(dtGridData.Rows[iLoop]["SERV_CONTR_DTL_DAYS"].ToString());
                        }
                    }
                    if (dtGridData.Rows[iLoop]["SERV_CONTR_PERCENTAGE"] != null)
                    {
                        if (dtGridData.Rows[iLoop]["SERV_CONTR_PERCENTAGE"].ToString().Length > 0)
                        {
                            aP_SERVICE_CONTRACT_DTL.PERCENTAGE = decimal.Parse(dtGridData.Rows[iLoop]["SERV_CONTR_PERCENTAGE"].ToString());
                        }
                    }
                    aP_SERVICE_CONTRACT_DTL.SERV_CONTR_INV_STATUS = dtGridData.Rows[iLoop]["SERV_CONTR_INV_STATUS"].ToString();
                    aP_SERVICE_CONTRACT_DTL.SERV_CONTR_PAY_STATUS = dtGridData.Rows[iLoop]["SERV_CONTR_PAY_STATUS"].ToString();
                    aP_SERVICE_CONTRACT_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    aP_SERVICE_CONTRACT_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(aP_SERVICE_CONTRACT_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["SERV_CONTR_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["SERV_CONTR_DTL_ID"].ToString() != string.Empty)
                        {
                            aP_SERVICE_CONTRACT_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(aP_SERVICE_CONTRACT_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            aP_SERVICE_CONTRACT_DTL.SERV_CONTR_DTL_ID = FINSP.GetSPFOR_SEQCode("AP_030_D", false, true);
                            aP_SERVICE_CONTRACT_DTL.CREATED_BY = this.LoggedUserName;
                            aP_SERVICE_CONTRACT_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(aP_SERVICE_CONTRACT_DTL, FINAppConstants.Add));
                        }
                    }


                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<AP_SERVICE_CONTRACT, AP_SERVICE_CONTRACT_DTL>(aP_SERVICE_CONTRACT, tmpChildEntity, aP_SERVICE_CONTRACT_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<AP_SERVICE_CONTRACT, AP_SERVICE_CONTRACT_DTL>(aP_SERVICE_CONTRACT, tmpChildEntity, aP_SERVICE_CONTRACT_DTL, true);
                            savedBool = true;
                            break;
                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Position");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtContDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtContDate.Text);
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("SERV_CONTR_ID", Master.StrRecordId.ToString());
                }
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.ServiceContract_BLL.GetServiceContractReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            TextBox txt_days = gvr.FindControl("txtdays") as TextBox;
            TextBox txt_PerCentage = gvr.FindControl("txtPerCentage") as TextBox;
            Label lbl_InvStatus = gvr.FindControl("lblInvStatus") as Label;
            Label lbl_PayStatus = gvr.FindControl("lblPayStatus") as Label;
           
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["SERV_CONTR_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = dtp_FromDate;
            slControls[1] = dtp_ToDate;
            slControls[2] = txt_days;
            string strCtrlTypes = FINAppConstants.TEXT_BOX ;
            string strMessage = "From Date";
            if (dtp_ToDate.Text.Trim().Length > 0)
            {
                slControls[1] = dtp_ToDate;
                 strCtrlTypes ="~" + FINAppConstants.DATE_RANGE_VALIDATE;
                 strMessage = "~" + " To Date ";
            }
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //Dictionary<string, string> Prop_File_Data;
            //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            //ErrorCollection.Clear();
            //string strCtrlTypes = "TextBox~TextBox~TextBox";
            //string strMessage = Prop_File_Data["Position_Code_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            ////string strMessage = "Position Code ~ Description ~ Start Date ~ Start Date ~ End Date";
            //ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            //if (ErrorCollection.Count > 0)
            //    return drList;


            //string strCondition = "POSITION_CODE='" + txtcode.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["POSITION_ID"].ToString(), ddlJobCode.SelectedValue, txtcode.Text);
            //if (ProReturn != string.Empty)
            //{
            //    if (ProReturn != "0")
            //    {
            //        ErrorCollection.Add("POSITION", ProReturn);
            //        if (ErrorCollection.Count > 0)
            //        {
            //            return drList;
            //        }
            //    }
            //}

            if (txt_days.Text.ToString().Length > 0)
            {
                drList["SERV_CONTR_DTL_DAYS"] = txt_days.Text;
            }
            else
            {
                drList["SERV_CONTR_DTL_DAYS"] = DBNull.Value;
            }
            if (txt_PerCentage.Text.ToString().Length > 0)
            {
                drList["SERV_CONTR_PERCENTAGE"] = txt_PerCentage.Text;
            }
            else
            {
                drList["SERV_CONTR_PERCENTAGE"] = DBNull.Value;
            }
            drList["SERV_CONTR_INV_STATUS"] = lbl_InvStatus.Text;
            drList["SERV_CONTR_PAY_STATUS"] = lbl_PayStatus.Text;

            if (dtp_FromDate.Text.ToString().Length > 0)
            {
                drList["SERV_CONTR_DTL_FROM_DATE"] = DBMethod.ConvertStringToDate(dtp_FromDate.Text.ToString());
            }
            else
            {
                drList["SERV_CONTR_DTL_FROM_DATE"] = DBNull.Value;
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {

                drList["SERV_CONTR_DTL_TO_DATE"] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList["SERV_CONTR_DTL_TO_DATE"] = DBNull.Value;
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                if (Master.StrRecordId != "0")
                {
                    GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                    Button btn_genInv = gvr.FindControl("btnGenerateInvoice") as Button;
                    btn_genInv.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    aP_SERVICE_CONTRACT.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<AP_SERVICE_CONTRACT>(aP_SERVICE_CONTRACT);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POS_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlSuppliername_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindSupplierSite();
        }
        private void BindSupplierSite()
        {
            try
            {
                ErrorCollection.Clear();

                FIN.BLL.AP.SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref  ddlSupplierSite, ddlSuppliername.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeGridColumn();
        }

        private void ChangeGridColumn()
        {
            if (ddlPayType.SelectedValue != "Fixed_Days")
            {
                gvData.Columns[0].Visible = true;
                gvData.Columns[1].Visible = true;
            }
            else
            {
                gvData.Columns[0].Visible = false;
                gvData.Columns[1].Visible = false;
            }
        }

        protected void btnGenerateInvoice_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            AutoInsertInvoice(gvr);
        }

        private void AutoInsertInvoice(GridViewRow gvr)
        {

            INV_INVOICES_HDR iNV_INVOICES_HDR = new INV_INVOICES_HDR();


            iNV_INVOICES_HDR.INV_NUM = txtContNumber.Text + "_" + gvr.RowIndex;
            iNV_INVOICES_HDR.INV_DATE = DateTime.Now;

            iNV_INVOICES_HDR.VENDOR_ID = ddlSuppliername.SelectedValue.ToString();
            iNV_INVOICES_HDR.INV_TYPE = "Contract";
            iNV_INVOICES_HDR.INV_PAYMENT_METHOD = "Cheque";
            iNV_INVOICES_HDR.INV_CURR_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
            iNV_INVOICES_HDR.INV_PAY_CURR_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
            iNV_INVOICES_HDR.INV_REMARKS = "";
            iNV_INVOICES_HDR.INV_REJECT_REASON = "";
            iNV_INVOICES_HDR.INV_AMT = CommonUtils.ConvertStringToDecimal(txtContValue.Text);
            iNV_INVOICES_HDR.INV_TAX_AMT = 0;
            // iNV_INVOICES_HDR.TERM_ID = ddlTerm.SelectedValue.ToString();
            //iNV_INVOICES_HDR.INV_BATCH_ID = ddlBatch.SelectedValue.ToString();
            // iNV_INVOICES_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;


            iNV_INVOICES_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
            iNV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;



            iNV_INVOICES_HDR.INV_DUE_DATE = null;

            if (ddlSupplierSite.SelectedValue.ToString().Length > 0)
            {
                iNV_INVOICES_HDR.VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;
            }
            else
            {
                iNV_INVOICES_HDR.VENDOR_LOC_ID = null;
            }


            iNV_INVOICES_HDR.INV_EXCHANGE_RATE_TYPE = null;



            iNV_INVOICES_HDR.INV_EXCHANGE_RATE = 1;


            iNV_INVOICES_HDR.INV_RETENTION_AMT = null;

            iNV_INVOICES_HDR.INV_PREPAY_BALANCE = null;


            iNV_INVOICES_HDR.INV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_M.ToString(), false, true);
            iNV_INVOICES_HDR.CREATED_BY = this.LoggedUserName;
            iNV_INVOICES_HDR.CREATED_DATE = DateTime.Today;

            iNV_INVOICES_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_INVOICES_HDR.INV_ID);

            iNV_INVOICES_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;


            //Save Detail Table




            var tmpChildEntity = new List<Tuple<object, string>>();
            INV_INVOICES_DTLS iNV_INVOICES_DTLS = new INV_INVOICES_DTLS();





            iNV_INVOICES_DTLS.INV_LINE_TYPE = "SERVICE";
            iNV_INVOICES_DTLS.INV_LINE_NUM = gvr.RowIndex;
            iNV_INVOICES_DTLS.INV_ITEM_ID = ddlContType.SelectedValue.ToString();
            iNV_INVOICES_DTLS.INV_ITEM_QTY_BILLED = CommonUtils.ConvertStringToDecimal(txtContValue.Text);
            iNV_INVOICES_DTLS.INV_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(txtContValue.Text);
            iNV_INVOICES_DTLS.INV_ITEM_AMT = CommonUtils.ConvertStringToDecimal(txtContValue.Text);

            INV_ITEM_MASTER iNV_ITEM_MASTER = new INV_ITEM_MASTER();
            using (IRepository<INV_ITEM_MASTER> userCtx = new DataRepository<INV_ITEM_MASTER>())
            {
                iNV_ITEM_MASTER = userCtx.Find(r =>
                    (r.ITEM_ID == ddlContType.SelectedValue.ToString())
                    ).SingleOrDefault();
            }

            if (iNV_ITEM_MASTER != null)
            {
                iNV_INVOICES_DTLS.INV_ITEM_ACCT_CODE_ID = iNV_ITEM_MASTER.INV_MAT_ACCT_CODE;
            }

            //  iNV_INVOICES_DTLS.ATTRIBUTE1 = dtGridData.Rows[iLoop]["MIS_NO"].ToString();

            // iNV_INVOICES_DTLS.RECEIPT_ID = dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_ID].ToString();


            iNV_INVOICES_DTLS.INV_LINE_TAX_AMT = 0;

            iNV_INVOICES_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

            iNV_INVOICES_DTLS.INV_ID = iNV_INVOICES_HDR.INV_ID;

            iNV_INVOICES_DTLS.WORKFLOW_COMPLETION_STATUS = "1";


            iNV_INVOICES_DTLS.INV_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_021_D.ToString(), false, true);
            iNV_INVOICES_DTLS.CREATED_BY = this.LoggedUserName;
            iNV_INVOICES_DTLS.CREATED_DATE = DateTime.Today;
            tmpChildEntity.Add(new Tuple<object, string>(iNV_INVOICES_DTLS, FINAppConstants.Add));





            switch (Master.Mode)
            {
                case FINAppConstants.Add:
                    {
                        CommonUtils.SavePCEntity<INV_INVOICES_HDR, INV_INVOICES_DTLS>(iNV_INVOICES_HDR, tmpChildEntity, iNV_INVOICES_DTLS);
                        savedBool = true;
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        CommonUtils.SavePCEntity<INV_INVOICES_HDR, INV_INVOICES_DTLS>(iNV_INVOICES_HDR, tmpChildEntity, iNV_INVOICES_DTLS, true);
                        savedBool = true;
                        break;
                    }
            }
        }

    }
}