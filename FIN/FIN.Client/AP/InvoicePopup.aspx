﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoicePopup.aspx.cs" Inherits="FIN.Client.AP.InvoicePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FIN</title>
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="../Scripts/JQueryValidatioin/jquery.validationEngine_EN.js" type="text/javascript"></script>
    <script src="../Scripts/JQueryValidatioin/jquery.validationEngine.js" type="text/javascript"></script>
    <link href="../Scripts/JQueryValidatioin/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" id="MainCss" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Scripts/DDLChoosen/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/DDLChoosen/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function fn_ClearForm() {
            alert('Inside FormClear');
        }
        $(function () {
            //            var validator = $("#form1").data('validator');
            //            validator.settings.ignore = ":hidden:not(select)";
            $("#form1").validationEngine('attach', { promptPosition: "bottomRight" });
            //            $('#form1').validationEngine({ prettySelect: true, usePrefix: 's2id_', autoPositionUpdate: true });
            //            $("#form1").validationEngine({
            //                prettySelect: true,
            //                useSuffix: "_chzn"
            //                //promptPosition : "bottomRight"
            //            });
        });

        function checkDate(sender, args) {
            $('#form1').validationEngine('hide');
        }
    </script>
    <script type="text/javascript">
        function fn_closeerror() {
            $('#div_Message').hide();
            $('#div_Message').text("");
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            //$(".ddlStype").chosen();
        });

        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded)
        });
        function PageLoaded(sender, args) {
            //$(".ddlStype").chosen();                      
        }
    </script>
    <style>
        div
        {
            pointer-events: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>
        <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <table width="100%">
            <tr>
                <td>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblInvoiceNumber">
                            Invoice Number
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px" id="lblInvoiceNoValue">
                            <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="validate[required] RequiredField txtBox"
                                TabIndex="1"></asp:TextBox>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 147px" id="lblInvoiceDate">
                            Invoice Date
                        </div>
                        <div class="divtxtBox" style="float: left; width: 132px">
                            <asp:TextBox ID="txtInvoiceDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                                AutoPostBack="True" OnTextChanged="txtInvoiceDate_TextChanged"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtInvoiceDate"
                                OnClientDateSelectionChanged="checkDate" />
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <%--<div style="float:left ;width :90px"> &nbsp;</div>--%>
                        <div class="lblBox" style="float: left; width: 152px" id="lblInvoiceDueDate">
                            Invoice Due Date
                        </div>
                        <div class="divtxtBox" style="float: left; width: 135px">
                            <asp:TextBox ID="txtInvDueDate" runat="server" CssClass=" txtBox" TabIndex="3"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="ceInvDueDate" TargetControlID="txtInvDueDate"
                                OnClientDateSelectionChanged="checkDate" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblInvoiceType">
                            Invoice Type
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlInvoiceType" runat="server" TabIndex="4" CssClass=" ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlInvoiceType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 145px" id="lblInvoiceCurrency">
                            Invoice Currency
                        </div>
                        <div class="divtxtBox" style="float: left; width: 445px">
                            <asp:DropDownList ID="ddlInvoiceCurrency" runat="server" TabIndex="5" CssClass="  ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlInvoiceCurrency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblSupplierNumber">
                            Supplier Number
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlSupplierNumber" runat="server" TabIndex="6" CssClass="validate[required] RequiredField ddlStype"
                                OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 145px" id="lblSupplierName">
                            Supplier Name
                        </div>
                        <div class="divtxtBox" style="float: left; width: 445px">
                            <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                                Enabled="false">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblSupplierSite">
                            Supplier Site
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="8" CssClass="ddlStype">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 145px" id="lblPaymentCurrency">
                            Payment Currency
                        </div>
                        <div class="divtxtBox" style="float: left; width: 445px">
                            <asp:DropDownList ID="ddlPaymentCurrency" runat="server" TabIndex="12" CssClass=" ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlInvoiceCurrency_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblPaymentType">
                            Payment Type
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlPaymentType" runat="server" TabIndex="11" CssClass=" ddlStype"
                                OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 145px" id="lblExchangeRateType">
                            Exchange Rate Type
                        </div>
                        <div class="divtxtBox" style="float: left; width: 155px">
                            <asp:DropDownList ID="ddlExchangeRateType" runat="server" TabIndex="9" CssClass="validate[required] RequiredField ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlExchangeRateType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 125px" id="lblExchangeRate">
                            Exchange Rate
                        </div>
                        <div class="divtxtBox" style="float: left; width: 145px">
                            <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="10" CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                            Global Segment
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                TabIndex="14">
                                <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 145px" id="lblRetentionAmount">
                            Retention Amount
                        </div>
                        <div class="divtxtBox" style="float: left; width: 130px;">
                            <asp:TextBox ID="txtRetAmount" runat="server" TabIndex="15" CssClass="txtBox_N"></asp:TextBox>
                        </div>
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,custom"
                            ValidChars=".," TargetControlID="txtRetAmount" />
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 150px;" id="Div1">
                            Invoice Amount
                        </div>
                        <div class="divtxtBox" style="float: left; width: 150px">
                            <asp:TextBox ID="txtInvoiceAmount" runat="server" TabIndex="16" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                                Width="150px" Enabled="False"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtInvoiceAmount" />
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div id="divPrepayment" runat="server" visible="false">
                            <div class="lblBox" style="float: left; width: 150px" id="Div2">
                                Prepayment Amount
                            </div>
                            <div class="divtxtBox" style="float: left; width: 145px;">
                                <asp:TextBox ID="txtPrePaymentAmt" runat="server" TabIndex="16" CssClass="txtBox_N"
                                    Enabled="False"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtPrePaymentAmt" />
                            </div>
                            <div class="colspace" style="float: left;">
                                &nbsp</div>
                            <div class="divtxtBox" style="float: left; width: 120px" align="center">
                                <input id="Button1" type="button" value="Adjust" onclick="ShowPoPrepay()" />
                            </div>
                            <div class="lblBox" style="float: left; width: 150px" id="lblPrePaymentBalance">
                                Prepayment Balance
                            </div>
                            <div class="divtxtBox" style="float: left; width: 145px;">
                                <asp:TextBox ID="txtPrePaymentBal" runat="server" TabIndex="16" CssClass="txtBox_N"
                                    Enabled="False"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                                    ValidChars=".," TargetControlID="txtPrePaymentBal" />
                            </div>
                        </div>
                    </div>
                    <div class="divRowContainer" style="position: fixed; top: 350; left: 300; display: none;
                        background-color: ThreeDFace" id="div_PoPrePay">
                        <asp:GridView ID="gv_Po_PerPay" runat="server" AutoGenerateColumns="False" DataKeyNames="PP_ADJ_ID,PO_HEADER_ID,PO_PRE_PAY_AMT,PO_USED_AMT,PO_CUR_AMT,PO_ADJ_AMT,DELETED">
                            <Columns>
                                <asp:BoundField DataField="PO_NUM" HeaderText="Po Number" />
                                <asp:BoundField DataField="PO_PRE_PAY_AMT" HeaderText="PrePayment Amount" />
                                <asp:BoundField DataField="PO_USED_AMT" HeaderText="Used Amount" />
                                <asp:BoundField DataField="PO_CUR_AMT" HeaderText="Current Amount" />
                                <asp:TemplateField HeaderText="Adjustment Amount">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCurrentAmto" runat="server" Text='<%# Eval("PO_ADJ_AMT") %>'
                                            Width="150px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GrdAltRow" />
                        </asp:GridView>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer" runat="server" id="divTerms" visible="false">
                        <div class="lblBox" style="float: left; width: 150px" id="lblTerms">
                            Terms
                        </div>
                        <div class="divtxtBox" style="float: left; width: 270px">
                            <asp:DropDownList ID="ddlTerm" runat="server" TabIndex="13" CssClass="validate[required] RequiredField ddlStype">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 200px; display: none" id="lblBatch">
                            Batch
                        </div>
                        <div class="divtxtBox" style="float: left; width: 155px; display: none">
                            <asp:DropDownList ID="ddlBatch" runat="server" TabIndex="17" CssClass="validate[required] RequiredField ddlStype">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                </td>
                <td valign="top">
                    <table runat="server" id="tblbtn" visible="false">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" />  
                                    <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox"  Font-Size ="18px" Font-Bold="true"  style="color:Green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/Print.png" OnClick="btnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <asp:ImageButton ID="imgBtnCancel" runat="server" ImageUrl="~/Images/CancelJV.png"
                                    Style="border: 0px" onclick="imgBtnCancel_Click" />
                                    <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox"  Font-Size ="18px" Font-Bold="true"  style="color:Red"></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td>
                               <asp:ImageButton ID="imgBtnCancelJV" runat="server" ImageUrl="~/Images/CancelJVPrint.png"
                                    Style="border: 0px" onclick="imgBtnCancelJV_Click"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divRowContainer " align="left">
            <%--style="height: 150px; overflow: scroll"--%>
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="100%"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="INV_LINE_ID,INV_LINE_TYPE,RECEIPT_ID,ITEM_ID,CODE_ID,INV_SEGMENT_ID1,INV_SEGMENT_ID2,INV_SEGMENT_ID3,INV_SEGMENT_ID4,INV_SEGMENT_ID5,INV_SEGMENT_ID6,GRN_NUM">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="28" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="29" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="30" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="31" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="27" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("INV_LINE_NUM") %>'
                                Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px" TabIndex="15" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("INV_LINE_NUM") %>'></asp:Label>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="40px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Line Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLineType" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlLineType_SelectedIndexChanged"
                                Width="120px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLineType" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="18" AutoPostBack="True" OnSelectedIndexChanged="ddlLineType_SelectedIndexChanged"
                                Width="120px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineType" runat="server" Text='<%# Eval("INV_LINE_TYPE_DESC") %>'
                                Width="120px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlReceiptNo" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlReceiptNo_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtMisNo" MaxLength="240" runat="server" Text='<%# Eval("MIS_NO") %>'
                                Visible="false" CssClass="EntryFont RequiredField txtBox" Width="200px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlReceiptNo" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="19" AutoPostBack="True" OnSelectedIndexChanged="ddlReceiptNo_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtMisNo" MaxLength="240" runat="server" Text='<%# Eval("MIS_NO") %>'
                                Visible="false" TabIndex="19" CssClass="EntryFont RequiredField txtBox" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReceiptNo" runat="server" Text='<%# Eval("GRN_NUM") %>' Width="200px"></asp:Label>
                            <asp:Label ID="lblmisno" runat="server" Text='<%# Eval("MIS_NO") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemNo" runat="server" CssClass="ddlStype RequiredField"
                                Width="170px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemNo_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' CssClass="EntryFont txtBox"
                                Enabled="false" Visible="false" Width="170px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemNo" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="20" Width="170px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemNo_SelectedIndexChanged">
                                <asp:ListItem>--- Select ---</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtItemName" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                Enabled="false" Width="170px" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemNo" Width="170px" runat="server" Text='<%# Eval("item_id") %>'
                                Visible="false"></asp:Label>
                            <asp:Label ID="lblItemName" Width="170px" MaxLength="250" runat="server" Text='<%# Eval("ITEM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("INV_ITEM_QTY_BILLED") %>'
                                AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                                Width="96%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                            <asp:Label ID="lblUnbilledQuantity" Visible="false" runat="server" Text='<%# Eval("unbilled_quantity") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="21" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" Width="120px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers" TargetControlID="txtQuantity" />
                            <asp:Label ID="lblUnbilledQuantity" Visible="false" runat="server" Text='<%# Eval("unbilled_quantity") %>'></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("INV_ITEM_QTY_BILLED") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Price">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="13" runat="server" Text='<%# Eval("INV_ITEM_PRICE") %>'
                                AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                                Width="96%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,custom"
                                ValidChars=".," TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="22" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" Width="120px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender23" runat="server" FilterType="Numbers,custom" ValidChars=".,"
                                    TargetControlID="txtUnitPrice" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("INV_ITEM_PRICE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" MaxLength="13" runat="server" Text='<%# Eval("INV_ITEM_AMT") %>'
                                AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" TabIndex="23" Enabled="true"
                                CssClass="EntryFont RequiredField txtBox_N" Width="96%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" TabIndex="23" Enabled="true"
                                Width="120px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender64"
                                    runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAMount" runat="server" Text='<%# Eval("INV_ITEM_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype RequiredField"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlAccountCodes_SelectedIndexChanged"
                                TabIndex="24" Width="200px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccCode" runat="server" Text='<%# Eval("CODE_NAME") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segments" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                        Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnSegments" runat="server" CssClass="btn" Text="Segments" OnClick="btnSegments_Click"
                                TabIndex="25" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnSegments" runat="server" CssClass="btn" Text="Segments" OnClick="btnSegments_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnSegments" runat="server" CssClass="btn" Text="Segments" OnClick="btnSegments_Click"
                                TabIndex="25" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 1">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment1" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment1" Width="180px" runat="server" Text='<%# Eval("SEGMENT_1_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 2">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment2" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment2" Width="180px" runat="server" Text='<%# Eval("SEGMENT_2_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 3">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment3" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment3" Width="180px" runat="server" Text='<%# Eval("SEGMENT_3_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 4">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment4" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment4" Width="180px" runat="server" Text='<%# Eval("SEGMENT_4_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 5">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment5" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment5" Width="180px" runat="server" Text='<%# Eval("SEGMENT_5_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Segment 6">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlGSegment6" runat="server" CssClass="ddlStype" Width="180px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlGSegment_SelectedIndexChanged"
                                TabIndex="8">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGSegment6" Width="180px" runat="server" Text='<%# Eval("SEGMENT_6_TEXT") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tax" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left"
                        Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnTax" runat="server" CssClass="btn" Text="Tax" TabIndex="26" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnTax" runat="server" CssClass="btn" Text="Tax" OnClick="btnTax_Click"
                                TabIndex="26" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnTax" runat="server" CssClass="btn" Text="Tax" OnClick="btnTax_Click"
                                TabIndex="26" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <%-- <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 550px; text-align: right" id="lblInvoiceAmount">
                Invoice Amount
            </div>
            <div class="divtxtBox" style="float: left; width: 100px">
                <asp:TextBox ID="txtInvoiceAmount" runat="server" TabIndex="32" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    Width="100px"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
            </div>
        </div>--%>
        <div class="divRowContainer" id="divTax" runat="server" visible="false">
            <div class="lblBox" style="float: left; width: 230px; text-align: right" id="lblTax">
                Tax
            </div>
            <div class="divtxtBox" style="float: left; width: 100px">
                <asp:TextBox ID="txtTax" runat="server" TabIndex="33" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                    Width="100px" Text="0"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars="." TargetControlID="txtTax" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblReason">
                Reason
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtReason" MaxLength="500" runat="server" TabIndex="34" TextMode="MultiLine"
                    CssClass=" txtBox" Width="200px"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblRejectedReason">
                Rejected Reason
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtRejectedReason" MaxLength="200" runat="server" TabIndex="35"
                    TextMode="MultiLine" CssClass=" txtBox" Width="200px"></asp:TextBox>
            </div>
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <asp:HiddenField ID="hf_Temp_RefNo" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="90%" Height="75%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="98%">
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment1" runat="server" Text="Segment 1" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment1" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                    TabIndex="36" OnSelectedIndexChanged="ddlSegment1_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment2" runat="server" Text="Segment 2" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment2" runat="server" CssClass="ddlStype" Enabled="false"
                                    TabIndex="37" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment2_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment3" runat="server" Text="Segment 3" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment3" runat="server" CssClass="ddlStype" Enabled="false"
                                    TabIndex="38" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment3_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment4" runat="server" Text="Segment 4" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment4" runat="server" CssClass="ddlStype" Enabled="false"
                                    TabIndex="39" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment4_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment5" runat="server" Text="Segment 5" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment5" runat="server" CssClass="ddlStype" Enabled="false"
                                    TabIndex="40" AutoPostBack="True" OnSelectedIndexChanged="ddlSegment5_SelectedIndexChanged">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="ConfirmHeading">
                            <td>
                                <asp:Label ID="lblSegment6" runat="server" Text="Segment 6" CssClass="lblBox"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSegment6" runat="server" CssClass="ddlStype" Enabled="false"
                                    TabIndex="41" AutoPostBack="True">
                                    <asp:ListItem>--- Select ---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divTaxPopup">
            <asp:HiddenField ID="hfTaxdet" runat="server" />
            <cc2:ModalPopupExtender ID="mptTaxDet" runat="server" TargetControlID="hfTaxdet"
                PopupControlID="pnlTaxDet" CancelControlID="btnTaxClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlTaxDet" runat="server" BackColor="Violet" Width="50%" ScrollBars="Auto">
                <div class="ConfirmForm" style="height: 200px; overflow: scroll">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:HiddenField ID="hf_ItemNo" runat="server" Value="" />
                                <asp:HiddenField ID="hf_ReceitpNo" runat="server" Value="" />
                                <asp:HiddenField ID="hf_TotTaxAmt" runat="server" Value="" />
                                <asp:Button ID="btnTaxOk" runat="server" Text="OK" OnClick="btnTaxOk_Click" />
                                &nbsp; &nbsp;
                                <asp:Button ID="btnTaxClose" runat="server" Text="Close" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvTaxDet" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                                    DataKeyNames="INV_TAX_ID,TAX_ID,EntityNo,ItemNo" OnRowDataBound="gvTaxDet_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="TAX_NAME" HeaderText="Tax Name" />
                                        <asp:TemplateField HeaderText="Percentage">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTaxPercentage" MaxLength="13" runat="server" Text='<%# Eval("INV_TAX_PER") %>'
                                                    CssClass="EntryFont RequiredField txtBox" Width="80px"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="fteTaxPercentage" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtTaxPercentage" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTaxAmount" MaxLength="13" runat="server" Text='<%# Eval("INV_TAX_AMT") %>'
                                                    CssClass="EntryFont RequiredField txtBox" Width="80px"></asp:TextBox>
                                                <cc2:FilteredTextBoxExtender ID="fteTaxAmount" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars=".," TargetControlID="txtTaxAmount" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divTmpData">
            <asp:HiddenField ID="TempData" runat="server" />
            <cc2:ModalPopupExtender ID="mpeTempData" runat="server" TargetControlID="TempData"
                PopupControlID="pnlTempData" CancelControlID="btnTempClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlTempData" runat="server" BackColor="White" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm" style="background-color: White">
                    <div align="right" style="padding-right: 10px">
                        <asp:ImageButton ID="btnTempClose" runat="server" AlternateText="Close" ImageUrl="~/Images/close.png"
                            ToolTip="Close" Width="15px" Height="15px" />
                    </div>
                    <div class="divClear_10">
                    </div>
                    <asp:GridView ID="gvTempData" runat="server" DataKeyNames="INV_ID" CssClass="Grid"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="INV_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="INV_NUM" HeaderText="Number" />
                            <asp:BoundField DataField="VENDOR_ID" HeaderText="Vendor Id" />
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:Button ID="btnTmpDataselect" runat="server" OnClick="btnTmpDataselect_Click"
                                        Text="Select" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Deleted">
                                <ItemTemplate>
                                    <asp:Button ID="btnTmpDataDelete" runat="server" OnClick="btnTmpDataDelete_Click"
                                        Text="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
