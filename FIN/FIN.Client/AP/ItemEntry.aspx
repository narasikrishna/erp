﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ItemEntry.aspx.cs" Inherits="FIN.Client.AP.ItemEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemNumber">
                Item Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtItemNumber" TabIndex="1" MaxLength="50" CssClass="validate[required] RequiredField txtBox_en"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Item Number (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtItemNumberOL" TabIndex="2" MaxLength="240" CssClass="txtBox_ol"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemCategory">
                Item Category
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 160px">
                <asp:DropDownList ID="ddlItemCategory" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemName">
                Item Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 308px">
                <asp:TextBox ID="txtItemName" MaxLength="50" CssClass="validate[required] RequiredField txtBox_en" 
                    runat="server" TabIndex="4"></asp:TextBox>
                     
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 145px" id="lblItemDescription">
                Item Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 327px">
                <asp:TextBox ID="txtItemDescription" MaxLength="200" CssClass="txtBox_en" runat="server"
                    TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div8">
                Item Name (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 308px">
                <asp:TextBox ID="txtItemNameOL" MaxLength="240" CssClass="txtBox_ol" runat="server"
                    TabIndex="6"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 145px" id="Div7">
                Item Description (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 327px">
                <asp:TextBox ID="txtItemDescriptionOL" MaxLength="240" CssClass="txtBox_ol" runat="server"
                    TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemShortName">
                Item Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtItemShortName" CssClass="validate[required] RequiredField  txtBox_en" MaxLength="20"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                Short Name (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtItemShortNameOL" CssClass="txtBox_ol" runat="server" MaxLength="40" TabIndex="9"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemGroup">
                Item Group
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 158px">
                <asp:DropDownList ID="ddlItemGroup" runat="server" TabIndex="10" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemUOM">
                Item UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 152px">
                <asp:DropDownList ID="ddlItemUOM" runat="server" TabIndex="11" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblEffectiveStartDate">
                Effective Start Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtEffectiveStartDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="12"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtEffectiveStartDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblEffectiveEnddate">
                Effective End Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 154px">
                <asp:TextBox ID="txtEffectiveEnddate" CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]  txtBox"
                    runat="server" TabIndex="13"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEffectiveEnddate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblUnitPrice">
                Unit Price
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtUnitPrice" MaxLength="13" CssClass="txtBox_N" runat="server"
                    TabIndex="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtUnitPrice" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblReOrderQuantity">
                Re-Order Quantity
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtReOrderQuantity" CssClass="txtBox_N" runat="server" MaxLength="13"
                    TabIndex="15"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                    TargetControlID="txtReOrderQuantity" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Item Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 157px">
                <%--<asp:TextBox ID="txtServiceUOM" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="15"></asp:TextBox>--%>
                <asp:DropDownList ID="ddlitemtyp" AutoPostBack="True" OnSelectedIndexChanged="ddlitemtyp_SelectedIndexChanged"
                    runat="server" TabIndex="16" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSalesFlag">
                Sales Flag
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkSalesFlag" Checked="true" runat="server" TabIndex="17" AutoPostBack="true"
                    OnCheckedChanged="chkSalesFlag_CheckedChanged" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblPurchaseFlag">
                Purchase Flag
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkPurchaseFlag" Checked="true" runat="server" TabIndex="18" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblServiceDuration">
                Service Duration
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 153px">
                <asp:TextBox ID="txtServiceDuration" CssClass="txtBox_N" runat="server" MaxLength="15" 
                    TabIndex="19"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtServiceDuration" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblServiceUOM">
                Service Flag
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkServiceflag" Checked="true" runat="server" TabIndex="20" 
                    AutoPostBack="True" oncheckedchanged="chkServiceflag_CheckedChanged" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 152px" id="lblItemLotControl">
                Service UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlServiceUOM" runat="server" TabIndex="21" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 146px" id="lblItemSerialControl">
                Item Lot Control
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkItemLotControl" Checked="true" runat="server" TabIndex="22" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Item Serial Control
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkItemSerialControl" Checked="true" runat="server" TabIndex="23" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 152px" id="Div4">
                Cost of Goods Sold
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlCostofgoodssold" runat="server" TabIndex="24" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                Revenue Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlRevAcct" runat="server" TabIndex="25" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div10">
                Material Overhead Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlmatovrhdacct" runat="server" TabIndex="26" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 156px" id="Div1">
                Material Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlMetAcct" runat="server" TabIndex="27" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" TabIndex="28" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <h1 class="lblBox LNOrient" style="font-weight: bold; font-size: 14px;">
                Dimension Details</h1>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblWeight">
                Weight
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtWeight" CssClass="txtBox_N" runat="server" MaxLength="10" TabIndex="29"
                    AutoPostBack="True" OnTextChanged="txtWeight_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtWeight" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblWeightUOM">
                Weight UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlWeightUOM" runat="server" Style="height: 22px" TabIndex="30"
                    CssClass="ddlStype" AutoPostBack="true" OnSelectedIndexChanged="ddlWeightUOM_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIdentifier1">
                Additional Identifier 1
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAdditionalIdenti1" CssClass="txtBox" runat="server" MaxLength="50"
                    TabIndex="31"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblLength">
                Length
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLength" CssClass="txtBox_N" runat="server" MaxLength="10" TabIndex="32"
                    AutoPostBack="True" OnTextChanged="txtLength_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtLength" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblLengthUOM">
                Length UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlLengthUOM" runat="server" TabIndex="33" CssClass="ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlLengthUOM_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIdentifier2">
                Additional Identifier 2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAdditionalIdenti2" CssClass="txtBox" runat="server" MaxLength="50"
                    TabIndex="34"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblArea">
                Area
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtArea" CssClass="txtBox_N" runat="server" TabIndex="35" MaxLength="10"
                    AutoPostBack="True" OnTextChanged="txtArea_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtArea" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAreaUOM">
                Area UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlAreaUOM" runat="server" TabIndex="36" CssClass="ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlAreaUOM_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIdentifier3">
                Additional Identifier 3
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAdditionalIdenti3" CssClass="txtBox" runat="server" MaxLength="50"
                    TabIndex="37"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblVolume">
                Volume
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtVolume" CssClass="txtBox_N" runat="server" TabIndex="38" MaxLength="10"
                    AutoPostBack="True" OnTextChanged="txtVolume_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtVolume" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblVolumeUOM">
                Volume UOM
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlVolumeUOM" runat="server" TabIndex="39" CssClass="ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlVolumeUOM_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblAdditionalIdentifier4">
                Additional Identifier 4
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAdditionalIdenti4" CssClass="txtBox" runat="server" MaxLength="50"
                    TabIndex="40"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="41" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="42" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="43" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="44" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
