﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class DraftRFQEntry : PageBase
    {
        PO_TMP_RFQ_HDR PO_TMP_RFQ_HDR = new PO_TMP_RFQ_HDR();
        PO_TMP_RFQ_DTL PO_TMP_RFQ_DTL = new PO_TMP_RFQ_DTL();
        PO_TMP_RFQ_SUPPLIER_MAPPING PO_TMP_RFQ_SUPPLIER_MAPPING = new PO_TMP_RFQ_SUPPLIER_MAPPING();
        PO_TMP_RFQ_REQ_MAPPING PO_TMP_RFQ_REQ_MAPPING = new PO_TMP_RFQ_REQ_MAPPING();

        DataTable dtGridData = new DataTable();
        DataTable dtREQData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            DataTable dtDropDownData = new DataTable();
            string idVal = string.Empty;
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrderReq_DAL.getPOReqData()).Tables[0];

            if (Master.Mode == FINAppConstants.Add)
            {
                if (dtDropDownData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDropDownData.Rows.Count; i++)
                    {
                        idVal = DBMethod.GetStringValue(DraftRFQ_DAL.GetExistingREQData(dtDropDownData.Rows[i]["PO_REQ_ID"].ToString()));
                        if (idVal != string.Empty && idVal != "0")
                        {
                            dtDropDownData.Rows[i].Delete();
                        }
                    }
                    dtDropDownData.AcceptChanges();
                }
            }
            chkReqNumber.DataSource = dtDropDownData;
            chkReqNumber.DataTextField = "PO_REQ_DESC";
            chkReqNumber.DataValueField = "PO_REQ_ID";
            chkReqNumber.DataBind();

            Lookup_BLL.GetLookUpValues(ref ddlStatus, "RFQ_STATUS");
            if (ddlStatus.Items.Count >= 1)
            {
                ddlStatus.SelectedIndex = 2;
            }
            Currency_BLL.getCurrencyDetails(ref ddlCurrency);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                txtRFQDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    PO_TMP_RFQ_HDR obj_PO_TMP_RFQ_HDR = new PO_TMP_RFQ_HDR();
                    using (IRepository<PO_TMP_RFQ_HDR> userCtx = new DataRepository<PO_TMP_RFQ_HDR>())
                    {
                        obj_PO_TMP_RFQ_HDR = userCtx.Find(r =>
                            (r.TMP_RFQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    btnProcess.Enabled = false;
                    btnGenerate.Enabled = false;
                    chkReqNumber.Enabled = false;

                    PO_TMP_RFQ_HDR = obj_PO_TMP_RFQ_HDR;
                    EntityData = PO_TMP_RFQ_HDR;


                    txtRFQNumber.Text = PO_TMP_RFQ_HDR.TMP_RFQ_NUM;
                    if (PO_TMP_RFQ_HDR.TMP_RFQ_DT != null)
                    {
                        txtRFQDate.Text = DBMethod.ConvertDateToString(PO_TMP_RFQ_HDR.TMP_RFQ_DT.ToString());
                    }
                    if (PO_TMP_RFQ_HDR.TMP_RFQ_DUE_DT != null)
                    {
                        txtRFQDueDate.Text = DBMethod.ConvertDateToString(PO_TMP_RFQ_HDR.TMP_RFQ_DUE_DT.ToString());
                    }
                    txtRemarks.Text = PO_TMP_RFQ_HDR.TMP_RFQ_REMARKS;
                    if (PO_TMP_RFQ_HDR.TMP_RFQ_CURRENCY != null)
                    {
                        ddlCurrency.SelectedValue = PO_TMP_RFQ_HDR.TMP_RFQ_CURRENCY.ToString();
                    }
                    if (PO_TMP_RFQ_HDR.TMP_RFQ_STATUS != null)
                    {
                        ddlStatus.SelectedValue = PO_TMP_RFQ_HDR.TMP_RFQ_STATUS.ToString();
                    }

                    dtREQData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetREQData(Master.StrRecordId)).Tables[0];
                    if (dtREQData.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtREQData.Rows.Count; j++)
                        {
                            if (chkReqNumber.Items.Count > 0)
                            {
                                for (int k = 0; k < chkReqNumber.Items.Count; k++)
                                {
                                    if (dtREQData.Rows[j]["PO_REQ_ID"].ToString() != string.Empty && chkReqNumber.Items[k].Value.ToString() != string.Empty)
                                    {
                                        if (dtREQData.Rows[j]["PO_REQ_ID"].ToString() == chkReqNumber.Items[k].Value.ToString())
                                        {
                                            chkReqNumber.Items[k].Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //  DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;
                DropDownList ddlSupplier = tmpgvr.FindControl("ddlSupplier") as DropDownList;

                //   Item_BLL.GetItemName(ref ddlItemName);
                UOMMasters_BLL.GetUOMDetails(ref ddlUOM);
                Supplier_BLL.GetSupplierName(ref ddlSupplier);

                if (gvData.EditIndex >= 0)
                {
                    // ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                    ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_REQ_UOM].ToString();
                    ddlSupplier.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_REQ_UOM].ToString();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            // TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtPOAmount = gvr.FindControl("txtPOAmount") as TextBox;

            DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
            // DropDownList ddlAmount = gvr.FindControl("ddlAmount") as DropDownList;
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PO_HEADER_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = txtLineNo;
            slControls[1] = ddlItemName;
            // slControls[2] = txtDescription;
            slControls[2] = txtQuantity;
            slControls[3] = ddlUOM;
            slControls[4] = ddlUnitPrice;
            //slControls[6] = txtPOAmount;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["UOM_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + "";
            //string strMessage = "Line No ~ Item Name ~ Quantity ~ UOM ~ Unit Price";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            // txtPOTotalAmount.Text = PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtPOAmount.Text));


            drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text.ToString();
            drList[FINColumnConstants.PO_DESCRIPTION] = txtDescription.Text;
            drList[FINColumnConstants.PO_QUANTITY] = txtQuantity.Text;
            drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            drList[FINColumnConstants.UOM_CODE] = ddlUOM.SelectedItem.Text.ToString();
            drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = ddlUnitPrice.SelectedValue;

            //drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            //drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_DESCRIPTION] = txtDescription.Text;
            //drList[FINColumnConstants.PO_QUANTITY] = txtQuantity.Text;
            //drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = txtUnitPrice.Text;
            drList["PO_line_Amount"] = txtPOAmount.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlUOM = e.Row.FindControl("ddlUOM") as DropDownList;
                //DropDownList ddlSupplier = e.Row.FindControl("ddlSupplier") as DropDownList;
                DataTable dtDropDownData = new DataTable();
                CheckBoxList chkSupplier = e.Row.FindControl("chkSupplier") as CheckBoxList;


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    UOMMasters_BLL.GetUOMDetails(ref ddlUOM);
                    //Supplier_BLL.GetSupplierName(ref ddlSupplier);

                    dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];

                    ddlUOM.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.UOM_ID].ToString();
                    //ddlSupplier.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.VENDOR_ID].ToString();

                    chkSupplier.DataSource = dtDropDownData;
                    chkSupplier.DataTextField = "VENDOR_NAME";
                    chkSupplier.DataValueField = "VENDOR_ID";
                    chkSupplier.DataBind();

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        if (gvData.DataKeys[e.Row.RowIndex].Values["TMP_RFQ_DTL_ID"].ToString() != "0" && gvData.DataKeys[e.Row.RowIndex].Values["TMP_RFQ_DTL_ID"].ToString() != string.Empty)
                        {
                            dtREQData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQSupplierDtls(gvData.DataKeys[e.Row.RowIndex].Values["TMP_RFQ_DTL_ID"].ToString())).Tables[0];
                            if (dtREQData.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtREQData.Rows.Count; j++)
                                {
                                    if (chkSupplier.Items.Count > 0)
                                    {
                                        for (int k = 0; k < chkSupplier.Items.Count; k++)
                                        {
                                            if (dtREQData.Rows[j]["vendor_id"].ToString() != string.Empty && chkSupplier.Items[k].Value.ToString() != string.Empty)
                                            {
                                                if (dtREQData.Rows[j]["vendor_id"].ToString() == chkSupplier.Items[k].Value.ToString())
                                                {
                                                    chkSupplier.Items[k].Selected = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }



                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (txtRFQDate.Text != string.Empty && txtRFQDueDate.Text != string.Empty)
                {
                    if (DBMethod.ConvertStringToDate(txtRFQDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtRFQDueDate.Text.ToString()))
                    {
                        ErrorCollection.Add("RFQdate", "RFQ Due date should be greater than the RFQ Date");
                        return;
                    }
                }

                AssignToBE();

                if (savedBool)
                {
                    VMVServices.Web.Utils.RecordMsg = FINMessageConstatns.PO_Number + Session["PO_NUM"];
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    PO_TMP_RFQ_HDR = (PO_TMP_RFQ_HDR)EntityData;
                }

                PO_TMP_RFQ_HDR.TMP_RFQ_NUM = txtRFQNumber.Text;

                if (txtRFQDate.Text != string.Empty)
                {
                    PO_TMP_RFQ_HDR.TMP_RFQ_DT = DBMethod.ConvertStringToDate(txtRFQDate.Text.ToString());
                }
                if (txtRFQDueDate.Text != string.Empty)
                {
                    PO_TMP_RFQ_HDR.TMP_RFQ_DUE_DT = DBMethod.ConvertStringToDate(txtRFQDueDate.Text.ToString());
                }
                PO_TMP_RFQ_HDR.TMP_RFQ_REMARKS = txtRemarks.Text;
                PO_TMP_RFQ_HDR.TMP_RFQ_CURRENCY = ddlCurrency.SelectedValue.ToString();
                PO_TMP_RFQ_HDR.TMP_RFQ_STATUS = ddlStatus.SelectedValue.ToString();

                PO_TMP_RFQ_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                PO_TMP_RFQ_HDR.TMP_RFQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    PO_TMP_RFQ_HDR.MODIFIED_BY = this.LoggedUserName;
                    PO_TMP_RFQ_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    PO_TMP_RFQ_HDR.TMP_RFQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_M.ToString(), false, true);
                    PO_TMP_RFQ_HDR.TMP_RFQ_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_N.ToString(), false, true);

                    PO_TMP_RFQ_HDR.CREATED_BY = this.LoggedUserName;
                    PO_TMP_RFQ_HDR.CREATED_DATE = DateTime.Today;
                }

                PO_TMP_RFQ_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, PO_TMP_RFQ_HDR.TMP_RFQ_ID);

                //Save Detail Table

                var tmpChildEntity = new List<Tuple<object, string>>();
                var tmpChildEntity1 = new List<Tuple<object, string>>();
                var tmpChildEntity2 = new List<Tuple<object, string>>();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Item ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (gvData.Rows.Count > 0)
                {
                    int selectedCount = 0;
                    int rowcount = 0;
                    for (int k = 0; k < gvData.Rows.Count; k++)
                    {
                        rowcount = (rowcount + 1);
                        DropDownList ddlUOM = gvData.Rows[k].FindControl("ddlUOM") as DropDownList;

                        if (ddlUOM.SelectedValue == string.Empty)
                        {
                            ErrorCollection.Remove("uom error");
                            ErrorCollection.Add("uom error", "UOM cannot be empty in row " + rowcount);
                            return;
                        }

                        CheckBoxList chkSupplier = gvData.Rows[k].FindControl("chkSupplier") as CheckBoxList;
                      
                        selectedCount = 0;

                        if (chkSupplier.Items.Count > 0)
                        {
                            for (int iLoop = 0; iLoop < chkSupplier.Items.Count; iLoop++)
                            {
                                if (chkSupplier.Items[iLoop].Selected == true)
                                {
                                    selectedCount = selectedCount + 1;
                                }
                            }
                        }
                        if (selectedCount == 0)
                        {
                            ErrorCollection.Remove("checked");
                            ErrorCollection.Add("checked", "Please select the supplier in row " + rowcount);
                            return;
                        }
                    }

                }

                if (gvData.Rows.Count > 0)
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {
                        PO_TMP_RFQ_DTL = new PO_TMP_RFQ_DTL();

                        if (gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString() != string.Empty)
                        {
                            PO_TMP_RFQ_DTL obj_PO_TMP_RFQ_DTL = new PO_TMP_RFQ_DTL();
                            using (IRepository<PO_TMP_RFQ_DTL> userCtx = new DataRepository<PO_TMP_RFQ_DTL>())
                            {
                                obj_PO_TMP_RFQ_DTL = userCtx.Find(r =>
                                    (r.TMP_RFQ_DTL_ID == gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                            PO_TMP_RFQ_DTL = obj_PO_TMP_RFQ_DTL;
                        }


                        PO_TMP_RFQ_DTL.TMP_RFQ_ID = PO_TMP_RFQ_HDR.TMP_RFQ_ID;
                        PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_ID = gvData.DataKeys[i].Values["tmp_rfq_item_id"].ToString();

                        DropDownList ddlUOM = gvData.Rows[i].FindControl("ddlUOM") as DropDownList;
                        if (ddlUOM.SelectedValue != string.Empty)
                        {
                            PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_UOM = ddlUOM.SelectedValue.ToString();
                        }
                        else
                        {
                            PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_UOM = "0";
                        }
                        //TextBox txtUnitPrice = gvData.Rows[i].FindControl("txtUnitPrice") as TextBox;
                        //PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_COST = CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text);
                        PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_COST = 0;
                        TextBox txtPORemarks = gvData.Rows[i].FindControl("txtPORemarks") as TextBox;
                        PO_TMP_RFQ_DTL.TMP_RFQ_COMMENTS = (txtPORemarks.Text);

                        TextBox txtItemType = gvData.Rows[i].FindControl("txtItemType") as TextBox;
                        PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_TYPE = (txtItemType.Text);

                        TextBox txtQuantity = gvData.Rows[i].FindControl("txtQuantity") as TextBox;
                        PO_TMP_RFQ_DTL.TMP_RFQ_ITEM_QTY = CommonUtils.ConvertStringToDecimal(txtQuantity.Text);


                        PO_TMP_RFQ_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        PO_TMP_RFQ_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        {
                            PO_TMP_RFQ_DTL.TMP_RFQ_DTL_ID = gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString();
                            PO_TMP_RFQ_DTL.MODIFIED_BY = this.LoggedUserName;
                            PO_TMP_RFQ_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(PO_TMP_RFQ_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            PO_TMP_RFQ_DTL.TMP_RFQ_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_D.ToString(), false, true);
                            PO_TMP_RFQ_DTL.CREATED_BY = this.LoggedUserName;
                            PO_TMP_RFQ_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(PO_TMP_RFQ_DTL, FINAppConstants.Add));
                        }


                        //start supplier mapping

                        DropDownList ddlSupplier = gvData.Rows[i].FindControl("ddlSupplier") as DropDownList;
                        CheckBoxList chkSupplier = gvData.Rows[i].FindControl("chkSupplier") as CheckBoxList;

                        if (chkSupplier.Items.Count > 0)
                        {
                            for (int iLoop = 0; iLoop < chkSupplier.Items.Count; iLoop++)
                            {
                                if (chkSupplier.Items[iLoop].Selected == true)
                                {
                                    PO_TMP_RFQ_SUPPLIER_MAPPING = new PO_TMP_RFQ_SUPPLIER_MAPPING();

                                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                    {
                                        if (gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString() != string.Empty)
                                        {
                                            PO_TMP_RFQ_SUPPLIER_MAPPING obj_PO_TMP_RFQ_SUPPLIER_MAPPING = new PO_TMP_RFQ_SUPPLIER_MAPPING();
                                            using (IRepository<PO_TMP_RFQ_SUPPLIER_MAPPING> userCtx = new DataRepository<PO_TMP_RFQ_SUPPLIER_MAPPING>())
                                            {
                                                obj_PO_TMP_RFQ_SUPPLIER_MAPPING = userCtx.Find(r =>
                                                    (r.TMP_RFQ_DTL_ID == gvData.DataKeys[i].Values["TMP_RFQ_DTL_ID"].ToString() && r.TMP_RFQ_SUPPLIER_ID == chkSupplier.Items[iLoop].Value.ToString())
                                                    ).SingleOrDefault();
                                            }
                                            if (obj_PO_TMP_RFQ_SUPPLIER_MAPPING != null)
                                            {
                                                PO_TMP_RFQ_SUPPLIER_MAPPING = obj_PO_TMP_RFQ_SUPPLIER_MAPPING;
                                            }
                                        }
                                    }


                                    PO_TMP_RFQ_SUPPLIER_MAPPING.TMP_RFQ_DTL_ID = PO_TMP_RFQ_DTL.TMP_RFQ_DTL_ID;

                                    PO_TMP_RFQ_SUPPLIER_MAPPING.TMP_RFQ_SUPPLIER_ID = chkSupplier.Items[iLoop].Value.ToString();
                                    PO_TMP_RFQ_SUPPLIER_MAPPING.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                                    PO_TMP_RFQ_SUPPLIER_MAPPING.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                    {
                                        if (PO_TMP_RFQ_SUPPLIER_MAPPING.TMP_RFQ_SM_ID == null)
                                        {
                                            PO_TMP_RFQ_SUPPLIER_MAPPING.TMP_RFQ_SM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_S.ToString(), false, true);
                                            PO_TMP_RFQ_SUPPLIER_MAPPING.CREATED_BY = this.LoggedUserName;
                                            PO_TMP_RFQ_SUPPLIER_MAPPING.CREATED_DATE = DateTime.Today;
                                            tmpChildEntity1.Add(new Tuple<object, string>(PO_TMP_RFQ_SUPPLIER_MAPPING, FINAppConstants.Add));
                                        }
                                        else
                                        {
                                            PO_TMP_RFQ_SUPPLIER_MAPPING.MODIFIED_BY = this.LoggedUserName;
                                            PO_TMP_RFQ_SUPPLIER_MAPPING.MODIFIED_DATE = DateTime.Today;
                                            tmpChildEntity1.Add(new Tuple<object, string>(PO_TMP_RFQ_SUPPLIER_MAPPING, FINAppConstants.Update));

                                        }
                                    }
                                    else
                                    {
                                        PO_TMP_RFQ_SUPPLIER_MAPPING.TMP_RFQ_SM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_S.ToString(), false, true);
                                        PO_TMP_RFQ_SUPPLIER_MAPPING.CREATED_BY = this.LoggedUserName;
                                        PO_TMP_RFQ_SUPPLIER_MAPPING.CREATED_DATE = DateTime.Today;
                                        tmpChildEntity1.Add(new Tuple<object, string>(PO_TMP_RFQ_SUPPLIER_MAPPING, FINAppConstants.Add));
                                    }
                                }
                            }
                        }
                        //end supplier

                    }
                }

                //Save req Detail Table               

                if (chkReqNumber.Items.Count > 0)
                {
                    for (int iLoop = 0; iLoop < chkReqNumber.Items.Count; iLoop++)
                    {
                        if (chkReqNumber.Items[iLoop].Selected == true)
                        {
                            PO_TMP_RFQ_REQ_MAPPING = new PO_TMP_RFQ_REQ_MAPPING();

                            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                            {
                                PO_TMP_RFQ_REQ_MAPPING obj_PO_TMP_RFQ_REQ_MAPPING = new PO_TMP_RFQ_REQ_MAPPING();
                                using (IRepository<PO_TMP_RFQ_REQ_MAPPING> userCtx = new DataRepository<PO_TMP_RFQ_REQ_MAPPING>())
                                {
                                    obj_PO_TMP_RFQ_REQ_MAPPING = userCtx.Find(r =>
                                        (r.TMP_REQ_ID == chkReqNumber.Items[iLoop].Value.ToString() && r.TMP_RFQ_ID == PO_TMP_RFQ_HDR.TMP_RFQ_ID.ToString())
                                        ).SingleOrDefault();
                                }
                                PO_TMP_RFQ_REQ_MAPPING = obj_PO_TMP_RFQ_REQ_MAPPING;
                            }


                            PO_TMP_RFQ_REQ_MAPPING.TMP_REQ_ID = chkReqNumber.Items[iLoop].Value;
                            PO_TMP_RFQ_REQ_MAPPING.TMP_RFQ_ID = PO_TMP_RFQ_HDR.TMP_RFQ_ID;

                            PO_TMP_RFQ_REQ_MAPPING.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                            PO_TMP_RFQ_REQ_MAPPING.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                            {
                                PO_TMP_RFQ_REQ_MAPPING.MODIFIED_BY = this.LoggedUserName;
                                PO_TMP_RFQ_REQ_MAPPING.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity2.Add(new Tuple<object, string>(PO_TMP_RFQ_REQ_MAPPING, FINAppConstants.Update));
                            }
                            else
                            {
                                PO_TMP_RFQ_REQ_MAPPING.TMP_RFQ_REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_025_R.ToString(), false, true);
                                PO_TMP_RFQ_REQ_MAPPING.CREATED_BY = this.LoggedUserName;
                                PO_TMP_RFQ_REQ_MAPPING.CREATED_DATE = DateTime.Today;
                                tmpChildEntity2.Add(new Tuple<object, string>(PO_TMP_RFQ_REQ_MAPPING, FINAppConstants.Add));
                            }
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveFourEntity<PO_TMP_RFQ_HDR, PO_TMP_RFQ_DTL, PO_TMP_RFQ_SUPPLIER_MAPPING, PO_TMP_RFQ_REQ_MAPPING>(PO_TMP_RFQ_HDR, tmpChildEntity, PO_TMP_RFQ_DTL, tmpChildEntity1, PO_TMP_RFQ_SUPPLIER_MAPPING, tmpChildEntity2, PO_TMP_RFQ_REQ_MAPPING);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveFourEntity<PO_TMP_RFQ_HDR, PO_TMP_RFQ_DTL, PO_TMP_RFQ_SUPPLIER_MAPPING, PO_TMP_RFQ_REQ_MAPPING>(PO_TMP_RFQ_HDR, tmpChildEntity, PO_TMP_RFQ_DTL, tmpChildEntity1, PO_TMP_RFQ_SUPPLIER_MAPPING, tmpChildEntity2, PO_TMP_RFQ_REQ_MAPPING, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (PO_TMP_RFQ_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.DRAFT_RFQ_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_TMP_RFQ_HDR>(PO_TMP_RFQ_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlRequisitionNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                // dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails_BasedonReq(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                BindGrid(dtGridData);


                DataTable dtDesc = new DataTable();
                // dtDesc = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetRequsitionDesc(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                // lblDescription.Text = dtDesc.Rows[0]["PO_REQ_DESC"].ToString();

                if (gvData.Rows.Count > 0)
                {

                    ////GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                    ////Label lblUnitPrice = gvr.FindControl("lblUnitPrice") as Label;
                    ////Label lblQuantity = gvr.FindControl("lblQuantity") as Label;
                    ////Label lblAmount = gvr.FindControl("lblAmount") as Label;

                    ////if (lblUnitPrice != null && lblQuantity != null)
                    ////{
                    ////    lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                    ////    if (txtPOTotalAmount.Text.Trim() == string.Empty)
                    ////    {
                    ////        txtPOTotalAmount.Text = "0";
                    ////    }
                    ////    if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                    ////    {
                    ////        txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                    ////    }
                    ////}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;
                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;

                txtUnitPrice.Text = ddlUnitPrice.SelectedValue;

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();

                            //if (txtPOTotalAmount.Text.Trim() == string.Empty)
                            //{
                            //    txtPOTotalAmount.Text = "0";
                            //}
                            //if ( CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) >= 0)
                            //{
                            //    double editpoamt;
                            //    if (Session["Poeditvalue"] != null)
                            //    {
                            //        editpoamt = double.Parse(Session["Poeditvalue"].ToString());
                            //        Session["Poeditvalue"] = null;
                            //    }
                            //    else
                            //    {
                            //        editpoamt = 0;
                            //    }
                            //        txtPOTotalAmount.Text = ((CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) - CommonUtils.ConvertStringToDecimal(editpoamt.ToString())) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();

                            //}
                        }
                    }
                }

                //if (lblUnitPrice != null && lblQuantity != null)
                //{
                //    if (lblUnitPrice.Text.Trim() != string.Empty && lblQuantity.Text.Trim() != string.Empty)
                //    {
                //        if (int.Parse(lblUnitPrice.Text.ToString()) > 0 && int.Parse(lblQuantity.Text.ToString()) > 0)
                //        {
                //            lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                //            if (txtPOTotalAmount.Text.Trim() == string.Empty)
                //            {
                //                txtPOTotalAmount.Text = "0";
                //            }
                //            if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                //            {
                //                // txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                //            }
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
                TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
                DataTable dtunitprice = new DataTable();


                dtunitprice = DBMethod.ExecuteQuery(PurchaseOrderReq_DAL.getPOReqItemData(ddlItemName.SelectedValue)).Tables[0];
                txtUnitPrice.Text = dtunitprice.Rows[0]["po_item_unit_price"].ToString();

                UOMMasters_BLL.getUOMBasedItem(ref ddlUOM, ddlItemName.SelectedValue);

                ddlUOM.SelectedItem.Text = dtunitprice.Rows[0]["uom_code"].ToString();
                txtQuantity.Text = dtunitprice.Rows[0]["po_quantity"].ToString();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlSupplierPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlSupplierSite, ddlSupplierName.SelectedValue);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                string selectedReq = string.Empty;

                if (chkReqNumber.Items.Count > 0)
                {
                    for (int iLoop = 0; iLoop < chkReqNumber.Items.Count; iLoop++)
                    {
                        if (chkReqNumber.Items[iLoop].Selected == true)
                        {
                            selectedReq += "'" + chkReqNumber.Items[iLoop].Value + "',";// "''" + chkReqNumber.Items[iLoop].Value + "'','";
                        }
                    }
                    if (selectedReq != string.Empty)
                    {
                        int index = selectedReq.LastIndexOf(',');
                        selectedReq = selectedReq.Remove(index, 1);

                        dtGridData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetRFQDetails(selectedReq.Trim())).Tables[0];
                        BindGrid(dtGridData);
                    }
                    else
                    {
                        ErrorCollection.Remove("adfd");
                        ErrorCollection.Add("adfd", "Please select the Requisition Number");

                        if ((txtRFQDate.Text.ToString()) != string.Empty && (txtRFQDueDate.Text.ToString()) != string.Empty)
                        {
                            ErrorCollection.Remove("datevalidate");
                            if (DBMethod.ConvertStringToDate(txtRFQDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtRFQDueDate.Text.ToString()))
                            {
                                ErrorCollection.Add("datevalidate", "RFQ Due Date should not less than the RFQ Date");
                            }
                        }
                    }
                }
                else
                {
                    ErrorCollection.Remove("adfd");
                    ErrorCollection.Add("adfd", "Please select the Requisition Number");

                    if ((txtRFQDate.Text.ToString()) != string.Empty && (txtRFQDueDate.Text.ToString()) != string.Empty)
                    {
                        ErrorCollection.Remove("datevalidate");
                        if (DBMethod.ConvertStringToDate(txtRFQDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtRFQDueDate.Text.ToString()))
                        {
                            ErrorCollection.Add("datevalidate", "RFQ Due Date should not less than the RFQ Date");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}