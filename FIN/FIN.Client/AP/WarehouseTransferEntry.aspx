﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WarehouseTransferEntry.aspx.cs" Inherits="FIN.Client.AP.WarehouseTransferEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblWarehousefrom">
                Warehouse From
            </div>
            <div class="divtxtBox LNOrient" style="width: 205px">
                <asp:DropDownList ID="ddlWarehousefrom" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlWarehousefrom_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp;</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblWarehouseName">
                Warehouse Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtWarehouseName" runat="server" TabIndex="2" CssClass=" RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Convert to Asset
            </div>
            <div class="divChkbox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkasset" runat="server" TabIndex="3" AutoPostBack="True" OnCheckedChanged="chkasset_CheckedChanged" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="WHTO">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblWarehouseTO">
                Warehouse To
            </div>
            <div class="divtxtBox LNOrient" style=" width: 205px">
                <asp:DropDownList ID="ddlWHTO" runat="server" CssClass="validate[]  ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlWHTO_SelectedIndexChanged" TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp;</div>
            <div class="lblBox LNOrient" style=" width: 195px" id="lblWHname">
                Warehouse Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtWHname" runat="server" TabIndex="5" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentDate">
                Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 204px">
                <asp:TextBox ID="txtTransferDate" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1d" TargetControlID="txtTransferDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient" >
                &nbsp;</div>
            <div class="lblBox LNOrient" style="width: 195px" id="Div1">
                Transfer Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtTransferNumber" runat="server" TabIndex="7" CssClass=" txtBox"></asp:TextBox>
            </div>
            <%-- <div class="lblBox" style="float: left; width: 200px" id="lblReceiptNumber">
                Receipt Number
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlReceiptNumber"  runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="5" OnSelectedIndexChanged="ddlReceiptNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblItemNumber">
                Item Number
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlItemno" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlItemno_SelectedIndexChanged" TabIndex="6">
                </asp:DropDownList>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblEmployeeName">
                Transferred By
            </div>
            <div class="divtxtBox LNOrient" style="width: 205px">
                <asp:DropDownList ID="ddlTransferredBy" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
            <%-- <div class="colspace" style="float: left;">
                &nbsp</div>--%>
            <%--  <div class="lblBox" style="float: left; width: 195px" id="Div3">
                Transferred To
            </div>
            <div class="divtxtBox" style="float: left; width: 205px">
                <asp:DropDownList ID="ddlTransferredTo" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div2">
                Received By
            </div>
            <div class="divtxtBox LNOrient" style="width: 205px">
                <asp:DropDownList ID="ddlReceivedBy" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="9">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp;</div>
            <div class="lblBox LNOrient" style="width: 195px" id="Div4">
                Approved By
            </div>
            <div class="divtxtBox LNOrient" style=" width: 205px">
                <asp:DropDownList ID="ddlApprovedBy" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="10">
                </asp:DropDownList>
            </div>
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblitemname">
                Item Name
            </div>
            <div class="divtxtBox" style="float: left; width: 524px">
                <asp:TextBox ID="txtitemname" runat="server" TabIndex="7" CssClass=" RequiredField txtBox"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
                Reason
            </div>
            <div class="divtxtBox LNOrient" style=" width: 203px">
                <asp:TextBox ID="txtReason" runat="server" TabIndex="11" MaxLength="500" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp;</div>
            <div class="lblBox LNOrient" style="width: 197px" id="lblTotalQuantity">
                Total Quantity
            </div>
            <div class="divtxtBox LNOrient" style="width: 203px">
                <asp:TextBox ID="txtTotalQuantity" runat="server" MaxLength="10" TabIndex="12" CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer  LNOrient" >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="500px" DataKeyNames="INV_TRANS_DTL_ID,DELETED,ITEM_ID,CODE_ID,LOT_ID" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Item Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemno" TabIndex="13" runat="server" Width="270px" CssClass="validate[required] ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlItemno_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemno" TabIndex="13" AutoPostBack="True" Width="270px"
                                OnSelectedIndexChanged="ddlItemno_SelectedIndexChanged" runat="server" CssClass="RequiredField ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemNameNo" CssClass="adminFormFieldHeading" runat="server" Width="270px"
                                Text='<%# Eval("ITEM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lot Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLotNo" TabIndex="14" runat="server" Width="270px" CssClass="validate[required] ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlLotNo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLotNo" TabIndex="14" AutoPostBack="True" Width="270px" OnSelectedIndexChanged="ddlLotNo_SelectedIndexChanged"
                                runat="server" CssClass="RequiredField ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLotNo" CssClass="adminFormFieldHeading" runat="server" Width="270px"
                                Text='<%# Eval("LOT_NUMBER") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtquantity" Enabled="true" runat="server" Width="170px" CssClass="RequiredField txtBox_N"
                                TabIndex="15" Text='<%# Eval("INV_ITEM_QTY") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtquantity" Enabled="true" runat="server" Width="170px" CssClass=" RequiredField txtBox_N"
                                TabIndex="15"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" Enabled="true" runat="server" Width="170px" Text='<%# Eval("INV_ITEM_QTY") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAcccode" TabIndex="16" runat="server" Width="270px" CssClass="RequiredField ddlStype" >
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAcccode" TabIndex="16" Width="270px" runat="server" CssClass="RequiredField ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAcccode" CssClass="adminFormFieldHeading" runat="server" Width="270px"
                                Text='<%# Eval("CODE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="17" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="18" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="19" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="20" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="21" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="22" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="23" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="24" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="25" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
