﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;

namespace FIN.Client.AP
{
    public partial class SupplierSummary : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
      
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;

                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getSupplierwiseSummaryData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["INVOICE_AMT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INVOICE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INVOICE_AMT"))));
                    }
                    if (dtData.Rows[0]["PAYMENT_AMT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAYMENT_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAYMENT_AMT"))));
                    }
                    if (dtData.Rows[0]["BALANCE"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BALANCE"))));
                    }

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierSummary", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
       
        #endregion

        # region Grid Events
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    string vendorId = gvData.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView invPayment = (GridView)e.Row.FindControl("gvInvPayment");
                    DataTable dtInvPayData = new DataTable();
                    dtInvPayData = DBMethod.ExecuteQuery(InvoiceRegister_DAL.getSupplierSummaryData(vendorId)).Tables[0];

                    if (dtInvPayData.Rows.Count > 0)
                    {
                        if (dtInvPayData.Rows[0]["INVOICE_AMT"].ToString().Length > 0)
                        {
                            dtInvPayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INVOICE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INVOICE_AMT"))));
                        }
                        if (dtInvPayData.Rows[0]["PAYMENT_AMT"].ToString().Length > 0)
                        {
                            dtInvPayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAYMENT_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAYMENT_AMT"))));
                        }
                        dtInvPayData.AcceptChanges();
                    }

                    invPayment.DataSource = dtInvPayData;
                    invPayment.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
   
        #endregion

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();

                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {
                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getSupplierwiseSummaryData()).Tables[0];
                    BindGrid(dtData);
                }
                else
                {
                    ErrorCollection.Add("datddemsg", "From Date and To Date cannot be empty");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierwiseSummary", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        



        //protected void btnPrint_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        ReportFile = Master.ReportName;
        //        string draftRFQId = string.Empty;
        //        VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
        //        VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


        //        ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.RFQ_DAL.getQuotationReportData());
        //        htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
        //        ReportFormulaParameter = htHeadingParameters;
        //        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //        }
        //    }
        //}

       
    }
}