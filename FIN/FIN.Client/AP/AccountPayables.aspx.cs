﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace FIN.Client.AP
{
    public partial class AccountPayables : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
        string connectionStringOracle = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringOracle"].ToString();
        bool postedMsg = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayableATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    }
                    //if (dtData.Rows[0]["PAIDAMOUNT"].ToString().Length > 0)
                    //{
                    //    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAIDAMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAIDAMOUNT"))));
                    //}

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();

                //   Search();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindFilteredGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;


                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    }
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion


        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablePOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion


        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                //Create Connection
                //SqlConnection con = new SqlConnection(connectionStringSQL);
                ////string sqlQuery = " select ACCOUNTPAYABLEID,TYPE,VENDORID,AMOUNT,CURRENCYID,convert(nvarchar,DueDate,103) as DueDate,PAIDAMOUNT,STATUS,convert(nvarchar,STATUSDATE,103) as [STATUSDATE]";
                ////sqlQuery += " ,REMARKS_EN,REMARKS_AR,COMPONENTID,PAYMENTMETHOD,BANKID,PAYMENTNO,CHECKID,convert(nvarchar,PAYMENTDATE,103) as PAYMENTDATE ,MERGEDAPID,SPLITTEDAPID,CUSERID,convert(nvarchar,CDATE,103) as CDATE ";
                ////sqlQuery += " ,MDATE,CASE DisablePartiallyPaid WHEN 'TRUE' THEN '1' ELSE '0' END as DisablePartiallyPaid,convert(nvarchar,CANCELDATE,103) as CANCELDATE,convert(nvarchar,INVOICEDATE,103) as INVOICEDATE ,ACCOUNTPAYABLENUMBER";
                ////sqlQuery += " from accountpayable";

                //string sqlQuery = "select v.vendornumber, v.name_en, t.type, sc.description,  t.accountpayableid,convert(nvarchar,t.invoicedate,103) as invoicedate,convert(nvarchar,t.DueDate,103) as DueDate, t.amount";
                //sqlQuery += " from ACCOUNTPAYABLE t, dm_intf_vendor_mapping v, ssm_code_masters sc  where v.personid = t.vendorid";
                //sqlQuery += " and sc.code = to_char(t.type) and sc.parent_code='INV_TY'";


                //SqlCommand cmd = new SqlCommand(sqlQuery, con);

                //con.Open();
                //DataTable dtData = new DataTable();
                //SqlDataAdapter da = new SqlDataAdapter(cmd);

                //da.Fill(dtData);


                //using (OracleConnection connection = new OracleConnection())
                //{

                //    connection.ConnectionString = connectionStringOracle;
                //    connection.Open();
                //    OracleCommand command = connection.CreateCommand();
                //    string sql = "";

                //    for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                //    {
                //        sql += " insert into accountpayable Values('";
                //        sql += dtData.Rows[iLoop]["ACCOUNTPAYABLEID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["TYPE"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["VENDORID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["AMOUNT"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["CURRENCYID"].ToString() + "',";
                //        sql += "to_date('" + dtData.Rows[iLoop]["DUEDATE"].ToString() + "','dd/MM/yyyy'),'";
                //        sql += dtData.Rows[iLoop]["PAIDAMOUNT"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["STATUS"].ToString() + "',";
                //        sql += "to_date('" + dtData.Rows[iLoop]["STATUSDATE"].ToString() + "','dd/MM/yyyy'),'";
                //        sql += dtData.Rows[iLoop]["REMARKS_EN"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["REMARKS_AR"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["COMPONENTID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["PAYMENTMETHOD"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["BANKID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["PAYMENTNO"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["CHECKID"].ToString() + "',";
                //        sql += "to_date('" + dtData.Rows[iLoop]["PAYMENTDATE"].ToString() + "','dd/MM/yyyy'),'";
                //        sql += dtData.Rows[iLoop]["MERGEDAPID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["SPLITTEDAPID"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["CUSERID"].ToString() + "',";
                //        sql += "to_date('" + dtData.Rows[iLoop]["CDATE"].ToString() + "','dd/MM/yyyy'),'";
                //        sql += dtData.Rows[iLoop]["MDATE"].ToString() + "','";
                //        sql += dtData.Rows[iLoop]["DISABLEPARTIALLYPAID"].ToString() + "',";
                //        sql += "to_date('" + dtData.Rows[iLoop]["CANCELDATE"].ToString() + "','dd/MM/yyyy'),";
                //        sql += "to_date('" + dtData.Rows[iLoop]["INVOICEDATE"].ToString() + "','dd/MM/yyyy'),'";
                //        sql += dtData.Rows[iLoop]["ACCOUNTPAYABLENUMBER"].ToString() + "'";
                //        sql += ")";

                //        command.CommandText = sql;
                //        command.ExecuteNonQuery();
                //        connection.Close();
                //    }

                //}


                ////Close Connection  
                //con.Close();
                //da.Dispose();



                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        FINSP.GetSP_GL_Posting(gvData.DataKeys[iLoop].Values["ACCOUNTPAYABLEID"].ToString(), "SSM_029");
                        postedMsg = true;
                    }
                }

                if (postedMsg)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

                DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                BindGrid(dtData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayables", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtVendorNum = gvr.FindControl("txtVendorNum") as TextBox;
                TextBox txtVendorName = gvr.FindControl("txtVendorName") as TextBox;
                TextBox txtdescription = gvr.FindControl("txtdescription") as TextBox;
                TextBox txtInvoiceNumber = gvr.FindControl("txtInvoiceNumber") as TextBox;
                TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
                TextBox txtDueDate = gvr.FindControl("txtDueDate") as TextBox;
                TextBox txtAMOUNT = gvr.FindControl("txtAMOUNT") as TextBox;

                if (txtVendorNum.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtVendorName.Text.Length > 0)
                {
                    txtVendorNum.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtdescription.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtInvoiceNumber.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtInvoiceDate.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtDueDate.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtAMOUNT.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                }
                DataTable dt = new DataTable();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt = (DataTable)Session[FINSessionConstants.GridData];
                    if (dt.Rows.Count > 0)
                    {
                        var var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString() != string.Empty);

                        if (txtVendorNum.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["vendornumber"].ToString().ToUpper().StartsWith(txtVendorNum.Text.ToUpper()));
                        }
                        else if (txtVendorName.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["name_en"].ToString().ToUpper().StartsWith(txtVendorName.Text.ToUpper()));
                        }
                        else if (txtdescription.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["description"].ToString().ToUpper().StartsWith(txtdescription.Text.ToUpper()));
                        }
                        else if (txtInvoiceNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString().ToUpper().StartsWith(txtInvoiceNumber.Text.ToUpper()));
                        }
                        else if (txtInvoiceDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["INVOICEDATE"].ToString().StartsWith(txtInvoiceDate.Text));
                        }
                        else if (txtDueDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["DueDate"].ToString().StartsWith(txtDueDate.Text));
                        }
                        else if (txtAMOUNT.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["AMOUNT"].ToString().StartsWith(txtAMOUNT.Text));
                        }
                        if (var_List.Any())
                        {
                            dt = System.Data.DataTableExtensions.CopyToDataTable(var_List);
                            BindFilteredGrid(dt);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string maxIdAP = "";
            string maxIdSCD = "";
            string maxIdGSCP = "";
            string maxPersonIDMNet = "";
            try
            {
                //AccountPayable
                //Get the MAX Id
                using (OracleConnection connMax = new OracleConnection())
                {
                    connMax.ConnectionString = connectionStringOracle;
                    connMax.Open();
                    OracleCommand cmdMax = connMax.CreateCommand();
                    string sql = "";

                    sql = "Select max(ap.ACCOUNTPAYABLEID) maxValue from accountpayable ap where ap.invoicedate <=to_date('" + txtFromDate.Text + "','dd/MM/yyyy')";
                    DataTable dtDataMax = new DataTable();
                    OracleDataAdapter oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[0]["maxValue"].ToString().Length > 0)
                        {
                            maxIdAP = dtDataMax.Rows[0]["maxValue"].ToString();
                        }
                    }

                    //ServiceContractPayment - Get the MAX Id
                    sql = "select max(PaymentId) as maxIdSCD from ServiceContractPayment";
                    DataTable dtDataMaxSCD = new DataTable();
                    OracleDataAdapter oaSCD = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaSCD.Fill(dtDataMaxSCD);
                    if (dtDataMaxSCD.Rows.Count > 0)
                    {
                        if (dtDataMaxSCD.Rows[0]["maxIdSCD"].ToString().Length > 0)
                        {
                            maxIdSCD = dtDataMaxSCD.Rows[0]["maxIdSCD"].ToString();
                        }
                    }

                    //GenericServiceContractPayment - Get the MAX Id
                    sql = "select max(PaymentId) as maxIdGSCP from GenericServiceContractPayment";
                    DataTable dtDataMaxGSCP = new DataTable();
                    OracleDataAdapter oaGSCP = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaGSCP.Fill(dtDataMaxGSCP);
                    if (dtDataMaxGSCP.Rows.Count > 0)
                    {
                        if (dtDataMaxGSCP.Rows[0]["maxIdGSCP"].ToString().Length > 0)
                        {
                            maxIdGSCP = dtDataMaxGSCP.Rows[0]["maxIdGSCP"].ToString();
                        }
                    }

                    //Vendor Mapping -- Get the max Person ID
                    sql = "select max(to_number(personid)) personIDMNet from dm_intf_vendor_mapping t where t.attribute10='MASONET'";
                    DataTable dtDataMaxPID = new DataTable();
                    OracleDataAdapter oaPID = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaPID.Fill(dtDataMaxPID);
                    if (dtDataMaxPID.Rows.Count > 0)
                    {
                        if (dtDataMaxPID.Rows[0]["personIDMNet"].ToString().Length > 0)
                        {
                            maxPersonIDMNet = dtDataMaxPID.Rows[0]["personIDMNet"].ToString();
                        }
                    }

                    connMax.Close();
                }

                //Create Connection - AccountPayable  -- SQL
                SqlConnection con = new SqlConnection(connectionStringSQL);

                string sqlQuery = " select ACCOUNTPAYABLEID,(TYPE*10) as TYPE,VENDORID,AMOUNT,CURRENCYID,convert(nvarchar,DueDate,103) as DueDate,PAIDAMOUNT,STATUS,convert(nvarchar,STATUSDATE,103) as [STATUSDATE]";
                sqlQuery += " ,REMARKS_EN,REMARKS_AR,COMPONENTID,PAYMENTMETHOD,BANKID,PAYMENTNO,CHECKID,convert(nvarchar,PAYMENTDATE,103) as PAYMENTDATE ,MERGEDAPID,SPLITTEDAPID,CUSERID,convert(nvarchar,CDATE,103) as CDATE ";
                sqlQuery += " ,MDATE,CASE DisablePartiallyPaid WHEN 'TRUE' THEN '1' ELSE '0' END as DisablePartiallyPaid,convert(nvarchar,CANCELDATE,103) as CANCELDATE,convert(nvarchar,INVOICEDATE,103) as INVOICEDATE ,ACCOUNTPAYABLENUMBER";
                sqlQuery += " from accountpayable where status=1";
                if (maxIdAP.ToString().Length > 0)
                {
                    sqlQuery += " and ACCOUNTPAYABLEID > '" + maxIdAP + "'";
                }
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                DataTable dtData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dtData);


                //ServiceContractPayment - SQL
                string sqlQuerySCD = " Select PaymentId,ServiceContractComponentId,InvoiceNumber,convert(nvarchar,InvoiceDate,103) as InvoiceDate";
                sqlQuerySCD += " from ServiceContractPayment";
                if (maxIdSCD.ToString().Length > 0)
                {
                    sqlQuerySCD += " where PaymentId > '" + maxIdSCD + "'";
                }

                SqlCommand cmdSCD = new SqlCommand(sqlQuerySCD, con);
                DataTable dtDataSCD = new DataTable();
                SqlDataAdapter daSCD = new SqlDataAdapter(cmdSCD);

                daSCD.Fill(dtDataSCD);


                //GenericServiceContractPayment -- SQL
                string sqlQueryGSCP = " select PaymentId, GenericServiceContractComponentId, InvoiceNumber, convert(nvarchar,InvoiceDate,103) as InvoiceDate";
                sqlQueryGSCP += " from GenericServiceContractPayment";
                if (maxIdGSCP.ToString().Length > 0)
                {
                    sqlQueryGSCP += " where PaymentId > '" + maxIdGSCP + "'";
                }

                SqlCommand cmdGSCP = new SqlCommand(sqlQueryGSCP, con);
                DataTable dtDataGSCP = new DataTable();
                SqlDataAdapter daGSCP = new SqlDataAdapter(cmdGSCP);

                daGSCP.Fill(dtDataGSCP);


                //Vendor Mapping - SQL
                string sqlQueryVM = " SELECT PersonID, Name_En, Name_Ar, NationalityID, PersonType, Address, VendorTypeID, IsActive, VendorNumber, CityID, IsPrimary";
                sqlQueryVM += " FROM vwvendor";
                if (maxPersonIDMNet.ToString().Length > 0)
                {
                    sqlQueryVM += " where PersonID > '" + maxPersonIDMNet + "'";
                }

                SqlCommand cmdVM = new SqlCommand(sqlQueryVM, con);
                DataTable dtDataVM = new DataTable();
                SqlDataAdapter daVM = new SqlDataAdapter(cmdVM);

                daVM.Fill(dtDataVM);


                //Create Connection -- Oracle
                //Account Payables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into accountpayable (accountpayableid,type,vendorid, amount, currencyid, duedate, paidamount, status, statusdate, remarks_en, remarks_ar, componentid, paymentmethod, ";
                            sql += "  bankid, paymentno, checkid, paymentdate, mergedapid, splittedapid, cuserid , cdate, mdate, disablepartiallypaid, canceldate, invoicedate, accountpayablenumber ) Values ('";
                            sql += dtData.Rows[iLoop]["ACCOUNTPAYABLEID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["TYPE"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["VENDORID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["AMOUNT"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CURRENCYID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["DUEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["PAIDAMOUNT"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["STATUS"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["STATUSDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["REMARKS_EN"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["REMARKS_AR"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["COMPONENTID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PAYMENTMETHOD"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["BANKID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["PAYMENTNO"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CHECKID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["PAYMENTDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["MERGEDAPID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["SPLITTEDAPID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CUSERID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["CDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "NULL,'";
                            sql += dtData.Rows[iLoop]["DISABLEPARTIALLYPAID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["CANCELDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "to_date('" + dtData.Rows[iLoop]["INVOICEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["ACCOUNTPAYABLENUMBER"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //Service Contract Payment
                if (dtDataSCD.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataSCD.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into ServiceContractPayment Values('";
                            sql += dtDataSCD.Rows[iLoop]["PaymentId"].ToString() + "','";
                            sql += dtDataSCD.Rows[iLoop]["ServiceContractComponentId"].ToString() + "','";
                            sql += dtDataSCD.Rows[iLoop]["InvoiceNumber"].ToString() + "',";
                            sql += "to_date('" + dtDataSCD.Rows[iLoop]["InvoiceDate"].ToString() + "','dd/MM/yyyy')";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        //  lblImported.Text = "Data Imported";
                    }
                }


                //Generic Service Contract Payment
                if (dtDataGSCP.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();


                        for (int iLoop = 0; iLoop < dtDataGSCP.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into GenericServiceContractPayment Values('";
                            sql += dtDataGSCP.Rows[iLoop]["PaymentId"].ToString() + "','";
                            sql += dtDataGSCP.Rows[iLoop]["GenericServiceContractComponentId"].ToString() + "','";
                            sql += dtDataGSCP.Rows[iLoop]["InvoiceNumber"].ToString() + "',";
                            sql += "to_date('" + dtDataGSCP.Rows[iLoop]["InvoiceDate"].ToString() + "','dd/MM/yyyy')";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                        }
                        connection.Close();
                        //  lblImported.Text = "Data Imported";
                    }
                }

                //Vendor Mapping Insert - Oracle
                if (dtDataVM.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtDataVM.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into dm_intf_vendor_mapping (PERSONID, NAME_EN, NAME_AR, NATIONALITYID, PERSONTYPE, ADDRESS, VENDORTYPEID, ISACTIVE, VENDORNUMBER,";
                            sql += " CITYID,ISPRIMARY, ATTRIBUTE10 ) Values ('";
                            sql += dtDataVM.Rows[iLoop]["PERSONID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NAME_EN"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NAME_AR"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NATIONALITYID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["PERSONTYPE"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ADDRESS"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["VENDORTYPEID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ISACTIVE"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["VENDORNUMBER"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["CITYID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ISPRIMARY"].ToString() + "','";
                            sql += "MASONET'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //for (int iLoop = 0; iLoop < dtDataVM.Rows.Count; iLoop++)
                //{
                //    FINSP.GetSP_GL_Posting("", "SSM_029_M");
                //}

                FINSP.GetSP_GL_Posting("1", "SSM_029_M");

                DataTable dtDataView = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                BindGrid(dtDataView);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATAIMPORT);

                //Close Connection  
                con.Close();
                da.Dispose();
                //close

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablesImport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gvData.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gvData.Rows[rloop].FindControl("chkSelect");
                chk_tmp.Checked = chk_SAA.Checked;
            }
        }

    }
}