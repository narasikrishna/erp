﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class MaterialIssueEntry : PageBase
    {
        INV_MATERIAL_ISSUE_HDR iNV_MATERIAL_ISSUE_HDR = new INV_MATERIAL_ISSUE_HDR();
        INV_MATERIAL_ISSUE_DTL iNV_MATERIAL_ISSUE_DTL = new INV_MATERIAL_ISSUE_DTL();
        INV_MATERIAL_ISSUE_WH iNV_MATERIAL_ISSUE_WH = new INV_MATERIAL_ISSUE_WH();

        DataTable dtGridData = new DataTable();
        DataTable dtLotGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        int rowIndexVal = 1;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session[FINSessionConstants.LotGridData] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        #endregion Pageload
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDept);
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            AccountCodes_BLL.fn_getAccount(ref ddlAccCode);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session[FINSessionConstants.LotGridData] = null;

                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = null;
                //Session[FINSessionConstants.LotGridData + "1"] = null;
                //Session[FINSessionConstants.LotGridData + "2"] = null;
                //Session[FINSessionConstants.LotGridData + "3"] = null;
                //Session[FINSessionConstants.LotGridData + "4"] = null;
                //Session[FINSessionConstants.LotGridData + "5"] = null;
                //Session[FINSessionConstants.LotGridData + "6"] = null;
                //Session[FINSessionConstants.LotGridData + "7"] = null;


                Session["GridData"] = null;
                Session["rowindex"] = null;

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetIndentDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                dtLotGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(Master.StrRecordId)).Tables[0];
                BindLotGrid(dtLotGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_MATERIAL_ISSUE_HDR = MaterialIssue_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = iNV_MATERIAL_ISSUE_HDR;

                    if (iNV_MATERIAL_ISSUE_HDR.INDENT_DEPT_ID != null)
                    {
                        ddlDept.SelectedValue = iNV_MATERIAL_ISSUE_HDR.INDENT_DEPT_ID;
                    }
                    if (iNV_MATERIAL_ISSUE_HDR.INDENT_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(iNV_MATERIAL_ISSUE_HDR.INDENT_DATE.ToString());
                    }
                    if (iNV_MATERIAL_ISSUE_HDR.ISSUING_EMP_ID != null)
                    {
                        ddlEmployeeName.SelectedValue = iNV_MATERIAL_ISSUE_HDR.ISSUING_EMP_ID.ToString();
                    }
                    if (iNV_MATERIAL_ISSUE_HDR.RECEIVED_BY != null)
                    {
                        txtReceivedBy.Text = iNV_MATERIAL_ISSUE_HDR.RECEIVED_BY.ToString();
                    }
                    if (iNV_MATERIAL_ISSUE_HDR.INDENT_REMARKS != null)
                    {
                        txtMIRemarks.Text = iNV_MATERIAL_ISSUE_HDR.INDENT_REMARKS;
                    }
                    if (iNV_MATERIAL_ISSUE_HDR.ACCT_CODE_ID != null)
                    {
                        ddlAccCode.SelectedValue = iNV_MATERIAL_ISSUE_HDR.ACCT_CODE_ID;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                Item_BLL.getItem_RECinWh_notServce(ref ddlItemName);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ITEM_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session["GridData"] != null)
            {
                dtGridData = (DataTable)Session["GridData"];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            System.Collections.SortedList slControls_n = new System.Collections.SortedList();

            TextBox txtUOM = gvr.FindControl("txtUOM") as TextBox;
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox txtRequestedQuantity = gvr.FindControl("txtRequestedQuantity") as TextBox;
            TextBox txtIssuedQuantity = gvr.FindControl("txtIssuedQuantity") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INDENT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }


            if (ddlItemName.SelectedValue.ToString().Length > 0)
            {
                slControls[0] = ddlItemName;
                slControls[1] = txtUOM;
                slControls[2] = txtRequestedQuantity;
                slControls[3] = txtIssuedQuantity;

                ErrorCollection.Clear();

                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

                string strMessage = "Item Name ~ UOM ~ Requested Quantity ~ Issued Quantity";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }

                string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.Trim() + "'";
                strMessage = FINMessageConstatns.RecordAlreadyExists;
                ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData ];
                    if (dtLotGridData.Select("ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'").Length == 0)
                    {
                        ErrorCollection.Add("InvalidLotDetails", "Lot Details Not Found");
                        return drList;
                    }
                }
                //else
                //{
                //    ErrorCollection.Add("InvalidLotDetails", "Lot Details Not Found");
                //    return drList;
                //}

                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }

                double dbl_Qty = 0;

                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];

                    if (dtLotGridData.Rows.Count > 0)
                    {
                        for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                        {
                            if (ddlItemName.SelectedValue == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString())
                            {
                                dbl_Qty += Convert.ToDouble(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                            }
                        }
                    }
                }
                if (dbl_Qty > 0)
                {
                    if (dbl_Qty == Convert.ToDouble(txtIssuedQuantity.Text))
                    {

                    }
                    else
                    {
                        // mpeReceipt.Show();
                        //lblLotError.Text = "Item Qty And Lot Qty Must be Same";
                        //lblLotError.Visible = true;
                    }
                }

                //if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < CommonUtils.ConvertStringToInt(txtRequestedQuantity.Text))
                //{
                //    ErrorCollection.Add("rec", "Requested quantity should not be greater than Quantity");
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return drList;
                //}

                if (CommonUtils.ConvertStringToInt(txtRequestedQuantity.Text) < CommonUtils.ConvertStringToInt(txtIssuedQuantity.Text))
                {
                    ErrorCollection.Add("recapp", "Issued quantity should not be greater than the Requested Quantity");
                }
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }

                //if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < (CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text) + CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text)))
                //{
                //    ErrorCollection.Add("rec2", "Quantity should not be greater than Requested quantity - Rejected quantity");
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return drList;
                //}

            }
            else
            {
                slControls_n[0] = txtRequestedQuantity;
                string strCtrlTypes1 = FINAppConstants.TEXT_BOX;
                string strMessage1 = "Requested Quantity";
                EmptyErrorCollection = CommonUtils.IsValid(slControls_n, strCtrlTypes1, strMessage1);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }
            }

            ErrorCollection.Clear();

            if (hdItemId.Value != string.Empty)
            {
                drList["ITEM_ID"] = (hdItemId.Value);
            }
            drList["ITEM_NAME"] = ddlItemName.SelectedItem.Text;
            drList["ITEM_ID"] = ddlItemName.SelectedValue;
            if (txtUOM.Text != String.Empty)
            {
                drList["ITEM_UOM"] = txtUOM.Text;
            }
            else
            {
                drList["ITEM_UOM"] = "0";
            }

            drList["item_req_qty"] = CommonUtils.ConvertStringToInt(txtRequestedQuantity.Text);

            if (txtIssuedQuantity.Text != String.Empty)
            {
                drList["item_issue_qty"] = CommonUtils.ConvertStringToInt(txtIssuedQuantity.Text);
            }
            else
            {
                drList["item_issue_qty"] = "0";
            }

            if (txtRemarks.Text != String.Empty)
            {
                drList["REMARKS"] = txtRemarks.Text;
            }
            else
            {
                drList["REMARKS"] = null;
            }

            drList["DELETED"] = FINAppConstants.N;

            return drList;
        }

        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = new DropDownList();
                DropDownList ddlWH = new DropDownList();
                TextBox txtUOM = new TextBox();

                ddlItemName.ID = "ddlItemName";
                txtUOM.ID = "txtUOM";
                ddlWH.ID = "ddlWH";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUOM = (TextBox)gvData.FooterRow.FindControl("txtUOM");
                        ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
                    }
                    else
                    {
                        txtUOM = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUOM");
                        ddlItemName = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlItemName");
                    }
                }
                else
                {
                    txtUOM = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUOM");
                    ddlItemName = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlItemName");
                }

                //Assign UOM based on Selected Item
                DataTable dtData = new DataTable();
                if (ddlItemName.SelectedValue != string.Empty && ddlItemName.SelectedValue != "0")
                {
                    dtData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemUOM(ddlItemName.SelectedValue.ToString())).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            txtUOM.Text = dtData.Rows[0]["ITEM_UOM"].ToString();
                        }
                    }
                }

                ////Fill the Warehouse based on the Item Selected
                //if (gvLot.FooterRow != null)
                //{
                //    if (gvLot.EditIndex < 0)
                //    {
                //        ddlWH = (DropDownList)gvLot.FooterRow.FindControl("ddlWH");
                //    }
                //    else
                //    {
                //        ddlWH = (DropDownList)gvLot.Rows[gvLot.EditIndex].FindControl("ddlWH");
                //    }
                //}
                //else
                //{
                //    ddlWH = (DropDownList)gvLot.Controls[0].Controls[1].FindControl("ddlWH");
                //}

                //MaterialIssue_BLL.GetWHDetails(ref ddlWH, ddlItemName.SelectedValue.ToString());

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIItemName", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList["DELETED"] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        #endregion

        //private void fn_POData()
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();

        //        DropDownList ddlItemName = new DropDownList();

        //        ddlItemName.ID = "ddlItemName";

        //        if (gvData.FooterRow != null)
        //        {
        //            if (gvData.EditIndex < 0)
        //            {
        //                ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
        //            }
        //            else
        //            {
        //                ddlItemName = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlItemName");
        //            }
        //        }
        //        else
        //        {
        //            ddlItemName = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlItemName");
        //        }
        //        DataTable dtData = new DataTable();
        //        if (ddlItemName.SelectedValue != string.Empty && ddlItemName.SelectedValue != "0")
        //        {
        //            dtData = DBMethod.ExecuteQuery(PurchaseItemReceiptDAL.GetPOData(ddlPoNumber.SelectedValue.ToString())).Tables[0];
        //            if (dtData != null)
        //            {
        //                if (dtData.Rows.Count > 0)
        //                {
        //                    ddlItemName.SelectedValue = dtData.Rows[0]["PO_ITEM_ID"].ToString();
        //                    hdItemId.Value = dtData.Rows[0]["PO_ITEM_ID"].ToString();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("MIE", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
        //        }
        //    }
        //}

        //private void BindLotGrid(DataTable dtData)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();


        //        bol_rowVisiable = false;
        //        Session[FINSessionConstants.LotGridData] = dtData;
        //        DataTable dt_tmp = dtData.Copy();
        //        if (dt_tmp.Rows.Count == 0)
        //        {
        //            DataRow dr = dt_tmp.NewRow();
        //            dr[0] = "0";
        //            dt_tmp.Rows.Add(dr);
        //            bol_rowVisiable = true;
        //        }
        //        gvLot.DataSource = dt_tmp;
        //        gvLot.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("RLD_BG", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
        //        }
        //    }
        //}


        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                Employee_BLL.GetEmplName(ref ddlEmployeeName, ddlDept.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIEmplName", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                var tmpSecondChildEntity = new List<Tuple<object, string>>();
                if (EntityData != null)
                {
                    iNV_MATERIAL_ISSUE_HDR = (INV_MATERIAL_ISSUE_HDR)EntityData;
                }

                if (txtDate.Text != string.Empty)
                {
                    iNV_MATERIAL_ISSUE_HDR.INDENT_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }
                iNV_MATERIAL_ISSUE_HDR.INDENT_DEPT_ID = ddlDept.SelectedValue.ToString();
                iNV_MATERIAL_ISSUE_HDR.ISSUING_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                iNV_MATERIAL_ISSUE_HDR.INDENT_REMARKS = txtMIRemarks.Text;
                iNV_MATERIAL_ISSUE_HDR.RECEIVED_BY = txtReceivedBy.Text;

                iNV_MATERIAL_ISSUE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                iNV_MATERIAL_ISSUE_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                iNV_MATERIAL_ISSUE_HDR.ACCT_CODE_ID = ddlAccCode.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_MATERIAL_ISSUE_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_MATERIAL_ISSUE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNV_MATERIAL_ISSUE_HDR.INDENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_028_M.ToString(), false, true);
                    iNV_MATERIAL_ISSUE_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_MATERIAL_ISSUE_HDR.CREATED_DATE = DateTime.Today;
                }

                iNV_MATERIAL_ISSUE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_MATERIAL_ISSUE_HDR.INDENT_ID);

                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Material Issue");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                INV_MATERIAL_ISSUE_DTL iNV_MATERIAL_ISSUE_DTL = new INV_MATERIAL_ISSUE_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_MATERIAL_ISSUE_DTL = new INV_MATERIAL_ISSUE_DTL();

                    if (dtGridData.Rows[iLoop][FINColumnConstants.INDENT_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INDENT_DTL_ID].ToString() != string.Empty)
                    {
                        iNV_MATERIAL_ISSUE_DTL = MaterialIssue_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.INDENT_DTL_ID].ToString());
                    }

                    iNV_MATERIAL_ISSUE_DTL.ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    iNV_MATERIAL_ISSUE_DTL.ITEM_UOM = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_UOM].ToString());
                    iNV_MATERIAL_ISSUE_DTL.ITEM_REQ_QTY = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.ITEM_REQ_QTY].ToString());
                    iNV_MATERIAL_ISSUE_DTL.ITEM_ISSUE_QTY = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ISSUE_QTY].ToString());
                    if (dtGridData.Rows[iLoop][FINColumnConstants.REMARKS].ToString() != string.Empty)
                    {
                        iNV_MATERIAL_ISSUE_DTL.REMARKS = dtGridData.Rows[iLoop][FINColumnConstants.REMARKS].ToString();
                    }
                    else
                    {
                        iNV_MATERIAL_ISSUE_DTL.REMARKS = null;
                    }

                    iNV_MATERIAL_ISSUE_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    iNV_MATERIAL_ISSUE_DTL.INDENT_ID = iNV_MATERIAL_ISSUE_HDR.INDENT_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.INDENT_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INDENT_DTL_ID].ToString() != string.Empty)
                        {
                            iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID = dtGridData.Rows[iLoop]["INDENT_DTL_ID"].ToString();
                            iNV_MATERIAL_ISSUE_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_028_D.ToString(), false, true);
                            iNV_MATERIAL_ISSUE_DTL.CREATED_BY = this.LoggedUserName;
                            iNV_MATERIAL_ISSUE_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_DTL, FINAppConstants.Add));
                        }
                    }

                    iNV_MATERIAL_ISSUE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    //Warehouse Entry
                    if (Session[FINSessionConstants.LotGridData] != null)
                    {
                        dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];

                        if (dtLotGridData.Rows.Count > 0)
                        {
                            for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                            {
                                if (dtGridData.Rows[iLoop]["ITEM_ID"].ToString() == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString())
                                {
                                    iNV_MATERIAL_ISSUE_WH = new INV_MATERIAL_ISSUE_WH();

                                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                    {
                                        if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                                        {
                                            iNV_MATERIAL_ISSUE_WH = MaterialIssue_BLL.getWHClassEntity(dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString());
                                        }
                                    }

                                    iNV_MATERIAL_ISSUE_WH.INDENT_DTL_ID = iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID;
                                    iNV_MATERIAL_ISSUE_WH.ISSUE_WH_ID = dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString();
                                    iNV_MATERIAL_ISSUE_WH.ISSUE_LOT_ID = dtLotGridData.Rows[jLoop]["lot_id"].ToString();
                                    iNV_MATERIAL_ISSUE_WH.ISSUE_LOT_QTY = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                                    iNV_MATERIAL_ISSUE_WH.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                                    if (dtLotGridData.Rows[jLoop]["DELETED"].ToString() == FINAppConstants.Y)
                                    {
                                        tmpSecondChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_WH, FINAppConstants.Delete));
                                    }
                                    else
                                    {
                                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                                        {
                                            if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                                            {
                                                iNV_MATERIAL_ISSUE_WH.MODIFIED_DATE = DateTime.Today;
                                                tmpSecondChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_WH, FINAppConstants.Update));
                                            }
                                        }
                                        else
                                        {
                                            iNV_MATERIAL_ISSUE_WH.ISSUE_MAT_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_028_T.ToString(), false, true);
                                            iNV_MATERIAL_ISSUE_WH.CREATED_BY = Session["UserId"].ToString();
                                            iNV_MATERIAL_ISSUE_WH.CREATED_DATE = DateTime.Today;
                                            tmpSecondChildEntity.Add(new Tuple<object, string>(iNV_MATERIAL_ISSUE_WH, FINAppConstants.Add));
                                        }
                                    }

                                    iNV_MATERIAL_ISSUE_WH.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                                }
                            }
                        }
                    }

                }

                //// Duplicate Check
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_PO_RECP(ddlEmployeeName.SelectedValue, iNV_MATERIAL_ISSUE_HDR.INDENT_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("POREQ", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveMultipleEntity<INV_MATERIAL_ISSUE_HDR, INV_MATERIAL_ISSUE_DTL, INV_MATERIAL_ISSUE_WH>(iNV_MATERIAL_ISSUE_HDR, tmpChildEntity, iNV_MATERIAL_ISSUE_DTL, tmpSecondChildEntity, iNV_MATERIAL_ISSUE_WH);
                            savedBool = true;


                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                                {
                                    DataTable dtQTY = new DataTable();
                                    decimal dtrecqty = 0;
                                    decimal balqty = 0;


                                    dtQTY = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqtyissed_fromRecLotHdr(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                    dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY_ISSUED"].ToString());
                                    balqty = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());

                                    HFlotqty.Value = (dtrecqty + balqty).ToString();

                                    MaterialIssue_DAL.UPDATE_Receiptlot_dtl(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                                    DataTable dtrecptid = new DataTable();
                                    dtrecptid = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(dtLotGridData.Rows[jLoop]["lot_id"].ToString())).Tables[0];

                                    FINSP.GetSPItemtxnLedger("ISSUE", dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString(), dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString(), dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString(), DateTime.Now.ToString(), dtrecptid.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ISSUES of " + dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                                }
                            }


                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                                {
                                    DataTable dtQTY = new DataTable();
                                    DataTable QtyfromMI = new DataTable();
                                    decimal dtrecqty = 0;
                                    decimal balqty = 0;
                                    decimal qtyMI = 0;
                                    decimal Finlqty = 0;

                                    dtQTY = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqtyissed_fromRecLotHdr(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                    QtyfromMI = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqty_fromMatIssu(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                    dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY_ISSUED"].ToString());
                                    qtyMI = CommonUtils.ConvertStringToDecimal(QtyfromMI.Rows[0]["ISSUE_LOT_QTY"].ToString());
                                    balqty = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                                    Finlqty = (qtyMI - balqty);

                                    HFlotqty.Value = (dtrecqty - Finlqty).ToString();
                                    MaterialIssue_DAL.UPDATE_Receiptlot_dtl(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                                   
                                }
                            }

                            CommonUtils.SaveMultipleEntity<INV_MATERIAL_ISSUE_HDR, INV_MATERIAL_ISSUE_DTL, INV_MATERIAL_ISSUE_WH>(iNV_MATERIAL_ISSUE_HDR, tmpChildEntity, iNV_MATERIAL_ISSUE_DTL, tmpSecondChildEntity, iNV_MATERIAL_ISSUE_WH, true);
                            savedBool = true;
                            for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            {
                                for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                                {
                                    //DataTable dtQTY = new DataTable();
                                    //DataTable QtyfromMI = new DataTable();
                                    //decimal dtrecqty = 0;
                                    //decimal balqty = 0;
                                    //decimal qtyMI = 0;
                                    //decimal Finlqty = 0;

                                    //dtQTY = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqtyissed_fromRecLotHdr(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                    //QtyfromMI = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqty_fromMatIssu(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                                    //dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY_ISSUED"].ToString());
                                    //qtyMI = CommonUtils.ConvertStringToDecimal(QtyfromMI.Rows[0]["ISSUE_LOT_QTY"].ToString());
                                    //balqty = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                                    //Finlqty = (qtyMI - balqty);

                                    //HFlotqty.Value = (dtrecqty - Finlqty).ToString();
                                    //MaterialIssue_DAL.UPDATE_Receiptlot_dtl(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                                    DataTable dtrecptid = new DataTable();
                                    dtrecptid = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(dtLotGridData.Rows[jLoop]["lot_id"].ToString())).Tables[0];
                                    FINSP.GetSPItemtxnLedger("ISSUE", dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString(), dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString(), dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString(), DateTime.Now.ToString(), dtrecptid.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ISSUES of " + dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                                }
                            }
                            break;
                        }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_MATERIAL_ISSUE_HDR.INDENT_ID, iNV_MATERIAL_ISSUE_HDR.ISSUING_EMP_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("INDENT", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MISAVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        //Grid1 Itemname change event
        //protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
        //        TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
        //        TextBox txtApprovedQuantity = gvr.FindControl("txtApprovedQuantity") as TextBox;
        //        TextBox txtRejectedQuantity = gvr.FindControl("txtRejectedQuantity") as TextBox;
        //        DropDownList ddlPoNumber = gvr.FindControl("ddlPoNumber") as DropDownList;

        //        DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;

        //        if (ddlPoNumber.SelectedValue.ToString().Length > 0)
        //        {
        //            txtQuantity.Enabled = true;
        //            txtApprovedQuantity.Enabled = true;
        //            txtRejectedQuantity.Enabled = true;
        //        }
        //        else
        //        {
        //            txtQuantity.Enabled = false;
        //            txtApprovedQuantity.Enabled = false;
        //            txtRejectedQuantity.Enabled = false;
        //        }
        //        DataTable dtunitprice = new DataTable();
        //        dtunitprice = DBMethod.ExecuteQuery(PurchaseOrderReq_DAL.GetItemUnitprice(ddlItemName.SelectedValue)).Tables[0];
        //        hf_UnitPrice.Value = dtunitprice.Rows[0]["ITEM_UNIT_PRICE"].ToString();

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("POR", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
        //        }
        //    }
        //}


        protected void btnLot_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                hdRowIndex.Value = gvr.RowIndex.ToString();



                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                TextBox txtRequestedQty = gvr.FindControl("txtRequestedQuantity") as TextBox;
                TextBox txtIssuedQty = gvr.FindControl("txtIssuedQuantity") as TextBox;

                if (ddlItemName.SelectedValue.ToString().Trim().Length > 0 && txtIssuedQty.Text.Length > 0)
                {
                    hdItemId.Value = ddlItemName.SelectedValue.ToString();



                    string itemId = string.Empty;

                    if (gvData.EditIndex >= 0)
                    {
                        if (gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString() != string.Empty)
                        {
                            itemId = gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString();
                            hdItemIndex.Value = itemId;
                        }
                    }

                    if (Session[FINSessionConstants.LotGridData] == null)
                    {
                        dtLotGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(Master.StrRecordId)).Tables[0];
                    }
                    else
                    {
                        dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                    }

                    lblLotError.Visible = false;
                    Session[FINSessionConstants.LotGridData] = dtLotGridData;
                    BindLotGrid(dtLotGridData);
                    hf_qty.Value = txtIssuedQty.Text.ToString();
                    mpeReceipt.Show();

                }
                else
                {
                    ErrorCollection.Add("PleaseSelectItem", "Please Select the Item / Please Entry The Quanty");
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotButtonClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void btnLotNo_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //int rowIndex = 0;
                //GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                //rowIndex = gvr.RowIndex;

                //if (Session[FINSessionConstants.LotGridData] == null)
                //{
                //    Session[FINSessionConstants.LotGridData + hdRowIndex.Value] = Session[FINSessionConstants.LotGridData];
                //}
                //else
                //{



                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                    double dbl_Qty = 0;
                    if (dtLotGridData.Rows.Count > 0)
                    {
                        for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                        {
                            if (hdItemId.Value == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString() && dtLotGridData.Rows[jLoop]["DELETED"].ToString() != "1")
                            {
                                dbl_Qty += Convert.ToDouble(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                            }
                        }
                    }
                    if (dbl_Qty > 0)
                    {
                        if (dbl_Qty <= Convert.ToDouble(hf_qty.Value))
                        {
                            mpeReceipt.Hide();
                        }
                        else
                        {
                            mpeReceipt.Show();
                            lblLotError.Text = "Issued Quantity and Lot Quantity must be Same";
                            lblLotError.Visible = true;
                        }
                    }
                    else
                    {
                        mpeReceipt.Hide();
                    }
                }


                // mpeReceipt.Hide();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotClose", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlWH_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillLot();
        }

        private void fillLot()
        {
            try
            {
                ErrorCollection.Clear();
                //DropDownList ddlWH = tmpgvr.FindControl("ddlWH") as DropDownList;

                DropDownList ddlWH = new DropDownList();
                DropDownList ddlLot = new DropDownList();

                ddlWH.ID = "ddlLot";
                ddlWH.ID = "ddlWH";

                if (gvLot.FooterRow != null)
                {
                    if (gvLot.EditIndex < 0)
                    {
                        ddlWH = (DropDownList)gvLot.FooterRow.FindControl("ddlWH");
                        ddlLot = (DropDownList)gvLot.FooterRow.FindControl("ddlLot");
                    }
                    else
                    {
                        ddlWH = (DropDownList)gvLot.Rows[gvLot.EditIndex].FindControl("ddlWH");
                        ddlLot = (DropDownList)gvLot.Rows[gvLot.EditIndex].FindControl("ddlLot");
                    }
                }
                else
                {
                    ddlWH = (DropDownList)gvLot.Controls[0].Controls[1].FindControl("ddlWH");
                    ddlLot = (DropDownList)gvLot.Controls[0].Controls[1].FindControl("ddlLot");
                }

                MaterialIssue_BLL.GetLotDetails(ref ddlLot, ddlWH.SelectedValue.ToString(), hdItemId.Value.ToString());
                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events

        private void BindLotGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();


                bol_rowVisiable = false;
                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvLot.DataSource = dt_tmp;
                gvLot.DataBind();
                GridViewRow gvr = gvLot.FooterRow;
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterLotGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlWH = tmpgvr.FindControl("ddlWH") as DropDownList;
                DropDownList ddlLot = tmpgvr.FindControl("ddlLot") as DropDownList;

                //Warehouse_BLL.GetWareHouseName(ref ddlWH);
                //MaterialIssue_BLL.GetLotNumber(ref ddlLot);
                MaterialIssue_BLL.getWHDetails_fritem(ref ddlWH, hdItemId.Value.ToString());


                if (gvLot.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    // ddlWH.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["INV_WH_NAME"].ToString();
                    ddlWH.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["INV_WH_ID"].ToString();
                    //ddlLot.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["LOT_NUMBER"].ToString();
                    fillLot();
                    ddlLot.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["LOT_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootLOTGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvLot_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
            //{
            //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
            //}
            if (Session[FINSessionConstants.LotGridData] != null)
            {
                dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
            }
            gvLot.EditIndex = -1;

            BindLotGrid(dtLotGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvLot_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvLot.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }

                }

                //if (e.CommandName.Equals("btnPop"))
                //{
                //    int rowindex = Convert.ToInt32(e.CommandArgument);
                //    hdRowIndex.Value = rowindex.ToString();
                //}

                if (Session["rowindex"] == null)
                {
                    Session["rowindex"] = 0;
                    hdRowIndex.Value = Session["rowindex"].ToString();
                }
                else
                {
                    hdRowIndex.Value = (int.Parse(Session["rowindex"].ToString()) + 1).ToString();
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    if (dtLotGridData != null)
                    {
                        int rowLotIndex = 0;
                        //if (gvr.RowIndex == -1)
                        //{
                        //    rowLotIndex = gvr.RowIndex + rowIndexVal;
                        //}
                        //else if (gvr.RowIndex >= 0)
                        //{
                        //    rowLotIndex = gvr.RowIndex;
                        //}

                        drList = AssignToLotGridControl(gvr, dtLotGridData, "A", 0);
                        if (ErrorCollection.Count > 0)
                        {
                            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                            return;
                        }
                        dtLotGridData.Rows.Add(drList);
                       // Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                        Session[FINSessionConstants.LotGridData] = dtLotGridData;
                        BindLotGrid(dtLotGridData);
                    }
                  
                }
                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToLotGridControl(GridViewRow gvr, DataTable tmpdtLotGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlWH = gvr.FindControl("ddlWH") as DropDownList;
            DropDownList ddlLot = gvr.FindControl("ddlLot") as DropDownList;
            TextBox txtQty = gvr.FindControl("txtQty") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtLotGridData.Copy();
            if (GMode == "A")
            {
                drList = dtLotGridData.NewRow();
                drList["INV_WH_ID"] = "0";
            }
            else
            {
                //if (dtLotGridData.Rows.Count > 0)
                //{
                drList = dtLotGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
                //}

            }

            slControls[0] = ddlWH;
            slControls[1] = ddlLot;
            slControls[2] = txtQty;

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList ~ DropDownList ~ TextBox";

            string strMessage = "Warehouse ~ Lot ~ Quantity";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = string.Empty;

            //strCondition = "INV_WH_ID='" + ddlWH.SelectedValue + "'";
            //strMessage = "Record Already Exists";

            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}


            strCondition = " INV_WH_ID='" + ddlWH.SelectedValue + "' AND LOT_ID= '" + ddlLot.SelectedValue + "'";

            strMessage = "Record Already Exists";
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            //DataTable dtqty = new DataTable();
            //dtqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.getLotqty(ddlLineNumber.SelectedValue)).Tables[0];
            ////hf_qty.Value = dtqty.Rows[0]["tot_qty"].ToString();
            //hf_qty.Value = "0";

            //if (decimal.Parse(txtQuantityforthislot.Text) > decimal.Parse(hf_qty.Value))
            //{
            //    ErrorCollection.Add("lotqty", "Quantity Cannot be greater than purchase item received quantity");
            //    return drList;
            //}
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}

            //DataTable dtlotqty = new DataTable();

            // dtlotqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.GetTotalReceivedLotqty(txtGRNNumber.Text, ddlLineNumber.SelectedValue, hid_Item_Id.Value)).Tables[0];

            //if (decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString()) > 0)
            //{
            //    decimal diffLot = decimal.Parse(hf_qty.Value) - decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString());

            //    if (decimal.Parse(txtQuantityforthislot.Text) > Math.Abs(diffLot))
            //    {
            //        ErrorCollection.Add("totlotqty", "Quantity for this lot exceeding the GRN quantity.Received quantity is " + dtlotqty.Rows[0]["lot_qty"].ToString() + ".Pending Quantity is " + diffLot);
            //        return drList;
            //    }
            //}
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}

            drList["INV_WH_NAME"] = ddlWH.SelectedItem.Text.ToString();
            drList["INV_WH_ID"] = ddlWH.SelectedValue.ToString();
            drList["LOT_NUMBER"] = ddlLot.SelectedItem.Text.ToString();
            drList["LOT_ID"] = ddlLot.SelectedValue.ToString();
            drList["LOT_QTY"] = txtQty.Text.ToString();
            drList["ITEM_ID"] = hdItemId.Value.ToString();

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvLot_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData ];
                }
                if (gvr == null)
                {
                    return;
                }
                //int rowindex = Convert.ToInt32(e.CommandArgument);

                //hdRowIndex.Value = rowindex.ToString();
                if (dtLotGridData != null)
                {
                    drList = AssignToLotGridControl(gvr, dtLotGridData, "U", e.RowIndex);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    gvLot.EditIndex = -1;
                    //Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                    Session[FINSessionConstants.LotGridData] = dtLotGridData;
                    BindLotGrid(dtLotGridData);
                }
                //if (Session[FINSessionConstants.LotGridData + hdRowIndex.Value] == null)
                //{
                //    Session[FINSessionConstants.LotGridData + hdRowIndex.Value] = Session[FINSessionConstants.LotGridData];
                //}

                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvLot_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }
                DataRow drList = null;
                drList = dtLotGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindLotGrid(dtLotGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvLot_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                //if (Session[FINSessionConstants.LotGridData + ] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdRowIndex.Value];
                //}
                gvLot.EditIndex = e.NewEditIndex;
                BindLotGrid(dtLotGridData);
                GridViewRow gvr = gvLot.Rows[e.NewEditIndex];
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvLot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvLot.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvLot_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    else
                    {
                        if (((DataRowView)e.Row.DataItem).Row["ITEM_ID"].ToString() != hdItemId.Value)
                        {
                            e.Row.Visible = false;
                        }
                    }
                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        #endregion

        protected void ddlLot_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                getLotData();
                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void getLotData()
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLot = new DropDownList();
                TextBox txtQuantity = new TextBox();

                ddlLot.ID = "ddlLot";
                txtQuantity.ID = "txtQty";


                if (gvLot.FooterRow != null)
                {
                    if (gvLot.EditIndex < 0)
                    {
                        //ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
                        ddlLot = (DropDownList)gvLot.FooterRow.FindControl("ddlLot");
                        txtQuantity = (TextBox)gvLot.FooterRow.FindControl("txtQty");
                    }
                    else
                    {
                        ddlLot = (DropDownList)gvLot.Rows[gvLot.EditIndex].FindControl("ddlLot");
                        txtQuantity = (TextBox)gvLot.Rows[gvLot.EditIndex].FindControl("txtQty");
                    }
                }
                else
                {
                    ddlLot = (DropDownList)gvLot.Controls[0].Controls[1].FindControl("ddlLot");
                    txtQuantity = (TextBox)gvLot.Controls[0].Controls[1].FindControl("txtQty");
                }
                DataTable dtData = new DataTable();
                if (ddlLot.SelectedValue != string.Empty && ddlLot.SelectedValue != "0")
                {
                    dtData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(ddlLot.SelectedValue.ToString())).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            txtQuantity.Text = dtData.Rows[0]["LOT_QTY"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIGetLotData", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

    }
}