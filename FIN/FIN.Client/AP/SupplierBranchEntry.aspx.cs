﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class SupplierBranchEntry : PageBase
    {
        SUPPLIER_CUSTOMER_BRANCH SUPPLIER_CUSTOMER_BRANCH = new SUPPLIER_CUSTOMER_BRANCH();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SUPPLIER_CUSTOMER_BRANCH> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BRANCH>())
                    {
                        SUPPLIER_CUSTOMER_BRANCH = userCtx.Find(r =>
                            (r.VENDOR_LOC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = SUPPLIER_CUSTOMER_BRANCH;

                    txtBranchShortName.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE.ToString();
                  
                    if (SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE_OL != null)
                    {
                        txtBranchShortNameOL.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE_OL.ToString();
                    }


                    ddlSupplierNumber.SelectedValue = SUPPLIER_CUSTOMER_BRANCH.VENDOR_ID.ToString();

                    if (ddlSupplierNumber.SelectedValue != null)
                    {
                        BindSupplierName();
                    }

                    ddlSupplierStatus.SelectedValue = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_STATUS.ToString();
                    txtAddress1.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD1;
                    txtAddress2.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD2;
                    txtAddress3.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD3;
                    txtEmail.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_EMAIL;
                    ddlCountry.SelectedValue = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_COUNTRY.ToString();
                    fillstate();
                    if (SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_STATE != null)
                    {
                        ddlState.SelectedValue = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_STATE.ToString();
                        fillcity();
                    }
                    if (SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_CITY != null)
                    {
                        ddlCity.SelectedValue = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_CITY.ToString();
                    }
                    txtPostalCode.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ZIP_CODE;
                    txtPhone.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_PHONE;
                    txtFax.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_FAX;
                    txtURL.Text = SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_URL;

                    if (SUPPLIER_CUSTOMER_BRANCH.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    SUPPLIER_CUSTOMER_BRANCH = (SUPPLIER_CUSTOMER_BRANCH)EntityData;
                }



                SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE = txtBranchShortName.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE_OL = txtBranchShortNameOL.Text;

                SUPPLIER_CUSTOMER_BRANCH.VENDOR_ID = ddlSupplierNumber.SelectedValue.ToString();
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_STATUS = ddlSupplierStatus.SelectedValue.ToString();
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD1 = txtAddress1.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD2 = txtAddress2.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ADD3 = txtAddress3.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_EMAIL = txtEmail.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_COUNTRY = ddlCountry.SelectedValue;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_STATE = ddlState.SelectedValue;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_CITY = ddlCity.SelectedValue;


                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ZIP_CODE = txtPostalCode.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_PHONE = txtPhone.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_FAX = txtFax.Text;
                SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_URL = txtURL.Text;

                SUPPLIER_CUSTOMER_BRANCH.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                // SUPPLIER_CUSTOMER_BRANCH.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //SUPPLIER_CUSTOMER_BRANCH.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    SUPPLIER_CUSTOMER_BRANCH.MODIFIED_BY = this.LoggedUserName;
                    SUPPLIER_CUSTOMER_BRANCH.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_010.ToString(), false, true);

                    SUPPLIER_CUSTOMER_BRANCH.CREATED_BY = this.LoggedUserName;
                    SUPPLIER_CUSTOMER_BRANCH.CREATED_DATE = DateTime.Today;


                }
                SUPPLIER_CUSTOMER_BRANCH.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ID);
                SUPPLIER_CUSTOMER_BRANCH.ORG_ID = VMVServices.Web.Utils.OrganizationID;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            Supplier_BLL.GetSupplierNumber(ref ddlSupplierNumber);
            Supplier_BLL.fn_getCountry(ref ddlCountry);
            Supplier_BLL.fn_getSupplierStatus(ref ddlSupplierStatus);
        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillstate();
        }

        private void fillstate()
        {
            ddlCountry.Focus();
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
            // Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillcity();
        }
        private void fillcity()
        {
            ddlState.Focus();
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ID, SUPPLIER_CUSTOMER_BRANCH.VENDOR_ID, SUPPLIER_CUSTOMER_BRANCH.VENDOR_BRANCH_CODE);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("SUPPLIERBRANCH", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                // Duplicate Check

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_SUP_BRANCH(txtBranchShortName.Text, SUPPLIER_CUSTOMER_BRANCH.VENDOR_LOC_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("SUPPLIERBRANCH", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}




                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BRANCH>(SUPPLIER_CUSTOMER_BRANCH);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BRANCH>(SUPPLIER_CUSTOMER_BRANCH, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SUPPLIER_CUSTOMER_BRANCH>(SUPPLIER_CUSTOMER_BRANCH);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindSupplierName()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName(ddlSupplierNumber.SelectedValue.ToString())).Tables[0];

                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtSupplierName.Text = dtData.Rows[0][FINColumnConstants.VENDOR_NAME].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSupplierNumber.Focus();
            BindSupplierName();
        }

        protected void txtAddress2_TextChanged(object sender, EventArgs e)
        {

        }


    }
}