﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace FIN.Client.AP
{
    public partial class SierraInvoiceToAP : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        bool postedMsg = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                Session["dtData"] = null;
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_ATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["Invoice_Amount"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Invoice_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Invoice_Amount"))));
                    }

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["POSTED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion


        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        private void BindFilteredGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["Invoice_Amount"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Invoice_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Invoice_Amount"))));
                    }

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["POSTED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceImportToARBFG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtSupplierName = gvr.FindControl("txtSupplierName") as TextBox;
                TextBox txtInvoiceNumber = gvr.FindControl("txtInvoiceNumber") as TextBox;
                TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
                TextBox txtInvoiceAmount = gvr.FindControl("txtInvoiceAmount") as TextBox;

                if (txtSupplierName.Text.Length > 0)
                {
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtInvoiceAmount.Text = string.Empty;
                }
                else if (txtInvoiceNumber.Text.Length > 0)
                {
                    txtSupplierName.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtInvoiceAmount.Text = string.Empty;
                }
                else if (txtInvoiceDate.Text.Length > 0)
                {
                    txtSupplierName.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceAmount.Text = string.Empty;
                }
                else if (txtInvoiceAmount.Text.Length > 0)
                {
                    txtInvoiceNumber.Text = string.Empty;
                    txtSupplierName.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                }
                DataTable dt = new DataTable();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt = (DataTable)Session[FINSessionConstants.GridData];
                    if (dt.Rows.Count > 0)
                    {
                        var var_List = dt.AsEnumerable().Where(r => r["Invoice_ID"].ToString() != string.Empty);

                        if (txtSupplierName.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["Supplier_Name"].ToString().ToUpper().StartsWith(txtSupplierName.Text.ToUpper()));
                        }
                        else if (txtInvoiceNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["Invoice_Number"].ToString().ToUpper().StartsWith(txtInvoiceNumber.Text.ToUpper()));
                        }
                        else if (txtInvoiceDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["Invoice_Date"].ToString().ToUpper().StartsWith(txtInvoiceDate.Text.ToUpper()));
                        }
                        else if (txtInvoiceAmount.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["Invoice_Amount"].ToString().ToUpper().StartsWith(txtInvoiceAmount.Text.ToUpper()));
                        }
                        if (var_List.Any())
                        {
                            dt = System.Data.DataTableExtensions.CopyToDataTable(var_List);
                            BindFilteredGrid(dt);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        FINSP.GetSP_GL_Posting(gvData.DataKeys[iLoop].Values["Invoice_ID"].ToString(), "SSM_033");
                        postedMsg = true;
                    }
                }

                if (postedMsg)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

                DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getSierraSupplierInvoice()).Tables[0];
                BindGrid(dtData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_POST", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDataView = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getSierraSupplierInvoice()).Tables[0];
                BindGrid(dtDataView);
                Session["dtData"] = dtDataView;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_View", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gvData.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gvData.Rows[rloop].FindControl("chkSelect");
                chk_tmp.Checked = chk_SAA.Checked;
            }
        }

        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["dtData"] != null)
                {
                    gvData.PageIndex = e.NewPageIndex;
                    gvData.DataSource = Session["dtData"];
                    gvData.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SierraInvoiceToAP_PI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

    }
}