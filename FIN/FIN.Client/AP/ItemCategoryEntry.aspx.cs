﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class ItemCategory : PageBase
    {
        INV_ITEM_CATEGORY iNV_ITEM_CATEGORY = new INV_ITEM_CATEGORY();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<INV_ITEM_CATEGORY> userCtx = new DataRepository<INV_ITEM_CATEGORY>())
                    {
                        iNV_ITEM_CATEGORY = userCtx.Find(r =>
                            (r.ITEM_CATEGORY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_ITEM_CATEGORY;

                    txtCategoryName.Text = iNV_ITEM_CATEGORY.ITEM_CAT_NAME;
                    txtCategoryDescription.Text = iNV_ITEM_CATEGORY.ITEM_CAT_DESC;

                    txtCategoryNameOL.Text = iNV_ITEM_CATEGORY.ITEM_CAT_NAME_OL;


                    if (iNV_ITEM_CATEGORY.ITEM_CATEGORY_PARENT_ID != null)
                    {
                        ddlCategoryParent.SelectedValue = iNV_ITEM_CATEGORY.ITEM_CATEGORY_PARENT_ID.ToString();
                    }

                    if (iNV_ITEM_CATEGORY.ITEM_CAT_EFF_START_DT != null)
                    {
                        txtEffectiveStartDate.Text = DBMethod.ConvertDateToString(iNV_ITEM_CATEGORY.ITEM_CAT_EFF_START_DT.ToString());
                    }
                    if (iNV_ITEM_CATEGORY.ITEM_CAT_EFF_END_DT != null)
                    {
                        txtEffectiveEnddate.Text = DBMethod.ConvertDateToString(iNV_ITEM_CATEGORY.ITEM_CAT_EFF_END_DT.ToString());
                    }

                    if (iNV_ITEM_CATEGORY.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.fn_getCategoryParent(ref ddlCategoryParent);
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_ITEM_CATEGORY = (INV_ITEM_CATEGORY)EntityData;
                }



                iNV_ITEM_CATEGORY.ITEM_CAT_NAME = txtCategoryName.Text;
                iNV_ITEM_CATEGORY.ITEM_CAT_NAME_OL = txtCategoryNameOL.Text;

                iNV_ITEM_CATEGORY.ITEM_CAT_DESC = txtCategoryDescription.Text;
                iNV_ITEM_CATEGORY.ITEM_CATEGORY_PARENT_ID = ddlCategoryParent.SelectedValue.ToString();

                if (txtEffectiveStartDate.Text != string.Empty)
                {
                    iNV_ITEM_CATEGORY.ITEM_CAT_EFF_START_DT = DBMethod.ConvertStringToDate(txtEffectiveStartDate.Text.ToString());
                }
                if (txtEffectiveEnddate.Text != string.Empty)
                {
                    iNV_ITEM_CATEGORY.ITEM_CAT_EFF_END_DT = DBMethod.ConvertStringToDate(txtEffectiveEnddate.Text.ToString());
                }

                iNV_ITEM_CATEGORY.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_CATEGORY.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_ITEM_CATEGORY.MODIFIED_BY = this.LoggedUserName;
                    iNV_ITEM_CATEGORY.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    iNV_ITEM_CATEGORY.ITEM_CATEGORY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_017.ToString(), false, true);

                    iNV_ITEM_CATEGORY.CREATED_BY = this.LoggedUserName;
                    iNV_ITEM_CATEGORY.CREATED_DATE = DateTime.Today;

                }
                iNV_ITEM_CATEGORY.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                iNV_ITEM_CATEGORY.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_ITEM_CATEGORY.ITEM_CATEGORY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                // Duplicate Check
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, iNV_ITEM_CATEGORY.ITEM_CATEGORY_ID, iNV_ITEM_CATEGORY.ITEM_CAT_NAME, iNV_ITEM_CATEGORY.ITEM_CAT_DESC);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ITEM_CATOG(txtCategoryName.Text,iNV_ITEM_CATEGORY.ORG_ID, iNV_ITEM_CATEGORY.ITEM_CATEGORY_ID, txtEffectiveStartDate.Text, txtEffectiveEnddate.Text);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {

                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INV_ITEM_CATEGORY>(iNV_ITEM_CATEGORY);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<INV_ITEM_CATEGORY>(iNV_ITEM_CATEGORY, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_ITEM_CATEGORY>(iNV_ITEM_CATEGORY);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtEffectiveEnddate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCategoryParent_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}