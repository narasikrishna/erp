﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class ItemEntry : PageBase
    {
        INV_ITEM_MASTER iNV_ITEM_MASTER = new INV_ITEM_MASTER();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;

                txtWeight.Enabled = false;
                txtLength.Enabled = false;
                txtArea.Enabled = false;
                txtVolume.Enabled = false;

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INV_ITEM_MASTER> userCtx = new DataRepository<INV_ITEM_MASTER>())
                    {
                        iNV_ITEM_MASTER = userCtx.Find(r =>
                            (r.ITEM_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_ITEM_MASTER;

                    ddlItemUOM.Enabled = false;


                    txtItemShortName.Text = iNV_ITEM_MASTER.ITEM_CODE;
                    txtItemNumber.Text = iNV_ITEM_MASTER.ITEM_NUMBER;
                    txtItemName.Text = iNV_ITEM_MASTER.ITEM_NAME;
                    txtItemDescription.Text = iNV_ITEM_MASTER.ITEM_DESCRIPTION;

                    txtItemShortNameOL.Text = iNV_ITEM_MASTER.ITEM_CODE_OL;
                    txtItemNumberOL.Text = iNV_ITEM_MASTER.ITEM_NUMBER_OL;
                    txtItemNameOL.Text = iNV_ITEM_MASTER.ITEM_NAME_OL;
                    txtItemDescriptionOL.Text = iNV_ITEM_MASTER.ITEM_DESCRIPTION_OL;


                    ddlItemCategory.SelectedValue = iNV_ITEM_MASTER.ITEM_CATEGORY_ID;
                    ddlItemUOM.SelectedValue = iNV_ITEM_MASTER.ITEM_UOM;
                    if (iNV_ITEM_MASTER.ITEM_WEIGHT != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_WEIGHT > 0)
                        {
                            txtWeight.Text = iNV_ITEM_MASTER.ITEM_WEIGHT.ToString();
                        }

                    }
                    if (iNV_ITEM_MASTER.ITEM_EFF_START_DT != null)
                    {
                        txtEffectiveStartDate.Text = DBMethod.ConvertDateToString(iNV_ITEM_MASTER.ITEM_EFF_START_DT.ToString());
                    }
                    if (iNV_ITEM_MASTER.ITEM_EFF_END_DT != null)
                    {
                        txtEffectiveEnddate.Text = DBMethod.ConvertDateToString(iNV_ITEM_MASTER.ITEM_EFF_END_DT.ToString());
                    }
                    if (iNV_ITEM_MASTER.ITEM_UNIT_PRICE != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_UNIT_PRICE > 0)
                        {
                            txtUnitPrice.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNV_ITEM_MASTER.ITEM_UNIT_PRICE.ToString());
                        }
                    }
                    if (iNV_ITEM_MASTER.ITEM_REORDER_QTY != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_REORDER_QTY > 0)
                        {
                            txtReOrderQuantity.Text = iNV_ITEM_MASTER.ITEM_REORDER_QTY.ToString();
                        }
                    }
                    if (iNV_ITEM_MASTER.ITEM_GROUP_ID != null)
                    {
                        ddlItemGroup.SelectedValue = iNV_ITEM_MASTER.ITEM_GROUP_ID;
                    }
                    if (iNV_ITEM_MASTER.SERVICE_DURATION != null)
                    {
                        txtServiceDuration.Text = iNV_ITEM_MASTER.SERVICE_DURATION.ToString();
                    }
                    //                    ddlServiceUOM.SelectedValue = iNV_ITEM_MASTER.SERVICE_DURATION_UOM;
                    //txtServiceDuration.Text = iNV_ITEM_MASTER.ITEM_WEIGHT.ToString();
                    ddlWeightUOM.SelectedValue = iNV_ITEM_MASTER.ITEM_WEIGHT_UOM;
                    if (iNV_ITEM_MASTER.ITEM_LENGTH != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_LENGTH > 0)
                        {

                            txtLength.Text = iNV_ITEM_MASTER.ITEM_LENGTH.ToString();
                        }
                    }
                    ddlLengthUOM.SelectedValue = iNV_ITEM_MASTER.ITEM_LENGTH_UOM;

                    ddlMetAcct.SelectedValue = iNV_ITEM_MASTER.INV_MAT_ACCT_CODE;

                    ddlmatovrhdacct.SelectedValue = iNV_ITEM_MASTER.INV_MAT_OVERHEAD_ACCT_CODE;
                    if (iNV_ITEM_MASTER.ITEM_AREA != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_AREA > 0)
                        {
                            txtArea.Text = iNV_ITEM_MASTER.ITEM_AREA.ToString();
                        }
                    }
                    ddlAreaUOM.SelectedValue = iNV_ITEM_MASTER.ITEM_AREA_UOM;
                    if (iNV_ITEM_MASTER.ITEM_VOLUME != null)
                    {
                        if (iNV_ITEM_MASTER.ITEM_VOLUME > 0)
                        {
                            txtVolume.Text = iNV_ITEM_MASTER.ITEM_VOLUME.ToString();
                        }
                    }
                    ddlVolumeUOM.SelectedValue = iNV_ITEM_MASTER.ITEM_VOLUME_UOM;
                    ddlitemtyp.SelectedValue = iNV_ITEM_MASTER.INV_ITEM_TYPE;
                    if (iNV_ITEM_MASTER.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                    if (iNV_ITEM_MASTER.SERVICE_FLAG == "1")
                    {
                        chkServiceflag.Checked = true;
                        ddlServiceUOM.Enabled = true;

                        ddlServiceUOM.SelectedValue = iNV_ITEM_MASTER.SERVICE_DURATION_UOM;

                    }
                    else
                    {
                        chkServiceflag.Checked = false;
                        ddlServiceUOM.Enabled = false;

                    }

                    if (iNV_ITEM_MASTER.ITEM_SALE_FLAG == "1")
                    {
                        chkActive.Checked = true;
                        // ddlServiceUOM.Enabled = true;
                        ddlCostofgoodssold.Enabled = true;
                        ddlRevAcct.Enabled = true;
                        ddlServiceUOM.SelectedValue = iNV_ITEM_MASTER.SERVICE_DURATION_UOM;
                        ddlCostofgoodssold.SelectedValue = iNV_ITEM_MASTER.INV_GOODS_COST_ACCT_CODE;
                        ddlRevAcct.SelectedValue = iNV_ITEM_MASTER.INV_REVENUE_ACCT_CODE;
                    }
                    else
                    {
                        // ddlServiceUOM.Enabled = false;
                        ddlCostofgoodssold.Enabled = false;
                        ddlRevAcct.Enabled = false;

                    }

                    chkSalesFlag.Checked = iNV_ITEM_MASTER.ITEM_SALE_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    chkPurchaseFlag.Checked = iNV_ITEM_MASTER.ITEM_PURCHASE_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    chkItemLotControl.Checked = iNV_ITEM_MASTER.ITEM_LOT_CONTROL == FINAppConstants.EnabledFlag ? true : false;
                    chkItemSerialControl.Checked = iNV_ITEM_MASTER.ITEM_SERIAL_CONTROL == FINAppConstants.EnabledFlag ? true : false;
                    txtAdditionalIdenti1.Text = iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER1;
                    txtAdditionalIdenti2.Text = iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER2;
                    txtAdditionalIdenti3.Text = iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER3;
                    txtAdditionalIdenti4.Text = iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER4;

                    if (ddlWeightUOM.SelectedValue != "")
                    {
                        if (ddlWeightUOM.SelectedValue.Length > 0)
                        {
                            txtWeight.Enabled = true;
                        }
                    }
                    if (ddlLengthUOM.SelectedValue != "")
                    {
                        if (ddlLengthUOM.SelectedItem.Value.Length > 0)
                        {
                            txtLength.Enabled = true;
                        }
                    }
                    if (ddlAreaUOM.SelectedValue != "")
                    {
                        if (ddlAreaUOM.SelectedItem.Value.Length > 0)
                        {
                            txtArea.Enabled = true;
                        }
                    }
                    if (ddlVolumeUOM.SelectedValue != "")
                    {
                        if (ddlVolumeUOM.SelectedItem.Value.Length > 0)
                        {
                            txtVolume.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.getCategory(ref ddlItemCategory);
            ComboFilling.fn_getUOMDetails(ref ddlItemUOM);
            ComboFilling.fn_getItemGroup(ref ddlItemGroup);
            ComboFilling.fn_getWeightUOM(ref ddlWeightUOM);
            ComboFilling.fn_getLengthUOM(ref ddlLengthUOM);
            ComboFilling.fn_getAreaUOM(ref ddlAreaUOM);
            ComboFilling.fn_getVolumeUOM(ref ddlVolumeUOM);
            Item_BLL.getServiceUOM(ref ddlServiceUOM);
            Lookup_BLL.GetLookUpValues(ref ddlitemtyp, "IS");
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlMetAcct);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlCostofgoodssold);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlRevAcct);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlmatovrhdacct);


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_ITEM_MASTER = (INV_ITEM_MASTER)EntityData;
                }

                iNV_ITEM_MASTER.ITEM_NUMBER = txtItemNumber.Text;
                iNV_ITEM_MASTER.ITEM_NUMBER_OL = txtItemNumberOL.Text;

                iNV_ITEM_MASTER.ITEM_CODE = txtItemShortName.Text;
                iNV_ITEM_MASTER.ITEM_NAME = txtItemName.Text;
                iNV_ITEM_MASTER.ITEM_DESCRIPTION = txtItemDescription.Text;


                iNV_ITEM_MASTER.ITEM_CODE_OL = txtItemShortNameOL.Text;
                iNV_ITEM_MASTER.ITEM_NAME_OL = txtItemNameOL.Text;
                iNV_ITEM_MASTER.ITEM_DESCRIPTION_OL = txtItemDescriptionOL.Text;


                iNV_ITEM_MASTER.ITEM_CATEGORY_ID = ddlItemCategory.SelectedValue.ToString();
                iNV_ITEM_MASTER.ITEM_UOM = ddlItemUOM.SelectedValue.ToString();

                if (txtEffectiveStartDate.Text != string.Empty)
                {
                    iNV_ITEM_MASTER.ITEM_EFF_START_DT = DBMethod.ConvertStringToDate(txtEffectiveStartDate.Text.ToString());
                }
                if (txtEffectiveEnddate.Text != string.Empty)
                {
                    iNV_ITEM_MASTER.ITEM_EFF_END_DT = DBMethod.ConvertStringToDate(txtEffectiveEnddate.Text.ToString());
                }

                iNV_ITEM_MASTER.ITEM_UNIT_PRICE = CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text.ToString());
                iNV_ITEM_MASTER.ITEM_REORDER_QTY = CommonUtils.ConvertStringToInt(txtReOrderQuantity.Text.ToString());
                iNV_ITEM_MASTER.ITEM_GROUP_ID = ddlItemGroup.SelectedValue.ToString();

                iNV_ITEM_MASTER.SERVICE_DURATION = CommonUtils.ConvertStringToDecimal(txtServiceDuration.Text.ToString());
                //                iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                iNV_ITEM_MASTER.ITEM_WEIGHT = CommonUtils.ConvertStringToDecimal(txtWeight.Text.ToString());
                iNV_ITEM_MASTER.ITEM_WEIGHT_UOM = ddlWeightUOM.SelectedValue.ToString();
                iNV_ITEM_MASTER.ITEM_LENGTH = CommonUtils.ConvertStringToDecimal(txtLength.Text.ToString());
                iNV_ITEM_MASTER.ITEM_LENGTH_UOM = ddlLengthUOM.SelectedValue.ToString();

                iNV_ITEM_MASTER.ITEM_AREA = CommonUtils.ConvertStringToDecimal(txtArea.Text.ToString());
                iNV_ITEM_MASTER.ITEM_AREA_UOM = ddlAreaUOM.SelectedValue.ToString();
                iNV_ITEM_MASTER.ITEM_VOLUME = CommonUtils.ConvertStringToDecimal(txtVolume.Text.ToString());
                iNV_ITEM_MASTER.ITEM_VOLUME_UOM = ddlVolumeUOM.SelectedValue.ToString();
                iNV_ITEM_MASTER.INV_ITEM_TYPE = ddlitemtyp.SelectedValue;
                iNV_ITEM_MASTER.INV_MAT_ACCT_CODE = ddlMetAcct.SelectedValue;

                iNV_ITEM_MASTER.INV_MAT_OVERHEAD_ACCT_CODE = ddlmatovrhdacct.SelectedValue;
                iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER1 = txtAdditionalIdenti1.Text;
                iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER2 = txtAdditionalIdenti2.Text;
                iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER3 = txtAdditionalIdenti3.Text;
                iNV_ITEM_MASTER.ADDITIONAL_IDENTIFIER4 = txtAdditionalIdenti4.Text;
                if (chkServiceflag.Checked == true)
                {
                    iNV_ITEM_MASTER.SERVICE_FLAG = FINAppConstants.EnabledFlag;
                    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                }
                else
                {
                    iNV_ITEM_MASTER.SERVICE_FLAG = FINAppConstants.DisabledFlag;
                    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = null;
                }
                iNV_ITEM_MASTER.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_MASTER.ITEM_SALE_FLAG = chkSalesFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_MASTER.ITEM_PURCHASE_FLAG = chkPurchaseFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_MASTER.ITEM_LOT_CONTROL = chkItemLotControl.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_MASTER.ITEM_SERIAL_CONTROL = chkItemSerialControl.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_ITEM_MASTER.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (chkSalesFlag.Checked == true)
                {
                    iNV_ITEM_MASTER.INV_REVENUE_ACCT_CODE = ddlRevAcct.SelectedValue;
                    iNV_ITEM_MASTER.INV_GOODS_COST_ACCT_CODE = ddlCostofgoodssold.SelectedValue;
                }
                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_ITEM_MASTER.MODIFIED_BY = this.LoggedUserName;
                    iNV_ITEM_MASTER.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    iNV_ITEM_MASTER.ITEM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0005.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    iNV_ITEM_MASTER.CREATED_BY = this.LoggedUserName;
                    iNV_ITEM_MASTER.CREATED_DATE = DateTime.Today;

                }



                iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_ITEM_MASTER.ITEM_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_ITEM_MASTER.ITEM_ID, iNV_ITEM_MASTER.ITEM_NAME);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEM", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                //Duplicate Validation Through Backend Package PKG_VALIDATIONS




                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE, txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_ITEM_MASTER.ITEM_ID, iNV_ITEM_MASTER.ITEM_NAME);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("ITEMNAME", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INV_ITEM_MASTER>(iNV_ITEM_MASTER);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<INV_ITEM_MASTER>(iNV_ITEM_MASTER, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_ITEM_MASTER>(iNV_ITEM_MASTER);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtWeight_TextChanged(object sender, EventArgs e)
        {
            if (txtWeight.Text.Length != null)
            {
                ddlWeightUOM.Enabled = true;

            }
            else
            {
                ddlWeightUOM.Enabled = false;
            }
        }

        protected void txtLength_TextChanged(object sender, EventArgs e)
        {
            if (txtLength.Text.Length != null)
            {
                ddlLengthUOM.Enabled = true;

            }

        }

        protected void txtArea_TextChanged(object sender, EventArgs e)
        {
            if (txtArea.Text.Length != null)
            {
                ddlAreaUOM.Enabled = true;

            }

        }

        protected void txtVolume_TextChanged(object sender, EventArgs e)
        {
            if (txtVolume.Text.Length != null)
            {
                ddlVolumeUOM.Enabled = true;

            }
        }


        private void emptyvalid()
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            if (iNV_ITEM_MASTER.ITEM_WEIGHT > 0)
            {

                if (ddlWeightUOM.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("WEIGHTEMT", Prop_File_Data["Weight_UOM_1_P"]
                        );
                }

            }
            if (iNV_ITEM_MASTER.ITEM_LENGTH > 0)
            {
                if (ddlLengthUOM.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("LENEMT", Prop_File_Data["Length_UOM_1_P"]);
                }
            }

            if (iNV_ITEM_MASTER.ITEM_AREA > 0)
            {

                if (ddlAreaUOM.SelectedValue == string.Empty)
                {
                    ErrorCollection.Add("AREAEMT", Prop_File_Data["Area_UOM_1_P"]);
                }
            }

            if (iNV_ITEM_MASTER.ITEM_VOLUME > 0)
            {
                if (ddlVolumeUOM.SelectedValue == string.Empty)
                {

                    ErrorCollection.Add("VOLEMT", Prop_File_Data["Volume_UOM_1_P"]);
                }
            }

        }

        protected void chkSalesFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSalesFlag.Checked == true)
            {
                ddlCostofgoodssold.Enabled = true;
                ddlRevAcct.Enabled = true;
            }
            else
            {
                ddlCostofgoodssold.Enabled = false;
                ddlRevAcct.Enabled = false;
            }

        }


        protected void ddlitemtyp_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlitemtyp.SelectedValue != "Service")
            {
                ddlServiceUOM.Enabled = true;
                iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
            }
            else
            {
                ddlServiceUOM.Enabled = false;
            }

        }

        protected void ddlWeightUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtWeight.Enabled = false;
            if (ddlWeightUOM.SelectedItem.Value.Length > 0)
            {
                txtWeight.Enabled = true;
            }

        }

        protected void ddlLengthUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtLength.Enabled = false;
            if (ddlLengthUOM.SelectedItem.Value.Length > 0)
            {
                txtLength.Enabled = true;
            }
        }

        protected void ddlAreaUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtArea.Enabled = false;
            if (ddlAreaUOM.SelectedItem.Value.Length > 0)
            {
                txtArea.Enabled = true;
            }
        }

        protected void ddlVolumeUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtVolume.Enabled = false;
            if (ddlVolumeUOM.SelectedItem.Value.Length > 0)
            {
                txtVolume.Enabled = true;
            }
        }

        protected void chkServiceflag_CheckedChanged(object sender, EventArgs e)
        {
            chkserviceuom();
        }

        private void chkserviceuom()
        {
            if (chkServiceflag.Checked == true)
            {
                ddlServiceUOM.Enabled = true;
            }
            else
            {
                ddlServiceUOM.Enabled = false;
            }
        }

    }
}