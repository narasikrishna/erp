﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class WarehouseEntry : PageBase
    {
        INV_WAREHOUSES iNV_WAREHOUSES = new INV_WAREHOUSES();
        INV_WAREHOUSE_LOCATIONS iNV_WAREHOUSE_LOCATIONS = new INV_WAREHOUSE_LOCATIONS();
        string ProReturn = null;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);                    
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<INV_WAREHOUSES> userCtx = new DataRepository<INV_WAREHOUSES>())
                    {
                        iNV_WAREHOUSES = userCtx.Find(r =>
                            (r.INV_WH_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_WAREHOUSES;
                    
                    txtWarehouseNumber.Text = iNV_WAREHOUSES.INV_WH_ID;
                    txtWarehouseName.Text = iNV_WAREHOUSES.INV_WH_NAME;
                    if (iNV_WAREHOUSES.INV_WH_NAME_OL != null)
                    {
                        txtWarehouseNameOL.Text = iNV_WAREHOUSES.INV_WH_NAME_OL;
                    }

                    ddlWarehouseType.SelectedValue = iNV_WAREHOUSES.INV_WH_TYPE;
                    txtWarehouseDescription.Text = iNV_WAREHOUSES.INV_WH_DESC;
                    txtWarehouseContact.Text = iNV_WAREHOUSES.INV_WH_CONTACT;
                    ddlWarehouseStatus.Text = iNV_WAREHOUSES.INV_WH_STATUS;


                    using (IRepository<INV_WAREHOUSE_LOCATIONS> userCtx = new DataRepository<INV_WAREHOUSE_LOCATIONS>())
                    {
                        iNV_WAREHOUSE_LOCATIONS = userCtx.Find(r =>
                            (r.INV_WH_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    txtAddress1.Text = iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD1;
                    txtAddress2.Text = iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD2;
                    txtAddress3.Text = iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD3;                

                    ddlCountry.SelectedValue = iNV_WAREHOUSE_LOCATIONS.INV_WH_COUNTRY;

                    Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
                    ddlState.SelectedValue = iNV_WAREHOUSE_LOCATIONS.INV_WH_STATE;

                    Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);                
                    ddlCity.SelectedValue = iNV_WAREHOUSE_LOCATIONS.INV_WH_CITY;

                    txtPostalCode.Text = iNV_WAREHOUSE_LOCATIONS.INV_WH_ZIP_CODE;
                    txtPhone.Text = iNV_WAREHOUSE_LOCATIONS.INV_WH_PHONE;


                    if (iNV_WAREHOUSES.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);                    
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlWarehouseType, "WT");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlWarehouseStatus, "WS");
            Supplier_BLL.fn_getCountry(ref ddlCountry);

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_WAREHOUSES = (INV_WAREHOUSES)EntityData;                   
                }

                iNV_WAREHOUSES.ATTRIBUTE1 = txtWarehouseNumber.Text;
                iNV_WAREHOUSES.INV_WH_NAME = txtWarehouseName.Text;

                iNV_WAREHOUSES.INV_WH_NAME_OL = txtWarehouseNameOL.Text;

                iNV_WAREHOUSES.INV_WH_TYPE = ddlWarehouseType.SelectedValue;
                iNV_WAREHOUSES.INV_WH_DESC = txtWarehouseDescription.Text;
                iNV_WAREHOUSES.INV_WH_STATUS = ddlWarehouseStatus.Text;
                iNV_WAREHOUSES.INV_WH_CONTACT = txtWarehouseContact.Text;
                iNV_WAREHOUSES.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                iNV_WAREHOUSES.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_WAREHOUSES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                iNV_WAREHOUSES.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INV_WAREHOUSE_LOCATIONS> userCtx = new DataRepository<INV_WAREHOUSE_LOCATIONS>())
                    {
                        iNV_WAREHOUSE_LOCATIONS = userCtx.Find(r =>
                            (r.INV_WH_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                }


                iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD1 = txtAddress1.Text;
                iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD2 = txtAddress2.Text;
                iNV_WAREHOUSE_LOCATIONS.INV_WH_ADD3 = txtAddress3.Text;

                iNV_WAREHOUSE_LOCATIONS.INV_WH_COUNTRY = ddlCountry.SelectedValue;             
                iNV_WAREHOUSE_LOCATIONS.INV_WH_STATE = ddlState.SelectedValue;           
                iNV_WAREHOUSE_LOCATIONS.INV_WH_CITY = ddlCity.SelectedValue;

                iNV_WAREHOUSE_LOCATIONS.INV_WH_ZIP_CODE = txtPostalCode.Text;
                iNV_WAREHOUSE_LOCATIONS.INV_WH_PHONE = txtPhone.Text;
               
                 
                 iNV_WAREHOUSE_LOCATIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                 iNV_WAREHOUSE_LOCATIONS.WORKFLOW_COMPLETION_STATUS = "1";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_WAREHOUSES.MODIFIED_BY = this.LoggedUserName;
                    iNV_WAREHOUSES.MODIFIED_DATE = DateTime.Today;

                    iNV_WAREHOUSE_LOCATIONS.MODIFIED_BY = this.LoggedUserName;
                    iNV_WAREHOUSE_LOCATIONS.MODIFIED_DATE = DateTime.Today;

                    iNV_WAREHOUSE_LOCATIONS.INV_WH_ID = Master.StrRecordId;

                }
                else
                {
                    iNV_WAREHOUSES.INV_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_007_M.ToString(), false, true);                 
                 
                    iNV_WAREHOUSES.CREATED_BY = this.LoggedUserName;
                    iNV_WAREHOUSES.CREATED_DATE = DateTime.Today;

                    iNV_WAREHOUSE_LOCATIONS.WH_LOCATION_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_007_D.ToString(), false, true);

                    iNV_WAREHOUSE_LOCATIONS.INV_WH_ID = iNV_WAREHOUSES.INV_WH_ID;
                             
                    iNV_WAREHOUSE_LOCATIONS.CREATED_BY = this.LoggedUserName;
                    iNV_WAREHOUSE_LOCATIONS.CREATED_DATE = DateTime.Today;

                }

                
                iNV_WAREHOUSES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_WAREHOUSES.INV_WH_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);                   
                }
            }
        }


        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, iNV_WAREHOUSES.INV_WH_ID, iNV_WAREHOUSES.INV_WH_NAME,iNV_WAREHOUSES.INV_WH_TYPE,iNV_WAREHOUSES.INV_WH_DESC);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("WAREHOUSE", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INV_WAREHOUSES>(iNV_WAREHOUSES);
                            DBMethod.SaveEntity<INV_WAREHOUSE_LOCATIONS>(iNV_WAREHOUSE_LOCATIONS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<INV_WAREHOUSES>(iNV_WAREHOUSES, true);
                            DBMethod.SaveEntity<INV_WAREHOUSE_LOCATIONS>(iNV_WAREHOUSE_LOCATIONS, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_WAREHOUSES>(iNV_WAREHOUSES);
                DBMethod.DeleteEntity<INV_WAREHOUSE_LOCATIONS>(iNV_WAREHOUSE_LOCATIONS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtWarehouseNumber_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtWarehouseName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlWarehouseType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}