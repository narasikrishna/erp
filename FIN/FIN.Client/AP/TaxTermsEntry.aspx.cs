﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class TaxTermsEntry : PageBase
    {
        TAX_TERMS tAX_TERMS = new TAX_TERMS();
        string ProReturn = null;
        string ProReturn_Valdn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<TAX_TERMS> userCtx = new DataRepository<TAX_TERMS>())
                    {
                        tAX_TERMS = userCtx.Find(r =>
                            (r.TAX_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = tAX_TERMS;

                    txtTaxName.Text = tAX_TERMS.TAX_NAME;
                    txtTaxNameOL.Text = tAX_TERMS.TAX_NAME;
                    txtTaxRate.Text = tAX_TERMS.TAX_RATE.ToString();
                    ddlTaxCountry.SelectedValue = tAX_TERMS.TAX_COUNTRY;
                    if (tAX_TERMS.TAX_EFF_START_DT != null)
                    {
                        txtEffectiveStartDate.Text = DBMethod.ConvertDateToString(tAX_TERMS.TAX_EFF_START_DT.ToString());
                    }
                    if (tAX_TERMS.TAX_EFF_END_DT != null)
                    {
                        txtEffectiveEndDate.Text = DBMethod.ConvertDateToString(tAX_TERMS.TAX_EFF_END_DT.ToString());
                    }
                    if (tAX_TERMS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tAX_TERMS = (TAX_TERMS)EntityData;
                }



                tAX_TERMS.TAX_NAME = txtTaxName.Text;
                tAX_TERMS.TAX_NAME = txtTaxNameOL.Text;


                tAX_TERMS.TAX_RATE = decimal.Parse(txtTaxRate.Text.ToString());
                tAX_TERMS.TAX_COUNTRY = ddlTaxCountry.SelectedValue;
                if (txtEffectiveStartDate.Text != string.Empty)
                {
                    tAX_TERMS.TAX_EFF_START_DT = DBMethod.ConvertStringToDate(txtEffectiveStartDate.Text.ToString());
                }
                if (txtEffectiveEndDate.Text != string.Empty)
                {
                    tAX_TERMS.TAX_EFF_END_DT = DBMethod.ConvertStringToDate(txtEffectiveEndDate.Text.ToString());
                }
                tAX_TERMS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    tAX_TERMS.MODIFIED_BY = this.LoggedUserName;
                    tAX_TERMS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    tAX_TERMS.TAX_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0001.ToString(), false, true);
                    //tAX_TERMS.WORKFLOW_COMPLETION_STATUS = "1";
                    tAX_TERMS.CREATED_BY = this.LoggedUserName;
                    tAX_TERMS.CREATED_DATE = DateTime.Today;


                }
                tAX_TERMS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tAX_TERMS.TAX_ID);
                tAX_TERMS.TAX_ORG_ID = VMVServices.Web.Utils.OrganizationID;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            ComboFilling.getCountry(ref ddlTaxCountry);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                //chkduplicateTAX();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //ErrorCollection.Clear();

               
                
                // Duplicate Check


                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, tAX_TERMS.TAX_ID.ToUpper(), tAX_TERMS.TAX_NAME.ToUpper());
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("TAXTERMS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, tAX_TERMS.TAX_ID.ToString(), tAX_TERMS.TAX_EFF_START_DT.ToString("dd/MMM/yyyy"), tAX_TERMS.TAX_EFF_END_DT.ToString(), int.Parse(tAX_TERMS.ENABLED_FLAG.ToString()));

                //if (ProReturn_Valdn != string.Empty)
                //{
                //    if (ProReturn_Valdn != "0")
                //    {

                //        ErrorCollection.Add("TAXTERMS", ProReturn_Valdn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}

                switch (Master.Mode)
                {

        
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<TAX_TERMS>(tAX_TERMS);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<TAX_TERMS>(tAX_TERMS, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        private void chkduplicateTAX()
        {
            try
            {

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                DataTable dttax = new DataTable();
                dttax = DBMethod.ExecuteQuery(FIN.DAL.AP.TaxTerms_DAL.getTaxName(txtTaxName.Text)).Tables[0];
                if (dttax.Rows.Count > 0)
                {
                    ErrorCollection.Add("DUPLICATE_TAX",Prop_File_Data["Tax_Name_already_exists_P"]); 
                       
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("IGDC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TAX_TERMS>(tAX_TERMS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }







    }
}