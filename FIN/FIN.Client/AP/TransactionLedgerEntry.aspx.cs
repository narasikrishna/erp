﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class TransactionLedgerEntry : PageBase
    {
        INV_TRANSACTION_LEDGER iNV_TRANSACTION_LEDGER = new INV_TRANSACTION_LEDGER();

        string ProReturn = null;
        Boolean savedBool;
        DataTable dtgriddata = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                

                //EntityData = null;

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<INV_TRANSACTION_LEDGER> userCtx = new DataRepository<INV_TRANSACTION_LEDGER>())
                //    {
                //        iNV_TRANSACTION_LEDGER = userCtx.Find(r =>
                //            (r.INV_TRAN_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = iNV_TRANSACTION_LEDGER;


                //    ddlTransactiontype.SelectedValue = iNV_TRANSACTION_LEDGER.INV_TRANS_TYPE;
                //    ddlItem.SelectedValue = iNV_TRANSACTION_LEDGER.INV_ITEM_ID;
                //    ddlWarehouse.SelectedValue = iNV_TRANSACTION_LEDGER.INV_WH_ID;
                //    txtQuantity.Text = iNV_TRANSACTION_LEDGER.INV_QTY.ToString();

                //    if (iNV_TRANSACTION_LEDGER.INV_TRANS_DATE != null)
                //    {
                //        txtTransdate.Text = DBMethod.ConvertDateToString(iNV_TRANSACTION_LEDGER.INV_TRANS_DATE.ToString());
                //    }
                //    txtRemarks.Text = iNV_TRANSACTION_LEDGER.INV_TRANS_REMARKS;

                //    if (iNV_TRANSACTION_LEDGER.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                //    {
                //        chkActive.Checked = true;
                //    }
                //    else
                //    {
                //        chkActive.Checked = false;
                //    }

                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            //FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlTransactiontype, "TRANSAC_TYPE");
            Item_BLL.GetItemName_frTransLedger(ref ddlItem);
            Warehouse_BLL.GetWareHouseName(ref ddlWarehouse);


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

              
                //if (EntityData != null)
                //{
                //    iNV_TRANSACTION_LEDGER = (INV_TRANSACTION_LEDGER)EntityData;
                //}
                //iNV_TRANSACTION_LEDGER.INV_TRANS_TYPE = ddlTransactiontype.SelectedValue.ToString();
                //iNV_TRANSACTION_LEDGER.INV_ITEM_ID = ddlItem.SelectedValue.ToString();
                //iNV_TRANSACTION_LEDGER.INV_WH_ID = ddlWarehouse.SelectedValue.ToString();
                //iNV_TRANSACTION_LEDGER.INV_QTY = CommonUtils.ConvertStringToDecimal(txtQuantity.Text.ToString());
                //iNV_TRANSACTION_LEDGER.INV_QOH = CommonUtils.ConvertStringToDecimal(txtQuantity.Text.ToString());
                //iNV_TRANSACTION_LEDGER.INV_ITEM_OPEN_BALANCE = CommonUtils.ConvertStringToDecimal("0");
                //if (txtTransdate.Text != string.Empty)
                //{
                //    iNV_TRANSACTION_LEDGER.INV_TRANS_DATE = DBMethod.ConvertStringToDate(txtTransdate.Text.ToString());
                //}
                //iNV_TRANSACTION_LEDGER.INV_TRANS_REMARKS = txtRemarks.Text.ToString();
                //iNV_TRANSACTION_LEDGER.INV_PO_LINE_ID = "null";
                //iNV_TRANSACTION_LEDGER.INV_PO_HEADER_ID = "null";
                //iNV_TRANSACTION_LEDGER.INV_LOT_ID = "null";
                //iNV_TRANSACTION_LEDGER.RECEIPT_ID = "null";


                //iNV_TRANSACTION_LEDGER.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //iNV_TRANSACTION_LEDGER.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                //// iNV_TRANSACTION_LEDGER.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;



                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    iNV_TRANSACTION_LEDGER.MODIFIED_BY = this.LoggedUserName;
                //    iNV_TRANSACTION_LEDGER.MODIFIED_DATE = DateTime.Today;

                //}
                //else
                //{
                //    iNV_TRANSACTION_LEDGER.INV_TRAN_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_027.ToString(), false, true);
                //    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                //    iNV_TRANSACTION_LEDGER.CREATED_BY = this.LoggedUserName;
                //    iNV_TRANSACTION_LEDGER.CREATED_DATE = DateTime.Today;

                //}



                //iNV_TRANSACTION_LEDGER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_TRANSACTION_LEDGER.INV_TRAN_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }





        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                //System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlTransactiontype;
                //slControls[1] = ddlItem;
                //slControls[2] = ddlWarehouse;
                //slControls[3] = txtQuantity;
                //slControls[4] = txtTransdate;

                //Dictionary<string, string> Prop_File_Data;
                //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

                //ErrorCollection.Clear();

                //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
                //string strMessage = Prop_File_Data["Transaction_Type_P"] + " ~ " + Prop_File_Data["Item_P"] + " ~ " + Prop_File_Data["Warehouse_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Transaction_Date_P"];

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}

                //AssignToBE();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}


                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, iNV_TRANSACTION_LEDGER.INV_TRAN_ID, iNV_TRANSACTION_LEDGER.INV_TRANS_TYPE, iNV_TRANSACTION_LEDGER.INV_ITEM_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("TRANSACTION LEDGER", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //if (iNV_TRANSACTION_LEDGER.INV_TRAN_ID != string.Empty &&  iNV_TRANSACTION_LEDGER.INV_TRAN_ID !=null)
                //{
                //    switch (Master.Mode)
                //    {
                //        case FINAppConstants.Add:
                //            {
                //                DBMethod.SaveEntity<INV_TRANSACTION_LEDGER>(iNV_TRANSACTION_LEDGER);

                //                savedBool = true;
                //                break;
                //            }
                //        case FINAppConstants.Update:
                //            {

                //                DBMethod.SaveEntity<INV_TRANSACTION_LEDGER>(iNV_TRANSACTION_LEDGER, true);

                //                savedBool = true;
                //                break;

                //            }
                //    }
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                //if (savedBool)
                //{
                //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //}

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_TRANSACTION_LEDGER>(iNV_TRANSACTION_LEDGER);

                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void BindGrid(DataTable dtData)
        {


            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();


        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            dtgriddata = DBMethod.ExecuteQuery(transactionLedger_DAL.GetTransLedgerDtl(ddlItem.SelectedValue, ddlWarehouse.SelectedValue)).Tables[0];
            BindGrid(dtgriddata);
        }




    }
}