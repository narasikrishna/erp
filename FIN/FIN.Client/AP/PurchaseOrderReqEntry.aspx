﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PurchaseOrderReqEntry.aspx.cs" Inherits="FIN.Client.AP.PurchaseOrderReqEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px;" id="lblRequisitionNumber" runat="server">
                Requisition Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px" id="lblRequisitionNumberValue"
                runat="server">
                <asp:TextBox ID="txtRequisitionNumber" runat="server" CssClass=" txtBox" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 149px" id="lblRequisitionDate">
                Requisition Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 152px">
                <asp:TextBox ID="txtRequisitionDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtRequisitionDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequisitionType">
                Requisition Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 173px">
                <asp:DropDownList ID="ddlRequisitionType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlRequisitionType_SelectedIndexChanged" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 145px" id="lblDepartmentName">
                Department Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlDepartmentName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDescription">
                Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 590px; height: 70px;">
                <asp:TextBox ID="txtDescription" runat="server" MaxLength="250" CssClass="txtBox"
                    Style="height: 60px;" TextMode="MultiLine" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Need By Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px">
                <asp:TextBox ID="txtNeedByDate" runat="server" TabIndex="6" CssClass="validate[] validate[custom[ReqDateDDMMYYY]] txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtendedr1"
                    TargetControlID="txtNeedByDate" OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                Requested By Employee
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlreqby" runat="server" AutoPostBack="true" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="7" OnSelectedIndexChanged="ddlreqby_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div5">
                Total
            </div>
            <div class="divtxtBox LNOrient" style="width: 170px">
                <asp:TextBox runat="server" ID="txtTotalAmount" Enabled="false" CssClass="txtBox_N"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 149px" id="lblItem/Service">
                Item/Service
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlItemService" runat="server" AutoPostBack="true" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlItemService_SelectedIndexChanged" TabIndex="8">
                </asp:DropDownList>
            </div>
            <%--<div class="lblBox" style="float: left; width: 150px" id="Div5">
                Requested By
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtRequestedBy" runat="server" TabIndex="10" CssClass="txtBox"></asp:TextBox>
            </div>--%>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                    Active
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="9" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer  LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                    OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                    Width="1000px" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="True" DataKeyNames="PO_REQ_LINE_ID,PO_REQ_ID,PO_REQ_UOM,ITEM_ID,UOM_ID">
                    <Columns>
                        <asp:TemplateField HeaderText="Line No" FooterStyle-Width="50px">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" TabIndex="10" Enabled="false" Text='<%# Eval("PO_REQ_LINE_NUM") %>'
                                    Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Text='' Enabled="false"
                                    CssClass="EntryFont RequiredField txtBox" Width="50px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("PO_REQ_LINE_NUM") %>'></asp:Label>
                                <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                    Width="50px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Name" FooterStyle-Width="220px">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype RequiredField"
                                    TabIndex="11" Width="220px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlItemName" runat="server" CssClass="ddlStype RequiredField"
                                    TabIndex="11" Width="220px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="220px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Quantity" FooterStyle-Width="80px">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("PO_QUANTITY") %>'
                                    TabIndex="12" CssClass="EntryFont RequiredField txtBox_N" Width="80px" AutoPostBack="True"
                                    OnTextChanged="txtQuantity_TextChanged"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                    TargetControlID="txtQuantity" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    TabIndex="12" Width="80px" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers" TargetControlID="txtQuantity" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("PO_QUANTITY") %>' Width="80px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="UOM" FooterStyle-Width="220px">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlUOM" runat="server" CssClass="ddlStype RequiredField" Width="220px"
                                    TabIndex="13">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlUOM" runat="server" CssClass="ddlStype RequiredField" Width="220px"
                                    TabIndex="13">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM_CODE") %>' Width="220px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estimated Price" FooterStyle-Width="100px">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUnitPrice" MaxLength="13" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                    TabIndex="14" runat="server" Text='<%# Eval("PO_ITEM_UNIT_PRICE") %>' CssClass="EntryFont RequiredField txtBox_N"
                                    Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                        runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtUnitPrice" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtUnitPrice" MaxLength="13" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                    TabIndex="14" runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                        TargetControlID="txtUnitPrice" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("PO_ITEM_UNIT_PRICE") %>'
                                    CssClass=" txtBox_N" Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estimated Amount" FooterStyle-Width="100px">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPOAmount" Enabled="false" Text='<%# Eval("PO_Amount") %>' runat="server"
                                    TabIndex="15" MaxLength="13" CssClass="EntryFont RequiredField txtBox_N" Width="70px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtPOAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPOAmount" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    TabIndex="15" MaxLength="13" Width="100px"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtPOAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("PO_Amount") %>' Width="100px"
                                    CssClass=" txtBox_N"> </asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" FooterStyle-Width="100px">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGridDescription" MaxLength="250" runat="server" Text='<%# Eval("PO_DESCRIPTION") %>'
                                    TextMode="MultiLine" TabIndex="15" CssClass="EntryFont txtBox" Width="100px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtGridDescription" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                    TextMode="MultiLine" TabIndex="15" Width="100px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" MaxLength="250" runat="server" Text='<%# Eval("PO_DESCRIPTION") %>'
                                    Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add/Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                    TabIndex="16" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                    TabIndex="17" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                    TabIndex="18" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                    TabIndex="19" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                    TabIndex="20" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                                TabIndex="21" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="22" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="23" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="24" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table width="100%">
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
