﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class SupplierBankDetails : PageBase
    {

        SUPPLIER_CUSTOMER_BANK SUPPLIER_CUSTOMER_BANK = new SUPPLIER_CUSTOMER_BANK();
        SupplierBankDetails_BLL SupplierBankDetails_BLL = new SupplierBankDetails_BLL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Supplier_BLL.GetSupplierName(ref ddlSupplierNumber);
            //SupplierBankDetails_BLL.fn_getSupplierName(ref ddlSupplierNumber);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtSupplierName.Enabled = false;
                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBankDetails_DAL.GetSupplierbankdtls(Master.RecordID)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    using (IRepository<SUPPLIER_CUSTOMER_BANK> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BANK>())
                    {
                        SUPPLIER_CUSTOMER_BANK = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = SUPPLIER_CUSTOMER_BANK;

                    ddlSupplierNumber.SelectedValue = SUPPLIER_CUSTOMER_BANK.VENDOR_ID;
                    fillsupNm();
                    SUPPLIER_CUSTOMERS SUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                    using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                    {
                        SUPPLIER_CUSTOMERS = userCtx.Find(r =>
                            (r.VENDOR_ID == SUPPLIER_CUSTOMER_BANK.VENDOR_ID)
                            ).SingleOrDefault();
                    }

                    if (SUPPLIER_CUSTOMERS != null)
                    {
                        ddlSupplierNumber.SelectedValue = SUPPLIER_CUSTOMERS.VENDOR_CODE;
                        txtSupplierName.Text = SUPPLIER_CUSTOMERS.VENDOR_NAME;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Supplier Bank ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    SUPPLIER_CUSTOMER_BANK = new SUPPLIER_CUSTOMER_BANK();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != "0")
                    {
                        using (IRepository<SUPPLIER_CUSTOMER_BANK> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BANK>())
                        {
                            SUPPLIER_CUSTOMER_BANK = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    SUPPLIER_CUSTOMER_BANK.BANK_ID = dtGridData.Rows[iLoop]["BANK_ID"].ToString();
                    SUPPLIER_CUSTOMER_BANK.BANK_BRANCH_ID = dtGridData.Rows[iLoop]["BANK_BRANCH_ID"].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_BANK_ACCOUNT_CODE = dtGridData.Rows[iLoop]["VENDOR_BANK_ACCOUNT_CODE"].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_BANK_ACCTOUNT_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString();
                    SUPPLIER_CUSTOMER_BANK.VENDOR_ID = ddlSupplierNumber.SelectedValue;
                    //SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = "1";
                    SUPPLIER_CUSTOMER_BANK.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        SUPPLIER_CUSTOMER_BANK.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        SUPPLIER_CUSTOMER_BANK.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(SUPPLIER_CUSTOMER_BANK, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != "0")
                        {

                            SUPPLIER_CUSTOMER_BANK.MODIFIED_BY = this.LoggedUserName;
                            SUPPLIER_CUSTOMER_BANK.MODIFIED_DATE = DateTime.Today;
                            SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SUPPLIER_CUSTOMER_BANK.PK_ID.ToString());

                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK, true);
                            saveBool = true;

                        }
                        else
                        {
                            SUPPLIER_CUSTOMER_BANK.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SUPPLIER_CUSTOMER_BANK_SEQ);
                            SUPPLIER_CUSTOMER_BANK.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, SUPPLIER_CUSTOMER_BANK.PK_ID.ToString());

                            SUPPLIER_CUSTOMER_BANK.CREATED_BY = this.LoggedUserName;
                            SUPPLIER_CUSTOMER_BANK.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK);
                            saveBool = true;
                        }


                    }

                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEEmpBank.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                getBankBranch(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void getBankBranch(GridViewRow gvr)
        {
            DropDownList ddl_BankName = (DropDownList)gvr.FindControl("ddlBankname");
            DropDownList ddl_BranchName = (DropDownList)gvr.FindControl("ddlBranchName");
            FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddl_BranchName, ddl_BankName.SelectedValue.ToString());
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAccountType = tmpgvr.FindControl("ddlAccountType") as DropDownList;
                DropDownList ddlBranchName = tmpgvr.FindControl("ddlBranchName") as DropDownList;
                DropDownList ddlBankname = tmpgvr.FindControl("ddlBankname") as DropDownList;

                //   SupplierBankDetails_BLL.fn_getfrlookup(ref ddlAccountType, "Account Type");

                Lookup_BLL.GetLookUpValues(ref ddlAccountType, "ACT_TYP");
                // SupplierBankDetails_BLL.fn_getBankBranchName(ref ddlBranchName);
                SupplierBankDetails_BLL.fn_getBankName(ref ddlBankname);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccountType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                    ddlBankname.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["BANK_ID"].ToString();
                    FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankname.SelectedValue.ToString());

                    ddlBranchName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["BANK_BRANCH_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection.Remove("ddlSupplierNumber");

                if (ddlSupplierNumber.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddlSupplierNumber.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
                {
                    ErrorCollection.Add("ddlSupplierNumber", "Supplier number cannot be empty");
                    return;
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlBankname = gvr.FindControl("ddlBankname") as DropDownList;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            DropDownList ddlAccountType = gvr.FindControl("ddlAccountType") as DropDownList;
            TextBox txtAcctNo = gvr.FindControl("txtAcctNo") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            TextBox txtifsc = gvr.FindControl("txtifsc") as TextBox;
            TextBox txtswift = gvr.FindControl("txtswift") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlBankname;
            //   slControls[1] = ddlBranchName;
            slControls[1] = ddlAccountType;
            slControls[2] = txtAcctNo;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~TextBox";
            string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Account_Type_P"] + " ~ " + Prop_File_Data["Account_No_P"] + "";
            //string strMessage = "Bank Name ~ Branch Name ~ Account Type ~ Account No";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            ErrorCollection.Remove("ddlBranchName");
            if (ddlBranchName.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddlBranchName.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ddlBranchName", "Branch cannot be empty");
                return drList;
            }

            string strCondition = "VENDOR_BANK_ACCOUNT_CODE='" + txtAcctNo.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (ddlBankname.SelectedItem != null)
            {
                drList["BANK_ID"] = ddlBankname.SelectedItem.Value;
                drList["BANK_NAME"] = ddlBankname.SelectedItem.Text;
            }

            if (ddlBranchName.SelectedItem != null)
            {
                drList["BANK_BRANCH_ID"] = ddlBranchName.SelectedItem.Value;
                drList["BANK_BRANCH_NAME"] = ddlBranchName.SelectedItem.Text;
            }
            if (ddlAccountType.SelectedItem != null)
            {
                drList["LOOKUP_ID"] = ddlAccountType.SelectedItem.Value;
                drList["LOOKUP_NAME"] = ddlAccountType.SelectedItem.Text;
            }

            drList["BANK_IFSC_CODE"] = txtifsc.Text;
            drList["BANK_SWIFT_CODE"] = txtswift.Text;

            drList["VENDOR_BANK_ACCOUNT_CODE"] = txtAcctNo.Text;

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountCodesEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_UPD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    else
                    {
                        CheckBox chkact = (CheckBox)e.Row.FindControl("chkact");

                        if (gvData.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"] != null)
                        {
                            if (gvData.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"].ToString() == "1")
                            {
                                chkact.Checked = true;
                            }
                            else
                            {
                                chkact.Checked = false;
                            }
                        }

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    SUPPLIER_CUSTOMER_BANK.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<SUPPLIER_CUSTOMER_BANK>(SUPPLIER_CUSTOMER_BANK);
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_BTN_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void ddlSupplierNumber_SelectedIndexChanged1(object sender, EventArgs e)
        {
            //txtSupplierName.Text = ddlSupplierNumber.SelectedItem.Text;
            //ddlSupplierNumber.Focus();
            fillsupNm();

        }
        private void fillsupNm()
        {
            DataTable dtSupplierNmae = new DataTable();
            if (ddlSupplierNumber.SelectedValue != null)
            {
                dtSupplierNmae = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBankDetails_DAL.getSuppNM(ddlSupplierNumber.SelectedValue)).Tables[0];
                txtSupplierName.Text = dtSupplierNmae.Rows[0]["vendor_name"].ToString();
            }

        }

        protected void txtSupplierName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlBranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            TextBox txtifsc = gvr.FindControl("txtifsc") as TextBox;
            TextBox txtswift = gvr.FindControl("txtswift") as TextBox;

            DataTable dtifsc_swiftcode = new DataTable();
            dtifsc_swiftcode = DBMethod.ExecuteQuery(FIN.DAL.AR.CustomerBankDetails_DAL.GetIFSC_SWIFTCODE(ddlBranchName.SelectedValue)).Tables[0];
            txtifsc.Text = dtifsc_swiftcode.Rows[0]["BANK_IFSC_CODE"].ToString();
            txtswift.Text = dtifsc_swiftcode.Rows[0]["BANK_SWIFT_CODE"].ToString();

        }



    }
}